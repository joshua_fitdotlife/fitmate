package com.fitdotlife.fitmate_lib.image;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.widget.Toast;

import com.fitdotlife.fitmate.R;
import com.fitdotlife.fitmate_lib.customview.RoundedImageView;
import com.fitdotlife.fitmate.ConfigureLog4J;
import com.soundcloud.android.crop.Crop;
import org.apache.log4j.Log;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by wooseok on 2015-06-16.
 */
public class ImageUtil extends Activity {
    public static final String PICTURE_URI = "PICURI";
    protected static final int CAMERA_REQUEST = 2;
    protected static final int GALLERY_PICTURE = 1;
    private final static String TAG = "ImageUtil";

    private Intent pictureActionIntent = null;


    private Handler mHandler = new Handler();
    private Uri fileUri, newfileUri;
    public static String cameraUri;
    public static String cropUri;

    Bitmap bitmap;
    String selectedImagePath;
    private ProgressDialog mDialog = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startDialog();
            }
        }, 0);
    }

    public static Uri getOutputMediaFileUri(){
        return Uri.fromFile(getOutputMediaFile());
    }
    public static Uri getOutputPhotoFileUri(){ return Uri.fromFile(getOutputPhoto()); }

    public static File getOutputPhoto()
    {
        String logPaths = ConfigureLog4J.FolderPath;
        File mediaStorageDir = new File(logPaths);

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                android.util.Log.d("MyCameraApp", "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        File mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                "MyPhoto.jpg");

        return mediaFile;
    }

    public static File getOutputMediaFile(){
        String logPaths = ConfigureLog4J.FolderPath;
        File mediaStorageDir = new File(logPaths);
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                android.util.Log.d("MyCameraApp", "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                "IMG_"+ timeStamp + ".jpg");

        return mediaFile;
    }

            @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){

        Log.i(TAG, "onActivityResult");

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode != Crop.REQUEST_CROP) {
            if (requestCode == GALLERY_PICTURE) {
                Log.i(TAG, "GALLERY_PICTURE");
                if (data != null) {
                    Uri selectedimg = data.getData();
                    fileUri = selectedimg;
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedimg);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } else if (requestCode == CAMERA_REQUEST) {
                Log.i(TAG, "CAMERA_REQUEST");
                if (data != null) {
                    if (data.hasExtra("data")) {

                        // retrieve the bitmap from the intent
                        bitmap = (Bitmap) data.getExtras().get("data");


                        Cursor cursor = getContentResolver()
                                .query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                                        new String[]{
                                                MediaStore.Images.Media.DATA,
                                                MediaStore.Images.Media.DATE_ADDED,
                                                MediaStore.Images.ImageColumns.ORIENTATION},
                                        MediaStore.Images.Media.DATE_ADDED, null, "date_added ASC");
                        if (cursor != null && cursor.moveToFirst()) {
                            do {
                                Uri uri = Uri.parse(cursor.getString(cursor
                                        .getColumnIndex(MediaStore.Images.Media.DATA)));
                                selectedImagePath = uri.toString();
                            } while (cursor.moveToNext());
                            cursor.close();
                        }

                        android.util.Log.e("path => ",
                                selectedImagePath);


                        bitmap = Bitmap.createScaledBitmap(bitmap, 1500,
                                1500, false);
                    } else if (data.getExtras() == null) {

                    }
                }
                else {
                    try {

                        //if( fileUri == null ){
                        //    fileUri = getOutputMediaFileUri();
                        //}
                        //bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), fileUri);

                        fileUri = Uri.parse( cameraUri );
                        File out = new File( new URI( fileUri.toString() ));

                        if(!out.exists()) {

                            Toast.makeText(getBaseContext(),

                                    "Error while capturing image", Toast.LENGTH_LONG)

                                    .show();

                            return;

                        }

                        bitmap = BitmapFactory.decodeFile(out.getAbsolutePath());


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }


            ExifInterface exif = null;     //Since API Level 5
            try {
                exif = new ExifInterface(fileUri.toString());
                String exifOrientation = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.i(TAG, "Crop Start");

            newfileUri = getOutputMediaFileUri();
            cropUri = newfileUri.toString();
            Crop.of(fileUri, newfileUri).asSquare().withMaxSize(500,500).start(this);

            Log.i(TAG, "Crop END");
            stopProgressDialog();
        }
        else if (resultCode == RESULT_CANCELED) {
            finish();
        }


        if (requestCode == Crop.REQUEST_CROP && resultCode == RESULT_OK) {
            try {
                //bitmap = Media.getBitmap(this.getContentResolver(), newfileUri);

                if(newfileUri == null){
                    newfileUri = Uri.parse( cropUri );
                }

                Bitmap ddd = null;
                try {
                    ddd = RoundedImageView.getCorrectlyOrientedImage(getApplicationContext(), newfileUri);
                    //iui.setImageBitmap(newfileUri);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Intent intent = new Intent();
                intent.putExtra(PICTURE_URI, newfileUri);
                setResult(Activity.RESULT_OK, intent);
                finish();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void startDialog() {
        AlertDialog.Builder myAlertDialog = new AlertDialog.Builder(this);
        myAlertDialog.setTitle( this.getString( R.string.myprofile_howto_select_photo ) );
        myAlertDialog.setMessage(this.getString(R.string.myprofile_select_photo));

        myAlertDialog.setPositiveButton(this.getString(R.string.myprofile_gallery),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1)
                    {
                        pictureActionIntent = new Intent(Intent.ACTION_GET_CONTENT, null);
                        pictureActionIntent.setType("image/*");
                        pictureActionIntent.putExtra("return-data", true);
                        startActivityForResult(pictureActionIntent, GALLERY_PICTURE);
                    }
                });

        myAlertDialog.setNegativeButton(this.getString(R.string.myprofile_camera),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1)
                    {
                        pictureActionIntent = new Intent( MediaStore.ACTION_IMAGE_CAPTURE );
                        File tempFile = getOutputMediaFile();
                        fileUri = Uri.fromFile(tempFile);
                        cameraUri = fileUri.toString();
                        pictureActionIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri );
                        startActivityForResult(pictureActionIntent, CAMERA_REQUEST);
                    }
                });

        myAlertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                finish();
            }
        });
        myAlertDialog.show();
    }

    public void startProgressDialog( String title , String message ){
        this.mDialog =  new ProgressDialog(this);
        mDialog.setTitle(title);
        mDialog.setMessage( message );
        mDialog.setIndeterminate( true );
        mDialog.setCancelable( false );
        mDialog.show();
    }

    public void stopProgressDialog(){
        if( this.mDialog != null ) {
            this.mDialog.dismiss();
        }
    }
}
