package com.fitdotlife.fitmate_lib.imodel;

/**
 * Created by Joshua on 2015-03-18.
 */
public interface IModel {

    void open();
    void close();

}
