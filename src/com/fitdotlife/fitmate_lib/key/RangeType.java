package com.fitdotlife.fitmate_lib.key;

/**
 * Created by Joshua on 2016-02-11.
 */
public enum RangeType {
    NO_RANGE , ONE_RANGE , TWO_RANGE
}
