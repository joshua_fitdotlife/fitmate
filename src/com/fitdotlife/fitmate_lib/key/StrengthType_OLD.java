package com.fitdotlife.fitmate_lib.key;
/**
 * 운동프로그램에서 목표 강도의 범위를 입력할때 입력 형식을 나타낸다.
 * @author fitlife.soondong
 *
 */
public enum StrengthType_OLD {
	/**
	 * maximal oxygen consumption, maximal oxygen uptake, peak oxygen uptake or maximal aerobic capacity
	 */
	VO2MAX(1),
	/**
	 * relative exercise intensities
	 */
    VO2R(2),
    /*&
     * rating of perceived exertion
     */
    RPE(3),
    /**
     * Metabolic Equivalent of Task
     */

    MET(4),
    /**
     * classify four level of intensity- underlow, low, medium, high
     */
    EXERCISE_STRENGTH(5),
    /**
     * sum of calorie of a day
     */
    CALORIE(6),
    /**
     * sum of calories of a week
     */
    TOTAL_CALORIE(7);

    private int mValue;
    private StrengthType_OLD(int value)
    {
       this.mValue = value;
    }

    public int getValue(){
        return this.mValue;
    }

    public static StrengthType_OLD getStrengthType( int value ){
        StrengthType_OLD strengthType = null;

            switch( value ){
                case 1:
                    strengthType = StrengthType_OLD.VO2MAX;
                    break;
                case 2:
                    strengthType = StrengthType_OLD.VO2R;
                    break;
                case 3:
                    strengthType = StrengthType_OLD.RPE;
                    break;
                case 4:
                    strengthType = StrengthType_OLD.MET;
                    break;
                case 5:
                    strengthType = StrengthType_OLD.EXERCISE_STRENGTH;
                    break;
                case 6:
                    strengthType = StrengthType_OLD.CALORIE;
                    break;
                case 7:
                    strengthType = StrengthType_OLD.TOTAL_CALORIE;
                    break;
            }

        return strengthType;
    }

}
