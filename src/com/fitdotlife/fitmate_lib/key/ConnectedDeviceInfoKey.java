package com.fitdotlife.fitmate_lib.key;

/**
 * Created by fitlife.soondong on 2015-05-11.
 */
public class ConnectedDeviceInfoKey {
    public static final String RecentConnectedTime="RecentConnectedTime";
    public static final String BatteryLevel="BatteryChargeLevel";
}
