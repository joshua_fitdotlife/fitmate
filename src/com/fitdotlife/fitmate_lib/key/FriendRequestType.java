package com.fitdotlife.fitmate_lib.key;

/**
 * Created by Joshua on 2015-08-06.
 */
public enum FriendRequestType {
    SENDREQUEST(0) , RECEIVEREQUEST(1);

    private int mValue;

    FriendRequestType( int value )
    {
        this.mValue = value;
    }

    public int getValue()
    {
        return this.mValue;
    }


    public static FriendRequestType getFriendRequestType( int value )
    {
        FriendRequestType friendRequestType = null;

        switch( value )
        {
            case 0:
                friendRequestType = FriendRequestType.SENDREQUEST;
                break;
            case 1:
                friendRequestType = FriendRequestType.RECEIVEREQUEST;
                break;
        }

        return friendRequestType;
    }
}
