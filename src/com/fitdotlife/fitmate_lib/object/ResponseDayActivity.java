package com.fitdotlife.fitmate_lib.object;

/**
 * Created by Joshua on 2015-02-02.
 */
public class ResponseDayActivity {

    public static final String DAY_ACTIVITY_DATE_KEY = "activitydate";
    public static final String DAY_ACTIVITY_WEEK_SCORE_KEY = "score";
    public static final String DAY_ACTIVITY_CALORIE_KEY = "calorie";
    public static final String DAY_ACTIVITY_STRENGTH_HIGH_KEY = "strengthhigh";
    public static final String DAY_ACTIVITY_STRENGTH_MEDIUM_KEY = "strengthmedium";
    public static final String DAY_ACTIVITY_STRENGTH_LOW_KEY = "strenghtlow";
    public static final String DAY_ACTIVITY_STRENGTH_UNDER_LOW_KEY = "strengthunderlow";
    public static final String DAY_ACTIVITY_MET_ARRAY_KEY = "metarray";
    public static final String DAY_ACTIVITY_AVERAGE_MET_KEY = "averagemet";
    public static final String DAY_ACTIVITY_ACHIEVE_TARGET_KEY = "achieveoftarget";

    private String activityDate = null;
    private double calorie = 0;
    private int strengthHigh = 0;
    private int strengthMedium = 0;
    private int strengthLow = 0;
    private int strengthUnderLow = 0;
    private float[] metArray = null;
    private float averageMET = 0;
    private int weekScore = 0;
    private int achieveOfTarget = 0;
    private int predayScore = 0;
    private int todayScore = 0;
    private int extraScore = 0;
    private boolean isAchieveMax = false;
    private int possibleMaxScore = 0;
    private int possibleTimes = 0;
    private int possibleValue = 0;


    public boolean isAchieveMax() {
        return isAchieveMax;
    }

    public void setAchieveMax(boolean isAchieveMax) {
        this.isAchieveMax = isAchieveMax;
    }

    public int getPossibleMaxScore() {
        return possibleMaxScore;
    }

    public void setPossibleMaxScore(int possibleMaxScore) {
        this.possibleMaxScore = possibleMaxScore;
    }

    public int getPossibleTimes() {
        return possibleTimes;
    }

    public void setPossibleTimes(int possibleTimes) {
        this.possibleTimes = possibleTimes;
    }

    public int getPossibleValue() {
        return possibleValue;
    }

    public void setPossibleValue(int possibleValue) {
        this.possibleValue = possibleValue;
    }

    public int getExtraScore() {return this.extraScore; }

    public void setExtraScore( int extraScore ){ this.extraScore = extraScore; }

    public int getPredayScore(){ return this.predayScore; }

    public void setPredayScore( int predayScore ){ this.predayScore = predayScore; }

    public int getTodayScore(){ return this.todayScore; }

    public void setTodayScore( int todayScore ){ this.todayScore = todayScore; }

    public String getActivityDate() {
        return activityDate;
    }

    public void setActivityDate(String activityDate) {
        this.activityDate = activityDate;
    }

    public double getCalorie() {
        return calorie;
    }

    public void setCalorie( double calorie) {
        this.calorie = calorie;
    }

    public int getStrengthHigh() {
        return strengthHigh;
    }

    public void setStrengthHigh(int strengthHigh) {
        this.strengthHigh = strengthHigh;
    }

    public int getStrengthMedium() {
        return strengthMedium;
    }

    public void setStrengthMedium(int strengthMedium) {
        this.strengthMedium = strengthMedium;
    }

    public int getStrengthLow() {
        return strengthLow;
    }

    public void setStrengthLow(int strengthLow) {
        this.strengthLow = strengthLow;
    }

    public int getStrengthUnderLow() {
        return strengthUnderLow;
    }

    public void setStrengthUnderLow(int strengthUnderLow) {
        this.strengthUnderLow = strengthUnderLow;
    }

    public float[] getMetArray() {
        return metArray;
    }

    public void setMetArray(float[] metArray) {
        this.metArray = metArray;
    }

    public float getAverageMET() {
        return averageMET;
    }

    public void setAverageMET(float averageMet) {
        this.averageMET = averageMet;
    }

    public int getWeekScore() {
        return weekScore;
    }

    public void setWeekScore(int weekScore) {
        this.weekScore = weekScore;
    }


    public int getAchieveOfTarget() {
        return achieveOfTarget;
    }

    public void setAchieveOfTarget(int achieveOfTarget) {
        this.achieveOfTarget = achieveOfTarget;
    }


}
