package com.fitdotlife.fitmate_lib.object;

import android.util.Base64;

import com.fitdotlife.fitmate_lib.service.protocol.object.TimeInfo;

import org.apache.log4j.Log;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Joshua on 2015-02-02.
 */
public class DayActivity {

    public static final String DAY_ACTIVITY_SERVER_UPDATE_KEY = "update";

    public static final String DAY_ACTIVITY_DATE_KEY = "activitydate";
    public static final String DAY_ACTIVITY_WEEK_SCORE_KEY = "score";
    public static final String DAY_ACTIVITY_CALORIE_KEY = "calorie";
    public static final String DAY_ACTIVITY_STRENGTH_HIGH_KEY = "strengthhigh";
    public static final String DAY_ACTIVITY_STRENGTH_MEDIUM_KEY = "strengthmedium";
    public static final String DAY_ACTIVITY_STRENGTH_LOW_KEY = "strengthlow";
    public static final String DAY_ACTIVITY_STRENGTH_UNDER_LOW_KEY = "strengthunder";
    public static final String DAY_ACTIVITY_MET_ARRAY_KEY = "metdataarray";
    public static final String DAY_ACTIVITY_AVERAGE_MET_KEY = "averagemet";
    public static final String DAY_ACTIVITY_ACHIEVE_TARGET_KEY = "achieve";
    public static final String DAY_ACTIVITY_STARTTIME_KEY = "starttime";
    public static final String DAY_ACTIVITY_DATASAVEINTERVAL_KEY = "interval";
    public static final String DAY_ACTIVITY_TODAYSCORE_KEY = "todayscore";
    public static final String DAY_ACTIVITY_PREDAYSCORE_KEY = "predayscore";
    public static final String DAY_ACTIVITY_EXTRASCORE_KEY = "extrascore";
    public static final String DAY_ACTIVITY_ACHIEVEMAX_KEY = "achievemax";
    public static final String DAY_ACTIVITY_POSSIBLEMAXSCORE_KEY = "possiblemaxscore";
    public static final String DAY_ACTIVITY_POSSIBLETIMES_KEY = "possibletimes";
    public static final String DAY_ACTIVITY_POSSIBLEVALUE_KEY = "possiblevalue";
    public static final String DAY_ACTIVITY_FAT_KEY = "fat";
    public static final String DAY_ACTIVITY_CARBOHYDRATE_KEY = "carbohydrate";
    public static final String DAY_ACTIVITY_DISTANCE_KEY = "distance";



    public static final String DATE_KEY = "date";

    public static final String LOCAL_START_DATA_DATE_KEY = "localstartdatadate";


    private final int DAY_SECOND = 60 * 60 * 24;

    private TimeInfo startTime = null;
    private float averageMET = 0;
    private int calorieByActivity = 0;
    private int[] hmlStrengthTime = new int[]{0,0,0,0};
    private int achieveOfTarget = 0;
    private byte[] metByteArray = null;
    private float[] metArray = null;
    private int programID = 0;
    private int dataSaveInterval = -1;
    private float[] metRawArray = null;
    private int lastFileIndex = 0;
    private DayActivity lastDayActivity = null;
    private List<Integer> fileIndexList = new ArrayList<Integer>();
    private int todayScore = 0;
    private int predayScore = 0;
    private int extraScore = 0;
    private boolean isAchieveMax = false;
    private int possibleMaxScore = 0;
    private int possibleTimes = 0;
    private int possibleValue = 0;
    private float fat = 0;
    private float carbohydrate = 0;
    private double distance = 0;

    private int uploadState = -1;

    public DayActivity(){}

//    public DayActivity( TimeInfo startTime , int calorie , int[] hmlTime , float[] metArray , int saveInterval ){
//        this.startTime = startTime;
//        this.calorieByActivity = calorie;
//        this.hmlStrengthTime = hmlTime;
//        this.metArray = metArray;
//        this.dataSaveInterval = saveInterval;
//
//        TimeInfo addTime = startTime.copyCurrentValue();
//
//        //METArray 만들기
//        this.metArray = new float[ DAY_SECOND / dataSaveInterval ];
//
//        for( int i = 0 ; i < metArray.length ;i++ ){
//
//            int hour = addTime.getHour();
//            int min = addTime.getMinute();
//            int sec = addTime.getSecond();
//
//            int index = ( (hour * 60 * 60) + (min * 60) + sec ) / dataSaveInterval ;
//
//            this.metArray[ index ] = metArray[ i ];
//
//            addTime.addSecond( dataSaveInterval );
//        }
//    }

    public boolean isAchieveMax() {
        return isAchieveMax;
    }

    public void setAchieveMax(boolean isAchieveMax) {
        this.isAchieveMax = isAchieveMax;
    }

    public int getPossibleMaxScore() {
        return possibleMaxScore;
    }

    public void setPossibleMaxScore(int possibleMaxScore) {
        this.possibleMaxScore = possibleMaxScore;
    }

    public int getPossibleTimes() {
        return possibleTimes;
    }

    public void setPossibleTimes(int possibleTimes) {
        this.possibleTimes = possibleTimes;
    }

    public int getPossibleValue() {
        return possibleValue;
    }

    public void setPossibleValue(int possibleValue) {
        this.possibleValue = possibleValue;
    }

    public int getExtraScore(){ return extraScore; }

    public void setExtraScore( int extraScore ){ this.extraScore = extraScore; }

    public int getTodayScore() {
        return todayScore;
    }

    public void setTodayScore(int todayScore) {
        this.todayScore = todayScore;
    }

    public int getPredayScore() {
        return predayScore;
    }

    public void setPredayScore(int predayScore) {
        this.predayScore = predayScore;
    }

    public void addFileIndex( int fileIndex ){
        this.fileIndexList.add( fileIndex );
    }

    public List<Integer> getFileIndexList(){
        return this.fileIndexList;
    }

    public DayActivity getLastDayActivity(){ return this.lastDayActivity; }

    public void setLastDayActivity( DayActivity dayActivity){
        this.lastDayActivity = dayActivity;
    }

    public int getLastFileIndex(){ return this.lastFileIndex; }

    public void setLastFileIndex( int fileIndex ){ this.lastFileIndex = fileIndex; }

    public String getActivityDate() { return this.startTime.getDateString(); }

    public int getCalorieByActivity() {
        return calorieByActivity;
    }

    public void setCalorieByActivity(int calorieByActivity) {
        this.calorieByActivity = calorieByActivity;
    }

    public int[] getHmlStrengthTime(){ return this.hmlStrengthTime; }

    public void setHmlStrengthTime(int[] hmlStrengthTime) {
        this.hmlStrengthTime = hmlStrengthTime;
    }

    public int getStrengthHigh() {
        return this.hmlStrengthTime[3];
    }

    public int getStrengthMedium() {
        return this.hmlStrengthTime[2];
    }

    public int getStrengthLow() {
        return this.hmlStrengthTime[1];
    }

    public int getStrengthUnderLow() {
        return this.hmlStrengthTime[0];
    }

    public int getDataSaveInterval() {
        return dataSaveInterval;
    }

    public void setDataSaveInterval(int dataSaveInterval) {
        this.dataSaveInterval = dataSaveInterval;
    }

    public TimeInfo getStartTime() {
        return startTime;
    }

    public void setStartTime(TimeInfo startTime) {
        this.startTime = startTime;
    }

    public float[] getMetArray() { return metArray; }

    public void setMetArray( float[] metArray ){
        this.metArray = metArray;
    }

    public float getAverageMET() {
        return averageMET;
    }
    public void setAverageMET(float averageMET) {
        this.averageMET = averageMET;
    }

    public int getAchieveOfTarget() {
        return achieveOfTarget;
    }
    public void setAchieveOfTarget(int achieveOfTarget) {
        this.achieveOfTarget = achieveOfTarget;
    }

    public int getProgramID() {
        return programID;
    }
    public void setProgramID(int programID) {
        this.programID = programID;
    }

    public float getFat(){ return this.fat; }
    public void setFat( float fat ){
        this.fat = fat;
    }

    public float getCarbohydrate(){ return carbohydrate; }
    public void setCarbohydrate( float carbohydrate ){ this.carbohydrate = carbohydrate; }

    public double getDistance(){ return distance; }
    public void setDistance( double distance ){ this.distance = distance; }

    public int getUploadState() {
        return uploadState;
    }

    public void setUploadState(int uploadState) {
        this.uploadState = uploadState;
    }

    public void add(DayActivity dayActivity ) {

        this.addFileIndex( dayActivity.getFileIndexList().get( 0 ) );
        this.calorieByActivity += dayActivity.getCalorieByActivity();

        int[] hmlTime = dayActivity.getHmlStrengthTime();
        for( int i = 0 ; i < this.hmlStrengthTime.length ; i++ ){
            hmlStrengthTime[i] += hmlTime[i];
        }

        float metSum =0;
        int metCount = 0;
        float[] dayMetArray = dayActivity.getMetArray();
        if( dayMetArray != null ) {
            for (int i = 0; i < this.metArray.length; i++) {
                this.metArray[i] += dayMetArray[i];
                float met = this.metArray[i];
                if(met >= 1){
                    metSum += met;
                    metCount += 1;
                }
            }
        }

        this.averageMET = metSum / metCount;

        if( achieveOfTarget < dayActivity.getAchieveOfTarget() ) { achieveOfTarget = dayActivity.getAchieveOfTarget(); }
    }

    public static byte[] convertFloatArrayToByteArray( float[] arrFloat ){
        byte[] arrTempByte = new byte[ arrFloat.length * 2 ];

        for( int i = 0 ; i < arrFloat.length ;i++ ){
            arrTempByte[2 * i] = (byte)((byte)( Math.floor( arrFloat[i] )) & 0xFF);
            arrTempByte[2 * i + 1 ] = (byte) ((byte) Math.round( ( arrFloat[i] - arrTempByte[ 2 * i ] ) * 100 ) & 0xFF);
        }

        return arrTempByte;
    }

    public static float[] convertByteArrayToFloatArray( byte[] arrByte ){
        float[] metFloatArray = new float[ arrByte.length / 2 ];

        for( int i = 0 ; i < metFloatArray.length ;i++ ){

            float met = (float) ( Math.round( arrByte[2 * i ] * 100d  +  arrByte[ 2 * i + 1] ) / 100d );
            metFloatArray[i] = met;
        }

        return metFloatArray;
    }





    public void overwrite(DayActivity dayActivity, int previousCalorie , int[] previousHMLTime ) {

        this.lastDayActivity = dayActivity.lastDayActivity;
        this.lastFileIndex = dayActivity.getLastFileIndex();
        this.calorieByActivity +=( dayActivity.getCalorieByActivity() - previousCalorie );

        int[] hmlTime = dayActivity.getHmlStrengthTime();
        for( int i = 0 ; i < this.hmlStrengthTime.length ; i++ ){
            hmlStrengthTime[i] += (hmlTime[i] - previousHMLTime[i]);
        }

        float metSum =0;
        int metCount = 0;
        float[] dayMetArray = dayActivity.getRawMetArray();

        int metArrayIndex = ((dayActivity.getStartTime().getHour() * 60 * 60 ) + ( dayActivity.getStartTime().getMinute() * 60 ) + dayActivity.getStartTime().getSecond()) / this.dataSaveInterval;

        if( dayMetArray != null ) {
            for( int i = 0 ; i < dayMetArray.length ;i++ ){

                this.metArray[ metArrayIndex + i ] = dayMetArray[i];
            }

            for (int i = 0; i < this.metArray.length; i++) {
                float met = this.metArray[i];
                if(met >= 1){
                    metSum += met;
                    metCount += 1;
                }
            }
        }

        this.averageMET = metSum / metCount;
        if( achieveOfTarget < dayActivity.getAchieveOfTarget() ) { achieveOfTarget = dayActivity.getAchieveOfTarget(); }
    }

    public void setRawMetArray(float[] rawMetArray) {
        this.metRawArray = rawMetArray;
    }

    public float[] getRawMetArray(){
        return this.metRawArray;
    }

    public JSONObject toJSONObject(  int exerprogramID  ) {

        JSONObject jsonObject = new JSONObject();

        try {

            jsonObject.put( ExerciseProgram.ID_KEY , exerprogramID );
            jsonObject.put( DAY_ACTIVITY_MET_ARRAY_KEY , Base64.encodeToString(convertFloatArrayToByteArray(this.getMetArray()), Base64.NO_WRAP) );
            jsonObject.put( DAY_ACTIVITY_AVERAGE_MET_KEY , this.getAverageMET()  );
            jsonObject.put( DAY_ACTIVITY_CALORIE_KEY , this.getCalorieByActivity()  );
            jsonObject.put( DAY_ACTIVITY_STRENGTH_HIGH_KEY , this.getStrengthHigh() );
            jsonObject.put( DAY_ACTIVITY_STRENGTH_MEDIUM_KEY , this.getStrengthMedium() );
            jsonObject.put( DAY_ACTIVITY_STRENGTH_LOW_KEY , this.getStrengthLow() );
            jsonObject.put( DAY_ACTIVITY_STRENGTH_UNDER_LOW_KEY , this.getStrengthUnderLow() );
            jsonObject.put( DAY_ACTIVITY_ACHIEVE_TARGET_KEY, this.getAchieveOfTarget() );
            jsonObject.put( DAY_ACTIVITY_DATASAVEINTERVAL_KEY , this.getDataSaveInterval());
            jsonObject.put( DAY_ACTIVITY_FAT_KEY , this.fat );
            jsonObject.put( DAY_ACTIVITY_CARBOHYDRATE_KEY , this.carbohydrate );
            jsonObject.put( DAY_ACTIVITY_DISTANCE_KEY , this.distance );
            //tick
            //jsonObject.put( DATE_KEY , this.getStartTime().getMilliSecond() );
            //yyyy-MM-dd
            jsonObject.put( DATE_KEY , this.getStartTime().getDateString() );

            jsonObject.put( DAY_ACTIVITY_TODAYSCORE_KEY , this.getTodayScore() );
            jsonObject.put( DAY_ACTIVITY_PREDAYSCORE_KEY , this.getPredayScore() );
            jsonObject.put( DAY_ACTIVITY_EXTRASCORE_KEY , this.getExtraScore() );
            jsonObject.put( DAY_ACTIVITY_ACHIEVEMAX_KEY , this.isAchieveMax() );
            jsonObject.put(DAY_ACTIVITY_POSSIBLEMAXSCORE_KEY , this.getPossibleMaxScore());
            jsonObject.put(DAY_ACTIVITY_POSSIBLETIMES_KEY , this.getPossibleTimes());
            jsonObject.put(DAY_ACTIVITY_POSSIBLEVALUE_KEY , this.getPossibleValue());

            jsonObject.put(DAY_ACTIVITY_FAT_KEY , this.getFat());
            jsonObject.put(DAY_ACTIVITY_CARBOHYDRATE_KEY , this.getCarbohydrate());
            jsonObject.put(DAY_ACTIVITY_DISTANCE_KEY , this.getDistance());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    public static DayActivity toDayActivity( JSONObject jsonObject ){

        DayActivity dayActivity = null;
        try {
            dayActivity = new DayActivity();

            //tick
           // dayActivity.setStartTime( new TimeInfo(jsonObject.getLong("date")) );
            //yyyy-mm-dd
            dayActivity.setStartTime( new TimeInfo(jsonObject.getString("date")));
            dayActivity.setCalorieByActivity(jsonObject.getInt("calorie"));
            dayActivity.setDataSaveInterval(jsonObject.getInt("interval"));
            dayActivity.setAverageMET((float) jsonObject.getDouble("averagemet"));

            int[] arrHMLTime = new int[4];
            arrHMLTime[0] = jsonObject.getInt("strengthunder");
            arrHMLTime[1] = jsonObject.getInt("strengthlow");
            arrHMLTime[2] = jsonObject.getInt("strengthmedium");
            arrHMLTime[3] = jsonObject.getInt("strengthhigh");
            dayActivity.setHmlStrengthTime(arrHMLTime);
            dayActivity.setAchieveOfTarget(jsonObject.getInt("achieve"));

            byte[] byteMetArray = Base64.decode(jsonObject.getString("metdataarray"), Base64.NO_WRAP);

            float[] metArray = null;
            if (byteMetArray != null) {
                metArray = DayActivity.convertByteArrayToFloatArray(byteMetArray);
            }
            dayActivity.setMetArray(metArray);
            Log.i("fitmate", "Day-toDay");
            dayActivity.setFat( Float.parseFloat(jsonObject.getString("fat") ) );
            dayActivity.setCarbohydrate(Float.parseFloat(jsonObject.getString("carbohydrate")));
            dayActivity.setDistance(jsonObject.getDouble("distance"));

        }catch(JSONException e){

        }

        return dayActivity;
    }

}
