package com.fitdotlife.fitmate_lib.object;

/**
 * Created by Joshua on 2015-09-25.
 */
public class NewsDayAchieve extends AddedContens{
    private String date = "";
    private int rangeFrom = 0;
    private int rangeTo = 0;
    private int rangeUnit = 0;
    private int exerciseProgramID = 0;
    private int achieveValue = 0;

    public int getAchieveValue() {
        return achieveValue;
    }

    public void setAchieveValue(int achieveValue) {
        this.achieveValue = achieveValue;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getRangeFrom() {
        return rangeFrom;
    }

    public void setRangeFrom(int rangeFrom) {
        this.rangeFrom = rangeFrom;
    }

    public int getRangeTo() {
        return rangeTo;
    }

    public void setRangeTo(int rangeTo) {
        this.rangeTo = rangeTo;
    }

    public int getRangeUnit() {
        return rangeUnit;
    }

    public void setRangeUnit(int rangeUnit) {
        this.rangeUnit = rangeUnit;
    }

    public int getExerciseProgramID() {
        return exerciseProgramID;
    }

    public void setExerciseProgramID(int exerciseProgramID) {
        this.exerciseProgramID = exerciseProgramID;
    }

}
