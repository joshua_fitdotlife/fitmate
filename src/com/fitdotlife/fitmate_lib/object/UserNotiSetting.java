package com.fitdotlife.fitmate_lib.object;


/**
 * Created by Joshua on 2015-08-26.
 */
public class UserNotiSetting {

    private boolean mytodayactivitynews = true;
    private boolean myweekactivitynews = true;
    private boolean batteryusagenoti = true;
    private boolean fitmeternotusednoti = true;
    private boolean fitmeterwearnoti = true;
    private String alarmtime = "08:00";
    private boolean friendnews = true;
    private boolean friendtodayactivitynews = true;
    private boolean friendweekactivitynews = true;

    public boolean isMytodayactivitynews() {
        return mytodayactivitynews;
    }

    public void setMytodayactivitynews(boolean mytodayactivitynews) {
        this.mytodayactivitynews = mytodayactivitynews;
    }

    public boolean isMyweekactivitynews() {
        return myweekactivitynews;
    }

    public void setMyweekactivitynews(boolean myweekactivitynews) {
        this.myweekactivitynews = myweekactivitynews;
    }

    public boolean isBatteryusagenoti() {
        return batteryusagenoti;
    }

    public void setBatteryusagenoti(boolean batteryusagenoti) {
        this.batteryusagenoti = batteryusagenoti;
    }

    public boolean isFitmeternotusednoti() {
        return fitmeternotusednoti;
    }

    public void setFitmeternotusednoti(boolean fitmeternotusednoti) {
        this.fitmeternotusednoti = fitmeternotusednoti;
    }

    public boolean isFitmeterwearnoti() {
        return fitmeterwearnoti;
    }

    public void setFitmeterwearnoti(boolean fitmeterwearnoti) {
        this.fitmeterwearnoti = fitmeterwearnoti;
    }

    public String getAlarmtime() {
        return alarmtime;
    }

    public void setAlarmtime(String alarmtime) {
        this.alarmtime = alarmtime;
    }

    public boolean isFriendnews() {
        return friendnews;
    }

    public void setFriendnews(boolean friendnews) {
        this.friendnews = friendnews;
    }

    public boolean isFriendtodayactivitynews() {
        return friendtodayactivitynews;
    }

    public void setFriendtodayactivitynews(boolean friendtodayactivitynews) {
        this.friendtodayactivitynews = friendtodayactivitynews;
    }

    public boolean isFriendweekactivitynews() {
        return friendweekactivitynews;
    }

    public void setFriendweekactivitynews(boolean friendweekactivitynews) {
        this.friendweekactivitynews = friendweekactivitynews;
    }

}
