package com.fitdotlife.fitmate_lib.object;

/**
 * Created by Joshua on 2015-12-10.
 */
public class CalData {

    int year;
    int month;
    int day;
    int dayofweek;

    public CalData(int y, int m,int d, int h) {
        year = y;
        month = m;
        day = d;
        dayofweek = h;
    }

    public int getDay() {
        return day;
    }

    public int getDayofweek() {
        return dayofweek;
    }

    public int getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }
}
