package com.fitdotlife.fitmate_lib.object;

/**
 * Created by Joshua on 2015-02-02.
 */
public class ResponseWeekActivity {

    public static final String WEEK_ACTIVITY_DATE_KEY = "activitydate";
    public static final String WEEK_ACTIVITY_SCORE_KEY = "score";
    public static final String WEEK_ACTIVITY_EXERCISE_TIME_LIST_KEY = "exercisetimelist";
    public static final String WEEK_ACTIVITY_AVERAGE_STRENGTH_HIGH_KEY = "averagestrengthhigh";
    public static final String WEEK_ACTIVITY_AVERAGE_STRENGTH_MEDIUM_KEY = "averagestrengthmedium";
    public static final String WEEK_ACTIVITY_AVERAGE_STRENGTH_LOW_KEY = "averagestrengthlow";
    public static final String WEEK_ACTIVITY_AVERAGE_MET_KEY = "averagemet";
    public static final String WEEK_ACTIVITY_CALORIE_LIST_KEY = "calorielist";

    private String activityDate = null;
    private int score = 0;
    private int[] exerciseTimeArray = null;
    private int[] calorieArray = null;
    private int averageStrengthHigh = 0;
    private int averageStrengthMedium = 0;
    private int averageStrengthLow = 0;
    private float averageMET = 0;

    public int[] getExerciseTimeArray() {
        return exerciseTimeArray;
    }

    public void setExerciseTimeArray(int[] exerciseTimeArray) {
        this.exerciseTimeArray = exerciseTimeArray;
    }

    public String getActivityDate() {
        return activityDate;
    }

    public void setActivityDate(String activityDate) {
        this.activityDate = activityDate;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getAverageStrengthHigh() {
        return averageStrengthHigh;
    }

    public void setAverageStrengthHigh(int averageStrengthHigh) {
        this.averageStrengthHigh = averageStrengthHigh;
    }

    public int getAverageStrengthMedium() {
        return averageStrengthMedium;
    }

    public void setAverageStrengthMedium(int averageStrengthMedium) {
        this.averageStrengthMedium = averageStrengthMedium;
    }

    public int getAverageStrengthLow() {
        return averageStrengthLow;
    }

    public void setAverageStrengthLow(int averageStrengthLow) {
        this.averageStrengthLow = averageStrengthLow;
    }

    public float getAverageMET() {
        return averageMET;
    }

    public void setAverageMET(float averageMET) {
        this.averageMET = averageMET;
    }

    public int[] getCalorieArray() {
        return calorieArray;
    }

    public void setCalorieArray(int[] calorieArray) {
        this.calorieArray = calorieArray;
    }
}
