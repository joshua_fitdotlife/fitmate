package com.fitdotlife.fitmate_lib.object;

import java.util.List;

/**
 * Created by Joshua on 2016-01-11.
 */
public class FriendWeekActivity {

    private int kindofdata;
    private List<DayAchieve> listofdayachieve;
    private String nodata;
    private DatesRangeInfo daterange;
    private TargetInfo targetrangeinfo;


    public DatesRangeInfo getDaterange() {
        return daterange;
    }

    public void setDaterange(DatesRangeInfo daterange) {
        this.daterange = daterange;
    }

    public int getKindofdata() {
        return kindofdata;
    }

    public void setKindofdata(int kindofdata) {
        this.kindofdata = kindofdata;
    }

    public List<DayAchieve> getListofdayachieve() {
        return listofdayachieve;
    }

    public void setListofdayachieve(List<DayAchieve> listofdayachieve) { this.listofdayachieve = listofdayachieve;}

    public String getNodata() {
        return nodata;
    }

    public void setNodata(String nodata) {
        this.nodata = nodata;
    }

    public TargetInfo getTargetrangeinfo() {
        return targetrangeinfo;
    }

    public void setTargetrangeinfo(TargetInfo targetrangeinfo) {

        this.targetrangeinfo = targetrangeinfo;
    }
}
