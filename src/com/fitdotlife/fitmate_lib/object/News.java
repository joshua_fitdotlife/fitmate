package com.fitdotlife.fitmate_lib.object;

import java.util.List;

/**
 * Created by Joshua on 2015-08-13.
 */
public class News {

    private int id = 0;
    private String ownerEmail = "";
    private String ownerProfileImagePath = "";
    private String ownerName = "";
    private long producedDate= 0;
    private int type = 0;
    private String imagePath = "";
    private String content = "";
    private List<NewsComment> comments;
    private AddedContens addedContents;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOwnerEmail() {
        return ownerEmail;
    }

    public void setOwnerEmail(String ownerEmail) {
        this.ownerEmail = ownerEmail;
    }

    public String getOwnerProfileImagePath() {
        return ownerProfileImagePath;
    }

    public void setOwnerProfileImagePath(String ownerProfileImagePath) {
        this.ownerProfileImagePath = ownerProfileImagePath;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public long getProducedDate() {
        return producedDate;
    }

    public void setProducedDate(long producedDate) {
        this.producedDate = producedDate;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<NewsComment> getComments() {
        return comments;
    }

    public void setComments(List<NewsComment> comments) {
        this.comments = comments;
    }

    public AddedContens getAddedContents() {
        return addedContents;
    }

    public void setAddedContents( AddedContens addedContents) {
        this.addedContents = addedContents;
    }

}
