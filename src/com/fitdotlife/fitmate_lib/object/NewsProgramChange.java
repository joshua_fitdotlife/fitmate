package com.fitdotlife.fitmate_lib.object;

/**
 * Created by Joshua on 2015-09-30.
 */
public class NewsProgramChange extends AddedContens{
    private long date;
    private int prevExerciseProgramID;
    private String prevExerciseProgramName;
    private int changedExerciseProgramID;
    private String changedExerciseProgramName;
    private String userEmail;
    private String userName;

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public int getPrevExerciseProgramID() {
        return prevExerciseProgramID;
    }

    public void setPrevExerciseProgramID(int prevExerciseProgramID) {
        this.prevExerciseProgramID = prevExerciseProgramID;
    }

    public String getPrevExerciseProgramName() {
        return prevExerciseProgramName;
    }

    public void setPrevExerciseProgramName(String prevExerciseProgramName) {
        this.prevExerciseProgramName = prevExerciseProgramName;
    }

    public int getChangedExerciseProgramID() {
        return changedExerciseProgramID;
    }

    public void setChangedExerciseProgramID(int changedExerciseProgramID) {
        this.changedExerciseProgramID = changedExerciseProgramID;
    }

    public String getChangedExerciseProgramName() {
        return changedExerciseProgramName;
    }

    public void setChangedExerciseProgramName(String changedExerciseProgramName) {
        this.changedExerciseProgramName = changedExerciseProgramName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
