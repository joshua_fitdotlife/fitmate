package com.fitdotlife.fitmate_lib.object;

/**
 * Created by Joshua on 2016-01-11.
 */
public class DatesRangeInfo {

    private String firstdate;
    private String lastdate;

    public String getFirstdate() {
        return firstdate;
    }

    public void setFirstdate(String firstdate) {
        this.firstdate = firstdate;
    }

    public String getLastdate() {
        return lastdate;
    }

    public void setLastdate(String lastdate) {
        this.lastdate = lastdate;
    }
}
