package com.fitdotlife.fitmate_lib.object;

/**
 * Created by Joshua on 2015-09-30.
 */
public class NewsUser {

    private String email;
    private String name;
    private String profileImagePath;
    private String nameSep;
    private String intro;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfileImagePath() {
        return profileImagePath;
    }

    public void setProfileImagePath(String profileImagePath) {
        this.profileImagePath = profileImagePath;
    }

    public String getNameSep() {
        return nameSep;
    }

    public void setNameSep(String nameSep) {
        this.nameSep = nameSep;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }
}
