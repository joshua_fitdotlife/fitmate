package com.fitdotlife.fitmate_lib.object;

/**
 * Created by Joshua on 2015-10-12.
 */
public class FriendAlarmSetting {

    private String friendEmail;
    private boolean sendMyNews;
    private boolean receiveFriendsNews;

    public String getFriendEmail() {
        return friendEmail;
    }

    public void setFriendEmail(String friendEmail) {
        this.friendEmail = friendEmail;
    }

    public boolean isSendMyNews() {
        return sendMyNews;
    }

    public void setSendMyNews(boolean sendMyNews) {
        this.sendMyNews = sendMyNews;
    }

    public boolean isReceiveFriendsNews() {
        return receiveFriendsNews;
    }

    public void setReceiveFriendsNews(boolean receiveFriendsNews) {
        this.receiveFriendsNews = receiveFriendsNews;
    }
}
