package com.fitdotlife.fitmate_lib.object;

import java.util.List;

/**
 * Created by Joshua on 2015-09-25.
 */
public class NewsWeekAchieve extends AddedContens {

    private String date = "";
    private int rangeFrom = 0;
    private int rangeTo = 0;
    private int rangeUnit = 0;
    private int exerciseProgramID = 0;
    private int weekScore = 0;
    private List<Integer> achieveValues;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getRangeFrom() {
        return rangeFrom;
    }

    public void setRangeFrom(int rangeFrom) {
        this.rangeFrom = rangeFrom;
    }

    public int getRangeTo() {
        return rangeTo;
    }

    public void setRangeTo(int rangeTo) {
        this.rangeTo = rangeTo;
    }

    public int getRangeUnit() {
        return rangeUnit;
    }

    public void setRangeUnit(int rangeUnit) {
        this.rangeUnit = rangeUnit;
    }

    public int getExerciseProgramID() {
        return exerciseProgramID;
    }

    public void setExerciseProgramID(int exerciseProgramID) {
        this.exerciseProgramID = exerciseProgramID;
    }

    public int getWeekScore() {
        return weekScore;
    }

    public void setWeekScore(int weekScore) {
        this.weekScore = weekScore;
    }

    public List<Integer> getAchieveValues() {
        return achieveValues;
    }

    public void setAchieveValues(List<Integer> achieveValues) {
        this.achieveValues = achieveValues;
    }
}