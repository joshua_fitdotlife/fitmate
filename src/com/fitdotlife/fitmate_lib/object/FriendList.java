package com.fitdotlife.fitmate_lib.object;

import java.util.List;

/**
 * Created by Joshua on 2015-08-05.
 */
public class FriendList {

    private List<UserFriend> recentList;
    private List<UserFriend> friendList;

    public List<UserFriend> getRecentList() {
        return recentList;
    }

    public void setRecentList(List<UserFriend> recentList) {
        this.recentList = recentList;
    }

    public List<UserFriend> getFriendList() {
        return friendList;
    }

    public void setFriendList(List<UserFriend> friendList) {
        this.friendList = friendList;
    }

}
