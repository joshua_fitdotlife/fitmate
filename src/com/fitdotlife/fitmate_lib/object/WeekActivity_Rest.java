package com.fitdotlife.fitmate_lib.object;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Joshua on 2015-09-01.
 */
public class WeekActivity_Rest {

    private int id;
    private String weekStartDate;
    private int userID;
    private int score;
    private int aveCalorie;
    private float aveMET;
    private int aveStrengthHigh;
    private int aveStrengthMedium;
    private int aveStrengthLow;
    private int aveStrengthUnderLow;
    private int exerciseProgramID;
    @JsonProperty("date")
    public String getWeekStartDate() {
        return weekStartDate;
    }
    @JsonProperty("date")
    public void setWeekStartDate(String weekStartDate) {
        this.weekStartDate = weekStartDate;
    }
    @JsonProperty("achieve")
    public int getScore() {
        return score;
    }
    @JsonProperty("achieve")
    public void setScore(int score) {
        this.score = score;
    }
    @JsonProperty("calorie")
    public int getAveCalorie() {
        return aveCalorie;
    }
    @JsonProperty("calorie")
    public void setAveCalorie(int aveCalorie) {
        this.aveCalorie = aveCalorie;
    }
    @JsonProperty("averagemet")
    public float getAveMET() {
        return aveMET;
    }
    @JsonProperty("averagemet")
    public void setAveMET(float aveMET) {
        this.aveMET = aveMET;
    }
    @JsonProperty("strengthhigh")
    public int getAveStrengthHigh() {
        return aveStrengthHigh;
    }
    @JsonProperty("strengthhigh")
    public void setAveStrengthHigh(int aveStrengthHigh) {
        this.aveStrengthHigh = aveStrengthHigh;
    }
    @JsonProperty("strengthmedium")
    public int getAveStrengthMedium() {
        return aveStrengthMedium;
    }
    @JsonProperty("strengthmedium")
    public void setAveStrengthMedium(int aveStrengthMedium) {
        this.aveStrengthMedium = aveStrengthMedium;
    }
    @JsonProperty("strengthlow")
    public int getAveStrengthLow() {
        return aveStrengthLow;
    }
    @JsonProperty("strengthlow")
    public void setAveStrengthLow(int aveStrengthLow) {
        this.aveStrengthLow = aveStrengthLow;
    }
    @JsonProperty("strengthunder")
    public int getAveStrengthUnderLow() {
        return aveStrengthUnderLow;
    }
    @JsonProperty("strengthunder")
    public void setAveStrengthUnderLow(int aveStrengthUnderLow) {
        this.aveStrengthUnderLow = aveStrengthUnderLow;
    }
    @JsonProperty("exprogramid")
    public int getExerciseProgramID() {
        return exerciseProgramID;
    }
    @JsonProperty("exprogramid")
    public void setExerciseProgramID(int exerciseProgramID) {
        this.exerciseProgramID = exerciseProgramID;
    }
}
