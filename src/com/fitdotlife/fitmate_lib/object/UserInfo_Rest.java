package com.fitdotlife.fitmate_lib.object;

/**
 * Created by Joshua on 2015-10-08.
 */
public class UserInfo_Rest
{
    private String email;
    private String name;
    private String birthdate;
    private boolean sex;
    private float height;
    private float weight;
    private String introyourself;
    private String profileimagepath;
    private int exerciseprogramid;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public boolean isSex() {
        return sex;
    }

    public void setSex(boolean sex) {
        this.sex = sex;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public String getIntroyourself() {
        return introyourself;
    }

    public void setIntroyourself(String introyourself) {
        this.introyourself = introyourself;
    }

    public String getProfileimagepath() {
        return profileimagepath;
    }

    public void setProfileimagepath(String profileimagepath) {
        this.profileimagepath = profileimagepath;
    }

    public int getExerciseprogramid() {
        return exerciseprogramid;
    }

    public void setExerciseprogramid(int exerciseprogramid) {
        this.exerciseprogramid = exerciseprogramid;
    }
}
