package com.fitdotlife.fitmate_lib.object;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.provider.ContactsContract;
import android.util.Log;

import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ContinuousCheckPolicy;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.USERVO2MAXLEVEL;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.UserInfoForAnalyzer;
import com.fitdotlife.fitmate_lib.key.GenderType;
import com.fitdotlife.fitmate_lib.key.VO2LevelType;
import com.fitdotlife.fitmate_lib.util.DateUtils;
import com.fitdotlife.fitmate_lib.util.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.WeakHashMap;

/**
 * Created by Joshua on 2015-01-29.
 */
public class UserInfo {

    public static final String PHOTO_URL = "photourl";
    public static final String ALARMTIME_KEY = "alarmtime";
    public static final String LASTALARMSET_KEY = "lastalarmset";
    public static final String ALARMSET_KEY = "alarmset";
    public static final String USERINFO_KEY = "userinfo";
    public static final String REGISTERED_KEY = "registered";
    public static final String AUTO_LOGIN_KEY = "autologin";

    public static final String NAME_KEY = "name";
    public static final String EMAIL_KEY ="email";
    public static final String PHONENUM_KEY = "phoneNum";
    public static final String BIRTHDATEKEY = "birthdate";
    public static final String SIKEY = "si";
    public static final String PROFILE_MAGE_KEY = "profileimg";
    public static final String HELLO_MESSAGE_KEY = "hellomessage";
    public static final String PASSWORD_KEY = "pw";
    public static final String ISVO2MAX_KEY = "isvo2max";
    public static final String SEX_KEY = "sex";
    public static final String AGE_KEY = "age";
    public static final String HEIGHT_KEY = "height";
    public static final String WEIGHT_KEY = "weight";
    public static final String DEVICE_ADDRESS_KEY = "deviceaddress";
    public static final String MAXMET_KEY = "maxmet";
    public static final String BTADDRESS_KEY = "btaddress";
    public static final String WEARAT_KEY = "wearat";
    public static final String ISINCENTIVEUSER_KEY = "isincentiveuser";
    public static final String LASTDATADATE_KEY="lastdate";
    public static final String WEARING_LOCATION_KEY = "wearinglocation";
    public static final String EXERCISE_PROGRAM_ID_KEY = "exprogramid";

    private String name;
    private String email;
    private String password;
    private String accountPhoto;
    private String intro;
    private GenderType gender;
    private float height;
    private float weight;
    private long birthDate;
    private String deviceAddress;
    private boolean isSI;
    private boolean isVO2Max;
    private double maxMet = 0;
    private int wearAt;
    private boolean isIncentiveUser;
    private long lastDataDate = 0;
    private int exerprogramId = 0;

    public VO2LevelType getVo2Level() { return vo2Level; }
    public void setVo2Level(VO2LevelType vo2Level) { this.vo2Level = vo2Level; }
    private VO2LevelType vo2Level;
    public String getName() { return name; }
    public void setName(String name) { this.name = name; }
    public String getEmail() { return email; }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getAccountPhoto() {
        return accountPhoto;
    }
    public void setAccountPhoto(String password) {
        this.accountPhoto = password;
    }


    public String getIntro() {
        return intro;
    }
    public void setIntro(String intro) {
        this.intro = intro;
    }
    public GenderType getGender() { return gender; }
    public float getHeight() { return height; }
    public float getWeight() { return weight;}
    public String getDeviceAddress() { return deviceAddress;}
    public void setGender(GenderType gender) { this.gender = gender; }
    public void setHeight(float height) { this.height = height; }
    public void setWeight(float weight) { this.weight = weight; }
    public void setDeviceAddress(String deviceAddress) { this.deviceAddress = deviceAddress; }
    public long getBirthDate() { return birthDate; }
    public void setBirthDate(long birthDate) { this.birthDate = birthDate;}
    public boolean isSI() { return isSI; }
    public void setIsSI(boolean isSI) { this.isSI = isSI;}
    public boolean isVO2Max() { return isVO2Max; }
    public void setIsVO2Max(boolean isVO2Max) { this.isVO2Max = isVO2Max; }
    public double getMaxMet() { return maxMet; }
    public int getWearAt(  ) { return this.wearAt; }
    public void setWearAt( int wearAt ){ this.wearAt = wearAt; }
    public void setMaxMet(double maxMet) {this.maxMet = maxMet; }
    public boolean getIncentiveUser(){ return this.isIncentiveUser; }
    public void setIncentiveUser( boolean isIncentiveUser ){  this.isIncentiveUser = isIncentiveUser; }
    public long getLastDataDate(){ return this.lastDataDate; }
    public void setLastDataDate( long lastDataDate ) {this.lastDataDate = lastDataDate; }
    public int getExerprogramId() {
        return exerprogramId;
    }

    public void setExerprogramId(int exerprogramId) {
        this.exerprogramId = exerprogramId;
    }

    public WeakHashMap<String, String> toParameters( int exprogramId ) {
        WeakHashMap<String , String> parameters = new WeakHashMap<String, String>();

        parameters.put(NAME_KEY, name);
        parameters.put(EMAIL_KEY, email);
        parameters.put(BIRTHDATEKEY, DateUtils.convertUTCTicktoDateString( birthDate ) );
        parameters.put(SEX_KEY, gender.getBooleanString() );
        parameters.put(HEIGHT_KEY, String.valueOf( height ));
        parameters.put(WEIGHT_KEY, String.valueOf( weight ));
        ContinuousCheckPolicy policy = ContinuousCheckPolicy.Loosely_SumOverXExercise;
        UserInfoForAnalyzer userInfoForAnalyzer = new UserInfoForAnalyzer(com.fitdotlife.fitmate_lib.service.util.Utils.calculateAge(birthDate) , gender.getBoolean() , (int)height , (int)weight , policy , 10 , USERVO2MAXLEVEL.Fair );
        if(isSI()){
            parameters.put(SIKEY, "true" );
        }else{
            parameters.put(SIKEY, "false");
            int h =(int) com.fitdotlife.fitmate_lib.service.util.Utils.ft_to_cm(height);
            int w =(int) com.fitdotlife.fitmate_lib.service.util.Utils.lb_to_kg(weight);
            userInfoForAnalyzer = new UserInfoForAnalyzer(com.fitdotlife.fitmate_lib.service.util.Utils.calculateAge(birthDate) , gender.getBoolean() , h , w , policy , 10 , USERVO2MAXLEVEL.Fair );
        }

        parameters.put(HELLO_MESSAGE_KEY, intro );
        parameters.put(PASSWORD_KEY, password);
        parameters.put(ISVO2MAX_KEY, "true");

        parameters.put(MAXMET_KEY, userInfoForAnalyzer.getVo2max() + "" );
        parameters.put(BTADDRESS_KEY , deviceAddress);
        parameters.put(ISINCENTIVEUSER_KEY, Utils.convertToString(isIncentiveUser));
        parameters.put( WEARING_LOCATION_KEY ,  wearAt + "");
        parameters.put(EXERCISE_PROGRAM_ID_KEY , exprogramId + "");

        return parameters;
    }

    public WeakHashMap<String,String> toAccountParameters( int exprogramId ){
        WeakHashMap<String , String> parameters = new WeakHashMap<String, String>();

        parameters.put(NAME_KEY, name);
        parameters.put(EMAIL_KEY, email);
        parameters.put(HELLO_MESSAGE_KEY, intro );
        parameters.put(PASSWORD_KEY, password);
        parameters.put(EXERCISE_PROGRAM_ID_KEY , exprogramId + "");
        parameters.put( PROFILE_MAGE_KEY , accountPhoto );
        return parameters;
    }

    public static UserInfo parseJSONObject( JSONObject jsonObject){
        UserInfo userInfo = null;
        try {

            userInfo = new UserInfo();
            userInfo.setName(jsonObject.getString(NAME_KEY));
            userInfo.setBirthDate( DateUtils.convertDateStringtoUTCTick(jsonObject.getString(BIRTHDATEKEY)));
            userInfo.setWeight((float) jsonObject.getDouble(WEIGHT_KEY));
            userInfo.setHeight((float) jsonObject.getDouble(HEIGHT_KEY));
            userInfo.setGender(GenderType.getGenderType(jsonObject.getBoolean(SEX_KEY) ? 1 : 0));
            userInfo.setIsSI(jsonObject.getBoolean(SIKEY));
            userInfo.setIntro(jsonObject.getString(HELLO_MESSAGE_KEY));
            userInfo.setMaxMet(jsonObject.getDouble(MAXMET_KEY));
            userInfo.setIsVO2Max(jsonObject.getBoolean(ISVO2MAX_KEY));
            userInfo.setDeviceAddress(jsonObject.getString(BTADDRESS_KEY));
            userInfo.setIncentiveUser(Utils.convertToBoolean(jsonObject.getString(ISINCENTIVEUSER_KEY)));
            userInfo.setLastDataDate(jsonObject.getLong(LASTDATADATE_KEY));
            userInfo.setWearAt(jsonObject.getInt(WEARING_LOCATION_KEY));
            userInfo.setExerprogramId(jsonObject.getInt(EXERCISE_PROGRAM_ID_KEY));
            userInfo.setAccountPhoto( jsonObject.getString( PROFILE_MAGE_KEY ) );

        }catch(JSONException e){
           Log.e("fitmate" , e.getMessage() );
        }

        return userInfo;
    }


    public static boolean getRegisterInSharedPreference( Context context ){
        SharedPreferences userInfo = context.getSharedPreferences(UserInfo.USERINFO_KEY, Activity.MODE_PRIVATE);
        return userInfo.getBoolean(UserInfo.REGISTERED_KEY, false);
    }

    public static String getEmailInSharedPreference( Context context ){
        SharedPreferences userInfo = context.getSharedPreferences(UserInfo.USERINFO_KEY, Activity.MODE_PRIVATE);
        return userInfo.getString(UserInfo.EMAIL_KEY, null);
    }

    public static String getPasswordSharedPreference( Context context ){
        SharedPreferences userInfo = context.getSharedPreferences(UserInfo.PASSWORD_KEY, Activity.MODE_PRIVATE);
        return userInfo.getString(UserInfo.PASSWORD_KEY, null);
    }

    public static void saveRegisterInSharedPreference( Context context ){
        SharedPreferences userInfo = context.getSharedPreferences(UserInfo.USERINFO_KEY, Activity.MODE_PRIVATE);

        SharedPreferences.Editor editor = userInfo.edit();
        editor.putBoolean(UserInfo.REGISTERED_KEY, true);
        editor.commit();
    }

    public static String getTimeFromCalendar(Calendar c)
    {
        return String.format("%02d%02d", c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE));
    }

    public static Calendar getCalendarFromString(String s)
    {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HHmm", Locale.US);
        try {
            cal.setTime(sdf.parse(s));
        } catch (ParseException e) {
            e.printStackTrace();
            cal = null;
        }

        return cal;
    }


    public static String getPhotoUrlSharedPreference( Context context ){
        SharedPreferences userInfo = context.getSharedPreferences(UserInfo.PHOTO_URL, Activity.MODE_MULTI_PROCESS);
        String time = userInfo.getString(UserInfo.PHOTO_URL, null);

        return time;
    }

    public static void savePhotoUrlInSharedPreference( Context context, String time ){
        SharedPreferences userInfo = context.getSharedPreferences(UserInfo.PHOTO_URL, Activity.MODE_MULTI_PROCESS);

        SharedPreferences.Editor editor = userInfo.edit();
        editor.putString(UserInfo.PHOTO_URL, time);
        editor.commit();
    }

    public static String getAlarmTimeSharedPreference( Context context ){
        SharedPreferences userInfo = context.getSharedPreferences(UserInfo.ALARMTIME_KEY, Activity.MODE_MULTI_PROCESS);
        String time = userInfo.getString(UserInfo.ALARMTIME_KEY, "0800");

        if(time == null)
        {
            time = "0800";
            saveAlarmTimeInSharedPreference(context, time);
        }

        return time;
    }

    public static void saveAlarmTimeInSharedPreference( Context context, String time ){
        SharedPreferences userInfo = context.getSharedPreferences(UserInfo.ALARMTIME_KEY, Activity.MODE_MULTI_PROCESS);

        SharedPreferences.Editor editor = userInfo.edit();
        editor.putString(UserInfo.ALARMTIME_KEY, time);
        editor.commit();
    }

    public static String getDateTimeFromCalendar(Calendar c)
    {
        return String.format("%04d%02d%02d %02d%02d%02d", c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1, c.get(Calendar.DAY_OF_MONTH), c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), c.get(Calendar.SECOND));
    }

    public static Calendar getCalendarFromDateTimeString(String s)
    {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd HHmmss", Locale.getDefault());
        try {
            cal.setTime(sdf.parse(s));
        } catch (ParseException e) {
            e.printStackTrace();
            cal = null;
        }

        return cal;
    }



    public static boolean getAlarmSetSharedPreference( Context context ){
        SharedPreferences userInfo = context.getSharedPreferences(UserInfo.ALARMSET_KEY, Activity.MODE_MULTI_PROCESS );
        return userInfo.getBoolean(UserInfo.ALARMSET_KEY, true);
    }

    public static void saveAlarmSetInSharedPreference( Context context, boolean isSet){
        SharedPreferences userInfo = context.getSharedPreferences(UserInfo.ALARMSET_KEY, Activity.MODE_MULTI_PROCESS);

        SharedPreferences.Editor editor = userInfo.edit();
        editor.putBoolean(UserInfo.ALARMSET_KEY, isSet);
        editor.commit();
    }

    public static boolean getLastAlarmSharedPreference( Context context ){
        SharedPreferences userInfo = context.getSharedPreferences(UserInfo.LASTALARMSET_KEY, Activity.MODE_MULTI_PROCESS );
        return userInfo.getBoolean(UserInfo.LASTALARMSET_KEY, false);
    }

    public static void saveLastAlarmInSharedPreference( Context context, boolean isSet){
        SharedPreferences userInfo = context.getSharedPreferences(UserInfo.LASTALARMSET_KEY, Activity.MODE_MULTI_PROCESS );

        SharedPreferences.Editor editor = userInfo.edit();
        editor.putBoolean(UserInfo.LASTALARMSET_KEY, isSet);
        editor.commit();
    }

    public static void saveEmailInSharedPreference( Context context , String email ){
        SharedPreferences userInfo = context.getSharedPreferences(UserInfo.USERINFO_KEY, Activity.MODE_PRIVATE);

        SharedPreferences.Editor editor = userInfo.edit();
        editor.putString(UserInfo.EMAIL_KEY, email);
        editor.commit();
    }

    public static void savePasswordSharedPreference( Context context , String password){
        SharedPreferences userInfo = context.getSharedPreferences(UserInfo.USERINFO_KEY, Activity.MODE_PRIVATE);

        SharedPreferences.Editor editor = userInfo.edit();
        editor.putString(UserInfo.PASSWORD_KEY, password);
        editor.commit();
    }

    public static void saveRegisterComplete( Context context )
    {
    	SharedPreferences userInfo = context.getSharedPreferences(USERINFO_KEY, Activity.MODE_PRIVATE);

    	SharedPreferences.Editor editor = userInfo.edit();
    	editor.putBoolean(REGISTERED_KEY, true);
    	editor.commit();
    }


    public static String getWeightUnit( boolean isSI ){
        if(isSI){
            return "kg";
        }else{
            return "lb";
        }
    }

    public static String getHeightUnit( boolean isSI ){
        if(isSI){
            return "cm";
        }else{
            return "ft";
        }
    }
}
