package com.fitdotlife.fitmate_lib.object;

/**
 * Created by fitlife.soondong on 2015-06-05.
 */
public class ScoreClass {
    /**
     * 어제까지 주간 점수
     */
    public int preDayAchieve;
    /**
     * 오늘까지 주간점수
     */
    public int weekAchieve;
    /**
     * 오늘 얻은 점수
     */
    public int todayGetPoint;

    /**
     * 오늘 운동을 더하면 추가로 얻을 수 있는 점수
     */
    public int todayPossibleMorePoint;

    /**
     * 주간 목표를 달성 했는지
     */
    public boolean isAchieveMax;
    /**
     * 이번주 달성 가능한 최고점은 몇점인지
     */
    public int possibleMaxScore;
    /**
     * 며칠을 더 운동해야 하는지
     */
    public int possibleTimes;
    /**
     * 몇 칼로리(혹은 몇분)을 매일 해야하는지
     */
    public int possibleValue;


}
