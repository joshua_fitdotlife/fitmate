package com.fitdotlife.fitmate_lib.object;

/**
 * Created by Joshua on 2015-09-30.
 */
public class NewsFriend extends AddedContens{

    private long date;
    private NewsUser requesterInfo;
    private NewsUser responderInfo;
    private int status;

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public NewsUser getRequesterInfo() {
        return requesterInfo;
    }

    public void setRequesterInfo(NewsUser requesterInfo) {
        this.requesterInfo = requesterInfo;
    }

    public NewsUser getResponderInfo() {
        return responderInfo;
    }

    public void setResponderInfo(NewsUser responderInfo) {
        this.responderInfo = responderInfo;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
