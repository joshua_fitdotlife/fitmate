package com.fitdotlife.fitmate_lib.object;

/**
 * Created by Joshua on 2015-02-02.
 */
public class ResponseMonthActivity {

    public static final String MONTH_ACTIVITY_ACTIVITY_DATE_KEY = "activitydate";
    public static final String MONTH_ACTIVITY_SCORE_LIST_KEY = "scorelist";
    public static final String MONTH_ACTIVITY_CALORIE_LIST_KEY = "calorielist";
    public static final String MONTH_ACTIVITY_AVERAGE_SCORE_KEY = "averagescore";
    //public static final String MONTH_ACTIVITY_MONTH_LIST_KEY = "monthlist";

    private String activityDate = null;
    private int[] scoreArray = null;
    private int[] calorieArray = null;
    //private String[] monthArray = null;
	private int averageScore = 0;
    
//    public String[] getMonthArray() {
//		return monthArray;
//	}

//    public void setMonthArray(String[] monthArray) {
//		this.monthArray = monthArray;
//	}

    public String getActivityDate() {
        return activityDate;
    }

    public void setActivityDate(String activityDate) {
        this.activityDate = activityDate;
    }

    public int[] getScoreArray() {
        return scoreArray;
    }

    public void setScoreArray(int[] scoreArray) {
        this.scoreArray = scoreArray;
    }

    public int[] getCalorieArray() {
        return calorieArray;
    }

    public void setCalorieArray(int[] calorieArray) {
        this.calorieArray = calorieArray;
    }

    public int getAverageScore() {
        return averageScore;
    }

    public void setAverageScore(int averageScore) {
        this.averageScore = averageScore;
    }
}
