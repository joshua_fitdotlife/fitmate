
package com.fitdotlife.fitmate_lib.object;

        import android.os.Bundle;

        import java.util.WeakHashMap;

/**
 * Created by Joshua on 2015-02-10.
 */
public class ExerciseProgramOld {

    public static final String ID_KEY = "exerciseprogramid";
    public static final String NAME_KEY = "name";
    public static final String CATEGORY_KEY = "category";
    public static final String STRENGTH_FROM_KEY = "strengthfrom";
    public static final String STRENGTH_TO_KEY = "strengthto";
    public static final String MINUTE_FROM_KEY = "minutefrom";
    public static final String MINUTE_TO_KEY = "minuteto";
    public static final String TIMES_FROM_KEY = "timesfrom";
    public static final String TIMES_TO_KEY = "timesto";
    public static final String CONTINUOUS_EXERCISE_KEY = "continuousexercise";
    public static final String OWNER_KEY = "owner";
    public static final String INFO_KEY = "info";
    public static final String RECOMMEND_NUMBER_KEY = "recommendnumber";
    public static final String PUBLISH_TYPE_KEY = "publishtype";

    public static final String NUMBER_OF_PROGRAMS_KEY = "numberofprograms";
    public static final String CONTAIN_KEY = "contain";
    public static final String PROGRAMLIST_KEY = "programlist";

    private int id;
    private String name;
    private int category;
    private int minuteFrom;
    private int minuteTo;
    private float strengthFrom;
    private float strengthTo;
    private int timesFrom;
    private int timesTo;
    private int recommendNumber;
    private String info;
    private int continuousExercise;
    private String owner;
    private int publishType;

    private boolean applied = false;

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMinuteFrom() {
        return minuteFrom;
    }

    public void setMinuteFrom(int minuteFrom) {
        this.minuteFrom = minuteFrom;
    }

    public int getMinuteTo() {
        return minuteTo;
    }

    public void setMinuteTo(int minuteTo) {
        this.minuteTo = minuteTo;
    }

    public float getStrengthFrom() {
        return strengthFrom;
    }

    public void setStrengthFrom(float strengthFrom) {
        this.strengthFrom = strengthFrom;
    }

    public float getStrengthTo() {
        return strengthTo;
    }

    public void setStrengthTo(float strengthTo) {
        this.strengthTo = strengthTo;
    }

    public int getTimesFrom() {
        return timesFrom;
    }

    public void setTimesFrom(int timesFrom) {
        this.timesFrom = timesFrom;
    }

    public int getTimesTo() {
        return timesTo;
    }

    public void setTimesTo(int timesTo) {
        this.timesTo = timesTo;
    }

    public int getRecommendNumber() {
        return recommendNumber;
    }

    public void setRecommendNumber(int recommendNumber) {
        this.recommendNumber = recommendNumber;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public int getContinuousExercise() {
        return continuousExercise;
    }

    public void setContinuousExercise(int continuousExercise) {
        this.continuousExercise = continuousExercise;
    }

    public boolean isApplied() {
        return applied;
    }

    public void setApplied(boolean applied) {
        this.applied = applied;
    }

    public Bundle toBundle(){

        Bundle exerciseProgramBundle = new Bundle();
        exerciseProgramBundle.putString(this.NAME_KEY , this.name);
        exerciseProgramBundle.putInt(this.CATEGORY_KEY, this.category);
        exerciseProgramBundle.putFloat(this.STRENGTH_FROM_KEY , this.strengthFrom);
        exerciseProgramBundle.putFloat(this.STRENGTH_TO_KEY , this.strengthTo);
        exerciseProgramBundle.putInt(this.TIMES_FROM_KEY, this.timesFrom);
        exerciseProgramBundle.putInt(this.TIMES_TO_KEY , this.timesTo);
        exerciseProgramBundle.putInt(this.MINUTE_FROM_KEY , this.minuteFrom);
        exerciseProgramBundle.putInt(this.MINUTE_TO_KEY , this.minuteTo);
        exerciseProgramBundle.putInt(this.CONTINUOUS_EXERCISE_KEY , this.continuousExercise);

        return exerciseProgramBundle;
    }


    public WeakHashMap<String, String> toParameters() {
        WeakHashMap<String , String> parameters = new WeakHashMap<String, String>();

        return parameters;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public int getPublishType() {
        return publishType;
    }

    public void setPublishType(int publishType) {
        this.publishType = publishType;
    }


}
