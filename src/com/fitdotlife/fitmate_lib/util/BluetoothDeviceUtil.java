package com.fitdotlife.fitmate_lib.util;

import android.bluetooth.BluetoothDevice;
import android.util.Log;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

/**
 * Created by wooseok on 2015-06-23.
 */
public class BluetoothDeviceUtil {
    public String toStringValue;
    public String Address;
    public int RssiSum = 0;
    public int RssiMin = -999;
    public int Cnt;
    public BluetoothDevice bd = null;

    static private Hashtable<String, BluetoothDeviceUtil> mMyDeviceList = new Hashtable<String,BluetoothDeviceUtil>();

    public static void clearList()
    {
        mMyDeviceList.clear();
    }

    public static boolean checkList(BluetoothDevice device, int rssi)
    {
        boolean isCheck = false;

        Log.v("BLE FIND", String.format("%s %d", device.getAddress(), rssi));
        if (!mMyDeviceList.containsKey(device.getAddress())) {
            BluetoothDeviceUtil md = new BluetoothDeviceUtil();
            md.Address = device.getAddress();
            md.bd = device;
            md.Add(rssi);
            mMyDeviceList.put(device.getAddress(), md);
            md.toStringValue = md.toString();
            isCheck = true;
        }
        else
        {
            BluetoothDeviceUtil md = mMyDeviceList.get(device.getAddress());
            md.Add(rssi);
            isCheck = false;
        }

        return isCheck;
    }

    public static BluetoothDevice getBluetoothDevice(String id)
    {
        if (!mMyDeviceList.containsKey(id)) {
            return null;
        }
        else
        {
            BluetoothDeviceUtil md = mMyDeviceList.get(id);
            return md.bd;
        }
    }

    public static BluetoothDeviceUtil [] find(int cnt, int rssi)
    {
        BluetoothDeviceUtil [] bts = null;

        ArrayList<BluetoothDeviceUtil> al = new ArrayList<BluetoothDeviceUtil>();

        Enumeration ee = mMyDeviceList.elements();

        while(ee.hasMoreElements())
        {
            BluetoothDeviceUtil bt = (BluetoothDeviceUtil) ee.nextElement();
            Log.v("BLE FIND", bt.toString());

            if(bt.Cnt > cnt && bt.RssiMin > rssi)
            {
                al.add(bt);
            }
        }

        return al.toArray(new BluetoothDeviceUtil[al.size()]);
    }

    public static int findCount(int cnt, int rssi)
    {
        BluetoothDeviceUtil [] bt = find(cnt, rssi);
        int cntcnt = 0;

        if(bt != null)
        {
            cntcnt = bt.length;
        }

        return cntcnt;
    }

    public static String firstDeviceAddress(int cnt, int rssi)
    {
        BluetoothDeviceUtil [] bt = find(cnt, rssi);
        String cntcnt = null;

        if(bt != null && bt.length == 1)
        {
            cntcnt = bt[0].Address;
        }

        return cntcnt;
    }

    public static String[] firstDeviceAddressStrings(int cnt, int rssi)
    {
        BluetoothDeviceUtil [] bt = find(cnt, rssi);
        String cntcnt[] = null;

        ArrayList<String> lCnt = new ArrayList<String>();

        if(bt != null)
        {
            for(BluetoothDeviceUtil b : bt)
            {
                lCnt.add(b.Address);
            }
        }

        if(lCnt.size() != 0)
        {
            cntcnt = lCnt.toArray(new String[lCnt.size()]);
        }

        return cntcnt;
    }




    public void Add(int rssi)
    {
        RssiSum += rssi;
        Cnt ++;

        if(RssiMin < rssi)
        {
            RssiMin = rssi;
        }
    }

    public int GetAvg()
    {
        return RssiSum / Cnt;
    }

    @Override
    public String toString() {
        return "[" + Address + "]" +
                " " + GetAvg() +
                " " + Cnt +
                " " + RssiMin +
                " " + RssiSum;
    }

    public BluetoothDeviceUtil()
    {
        RssiSum = 0;
        Cnt = 0;
    }
}