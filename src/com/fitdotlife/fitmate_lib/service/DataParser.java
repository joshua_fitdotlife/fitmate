package com.fitdotlife.fitmate_lib.service;//package com.fitdotlife.fitmateservice_beta;
//
//import android.os.Messenger;
//import android.util.Log;
//
//import com.fitdotlife.exerciseanalysis.ExerciseAnalyzer;
//import com.fitdotlife.fitmateservice_beta.database.FitmateServiceDBManager;
//import com.fitdotlife.fitmateservice_beta.object.NextDayListener;
//import com.fitdotlife.fitmateservice_beta.object.TimeInfo;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Queue;
//
///**
// * Created by Joshua on 2015-01-28.
// */
//public class DataParser extends Thread implements NextDayListener
//{
//    private String TAG = "fitmateservice";
//    private final int METARRAY_NUMBER = 86400;
//    private final int GRAVITY = 8;
//
//    private FitmateServiceDBManager mDBManager = null;
//    private boolean isRunning = false;
//    private Queue<Integer> mDataQueue = null;
//    private ExerciseAnalyzer mAnalyzer = null;
//
//    private long mStartTimeMilliSecond = 0;
//    private TimeInfo mCurrentTime = null;
//    private List<Float> mMetArrayList = null;
//    private int mCalorie = 0;
//    private Messenger mMessenger = null;
//
//    public DataParser(FitmateServiceDBManager dbManager, Queue<Integer> dataQueue, ExerciseAnalyzer analyzer , long startTimeMilliSecond ){
//        this.mDBManager = dbManager;
//        this.mDataQueue = dataQueue;
//        this.mAnalyzer = analyzer;
//        this.mMetArrayList = new ArrayList<Float>();
//
//        this.mStartTimeMilliSecond = startTimeMilliSecond;
//        this.mCurrentTime = new TimeInfo( this.mStartTimeMilliSecond );
//
//        this.isRunning = true;
//        this.start();
//
//    }
//
//    public void run()
//    {
//        while( isRunning )
//        {
//            if( !mDataQueue.isEmpty() )
//            {
//                int activityValue = this.mDataQueue.poll();
//
//                //MET와 칼로리를 계산한다.
//                float met = 0;
//                int calorie = 0;
//
//                activityValue = this.devideBy32ForInt(this.getTransValueForInt( activityValue , this.GRAVITY ));
//                calorie = this.mAnalyzer.Calcuate_Calorie(activityValue);
//                met = this.mAnalyzer.Calculate_METbyCaloire(calorie);
//
//                Log.d(TAG , "met : " + met);
//                Log.d(TAG , "calorie : " + calorie);
//
//                this.mMetArrayList.add(met);
//                this.mCalorie += calorie;
//
//                //starttime에 1초를 더한다.
//                this.mCurrentTime.addSecond( 1 );
//            }
//        }
//    }
//
//    @Override
//    public void onNextDay() {
//
//        this.requestCalculate(false);
//
//    }
//
//    public void requestCalculate( boolean isResponse )
//    {
//    	//Log.d(TAG, "TOTAL CALORIE = " + this.mCalorie);
//    	float[] metArray = this.convertListToFloatArray();
//        DataCalculater calculater = new DataCalculater( new TimeInfo(this.mStartTimeMilliSecond) , this.mCalorie , metArray , this.mDBManager , this.mAnalyzer , this.mMessenger , isResponse );
//        calculater.start();
//
//        //칼로리와 MET 정보를 재설정한다.
//        this.mStartTimeMilliSecond = this.mCurrentTime.getTimeMilliSecond();
//        this.mCalorie = 0;
//        this.mMetArrayList = new ArrayList<Float>();
//    }
//
//
//    public void requestCalculateForResult()
//    {
//        Log.d(TAG , "requestCalculateForResult()");
//        this.requestCalculate(true);
//    }
//
//
////    public void saveActivityData()
////    {
////        //데이터 베이스에 저장한다.
////        DayActivity dayActivity = new DayActivity();
////        dayActivity.setActivityDate( this.mCurrentTime.getDateString_yyyy_MM_dd() );
////        dayActivity.setCalorieByActivity(this.mCalorie);
////        dayActivity.setMetArray(this.mMetArray);
////
////        this.mDBManager.setDayActivity( dayActivity );
////    }
//
//    private int devideBy32ForInt(int k)
//    {
//        boolean negative = false;
//        if (k < 0)
//        {
//            negative = true;
//            k = -k;
//        }
//        k += 16;
//
//        int r = k >> 5;
//        if (negative)
//        {
//            r = -r;
//        }
//        return (int)r;
//    }
//
//    private int getTransValueForInt(int k, int gravity)
//    {
//        return (int)Math.round(k * 980.0 / 512 * gravity);
//    }
//
//    public void setRunning( boolean isRunning )
//    {
//        this.isRunning = isRunning;
//    }
//
//    private float[] convertListToFloatArray()
//    {
//        float[] result = new float[ this.mMetArrayList.size() ];
//
//        for( int i = 0 ; i < this.mMetArrayList.size() ;i++ )
//        {
//            result[i] = this.mMetArrayList.get(i);
//        }
//
//        return result;
//    }
//
//    public void setResponseMessenger(Messenger responseMessenger) {
//        this.mMessenger = responseMessenger;
//    }
//}
