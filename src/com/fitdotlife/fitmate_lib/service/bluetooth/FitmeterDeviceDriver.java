package com.fitdotlife.fitmate_lib.service.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;

import org.apache.log4j.Log;

import com.fitdotlife.fitmate_lib.service.protocol.CommunicationInterface;
import com.fitdotlife.fitmate_lib.service.protocol.FileReadException;
import com.fitdotlife.fitmate_lib.service.protocol.type.CodeConstants;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;


public class FitmeterDeviceDriver implements CommunicationInterface
{

    public interface BLEDeviceStateCallback {
        void deviceDisconnected(final BluetoothGatt gatt);
        void deviceInitializedErrorOccured(String errorMessage);
        void deviceInitializedTimeout();
        void initializeCompleted();
    }

    private final String TAG = "fitmateservice - " + FitmeterDeviceDriver.class.getSimpleName();
    private final int TIMEOUT_NUMBER = 2000;

    private final UUID TX_POWER_UUID = UUID.fromString("00001804-0000-1000-8000-00805f9b34fb");
    private final UUID TX_POWER_LEVEL_UUID = UUID.fromString("00002a07-0000-1000-8000-00805f9b34fb");
    public static final UUID CCCD = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
    private final UUID FIRMWARE_REVISON_UUID = UUID.fromString("00002a26-0000-1000-8000-00805f9b34fb");
    private final UUID DIS_UUID = UUID.fromString("0000180a-0000-1000-8000-00805f9b34fb");
    private final UUID RX_SERVICE_UUID = UUID.fromString("6e400001-b5a3-f393-e0a9-e50e24dcca9e");
    private final UUID RX_CHAR_UUID = UUID.fromString("6e400002-b5a3-f393-e0a9-e50e24dcca9e");
    public static final UUID TX_CHAR_UUID = UUID.fromString("6e400003-b5a3-f393-e0a9-e50e24dcca9e");
	
	private Context mContext = null;
	private BluetoothDevice mBluetoothDevice = null;
	private BluetoothGatt mBluetoothGatt = null;
	private BluetoothAdapter mBluetoothAdapter = null;
	
	private String mDeviceAddress = null;
	private BLEDeviceStateCallback deviceStateCallbacks = null;
    private BlockingQueue<byte[]> mQueue = null;

    // ConnectGatt을 했는지 확인하는 변수
    public boolean isAfterConnecting = false;


    //    private CountDownTimer timeOutTimer = new CountDownTimer( 10000 , 10000 ) {
//        @Override
//        public void onTick(long millisUntilFinished) {
//        }
//
//        @Override
//        public void onFinish() {
//            deviceStateCallbacks.deviceInitializedTimeout();
//        }
//    };
    int RetryCnt = 0;

    public boolean isConnected = false; // 연결만 완료
    public boolean isDisonnected = false; // 연결만 완료
    public boolean isDisconvered = false; // 연결만 완료
    public boolean isFailed = false; // 연결만 완료


    public static FitmeterDeviceDriver ConnectionOnce(Context ctx, String addr)
    {
        BluetoothManager mBluetoothManager = (BluetoothManager)ctx.getSystemService( Context.BLUETOOTH_SERVICE );
        BluetoothAdapter mBluetoothAdapter = mBluetoothManager.getAdapter();
        BluetoothDevice mBd = mBluetoothAdapter.getRemoteDevice(addr);

        final boolean[] isDisconnected = {true};
        final boolean[] isConnectedComplted = {true};

        final String TAG = "ConnectionOnce";
        FitmeterDeviceDriver fdd = new FitmeterDeviceDriver(ctx, mBluetoothAdapter, mBd, new FitmeterDeviceDriver.BLEDeviceStateCallback() {
            @Override
            public void deviceDisconnected(BluetoothGatt gatt) {
                org.apache.log4j.Log.d(TAG, "deviceDisconnected");
                isDisconnected[0] = true;
            }

            @Override
            public void deviceInitializedErrorOccured(String errorMessage) {
                org.apache.log4j.Log.d(TAG, "deviceInitializedErrorOccured msg :" + errorMessage);
            }

            @Override
            public void deviceInitializedTimeout() {
                org.apache.log4j.Log.d(TAG, "deviceInitializedTimeout");
            }

            @Override
            public void initializeCompleted() {
                org.apache.log4j.Log.d(TAG, "initializeCompleted");
                isDisconnected[0] = false;
                isConnectedComplted[0] = true;
            }
        });

        boolean isError133 = false;

        do{
            try {
                isConnectedComplted[0] = false;
                android.util.Log.i("ConThread", "RunConnection");

                android.util.Log.i("ConThread", "StartScan");
                android.util.Log.i("ConThread", "Start Connection");
                fdd.initialize();

                android.util.Log.i("ConThread", "Connection Check");
                // todo 10을 50으로 변경해야됨. 테스트용
                for(int i = 0; i < 30; i++)
                {
                    if(fdd.isConnected)
                    {
                        android.util.Log.i(TAG, "연결 성공");
                        break;
                    }

                    if(!fdd.isDisonnected)
                    {
                        android.util.Log.i(TAG, "연결 실패");
                        break;
                    }

                    if(fdd.isFailed)
                    {
                        android.util.Log.i(TAG, "연결 isFailed");
                        isError133 = true;

                        //fdd.disconnect();
                        //fdd.close();

                        Thread.sleep(1000);

                        // todo 여기 걸리면 삼성폰은 계속 대기함. 아마 커넥션을 강제로 끊는게 먹지 않는듯.
                        // 133 에러날때도 마찬가지임

                        throw new Exception("커넥션 중 133 에러 발생");

                        //break;
                    }

                    Thread.sleep(100);
                }

                if(fdd.isConnected)
                {
                    android.util.Log.i("ConThread", "Connected");
                    android.util.Log.i("ConThread", "Start Discovery");
                    fdd.startServicesDiscovery();
                    for(int i = 0; i < 30; i++)
                    {
                        if(fdd.isDisconvered)
                        {
                            break;
                        }

                        Thread.sleep(100);
                    }
                }
                else
                {
                    android.util.Log.i("ConThread", "Not Connected");

                    // todo 133과 비슷한 상황임
                    isError133 = true;

                    fdd.OnlyClose();
                    //fdd.close();
                    fdd.disconnect();
                    Thread.sleep(1000);

                    List<BluetoothDevice> mList = mBluetoothManager.getConnectedDevices(BluetoothProfile.GATT);
                    for (BluetoothDevice bd : mList) {
                        android.util.Log.i(TAG, String.format("연결 장치 체크 : %s[%s] Connected", bd.getName(), bd.getAddress()));
                    }

                    mList = mBluetoothManager.getConnectedDevices(BluetoothProfile.GATT_SERVER);
                    for (BluetoothDevice bd : mList) {
                        android.util.Log.i(TAG, String.format("연결 서버 체크 : %s[%s] Connected", bd.getName(), bd.getAddress()));
                    }
                    continue;
                }

                if(fdd.isDisconvered)
                {
                    android.util.Log.i("ConThread", "Start Descriptor Write");
                    fdd.setTXNotification();
                    for(int i = 0; i < 30; i++)
                    {
                        if(fdd.isSetTxNotification)
                        {
                            android.util.Log.i("ConThread", "Descriptor Write Completed");
                            break;
                        }

                        Thread.sleep(100);
                    }
                }
                else
                {
                    android.util.Log.i("ConThread", "Not Discorverd");
                    continue;
                }

                for(int i = 0; i < 30; i++)
                {
                    if(isConnectedComplted[0])
                    {
                        break;
                    }

                    Thread.sleep(100);
                }

                if(!isConnectedComplted[0])
                {
                    android.util.Log.i("ConThread", "Not isConnectedComplted");
                    continue;
                }

                android.util.Log.i("ConThread", "Start GetData");

                return fdd;
            }
            catch(Exception ex)
            {
                StringWriter errors = new StringWriter();
                ex.printStackTrace(new PrintWriter(errors));

                org.apache.log4j.Log.d("ConThread", "throw>" + ex.getMessage() + "\n" + errors.toString());
            }
            finally {
            }
        }while(false);

        return null;
    }


	private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback( )
	{
		@Override
		public void onCharacteristicChanged( BluetoothGatt gatt, BluetoothGattCharacteristic characteristic )
		{
			super.onCharacteristicChanged(gatt, characteristic);
			byte[] rawValue = characteristic.getValue();
			mQueue.add(rawValue);
		}

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt,
                                          BluetoothGattCharacteristic characteristic, int status) {
            if(status == BluetoothGatt.GATT_SUCCESS)
            {

            }
        }

		@Override
		public void onServicesDiscovered(BluetoothGatt gatt, int status) 
		{
            if(status != BluetoothGatt.GATT_SUCCESS)
                Log.e(TAG, "onServicesDiscovered error status:"+status);

			if( gatt.getDevice().getAddress() == mDeviceAddress ){

				if( status == BluetoothGatt.GATT_SUCCESS )
				{
                    isDisconvered = true;
                }
				else
				{
                    //타임아웃이 실행되어 있다면 종료
                    Log.e(TAG, "onServicesDiscovered error status:" + status);
				}
			}
		}

		@Override
		public void onConnectionStateChange(final BluetoothGatt gatt, int status , int newState)
		{
            isAfterConnecting = false;

            if(status != 0)
                Log.e(TAG, String.format("onConnectionStateChange status : %d newState : %d",status, newState));

            RetryCnt++;

            Log.d(TAG, String.format("Retry Cnt Add  : " + RetryCnt));


            try {
                BluetoothManager mBluetoothManager = (BluetoothManager) mContext.getSystemService(Context.BLUETOOTH_SERVICE);
                if(mBluetoothManager != null)
                {
                    List<BluetoothDevice> mList = mBluetoothManager.getConnectedDevices(BluetoothProfile.GATT);
                    for (BluetoothDevice bd : mList) {
                        Log.i(TAG, String.format("연결 장치 체크 : %s[%s] Connected", bd.getName(), bd.getAddress()));
                    }
                }
            }
            catch (Exception ex)
            {
                Log.e(TAG, "ERROR IN CONNECTED DEVICE CHECK : " + ex.getMessage());
            }

            try{
                if( gatt.getDevice().getAddress() == mDeviceAddress ) {
                    if (status == BluetoothGatt.GATT_SUCCESS) {
                        if (newState == BluetoothProfile.STATE_CONNECTED) {
                            Log.i(TAG, "기기와 연결이 되었습니다.");
                            isConnected = true;
                        } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                            //타임아웃이 실행되어 있다면 종료한다.
                            Log.i(TAG, "기기와 연결이 해제습니다.(onConnectionStateChange) checkcheck");
                            isFailed = true;
                            isDisonnected = true;

                            // todo : LG는 비정상적으로 연결 끊길 때 여기 콜되고, 삼성은 정상적으로 끊기면 여기 콜되는듯...확인 필요.
                            // Reconnect 대신 connect 하고나서 연결 붙었다 귾어졌다 하더니 LG 폰 아이 정상 동작이 안됨. 블투계속  onConnectionStateChange status : 133 newState : 2 이 나옴,
                        }
                    }
                    else
                    {


                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                Log.e(TAG, String.format("onConnectionStateChange status closing"));

                                try {
                                    //Thread.sleep(2000);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                //mBluetoothGatt.close();
                                //close();
                                //disconnect();
                                close();
                                Log.e(TAG, String.format("onConnectionStateChange status closed"));

                                // 삼성 폰에서 발생하는 연결중 133 에러 처리하기 위한 방법. 133에러 뿜고 장치와의 연결이 끊기지 않음
                                // todo 아직 100프로 해결은 아님. 간혹 close만 했을 때 안되는 경우가 있음
                            }}).start();


                        // todo : 아직 확인이 필요함(onClientStateChange 133 발생하면 통신 끊고 다시 시도하도록. 확인 필요)
                        Log.e(TAG, String.format("onConnectionStateChange status not success. status : %d newState : %d",status, newState));
                        isFailed = true;
                    }
                }
                else
                {
                    Log.e(TAG, String.format("onConnectionStateChange not equal? status : %d newState : %d",status, newState));
                }

            }
            catch(Exception ex)
            {
                Log.e(TAG, String.format("onConnectionStateChange exception " + ex.getMessage() + "\n" + ex.getStackTrace()));
            }
		}


		@Override
		public void onDescriptorRead(BluetoothGatt gatt , BluetoothGattDescriptor descriptor, int status) 
		{
			super.onDescriptorRead(gatt, descriptor, status);

		}

		@Override
		public void onDescriptorWrite( BluetoothGatt gatt , BluetoothGattDescriptor descriptor, int status )
		{
			super.onDescriptorWrite(gatt, descriptor, status);
            Log.d(TAG, "Bluetooth onDescriptorWrite status : " + (status==0?"SUCCESS":"FAIL"));
            if( descriptor.getUuid().equals(FitmeterDeviceDriver.CCCD ) ){

                //타임아웃이 실행되어 있다면
                //timeOutTimer.cancel();

                if( status == BluetoothGatt.GATT_SUCCESS ){
                    isSetTxNotification = true;

                    RetryCnt = 0;
                    deviceStateCallbacks.initializeCompleted();

                }else if( status == BluetoothGatt.GATT_FAILURE ){
                    try {
                        setTXNotification();
                    } catch (IOException e) {
                        disconnect();
                        close();

                        deviceStateCallbacks.deviceInitializedErrorOccured( "onDescriptorWrite() is Fail");
                    }
                }
            }

		}
		
		@Override
		public void onCharacteristicRead(BluetoothGatt gatt , BluetoothGattCharacteristic characteristic, int status) 
		{
			super.onCharacteristicRead(gatt, characteristic, status);
		}
	};


    public interface BTCallback{

        void DeviceRecoonnect();
        void DeviceRunning();
    }


	public FitmeterDeviceDriver( Context context, BluetoothAdapter bluetoothAdapter, BluetoothDevice device, BLEDeviceStateCallback callbacks )
	{
		this.mContext = context;
        this.mQueue = new ArrayBlockingQueue<byte[]>(50000);
		this.mBluetoothAdapter = bluetoothAdapter;
		this.mBluetoothDevice = device;

        String deviceAddress = mBluetoothDevice.getAddress();
        mDeviceAddress = deviceAddress;

		this.deviceStateCallbacks = callbacks;
	}

    public void initialize() {
        this.connect();
    }

    BluetoothAdapter.LeScanCallback mSacn = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {
            String deviceAddress = device.getAddress();
            if( deviceAddress != null ) {
                if (deviceAddress.equals(mDeviceAddress)) {
                    Log.i(TAG, "휘트미터 기기를 찾았습니다. 주소 : " + mDeviceAddress);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            mBluetoothAdapter.stopLeScan(mSacn);
                            // http://stackoverflow.com/a/18889509 3번 참고. 삼성폰은 onLeScan에서 stop 하지 말라고 함
                        }
                    });

                    isScaned = true;
                }
            }
        }
    };

    enum ConnectStatus
    {
        Idle,
        Scaning,
        Scaned,
        Connecting,
        Connted,
        Disconvering,
        Discovered,
        SetTxing,
        SetTxCompleted,
        InitalComplted,
        Disconnecting,
        Disconnected
    }

    ConnectStatus mConnectStatus = ConnectStatus.Idle;
    public void SetMode(ConnectStatus con)
    {
        mConnectStatus = con;
    }

    public boolean StartScan()
    {
        isScaned = false;
        mBluetoothAdapter.startLeScan(mSacn);
        int cnt = 0;
        while(!isScaned)
        {
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if(cnt++ > 10)
            {
                break;
            }
        }
        try {
            mBluetoothAdapter.stopLeScan(mSacn);
        }
        catch (Exception ex)
        {
            Log.e(TAG, "stopLeScan 실패 : " + ex.getMessage());
        }

        return isScaned;
    }

	public void connect() {

        isFailed = false;
        isConnected = false;
        isDisonnected = true;

        if(!mBluetoothAdapter.isEnabled())
        {
            Log.e(TAG, String.format("bt turn off when connect"));
            return;
        }

		String deviceAddress = mBluetoothDevice.getAddress();

		if (mBluetoothAdapter == null || deviceAddress == null) {
            this.deviceStateCallbacks.deviceInitializedErrorOccured("connect() - BluetoothAdapter is null and deviceaddress is null");
        }
        mDeviceAddress = deviceAddress;


        try
        {
            //throw new Exception();
        }
        catch(Exception ex)
        {
            StringWriter errors = new StringWriter();
            ex.printStackTrace(new PrintWriter(errors));

            Log.d(TAG, "CALL CONNECT " + errors.toString());
        }

        isAfterConnecting = true;

        // check if we need to connect from scratch or just reconnect to previous device
        if(mBluetoothGatt != null && mBluetoothGatt.getDevice().getAddress().equals( deviceAddress )) 
        {

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            Log.d(TAG, "기기 연결 요청 시작 RECONNECT");
            mBluetoothGatt = mBluetoothDevice.connectGatt( mContext , false, this.mGattCallback );
        }
        else 
        {

        	// connect from scratch
            // get BluetoothDevice object for specified address
            mBluetoothDevice = mBluetoothAdapter.getRemoteDevice( mDeviceAddress );
            if (mBluetoothDevice == null) 
            {
                // we got wrong address - that device is not available!
                this.deviceStateCallbacks.deviceInitializedErrorOccured("connect() - BluetoothDevice is null");
            }

            Log.d(TAG, "기기 연결 요청 시작");
            // connect with remote device
        	mBluetoothGatt = mBluetoothDevice.connectGatt( mContext , false, this.mGattCallback );

            // todo onClientConnectionState에서 133이 오면 service 자체가 멈춤(해결 필요)
        }

        //타임아웃 타이머 시작
       // this.timeOutTimer.start();
	}

    public boolean isScaned = false;
    //private Handler mHandler = new Handler();

    /* disconnect the device. It is still possible to reconnect to it later with this Gatt client */
    public void disconnect() {
        if(mBluetoothGatt != null) mBluetoothGatt.disconnect();
    }

    /* close GATT client completely */
    public void close() 
    {

        try {
            if (mBluetoothGatt != null) mBluetoothGatt.close();
            mBluetoothGatt = null;  // 이게 없으면 삼성폰에서 133 에러 나왔을 때 close가 되지 않음... LG 폰은 상관없음
        }
        catch (Exception ex)
        {
            Log.d(TAG, "Error in close  : " + ex.getMessage());
        }
    }

    /* close GATT client completely */
    public void OnlyClose()
    {

        try {
            if (mBluetoothGatt != null) mBluetoothGatt.close();
            //mBluetoothGatt = null;  // 이게 없으면 삼성폰에서 133 에러 나왔을 때 close가 되지 않음... LG 폰은 상관없음
        }
        catch (Exception ex)
        {
            Log.d(TAG, "Error in onlyclose  : " + ex.getMessage());
        }
    }
	
    public void startServicesDiscovery() throws IOException {
        isDisconvered = false;

        Log.d(TAG, "startServicesDiscovery function call");
        if( mBluetoothGatt == null ){
            throw new IOException( "startServiceDiscovery() - BluetoothGatt is null" );
        }

        if(!mBluetoothGatt.discoverServices()){
            throw new IOException( "startServiceDiscovery() - discoverServices() is Fail " );
        }

    }
    
    public String getAddress()
    {
    	return this.mBluetoothDevice.getAddress();
    }

    @Override
    public void send( byte[] message ) throws IOException {
        Log.d(TAG, "TX : " + this.bytesToHex(message));

        int length = message.length;
        int left = message.length;
        int start = 0;
        int datalen = 20;

        while(length > 0)
        {
            int realdatalen = Math.min(length, datalen);
            byte[] newMessage = Arrays.copyOfRange(message, start, start + realdatalen);

            this.writeRXCharacteristic(newMessage);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            start += realdatalen;
            length -= realdatalen;
        }

        //TODO onCharateristicWrite으로 돌아오는 데이터를 보고 확실히 쓰여졌는지 확인해야 함.
    }

    @Override
    public void send(byte message) throws IOException {
        //TODO 구현해야함.
    }

    @Override
    public byte[] read( int length ) throws IOException, FileReadException {

        byte[] resultBytes = new byte[ length ];
        int index = 0;
        int timeOutCount = 0;

        while( index < length  )
        {
            if( this.mQueue.isEmpty()) {

                timeOutCount++;

            }else{

                byte[] tempBytes = null;
                try {

                    tempBytes = this.mQueue.poll();

                }catch(NoSuchElementException e){
                    throw new FileReadException("큐에서 데이터를 읽는 중에 오류가 발생하였습니다. 큐에서 읽은 데이터 값 : " + tempBytes );
                }

                if( tempBytes == null ){
                    throw new FileReadException("큐에서 데이터를 읽는 중에 오류가 발생하였습니다. 큐에서 읽은 데이터 값 : null");
                }

                try{
                    System.arraycopy( tempBytes , 0 , resultBytes , index , tempBytes.length );
                }catch( IndexOutOfBoundsException e ){
                    throw new FileReadException( "read() 함수에서 바이트 배열을 복사하는 중에 오류가 발생하였습니다. - " + e.getMessage() );
                }

                index += tempBytes.length;

                Log.d(TAG , "read() - index : " + index );

               //데이터를 받는다면 타임아웃 카운트를 초기화 시킨다.
                timeOutCount = 0;
            }

            try {
                Thread.sleep( 2 );
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if( timeOutCount == TIMEOUT_NUMBER ){

                throw new FileReadException("read( length ) 함수에서 타임아웃 발생.");
            }
        }

        return resultBytes;
    }

    @Override
    public byte[] read() throws IOException, FileReadException {
        byte[] resultBytes = null;
        byte[] readBytes = new byte[ 1024 ];
        int readLength = 0;
        boolean isRunning = true;
        int timeoutCount = 0;
        boolean isSTX = false;

        while( isRunning ){

            if( this.mQueue.isEmpty() ) {

                timeoutCount++;

            }else{

                byte[] tempBytes = null;
                try {

                    tempBytes = this.mQueue.poll();

                }catch(NoSuchElementException e){
                    throw new FileReadException("큐에서 데이터를 읽는 중에 오류가 발생하였습니다. 큐에서 읽은 데이터 값 : " + tempBytes );
                }

                if( tempBytes == null ){
                    throw new FileReadException("큐에서 데이터를 읽는 중에 오류가 발생하였습니다. 큐에서 읽은 데이터 값 : null");
                }

                //STX 확인되었으면 다음으로 확인이 안되었으면 확인한다.
                if( !isSTX ) {
                    if (tempBytes[0] == CodeConstants.PC_COMM_START) {
                        isSTX = true;
                    }else{
                        continue;
                    }
                }

                try {
                    System.arraycopy(tempBytes, 0, readBytes, readLength, tempBytes.length);
                }catch( IndexOutOfBoundsException e ){
                    throw new FileReadException( "read() 함수에서 바이트 배열을 복사하는 중에 오류가 발생하였습니다. - " + e.getMessage() );
                }
                readLength += tempBytes.length;

                if(tempBytes[ tempBytes.length - 1 ] == CodeConstants.PC_COMM_END ){ //ETX 이다.
                    isRunning = false;
                }

                //데이터를 받게 된다면 타임아웃 카운트를 초기화 시킨다.
                timeoutCount = 0;
            }

            try {
                Thread.sleep( 2 );
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if( timeoutCount == TIMEOUT_NUMBER ){
                throw new FileReadException("read() 함수에 타임아웃 발생.");
            }
        }

        resultBytes = new byte[ readLength ];
        System.arraycopy(readBytes , 0 , resultBytes , 0 , readLength);

        Log.d(TAG, "RX : " + this.bytesToHex(resultBytes));

        return resultBytes;
    }

    @Override
    public byte[] readData(int length) throws IOException, FileReadException {

        byte[] resultBytes = new byte[ length ];
        int index = 0;
        int receiveLength = 0;
        int timeOutCount = 0;

        while( receiveLength < length  )
        {
            if( this.mQueue.isEmpty()){

                timeOutCount++;

            }else{

                byte[] tempByte = null;
                try {

                    tempByte = this.mQueue.poll();

                }catch(NoSuchElementException e){
                    throw new FileReadException("큐에서 데이터를 읽는 중에 오류가 발생하였습니다. 큐에서 읽은 데이터 값 : " + tempByte );
                }

                if( tempByte == null ){
                    throw new FileReadException("큐에서 데이터를 읽는 중에 오류가 발생하였습니다. 큐에서 읽은 데이터 값 : null");
                }

                receiveLength += tempByte.length;

                try{
                    System.arraycopy( tempByte , 0 , resultBytes , index , tempByte.length );
                }catch( IndexOutOfBoundsException e ){
                    throw new FileReadException( "read() 함수에서 바이트 배열을 복사하는 중에 오류가 발생하였습니다. - " + e.getMessage() );
                }

                index += tempByte.length;

                //데이터를 받으면 타임아웃을 초기화한다.
                timeOutCount = 0;
            }

            try {
                Thread.sleep( 2 );
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if( timeOutCount == TIMEOUT_NUMBER ){
                throw new FileReadException("readData() 함수에서 타임아웃 발생");
            }
        }

        return resultBytes;
    }

    private boolean writeRXCharacteristic( byte[] value ) throws IOException {
        Log.d(TAG, "Bluetooth writeRXCharacteristic start");

        if( mBluetoothGatt == null ){
            throw new IOException("BluetoothGatt is Null");
        }

        BluetoothGattService RxService = mBluetoothGatt.getService(RX_SERVICE_UUID);

        if (RxService == null) {
            throw new IOException("RXService not found.");
        }

        BluetoothGattCharacteristic RxChar = RxService.getCharacteristic(RX_CHAR_UUID);
        if (RxChar == null) {
            throw new IOException("Rx charateristic not found!");
        }
        RxChar.setValue( value );

        boolean status = mBluetoothGatt.writeCharacteristic(RxChar);
        android.util.Log.d(TAG, "write TXchar - status:" + status);
        return status;
    }

    public boolean isSetTxNotification = false;

    public void setTXNotification(  ) throws IOException {
        isSetTxNotification = false;
        Log.d(TAG, "Bluetooth setTXNotification start");

        if( mBluetoothGatt == null ){
            throw new IOException("BluetoothGatt is Null");
        }

        BluetoothGattService RxService = mBluetoothGatt.getService(RX_SERVICE_UUID);

        if (RxService == null) {
            throw new IOException("Rx service not found!");
        }

        BluetoothGattCharacteristic TxChar = RxService.getCharacteristic(TX_CHAR_UUID);
        if (TxChar == null) {
            throw new IOException("Tx charateristic not found!");
        }
        mBluetoothGatt.setCharacteristicNotification(TxChar,true);

        BluetoothGattDescriptor descriptor = TxChar.getDescriptor(CCCD);
        descriptor.setValue( BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE );
        mBluetoothGatt.writeDescriptor(descriptor);

        Log.d(TAG, "Bluetooth setTXNotification end");
    }

    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }
}
