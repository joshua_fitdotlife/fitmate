package com.fitdotlife.fitmate_lib.service.bluetooth;

/**
 * Created by Joshua on 2015-03-19.
 */
public class BluetoothRawData {

    private int mFileIndex = 0;
    private byte[] mFileData = null;
    private int mFileOffset = 0;
    private boolean isAdded = false;
    private boolean doParse = true;

    public int getFileIndex() {
        return mFileIndex;
    }

    public void setFileIndex(int fileIndex) {
        this.mFileIndex = fileIndex;
    }

    public byte[] getFileData() {
        return mFileData;
    }

    public void setFileData(byte[] rawData) {
        this.mFileData = new byte[ rawData.length ];
        System.arraycopy(rawData , 0, mFileData, 0 , mFileData.length);
    }

    public void setFileOffset( int fileOffset ){
        this.mFileOffset = fileOffset;
    }

    public int getFileOffset(){
        return this.mFileOffset;
    }

    public void addFileData(byte[] addBytes){
        byte[] tempBytes = new byte[ mFileData.length + addBytes.length ];
        System.arraycopy(mFileData, 0 , tempBytes , 0 , mFileData.length );
        System.arraycopy(addBytes , 0 , tempBytes , mFileData.length , addBytes.length);
        this.mFileData = tempBytes;
        isAdded = true;
    }

    public boolean getIsAdded(){
        return this.isAdded;
    }

    public void setDoParse( boolean doParse ){
        this.doParse = doParse;
    }

    public boolean getDoParse(){
        return this.doParse;
    }

}
