package com.fitdotlife.fitmate_lib.service.bluetooth;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.service.database.FitmateServiceDBManager;
import com.fitdotlife.fitmate_lib.service.key.SyncType;
import com.fitdotlife.fitmate_lib.service.protocol.FileReadException;
import com.fitdotlife.fitmate_lib.service.protocol.ProtocolManager;
import com.fitdotlife.fitmate_lib.service.protocol.object.SystemInfo_Request;
import com.fitdotlife.fitmate_lib.service.protocol.object.SystemInfo_Response;
import com.fitdotlife.fitmate_lib.service.protocol.object.TimeInfo;
import com.fitdotlife.fitmate_lib.service.protocol.type.VersionType;

import org.apache.log4j.Log;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by Joshua on 2015-02-27.
 */
        public class FitmeterDeviceManager implements FitmeterDeviceDriver.BLEDeviceStateCallback
        {
            public static final String FILE_INDEX_KEY = "fileindex";
            private final String PREF_TIMEZONE = "previoustimezone";
            private final String TAG = "fitmateservice - " + this.getClass().getSimpleName();

            private FitmeterDeviceDriver mDeviceDriver = null;
            private Context mContext = null;
            private FitmeterDeviceStateCallback mCallback = null;
            private FitmateServiceDBManager mFitmateServiceDBManager = null;
            private FitmateDBManager mDBManager = null;
            private BluetoothAdapter mBluetoothAdapter = null;

            private Calendar lastbatteryCheckTime;
            private Calendar lastTimeSetTime;

            public interface FitmeterDeviceStateCallback{
                void onSyncInfo(SyncType syncType, int recceiveBytes, int totalBytes);
                void onBatteryStatus(int batteryRatio);
            }

            public FitmeterDeviceManager( Context context, BluetoothAdapter bluetoothAdapter, FitmeterDeviceStateCallback callback , FitmateServiceDBManager dbManager ){
                this.mContext = context;
                this.mCallback = callback;
                this.mFitmateServiceDBManager = dbManager;
        this.mBluetoothAdapter = bluetoothAdapter;
        this.mDBManager = new FitmateDBManager(context);

        lastbatteryCheckTime =Calendar.getInstance();
       // lastbatteryCheckTime.setTimeInMillis(this.mDBManager.getLastConnectedTime(this.mDBManager.getUserInfo().getEmail()));
        lastbatteryCheckTime.setTimeInMillis(0);

        lastTimeSetTime= Calendar.getInstance();
        lastTimeSetTime.setTimeInMillis((0));
    }

    private boolean isPastOverOneHour(Calendar prev){

        if( Calendar.getInstance().getTimeInMillis() - prev.getTimeInMillis() >3600000)
        {
            return true;
        }
        return false;
    }

    public void setBluetoothDevice( BluetoothDevice device ){
        /*
        if(this.mDeviceDriver == null)
            this.mDeviceDriver = new FitmeterDeviceDriver( this.mContext , this.mBluetoothAdapter ,  device , this);
            */
    }


    @Override
    public void deviceDisconnected( BluetoothGatt gatt ) {
        Log.d(TAG, "deviceDisconnected");
    }

    @Override
    public void deviceInitializedErrorOccured(String errorMessage) {
        Log.d(TAG, "deviceInitializedErrorOccured\n" + errorMessage);
        //this.mCallback.deviceInitializedErrorOccured();
    }

    @Override
    public void deviceInitializedTimeout() {
        Log.d(TAG, "deviceInitializedTimeout");
        //this.mCallback.deviceInitializedErrorOccured();
    }

    @Override
    public void initializeCompleted(){
        Log.d(TAG, "initializeCompleted");
        new Thread(new Runnable() {
            @Override
            public void run() {

                getData(mDeviceDriver);
                mDeviceDriver.disconnect();

                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                mDeviceDriver.initialize();
            }
        });
        //}).start();
    }

    public int getBatteryRatio(ProtocolManager protocolManager)
    {
        int battery = -1;
        try {

            battery = protocolManager.getBatteryInfo();

        } catch (FileReadException e) {
            Log.e(TAG, e.getMessage());
            //FitmateServiceStarter.queueUsed = false;
            return battery;
        }

        return battery;
    }

    /**
     * 자정 기준으로 1시간 이내이면 false, 이전 동기화 시간에서 6시간 지났으면 true
     * @param prev
     * @return
     */
    private boolean isTimeToSetCurrentTime(Calendar prev)
    {
        Calendar current = Calendar.getInstance();

        if (current.get(Calendar.HOUR) <= 1 || current.get(Calendar.HOUR) >= 23) {
            return false;
        }
        if(current.getTimeInMillis() - prev.getTimeInMillis()> 2*3600000){
            return true;
        }else{
            return false;
        }


    }


    /**
     * 휘트미터에서 데이터를 가져온다.
     *
     * 1. 이전에 저장해 놓은 파일 인덱스와 데이터 인덱스를 가져온다.
     * 2. getNumberOfFile로 기기로부터 파일 개수를 알아온다.
     * 3. 이전에 저장해 놓은 파일 인덱스의 파일의 크기를 가져온다.
     * 4. getDataFile(int fileIndex , int dataIndex , int fileSize)로 호출하여 데이터를 읽는다.
     * 5. 이전에 저장해 놓은 바이트 배열을 가져와서 지금 읽은 데이터를 뒤에 붙인다.
     * 6 에러가 발생한 경우 현재 Index와 현재 FIleINdex를 저장한다.
     */
    public boolean getData(FitmeterDeviceDriver mDeviceDriver) {

        this.mDeviceDriver = mDeviceDriver;
        ProtocolManager protocolManager = new ProtocolManager( this.mContext , mDeviceDriver );

        this.setLastConnectedTime();

        if(isPastOverOneHour(lastbatteryCheckTime)){
            int battery = getBatteryRatio(protocolManager);
            mDBManager.setLastBatterhRatio(mDBManager.getUserInfo().getEmail(), battery, mBluetoothAdapter.getAddress());
            lastbatteryCheckTime = Calendar.getInstance();
            Log.i(TAG , "배터리 체크 완료 - " + battery);
            this.mCallback.onBatteryStatus(battery);

        }


        //기기에 시간을 설정하기 전에 시간대가 변했는지 확인한다.
        //기기에 시간을 설정한다.
        try {

            if(isTimeToSetCurrentTime(lastTimeSetTime)){
                protocolManager.setCurrentTime( );
                lastTimeSetTime = Calendar.getInstance();
                Log.i(TAG , "시간설정 완료");

                SystemInfo_Response systemInfo = protocolManager.getSystemInfo();
                String firmwareVersion = systemInfo.getMajorVersion() + "." + systemInfo.getMinorVersion();
                saveFirmwareVersionSharedPreference( mContext ,  firmwareVersion);
            }

        } catch (FileReadException e) {
            Log.e(TAG, "시간 설정 중에 오류가 발생했습니다.");

            return false;
        }

        //휘트미터로부터 활동량 데이터를 가져온다.
        int[] numberOfFile = null;
        try {

            numberOfFile = protocolManager.getNumberOfFile();

        } catch (FileReadException e) {
            Log.e(TAG, e.getMessage());
            //DisconnectAndRetry();
            return false;
        }

        int startIndex = numberOfFile[0];
        int endIndex = numberOfFile[1];

        int fileIndex = 0;
        int fileOffset = 0;
        BluetoothRawData lastRawData = this.mFitmateServiceDBManager.getLastFileData();

        if( lastRawData == null ){

            long lastDataTime = this.mDBManager.getUserInfo().getLastDataDate();

            if( lastDataTime > 0 ){

                boolean findIndex = false;

                for( int i = endIndex ;  i >= startIndex ; i--) {
                    //현재 파일인덱스에 해당하는 날짜를 가져온다.

                    byte[] readData = null;
                    try {

                       readData =  protocolManager.readCurrentFileData( i , 42 , 7 );

                    } catch (FileReadException e) {
                        Log.e(TAG , "파일 인덱스 계산중 데이터를 읽는 중에 오류가 발생했습니다. - " + e.getMessage() );
                        break;
                    }

                    if( readData != null ){

                        long fileIndexTime = new TimeInfo(readData).getMilliSecond();
                        if( lastDataTime > fileIndexTime ){
                            fileIndex = i + 1 ;
                            findIndex = true;
                            break;
                        }
                    }
                } // end for

                if( !findIndex ){
                    fileIndex = startIndex;
                    fileOffset = 0;
                }

            }else{
                fileIndex = startIndex;
                fileOffset = 0;
            }

        }else{
            fileIndex = lastRawData.getFileIndex();
            fileOffset = lastRawData.getFileOffset();
        }


        Log.i(TAG , "현재 인덱스" + fileIndex);
        Log.i(TAG , "끝 인덱스" + endIndex);

        //가져올 파일의 모든 용량을 가져온다.
        int index = fileIndex;
        int totalBytes = 0;
        int receiveBytes = 0;

        int[] arrFileLength = new int[endIndex - fileIndex + 1];
        try {

            for (int i = 0; i < arrFileLength.length; i++) {
                arrFileLength[i] = protocolManager.getFileInfo(i + fileIndex);
                BluetoothRawData dataFile = this.mFitmateServiceDBManager.getDataFile(i + fileIndex);
                int dataFileLength = 0;

                if (dataFile != null) {
                    dataFileLength = dataFile.getFileData().length;
                }

                totalBytes += (arrFileLength[i] - dataFileLength);

            } //End For

        } catch (FileReadException e) {
            Log.e(TAG, "파일의 크기를 읽는 중에 오류가 발생하였습니다.");
            //DisconnectAndRetry();
            return false;
        }


        Log.i( TAG , "totalbyte : " + totalBytes );
        for (; index <= endIndex;) {

            try{

                int fileLength = arrFileLength[ index - fileIndex  ];

                Log.i(TAG, "읽어야 할 오프셋 : " + fileOffset );
                Log.i(TAG, "휘트미터 기기로부터 " + index  + "번째 데이터를 가져오는 중입니다.");
                Log.i(TAG, "각 데이터 파일의 크기 : index - " + index + " , length - " + fileLength);

                if (fileLength != 0) {

                    while ( fileOffset < fileLength ) {

                        byte[] arrReadData = null;
                        int readLength = fileLength - fileOffset;
                        readLength = Math.min( readLength, 0x200 );

                        arrReadData = protocolManager.readCurrentFileData( index , fileOffset, readLength );

                        fileOffset += readLength;
                        this.mFitmateServiceDBManager.updateFileData( index, fileOffset, arrReadData);

                        Log.i( TAG , "디비에 저장 fileindex : " + index + ", fileOffset : " + fileOffset );

                        //동기화 진행율 전송
                        receiveBytes += readLength;
                        Log.i( TAG , "동기화 중 receiveBytes : " + receiveBytes + ", totalBytes : " + totalBytes );
                        this.mCallback.onSyncInfo(SyncType.PROGRESS , receiveBytes , totalBytes);
                    }
                }

                //첫 파일 인덱스 이후는 파일 오프셋을 0으로 설정한다.
                fileOffset = 0;
                index++;

            }catch( FileReadException e ){
                Log.e(TAG, index + " 번째 데이터 파일을 읽는 동안에 에러가 발생하였습니다. - " + e.getMessage());

                //this.mCallback.deviceErrorOccured( );
                this.mCallback.onSyncInfo(SyncType.END, 0, 0);
                return false;
            }
        }

        Log.i(TAG, "동기화 끝");
        this.mCallback.onSyncInfo(SyncType.END, 0, 0);
        this.checkChangeTimeZone(protocolManager);
        return true;
    }

    private void checkChangeTimeZone(ProtocolManager protocolManager){

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        String oldTimezone = prefs.getString(PREF_TIMEZONE, null);
        String newTimezone = TimeZone.getDefault().getID();
        long now = System.currentTimeMillis();

        if( oldTimezone == null ){
            oldTimezone = TimeZone.getDefault().getID();
        }

        if( TimeZone.getTimeZone(oldTimezone).getOffset( now ) != TimeZone.getTimeZone(newTimezone).getOffset(now) ){
            prefs.edit().putString(PREF_TIMEZONE , newTimezone).commit();
            Log.d(TAG, "시간대 변경됨");

            //파일을 다시 시작한다.
            Log.d(TAG , "파일 다시 시작 요청");
            try {
                protocolManager.setCurrentTime( );
                Log.d(TAG, "시간 설정");

                SystemInfo_Response sr = protocolManager.getSystemInfo();
                SystemInfo_Request dd = sr.getSystemInfo();

                boolean setResult = protocolManager.setSystemInfo(dd);
                if(setResult){
                    Log.d("PROTOCOL", "SETOK");
                }

            } catch (Exception ex) {
                Log.d("DATACOMM", ex.getMessage());
            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Log.d(TAG , "파일 다시 시작 완료");

            //이전에 NeedMerge인 날짜들을 NOT_UPLOAD로 설정한다.
            this.mDBManager.setNotUploadData();

            Calendar todayCalendar = Calendar.getInstance();
            String todayDate = todayCalendar.get(Calendar.YEAR) + "-" + String.format("%02d", todayCalendar.get(Calendar.MONTH) + 1) + "-" + String.format("%02d", todayCalendar.get(Calendar.DAY_OF_MONTH));
            this.mDBManager.setNeedMergeDate( todayDate );
            Log.d(TAG , "Need Merge 설정");
        }
    }


    private void setLastConnectedTime(){
        try {
            UserInfo userInfo = this.mDBManager.getUserInfo();
            this.mDBManager.setLastConnectedTime( userInfo.getEmail() , new Date() , this.mDeviceDriver.getAddress() );
            UserInfo.saveLastAlarmInSharedPreference(mContext, true);
            Log.e(TAG, "마지막 연결 시간 -  " +  new Date().getTime() );
        }
        catch(Exception ex){
            Log.e(TAG, "setLastConnectedTime set error " + ex.getMessage());
        }
    }

    private void saveFirmwareVersionSharedPreference( Context context, String firmwareVersion )
    {
        SharedPreferences userInfo = context.getSharedPreferences( "fitmate" , Activity.MODE_PRIVATE );
        SharedPreferences.Editor editor = userInfo.edit();
        editor.putString("firmwareversion" , firmwareVersion );
        editor.commit();
    }
}
