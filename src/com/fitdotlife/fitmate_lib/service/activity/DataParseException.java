package com.fitdotlife.fitmate_lib.service.activity;

public class DataParseException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String message;
	
	public DataParseException( String message )
	{
		this.message = message;
	}

	@Override
	public String getMessage() {

		return this.message;
	}
}
