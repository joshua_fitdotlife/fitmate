package com.fitdotlife.fitmate_lib.service.activity;

public class HeaderParseException extends Exception {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private String message;

	public HeaderParseException(String message)
	{
		this.message = message;
	}

	@Override
	public String getMessage() {

		return this.message;
	}
}
