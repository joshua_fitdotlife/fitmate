package com.fitdotlife.fitmate_lib.service;

import android.os.Bundle;

/**
 * Created by Joshua on 2015-02-24.
 */
public interface ServiceRequestCallback {
    public void onServiceResultReceived(int what, Bundle data);
    public void serviceInitializeCompleted();
}
