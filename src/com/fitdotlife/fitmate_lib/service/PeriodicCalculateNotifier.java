package com.fitdotlife.fitmate_lib.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

/**
 * Created by Joshua on 2015-03-17.
 */
public class PeriodicCalculateNotifier {

    private final String TAG = "fitmateservice - " + PeriodicCalculateNotifier.class.getSimpleName();

    private final String ACTION_CALCULATE = "com.fitdotlife.fitmateservice.CALCULATE";
    private final int CALCULATE_INTERVAL = 60 * 60 * 1000;

    private CalculateNotifyListener mListener = null;
    private Context mContext = null;

    private PendingIntent mSender = null;
    private AlarmManager mAlarmManager = null;

    public interface CalculateNotifyListener{
        public void onRequestCalculate();
    }

    private BroadcastReceiver mCalcualateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d( TAG, "계산 알람이 호출되었습니다.");
            mListener.onRequestCalculate();
        }
    };


    public PeriodicCalculateNotifier( Context context , CalculateNotifyListener listener ){
        this.mContext = context;
        this.mListener = listener;

        this.mAlarmManager = (AlarmManager) this.mContext.getSystemService( Context.ALARM_SERVICE );
    }

    public void startCalculateAlarm()
    {
        Log.d(TAG, "계산 알람을 시작합니다.");
        IntentFilter filter = new IntentFilter();
        filter.addAction( this.ACTION_CALCULATE );
        this.mContext.registerReceiver(this.mCalcualateReceiver , filter);

        Intent intent = new Intent( ACTION_CALCULATE );
        this.mSender = PendingIntent.getBroadcast(this.mContext, 0, intent, 0);
        this.mAlarmManager.setRepeating(AlarmManager.RTC_WAKEUP , System.currentTimeMillis() , this.CALCULATE_INTERVAL , this.mSender);
    }


    public void stopCalculateAlarm()
    {
        Log.d( TAG, "계산 알람을 종료합니다.");
        this.mAlarmManager.cancel( this.mSender );
        this.mContext.unregisterReceiver( this.mCalcualateReceiver );
    }

}
