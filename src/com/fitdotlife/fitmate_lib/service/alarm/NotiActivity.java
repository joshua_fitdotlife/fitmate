package com.fitdotlife.fitmate_lib.service.alarm;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.fitdotlife.fitmate.LoginActivity;
import com.fitdotlife.fitmate.R;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by wooseok on 2015-06-03.
 */
public class NotiActivity extends Activity {

    Context mContext = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("NOTI", "ALARM");

        mContext = this;
        ShowNotifiation("Fitmate", this.getString(R.string.myprofile_for_healthy_life), "Fitmate");

        this.finish();
    }

    //�˶��� ���� �ð��� �߻��ϴ� ����Ʈ �ۼ�
    private PendingIntent pendingIntent() {
        Intent i = new Intent(getApplicationContext(), NotiActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pi = PendingIntent.getActivity(this, 0, i, 0);

        Log.i("HelloAlarmActivity", "Pending Indent" + mCalendar.getTime().toString());
        return pi;
    }

    // �˶� �޴���


    //�˶��� ����
    private void setAlarm() {
        mCalendar = new GregorianCalendar();
        mCalendar.add(Calendar.SECOND, 10);
        mManager.set(AlarmManager.RTC_WAKEUP, mCalendar.getTimeInMillis(), pendingIntent());
        Log.i("HelloAlarmActivity", "SET ALARM" + mCalendar.getTime().toString());
        //finish();
    }

    //�˶��� ����
    private void resetAlarm() {
        mManager.cancel(pendingIntent());
        Log.i("HelloAlarmActivity", "CLEAR ALARM" + mCalendar.getTime().toString());
    }


    GregorianCalendar mCalendar;
    private AlarmManager mManager;
    private NotificationManager mNotification;
    void SetNotify()
    {

        mNotification = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);

        //�˶� �Ŵ����� ���
        mManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);


        mCalendar = new GregorianCalendar();
        mCalendar.add(Calendar.SECOND, 10);
        mManager.set(AlarmManager.RTC_WAKEUP, mCalendar.getTimeInMillis(), pendingIntent());
        Log.i("HelloAlarmActivity", "SET ALARM" + mCalendar.getTime().toString());
    }


    void ShowNotifiation(String title, String content, String noti)
    {
        NotificationManager nm = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0, new Intent(mContext, LoginActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mCompatBuilder = new NotificationCompat.Builder(mContext);
        mCompatBuilder.setSmallIcon(R.drawable.fitlife_ico);
        mCompatBuilder.setTicker(noti);
        mCompatBuilder.setWhen(System.currentTimeMillis());
        //mCompatBuilder.setNumber(10);
        mCompatBuilder.setContentTitle(title);
        mCompatBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(content));
        //mCompatBuilder.setContentText(content);
        mCompatBuilder.setVisibility(Notification.VISIBILITY_PUBLIC);
        mCompatBuilder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);
        mCompatBuilder.setContentIntent(pendingIntent);
        mCompatBuilder.setAutoCancel(true);

        nm.notify(222, mCompatBuilder.build());
    }
}
