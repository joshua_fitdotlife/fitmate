package com.fitdotlife.fitmate_lib.service.alarm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.WearingLocation;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseAnalyzer;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseProgram;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.StrengthInputType;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.UserInfoForAnalyzer;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.object.DayActivity;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate.LoginActivity;
import com.fitdotlife.fitmate.R;
import com.fitdotlife.fitmate_lib.object.UserNotiSetting;
import com.fitdotlife.fitmate_lib.object.WeekActivity;
import com.fitdotlife.fitmate_lib.service.ActivityDataCalculator;
import com.fitdotlife.fitmate_lib.service.util.Utils;
import com.fitdotlife.fitmate_lib.util.DateUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class AlarmService extends Service implements Runnable
{
    private final String TAG = "AlarmService";
    private UserNotiSetting userNotiSetting = null;

    static boolean isRunThread = false;
    Thread mThread = null;

    Context mContext = null;



    private boolean alreadyBatteryNotification = false;

    @Override
    public void onCreate()
    {
        Log.d(TAG, "onCreate()");
        mContext = getBaseContext();
        this.mDBManager = new FitmateDBManager( this.getApplicationContext() );

        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG , "onStartCommand()");
        super.onStartCommand(intent, flags, startId);

        if(!isRunThread ) {
            mThread = new Thread(this);
            mThread.start();
        }

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy()");

        if(mThread != null)
        {
            mThread.interrupt();
        }

        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent)
    {
        Log.d(TAG, "onBind()");
        return null;
    }

    @Override
    public boolean onUnbind( Intent intent )
    {
        Log.d(TAG , "onUnBind()");
        return super.onUnbind(intent);
    }

    private FitmateDBManager mDBManager = null;
    final int TimeOutHours = 8;
    final int TimeOutMilli = TimeOutHours * 1000 * 60 * 60;

    private boolean isConnectionTimeout() {
        UserInfo.getAlarmTimeSharedPreference(mContext);
        String email = mDBManager.getUserInfo().getEmail();
        long time=  mDBManager.getLastConnectedTime(email);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        Calendar oCalendar = Calendar.getInstance();  // 현재 날짜/시간 등의 각종 정보 얻기
        long current = oCalendar.getTimeInMillis();
        long diff = current - time;

        boolean isOverLastAlarm = UserInfo.getLastAlarmSharedPreference(mContext);
        if(diff >= TimeOutMilli && isOverLastAlarm)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    void showNotification(String title, String content, String noti)
    {
        NotificationManager nm = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0, new Intent(mContext, LoginActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mCompatBuilder = new NotificationCompat.Builder(mContext);
        mCompatBuilder.setSmallIcon(R.drawable.fitlife_ico);
        mCompatBuilder.setTicker(noti);
        mCompatBuilder.setWhen(System.currentTimeMillis());
        //mCompatBuilder.setNumber(10);
        mCompatBuilder.setContentTitle(title);
        mCompatBuilder.setContentText( content );
        mCompatBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(content));
        //mCompatBuilder.setContentText(content);
        mCompatBuilder.setVisibility(Notification.VISIBILITY_PUBLIC);
        mCompatBuilder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);
        mCompatBuilder.setContentIntent(pendingIntent);
        mCompatBuilder.setAutoCancel(true);

        nm.notify(222, mCompatBuilder.build());
    }

    @Override
    public void run() {
        isRunThread = true;

        while(true)
        {
            try
            {
                userNotiSetting = mDBManager.getUserNotiSetting();
                Log.d(TAG, "ALARM CHECK " + userNotiSetting.getAlarmtime() );

                Calendar c = Calendar.getInstance();
                int h = c.get(Calendar.HOUR_OF_DAY);
                int m = c.get(Calendar.MINUTE);

                Log.d( TAG , h + ":" + m  );

                //활동 독려를 확인한다.
                if( userNotiSetting.isFitmeterwearnoti() )
                {
                    Calendar time = Calendar.getInstance();
                    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
                    try {
                        time.setTime(sdf.parse(userNotiSetting.getAlarmtime()));
                    } catch (ParseException e) {
                        e.printStackTrace();
                        time = null;
                    }

                    Log.d(TAG, "ALARM HOUR " + time.get(Calendar.HOUR_OF_DAY));
                    Log.d(TAG, "ALARM MIN " + time.get(Calendar.MINUTE));

                    if (h == time.get(Calendar.HOUR_OF_DAY) && m == time.get(Calendar.MINUTE)) {

                        ExerciseProgram appliedExerciseProgram = this.mDBManager.getAppliedExerciseProgram();
                        UserInfo userInfo = this.mDBManager.getUserInfo();
                        UserInfoForAnalyzer userInfoForAnalyzer = com.fitdotlife.fitmate_lib.service.util.Utils.getUserInfoForAnalyzer(userInfo);
                        //Analyzer를 생성한다.
                        ExerciseAnalyzer exerciseAnalyzer = new ExerciseAnalyzer(appliedExerciseProgram, userInfoForAnalyzer, WearingLocation.WAIST , 10);
                        StrengthInputType strengthType = appliedExerciseProgram.getStrengthInputType();


                        UserNotiSetting userNotiSetting = mDBManager.getUserNotiSetting();
                        String dayStartMessae = null;
                        if (strengthType.equals(StrengthInputType.CALORIE)) {
                            dayStartMessae = String.format(this.getString(R.string.alarm_today_start_calorie), appliedExerciseProgram.getStrength_From());
                        } else if (strengthType.equals(StrengthInputType.CALORIE_SUM)) {
                            dayStartMessae = String.format(this.getString(R.string.alarm_today_start_calorie), (int) appliedExerciseProgram.getStrength_From() / 7);
                        } else if (strengthType.equals(StrengthInputType.VS_BMR)) {
                            int bmr = exerciseAnalyzer.getUserInfo().getBMR();
                            dayStartMessae = String.format(this.getString(R.string.alarm_today_start_calorie), (int) (bmr * (appliedExerciseProgram.getStrength_From() / 100)));
                        } else {
                            dayStartMessae = String.format(this.getString(R.string.alarm_today_start_time), appliedExerciseProgram.getMinutes_From());
                        }

                        showNotification("Fitmate", dayStartMessae, dayStartMessae);

                        int weekDay = c.get(Calendar.DAY_OF_WEEK);
                        if (weekDay == Calendar.SUNDAY) {
                            String weekStartMessae = null;
                            if (strengthType.equals(StrengthInputType.CALORIE)) {
                                weekStartMessae = String.format(this.getString(R.string.alarm_week_start_calorie), appliedExerciseProgram.getStrength_From() * appliedExerciseProgram.getTimes_From());
                            } else if (strengthType.equals(StrengthInputType.CALORIE_SUM)) {
                                weekStartMessae = String.format(this.getString(R.string.alarm_week_start_calorie), appliedExerciseProgram.getStrength_From());
                            } else if (strengthType.equals(StrengthInputType.VS_BMR)) {
                                int bmr = exerciseAnalyzer.getUserInfo().getBMR();

                                weekStartMessae = String.format(this.getString(R.string.alarm_week_start_calorie), (int) (bmr * (appliedExerciseProgram.getStrength_From() / 100) * 7));
                            } else {
                                weekStartMessae = String.format(this.getString(R.string.alarm_week_start_time), appliedExerciseProgram.getTimes_From(), appliedExerciseProgram.getTimes_From() * appliedExerciseProgram.getMinutes_From());
                            }


                            showNotification("Fitmate", weekStartMessae, weekStartMessae);


                        }
                    }
                }

                //오후 6시에 목표가 미달성이면
                if( h == 18 && m == 0 ){
                    Date todayDate = new Date();
                    String strTodayDate = DateUtils.getDateString_yyyy_MM_dd(todayDate);
                    DayActivity todayActivity = mDBManager.getDayActivity( strTodayDate );

                    //오늘 날짜 데이터가 없으면
                    if( todayActivity == null ) return;

                    //주간 목표 확인
                    Calendar weekCalendar = Calendar.getInstance();
                    int dayOfWeek = weekCalendar.get(Calendar.DAY_OF_WEEK);
                    weekCalendar.add(Calendar.DATE, -(dayOfWeek - 1));
                    String weekStartDate = DateUtils.getDateString( weekCalendar );
                    WeekActivity weekActivity = mDBManager.getWeekActivity(weekStartDate);

                    if( weekActivity.getScore() >= 100 )
                    {
                        //주간 목표를 달성했으면 오늘 운동 목표에 대한 푸쉬를 보내지 않는다.
                        return;
                    }

                    float goalValue = 0;
                    //운동 프로그램을 가져온다.
                    com.fitdotlife.fitmate_lib.object.ExerciseProgram program = mDBManager.getAppliedProgram();
                    //운동프로그램 종류를 확인한다.
                    StrengthInputType strengthInputType = StrengthInputType.values()[ program.getCategory() ];

                    if( strengthInputType== StrengthInputType.CALORIE|| strengthInputType == StrengthInputType.CALORIE_SUM || strengthInputType == StrengthInputType.VS_BMR)
                    {
                        if( strengthInputType.equals( StrengthInputType.CALORIE) ){
                            goalValue = program.getStrengthFrom();
                        }else if(strengthInputType.equals( StrengthInputType.CALORIE_SUM )){
                            goalValue = program.getStrengthFrom() / 7;
                        }else{
                            UserInfoForAnalyzer userInfoForAnalyzer = Utils.getUserInfoForAnalyzer( mDBManager.getUserInfo() );
                            goalValue =  ( userInfoForAnalyzer.getBMR() * program.getStrengthFrom() ) * 100;
                        }

                    }else{
                        goalValue = program.getMinuteFrom();
                    }

                    int achieveOfTarget = todayActivity.getAchieveOfTarget();
                    int achievePercent = (int) (( achieveOfTarget / goalValue ) * 100);
                    ActivityDataCalculator.DayAchieveType dayAchieveType = ActivityDataCalculator.DayAchieveType.values()[getDayAchieve(strTodayDate) ];

                    if( achievePercent == 0 ){

                        if( !dayAchieveType.equals( ActivityDataCalculator.DayAchieveType.DAY_ACHIEVE_NO ) )
                        {
                            showNotification("Fitmate", mContext.getString(R.string.noti_day_achieve_no), mContext.getString(R.string.noti_day_achieve_no));
                        }

                    }else{

                        if( achievePercent < 30 ){

                            if( !dayAchieveType.equals( ActivityDataCalculator.DayAchieveType.DAY_ACHIEVE_29 ) ) {

                                String achieveMessage = String.format( mContext.getString( R.string.noti_day_achieve_below_30 ) , achievePercent );
                                showNotification("Fitmate", achieveMessage, achieveMessage);
                            }

                        }
                    }
                }

                //##### 배터리가 10%이하로 떨어질 때
                if( userNotiSetting.isBatteryusagenoti() ) {
                    int batteryRatio = mDBManager.getLastBatteryRatio(mDBManager.getUserInfo().getEmail());
                    if (batteryRatio <= 10 && batteryRatio!=-1) {
                        if (!alreadyBatteryNotification) {

                            showNotification("Fitmate", this.getString(R.string.alarm_battery_low), this.getString(R.string.alarm_battery_low));
                            this.alreadyBatteryNotification = true;

                        }
                    } else if (batteryRatio > 10) {
                        alreadyBatteryNotification = false;
                    }
                }

                //###### 휘트미터 미사용 경고
                //휘트미터 미사용 경고가 설정되어 있는지 확인한다.
                if(userNotiSetting.isFitmeternotusednoti()) {
                    if (isConnectionTimeout()) {
                        showNotification("Fitmate", this.getString(R.string.myprofile_not_connect_eight_hour), this.getString(R.string.myprofile_connecting_device));
                        UserInfo.saveLastAlarmInSharedPreference(mContext, false);
                    }
                }

                Log.d(TAG, "ALARM SLEEP");
                //Thread.sleep(60*1000);  // 1분 sleep
                Thread.sleep(60*1000);  // 1분 sleep
            }
            catch (InterruptedException ie)
            {
                break;
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
        }
        isRunThread = false;
    }

    private boolean isSendWeekAchievePush( String weekStartDate  ){
        SharedPreferences pref = mContext.getSharedPreferences("fitmate", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        return pref.getBoolean(ActivityDataCalculator.WEEK_ACHIEVE_KEY + "_" + weekStartDate, false);
    }

    private void setSendWeekAchievePush( String weekStartDate , boolean isSetWeekAchieve   ){
        SharedPreferences pref = mContext.getSharedPreferences("fitmate", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(ActivityDataCalculator.WEEK_ACHIEVE_KEY + "_" + weekStartDate, isSetWeekAchieve);
        editor.commit();
    }

    private int getDayAchieve( String dayDate  ){
        SharedPreferences pref = mContext.getSharedPreferences("fitmate", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        return pref.getInt(ActivityDataCalculator.DAY_ACHIEVE_KEY + "_" + dayDate, 0 );
    }

    private void setDayAchieve( String dayDate , int dayAchieve   ){
        SharedPreferences pref = mContext.getSharedPreferences("fitmate", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(ActivityDataCalculator.DAY_ACHIEVE_KEY + "_" + dayDate, dayAchieve );
        editor.commit();
    }

}