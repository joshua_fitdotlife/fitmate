package com.fitdotlife.fitmate_lib.service.protocol;


import com.fitdotlife.fitmate_lib.service.protocol.type.CodeConstants;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class Encoder 
{   
	public static byte[] encoding(byte header, byte[] body)
    {
		byte checkSum = 0;
		
        ByteArrayOutputStream temp = null;
        
        try 
        {
	        temp = new ByteArrayOutputStream();
	        //PC_COMM_START 
	        temp.write(CodeConstants.PC_COMM_START);
	        
	        //header 
	        temp.write(byteEncoding(header));
	        checkSum += header;
	        
	        //body 
	        for (int i = 0; i < body.length; i++)
	        {
	            checkSum += body[i];
	            temp.write(byteEncoding(body[i]));
	        }
	        
	        //checkSum 
	        temp.write(byteEncoding(checkSum));
	        
	        //PC_COMM_END 
	        temp.write(CodeConstants.PC_COMM_END);
	        
        } 
        catch (IOException e)
        {
			e.printStackTrace();
		}
        
        return temp.toByteArray();
    }
	
	public static byte[] encoding(byte[] sendBytes)
	{
		byte checkSum = 0;
		
        ByteArrayOutputStream temp = null;
        
        try 
        {
	        temp = new ByteArrayOutputStream();
	        //PC_COMM_START
	        temp.write(CodeConstants.PC_COMM_START);
	        
	        //body 
	        for (int i = 0; i < sendBytes.length; i++)
	        {
	            checkSum += sendBytes[i];
	            temp.write(byteEncoding(sendBytes[i]));
	        }
	        
	        //checkSum 
	        temp.write(byteEncoding(checkSum));
	        
	        //PC_COMM_END 
	        temp.write(CodeConstants.PC_COMM_END);
	        
        } 
        catch (IOException e)
        {
			e.printStackTrace();
		}
        
        return temp.toByteArray();
	}
	
	public static byte[] encoding(byte source)
	{
		byte[] arrSource = new byte[]{source};
		return encoding(arrSource);
	}
	
	private static byte[] byteEncoding(byte source)
	{
		byte[] result;
		
        if (source == CodeConstants.PC_COMM_START)
        {
            result = new byte[]{CodeConstants.PC_COMM_EXCEPTION,CodeConstants.PC_COMM_EXP_START};
        }
        else if (source == CodeConstants.PC_COMM_END)
        {
        	result = new byte[]{CodeConstants.PC_COMM_EXCEPTION,CodeConstants.PC_COMM_EXP_END};
        }
        else if (source == CodeConstants.PC_COMM_EXCEPTION)
        {
        	result = new byte[]{CodeConstants.PC_COMM_EXCEPTION,CodeConstants.PC_COMM_EXP_EXP};
        }
        else
        {
        	result = new byte[]{source};
        }
        
        return result;
	}
}
