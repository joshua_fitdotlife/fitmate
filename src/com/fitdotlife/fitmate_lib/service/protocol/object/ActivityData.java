package com.fitdotlife.fitmate_lib.service.protocol.object;

import java.util.ArrayList;
import java.util.List;

public class ActivityData
{
    private List<Integer> mSvmList = new ArrayList<Integer>();
    private double mSaveInterval = 0;
    private TimeInfo mStartTime = null;
    private SystemInfo_Response systemInfo = null;

    private int mRawFileIndex = 0;
    private byte[] mRawData = null;

    public List<Integer> getSVMList(){
        return this.mSvmList;
    }
    public void setSVMList( List<Integer> svmList ){
        this.mSvmList = svmList;
    }
    public void setSVM( int svm ){ this.mSvmList.add( svm ) ; }

    public double getSaveInterval(){
        return this.mSaveInterval;
    }
    public void setSaveInterval( double saveInterval ){
        this.mSaveInterval = saveInterval;
    }

    public TimeInfo getStartTime(){
        return this.mStartTime;
    }
    public void setStartTime( TimeInfo startTime ){
        this.mStartTime = startTime;
    }

    public int getRawFileIndex(){ return this.mRawFileIndex; }
    public void setRawFileIndex( int rawFileIndex ){ this.mRawFileIndex = rawFileIndex;  }

    public byte[] getRawData(){ return this.mRawData; }
    public void setRawData( byte[] rawData ){
        this.mRawData = rawData;
    }

    public SystemInfo_Response getSystemInfoResponse() {
        return systemInfo;
    }

    public void setSystemInfoResponse(SystemInfo_Response systemInfo) {
        this.systemInfo = systemInfo;
    }

}
