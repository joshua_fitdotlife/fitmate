package com.fitdotlife.fitmate_lib.service.protocol.object;

public class AxisData 
{
	public static final int LENGTH = 5;
	
	private short X;
	private short Y;
	private short Z;
	
	public AxisData( byte[] arrAxisData )
	{
		this.setX( arrAxisData );
		this.setY( arrAxisData );
		this.setZ( arrAxisData );
	}
	
	public short getX()
	{
		 return this.X;
	}
	
	public short getY()
	{
		 return this.Y;
	}
	
	public short getZ()
	{
		 return this.Z;
	}
	
	private void setX(byte[] axisData)
	{	
		boolean negative = false;
		
		if( (axisData[0] & 0x08)  > 0 )
		{
			negative = true;
		}
		
		//상위 4비트 값 읽음
		int hvalue = (axisData[0] & 0x0F ) << 8;
		//하위 8비트 값 읽음
		int lvalue = (axisData[1] & 0xFF );
		
		if( negative )
		{
			this.X = (short) (hvalue + lvalue - 4096 );
		}
		else
		{
			this.X = (short) (hvalue + lvalue);
		}
	}
	
	private void setY(byte[] axisData)
	{
		
		boolean negative = false;
		
		if( (axisData[3] & 0x80)  > 0 )
		{
			negative = true;
		}
		
		//상위 8비트 값 읽음
        int hvalue = (axisData[2] & 0xFF ) << 4;
        //하위 4비트 값 읽음
        int lvalue = (axisData[3] & 0xF0 ) >> 4;
        
        if( negative )
		{
			this.Y = (short) (hvalue + lvalue - 4096 );
		}
		else
		{
			this.Y = (short) (hvalue + lvalue);
		}
        
        
        
	}
	
	private void setZ(byte[] axisData)
	{
		boolean negative = false;
		
		if( (axisData[3] & 0x08)  > 0 )
		{
			negative = true;
		}
		
        //상위 4비트값 읽음
        int hvalue = (axisData[3] & 0x0F) << 8;
        //하위 8비트 값 읽음
        int lvalue = (axisData[4] & 0xFF);
        
		if( negative )
		{
			this.Z = (short) (hvalue + lvalue - 4096 );
		}
		else
		{
			this.Z = (short) (hvalue + lvalue);
		}
	}
}
