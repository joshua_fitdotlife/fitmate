package com.fitdotlife.fitmate_lib.service.protocol.object;

import java.io.UnsupportedEncodingException;

/**
 * 시리얼 코드
 * @author Joshua
 *
 */
public class SerialCode 
{
	public static final int Length = 10;
	private byte[] arrSerialCode;
	
	/**
	 * 생성자
	 */
	public SerialCode()
	{
		this.arrSerialCode = new byte[Length];
	}
	
	/**
	 * 생성자
	 * @param serialCode
	 */
	public SerialCode(byte[] serialCode)
	{
		this.arrSerialCode = serialCode;
	}
	
	/**
	 * 바이트 배열을 반환한다.
	 * @return
	 */
	public byte[] getBytes()
	{
		return this.arrSerialCode;
	}
	
	public void setProductType(ProductType productType)
	{
		for(int i = 0 ; i < ProductType.Length ; i++)
		{
			this.arrSerialCode[i] = productType.getBytes()[i];
		}
	}
	
	public ProductType getProductType()
	{
		byte[] arrProductType = new byte[]{this.arrSerialCode[1],this.arrSerialCode[2],this.arrSerialCode[3],this.arrSerialCode[4]};
		return new ProductType(arrProductType);
	}
	
	public void setManufacturingDate(byte[] manufacturingDate)
	{
		for(int i = 0 ; i < 3 ; i++)
		{
			this.arrSerialCode[ i + 4 ] = manufacturingDate[i];
		}
	}
	
	public byte[] getManufacturingDate()
	{
		return new byte[]{this.arrSerialCode[4], this.arrSerialCode[5], this.arrSerialCode[6] };
	}
	
	public void setManufacturingOrder(byte[] manufacturingOrder)
	{
		for(int i = 0 ; i < 3 ; i++)
		{
			this.arrSerialCode[ i+ 7 ] = manufacturingOrder[i];
		}
	}
	
	public byte[] getManufacturingOrder()
	{
		return new byte[]{this.arrSerialCode[7], this.arrSerialCode[8], this.arrSerialCode[9]};
	}
	
	public String toString()
	{
		String result = null;
		try {
			result = new String(this.arrSerialCode,"UTF-8");
		} catch (UnsupportedEncodingException e) {}
		
		return result;
	}
}
