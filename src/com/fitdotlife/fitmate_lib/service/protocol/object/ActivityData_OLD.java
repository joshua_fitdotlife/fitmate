package com.fitdotlife.fitmate_lib.service.protocol.object;

import android.util.Base64;

import com.fitdotlife.fitmate_lib.service.util.Utils;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

public class ActivityData_OLD
{
	private final double MEDIUM_MET = 6;
	private final double LIGHT_MET = 3;
	private final double UNDERLIGHT_MET = 1.5;

	private TimeInfo activityDate = null;
	
	private TimeInfo timeFrom = null;
	
	private TimeInfo timeTo = null;
	
	private float totalMET = 0;
	private int mCount = 0;
	
	private int calorieByActivity = 0;
	
	private int[] arrHMLMet = new int[4];
	
	private int[] arrMet = new int[10];
	private float[] arrChartData = new float[24];
	
	private int[] arrHourCount = new int[24];
	private int[] arrHourCalorie = new int[24];
	
	public TimeInfo getActivityDate() {
		return activityDate;
	}
	public void setActivityDate( TimeInfo activityDate) {
		this.activityDate = activityDate;
	}
	
	public TimeInfo getTimeFrom() {
		return timeFrom;
	}
	
	public void setTimeFrom( TimeInfo timeFrom) {
		this.timeFrom = timeFrom;
	}
	
	public TimeInfo getTimeTo() {
		return timeTo;
	}
	public void setTimeTo( TimeInfo timeTo) {
		this.timeTo = timeTo;
	}
	
	public int getCalorie() {
		return calorieByActivity;
	}
	
	public void setCalorie(int calorie) {
		this.calorieByActivity = calorie;
	}
	
	public int getStrengthHard() {
		return this.arrHMLMet[3];
	}
	
	public void setStrengthHard( int strengthHard) {
		this.arrHMLMet[3] = strengthHard;
	}
	
	public int getStrengthMedium() {
		return this.arrHMLMet[2];
	}
	public void setStrengthMedium( int strengthMedium) { 
		this.arrHMLMet[2] = strengthMedium;
	}
	
	public int getStrengthLight() {
		return this.arrHMLMet[1];
	}
	public void setStrengthLight( int strengthLight) { 
		this.arrHMLMet[1] = strengthLight;
	}
	
	public int getStrengthUnderLight() {
		return this.arrHMLMet[0];
	}
	public void setStrengthUnderLight( int strengthUnderLight) { 
		this.arrHMLMet[0] = strengthUnderLight;
	}
	
	public int getMet1() {
		return this.arrMet[0];
	}
	public void setMet1( int met1) {
		this.arrMet[0] = met1; 
	}
	
	public int getMet2() {
		return this.arrMet[1];
	}
	public void setMet2( int met2) {
		this.arrMet[1]  = met2;
	}
	
	public int getMet3() {
		return this.arrMet[2];
	}
	public void setMet3( int met3) {
		this.arrMet[2]  = met3;
	}
	
	public int getMet4() {
		return this.arrMet[3];
	}
	public void setMet4( int met4) {
		this.arrMet[3]  = met4;
	}
	
	public int getMet5() {
		return this.arrMet[4];
	}
	public void setMet5( int met5) {
		this.arrMet[4] = met5; 
	}
	
	public int getMet6() {
		return this.arrMet[5];
	}
	public void setMet6( int met6) {
		this.arrMet[5]  = met6;
	}
	
	public int getMet7() {
		return this.arrMet[6];
	}
	public void setMet7( int met7) {
		this.arrMet[6]  = met7;
	}
	
	public int getMet8() {
		return this.arrMet[7];
	}
	public void setMet8( int met8) {
		this.arrMet[7]  = met8; 
	}
	
	public int getMet9() {
		return this.arrMet[8];
	}
	public void setMet9( int met9) {
		this.arrMet[8]  = met9;
	}
	
	public int getMet10() {
		return this.arrMet[9];
	}
	public void setMet10( int met10) {
		this.arrMet[9]  = met10;
	}
	
	public float[] getChartData()
	{
		return this.arrChartData;
	}
	public void setChartData( float[] chartData )
	{
		this.arrChartData = chartData;
	}
	
	public int[] getChartCalorie()
	{
		return this.arrHourCalorie;
	}
	public void setChartCalorie( int[] chartCalorie )
	{
		this.arrHourCalorie = chartCalorie;
	}
	
	public int[] getHourCount()
	{
		return this.arrHourCount;
	}
	public void setHoutCount( int[] hourCount )
	{
		this.arrHourCount = hourCount;
	}
	
	public int getDataCount()
	{
		return this.mCount;
	}
	
	public void setDataCount( int dataCount )
	{
		this.mCount = dataCount;
	}
	
	public float getTotalMet()
	{
		return this.totalMET;
	}
	
	public void setTotalMet( float totalMet )
	{
		this.totalMET = totalMet;
	}
	
	public void addActivityInfo( int calorie , float met , TimeInfo currentTime , int saveInterVal )
	{
		
		//입력 카운트 올리기
		this.mCount += 1;
		
		//소모칼로리 더하기
		this.calorieByActivity += calorie;
		
		//평균 MET를 구하기 위하여 MET 더하기
		this.totalMET += met;
		
		//저중고 강도 운동 시간 계산하기
		int hmlIndex = 0;
		
		if( met > this.MEDIUM_MET )
		{ hmlIndex = 3; }
		
		if( met <= this.MEDIUM_MET )
		{ hmlIndex = 2; }
		
		if( met <= this.LIGHT_MET )
		{ hmlIndex = 1;}
		
		if( met <= this.UNDERLIGHT_MET )
		{ hmlIndex = 0; }
		
		this.arrHMLMet[ hmlIndex ] += saveInterVal;
		
		//MET별 강도 운동시간 더하기
		int metIndex = 0;
		if( met >= 10  )
		{ metIndex = 9; }
		else
		{ metIndex = (int) (met - 1);}
		this.arrMet[ metIndex ] += saveInterVal;
		
		//시간별 평균 MET를 계산하기 위해 시간별 MET 합산과 입력카운트를 더한다.
		this.arrChartData[ currentTime.getHour() ] += met;
		this.arrHourCount[ currentTime.getHour()  ] += 1;
		
		this.arrHourCalorie[ currentTime.getHour() ] += calorie;
	}
	
	public void add( ActivityData_OLD addActivityDataOLDOLD)
	{
		// 끝나는 시간 
		this.timeFrom = addActivityDataOLDOLD.getTimeFrom();
		
		//칼로리 합치기
		this.calorieByActivity += addActivityDataOLDOLD.getCalorie();
		
		//평균 MET 합치기
		this.totalMET += addActivityDataOLDOLD.getTotalMet();
		this.mCount += addActivityDataOLDOLD.getDataCount();
		
		//저중고강도 합치기
		this.arrHMLMet[0] += addActivityDataOLDOLD.getStrengthUnderLight();
		this.arrHMLMet[1] += addActivityDataOLDOLD.getStrengthLight();
		this.arrHMLMet[2] += addActivityDataOLDOLD.getStrengthMedium();
		this.arrHMLMet[3] += addActivityDataOLDOLD.getStrengthHard();
		
		//MET별 운동시간 합치기
		this.arrMet[0] += addActivityDataOLDOLD.getMet1();
		this.arrMet[1] += addActivityDataOLDOLD.getMet2();
		this.arrMet[2] += addActivityDataOLDOLD.getMet3();
		this.arrMet[3] += addActivityDataOLDOLD.getMet4();
		this.arrMet[4] += addActivityDataOLDOLD.getMet5();
		this.arrMet[5] += addActivityDataOLDOLD.getMet6();
		this.arrMet[6] += addActivityDataOLDOLD.getMet7();
		this.arrMet[7] += addActivityDataOLDOLD.getMet8();
		this.arrMet[8] += addActivityDataOLDOLD.getMet9();
		this.arrMet[9] += addActivityDataOLDOLD.getMet10();
		
		//시간별 평균 MET와 시간별 소모칼로리 합치기
		float[] addChartData = addActivityDataOLDOLD.getChartData();
		int[] addChartCalorie = addActivityDataOLDOLD.getChartCalorie();
		int[] addHourCount = addActivityDataOLDOLD.getHourCount();
		for( int i = 0 ; i < this.arrChartData.length ;i++ )
		{
			//시간별 평균 MET 합치기
			this.arrChartData[i] += addChartData[i];
			this.arrHourCount[i] += addHourCount[i];
			
			//시간별 소모칼로리 합치기
			this.arrHourCalorie[i] = this.arrHourCalorie[ i ] + addChartCalorie[ i ];
		}
	}
	
	@Override
	public String toString()
	{
//		StringBuffer messageBuffer = new StringBuffer();
//		//기록날짜
//		messageBuffer.append( UserLoginActivity.ROW_SEPERATOR );
//		messageBuffer.append( this.activityDate.getDateString_yyyy_MM_dd() + UserLoginActivity.COL_SEPERATOR );
//
////		//기록시작시간
////		messageBuffer.append( this.timeFrom.getTimeString() + UserLoginActivity.COL_SEPERATOR );
////		//기록종료시간
////		messageBuffer.append( this.timeTo.getTimeString() + UserLoginActivity.COL_SEPERATOR );
////		//평균MET
////		float averageMet = this.totalMET / this.mCount ;
////		messageBuffer.append( averageMet + UserLoginActivity.COL_SEPERATOR );
////		//칼로리
////		messageBuffer.append( ( ( this.calorieByActivity + 500 ) / 1000 ) + UserLoginActivity.COL_SEPERATOR );
////
////		//저중고강도 운동시간
////		for( int i = 0 ; i < arrHMLMet.length ;i++ )
////		{
////			messageBuffer.append( Math.round(this.arrHMLMet[i] / 60) + UserLoginActivity.COL_SEPERATOR );
////		}
//
//		//시간별 소모칼로리
//		messageBuffer.append( chartCalorieToString( this.arrHourCalorie ) + UserLoginActivity.COL_SEPERATOR );
//
//		//시간별 평균 MET
//		messageBuffer.append( chartDataToString( this.arrChartData , this.arrHourCount) + UserLoginActivity.COL_SEPERATOR );
//
//		//MET별 운동시간
//		for( int i = 0 ; i < arrMet.length ;i++ )
//		{
//			messageBuffer.append( Math.round(this.arrMet[i] / 60) );
//
//			if( i != arrMet.length - 1 )
//			{
//				messageBuffer.append( UserLoginActivity.SUB_SEPERATOR );
//			}
//		}
//
//		messageBuffer.append( UserLoginActivity.COL_SEPERATOR  + UserLoginActivity.COL_SEPERATOR);
//		return messageBuffer.toString();

        return null;
	}
	
	
	private String chartDataToString( float[] chartData , int[] hourCount )
	{
		byte[] tempChart = new byte[48];
		
		for( int i = 0 ; i < hourCount.length ;i++ )
		{
			float averageMet = 0;
			
			//평균 구하기
			if( hourCount[ i ]  == 0 )
			{
				averageMet = 0;
			}
			else
			{						
				averageMet = ( chartData[ i ] / hourCount[ i ] );
			}
			
			averageMet = (float) (Math.round(averageMet * 100d) / 100d);
			
			tempChart[ 2 * i ] = (byte) ( (byte) ( Math.floor(averageMet) ) & 0xFF) ;
			tempChart[ 2 * i + 1 ] = (byte) ((byte) Math.round((averageMet - tempChart[2 * i]) * 100) & 0xFF);
		}
		
		String result = Base64.encodeToString(tempChart, Base64.NO_WRAP);
		tempChart = null;
		
		return result;
	}
	
	 private String chartCalorieToString( int[] chartCalorie )
	{
		byte[] tempChart = new byte[48];
		for( int i = 0 ; i < chartCalorie.length ;i++ )
		{	
			//kcal로 만들기
			int kCalorie = ( chartCalorie[i] + 500 ) / 1000;
			
			byte[] tempBytes = Utils.IntToByteArray(kCalorie);
			tempChart[ 2 * i ] = tempBytes[3];
			tempChart[ 2 * i + 1 ] = tempBytes[2];
		}
		
		return Base64.encodeToString(tempChart, Base64.NO_WRAP);
	}
	
	public static float[] convertByteArrayToFloatArray( byte[]  byteArray )
	{
		float floatArray[] = new float[byteArray.length/4]; 
		ByteBuffer byteBuf = ByteBuffer.wrap(byteArray);
		FloatBuffer floatBuf = byteBuf.asFloatBuffer();
		floatBuf.get (floatArray); 
		return floatArray; 
	}
	
	public static byte[] convertFloatArrayToByteArray( float[] floatArray )
	{
		ByteBuffer byteBuffer = ByteBuffer.allocate(4 * floatArray.length);
		FloatBuffer floatBuffer =  byteBuffer.asFloatBuffer();
		floatBuffer.put(floatArray);
		return byteBuffer.array();
	}
	
	public static int[] convertByteArrayToIntArray( byte[] byteArray )
	{
		int intArray[] = new int[byteArray.length/4]; 
		ByteBuffer byteBuf = ByteBuffer.wrap(byteArray);
		IntBuffer intBuf = byteBuf.asIntBuffer();
		intBuf.get (intArray);
		return intArray;
	}
	
	public static byte[] convertIntArrayToByteArray( int[] intArray )
	{
		ByteBuffer byteBuffer = ByteBuffer.allocate(4 * intArray.length);
		IntBuffer intBuffer =  byteBuffer.asIntBuffer();
		intBuffer.put(intArray);
		return byteBuffer.array();
	}
}
