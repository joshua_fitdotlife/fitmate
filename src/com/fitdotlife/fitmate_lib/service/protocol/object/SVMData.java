package com.fitdotlife.fitmate_lib.service.protocol.object;

import java.util.Date;

public class SVMData
{
	private double activity;
	private Date currentTime;
	private boolean endOfFile = false;
	
	public boolean isEndOfFile() {
		return endOfFile;
	}
	
	public void setEndOfFile(boolean endOfFile) 
	{
		this.endOfFile = endOfFile;
	}

	public void setActivity( double act )
	{
		this.activity = act;
	}
	
	public double getActivity()
	{
		return this.activity;
	}
	
	public void setCurrentTime( Date cTime )
	{
		this.currentTime = cTime;
	}
	
	public Date getCurrentTime()
	{
		return this.currentTime;
	}
}
