package com.fitdotlife.fitmate_lib.service.protocol.object;

/**
 * Created by Joshua on 2015-02-02.
 */
public interface NextDayListener {

    public void onNextDay();

}
