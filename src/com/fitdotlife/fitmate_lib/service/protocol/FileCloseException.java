package com.fitdotlife.fitmate_lib.service.protocol;

public class FileCloseException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String message;
	
	public FileCloseException( String message )
	{
		this.message = message;
	}

	@Override
	public String getMessage() {

		return this.message;
	}

}
