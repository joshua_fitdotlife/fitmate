package com.fitdotlife.fitmate_lib.service.protocol.type;

public class CodeConstants {
	
    public static byte PC_COMM_START 		= (byte)0xf5;
    public static byte PC_COMM_END 			= (byte)0xfa;
    public static byte PC_COMM_EXCEPTION 	= (byte)0xf3;
    public static byte PC_COMM_EXP_START 	= 0;
    public static byte PC_COMM_EXP_END 	= 1;
    public static byte PC_COMM_EXP_EXP 		= 2;
	
}
