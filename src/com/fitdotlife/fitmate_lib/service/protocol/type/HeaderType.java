package com.fitdotlife.fitmate_lib.service.protocol.type;

public class HeaderType
{
	//##### COMMON REQUEST MESSAGE HEADER TYPE #####
	//Sync(Health Check)
	public static final byte SYNC 										= 0x00;
	//시스템 정보 설정
	public static final byte REQ_SET_SYSTEMINFO 					= 0x01;
	//시간 정보 설정
	public static final byte REQ_SET_TIMEINFO 						= 0x02;
	//시스템 정보 조회
	public static final byte REQ_GET_SYSTEMINFO					= 0x03;
	//시간 조회
	public static final byte REQ_GET_TIMEINFO 						= 0x04;
	//배터리 잔량 조회
	public static final byte REQ_GET_BATTERY							= 0x05;
	//시리얼 코드 설정
	public static final byte REQ_SET_SERIALCODE						= 0x06;
	//데이터 정보 요청
	public static final byte REQ_GET_SENSEDATA_INFO				= 0x07;
	//데이터 삭제 요청
	public static final byte REQ_DEL_SENSEDATA						= 0x08;
	//데이터 전송 요청
	public static final byte REQ_TRANS_SENSEDATA 				= 0x09;
    //3축센서 보정 값 조회
    public static final byte REQ_GET_ACCELERO_CORRECTION 	= 0x0A;
    //3축 센서 보정 값 설정
    public static final byte REQ_SET_ACCELERO_CORRECTION		= 0x0B;
    //조도 보정 값 조회
    public static final byte REQ_GET_ILLUMINACE_CORRECTION	= 0x0C;
    //조도 보정 값 설정
    public static final byte REQ_SET_ILLUMINACE_CORRECTION	= 0x0D;
    //사용자 정보 조회
    public static final byte REQ_GET_USERINFO						= 0x0E;
    //사용자 정보 설정
    public static final byte REQ_SET_USERINFO						= 0x0F;
    //데이터 싱크 시작
    public static final byte REQ_START_DATASYNC					= 0x10;
    //데이터 싱크 끝
    public static final byte REQ_END_DATASYNC						= 0x11;
    //Format
    public static final byte REQ_FORMAT								= 0x20;
    //Number Of File
    public static final byte REQ_NUMBER_OF_FILE					= 0x21;
    //File Info
    public static final byte REQ_FILE_INFO								= 0x22;
    //File Open
    public static final byte REQ_FILE_OPEN							= 0x23;
    //File Read
    public static final byte REQ_FILE_READ								= 0x24;
    //File Close
    public static final byte REQ_FILE_CLOSE							= 0x25;
    //Delete All File
    public static final byte REQ_DELETE_ALL_FILE						= 0x26;
    //Next Data
    public static final byte REQ_NEXT_BLOCK 						= 0x40;
    // Blink LED
    public static final byte REQ_ON_LED                             = (byte)0xA3;

    public static final byte REQ_OFF_LED                            = (byte)0xA8;

	//##### COMMON RESPONSE MESSAGE HEADER TYPE #####
	//성공
	public static final byte RES_SUCCESS								= 0x00;
	//실패
	public static final byte RES_FAIL										= 0x01;
	//시스템 정보
	public static final byte RES_SYSTEMINFO							= 0x02;
    //배터리 잔량
    public static final byte RES_BATTERY								= 0x03;
    //시간 정보
    public static final byte RES_TIMEINFO								= 0x04;
    //사용자 정보
    public static final byte RES_USERINFO								     = 0x05;
    //3축 센서 보정 값
    public static final byte RES_ACCELERO_CORRECTION 			       = 0x06;
    //조도 보정값
    public static final byte RES_ILLUMINANCE_CORRECTION		           = 0x07;
    //NumberOfFile
    public static final byte RES_NUMBER_OF_FILE						= 0x21;
    //FileInfo
    public static final byte RES_FILE_INFO  							= 0x22;
    //FileOpen
    public static final byte RES_FILE_OPEN								= 0x23;
    //File Read
    public static final byte RES_FILE_READ								= 0x24;
    //File Close
    public static final byte RES_FILE_CLOSE							= 0x25;
    //Format Status
    public static final byte RES_FORMAT_STATUS						= 0x26;
    //Delete Status
    public static final byte RES_DELETE_STATUS						= 0x27;

    //###
    public static final byte REQ_GET_MASS_DATA                        = (byte)0xB0;


}
