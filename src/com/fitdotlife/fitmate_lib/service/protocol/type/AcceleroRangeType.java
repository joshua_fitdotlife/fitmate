package com.fitdotlife.fitmate_lib.service.protocol.type;

public class AcceleroRangeType {
	 public static final byte GRAVITY_2 	= 0x01; 
	 public static final byte GRAVITY_4 	= 0x02;
	 public static final byte GRAVITY_8 	= 0x03;
	 public static final byte GRAVITY_16 	= 0x04;
}
