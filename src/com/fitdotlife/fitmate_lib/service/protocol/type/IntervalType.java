package com.fitdotlife.fitmate_lib.service.protocol.type;

public class IntervalType {
	 public static final byte NOTSENSING 		= 0x00;
	 
	 public static final byte SECOND_1 		= 0x01;
	 public static final byte SECOND_5 		= 0x02;
	 public static final byte SECOND_10 		= 0x03;
	 public static final byte SECOND_30 		= 0x04;
	 public static final byte SECOND_60 		= 0x05;
	 
	 public static final byte HERTZ_2			= 0x06;
	 public static final byte HERTZ_4			= 0x07;
	 public static final byte HERTZ_8			= 0x08;
	 public static final byte HERTZ_16			= 0x09;
	 public static final byte HERTZ_32			= 0x0A;
	 public static final byte HERTZ_64			= 0x0B;
	 public static final byte HERTZ_128			= 0x0C;
	 public static final byte HERTZ_256			= 0x0D;
	 public static final byte HERTZ_512			= 0x0E;
	 public static final byte HERTZ_1024		= 0x0F;
}
