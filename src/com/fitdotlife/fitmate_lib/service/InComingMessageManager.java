package com.fitdotlife.fitmate_lib.service;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.key.GenderType;
import com.fitdotlife.fitmate_lib.object.ResponseDayActivity;
import com.fitdotlife.fitmate_lib.object.ResponseMonthActivity;
import com.fitdotlife.fitmate_lib.object.ResponseWeekActivity;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.service.database.FitmateServiceDBManager;
import com.fitdotlife.fitmate_lib.service.key.MessageType;
import com.fitdotlife.fitmate_lib.service.key.SyncType;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Joshua on 2015-01-28.
 */
public class InComingMessageManager extends Handler implements FitmateServiceStarter.DataSyncCallback, FitmateServiceStarter.BatteryStatusCallback{

    private final String TAG = "fitmateservice";
    private final String SUCCESS = "SUCCESS";
    private final String FAIL = "FAIL";
    private final String NONE = "NONE";

    private final String RESULT_KEY = "result";
    private final String CLIENT_NUMBER_KEY = "clientnumber";

    private Context mContext = null;

    private FitmateDBManager mDBManager = null;
    private Messenger mResponseMessenger = null;
    private FitmateServiceStarter mStarter = null;
    private List<ClientInfo> mClients = new ArrayList<ClientInfo>();

    private int clientIndex = 0;
    private int clientNumber = 0;

    public InComingMessageManager( Context context , FitmateDBManager dbManager )
    {
    	this.mContext = context;
        this.mDBManager = dbManager;
    }
    
    @Override
    public void handleMessage(Message msg) {
        super.handleMessage(msg);

        switch( msg.what ) {
            case MessageType.MSG_DELETE_ALLFILES:
                FitmateServiceDBManager fitmateServiceDBManager = new FitmateServiceDBManager(this.mContext);
                fitmateServiceDBManager.deleteAllFiles();
                break;
            case MessageType.MSG_REGISTER_CLIENT:
                Messenger responseMesseger = msg.replyTo;
                ClientInfo registerClientInfo = new ClientInfo();
                clientIndex += 1;
                registerClientInfo.clientNumber = clientIndex;
                registerClientInfo.messenger = responseMesseger;
                this.mClients.add(registerClientInfo);

                Message responseRegisterClientMessage = Message.obtain(null , msg.what);
                Bundle responseRegisterClientBundle = new Bundle();
                responseRegisterClientBundle.putInt(this.CLIENT_NUMBER_KEY , registerClientInfo.clientNumber );
                responseRegisterClientMessage.setData(responseRegisterClientBundle);

                try {
                    responseMesseger.send(responseRegisterClientMessage);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }

                Log.d(TAG, "클라이언트 등록 , 인덱스 =" +  clientIndex);
                break;
            case MessageType.MSG_UNREGISTER_CLIENT:

                Bundle requestUnregisterClientBundle = msg.getData();
                clientNumber = requestUnregisterClientBundle.getInt(this.CLIENT_NUMBER_KEY);

                this.removeResponseMessenger(clientNumber);

                Log.d(TAG, "클라이언트 해제");
                break;
            case MessageType.MSG_SET_USERINFO:
                Bundle userInfoBundle = msg.getData();
                clientNumber = userInfoBundle.getInt(this.CLIENT_NUMBER_KEY);
                UserInfo userInfo = new UserInfo();
                userInfo.setGender(GenderType.getGenderType(userInfoBundle.getInt(UserInfo.SEX_KEY)));
                userInfo.setHeight( userInfoBundle.getInt(UserInfo.HEIGHT_KEY) );
                userInfo.setWeight( userInfoBundle.getInt(UserInfo.WEIGHT_KEY) );
                userInfo.setDeviceAddress( userInfoBundle.getString(UserInfo.DEVICE_ADDRESS_KEY ) );

                this.mResponseMessenger = this.getResponseMessenger(clientNumber);

                Message responseUserInfoMessage = Message.obtain(null, msg.what);
                Bundle responseUserInfoBundle = new Bundle();

                this.mDBManager.setUserInfo( userInfo );

                String result = this.SUCCESS;
                responseUserInfoBundle.putString(this.RESULT_KEY, result);
                responseUserInfoMessage.setData(responseUserInfoBundle);

                try {
                    this.mResponseMessenger.send(responseUserInfoMessage);
                } catch (RemoteException e) {
                    //TODO 에러 처리
                }

                Log.d(TAG, "사용자 등록");
                break;

            case MessageType.MSG_GET_DAY_ACTIVITY:

                Bundle requestDayActivityBundle = msg.getData();
                clientNumber = requestDayActivityBundle.getInt(this.CLIENT_NUMBER_KEY);
                String dayActivityDate = requestDayActivityBundle.getString(ResponseDayActivity.DAY_ACTIVITY_DATE_KEY);

                this.mResponseMessenger = this.getResponseMessenger(clientNumber);

                ResponseDayActivity dayActivity = this.mDBManager.getResponseDayActivity(dayActivityDate);
                Message responseDayActivityMessage = Message.obtain(null, msg.what);
                Bundle responseDayActivityBundle = new Bundle();

                if (dayActivity != null) {

                    responseDayActivityBundle.putString(this.RESULT_KEY, this.SUCCESS);
                    responseDayActivityBundle.putString(ResponseDayActivity.DAY_ACTIVITY_DATE_KEY, dayActivity.getActivityDate());
                    responseDayActivityBundle.putDouble(ResponseDayActivity.DAY_ACTIVITY_CALORIE_KEY, dayActivity.getCalorie());
                    responseDayActivityBundle.putInt(ResponseDayActivity.DAY_ACTIVITY_STRENGTH_HIGH_KEY, dayActivity.getStrengthHigh());
                    responseDayActivityBundle.putInt(ResponseDayActivity.DAY_ACTIVITY_STRENGTH_MEDIUM_KEY, dayActivity.getStrengthMedium());
                    responseDayActivityBundle.putInt(ResponseDayActivity.DAY_ACTIVITY_STRENGTH_LOW_KEY, dayActivity.getStrengthLow());
                    responseDayActivityBundle.putInt(ResponseDayActivity.DAY_ACTIVITY_STRENGTH_UNDER_LOW_KEY, dayActivity.getStrengthUnderLow());
                    //responseDayActivityBundle.putByteArray(ResponseDayActivity.DAY_ACTIVITY_MET_ARRAY_KEY, dayActivity.getMetArray());
                    responseDayActivityBundle.putInt(ResponseDayActivity.DAY_ACTIVITY_WEEK_SCORE_KEY, dayActivity.getWeekScore());
                    responseDayActivityBundle.putInt(ResponseDayActivity.DAY_ACTIVITY_ACHIEVE_TARGET_KEY, dayActivity.getAchieveOfTarget());

                } else {
                    responseDayActivityBundle.putString(this.RESULT_KEY, this.NONE);
                }

                responseDayActivityMessage.setData(responseDayActivityBundle);
                try {
                    mResponseMessenger.send(responseDayActivityMessage);
                } catch (RemoteException e) {
                    //TODO 에러 처리
                }

                Log.d(TAG , "일 활동량 요청 =" + clientNumber );
                break;
            case MessageType.MSG_GET_WEEK_ACTIVITY:

                Bundle requestWeekActivityBundle = msg.getData();
                clientNumber = requestWeekActivityBundle.getInt(this.CLIENT_NUMBER_KEY);
                String weekActivityDate = requestWeekActivityBundle.getString(ResponseWeekActivity.WEEK_ACTIVITY_DATE_KEY);

                this.mResponseMessenger = this.getResponseMessenger(clientNumber);

                ResponseWeekActivity weekActivity = this.mDBManager.getResponseWeekActivity(weekActivityDate);
                Message responseWeekActivityMessage = Message.obtain(null, msg.what);
                Bundle responseWeekActivityBundle = new Bundle();

                if (weekActivity != null) {
                    responseWeekActivityBundle.putString(this.RESULT_KEY, this.SUCCESS);
                    responseWeekActivityBundle.putString(ResponseWeekActivity.WEEK_ACTIVITY_DATE_KEY, weekActivity.getActivityDate());
                    responseWeekActivityBundle.putInt(ResponseWeekActivity.WEEK_ACTIVITY_SCORE_KEY, weekActivity.getScore());
                    responseWeekActivityBundle.putInt(ResponseWeekActivity.WEEK_ACTIVITY_AVERAGE_STRENGTH_HIGH_KEY, weekActivity.getAverageStrengthHigh());
                    responseWeekActivityBundle.putInt(ResponseWeekActivity.WEEK_ACTIVITY_AVERAGE_STRENGTH_MEDIUM_KEY, weekActivity.getAverageStrengthMedium());
                    responseWeekActivityBundle.putInt(ResponseWeekActivity.WEEK_ACTIVITY_AVERAGE_STRENGTH_LOW_KEY, weekActivity.getAverageStrengthLow());
                    responseWeekActivityBundle.putIntArray(ResponseWeekActivity.WEEK_ACTIVITY_EXERCISE_TIME_LIST_KEY, weekActivity.getExerciseTimeArray());
                } else {
                    responseWeekActivityBundle.putString(this.RESULT_KEY, this.NONE);
                }

                responseWeekActivityMessage.setData(responseWeekActivityBundle);
                try {
                    mResponseMessenger.send(responseWeekActivityMessage);

                } catch (RemoteException e) {
                    //TODO 에러 처리
                }

                Log.d(TAG , "주 활동량 요청 =" + clientNumber );
                break;
            case MessageType.MSG_GET_MONTH_ACTIVITY:
                Bundle requestMonthActivityBundle = msg.getData();
                clientNumber = requestMonthActivityBundle.getInt(this.CLIENT_NUMBER_KEY);
                String monthActivityDate = requestMonthActivityBundle.getString(ResponseMonthActivity.MONTH_ACTIVITY_ACTIVITY_DATE_KEY);

                this.mResponseMessenger = this.getResponseMessenger(clientNumber);

                ResponseMonthActivity monthActivity = this.mDBManager.getResponseMonthActivity( monthActivityDate );
                Message responseMonthActivityMessage = Message.obtain(null, msg.what);
                Bundle responseMonthActivityBundle = new Bundle();

                if (monthActivity != null) {
                    responseMonthActivityBundle.putString(this.RESULT_KEY, this.SUCCESS);
                    responseMonthActivityBundle.putString(ResponseMonthActivity.MONTH_ACTIVITY_ACTIVITY_DATE_KEY, monthActivity.getActivityDate());
                    responseMonthActivityBundle.putInt(ResponseMonthActivity.MONTH_ACTIVITY_AVERAGE_SCORE_KEY, monthActivity.getAverageScore());
                    responseMonthActivityBundle.putIntArray(ResponseMonthActivity.MONTH_ACTIVITY_SCORE_LIST_KEY, monthActivity.getScoreArray());
                    responseMonthActivityBundle.putIntArray(ResponseMonthActivity.MONTH_ACTIVITY_CALORIE_LIST_KEY, monthActivity.getCalorieArray());
                    //responseMonthActivityBundle.putStringArray(ResponseMonthActivity.MONTH_ACTIVITY_MONTH_LIST_KEY, monthActivity.getMonthArray());
                } else {
                    responseMonthActivityBundle.putString(this.RESULT_KEY, this.NONE);
                }

                responseMonthActivityMessage.setData(responseMonthActivityBundle);

                try {
                    this.mResponseMessenger.send(responseMonthActivityMessage);
                } catch (RemoteException e) {
                    //TODO 에러처리
                }

                Log.d(TAG , "월 활동량 요청 =" + clientNumber );
                break;
            case MessageType.MSG_CALCULATE_ACTIVITY:
                Log.d(TAG , "계산 요청 =" + clientNumber );

                Bundle requestCalculateBundle = msg.getData();
                clientNumber = requestCalculateBundle.getInt( this.CLIENT_NUMBER_KEY );
                this.mResponseMessenger = this.getResponseMessenger(clientNumber);

                this.mStarter.requestCalculate( this.mResponseMessenger );

//                Message responseCalculateMessage = Message.obtain(null, msg.what);
//                try {
//                    this.mResponseMessenger.send(responseCalculateMessage);
//                } catch (RemoteException e) { }
//
                Log.d(TAG , "계산 요청 완료 =" + clientNumber );
                break;
            case MessageType.MSG_START_SERVICE:

                break;
            case MessageType.MSG_STOP_SERVICE:
                Log.d(TAG, "서비스 스탑 요청");
                boolean st = this.mStarter.StopThread();

                Log.d(TAG, "서비스 스탑 요청 결과 : " + st);

                Bundle reqbd = msg.getData();
                clientNumber = reqbd.getInt(this.CLIENT_NUMBER_KEY);
                this.mResponseMessenger = this.getResponseMessenger(clientNumber);
                Message msgs = Message.obtain(null, msg.what);
                Bundle msgbd = new Bundle();
                msgs.setData(msgbd);

                try {
                    mResponseMessenger.send(msgs);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }

                break;
            case MessageType.MSG_RESTART_SERVICE:
                Log.d(TAG, "서비스 재시작 요청");
                this.mStarter.Connect();
                Log.d(TAG, "서비스 스탑 요청 완료");

                break;
            case MessageType.MSG_RESET_GET_DATA:
                Log.d(TAG, "서비스 스탑 후 시작 요청");
                this.mStarter.resetGetData();
                Log.d(TAG, "서비스 스탑 후 시작 완료");
                break;
            case MessageType.MSG_APPLY_PROGRAM:
//                Message responseExerciseProgramMessage = Message.obtain(null, msg.what);
//                Bundle responseExerciseProgramBundle = new Bundle();
//                String response = null;
//
//                Bundle requestExerciseProgramBundle = msg.getData();
//                clientNumber = requestExerciseProgramBundle.getInt( this.CLIENT_NUMBER_KEY );
//                this.mResponseMessenger = this.getResponseMessenger( clientNumber );
//
//                //클라이언트로부터 받은 데이터를 가지고 운동프로그램 객체를 생성한다.
//                int id = requestExerciseProgramBundle.getInt( ExerciseProgram.ID_KEY );
//                String name = requestExerciseProgramBundle.getString( ExerciseProgram.NAME_KEY);
//                StrengthInputType strengthInputType = StrengthInputType.values()[requestExerciseProgramBundle.getInt(ExerciseProgram.CATEGORY_KEY)];
//                int strengthFrom = requestExerciseProgramBundle.getInt(ExerciseProgram.STRENGTH_FROM_KEY);
//                int strengthTo = requestExerciseProgramBundle.getInt(ExerciseProgram.STRENGTH_TO_KEY);
//                int timesFrom = requestExerciseProgramBundle.getInt(ExerciseProgram.TIMES_FROM_KEY);
//                int timesTo = requestExerciseProgramBundle.getInt(ExerciseProgram.TIMES_TO_KEY);
//                int minuteFrom = requestExerciseProgramBundle.getInt(ExerciseProgram.MINUTE_FROM_KEY);
//                int minuteTo = requestExerciseProgramBundle.getInt(ExerciseProgram.MINUTE_TO_KEY);
//                boolean continuousExercise = (requestExerciseProgramBundle.getInt(ExerciseProgram.CONTINUOUS_EXERCISE_KEY) != 0);
//                ExerciseProgram exerciseProgram = new ExerciseProgram( name, strengthInputType , strengthFrom , strengthTo , timesFrom, timesTo ,minuteFrom ,minuteTo,continuousExercise, false ,0,0 );
//
//                //운동 프로그램 객체를 DB에 저장한다.
//                //성공하면 SUCCESS를 리턴하고 , 실패하면 FAIL을 리턴한다.
//                //boolean dbResult = this.mDBManager.setAppliedExerciseProgram( exerciseProgram );
//                boolean dbResult = false;
//
//                if( dbResult ){
//                    response = this.SUCCESS;
//                }else{
//                    response = this.FAIL;
//                }
//
//                responseExerciseProgramBundle.putString(this.RESULT_KEY, response);
//                responseExerciseProgramBundle.putInt(ExerciseProgram.ID_KEY , id);
//                responseExerciseProgramMessage.setValues( responseExerciseProgramBundle );
//
//                try {
//                        this.mResponseMessenger.send(responseExerciseProgramMessage);
//                 } catch (RemoteException e) {   }
//
//                Log.d(TAG , "운동 프로그램 등록 =" + clientNumber );
                break;
            case MessageType.MSG_DATA_SYNC:

                break;
        }
    }

    private Messenger getResponseMessenger( int clientNumber )
    {
        Iterator<ClientInfo> clientIter = this.mClients.iterator();
        Messenger result = null;
        while( clientIter.hasNext() )
        {
            ClientInfo clientInfo = clientIter.next();
            if( clientInfo.clientNumber == clientNumber )
            {
                result = clientInfo.messenger;
                break;
            }
        }

        return result;
    }

    private void removeResponseMessenger( int clientNumber )
    {
        int removeIndex = -1;
        for( int i = 0 ; i < this.mClients.size();i++ )
        {
            ClientInfo clientInfo = this.mClients.get(i);
            if( clientInfo.clientNumber == clientNumber ){
                removeIndex = i;
                break;
            }
        }

        if( removeIndex > -1 ) {
            this.mClients.remove(removeIndex);
        }

    }

    public void setStarter( FitmateServiceStarter starter ){
        this.mStarter = starter;
    }

//    @Override
//    public void onSyncEnd() {
//
//        Message syncMessage = Message.obtain(null , MessageType.MSG_DATA_SYNC);
//        Bundle syncBundle = new Bundle();
//        syncBundle.putInt( "synctype" , SyncType.END.getValue());
//        syncMessage.setValues( syncBundle );
//
//        for( int i = 0 ; i < this.mClients.size();i++ )
//        {
//            ClientInfo clientInfo = this.mClients.get(i);
//            Messenger messenger = clientInfo.messenger;
//
//            try {
//                messenger.send(syncMessage);
//            } catch (RemoteException e) {
//                e.printStackTrace();
//            }
//        }
//
//        Log.i(TAG , "동기화 끝");
//    }

    @Override
    public void onSyncInfo(SyncType syncType , int recceiveBytes, int totalBytes) {

//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                mDataSyncCallback.onSyncInfo(syncType , recceiveBytes, totalBytes);
//            }
//        }).start();

        Message syncMessage = Message.obtain(null , MessageType.MSG_DATA_SYNC);

        Bundle syncBundle = new Bundle();
        syncBundle.putInt( "synctype" , syncType.getValue());
        syncBundle.putInt( "receivebyte" , recceiveBytes );
        syncBundle.putInt( "totalbyte" , totalBytes );
        syncMessage.setData( syncBundle );

        for( int i = 0 ; i < this.mClients.size();i++ )
        {
            ClientInfo clientInfo = this.mClients.get(i);
            Messenger messenger = clientInfo.messenger;

            try {
                messenger.send( syncMessage );
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        Log.i(TAG , "동기화 정보 송신 -  RECEIVE LENGTH :" + recceiveBytes + "  TOTAL LENGTH : " + totalBytes );
    }

    @Override
    public void onBatteryStatus(int batteryRatio) {
        Message syncMessage = Message.obtain(null , MessageType.MSG_BATTERYRATE);

        Bundle syncBundle = new Bundle();
        syncBundle.putInt( "battery" , batteryRatio);
        syncMessage.setData( syncBundle );

        for( int i = 0 ; i < this.mClients.size();i++ )
        {
            ClientInfo clientInfo = this.mClients.get(i);
            Messenger messenger = clientInfo.messenger;

            try {
                messenger.send( syncMessage );
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        Log.i(TAG , "배터리 정보 전송: "+batteryRatio);
    }

    class ClientInfo{
        int clientNumber;
        Messenger messenger;
    }
}
