package com.fitdotlife.fitmate_lib.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.WearingLocation;
import com.fitdotlife.fitmate.LoginActivity;
import com.fitdotlife.fitmate.R;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.service.bluetooth.FitmeterDeviceDriver;
import com.fitdotlife.fitmate_lib.service.bluetooth.FitmeterDeviceManager;
import com.fitdotlife.fitmate_lib.service.bluetooth.FitmeterDevicePeriodicScanner;
import com.fitdotlife.fitmate_lib.service.database.FitmateServiceDBManager;
import com.fitdotlife.fitmate_lib.service.key.MessageType;
import com.fitdotlife.fitmate_lib.service.key.SyncType;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;

/**
 * Created by Joshua on 2015-01-28.
 */
public class FitmateServiceStarter implements FitmeterDevicePeriodicScanner.FitmemterScanListener , FitmeterDeviceManager.FitmeterDeviceStateCallback ,  ActivityDataCalculator.CalculateCompleteListener
{
    FitmateServiceStarter fss = null;

    private final int MAX_ERROR_COUNT = 2;
    private final String TAG = "fitmateservice - " + FitmateServiceStarter.class.getSimpleName();
    private final String ACTION_CALCULATE = "com.fitdotlife.fitmateservice.CALCULATE";
    private boolean isSleep = false;

    public interface DataSyncCallback{
        public void onSyncInfo(SyncType syncType, int recceiveBytes, int totalBytes);
    }

    public interface BatteryStatusCallback{
        public void onBatteryStatus(int batteryRatio);
    }

    private BatteryStatusCallback mBatteryStatusCallback=null;
    private DataSyncCallback mDataSyncCallback = null;
    private int mDataInterval = 1;
    private WearingLocation mWearingLocation = WearingLocation.WRIST;

    private static Context mContext = null;
    private BluetoothManager mBluetoothManager = null;
    private BluetoothAdapter mBluetoothAdapter = null;
    private FitmateDBManager mDBManager = null;
    //private FitmeterDevicePeriodicScanner mScanner = null;
    private PeriodicCalculateNotifier mCalculateNotifier = null;

    private FitmateServiceDBManager mServiceDBManager = null;

    private boolean isDataCalculate = false;
    private FitmeterDeviceManager mFitmeterDeviceManager = null;

    private int mDeviceErrorCount = 0;

    private BroadcastReceiver mBluetoothReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            String action = intent.getAction();
            if( BluetoothAdapter.ACTION_STATE_CHANGED.equals( action ) )
            {
                final int state = intent.getIntExtra( BluetoothAdapter.EXTRA_STATE , BluetoothAdapter.ERROR);
                switch( state )
                {
                    case BluetoothAdapter.STATE_TURNING_OFF:
                        org.apache.log4j.Log.d(TAG, "블루투스가 꺼집니다.");
                        try{
                            mDataGetThread.interrupt();
                            fdd.disconnect();
                            fdd.close();
                        }
                        catch (Exception ex)
                        {
                            org.apache.log4j.Log.d(TAG, "블루투스 데이터 스레드 에러 발생. " + ex.getMessage());
                        }
                        //mScanner.stopSearchProgress();
                        break;
                    case BluetoothAdapter.STATE_ON:
                        org.apache.log4j.Log.d(TAG, "블루투스가 켜집니다.");
                        UserInfo userInfo = mDBManager.getUserInfo();
                        //mBluetoothAdapter.startLeScan(mSacn);
                        //mScanner.startSearchProgress( userInfo.getDeviceAddress() , 0 );
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        if(!isRunThread)
                            Connect();

                        //RunConnection(mContext);
                        break;
                }
            }
        }
    };

    BluetoothAdapter.LeScanCallback mSacn = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {

        }
    };


    void ShowNotifiation(String title, String content, String noti)
    {
        NotificationManager nm = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0, new Intent(mContext, LoginActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mCompatBuilder = new NotificationCompat.Builder(mContext);
        mCompatBuilder.setSmallIcon(R.drawable.fitlife_ico);
        mCompatBuilder.setTicker(noti);
        mCompatBuilder.setWhen(System.currentTimeMillis());
        //mCompatBuilder.setNumber(10);
        mCompatBuilder.setContentTitle(title);
        mCompatBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(content));
        //mCompatBuilder.setContentText(content);
        mCompatBuilder.setVisibility(Notification.VISIBILITY_PUBLIC);
        mCompatBuilder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);
        mCompatBuilder.setContentIntent(pendingIntent);
        mCompatBuilder.setAutoCancel(true);

        nm.notify(222, mCompatBuilder.build());
    }

    FitmeterDeviceDriver fdd = null;
    BluetoothDevice mBd = null;



    boolean isConnectedComplted = false;
    boolean isDisconnected  = false;

    void RunConnection(Context ctx)
    {
        isConnectedComplted = false;

        if(this.mBluetoothManager == null || this.mBluetoothAdapter == null || mBd == null)
        {
            Log.d(TAG, "mBluetoothManager or mBluetoothAdapter or mBd null");

            //this.mBluetoothManager = (BluetoothManager)this.mContext.getSystemService( Context.BLUETOOTH_SERVICE );
            //this.mBluetoothAdapter = this.mBluetoothManager.getAdapter();

            UserInfo userInfo = mDBManager.getUserInfo();
            mBd = mBluetoothAdapter.getRemoteDevice(userInfo.getDeviceAddress());
        }


        if(fdd != null)
        {
            /*
            fdd.disconnect();
            //fdd.close();

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            */
        }
        else
        {
            fdd = new FitmeterDeviceDriver(ctx, mBluetoothAdapter, mBd, new FitmeterDeviceDriver.BLEDeviceStateCallback() {
                @Override
                public void deviceDisconnected(BluetoothGatt gatt) {
                    org.apache.log4j.Log.d(TAG, "deviceDisconnected");
                    isDisconnected = true;
                }

                @Override
                public void deviceInitializedErrorOccured(String errorMessage) {
                    org.apache.log4j.Log.d(TAG, "deviceInitializedErrorOccured msg :" + errorMessage);
                }

                @Override
                public void deviceInitializedTimeout() {
                    org.apache.log4j.Log.d(TAG, "deviceInitializedTimeout");
                }

                @Override
                public void initializeCompleted() {
                    org.apache.log4j.Log.d(TAG, "initializeCompleted");
                    isDisconnected = false;
                    isConnectedComplted = true;
                }
            });
        }

        Log.d(TAG, "RunConnection");
        if(mBd == null)
        {
            Log.e(TAG, "Device null");
        }
    }


    FitmeterDeviceManager fddata = null;

    public FitmateServiceStarter( final Context context  , FitmateDBManager dbManager , InComingMessageManager inComingMessageManager )
    {
        fss = this;
        this.mContext = context;
        this.mDBManager = dbManager;
        this.mServiceDBManager = new FitmateServiceDBManager(this.mContext);
        this.mDataSyncCallback = inComingMessageManager;
        this.mBatteryStatusCallback= inComingMessageManager;

        this.mBluetoothManager = (BluetoothManager)this.mContext.getSystemService( Context.BLUETOOTH_SERVICE );
        this.mBluetoothAdapter = this.mBluetoothManager.getAdapter();

        //this.mCalculateNotifier = new PeriodicCalculateNotifier( this.mContext ,  this );
        //this.mCalculateNotifier.startCalculateAlarm();

        //블루투스가 꺼질때와 켜질때를 알수 있는 리시버를 등록한다.
        IntentFilter bluetoothFilter = new IntentFilter( );
        bluetoothFilter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        this.mContext.registerReceiver(this.mBluetoothReceiver, bluetoothFilter);

        //this.mScanner = new FitmeterDevicePeriodicScanner( this.mContext , this.mBluetoothAdapter , this );
        Connect();
    }

    static boolean isRunThread = false;
    static boolean isRun = true;

    Thread mDataGetThread = null;



    final int TimeOutHours = 8;

    boolean isNotified = false;


    public boolean StopThread()
    {
        Log.d("CONTROLSERVICE", "STOP Thread start");
        isRun = false;
        for (int i = 0; i < 100; i++)
        {

            if(!isRunThread) {
                Log.d("CONTROLSERVICE", "STOP Thread Completed");
                break;
            }
            try {
                if(mDataGetThread != null)
                    mDataGetThread.interrupt();
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        fdd = null;
        Log.d("CONTROLSERVICE", "STOP Thread end");

        return isRunThread;
    }

    void Connect()
    {
        if(isRunThread)
            return;


        if( isBtEnabled() )
        {
            UserInfo userInfo = mDBManager.getUserInfo();
            //this.mScanner.startSearchProgress( userInfo.getDeviceAddress() , 0 );

            Log.e(TAG, "FitmateServiceStarter Connect : " + userInfo.getDeviceAddress());

            mBd = mBluetoothAdapter.getRemoteDevice(userInfo.getDeviceAddress());
            fddata = new FitmeterDeviceManager( mContext , mBluetoothAdapter , fss, mServiceDBManager);

            mDataGetThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    //Debug.waitForDebugger();
                    isRunThread = true;
                    isRun = true;
                    boolean isDataTransfer = false;
                    boolean isConnected = false;

                    int scan_fail_cnt = 0;

                    int dataReceivedFailCnt = 0;

                    Log.i("ConThread", "Thread Run Start");

                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    while(mBluetoothAdapter.isEnabled() && isRun)
                    {

                        /*
                        if(isAnother)
                        {

                            // todo
                            continue;
                        }
                        */
                        isSleep = false;

                        boolean isError133 = false;
                        try {
                            isConnectedComplted = false;
                            Log.i("ConThread", "RunConnection");
                            RunConnection(mContext);

                            Log.i("ConThread", "StartScan");
                            if(fdd.StartScan())
                            {

                                Thread.sleep(100);
                                Log.i("ConThread", "Start Connection");
                                fdd.initialize();
                            }
                            else
                            {
                                Log.i("ConThread", "Scan Fail");
                                if(scan_fail_cnt > 1)
                                {
                                    new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            //NetworkClass nc = new NetworkClass();
                                            //nc.PostInputStreamFromUrl("https://api.hipchat.com/v2/room/dev_fitmatesnu/notification?auth_token=uWBYEXiP4lZvCftjVJ5SVPsdk7KEkEM8E7Hy4zKD", new J);
                                            //NetworkClassThread networkClassThread = new NetworkClassThread( mContext , "https://api.hipchat.com/v2/room/dev_fitmatesnu/notification?auth_token=uWBYEXiP4lZvCftjVJ5SVPsdk7KEkEM8E7Hy4zKD", jsonArray , responseCode , this.mCallback );
                                            //networkClassThread.start();
                                            Vibrator vibe = (Vibrator) mContext.getSystemService(Context.VIBRATOR_SERVICE);
                                            //vibe.vibrate(500);

                                            // todo 테스트용 진동 코드

                                            Log.e("ConThread", "스캔 다수 실패로 진동!!");
                                        }
                                    }).start();
                                }
                                else {
                                    scan_fail_cnt++;
                                }

                                continue;
                            }

                            Log.i("ConThread", "Connection Check");
                            // todo 10을 50으로 변경해야됨. 테스트용
                            for(int i = 0; i < 500; i++)
                            {
                                if(fdd.isConnected)
                                {
                                    Log.i(TAG, "연결 성공");
                                    scan_fail_cnt = 0;
                                    break;
                                }

                                if(!fdd.isDisonnected)
                                {
                                    Log.i(TAG, "연결 실패");
                                    break;
                                }

                                if(fdd.isFailed)
                                {
                                    Log.i(TAG, "연결 isFailed");
                                    isError133 = true;

                                    //fdd.disconnect();
                                    //fdd.close();

                                    Thread.sleep(5000);

                                    // todo 여기 걸리면 삼성폰은 계속 대기함. 아마 커넥션을 강제로 끊는게 먹지 않는듯.
                                    // 133 에러날때도 마찬가지임

                                    throw new Exception("커넥션 중 133 에러 발생");

                                    //break;
                                }

                                Thread.sleep(100);
                            }

                            if(fdd.isConnected)
                            {
                                Log.i("ConThread", "Connected");
                                Log.i("ConThread", "Start Discovery");
                                fdd.startServicesDiscovery();
                                for(int i = 0; i < 30; i++)
                                {
                                    if(fdd.isDisconvered)
                                    {
                                        break;
                                    }

                                    Thread.sleep(100);
                                }
                            }
                            else
                            {
                                Log.i("ConThread", "Not Connected");

                                // todo 133과 비슷한 상황임
                                isError133 = true;

                                fdd.OnlyClose();
                                //fdd.close();
                                fdd.disconnect();
                                Thread.sleep(5000);

                                List<BluetoothDevice> mList = mBluetoothManager.getConnectedDevices(BluetoothProfile.GATT);
                                for (BluetoothDevice bd : mList) {
                                    Log.i(TAG, String.format("연결 장치 체크 : %s[%s] Connected", bd.getName(), bd.getAddress()));
                                }

                                mList = mBluetoothManager.getConnectedDevices(BluetoothProfile.GATT_SERVER);
                                for (BluetoothDevice bd : mList) {
                                    Log.i(TAG, String.format("연결 서버 체크 : %s[%s] Connected", bd.getName(), bd.getAddress()));
                                }
                                continue;
                            }

                            if(fdd.isDisconvered)
                            {
                                Log.i("ConThread", "Start Descriptor Write");
                                fdd.setTXNotification();
                                for(int i = 0; i < 30; i++)
                                {
                                    if(fdd.isSetTxNotification)
                                    {
                                        Log.i("ConThread", "Descriptor Write Completed");
                                        break;
                                    }

                                    try {
                                        Thread.sleep(100);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                            else
                            {
                                Log.i("ConThread", "Not Discorverd");
                                continue;
                            }

                            for(int i = 0; i < 30; i++)
                            {
                                if(isConnectedComplted)
                                {
                                    break;
                                }

                                Thread.sleep(100);
                            }

                            if(!isConnectedComplted)
                            {
                                Log.i("ConThread", "Not isConnectedComplted");
                                continue;
                            }

                            Log.i("ConThread", "Start GetData");
                            isDataTransfer = true;
                            if(!fddata.getData(fdd))
                            {
                                throw new Exception("데이터 전송 중 실패. 빠른 재연결 필요!!");

                            }
                            isDataTransfer = false;
                            Log.i("ConThread", "End GetData");
                        }
                        catch(Exception ex)
                        {
                            StringWriter errors = new StringWriter();
                            ex.printStackTrace(new PrintWriter(errors));

                            org.apache.log4j.Log.d("ConThread", "throw>" + ex.getMessage() + "\n" + errors.toString());
                        }
                        finally {
                            try {
                                if(!isError133) {
                                    Log.i("ConThread", "Finally Call");
                                    fdd.disconnect();
                                    Thread.sleep(100);
                                    fdd.close();
                                    Log.i("ConThread", "Device disconnet complete");
                                }
                                else
                                {
                                    //Thread.sleep(20000);
                                    // 혹시 슬립을 20초를 줘야 블투 장비를 정리하나??? 테스트 해봐야지, 133일땐 10초 더 쉬기
                                }

                                if(isDataTransfer && dataReceivedFailCnt <= 3)
                                {
                                    dataReceivedFailCnt++;
                                    Thread.sleep(1000);
                                }
                                else
                                {
                                    dataReceivedFailCnt = 0;
                                    isSleep = true;
                                    Thread.sleep(10000);

                                    // 혹시 슬립을 20초를 줘야 블투 장비를 정리하나??? 테스트 해봐야지
                                }
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                        }
                    }

                    isRunThread = false;

                }
            });
            mDataGetThread.start();

        }else{
            //TODO 알림바로 블루투스가 켜져 있지 않아 데이터 수집이 힘들다는 메시지를 전송해야함.
        }

    }

    static public boolean isBtEnabled()
    {

        final BluetoothManager manager = (BluetoothManager) mContext.getSystemService( Context.BLUETOOTH_SERVICE );
        if( manager == null ) return false;

        final BluetoothAdapter adapter = manager.getAdapter();
        if( adapter == null ) return false;

        return adapter.isEnabled();
    }

    public void requestCalculate( Messenger responseMessenger){

        if( true ) {
            isDataCalculate = true;

            ActivityDataCalculator dataCalculator = ActivityDataCalculator_.getInstance_(mContext);
            dataCalculator.setFitmateDBManager(this.mDBManager);
            dataCalculator.setCalculateCompleteListener( this );
            dataCalculator.setFitmateServiceDBManager( this.mServiceDBManager );
            dataCalculator.setNotifyCalculateComplete(true);
            dataCalculator.setResponseMessenger( responseMessenger );
            dataCalculator.start();
        }else{
            Message msg = Message.obtain(null, MessageType.MSG_CALCULATE_ACTIVITY);

            try {

                responseMessenger.send(msg);

            } catch (RemoteException e) {
            }
        }
    }

    public void close()
    {
        this.mContext.unregisterReceiver(this.mBluetoothReceiver );
        //this.mScanner.stopSearchProgress();
        this.mCalculateNotifier.stopCalculateAlarm();

    }

    public void resetGetData(){
        if( isSleep ) {
            if( mDataGetThread != null ) {
                try {
                    if(mDataGetThread != null)
                        mDataGetThread.interrupt();
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    return;
                }
            }

            Connect();
        }
    }


    @Override
    public void onFitmeterFound( BluetoothDevice device ) {

        /*ExerciseProgram program = this.mDBManager.getAppliedExerciseProgram();
        UserInfo userInfo = this.mDBManager.getUserInfo();
        UserInfoForAnalyzer userInfoForAnalyzer = new UserInfoForAnalyzer(NewHomeUtils.calculateAge( userInfo.getBirthDate() )  , userInfo.getGender().getBoolean() , userInfo.getHeight() , userInfo.getWeight() , ContinuousCheckPolicy.Strictly , 1 , USERVO2MAXLEVEL.Fair );*/

        //ExerciseAnalyzer analyzer = new ExerciseAnalyzer( program , userInfoForAnalyzer , this.mWearingLocation , this.mDataInterval  );
        //mFitmeterDeviceManager.setBluetoothDevice( device );
        //mFitmeterDeviceManager.start();
    }

    @Override
    public void onSyncInfo( final SyncType syncType , final int recceiveBytes, final int totalBytes ) {

        mDataSyncCallback.onSyncInfo(syncType , recceiveBytes, totalBytes);

    }

    @Override
    public void onBatteryStatus(int batteryRatio) {
        mBatteryStatusCallback.onBatteryStatus(batteryRatio);
    }

//    @Override
//    public void onRequestCalculate() {
//        if( !this.isDataCalculate ) {
//            isDataCalculate = true;
//
//            ActivityDataCalculator dataCalculator = new ActivityDataCalculator( this.mContext , this.mDBManager , this , this.mServiceDBManager);
//            dataCalculator.setNotifyCalculateComplete(false);
//            dataCalculator.start();
//        }
//    }

    @Override
    public void onCalculateCompleted() {
        this.isDataCalculate = false;
    }
}
