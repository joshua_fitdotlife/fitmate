//package com.fitdotlife.fitmate.service;
//
//import android.content.Context;
//import android.content.SharedPreferences;
//import android.os.Environment;
//import android.os.Message;
//import android.os.Messenger;
//import android.os.RemoteException;
//import android.util.Log;
//
//import com.fitdotlife.exerciseanalysis.ContinuousCheckPolicy;
//import com.fitdotlife.exerciseanalysis.ContinuousExerciseInfo;
//import com.fitdotlife.exerciseanalysis.ExerciseAnalyzer;
//import com.fitdotlife.exerciseanalysis.ExerciseProgram;
//import com.fitdotlife.exerciseanalysis.StrengthInputType;
//import com.fitdotlife.exerciseanalysis.USERVO2MAXLEVEL;
//import com.fitdotlife.exerciseanalysis.UserInfoForAnalyzer;
//import com.fitdotlife.exerciseanalysis.WearingLocation;
//import com.fitdotlife.fitmate.database.FitmateDBManager;
//import com.fitdotlife.fitmate.object.DayActivity;
//import com.fitdotlife.fitmate.object.UserInfo;
//import com.fitdotlife.fitmate.service.activity.ActivityParser;
//import com.fitdotlife.fitmate.service.activity.DataParseException;
//import com.fitdotlife.fitmate.service.bluetooth.BluetoothRawData;
//import com.fitdotlife.fitmate.service.key.MessageType;
//import com.fitdotlife.fitmate.service.protocol.object.ActivityData;
//
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.FileNotFoundException;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.List;
//import java.util.Queue;
//
///**
//* Created by Joshua on 2015-03-16.
//*/
//public class ActivityDataCalculator_OLD extends  Thread {
//
//    private final String TAG = "fitmateservice - " + ActivityDataCalculator_OLD.class.getSimpleName();
//    private final String LAST_FILE_KEY = "lastfile";
//    private static final String LAST_DAY_FILE_INDEX_KEY = "lastdayfileindex";
//    private final String LAST_FILE_CALORIE_KEY = "lastfilecalorie";
//    private final String LAST_FILE_STRENGTH_HIGH_KEY = "lastfilestrengthhigh";
//    private final String LAST_FILE_STRENGTH_MEDIUM_KEY = "lastfilestrengthmedium";
//    private final String LAST_FILE_STRENGTH_LOW_KEY = "lastfilestrengthlow";
//    private final String LAST_FILE_STRENGTH_UNDERLOW_KEY = "lastfilestrengthunderlow";
//
//    private FitmateDBManager mDBManager = null;
//    private Queue<BluetoothRawData> mDataQueue = null;
//
//    private boolean mCalculated = false;
//    private boolean mNotifyCalculated = false;
//
//    private Messenger mResponseMessenger = null;
//    private Context mContext = null;
//    private CalculateCompleteListener mListener;
//
//    public interface CalculateCompleteListener{
//        public void onCalculateCompleted();
//    }
//
//    public ActivityDataCalculator_OLD(Context context, FitmateDBManager dbManager, Queue<BluetoothRawData> dataQueue, CalculateCompleteListener listener){
//        this.mDBManager = dbManager;
//        this.mDataQueue = dataQueue;
//        this.mListener = listener;
//        this.mContext = context;
//    }
//
//    public boolean isCalculated(){
//        return this.mCalculated;
//    }
//
//    public void setNotifyCalculateComplete(  boolean notifyCalculated ){
//        this.mNotifyCalculated = notifyCalculated;
//    }
//
//    public void setResponseMessenger( Messenger responseMessenger ){
//        this.mResponseMessenger = responseMessenger;
//    }
//
//    public void run(){
//
//        //큐에 데이터가 없다면 그냥 반환한다.
//        if( this.mDataQueue.isEmpty() ) {
//            Log.d(TAG , "데이터가 없습니다.");
//            this.mListener.onCalculateCompleted();
//            if( this.mNotifyCalculated ) {this.sendServiceMessage();}
//            return;
//        }
//        this.mCalculated = true;
//
//        //큐를 다른 쪽에서 사용하고 있다면 계속 기다린다.
//        while( FitmateServiceStarter.queueUsed  ){
//            try {
//                Thread.sleep( 1000 );
//                Log.d(TAG , "큐를 사용하기 위해 기다리고 있음...");
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
//
//        //다른 곳에서 큐를 사용하지 않도록
//        FitmateServiceStarter.queueUsed =  true;
//
//        ContinuousCheckPolicy policy = ContinuousCheckPolicy.Strictly;
//        ExerciseProgram appliedExerciseProgram = this.mDBManager.getAppliedExerciseProgram();
//        UserInfo userInfo = this.mDBManager.getUserInfo();
//        UserInfoForAnalyzer userInfoForAnalyzer = new UserInfoForAnalyzer(userInfo.getAge() , userInfo.getGender().getBoolean() , userInfo.getHeight() , userInfo.getWeight() , ContinuousCheckPolicy.Strictly , 1 , USERVO2MAXLEVEL.Fair );
//
//        //큐에서 모든 데이터를 가져온다.
//        List<BluetoothRawData> rawDataList = new ArrayList<BluetoothRawData>();
//        int queueSize = mDataQueue.size();
//        for( int i = 0 ; i < queueSize ;i++ ){
//            rawDataList.add( this.mDataQueue.poll() );
//        }
//
//        Log.d(TAG , "RawDataList 크기 : " + rawDataList.size() );
//
//        ActivityParser activityParser = new ActivityParser();
//
//        //마지막 데이터 파일의 인덱스와 마지막 데이터 파일을 가져온다.
//        int lastFileIndex = this.getLastFileIndex();
//        BluetoothRawData lastFileData = null;
//        byte[] lastFileBytes = this.readLastDataFile();
//        lastFileData = new BluetoothRawData(lastFileIndex, lastFileBytes);
//
//        //파일을 합친다.
//        int previousIndex = 0;
//        for( int i = 0 ; i < rawDataList.size() ; i++ ) {
//
//            if( lastFileData.getFileIndex() == rawDataList.get(i).getFileIndex() ){
//                lastFileData.addFileData(rawDataList.get(i).getFileData());
//                rawDataList.get(i).setDoParse(false);
//                continue;
//            }else {
//
//                if (i != 0) {
//
//                    if (rawDataList.get(previousIndex).getFileIndex() == rawDataList.get(i).getFileIndex()) {
//                        rawDataList.get(previousIndex).addFileData(rawDataList.get(i).getFileData());
//                        rawDataList.get(i).setDoParse(false);
//                        continue;
//                    }
//                }
//            }
//            previousIndex = i;
//        }
//
//        //마지막 파일을 분석합니다.
//        try {
//            if( lastFileData.getFileData() != null ) {
//                if( lastFileData.getIsAdded() ) {
//                    activityParser.parse(lastFileData);
//                    this.setLastFileIndex(lastFileData.getFileIndex());
//                    this.writeLastDataFile(lastFileData.getFileData());
//                }
//            }
//        } catch (DataParseException e) {
//            Log.e(TAG , "파일을 분석하는 중에 오류가 발생하였습니다. : " + e.getMessage());
//        }
//
//        //데이터 파일별로 분석한다.
//        for( int i = 0 ; i < rawDataList.size() ;i++ )
//        {
//            try {
//
//                if( rawDataList.get(i).getDoParse() ) {
//                    activityParser.parse(rawDataList.get(i));
//
//                    if( i == ( rawDataList.size() - 1 ) ){
//                        this.setLastFileIndex( rawDataList.get(i).getFileIndex() );
//                        this.writeLastDataFile( rawDataList.get(i).getFileData() );
//                    }
//                }
//
//            } catch (DataParseException e) {
//                Log.e(TAG , "파일을 분석하는 중에 오류가 발생하였습니다. : " + e.getMessage());
//                continue;
//            }
//        }
//
//        List<ActivityData> activityDataList = activityParser.getActivityData();
//        Log.d(TAG, "Activity Data 목록의 크기 : " + activityDataList.size());
//
//        //날짜별로 데이터를 만든다.
//        //날짜에 따른 칼로리, MET , 저중고 운동시간, 평균 MET , 운동시간을 계산한다.
//        //계산되어진 활동량 정보를 날짜목록에 저장한다.
//
//        List<DayActivity> dayActivityList = new ArrayList<DayActivity>();
//        DayActivity lastDayActivity = this.mDBManager.getLastDayActivity();
//        if( lastDayActivity != null ){
//            dayActivityList.add( lastDayActivity );
//        }
//
//        for( int index = 0 ; index < activityDataList.size() ;index++ ){
//
//            ActivityData activityData = activityDataList.get(index);
//
//            ExerciseAnalyzer analyzer = new ExerciseAnalyzer( appliedExerciseProgram , userInfoForAnalyzer , WearingLocation.WRIST , (int) activityData.getSaveInterval() );
//            List<Integer> svmList = activityData.getSVMList();
//
//            int calorieByActivity = 0;
//            float[] metArray = new float[ ( 24 * 60 * 60 ) / (int)activityData.getSaveInterval() ];
//
//            float[] rawMetArray = new float[ svmList.size() ];
//            DayActivity dayActivity = new DayActivity();
//            dayActivity.setFileIndex( activityData.getRawFileIndex() );
//
//            //칼로리와 MET를 계산한다.
//            int metArrayIndex = ( ( activityData.getStartTime().getHour() * 60 * 60 ) + ( activityData.getStartTime().getMinute() * 60 ) + activityData.getStartTime().getSecond() ) / (int)activityData.getSaveInterval();
//            for( int i = 0 ; i < svmList.size() ; i++ ){
//                int calorie = analyzer.Calcuate_Calorie( svmList.get(i) );
//                calorieByActivity += calorie;
//                float met = analyzer.Calculate_METbyCaloire( calorie );
//                rawMetArray[i] = met;
//                metArray[ metArrayIndex + i ] = met;
//            }
//
//            //저중고 강도 운동시간을 계산한다.
//            int[] arrHMLTime = analyzer.MinutesforEachStrength( rawMetArray );
//            dayActivity.setRawMetArray( rawMetArray );
//            dayActivity.setStartTime( activityData.getStartTime() );
//            dayActivity.setCalorieByActivity(calorieByActivity);
//            dayActivity.setHmlStrengthTime(arrHMLTime);
//            dayActivity.setMetArray( metArray );
//            dayActivity.setDataSaveInterval((int) activityData.getSaveInterval());
//
//            //평균 MET를 계산한다.
//            dayActivity.setAverageMET( analyzer.CalculateAverageMET( dayActivity.getRawMetArray() ));
//
//            //AchieveOfTarget 를 계산한다.
//            List<ContinuousExerciseInfo> todayExerciseInfoList = analyzer.getContinuousExercise( policy, rawMetArray , dayActivity.getStartTime().getMilliSecond() , 1, 60 , 0, 0 );
//            int todayDayOfAchieve = 0;
//            if( todayExerciseInfoList.size() > 0 ){
//                ContinuousExerciseInfo todayExerciseInfo = todayExerciseInfoList.get(0);
//                todayDayOfAchieve = Math.round(( todayExerciseInfo.EndTime - todayExerciseInfo.StartTime ) / ( 60 * 1000 ) );
//            }
//            dayActivity.setAchieveOfTarget( todayDayOfAchieve );
//
//            if( index == activityDataList.size() -1 ){
//                this.setLastFileIndex( dayActivity.getFileIndex() );
//                this.writeLastDataFile( activityData.getRawData() );
//
//                this.setPreviousCalorie( dayActivity.getCalorieByActivity() );
//                this.setPreviousStrengthHigh( dayActivity.getStrengthHigh() );
//                this.setPreviousStrengthMedium( dayActivity.getStrengthMedium() );
//                this.setPreviousStrengthLow( dayActivity.getStrengthLow() );
//                this.setPreviousStrengthUnderLow( dayActivity.getStrengthUnderLow() );
//            }
//
//            //이전데이터와 겹치는지 확인한다.
//            if( dayActivityList.size() > 0 ){
//
//                //최근에 날짜목록에 저장된 데이터를 가져온다. 날짜목록에 있는 데이터의 날짜와 현재의 날짜가 겹치는 확인한다.
//                //저장간격이 같은지 확인한다.
//                //날짜도 같고 저장간격도 같다면 데이터를 합친다.
//                DayActivity preDayActivity = dayActivityList.get( dayActivityList.size() - 1 );
//
//                if( dayActivity.getActivityDate() .equals( preDayActivity.getActivityDate() ) ){ //날짜가 겹치는지 확인한다.
//                    //if( dayActivity.getDataSaveInterval() == preDayActivity.getDataSaveInterval() ){ //저장간격이 겹치는지 확인한다.
//
//                    if( lastFileIndex == dayActivity.getFileIndex() ){
//
//                        preDayActivity.overwrite( dayActivity , this.getPreviousCalorie() , new int[]{ this.getPreviousStrengthUnderLow() , this.getPreviousStrengthLow() , this.getPreviousStrengthMedium() , this.getPreviousStrengthHigh() } );
//                        continue;
//
//                    }else {
//                        preDayActivity.add( dayActivity );
//                        continue;
//                    }
//                    //}
//                }
//            }
//
//            dayActivityList.add( dayActivity );
//
//        }//END FOR
//
//
//        //마지막 날짜의 시작 파일 인덱스를 저장한다.
//        Log.d(TAG, "Day Activity 목록의 크기 : " + dayActivityList.size());
//
//        //####
//        // 위에서 일별 활동량 목록을 계산을 다 했다.
//        // 주 활동량을 계산 해야 하는데 먼저 각 날짜가 어느 주에 속하는지 알아야 한다.
//        // 각 날짜는 52주 중에 한 주에 해당할 것이다. 각 날짜의 주 숫자를 알아내어서 주활동량을 계산하게 된다.
//        // 그리고, 위의 일 활동량 목록의 첫날이 주중 요일이라면 주의 처음부터 데이터가 필요하다.
//        //예를 들면 일 활동량 목록이 첫날이 화요일 이라면 주의 처음부터 화요일까지 데이터가 필요하다.
//        //그래서 아래의 코드는 일 활동량 목록의 첫날이 무슨 요일인지 알아내고 데이터베이스로부터 이 날짜가 속한 주 활동량을 가져와서 먼저 계산하도록 한다.
//        //####
//
//        //일 활동량 목록에서 첫날이 속한 요일의 시작일 요일의 종료일 가져온다.
//        int preDataSaveInterval = 0;
//        Calendar weekCalendar = Calendar.getInstance();
//        weekCalendar.setTimeInMillis( dayActivityList.get(0).getStartTime().getMilliSecond() );
//        int preWeekNumber = weekCalendar.get( Calendar.WEEK_OF_YEAR );
//        Log.d(TAG, "주의 숫자 : " + preWeekNumber );
//
//        int dayOfWeek = weekCalendar.get( Calendar.DAY_OF_WEEK );
//        weekCalendar.add(Calendar.DATE , -( dayOfWeek - 1 ) );
//        String weekStartDate = this.getDateString_yyyy_MM_dd( weekCalendar );
//        Log.d(TAG, "주 시작일 : " + weekStartDate );
//
//        weekCalendar.add(Calendar.DATE , 6);
//        String weekEndDate = this.getDateString_yyyy_MM_dd( weekCalendar );
//        Log.d(TAG, "주 종료일 : " + weekEndDate );
//
//        int weekHighTimeSum = 0;
//        int weekMediumTimeSum = 0;
//        int weekLowTimeSum = 0;
//        int weekUnderLowTimeSum = 0;
//        int weekCalorieSum = 0;
//        float weekMETSum = 0;
//
//        List<Integer> calorieList = new ArrayList<Integer>();
//        List<List<Integer>> weekContinuousExerciseList = new ArrayList<List<Integer>>();
//
//        List<DayActivity> weekActivityList = this.mDBManager.getDayActivityList( weekStartDate , weekEndDate );
//        Log.d(TAG, "데이터베이스로부터 가져온 첫번째 날짜가 해당하는 주의 일 활동량 목록 크기 : " + weekActivityList.size() );
//
//        if( weekActivityList.size() > 0 ) {
//            DayActivity fitstDayActivity = dayActivityList.get(0);
//            int weekListSize = weekActivityList.size();
//            for (int i = 0; i < weekListSize; i++) {
//
//                DayActivity preDayActivity = weekActivityList.get(i);
//
//                //휘트미터로부터 읽어온 일별 데이터 중 DB로부터 읽어온 첫번째 데이터의 날짜와 같은 데이터가 데이터베이스에 있을 수 있다.
//                //온종일 데이터를 저장하지 않을 수 있기때문에
//                //여기서 날짜가 겹치는지 확인한다.
//                if (fitstDayActivity.getActivityDate().equals( preDayActivity.getActivityDate() )) {
//                    Log.d(TAG, "데이터베이스로부터 가져온 일 활동량과 현재 목록의 일 활동량이 겹침. : 인덱스" + i );
//                    //weekActivityList.add(i, fitstDayActivity);
//                    //preDayActivity = fitstDayActivity;
//                    //dayActivityList.remove(0);
//                    break;
//                }
//
//                weekHighTimeSum += preDayActivity.getStrengthHigh();
//                weekMediumTimeSum += preDayActivity.getStrengthMedium();
//                weekLowTimeSum += preDayActivity.getStrengthMedium();
//                weekUnderLowTimeSum += preDayActivity.getStrengthUnderLow();
//                weekCalorieSum += preDayActivity.getCalorieByActivity();
//                weekMETSum += preDayActivity.getAverageMET();
//                calorieList.add(preDayActivity.getCalorieByActivity());
//
//                List<Integer> achieveOfTageList =  new ArrayList<Integer>();
//                achieveOfTageList.add( preDayActivity.getAchieveOfTarget() );
//                weekContinuousExerciseList.add( achieveOfTageList );
//
//            } //End For
//        }
//
//        //그 날짜의 주간 첫날과 일 목록의 첫날까지의 일 활동량을 디비에서 가져온다.
//        //일을 업데이트한다.
//        for( int i = 0 ; i < dayActivityList.size() ;i++ )
//        {
//
//            DayActivity dayActivity = dayActivityList.get(i);
//
//            this.mDBManager.updateDayActivity(
//                    dayActivity.getActivityDate() ,
//                    dayActivity.getCalorieByActivity() ,
//                    dayActivity.getStrengthUnderLow() ,
//                    dayActivity.getStrengthLow() ,
//                    dayActivity.getStrengthMedium() ,
//                    dayActivity.getStrengthHigh() ,
//                    dayActivity.getMetArray() ,
//                    dayActivity.getAchieveOfTarget() ,
//                    dayActivity.getAverageMET() ,
//                    dayActivity.getStartTime().getTimeString() ,
//                    dayActivity.getDataSaveInterval() ,
//                    policy
//            );
//
//            weekCalendar.setTimeInMillis( dayActivity.getStartTime().getMilliSecond() );
//            int weekNumber = weekCalendar.get( Calendar.WEEK_OF_YEAR );
//
//            weekHighTimeSum += dayActivity.getStrengthHigh();
//            weekMediumTimeSum += dayActivity.getStrengthMedium();
//            weekLowTimeSum += dayActivity.getStrengthMedium();
//            weekUnderLowTimeSum += dayActivity.getStrengthUnderLow();
//            weekCalorieSum += dayActivity.getCalorieByActivity();
//            weekMETSum += dayActivity.getAverageMET();
//            calorieList.add(dayActivity.getCalorieByActivity());
//
//            List<Integer> achieveOfTageList =  new ArrayList<Integer>();
//            achieveOfTageList.add( dayActivity.getAchieveOfTarget() );
//            weekContinuousExerciseList.add( achieveOfTageList );
//
//            //다음주로 넘어가거나 마지막 데이터 일 때 지금까지 모아놓은 데이터로 주 활동량을 계산한다.
//            if( weekNumber > preWeekNumber  || i == ( dayActivityList.size() - 1 ) ){
//
//                if( i == (dayActivityList.size() - 1 ) ){
//                    weekActivityList.add( dayActivity );
//                }
//
//                ExerciseAnalyzer exerciseAnalyzer = new ExerciseAnalyzer( appliedExerciseProgram , userInfoForAnalyzer , WearingLocation.WRIST , dayActivity.getDataSaveInterval() );
//
//                //점수를 계산한다.
//                int weekAchieve = 0;
//                if( appliedExerciseProgram.getStrengthInputType().equals(StrengthInputType.CALORIE ) ) {
//                    weekAchieve = exerciseAnalyzer.CalculateWeekAchievement_Calorie(calorieList);
//                } else if(appliedExerciseProgram.getStrengthInputType().equals(StrengthInputType.CALORIE_SUM)) {
//                    weekAchieve = exerciseAnalyzer.CalculateWeekAchievement_CalorieSum(weekCalorieSum);
//                } else {
//                    weekAchieve = exerciseAnalyzer.CalcuateWeekAchieveByMinutesList( policy, weekContinuousExerciseList );
//                }
//
//                int size = weekActivityList.size();
//                int averageWeekHighTime = weekHighTimeSum / size;
//                int averageWeekMediumTime = weekMediumTimeSum / size;
//                int averageWeekLowTime = weekLowTimeSum / size;
//                int averageWeekUnderLowTime = weekUnderLowTimeSum / size;
//                int averageWeekCalorie = weekCalorieSum /size;
//                float averageWeekAverageMet = weekMETSum / size;
//
//                Log.d(TAG, " 평균 고강도 시간 : " + averageWeekHighTime );
//                Log.d(TAG, " 평균 중간도 시간 : " + averageWeekMediumTime );
//                Log.d(TAG, " 평균 저강도 시간 : " + averageWeekLowTime );
//                Log.d(TAG, " 평균 저강도 미만 시간 : " + averageWeekUnderLowTime );
//                Log.d(TAG, " 평균 칼로리 : " + averageWeekCalorie );
//                Log.d(TAG, " 평균 MET : " + averageWeekAverageMet );
//                Log.d(TAG, " 점수 : " + weekAchieve );
//
//                this.mDBManager.updateWeekActivity(weekStartDate, averageWeekCalorie, weekAchieve, averageWeekHighTime, averageWeekMediumTime, averageWeekLowTime, averageWeekUnderLowTime , averageWeekAverageMet);
//
//                //주 시작일을 가져온다.
//                weekCalendar.setTimeInMillis(dayActivity.getStartTime().getMilliSecond());
//                dayOfWeek = weekCalendar.get( Calendar.DAY_OF_WEEK );
//                weekCalendar.add(Calendar.DATE , -( dayOfWeek - 1 ) );
//                weekStartDate = this.getDateString_yyyy_MM_dd( weekCalendar );
//                Log.d(TAG, "주 시작일 : " + weekStartDate );
//
//                //초기화
//                weekMediumTimeSum = 0;
//                weekHighTimeSum = 0;
//                weekLowTimeSum = 0;
//                weekUnderLowTimeSum = 0;
//                weekCalorieSum = 0;
//                weekMETSum = 0;
//                calorieList.clear();
//                weekActivityList.clear();
//                weekContinuousExerciseList.clear();
//            }
//
//            weekActivityList.add( dayActivity );
//            preWeekNumber = weekNumber;
//            preDataSaveInterval = dayActivity.getDataSaveInterval();
//
//        } //END FOR
//
//
//        FitmateServiceStarter.queueUsed =  false;
//        this.mListener.onCalculateCompleted();
//        if( mNotifyCalculated ) {
//            this.sendServiceMessage();
//        }
//    }
//
//    private String getDateString_yyyy_MM_dd( Calendar calendar )
//    {
//        return calendar.get(Calendar.YEAR) + "-" +String.format("%02d", calendar.get(Calendar.MONTH) + 1) + "-" + String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH));
//    }
//
//    private void sendServiceMessage(){
//        Message msg = Message.obtain(null, MessageType.MSG_CALCULATE_ACTIVITY);
//
//        try {
//
//            this.mResponseMessenger.send(msg);
//
//        } catch (RemoteException e) {
//        }
//    }
//
//    private void setLastFileIndex( int fileIndex ){
//        SharedPreferences pref = this.mContext.getSharedPreferences( "fitmateservice" , this.mContext.MODE_PRIVATE);
//        SharedPreferences.Editor editor = pref.edit();
//        editor.putInt( this.LAST_FILE_KEY  , fileIndex);
//        editor.commit();
//    }
//
//    private int getLastFileIndex( ){
//        SharedPreferences pref = this.mContext.getSharedPreferences( "fitmateservice" , this.mContext.MODE_PRIVATE);
//        SharedPreferences.Editor editor = pref.edit();
//        return pref.getInt( this.LAST_FILE_KEY , -1 );
//    }
//
//    private byte[] readLastDataFile( ){
//
//        byte[] dataFile = null;
//
//        try {
//            FileInputStream fis = this.mContext.openFileInput( "lastfile.dat" );
//            dataFile = new byte[fis.available()];
//
//            fis.read(dataFile);
//            fis.close();
//
//        } catch (FileNotFoundException e) {
//            Log.e(TAG , "마지막 파일을 읽는 중에 에러 발생 : 파일이 없습니다." + e.getMessage());
//        } catch (IOException e) {
//            Log.e(TAG , "마지막 파일을 읽는 중에 에러 발생 : " + e.getMessage());
//        }
//
//        return dataFile;
//    }
//
//    private void writeLastDataFile( byte[] writeBytes ){
//
//        if( writeBytes == null ) return;
//
//        try {
//
//            FileOutputStream fos =  this.mContext.openFileOutput( "lastfile.dat" , Context.MODE_PRIVATE );
//            fos.write( writeBytes );
//            fos.close();
//
//        } catch (FileNotFoundException e) {
//            Log.e(TAG , "마지막 파일을 쓰는 중에 에러 발생 : 파일이 없습니다." + e.getMessage());
//        } catch (IOException e) {
//            Log.e(TAG , "마지막 파일을 쓰는 중에 에러 발생 : " + e.getMessage());
//        }
//    }
//
//
//    private void writeTestFile( byte[] writeBytes , int fileName ){
//
//        try {
//            String saPath = Environment.getExternalStorageDirectory().getAbsolutePath();
//
//            File dir = new File( saPath + "/dir" );
//            if( !dir.exists() ) {
//                dir.mkdir();
//            }
//
//            File file = new File(saPath + "/dir/" + fileName + ".dat");
//
//            if( file.exists() ){
//
//                FileInputStream fis = new FileInputStream( file );
//                byte[] readBytes = new byte[ fis.available() ];
//                fis.read( readBytes );
//
//                byte[] tempBytes = new byte[ writeBytes.length + readBytes.length ];
//
//                System.arraycopy( readBytes , 0, tempBytes , 0, readBytes.length );
//                System.arraycopy( writeBytes , 0 , tempBytes , readBytes.length , writeBytes.length);
//
//                writeBytes = tempBytes;
//            }
//
//            FileOutputStream out = new FileOutputStream(file);
//            out.write( writeBytes );
//            out.close();
//
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//    }
//
//    private byte[] readTestFile(int fileIndex){
//        String saPath = Environment.getExternalStorageDirectory().getAbsolutePath();
//
//        File dir = new File( saPath + "/dir");
//        if( !dir.exists() ){
//            dir.mkdir();
//        }
//
//        File lastFile = new File( saPath + "/dir/lastfile" + fileIndex + ".dat" );
//
//        byte[] resultBytes = null;
//
//        try {
//            FileInputStream fis = new FileInputStream(lastFile);
//            resultBytes = new byte[ fis.available() ];
//            fis.read( resultBytes );
//
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        return resultBytes;
//    }
//
//    private int getPreviousCalorie(){
//        SharedPreferences pref = this.mContext.getSharedPreferences( "fitmateservice" , this.mContext.MODE_PRIVATE);
//        SharedPreferences.Editor editor = pref.edit();
//        return pref.getInt( this.LAST_FILE_CALORIE_KEY , 0 );
//    }
//
//    private void setPreviousCalorie( int calorie ){
//        SharedPreferences pref = this.mContext.getSharedPreferences( "fitmateservice" , this.mContext.MODE_PRIVATE);
//        SharedPreferences.Editor editor = pref.edit();
//        editor.putInt( this.LAST_FILE_CALORIE_KEY  , calorie );
//        editor.commit();
//    }
//
//    private int getPreviousStrengthHigh(){
//        SharedPreferences pref = this.mContext.getSharedPreferences( "fitmateservice" , this.mContext.MODE_PRIVATE);
//        SharedPreferences.Editor editor = pref.edit();
//        return pref.getInt( this.LAST_FILE_STRENGTH_HIGH_KEY , 0 );
//    }
//
//    private void setPreviousStrengthHigh( int strengthHigh ){
//        SharedPreferences pref = this.mContext.getSharedPreferences( "fitmateservice" , this.mContext.MODE_PRIVATE);
//        SharedPreferences.Editor editor = pref.edit();
//        editor.putInt( this.LAST_FILE_STRENGTH_HIGH_KEY  , strengthHigh );
//        editor.commit();
//    }
//
//    private int getPreviousStrengthMedium(){
//        SharedPreferences pref = this.mContext.getSharedPreferences( "fitmateservice" , this.mContext.MODE_PRIVATE);
//        SharedPreferences.Editor editor = pref.edit();
//        return pref.getInt( this.LAST_FILE_STRENGTH_MEDIUM_KEY , 0 );
//    }
//
//    private void setPreviousStrengthMedium( int strengthMedium ){
//        SharedPreferences pref = this.mContext.getSharedPreferences( "fitmateservice" , this.mContext.MODE_PRIVATE);
//        SharedPreferences.Editor editor = pref.edit();
//        editor.putInt( this.LAST_FILE_STRENGTH_MEDIUM_KEY  , strengthMedium );
//        editor.commit();
//    }
//
//    private int getPreviousStrengthLow(){
//        SharedPreferences pref = this.mContext.getSharedPreferences( "fitmateservice" , this.mContext.MODE_PRIVATE);
//        SharedPreferences.Editor editor = pref.edit();
//        return pref.getInt( this.LAST_FILE_STRENGTH_LOW_KEY , 0 );
//    }
//
//    private void setPreviousStrengthLow( int strengthLow ){
//        SharedPreferences pref = this.mContext.getSharedPreferences( "fitmateservice" , this.mContext.MODE_PRIVATE);
//        SharedPreferences.Editor editor = pref.edit();
//        editor.putInt( this.LAST_FILE_STRENGTH_LOW_KEY  , strengthLow );
//        editor.commit();
//    }
//
//    private int getPreviousStrengthUnderLow(){
//        SharedPreferences pref = this.mContext.getSharedPreferences( "fitmateservice" , this.mContext.MODE_PRIVATE);
//        SharedPreferences.Editor editor = pref.edit();
//        return pref.getInt( this.LAST_FILE_STRENGTH_UNDERLOW_KEY , 0 );
//    }
//
//    private void setPreviousStrengthUnderLow( int strengthUnderLow ){
//        SharedPreferences pref = this.mContext.getSharedPreferences( "fitmateservice" , this.mContext.MODE_PRIVATE);
//        SharedPreferences.Editor editor = pref.edit();
//        editor.putInt( this.LAST_FILE_STRENGTH_UNDERLOW_KEY  , strengthUnderLow );
//        editor.commit();
//    }
//
//
//}
//
////        for( int i = 0 ; i < rawDataList.size() ;i++ ){
////
////            BluetoothRawData rawData = rawDataList.get(i);
////
////            //if( lastFileIndex == rawData.getFileIndex()){
////
////                //byte[] readBytes = this.readTestFile( rawData.getFileIndex() );
////                //byte[] tempBytes = new byte[ readBytes.length + rawData.getFileData().length ];
////                //System.arraycopy(readBytes , 0 , tempBytes , 0 , readBytes.length);
////                //System.arraycopy( rawData.getFileData() , 0 , tempBytes , readBytes.length , rawData.getFileData().length );
////
////                this.writeTestFile( rawData.getFileData() , rawData.getFileIndex() );
////
////            //}
////
////            if(i == rawDataList.size() - 1){
////                this.setLastFileIndex( rawDataList.get(i).getFileIndex() );
////                Log.d(TAG , "마지막 파일 인덱스 저장 : " + i );
////            }
////        }
////
////        Log.d(TAG , "저장 완료" );
