package com.fitdotlife.fitmate_lib.service.key;

/**
 * Created by Joshua on 2015-04-16.
 */
public enum SyncType {
    PROGRESS(0),
    END(1);

    private int mValue;

    SyncType( int value )
    {
        this.mValue = value;
    }

    public int getValue()
    {
        return this.mValue;
    }

    public static SyncType getSyncType( int value )
    {
        SyncType syncType = null;

        switch( value )
        {
            case 0:
                syncType = SyncType.PROGRESS;
                break;
            case 1:
                syncType = SyncType.END;
                break;
        }

        return syncType;
    }
}
