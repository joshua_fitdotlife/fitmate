package com.fitdotlife.fitmate_lib.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.Messenger;
import android.util.Log;

import com.fitdotlife.fitmate_lib.database.FitmateDBManager;

public class FitmateService extends Service
{
    private final String TAG = "fitmateservice";
    private FitmateDBManager mDBManager = null;

    private Messenger mMessenger = null;
    private FitmateServiceStarter mStarter = null;
    private InComingMessageManager mIncomingMessageManager = null;

    @Override
    public void onCreate()
    {
        Log.d(TAG , "onCreate()");

        //android.os.Debug.waitForDebugger();  // this line is key
    	super.onCreate();

        this.mDBManager = new FitmateDBManager( this.getApplicationContext() );
        if( mDBManager == null  ){
            Log.e(TAG , "dbManager is null");
        }
        this.mIncomingMessageManager = new InComingMessageManager( this.getApplicationContext() ,  this.mDBManager );
        this.mMessenger = new Messenger( this.mIncomingMessageManager );
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
    	Log.d(TAG , "onStartCommand()");
        super.onStartCommand(intent, flags, startId);

        if( this.mStarter == null ) {

            this.mStarter = new FitmateServiceStarter(this.getApplicationContext() , this.mDBManager , mIncomingMessageManager);
            this.mIncomingMessageManager.setStarter( mStarter );
        }

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy()");
        if (this.mStarter != null){
            this.mStarter.close();
        }
    	super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent)
    {
    	Log.d(TAG , "onBind()");
        return this.mMessenger.getBinder();
    }

    @Override
    public boolean onUnbind( Intent intent )
    {
    	Log.d(TAG , "onUnBind()");
    	return super.onUnbind(intent);
    }
}
