//package com.fitdotlife.fitmate.service;
//
//import android.content.Context;
//import android.content.SharedPreferences;
//import android.os.Environment;
//import android.os.Message;
//import android.os.Messenger;
//import android.os.RemoteException;
//import android.util.Log;
//
//import com.fitdotlife.exerciseanalysis.ContinuousCheckPolicy;
//import com.fitdotlife.exerciseanalysis.ContinuousExerciseInfo;
//import com.fitdotlife.exerciseanalysis.ExerciseAnalyzer;
//import com.fitdotlife.exerciseanalysis.ExerciseProgram;
//import com.fitdotlife.exerciseanalysis.StrengthInputType;
//import com.fitdotlife.exerciseanalysis.USERVO2MAXLEVEL;
//import com.fitdotlife.exerciseanalysis.UserInfoForAnalyzer;
//import com.fitdotlife.exerciseanalysis.WearingLocation;
//import com.fitdotlife.fitmate.database.FitmateDBManager;
//import com.fitdotlife.fitmate.object.DayActivity;
//import com.fitdotlife.fitmate.object.UserInfo;
//import com.fitdotlife.fitmate.service.activity.ActivityParser;
//import com.fitdotlife.fitmate.service.activity.DataParseException;
//import com.fitdotlife.fitmate.service.bluetooth.BluetoothRawData;
//import com.fitdotlife.fitmate.service.key.MessageType;
//import com.fitdotlife.fitmate.service.protocol.object.ActivityData;
//import com.fitdotlife.fitmate.service.util.QueueList;
//
//import java.io.File;
//import java.io.FileNotFoundException;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.List;
//
///**
// * Created by Joshua on 2015-03-16.
// */
//public class DataCalculator_OLD extends  Thread {
//
//    private final String TAG = "fitmateservice";
//    private final String LAST_FILE_KEY = "lastfile";
//    private static final String LAST_DAY_FILE_INDEX_KEY = "lastdayfileindex";
//
//    private FitmateDBManager mDBManager = null;
//    private QueueList<BluetoothRawData> mDataQueue = null;
//
//    private boolean mCalculated = false;
//    private boolean mNotifyCalculated = false;
//
//    private Messenger mResponseMessenger = null;
//    private Context mContext = null;
//
//    public DataCalculator_OLD(Context context, FitmateDBManager dbManager, QueueList<BluetoothRawData> dataQueue){
//        this.mDBManager = dbManager;
//        this.mDataQueue = dataQueue;
//    }
//
//    public boolean isCalculated(){
//        return this.mCalculated;
//    }
//
//    public void setNotifyCalculateComplete(  boolean notifyCalculated ){
//        this.mNotifyCalculated = notifyCalculated;
//    }
//
//    public void setResponseMessenger( Messenger responseMessenger ){
//        this.mResponseMessenger = responseMessenger;
//    }
//
//    public void run(){
//
//        //큐에 데이터가 없다면 그냥 반환한다.
//        if( this.mDataQueue.isEmpty() ) {
//            if( this.mNotifyCalculated ) {this.sendServiceMessage();}
//            return;
//        }
//
//        this.mCalculated = true;
//
//        ContinuousCheckPolicy policy = ContinuousCheckPolicy.Strictly;
//        ExerciseProgram appliedExerciseProgram = this.mDBManager.getAppliedExerciseProgram();
//        UserInfo userInfo = this.mDBManager.getUserInfo();
//        UserInfoForAnalyzer userInfoForAnalyzer = new UserInfoForAnalyzer(userInfo.getAge() , userInfo.getGender().getBoolean() , userInfo.getHeight() , userInfo.getWeight() , ContinuousCheckPolicy.Strictly , 1 , USERVO2MAXLEVEL.Fair );
//
//        //큐에서 데이터를 가져온다.
//        List<BluetoothRawData> rawDataList = mDataQueue.dequeueAllData();
//        ActivityParser activityParser = new ActivityParser();
//
//        //마지막 데이터 파일의 인덱스를 가져온다.
//        int lastFileIndex = this.getLastFileIndex();
//
//        List<Integer> removeIndexList = new ArrayList<Integer>();
//        //파일 인덱스와 데이터 파일의 인덱스를 비교하여 같으면 바이트 배열을 더한다.
//
//        for( int i = 0 ; i < rawDataList.size() ; i++ )
//        {
//            if (lastFileIndex == rawDataList.get(i).getFileIndex()) {
//                //현재 저장되어 있는 바이트 배열에 붙인다.
//                this.appendLastDataFile(rawDataList.get(i).getFileData());
//                removeIndexList.add( i );
//            }
//        }
//
//        //더하여진 파일 인덱스를 rawDataList에서 삭제한다.
//        for( int i = 0 ; i < removeIndexList.size() ;i++ ){
//            rawDataList.remove( removeIndexList.get(i) );
//        }
//
//        byte[] lastDayFileBytes = this.readLastDataFile();
//        int[]  lastDayFileSliceIndexs = this.writeLastDaySliceFileIndexs();
//        int[] lastDayFileIndexs = this.getLastDayFileIndexs();
//        int sliceIndex = 0;
//
//        //읽어온 바이트 배열을 데이터 파일별로 나눈다.
//        for( int i = 0 ; i < lastDayFileSliceIndexs.length ; i++){
//
//            byte[] tempBytes = new byte[ lastDayFileSliceIndexs[i] ];
//            System.arraycopy( lastDayFileBytes , sliceIndex , tempBytes , 0 , lastDayFileSliceIndexs[i] );
//
//            try {
//                activityParser.parse( new BluetoothRawData( lastDayFileIndexs[i] , tempBytes ) );
//            } catch (DataParseException e) { }
//
//        }
//
//        //데이터 파일별로 분석한다.
//        for( int i = 0 ; i < rawDataList.size() ;i++ )
//        {
//            try {
//                activityParser.parse( rawDataList.get(i) );
//
//            } catch (DataParseException e) {
//                Log.e(TAG , "파일을 분석하는 중에 오류가 발생하였습니다. : " + e.getMessage());
//                continue;
//            }
//        }
//
//
//        List<ActivityData> activityDataList = activityParser.getActivityData();
//        Log.d(TAG, "Activity Data 목록의 크기 : " + activityDataList.size());
//        this.setLastFileIndex( activityDataList.get( activityDataList.size()  - 1 ).getRawFileIndex() );
//
//        List<byte[]> lastDayBytes = new ArrayList<byte[]>();
//        List<Integer> lastDayIndex = new ArrayList<Integer>();
//
//        //날짜별로 데이터를 만든다.
//        //날짜에 따른 칼로리, MET , 저중고 운동시간, 평균 MET , 운동시간을 계산한다.
//        //계산되어진 활동량 정보를 날짜목록에 저장한다.
//        List<DayActivity> dayActivityList = new ArrayList<DayActivity>();
//        for( int index = 0 ; index < activityDataList.size() ;index++ ){
//
//            ActivityData activityData = activityDataList.get(index);
//            ExerciseAnalyzer analyzer = new ExerciseAnalyzer( appliedExerciseProgram , userInfoForAnalyzer , WearingLocation.WRIST , (int) activityData.getSaveInterval() );
//
//            List<Integer> svmList = activityData.getSVMList();
//
//            int calorieByActivity = 0;
//            float[] metArray = new float[ svmList.size() ];
//
//            //칼로리와 MET를 계산한다.
//            for( int i = 0 ; i < svmList.size() ; i++ ){
//
//                int calorie = analyzer.Calcuate_Calorie( svmList.get(i) );
//                calorieByActivity += calorie;
//                metArray[i] = analyzer.Calculate_METbyCaloire( calorie );
//            }
//
//            //저중고 강도 운동시간을 계산한다.
//            int[] arrHMLTime = analyzer.MinutesforEachStrength( metArray );
//            DayActivity dayActivity = new DayActivity( activityData.getStartTime() , calorieByActivity , arrHMLTime , metArray , (int)activityData.getSaveInterval()  );
//
//            //평균 MET를 계산한다.
//            dayActivity.setAverageMET( analyzer.CalculateAverageMET( dayActivity.getMetArray() ) );
//
//            //AchieveOfTarget 를 계산한다.
//            List<ContinuousExerciseInfo> todayExerciseInfoList = analyzer.getContinuousExercise( policy, metArray , dayActivity.getStartTime().getMilliSecond() , 1, 1, 0, 0 );
//            int todayDayOfAchieve = 0;
//            if( todayExerciseInfoList.size() > 0 ){
//                ContinuousExerciseInfo todayExerciseInfo = todayExerciseInfoList.get(0);
//                todayDayOfAchieve = Math.round(( todayExerciseInfo.EndTime - todayExerciseInfo.StartTime ) / ( 60 * 1000 ) );
//            }
//            dayActivity.setAchieveOfTarget( todayDayOfAchieve );
//
//            //이전데이터와 겹치는지 확인한다.
//            if( dayActivityList.size() > 0 ){
//
//                //최근에 날짜목록에 저장된 데이터를 가져온다. 날짜목록에 있는 데이터의 날짜와 현재의 날짜가 겹치는 확인한다.
//                //저장간격이 같은지 확인한다.
//                //날짜도 같고 저장간격도 같다면 데이터를 합친다.
//                DayActivity preDayActivity = dayActivityList.get( dayActivityList.size() - 1 );
//
//                if( dayActivity.getActivityDate() .equals( preDayActivity.getActivityDate() ) ){ //날짜가 겹치는지 확인한다.
//                    if( dayActivity.getDataSaveInterval() == preDayActivity.getDataSaveInterval() ){ //저장간격이 겹치는지 확인한다.
//
//                        preDayActivity.add(dayActivity , policy);
//                        continue;
//                    }
//                }else{
//                    lastDayBytes.clear();
//                    lastDayIndex.clear();
//                }
//            }
//
//            dayActivityList.add( dayActivity );
//
//        }//END FOR
//
//
//
//
//        //마지막 날짜의 시작 파일 인덱스를 저장한다.
//        Log.d(TAG, "Day Activity 목록의 크기 : " + dayActivityList.size());
//
//        //####
//        // 위에서 일별 활동량 목록을 계산을 다 했다.
//        // 주 활동량을 계산 해야 하는데 먼저 각 날짜가 어느 주에 속하는지 알아야 한다.
//        // 각 날짜는 52주 중에 한 주에 해당할 것이다. 각 날짜의 주 숫자를 알아내어서 주활동량을 계산하게 된다.
//        // 그리고, 위의 일 활동량 목록의 첫날이 주중 요일이라면 주의 처음부터 데이터가 필요하다.
//        //예를 들면 일 활동량 목록이 첫날이 화요일 이라면 주의 처음부터 화요일까지 데이터가 필요하다.
//        //그래서 아래의 코드는 일 활동량 목록의 첫날이 무슨 요일인지 알아내고 데이터베이스로부터 이 날짜가 속한 주 활동량을 가져와서 먼저 계산하도록 한다.
//        //####
//
//        //일 활동량 목록에서 첫날이 속한 요일의 시작일 요일의 종료일 가져온다.
//        int preDataSaveInterval = 0;
//        Calendar weekCalendar = Calendar.getInstance();
//        weekCalendar.setTimeInMillis( dayActivityList.get(0).getStartTime().getMilliSecond() );
//        int preWeekNumber = weekCalendar.get( Calendar.WEEK_OF_YEAR );
//        Log.d(TAG, "주의 숫자 : " + preWeekNumber );
//
//        int dayOfWeek = weekCalendar.get( Calendar.DAY_OF_WEEK );
//        weekCalendar.add(Calendar.DATE , -( dayOfWeek - 1 ) );
//        String weekStartDate = this.getDateString_yyyy_MM_dd( weekCalendar );
//        Log.d(TAG, "주 시작일 : " + weekStartDate );
//
//        weekCalendar.add(Calendar.DATE , 6);
//        String weekEndDate = this.getDateString_yyyy_MM_dd( weekCalendar );
//        Log.d(TAG, "주 종료일 : " + weekEndDate );
//
//        int weekHighTimeSum = 0;
//        int weekMediumTimeSum = 0;
//        int weekLowTimeSum = 0;
//        int weekUnderLowTimeSum = 0;
//        int weekCalorieSum = 0;
//        float weekMETSum = 0;
//
//        List<Integer> calorieList = new ArrayList<Integer>();
//        List<List<Integer>> weekContinuousExerciseList = new ArrayList<List<Integer>>();
//
//        List<DayActivity> weekActivityList = this.mDBManager.getDayActivityList( weekStartDate , weekEndDate );
//        Log.d(TAG, "데이터베이스로부터 가져온 첫번째 날짜가 해당하는 주의 일 활동량 목록 크기 : " + weekActivityList.size() );
//
//        DayActivity firstDayActivity = dayActivityList.get(0);
//
//        for( int i = 0 ; i < weekActivityList.size() ;i++ ){
//
//            preDataSaveInterval = weekActivityList.get(i).getDataSaveInterval();
//            DayActivity preDayActivity = weekActivityList.get(i);
//
//            //휘트미터로부터 읽어온 일별 데이터 중 첫번째 데이터의 날짜와 같은 데이터가 데이터베이스에 있을 수 있다.
//            //온종일 데이터를 저장하지 않을 수 있기때문에
//            //여기서 날짜가 겹치는지 확인한다.
//            if( firstDayActivity.getActivityDate().equals(preDayActivity.getActivityDate()) ){
//                Log.d(TAG, "데이터베이스로부터 가져온 일 활동량과 현재 목록의 일 활동량이 겹침. : " + weekActivityList.size() );
//                weekActivityList.get(i).add(firstDayActivity , policy);
//                dayActivityList.remove(0);
//                break;
//            }
//
//            weekHighTimeSum += preDayActivity.getStrengthHigh();
//            weekMediumTimeSum += preDayActivity.getStrengthMedium();
//            weekLowTimeSum += preDayActivity.getStrengthMedium();
//            weekUnderLowTimeSum += preDayActivity.getStrengthUnderLow();
//            weekCalorieSum += preDayActivity.getCalorieByActivity();
//            weekMETSum += preDayActivity.getAverageMET();
//            calorieList.add( preDayActivity.getCalorieByActivity() );
//
//        } //End For
//
//
//        //그 날짜의 주간 첫날과 일 목록의 첫날까지의 일 활동량을 디비에서 가져온다.
//        //일을 업데이트한다.
//        for( int i = 0 ; i < dayActivityList.size() ;i++ )
//        {
//
//            DayActivity dayActivity = dayActivityList.get(i);
//
//            this.mDBManager.updateDayActivity(
//                    dayActivity.getActivityDate() ,
//                    dayActivity.getCalorieByActivity() ,
//                    dayActivity.getStrengthUnderLow() ,
//                    dayActivity.getStrengthLow() ,
//                    dayActivity.getStrengthMedium() ,
//                    dayActivity.getStrengthHigh() ,
//                    dayActivity.getMetByteArray() ,
//                    dayActivity.getAchieveOfTarget() ,
//                    dayActivity.getAverageMET() ,
//                    policy
//            );
//
//            weekCalendar.setTimeInMillis( dayActivity.getStartTime().getMilliSecond() );
//            int weekNumber = weekCalendar.get( Calendar.WEEK_OF_MONTH );
//
//            weekHighTimeSum += dayActivity.getStrengthHigh();
//            weekMediumTimeSum += dayActivity.getStrengthMedium();
//            weekLowTimeSum += dayActivity.getStrengthMedium();
//            weekUnderLowTimeSum += dayActivity.getStrengthUnderLow();
//            weekCalorieSum += dayActivity.getCalorieByActivity();
//            weekMETSum += dayActivity.getAverageMET();
//            calorieList.add(dayActivity.getCalorieByActivity());
//
//            List<Integer> achieveOfTageList =  new ArrayList<Integer>();
//            achieveOfTageList.add(dayActivity.getAchieveOfTarget());
//            weekContinuousExerciseList.add( achieveOfTageList );
//
//            //다음주로 넘어가면 지금까지 모아놓은 데이터로 주 활동량을 계산한다.
//            if( weekNumber > preWeekNumber ){
//
//                ExerciseAnalyzer exerciseAnalyzer = new ExerciseAnalyzer( appliedExerciseProgram , userInfoForAnalyzer , WearingLocation.WRIST , preDataSaveInterval );
//
//                //점수를 계산한다.
//                int weekAchieve = 0;
//                if( appliedExerciseProgram.getStrengthInputType().equals(StrengthInputType.CALORIE ) ) {
//                    weekAchieve = exerciseAnalyzer.CalculateWeekAchievement_Calorie(calorieList);
//                } else if(appliedExerciseProgram.getStrengthInputType().equals(StrengthInputType.TOTAL_CALORIE)) {
//                    weekAchieve = exerciseAnalyzer.CalculateWeekAchievement_CalorieSum(weekCalorieSum);
//                } else {
//                    weekAchieve = exerciseAnalyzer.CalcuateWeekAchieveByMinutesList(policy, weekContinuousExerciseList);
//                }
//
//                int size = weekActivityList.size();
//                int averageWeekHighTime = weekHighTimeSum / size;
//                int averageWeekMediumTime = weekMediumTimeSum / size;
//                int averageWeekLowTime = weekLowTimeSum / size;
//                int averageWeekUnderLowTime = weekUnderLowTimeSum / size;
//                int averageWeekCalorie = weekCalorieSum /size;
//                float averageWeekAverageMet = weekMETSum / size;
//
//                Log.d(TAG, " 평균 고강도 시간 : " + averageWeekHighTime );
//                Log.d(TAG, " 평균 중간도 시간 : " + averageWeekMediumTime );
//                Log.d(TAG, " 평균 저강도 시간 : " + averageWeekLowTime );
//                Log.d(TAG, " 평균 저강도 미만 시간 : " + averageWeekUnderLowTime );
//                Log.d(TAG, " 평균 칼로리 : " + averageWeekCalorie );
//                Log.d(TAG, " 평균 MET : " + averageWeekAverageMet );
//                Log.d(TAG, " 점수 : " + weekAchieve );
//
//                this.mDBManager.updateWeekActivity(weekStartDate, averageWeekCalorie, weekAchieve, averageWeekHighTime, averageWeekMediumTime, averageWeekLowTime, averageWeekUnderLowTime , averageWeekAverageMet);
//
//                //주 시작일을 가져온다.
//                weekCalendar.setTimeInMillis(dayActivity.getStartTime().getMilliSecond());
//                dayOfWeek = weekCalendar.get( Calendar.DAY_OF_WEEK );
//                weekCalendar.add(Calendar.DATE , -( dayOfWeek - 1 ) );
//                weekStartDate = this.getDateString_yyyy_MM_dd( weekCalendar );
//                Log.d(TAG, "주 시작일 : " + weekStartDate );
//
//                //초기화
//                weekMediumTimeSum = 0;
//                weekHighTimeSum = 0;
//                weekLowTimeSum = 0;
//                weekUnderLowTimeSum = 0;
//                weekCalorieSum = 0;
//                weekMETSum = 0;
//                calorieList.clear();
//                weekActivityList.clear();
//            }
//
//            weekActivityList.add( dayActivity );
//            preWeekNumber = weekNumber;
//            preDataSaveInterval = dayActivity.getDataSaveInterval();
//
//        } //END FOR
//
//        this.mCalculated = false;
//
//        if( mNotifyCalculated ) {
//            this.sendServiceMessage();
//        }
//    }
//
//    private int[] getLastDayFileIndexs() {
//        return new int[0];
//    }
//
//
//    private String getDateString_yyyy_MM_dd( Calendar calendar )
//    {
//        return calendar.get(Calendar.YEAR) + "-" +String.format("%02d", calendar.get(Calendar.MONTH) + 1) + "-" + String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH));
//    }
//
//    private void sendServiceMessage(){
//        Message msg = Message.obtain(null, MessageType.MSG_CALCULATE_ACTIVITY);
//
//        try {
//
//            this.mResponseMessenger.send(msg);
//
//        } catch (RemoteException e) {
//        }
//    }
//
//    private void setLastFileIndex( int fileIndex ){
//        SharedPreferences pref = this.mContext.getSharedPreferences( "fitmateservice" , this.mContext.MODE_PRIVATE);
//        SharedPreferences.Editor editor = pref.edit();
//        editor.putInt( this.LAST_FILE_KEY  , fileIndex);
//        editor.commit();
//    }
//
//    private int getLastFileIndex( ){
//        SharedPreferences pref = this.mContext.getSharedPreferences( "fitmateservice" , this.mContext.MODE_PRIVATE);
//        SharedPreferences.Editor editor = pref.edit();
//        return pref.getInt( this.LAST_FILE_KEY , 0 );
//    }
//
//    private int[] writeLastDaySliceFileIndexs() {
//        return null;
//    }
//
//    private void readLastDaySliceFileIndex(int[] sliceIndex){
//
//    }
//
//    private void appendLastDataFile(byte[] rawData){
//
//    }
//
//    private byte[] readLastDataFile(){
//
//        byte[] dataFile = null;
//
//        return dataFile;
//    }
//
//    private void writeLastDataFile( byte[] writeBytes , String fileName ){
//
//        try {
//            String saPath = Environment.getExternalStorageDirectory().getAbsolutePath();
//
//            File dir = new File( saPath + "/dir" );
//            if( !dir.exists() ) {
//                dir.mkdir();
//            }
//
//            File file = new File(saPath + "/dir/" + fileName + ".dat");
//
//            FileOutputStream out = new FileOutputStream(file);
//            out.write(writeBytes);
//            out.close();
//
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//}
