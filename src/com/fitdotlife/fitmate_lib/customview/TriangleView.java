package com.fitdotlife.fitmate_lib.customview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;

import com.fitdotlife.fitmate.R;

public class TriangleView extends View {

	private Path trianglePath = new Path();
	private Paint trianglePaint = new Paint();
	private int mColor = 0;
	
	public TriangleView(Context context, AttributeSet attrs)
	{
		super( context , attrs );
		TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.TriangleView , 0, 0);
		
		try
		{
			this.mColor = a.getColor( R.styleable.TriangleView_triangleColor , Color.BLUE );
		}
		finally
		{
			a.recycle();
		}
		
		this.setBackgroundColor( Color.TRANSPARENT );
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) 
	{
		int heightMode = MeasureSpec.getMode(heightMeasureSpec);
		int heightSize = 0;
		
		switch( heightMode )
		{
			case MeasureSpec.UNSPECIFIED:
				heightSize = heightMeasureSpec;
				break;
			case MeasureSpec.AT_MOST:
				heightSize = 200;
				break;
			case MeasureSpec.EXACTLY:
				heightSize = MeasureSpec.getSize(heightMeasureSpec);
				break;
		}
		
		int widthMode = MeasureSpec.getMode(widthMeasureSpec);
		int widthSize = 0;
		switch( widthMode )
		{
			case MeasureSpec.UNSPECIFIED:
				widthSize = widthMeasureSpec;
				break;
			case MeasureSpec.AT_MOST:
				widthSize = 300;
				break;
			case MeasureSpec.EXACTLY:
				widthSize = MeasureSpec.getSize(widthMeasureSpec);
				break;
		}
		
		this.setMeasuredDimension( widthSize, heightSize );
		
	}

	
	@Override
	protected void onDraw(Canvas canvas)
	{
		super.onDraw(canvas);

        canvas.drawColor( Color.argb( 0 , 0 , 0 , 0) );

		trianglePath.moveTo(0, this.getMeasuredHeight() );
		trianglePath.lineTo( this.getMeasuredWidth() / 2 , 0 );
		trianglePath.lineTo(this.getMeasuredWidth() , this.getMeasuredHeight() );
		trianglePath.close();

		trianglePaint.setColor(mColor);

		canvas.drawPath(trianglePath, trianglePaint);
	}
}
