package com.fitdotlife.fitmate_lib.customview;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.fitdotlife.fitmate.FriendActivity;

import com.fitdotlife.fitmate.FriendProfileActivity_;
import com.fitdotlife.fitmate.FriendProfileAddActivity_;
import com.fitdotlife.fitmate.FriendProfileAgreeActivity_;
import com.fitdotlife.fitmate.R;
import com.fitdotlife.fitmate.RefreshListener;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.http.FriendService;
import com.fitdotlife.fitmate_lib.http.NetworkClass;
import com.fitdotlife.fitmate_lib.key.FriendStatusType;
import com.fitdotlife.fitmate_lib.object.UserFriend;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringRes;
import org.androidannotations.rest.spring.annotations.RestService;
import org.springframework.web.client.RestClientException;

/**
 * Created by Joshua on 2015-07-31.
 */
@EViewGroup(R.layout.child_activity_friend_findfriend_listitem)
public class FindFriendListItem extends LinearLayout {

    private Context mContext;
    private UserFriend mUserFriend;
    private String mEmail = null;
    private RefreshListener mRefreshListener = null;
    private int mUserID = 0;

    @RestService
    FriendService friendServiceClient;

    @ViewById( R.id.txt_child_activity_friend_findfriend_listitem_name)
    TextView txtName;

    @ViewById(R.id.txt_child_activity_friend_findfriend_listitem_email)
    TextView txtEmail;

    @ViewById(R.id.txt_child_activity_friend_findfriend_listitem_introself)
    TextView txtIntroSelf;

    @ViewById(R.id.btn_child_activity_findfriend_listitem_add)
    Button btnAdd;

    @ViewById(R.id.img_child_activity_friend_findfriend_listitem_profile)
    ImageView imgProfile;

    @StringRes(R.string.common_cancel)
    String strCancle;

    @StringRes(R.string.common_add)
    String strAdd;



    public FindFriendListItem(Context context ) {
        super(context);
        this.mContext = context;
    }

    public void setUserFriend(UserFriend userFriend, String email, int userID){

        friendServiceClient.setRootUrl(NetworkClass.baseURL + "/api");

        this.mUserFriend = userFriend;
        this.mEmail = email;
        this.mUserID = userID;


        txtName.setText( mUserFriend.getName()  );
        FriendStatusType statusType = FriendStatusType.getFriendStatusType( mUserFriend.getRequestStatus() );

        if( statusType.equals( FriendStatusType.ACCEPT ) )
        {
            txtEmail.setText(mUserFriend.getEmail());
        }else{
            txtEmail.setVisibility(View.GONE);
        }

        if( userFriend.getIntroYourSelf().equals("") ){
            txtIntroSelf.setVisibility(View.GONE);
        }else {
            txtIntroSelf.setText(userFriend.getIntroYourSelf());
        }

        if( !(mUserFriend.getProfileImagePath() == null) ) {
            DisplayImageOptions options = new DisplayImageOptions.Builder()
                    .showImageForEmptyUri(R.drawable.profile_img1)
                    .showImageOnFail(R.drawable.profile_img1)
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .displayer(new RoundedBitmapDisplayer(200))
                    .build();

            ImageLoader.getInstance().displayImage(NetworkClass.imageBaseURL + mUserFriend.getProfileImagePath(), imgProfile, options);
        }

        if( mUserFriend.getUserID() == mUserID || mUserFriend.getFriendID() == mUserID) {
            if (statusType.equals(FriendStatusType.REQUEST)) {
                if(mUserFriend.getUserID() == userID) {
                    btnAdd.setText(strCancle);
                    btnAdd.setBackground(this.getResources().getDrawable(R.drawable.button_selector_gray));
                    btnAdd.setTextColor(0xFF939393);
                }else if(mUserFriend.getFriendID() == userID){
                    btnAdd.setVisibility(GONE);
                }
            } else if (statusType.equals(FriendStatusType.ACCEPT)) {
                btnAdd.setVisibility(GONE);
            }
        }
    }

    public void setRefreshListener( RefreshListener listener  ){
        this.mRefreshListener = listener;
    }

    @Click(R.id.ll_child_activity_friend_findfriend_listitem)
    void addIntentClick(){

        FriendStatusType statusType = FriendStatusType.getFriendStatusType(mUserFriend.getRequestStatus());

        if(statusType.equals( FriendStatusType.ACCEPT )) {

            FriendProfileActivity_.intent(mContext).extra("UserFriend", mUserFriend).startForResult(FriendActivity.FRIEND_FINDFRIEND_FRIEND);

        }else if( statusType.equals(FriendStatusType.REQUEST) ){

            if(mUserFriend.getUserID() == mUserID) {
                FriendProfileAddActivity_.intent(mContext).extra("UserFriend", mUserFriend).startForResult(FriendActivity.FRIEND_REQUEST_ADD);
            }else if(mUserFriend.getFriendID() == mUserID){
                FriendProfileAgreeActivity_.intent(mContext).extra("UserFriend",mUserFriend).startForResult(FriendActivity.FRIEND_FINDFRIEND_FRIEND);
            }

        }
    }

    @UiThread
    void refreshList( ){
        this.mRefreshListener.refreshList(FriendActivity.FriendModeType.FINDFRIEND);
    }

    @Background
    void addFriend(){
        try {
            int addID = friendServiceClient.addFriend(mEmail, mUserFriend.getEmail() );

            if(addID > 0){
                mUserFriend.setRequestStatus(FriendStatusType.REQUEST.getValue());

                mUserFriend.setUserFriendsID(addID);
                setCancleButtonText(strCancle);

                refreshList();
            }

        }catch(RestClientException e){

        }
    }

    @UiThread
    void setCancleButtonText( String buttonText ){
        btnAdd.setText(buttonText);
        btnAdd.setBackground(this.getResources().getDrawable(R.drawable.button_selector_gray));
        btnAdd.setTextColor(0xFF939393);
    }

    @UiThread
    void setAddButtonText( String buttonText ){
        btnAdd.setText(buttonText);
        btnAdd.setBackground(this.getResources().getDrawable(R.drawable.button_selector_blue));
        btnAdd.setTextColor(Color.WHITE);
    }

    @Background
    void cancleFriend(){
        try {
            String email = new FitmateDBManager(mContext).getUserInfo().getEmail();
            boolean result = friendServiceClient.changeStatus(email, mUserFriend.getUserFriendsID(), FriendStatusType.CANCLE.getValue() );

            if (result) {
                mUserFriend.setRequestStatus(FriendStatusType.NOVALUE.getValue() );
                setAddButtonText(strAdd);
            }
        }catch( RestClientException e ){

        }
    }

    @Click(R.id.btn_child_activity_findfriend_listitem_add)
    void addClick(){

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);     // 여기서 this는 Activity의 this

        String message = "";

        final FriendStatusType statusType = FriendStatusType.getFriendStatusType(  mUserFriend.getRequestStatus() );
        if( statusType.equals( FriendStatusType.REQUEST ) ){
            message = mContext.getString(R.string.friend_findfriend_cancel_friend);
        }else{
            message = mContext.getString(R.string.friend_findfriend_accept_warning);
        }

        builder.setMessage(message)        // 메세지 설정
                .setCancelable(true)        // 뒤로 버튼 클릭시 취소 가능 설정
                .setPositiveButton(R.string.common_ok, new DialogInterface.OnClickListener() {
                    // 취소 버튼 클릭시 설정
                    public void onClick(DialogInterface dialog, int whichButton) {

                        if (statusType.equals(FriendStatusType.REQUEST)) {
                            cancleFriend();
                        } else {
                            addFriend();
                        }

                        dialog.dismiss();
                    }
                })
                .setNegativeButton(R.string.common_close, new DialogInterface.OnClickListener() {
                    // 확인 버튼 클릭시 설정
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                });

        AlertDialog dialog = builder.create();    // 알림창 객체 생성
        dialog.show();    // 알림창 띄우기

    }

}
