package com.fitdotlife.fitmate_lib.customview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.StrengthInputType;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.object.ExerciseProgram;
import com.fitdotlife.fitmate_lib.util.Utils;
import com.fitdotlife.fitmate.R;

/**
 * Created by Joshua on 2015-04-02.
 */
public class AppliedProgramSummaryLayout extends LinearLayout{

    private boolean mViewProgramTitle = true;
    private ExerciseProgram mAppliedProgram = null;
    private Context mContext = null;
    private OnClickListener mListener = null;
    private View mProgramInfoView = null;
    private int titleColor = 0;

    public AppliedProgramSummaryLayout(Context context){
        super(context);
    }

    public AppliedProgramSummaryLayout(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.mContext = context;

        TypedArray a = context.obtainStyledAttributes(attrs,R.styleable.AppliedProgramLayout);
        this.mViewProgramTitle = a.getBoolean(R.styleable.AppliedProgramLayout_titleDisplay , true);
        this.titleColor = a.getColor( R.styleable.AppliedProgramLayout_titleColor , 0xFF7db932 );
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

    public void setOnClickListener( OnClickListener listener ){
        this.mListener = listener;
    }

    public void setAppliedExerciseProgram( ExerciseProgram appliedExerciseProgramSNU){
        this.mAppliedProgram = appliedExerciseProgramSNU;

        LayoutInflater layoutInflater = LayoutInflater.from(this.mContext);

        StrengthInputType strengthType = StrengthInputType.getStrengthType( this.mAppliedProgram.getCategory() );
        String strengthUnit = Utils.getStrengthUnit(strengthType);

        if( strengthType.equals( StrengthInputType.CALORIE ) || strengthType.equals( StrengthInputType.CALORIE_SUM ) || strengthType.equals(StrengthInputType.VS_BMR) ){
            if( this.mProgramInfoView == null ) {
                mProgramInfoView = layoutInflater.inflate(R.layout.child_applied_program_info_calorie_sum, this, true);
            }
        }else{
            if( this.mProgramInfoView == null ) {
                mProgramInfoView = layoutInflater.inflate(R.layout.child_applied_program_info_no_calorie_sum, this, true);
            }

            TextView tvTimeTile = (TextView) mProgramInfoView.findViewById(R.id.tv_child_applied_program_info_time_title);
            tvTimeTile.setTextColor(this.titleColor);
//            tvTimeTile.setEllipsize(TextUtils.TruncateAt.MARQUEE);
//            tvTimeTile.setSingleLine(true);
//            tvTimeTile.setVisible(true);
//            tvTimeTile.setMarqueeRepeatLimit(-1);

            TextView tvTime = (TextView) mProgramInfoView.findViewById(R.id.tv_child_applied_program_info_time);
            int timeFrom = this.mAppliedProgram.getMinuteFrom();
            int timeTo = this.mAppliedProgram.getMinuteTo();

            String timeText = null;
            String timeUnit = this.getResources().getString(R.string.common_minute);
            if( timeFrom == timeTo ){
                timeText = timeFrom  + timeUnit;
            }else{
                timeText = timeFrom + timeUnit + "-" + timeTo + timeUnit;
            }
            tvTime.setText( timeText );
        }

        TextView tvStrengthRangeTitle = (TextView) mProgramInfoView.findViewById(R.id.tv_child_applied_program_info_strength_range_title);
        tvStrengthRangeTitle.setTextColor( titleColor );

        TextView tvStrengthRange = (TextView) mProgramInfoView.findViewById(R.id.tv_child_applied_program_info_strength_range);
     //   int strengthFrom = (int) this.mAppliedProgram.getCalorieFrom();
        int strengthFrom= (int) this.mAppliedProgram.getStrengthFrom();
        int strengthTo= (int) this.mAppliedProgram.getStrengthTo();

        String strengthRageText = null;


        if( strengthFrom == strengthTo ){
            if(strengthType.equals( StrengthInputType.STRENGTH ) ){

                String[] arrStrengthType = this.mContext.getResources().getStringArray( R.array.exercise_strength_type_char );
                strengthRageText = arrStrengthType[strengthFrom] + Utils.getStrengthUnit(strengthType);

            }else {
            strengthRageText = strengthFrom + Utils.getStrengthUnit(strengthType);
        }
        }
        else{
            if(strengthType.equals( StrengthInputType.STRENGTH ) ){
                String[] arrStrengthType = this.mContext.getResources().getStringArray( R.array.exercise_strength_type_char );
                strengthRageText = arrStrengthType[strengthFrom] + "-" + arrStrengthType[strengthTo] + " " + strengthUnit;

            }else {
            strengthRageText = strengthFrom + "-"+ strengthTo + " " +strengthUnit;
        }
        }
        tvStrengthRange.setText( strengthRageText );

        TextView tvTimesTitle = (TextView) mProgramInfoView.findViewById(R.id.tv_child_applied_program_info_times_title);
        tvTimesTitle.setTextColor( titleColor );

        TextView tvTimes = (TextView) mProgramInfoView.findViewById(R.id.tv_child_applied_program_info_times);
        int timesFrom = this.mAppliedProgram.getTimesFrom();
        int timesTo = this.mAppliedProgram.getTimesTo();

        String timesText = null;
        String timesUnit = this.getResources().getString(R.string.common_day).toLowerCase();
        if( timesFrom == timesTo ){
            timesText = timesFrom + timesUnit;
        }else{
            timesText = timesFrom + timesUnit + "-" + timesTo + timesUnit;
        }
        tvTimes.setText( timesText );
    }

}
