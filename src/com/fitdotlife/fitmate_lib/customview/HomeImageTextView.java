package com.fitdotlife.fitmate_lib.customview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.TextView;

import com.fitdotlife.fitmate.R;


/**
 * Created by Joshua on 2015-03-20.
 */
public class HomeImageTextView extends TextView {

    private Drawable backgroundDrawable = null;
    private float backgroundDrawableWidth = 0;


    public HomeImageTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.obtainStyledAttributes(attrs , R.styleable.HomeImageTextView );
        Drawable aBackgroundDrawable = a.getDrawable(R.styleable.HomeImageTextView_src);
        if( aBackgroundDrawable != null ) { backgroundDrawable = aBackgroundDrawable;}

        backgroundDrawableWidth = TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP , 77 , this.getResources().getDisplayMetrics() );
    }

    @Override
    protected void onDraw(Canvas canvas) {

        Rect drawableArea = new Rect();

        //이미지를 그린다.
        if( backgroundDrawable != null ) {
            drawableArea.left = 0;
            drawableArea.right = (int) backgroundDrawableWidth;
            drawableArea.top = 0;
            drawableArea.bottom = this.getMeasuredHeight();

            //canvas.drawRect( drawableArea . new Paint() );
            //Paint paint =new Paint();
            //canvas.drawRect(drawableArea , paint);
            backgroundDrawable.setBounds(drawableArea);
            backgroundDrawable.draw(canvas);
        }

        super.onDraw(canvas);
    }
}
