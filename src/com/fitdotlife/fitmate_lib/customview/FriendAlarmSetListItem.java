package com.fitdotlife.fitmate_lib.customview;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fitdotlife.fitmate.R;
import com.fitdotlife.fitmate_lib.http.FriendService;
import com.fitdotlife.fitmate_lib.http.NetworkClass;
import com.fitdotlife.fitmate_lib.key.FriendRequestType;
import com.fitdotlife.fitmate_lib.object.UserFriendsNewsSetting;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.rest.spring.annotations.RestService;
import org.androidannotations.annotations.res.DrawableRes;

/**
 * Created by Joshua on 2015-08-12.
 */
@EViewGroup(R.layout.child_activity_friend_alarm_setting_listitem)
public class FriendAlarmSetListItem  extends LinearLayout{

    private Context mContext;
    private UserFriendsNewsSetting mUserFriendNewsSetting;
    private boolean isChecked = false;
    private FriendRequestType mRequestType = FriendRequestType.RECEIVEREQUEST;

    @RestService
    FriendService friendServiceClient;

    @ViewById(R.id.tv_child_activity_alarm_setting_listitem_name)
    TextView tvName;

    @ViewById(R.id.tv_child_activity_alarm_setting_listitem_email)
    TextView tvEmail;

    @ViewById(R.id.tv_child_activity_alarm_setting_listitem_introself)
    TextView tvIntroSelf;

    @ViewById(R.id.img_child_activity_friend_alarm_setting_listitem_profile)
    ImageView imgProfile;

    @ViewById(R.id.img_child_activity_alarm_setting_listitem_select)
    ImageView imgSelect;

    @DrawableRes(R.drawable.friend_checkbox)
    Drawable checkBox;

    @DrawableRes(R.drawable.friend_checkbox_sel)
    Drawable checkBoxSel;

    public FriendAlarmSetListItem(Context context) {
        super(context);

        this.mContext = context;
    }

    public void setFriendNewsSetting(UserFriendsNewsSetting userFriendNewsSetting){
        friendServiceClient.setRootUrl(NetworkClass.baseURL + "/api");

        this.mUserFriendNewsSetting = userFriendNewsSetting;

        tvName.setText(userFriendNewsSetting.getFriendInfo().getName());
        tvEmail.setText(userFriendNewsSetting.getFriendInfo().getEmail() );

        if( userFriendNewsSetting.getFriendInfo().getIntro().equals("") ){
            tvIntroSelf.setVisibility(View.GONE);
        }else {
            tvIntroSelf.setText(userFriendNewsSetting.getFriendInfo().getIntro());
        }

        if( !(mUserFriendNewsSetting.getFriendInfo().getProfileImagePath() == null) ) {
            DisplayImageOptions options = new DisplayImageOptions.Builder()
                    .showImageForEmptyUri(R.drawable.profile_img1 )
                    .showImageOnFail(R.drawable.profile_img1 )
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .displayer(new RoundedBitmapDisplayer(200))
                    .build();

            ImageLoader.getInstance().displayImage(NetworkClass.imageBaseURL + mUserFriendNewsSetting.getFriendInfo().getProfileImagePath(), imgProfile, options);
        }

        if( mUserFriendNewsSetting.isReceiveFriendsNews()){
            imgSelect.setImageDrawable( checkBoxSel );
            isChecked = true;
        }
    }

    @Click(R.id.img_child_activity_alarm_setting_listitem_select)
    void setAlarmClick(){
        if(isChecked){
            imgSelect.setImageDrawable(checkBox);
            isChecked = false;

        }else{
            imgSelect.setImageDrawable( checkBoxSel );
            isChecked = true;
        }

        mUserFriendNewsSetting.setReceiveFriendsNews(isChecked);
    }

    public void setChecked( boolean checked ){
        if(checked){
            imgSelect.setImageDrawable( checkBoxSel );
            isChecked = true;
        }else{
            imgSelect.setImageDrawable(checkBox);
            isChecked = false;
        }

        mUserFriendNewsSetting.setReceiveFriendsNews(isChecked);
    }
}
