package com.fitdotlife.fitmate_lib.customview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by Joshua on 2015-04-16.
 */
public class MenuPopupView extends View {

    private int heightSize = 0;
    private int widthSize = 0;

    private Paint mPopupPaint = null;

    private int mCenterX = 0;
    private int mBottomY = 0;


    public MenuPopupView(Context context) {
        super(context);

        this.init();
    }

    public MenuPopupView(Context context , AttributeSet attrs)
    {
        super(context , attrs);
        this.init();
    }

    private void init(){
        this.mPopupPaint = new Paint();
        this.mPopupPaint.setColor(Color.BLUE );
    }

    @Override
    protected void onMeasure(int widthMeasureSpec , int heightMeasureSpec)
    {
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        switch( heightMode )
        {
            case MeasureSpec.UNSPECIFIED:
                heightSize = heightMeasureSpec;
                break;
            case MeasureSpec.AT_MOST:
                heightSize = 200;
                break;
            case MeasureSpec.EXACTLY:
                heightSize = MeasureSpec.getSize(heightMeasureSpec);
                break;
        }

        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        switch( widthMode )
        {
            case MeasureSpec.UNSPECIFIED:
                widthSize = widthMeasureSpec;
                break;
            case MeasureSpec.AT_MOST:
                widthSize = 300;
                break;
            case MeasureSpec.EXACTLY:
                widthSize = MeasureSpec.getSize(widthMeasureSpec);
                break;
        }

        this.setMeasuredDimension(widthSize, heightSize);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        //this.drawBatteryPopup(canvas);
    }

    private void drawBatteryPopup( Canvas canvas ){
        Path popupPath = new Path();
        int centerX = this.mCenterX;
        int centerY = this.mBottomY;

        popupPath.moveTo( centerX , centerY );
        popupPath.lineTo( centerX - 100 , centerY + 100 );
        popupPath.lineTo( centerX + 100 , centerY + 100 );
        popupPath.close();

        canvas.drawPath(popupPath, this.mPopupPaint);
    }

    private void drawSyncPopup(){

    }

    public void setCenterPosition( int centerX , int bottomY ){
        this.mCenterX = centerX;
        this.mBottomY = bottomY;

        this.invalidate();
    }
}
