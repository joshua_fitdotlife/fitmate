package com.fitdotlife.fitmate_lib.customview;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fitdotlife.fitmate.FriendActivity;
import com.fitdotlife.fitmate.FriendNewHomeActivity_;
import com.fitdotlife.fitmate.R;
import com.fitdotlife.fitmate_lib.http.NetworkClass;
import com.fitdotlife.fitmate_lib.object.UserFriend;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

/**
 * Created by Joshua on 2015-08-03.
 */
@EViewGroup(R.layout.child_activity_friend_myfriend_listitem)
public class MyFriendListItem  extends LinearLayout {

    private Context mContext;
    private UserFriend mUserFriend;

    @ViewById(R.id.tv_child_activity_myfriend_listitem_name)
    TextView tvName;

    @ViewById(R.id.tv_child_activity_myfriend_listitem_email)
    TextView tvEmail;

    @ViewById(R.id.tv_child_activity_myfriend_listitem_introself)
    TextView tvIntro;

    @ViewById(R.id.img_child_activity_friend_myfriend_listitem_profile)
    ImageView imgProfile;

    public MyFriendListItem(Context context) {
        super(context);

        this.mContext = context;
    }

    public void setUserFriend( UserFriend userFriend){

        tvName.setText(userFriend.getName());
        tvEmail.setText(userFriend.getEmail());

        if( userFriend.getIntroYourSelf().equals("") ){
         tvIntro.setVisibility(View.GONE);
        }else {
            tvIntro.setText(userFriend.getIntroYourSelf());
        }

        if( !(userFriend.getProfileImagePath() == null ) ) {
            DisplayImageOptions options = new DisplayImageOptions.Builder()
                    .showImageForEmptyUri(R.drawable.profile_img1)
                    .showImageOnFail(R.drawable.profile_img1)
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .displayer(new RoundedBitmapDisplayer(200))
                    .build();

            ImageLoader.getInstance().displayImage(NetworkClass.imageBaseURL + userFriend.getProfileImagePath(), imgProfile, options);
        }

        this.mUserFriend = userFriend;
    }

    @Click(R.id.ll_child_activity_friend_myfriend_listitem)
    void friendClidk(){

        this.setDeletedFriendEmail(null);
        //FriendHomeActivity_.intent(mContext).extra("UserFriend", mUserFriend).startForResult(FriendActivity.FRIEND_MYFRIEND_RESULT);
        FriendNewHomeActivity_.intent(mContext).extra("UserFriend", mUserFriend).startForResult(FriendActivity.FRIEND_MYFRIEND_RESULT);

    }

    private void setDeletedFriendEmail( String friendEmail ){
        SharedPreferences pref = this.mContext.getSharedPreferences( "fitmateservice" , Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(FriendActivity.FRIEND_DELETED_FRIEND_EMAIL_KEY, friendEmail);
        editor.commit();
    }
}
