package com.fitdotlife.fitmate_lib.customview;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.view.View;

import com.fitdotlife.fitmate.R;

public class WeekPointView extends View
{

	private Context mContext = null;

	private int mLeftBlank = 10;
	private int mTopBlank = 0;
	private int mRightBlank = 10;
	private int mBottomBlank = 0;

	private int heightSize = 0;
	private int widthSize = 0;

	private Path barPath = null;
    private Path starPath = null;

	private Paint barPaint = null;
    private Paint starPaint = null;
    private Paint mainTextPaint = null;
    private Paint subTextPaint = null;
	private int mColor = 0;
    private Paint imagePaint = null;


    private int mMainTextSize = 0;
    private int mSubTextSize = 0;
    private int mTextBlank = 0;

    private int mPoint = 0;

	public WeekPointView(Context context)
	{
		super(context);
		
		this.mContext = context;
		this.init();
	}
	
	private void init()
	{
        this.mLeftBlank = this.getResources().getDimensionPixelSize(R.dimen.week_point_oval_chart_left_blank);
        this.mRightBlank = this.getResources().getDimensionPixelSize(R.dimen.week_point_oval_chart_right_blank);
        this.mMainTextSize = this.getResources().getDimensionPixelSize(R.dimen.week_point_oval_main_text_size);
        this.mSubTextSize = this.getResources().getDimensionPixelSize(R.dimen.week_point_oval_sub_text_size);
        this.mTextBlank = this.getResources().getDimensionPixelSize(R.dimen.week_point_oval_text_blank);

		this.barPath = new Path();
        this.starPath = new Path();

		this.barPaint = new Paint( Paint.ANTI_ALIAS_FLAG );

        this.starPaint = new Paint( Paint.ANTI_ALIAS_FLAG );
        this.starPaint.setColor(Color.WHITE);
        this.starPaint.setStyle(Paint.Style.FILL);

        this.mainTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.mainTextPaint.setTextSize(this.mMainTextSize);
        this.mainTextPaint.setColor(Color.WHITE);

        this.subTextPaint = new Paint( Paint.ANTI_ALIAS_FLAG );
        this.subTextPaint.setTextSize(this.mSubTextSize);
        this.subTextPaint.setColor(Color.WHITE);

        this.imagePaint = new Paint( Paint.ANTI_ALIAS_FLAG );
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) 
	{
		int heightMode = MeasureSpec.getMode(heightMeasureSpec);
		switch( heightMode )
		{
		case MeasureSpec.UNSPECIFIED:
			heightSize = heightMeasureSpec;
			break;
		case MeasureSpec.AT_MOST:
			heightSize = 200;
			break;
		case MeasureSpec.EXACTLY:
			heightSize = MeasureSpec.getSize(heightMeasureSpec);
			break;
		}
		
		int widthMode = MeasureSpec.getMode(widthMeasureSpec);
		switch( widthMode )
		{
		case MeasureSpec.UNSPECIFIED:
			widthSize = widthMeasureSpec;
			break;
		case MeasureSpec.AT_MOST:
			widthSize = 300;
			break;
		case MeasureSpec.EXACTLY:
			widthSize = MeasureSpec.getSize(widthMeasureSpec);
			break;
		}
		
		this.setMeasuredDimension(widthSize, heightSize);
	}
	
	@Override
	protected void onDraw( Canvas canvas )
	{
		float ovalLeft = this.mLeftBlank;
		float ovalTop = this.mTopBlank;
		float ovalRight = this.getMeasuredWidth() - this.mRightBlank;
		float ovalBottom = this.getMeasuredHeight() - this.mBottomBlank;

        int capRadius = ( this.getMeasuredHeight() - this.mTopBlank - this.mBottomBlank ) / 2;

		drawOval( canvas , ovalLeft , ovalTop , ovalRight , ovalBottom , capRadius);
        int startWidth = drawImage(canvas , capRadius );
        drawWeekPointText(canvas , startWidth+25 , capRadius);
        drawWeekPoint(canvas, capRadius);
	}

    private void drawWeekPoint( Canvas canvas , int capRadius ) {
        int viewHeight = this.getMeasuredHeight() - this.mTopBlank - this.mBottomBlank;
        int center_y = viewHeight / 2;

        int mainTextHeight = this.getTextHeight( String.valueOf( this.mPoint ) , this.mainTextPaint);
        int maintTextWidth = this.getTextWidth(String.valueOf( this.mPoint ) , this.mainTextPaint);
        int subTextWidth = this.getTextWidth(this.getResources().getString(R.string.activity_week_pointchart_pt), this.subTextPaint);

        canvas.drawText( String.valueOf( this.mPoint ) , this.getMeasuredWidth() - this.mRightBlank - capRadius - ( this.mTextBlank * 2 ) - subTextWidth - maintTextWidth , center_y + ( mainTextHeight / 2 ) , this.mainTextPaint );
        canvas.drawText(this.getResources().getString(R.string.activity_week_pointchart_pt) , this.getMeasuredWidth() - this.mRightBlank - capRadius - this.mTextBlank - subTextWidth , center_y + ( mainTextHeight / 2 ) , this.subTextPaint );
    }

    private void drawWeekPointText( Canvas canvas , int starWidth , int capRadius ) {

        int viewHeight = this.getMeasuredHeight() - this.mTopBlank - this.mBottomBlank;
        int center_y = viewHeight / 2;

        int textHeight = this.getTextHeight( this.getResources().getString(R.string.activity_week_pointchart_weekpoint) , this.mainTextPaint);

        canvas.drawText( this.getResources().getString(R.string.activity_week_pointchart_weekpoint) , this.mLeftBlank + starWidth + ( this.mTextBlank * 2 ) , center_y + ( textHeight / 2 )   ,  this.mainTextPaint);
    }

    private int drawImage(Canvas canvas , int capRadius ) {

        int viewHeight = this.getMeasuredHeight() - this.mTopBlank - this.mBottomBlank;
        int contentSpace = viewHeight / 10;
        int imageWidth = contentSpace * 6 ;
        int startY = contentSpace * 2;

        Bitmap src = BitmapFactory.decodeResource(this.mContext.getResources(), R.drawable.icon_star);
        Bitmap resizeBitmap = Bitmap.createScaledBitmap( src , imageWidth , imageWidth , false );

        canvas.drawBitmap(resizeBitmap , this.mLeftBlank + this.mTextBlank+25 , startY  , this.imagePaint );

        return imageWidth;
    }



    private void drawOval( Canvas canvas , float left , float top , float right , float bottom , int capRadius )
	{
		barPath.addRect( left + capRadius  , top , right - capRadius , bottom , Path.Direction.CW );
		barPath.addCircle( left + capRadius , top + capRadius  , capRadius , Path.Direction.CW );
		barPath.addCircle( right - capRadius , top + capRadius  , capRadius , Path.Direction.CW );
		
		barPath.close();
		
		this.barPaint.setColor(mColor);
		canvas.drawPath( barPath , this.barPaint );



	}
	
	public void setColor( int color )
	{
		this.mColor = color;
	}

    public void setPoint( int point )
    {
        this.mPoint = point;
    }

    private int getTextWidth(String text, Paint paint)
    {
        Rect bounds = new Rect();
        paint.getTextBounds(text, 0, text.length(), bounds);
        return bounds.width();
    }

    private int getTextHeight(String text, Paint paint)
    {
        Rect bounds = new Rect();
        paint.getTextBounds(text, 0, text.length(), bounds);
        return bounds.height();
    }

}
