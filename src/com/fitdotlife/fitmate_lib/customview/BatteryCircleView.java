package com.fitdotlife.fitmate_lib.customview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import com.fitdotlife.fitmate.R;

/**
 * Created by Joshua on 2015-12-21.
 */
public class BatteryCircleView extends View {

    Paint mFillPaint = null;
    Paint mEmptyPaint = null;

    private int heightSize = 0;
    private int widthSize = 0;

    private int mBatteryValue = 0;

    public BatteryCircleView(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.mFillPaint = new Paint( Paint.ANTI_ALIAS_FLAG );
        this.mFillPaint.setColor(this.getResources().getColor(R.color.newhome_battery_fill));

        this.mEmptyPaint = new Paint( Paint.ANTI_ALIAS_FLAG );
        this.mEmptyPaint.setColor(this.getResources().getColor(R.color.newhome_battery_empty));
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        switch( heightMode )
        {
            case MeasureSpec.UNSPECIFIED:
                heightSize = heightMeasureSpec;
                break;
            case MeasureSpec.AT_MOST:
                heightSize = 200;
                break;
            case MeasureSpec.EXACTLY:
                heightSize = MeasureSpec.getSize(heightMeasureSpec);
                break;
        }

        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        switch( widthMode )
        {
            case MeasureSpec.UNSPECIFIED:
                widthSize = widthMeasureSpec;
                break;
            case MeasureSpec.AT_MOST:
                widthSize = 300;
                break;
            case MeasureSpec.EXACTLY:
                widthSize = MeasureSpec.getSize(widthMeasureSpec);
                break;
        }
        this.setMeasuredDimension(widthSize, heightSize);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        RectF oval = new RectF( 0 , 0 , this.getMeasuredWidth() , this.getMeasuredHeight() );
        float fillAngle = (360 * mBatteryValue ) / 100;

        canvas.drawArc( oval , 270 , fillAngle , true , mFillPaint);
        canvas.drawArc(oval, fillAngle + 270, (360 * (100 - mBatteryValue)) / 100, true, mEmptyPaint);
    }

    public void setBatteryValue(  int batteryValue  ){
        if( batteryValue > 0 ) {
        this.mBatteryValue = batteryValue;
    }
}
}
