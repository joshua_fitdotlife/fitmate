package com.fitdotlife.fitmate_lib.customview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;

import com.fitdotlife.fitmate.R;

/**
 * Created by Joshua on 2015-12-17.
 */
public class DayHalfCircleView extends View {

    private int heightSize = 0;
    private int widthSize = 0;

    private float mValue = 0;
    private Paint mEmptyCirclePaint = null;
    private Paint mValueCirClePaint = null;

    private float mCircleThickness = 0;

    private float mRangeFrom = 30;
    private float mRangeTo = 60;

    private String mRangeUnitText = "min";

    private Context mContext = null;


    public DayHalfCircleView(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.mContext = context;

        this.mEmptyCirclePaint = new Paint( Paint.ANTI_ALIAS_FLAG );
        this.mEmptyCirclePaint.setStyle(Paint.Style.STROKE);
        this.mEmptyCirclePaint.setStrokeCap(Paint.Cap.ROUND);

        this.mValueCirClePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.mValueCirClePaint.setStyle(Paint.Style.STROKE);
        this.mValueCirClePaint.setStrokeCap(Paint.Cap.ROUND);

    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        switch( heightMode )
        {
            case MeasureSpec.UNSPECIFIED:
                heightSize = heightMeasureSpec;
                break;
            case MeasureSpec.AT_MOST:
                heightSize = 200;
                break;
            case MeasureSpec.EXACTLY:
                heightSize = MeasureSpec.getSize(heightMeasureSpec);
                break;
        }

        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        switch( widthMode )
        {
            case MeasureSpec.UNSPECIFIED:
                widthSize = widthMeasureSpec;
                break;
            case MeasureSpec.AT_MOST:
                widthSize = 300;
                break;
            case MeasureSpec.EXACTLY:
                widthSize = MeasureSpec.getSize(widthMeasureSpec);
                break;
        }

        this.setMeasuredDimension(widthSize, heightSize);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int viewHeight =  this.getMeasuredHeight();
        float topMargin = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 32.33f, getResources().getDisplayMetrics());
        float circleHeight = viewHeight - topMargin;
        float radius = ( circleHeight - mCircleThickness ) / 2;

        float center_x = this.getMeasuredWidth() / 2;
        float center_y = (circleHeight / 2 ) + topMargin;

        this.drawRingArc( canvas, center_x, center_y, radius, radius, 123, 294, mEmptyCirclePaint );
        this.drawRingArc( canvas , center_x , center_y , radius , radius , 123 , ( 294 * (mValue) ) / (mRangeFrom + mRangeTo) , mValueCirClePaint );
        this.drawRangeCircles(canvas, center_x, center_y, radius);
    }

    private void drawRangeCircles(Canvas canvas, float center_x, float center_y, float radius)
    {
        int[] attrs = new int[]{R.attr.newhomeDayCirleRangeEmpty , R.attr.newhomeAchieveEmpty , R.attr.newhomeAchieveUnder ,R.attr.newhomeAchieve , R.attr.newhomeAchieveOver };
        TypedArray a = mContext.getTheme().obtainStyledAttributes(attrs);

        int dayCirclRangeemptyColor = a.getColor(0, 0);
        int achieveEmptyColor = a.getColor(1, 0);
        int achieveUnderColor = a.getColor(2, 0);
        int achieveColor = a.getColor(3,0);
        int achieveOverColor = a.getColor(4,0);

        a.recycle();

        float maxValue = mRangeFrom + mRangeTo;

        if(mRangeTo == mRangeFrom){
            int fromCircleColor = achieveEmptyColor;
            int fromTextColor = dayCirclRangeemptyColor;
            if( this.mValue > 0 ) {
                if (this.mValue < mRangeFrom) {
                    fromCircleColor = achieveEmptyColor;
                    fromTextColor = achieveUnderColor;
                } else if (this.mValue == mRangeFrom) {
                    fromCircleColor = achieveColor;
                    fromTextColor = achieveColor;
                } else {
                    fromCircleColor = achieveColor;
                    fromTextColor = achieveColor;
                }
            }

            float strengthFromDegree = (( 294 * mRangeFrom) / maxValue ) + 123;
            drawRangeCircle(canvas, radius, center_x, center_y, strengthFromDegree, fromCircleColor);
            drawRangeText(canvas, radius, center_x, center_y, strengthFromDegree, fromTextColor, mRangeFrom);
        }
        else
        {
            int fromCircleColor = achieveEmptyColor;
            int fromTextColor = dayCirclRangeemptyColor;
            int toCircleColor = achieveEmptyColor;
            int toTextColor = dayCirclRangeemptyColor;

            if( this.mValue > 0 )
            {
                if (this.mValue < mRangeFrom) {

                } else if (this.mValue < mRangeTo) {

                    fromCircleColor = achieveColor;
                    fromTextColor = achieveColor;
                }else if( this.mValue == mRangeTo ){

                    fromCircleColor = achieveColor;
                    fromTextColor = achieveColor;
                    toCircleColor = achieveColor;
                    toTextColor = achieveColor;

                } else {

                    fromCircleColor = achieveOverColor;
                    fromTextColor = achieveOverColor;
                    toCircleColor = achieveOverColor;
                    toTextColor = achieveOverColor;
                }
            }

            float strengthFromDegree = (( 294 * mRangeFrom) / maxValue ) + 123;
            drawRangeCircle( canvas , radius , center_x , center_y , strengthFromDegree , fromCircleColor );
            drawRangeText( canvas , radius , center_x , center_y,strengthFromDegree , fromTextColor , mRangeFrom );

            float strengthToDegree = (( 294 * mRangeTo) / maxValue ) + 123;
            drawRangeCircle( canvas , radius , center_x , center_y , strengthToDegree , toCircleColor );
            drawRangeText( canvas , radius , center_x , center_y, strengthToDegree , toTextColor , mRangeTo );

        }
    }


    private void drawRangeCircle( Canvas canvas , float radius , float center_x , float center_y , float rangeDegree , int circleColor )
    {
        float innerCircleRadius = TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP , 10 , this.getResources().getDisplayMetrics() ) / 2;
        float outerCircleRadius = TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP , 18 , this.getResources().getDisplayMetrics() ) / 2;

        Paint innerCirclePaint = new Paint();
        innerCirclePaint.setColor(circleColor);
        Paint outerCirclePaint = new Paint();
        outerCirclePaint.setColor(Color.WHITE );

        double rangeRadian = Math.toRadians( rangeDegree );
        float strengthToX = (float) (center_x + (radius * Math.cos(rangeRadian)));
        float strengthToY = (float) (center_y + (radius * Math.sin( rangeRadian )));

        canvas.drawCircle(strengthToX, strengthToY, outerCircleRadius, outerCirclePaint);
        canvas.drawCircle(strengthToX, strengthToY, innerCircleRadius, innerCirclePaint);
    }

    private void drawRangeText( Canvas canvas , float radius , float center_x , float center_y , float rangeDegree , int textColor , float rangeValue){

        float rangeTextRadius = radius + TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP , 10.5f , this.getResources().getDisplayMetrics() );
        float blankWidth = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 2, this.getResources().getDisplayMetrics());

        Paint rangeTextPaint = new Paint( Paint.ANTI_ALIAS_FLAG );
        rangeTextPaint.setTextSize( TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_SP , 14 , this.getResources().getDisplayMetrics() )  );
        rangeTextPaint.setColor( textColor );

        Paint rangeUnitTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        rangeUnitTextPaint.setTextSize(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 9, this.getResources().getDisplayMetrics()));
        rangeUnitTextPaint.setColor( textColor );

        int rangeTextWidth = this.getTextWidth( (int)rangeValue + "" , rangeTextPaint );
        int rangeUnitTextWidth = this.getTextWidth( this.mRangeUnitText , rangeUnitTextPaint );

        double rangeRadian = Math.toRadians( rangeDegree );
        float rangeTextX = (float) (center_x + ( rangeTextRadius * Math.cos( rangeRadian ) ));
        float rangeTextY = (float) (center_y + ( rangeTextRadius * Math.sin( rangeRadian ) ));

        if( rangeDegree < 270 ){

            rangeTextX -= rangeUnitTextWidth;
            canvas.drawText(this.mRangeUnitText, rangeTextX , rangeTextY, rangeUnitTextPaint );
            rangeTextX -= (rangeTextWidth + blankWidth );
            canvas.drawText(  (int)rangeValue + "" , rangeTextX  , rangeTextY , rangeTextPaint );

        }else if( rangeDegree == 270 ){

            rangeTextX -= (( rangeTextWidth + blankWidth + rangeUnitTextWidth ) / 2 );
            canvas.drawText( (int)rangeValue + " " , rangeTextX , rangeTextY , rangeTextPaint );
            rangeTextX += (rangeTextWidth + blankWidth );
            canvas.drawText(this.mRangeUnitText, rangeTextX , rangeTextY, rangeUnitTextPaint );

        }else{
            canvas.drawText( (int)rangeValue + "" , rangeTextX  , rangeTextY , rangeTextPaint );
            rangeTextX += ( rangeTextWidth + blankWidth);
            canvas.drawText(this.mRangeUnitText, rangeTextX , rangeTextY, rangeUnitTextPaint );
        }
    }

    private void drawRingArc( Canvas canvas, float x , float y ,float xThickness , float yThickness ,  float startDegree ,  float endDegree , Paint paint )
    {
        RectF oval = new RectF( x - xThickness ,y - yThickness , x + xThickness , y + yThickness );

        Path path = new Path();
        if( endDegree > 294 ){
            endDegree = 294;
        }

        path.arcTo(oval, startDegree, endDegree);
        canvas.drawPath(path, paint);
    }

    public void setValue(float value){
        mValue = value;
    }

    public void setCircleThickness( float thickness ){
        mCircleThickness = thickness;
        this.mEmptyCirclePaint.setStrokeWidth(thickness);
        this.mValueCirClePaint.setStrokeWidth(thickness);
    }

    public void setEmptyCircleColor( int emptyCircleColor )
    {
        this.mEmptyCirclePaint.setColor(emptyCircleColor);
    }

    public void setValueColor(int valueColor)
    {
        this.mValueCirClePaint.setColor( valueColor );
    }

    public void setRange( float rangeFrom , float rangeTo , String rangeUnitText ){
        this.mRangeFrom = rangeFrom;
        this.mRangeTo = rangeTo;
        this.mRangeUnitText = rangeUnitText;
    }

    private int getTextWidth(String text, Paint paint)
    {
        Rect bounds = new Rect();
        paint.getTextBounds(text, 0, text.length(), bounds);
        return bounds.width();
    }

    private int getTextHeight(String text, Paint paint)
    {
        Rect bounds = new Rect();
        paint.getTextBounds(text, 0, text.length(), bounds);
        return bounds.height();
    }

}
