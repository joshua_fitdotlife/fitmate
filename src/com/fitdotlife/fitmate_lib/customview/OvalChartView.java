package com.fitdotlife.fitmate_lib.customview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.view.View;

public class OvalChartView extends View
{

	private Context mContext = null;
	
	private int mLeftBlank = 10;
	private int mTopBlank = 10;
	private int mRightBlank = 10;
	private int mBottomBlank = 10;
	
	private int heightSize = 0;
	private int widthSize = 0;
	
	private Path barPath = null;
	private Paint barPaint = null;
	private int mColor = 0;
	
	public OvalChartView(Context context)
	{
		super(context);
		
		this.mContext = context;
		this.init();
	}
	
	private void init()
	{
		this.barPath = new Path();
		this.barPaint = new Paint( Paint.ANTI_ALIAS_FLAG );
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) 
	{
		int heightMode = MeasureSpec.getMode(heightMeasureSpec);
		switch( heightMode )
		{
		case MeasureSpec.UNSPECIFIED:
			heightSize = heightMeasureSpec;
			break;
		case MeasureSpec.AT_MOST:
			heightSize = 200;
			break;
		case MeasureSpec.EXACTLY:
			heightSize = MeasureSpec.getSize(heightMeasureSpec);
			break;
		}
		
		int widthMode = MeasureSpec.getMode(widthMeasureSpec);
		switch( widthMode )
		{
		case MeasureSpec.UNSPECIFIED:
			widthSize = widthMeasureSpec;
			break;
		case MeasureSpec.AT_MOST:
			widthSize = 300;
			break;
		case MeasureSpec.EXACTLY:
			widthSize = MeasureSpec.getSize(widthMeasureSpec);
			break;
		}
		
		this.setMeasuredDimension(widthSize, heightSize);
	}
	
	@Override
	protected void onDraw( Canvas canvas )
	{
		float ovalLeft = this.mLeftBlank;
		float ovalTop = this.mTopBlank;
		float ovalRight = this.getMeasuredWidth() - this.mRightBlank;
		float ovalBottom = this.getMeasuredHeight() - this.mBottomBlank;
		
		drawOval( canvas , ovalLeft , ovalTop , ovalRight , ovalBottom );
	}
	
	private void drawOval( Canvas canvas , float left , float top , float right , float bottom )
	{
		int capRadius = ( this.getMeasuredHeight() - this.mTopBlank - this.mBottomBlank ) / 2;
		
		barPath.addRect( left + capRadius  , top , right - capRadius , bottom , Path.Direction.CW );
		barPath.addCircle( left + capRadius , top + capRadius  , capRadius , Path.Direction.CW );
		barPath.addCircle( right - capRadius , top + capRadius  , capRadius , Path.Direction.CW );
		
		barPath.close();
		
		this.barPaint.setColor(mColor);
		canvas.drawPath( barPath , this.barPaint );
		
		barPath.reset();
	}
	
	public void setColor( int color )
	{
		this.mColor = color;
	}
	
}
