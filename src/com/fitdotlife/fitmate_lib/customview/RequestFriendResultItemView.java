package com.fitdotlife.fitmate_lib.customview;

import android.content.Context;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fitdotlife.fitmate.FriendActivity;
import com.fitdotlife.fitmate.R;
import com.fitdotlife.fitmate.RefreshListener;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.http.FriendService;
import com.fitdotlife.fitmate_lib.http.NetworkClass;
import com.fitdotlife.fitmate_lib.key.FriendStatusType;
import com.fitdotlife.fitmate_lib.object.UserFriend;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringRes;
import org.androidannotations.rest.spring.annotations.RestService;

import java.util.List;

/**
 * Created by Joshua on 2015-08-03.
 */
@EViewGroup(R.layout.child_activity_friend_requestfriend_listitem_result)
public class RequestFriendResultItemView  extends LinearLayout{

    private Context mContext;
    private UserFriend mUserFriend;
    private List<UserFriend> mList;
    private int mId = 0;
    private RefreshListener mRefreshListener;
    private int mRequestType = 0;

    @RestService
    FriendService friendServiceClient;

    @ViewById(R.id.tv_child_activity_friend_requestfriend_listitem_result_content)
    TextView tvContent;

    @StringRes(R.string.friend_requestfriend_delete_friend)
    String strDeleteFriend;

    @StringRes(R.string.friend_requestfriend_accept_friend)
    String strAcceptFriend;

    @StringRes(R.string.friend_requestfriend_cancle_friend)
    String strCancleFriend;

    @ViewById(R.id.child_activity_friend_requestfriend_result_profile)
    ImageView imgProfile;

    public RequestFriendResultItemView(Context context) {
        super(context);
        mContext = context;
    }

    public void setUserFriend( UserFriend userFriend , List<UserFriend> list , int id , FriendStatusType statusType , int requestType){
        friendServiceClient.setRootUrl(NetworkClass.baseURL + "/api");

        this.mUserFriend = userFriend;
        this.mList = list;
        this.mRequestType = requestType;

        String content = "";
        if( statusType.equals( FriendStatusType.ACCEPT ) ){
            content = String.format( strAcceptFriend , userFriend.getName() );
        }else if( statusType.equals( FriendStatusType.DECLINE )){
            content = String.format( strDeleteFriend , userFriend.getName() );
        }else if(statusType.equals( FriendStatusType.CANCLE )){
            content = String.format( strCancleFriend , userFriend.getName() );
        }

        tvContent.setText( content );

        if( !(mUserFriend.getProfileImagePath() == null ) ) {
            DisplayImageOptions options = new DisplayImageOptions.Builder()
                    .showImageForEmptyUri(R.drawable.profile_img1)
                    .showImageOnFail(R.drawable.profile_img1)
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .displayer(new RoundedBitmapDisplayer(200))
                    .build();

            ImageLoader.getInstance().displayImage(NetworkClass.imageBaseURL + mUserFriend.getProfileImagePath(), imgProfile, options);
        }

}

    @UiThread
    void deleteUserFriendFromList( ){
        this.mList.remove( mUserFriend );

        if( this.mRefreshListener != null ){
            this.mRefreshListener.refreshList(FriendActivity.FriendModeType.REQUESTFRIEND );
        }
    }

    public void setRefreshListener( RefreshListener listener ){
        this.mRefreshListener = listener;
    }

    @Background
    void confirm(){

        String email = new FitmateDBManager(mContext).getUserInfo().getEmail();
        boolean result = friendServiceClient.confirm( email , mUserFriend.getUserFriendsID() , this.mRequestType  );

        if(result){
            this.deleteUserFriendFromList();
        }
    }

    @Click(R.id.img_child_activity_friend_requestfriend_listitem_result_confirm)
    void confirmClick(){
        this.confirm();
    }
}
