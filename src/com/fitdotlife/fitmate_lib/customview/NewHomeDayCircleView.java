package com.fitdotlife.fitmate_lib.customview;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fitdotlife.fitmate.R;
import com.fitdotlife.fitmate.newhome.NewHomeProgramClickListener;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

/**
 * Created by Joshua on 2015-12-17.
 */
@EViewGroup(R.layout.child_newhome_day_circle)
public class NewHomeDayCircleView extends RelativeLayout implements ValueAnimator.AnimatorUpdateListener
{
    private float mValue = 85;
    private Context mContext = null;
    private NewHomeProgramClickListener mListener = null;
    private int mExerciseProgramId = 0;
    private int mDuration = 0;
    private float preValue = 0;

    @ViewById(R.id.img_activity_newhome_program)
    ImageView imgProgram;

    @ViewById(R.id.dcv_child_newhome_day_circle)
    DayCircleView dcvDay;

    @ViewById(R.id.tv_child_newhome_day_circle_value)
    TextView tvValue;

    @ViewById(R.id.tv_child_newhome_day_circle_value_unit)
    TextView tvValueUnit;

    @ViewById(R.id.tv_child_newhome_day_circle_category)
    TextView tvCategory;

    @ViewById(R.id.rl_child_newhome_day_circle_content)
    RelativeLayout rlContent;

    public NewHomeDayCircleView(Context context) {

        super(context);
        this.mContext = context;
    }


    public void setValueText( String valueText ){
        tvValue.setText(valueText);
    }

    public void setValue( float value )
    {

        int[] attrs = new int[]{R.attr.newhomeAchieveEmpty , R.attr.newhomeAchieve };
        TypedArray a = mContext.getTheme().obtainStyledAttributes(attrs);


        int achieveEmptyColor = a.getColor(0, 0);
        int achieveColor = a.getColor(1,0);

        a.recycle();

        mValue = value;
        dcvDay.setCircleThickness(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8.5f, getResources().getDisplayMetrics()));
        dcvDay.setEmptyCircleColor(  achieveEmptyColor );
        dcvDay.setValueCircleColor( achieveColor );
        dcvDay.setValue(value);
        dcvDay.invalidate();
    }

    public void setValueUnitText(String valueUnitText ){
        tvValueUnit.setText(valueUnitText);
    }

    public void setCategoryText( String categoryText ){
        tvCategory.setText(categoryText);
    }

    public void setExerciseProgramId( int exerciseProgramID ){
        this.mExerciseProgramId = exerciseProgramID;
    }

    public void startAnimation( int duration , boolean startZero )
    {
        mDuration = duration;

        if( mValue < 10 ) {
            mDuration = (int) (mValue * 100 );
        }

        startAnimation(startZero);
    }

    public void startAnimation( boolean startZero ) {
        float startValue = 0;

        if( !startZero) {
            startValue = preValue;
        }

        float animatedValue = mValue;
        preValue = mValue;
        ValueAnimator animator = ValueAnimator.ofFloat( startValue , animatedValue);
        animator.addUpdateListener(this);
        animator.setDuration(mDuration);
        animator.start();
    }

    public void display(){

        tvValue.setText(String.format("%d", (int) mValue));
        dcvDay.setValue(mValue);
        dcvDay.invalidate();

    }

    @Override
    public void onAnimationUpdate(ValueAnimator animation)
    {
        float value = (float) animation.getAnimatedValue();
        mValue = value;
        display();
    }

    public void setCategoryIntroListener( View.OnClickListener clickListener , int categoryIndex ){

        rlContent.setTag( categoryIndex );
        rlContent.setOnClickListener(clickListener);
    }


}
