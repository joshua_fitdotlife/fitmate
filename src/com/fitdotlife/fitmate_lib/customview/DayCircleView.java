package com.fitdotlife.fitmate_lib.customview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

/**
 * Created by Joshua on 2015-12-17.
 */
public class DayCircleView extends RelativeLayout
{

    private float mValue = 0;
    private Paint mEmptyCirclePaint = null;
    private Paint mValueCirClePaint = null;

    private float mCircleThickness = 0;

    public DayCircleView(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.mEmptyCirclePaint = new Paint( Paint.ANTI_ALIAS_FLAG );
        this.mEmptyCirclePaint.setStyle(Paint.Style.STROKE);

        this.mValueCirClePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.mValueCirClePaint.setStyle(Paint.Style.STROKE);
        this.mValueCirClePaint.setStrokeCap(Paint.Cap.ROUND);

        this.setWillNotDraw(false);
    }

    @Override
    protected void onDraw(Canvas canvas) {

        int viewWidth = this.getMeasuredWidth();

        float center_x = viewWidth / 2;
        float center_y = this.getMeasuredHeight() / 2;

        float radius = ( viewWidth - mCircleThickness ) / 2;
        canvas.drawCircle(center_x, center_y, radius, mEmptyCirclePaint);

        float sweepAngle = ( 360 * mValue ) / 100;
        if( sweepAngle < 360 ) {
            this.drawRingArc(canvas, center_x, center_y, radius, radius, 270, sweepAngle , mValueCirClePaint);
        }else{
            canvas.drawCircle( center_x , center_y , radius , mValueCirClePaint );
        }
        super.onDraw(canvas);
    }

    private void drawRingArc( Canvas canvas, float x , float y ,float xThickness , float yThickness ,  float startDegree ,  float endDegree , Paint paint )
    {
        RectF oval = new RectF( x - xThickness ,y - yThickness , x + xThickness , y + yThickness );

        Path path = new Path();
        path.arcTo(oval, startDegree, endDegree);
        path.arcTo(oval , startDegree , endDegree , true);
        canvas.drawPath(path, paint);
    }

    public void setValue(float value){
        mValue = value;
    }

    public void setCircleThickness( float thickness ){
        mCircleThickness = thickness;
        this.mEmptyCirclePaint.setStrokeWidth(thickness);
        this.mValueCirClePaint.setStrokeWidth(thickness);
    }

    public void setEmptyCircleColor( int emptyCircleColor ){
        this.mEmptyCirclePaint.setColor( emptyCircleColor );
    }

    public void setValueCircleColor( int valueCircleColor ){
        this.mValueCirClePaint.setColor( valueCircleColor );
    }

}
