package com.fitdotlife.fitmate_lib.customview;

import android.content.Context;
import android.widget.LinearLayout;

import com.fitdotlife.fitmate.R;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;

/**
 * Created by Joshua on 2015-08-03.
 */
@EViewGroup(R.layout.child_activity_friend_myfriend_no_content)
public class FriendNoContentView extends LinearLayout{

    public interface MoveListener {
        void clickMoveFindFreind();
    }

    private MoveListener mMoveListener = null;

    public FriendNoContentView(Context context) {
        super(context);
    }

    public void setMoveListener(MoveListener listener){
        this.mMoveListener = listener;
    }

    @Click(R.id.btn_child_activity_friend_myfriend_nocontent_move)
    void moveClick (){
        if(this.mMoveListener != null) {
            this.mMoveListener.clickMoveFindFreind();
        }
    }
}
