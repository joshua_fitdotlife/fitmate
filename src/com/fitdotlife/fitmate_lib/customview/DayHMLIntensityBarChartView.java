package com.fitdotlife.fitmate_lib.customview;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.TypedValue;
import android.view.View;

import com.fitdotlife.fitmate.R;

import java.util.ArrayList;
import java.util.List;

public class DayHMLIntensityBarChartView extends View implements AnimatorUpdateListener
{
	private final int DEFAULT_MAX_VALUE = 100;

	private float series_right_blank = 0;
	private float series_text_blank = 0;

	private Context mContext = null;

	private int mTopBlank = 10;
	private int mBottomBlank = 10;
	private int mLeftBlank = 0;
	private int mRightBlank = 0;

	private int mTextTopBlank = 0;
	private int mTextBottomBlank = 0;
	private int mTextLeftBlank = 0;
	private int mTextRightBlank = 0;

	private float mYTitleTextBlank = 0;

	private int heightSize = 0;
	private int widthSize = 0;

	private String mYAxisTitle = "";
	private String[] mYAxisTextList = null;
	private String mXAxisTitle = "";
	private int[] mValues = null;
	private int[] mColors = null;
	private int[] mHashes = null;
	private int[] mBarWidths = null;

	private int mAxisTextSize = 0;
	private int mBarTextSize = 0;
	private float mLabelBlank;

	private Paint axisPaint = null;
	private Paint seriesPaint = null;
	private Paint textPaint = null;

	private int mMaxValue = 0;

	private int[] mPreValues = null;

	public DayHMLIntensityBarChartView(Context context)
	{
		super( context );
		this.mContext = context;

		this.init();
	}

	private void init()
	{
		this.mTopBlank = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP , 5 , this.getResources().getDisplayMetrics());
		this.mLeftBlank = this.getResources().getDimensionPixelSize(R.dimen.week_exercisetime_bar_chart_left_blank);
		this.mRightBlank = this.getResources().getDimensionPixelSize(R.dimen.day_intensity_bar_chart_right_blank);
		this.mAxisTextSize = this.getResources().getDimensionPixelSize(R.dimen.day_intentisy_bar_chart_axis_text_size);
		this.mBarTextSize = this.getResources().getDimensionPixelSize(R.dimen.day_intensity_bar_chart_bar_text_size);
		this.mTextLeftBlank = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP , 24 , this.getResources().getDisplayMetrics());
		this.mTextRightBlank = this.getResources().getDimensionPixelSize(R.dimen.day_intensity_bar_chart_text_right_blank);
		this.mTextTopBlank = this.getResources().getDimensionPixelSize(R.dimen.day_intensity_bar_chart_text_top_blank);
		this.mTextBottomBlank = this.getResources().getDimensionPixelSize(R.dimen.day_intensity_bar_chart_text_bottom_blank);
		this.mLabelBlank =(int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP , 5.33f , this.getResources().getDisplayMetrics());
		this.series_text_blank = TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP , 3.33f , this.getResources().getDisplayMetrics() );
		this.mYTitleTextBlank = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 7, this.getResources().getDisplayMetrics());

		this.axisPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		this.axisPaint.setColor( 0xFF939393 );

		this.seriesPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		this.seriesPaint.setTextSize(this.mBarTextSize);

		this.textPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		this.textPaint.setTextSize(this.mAxisTextSize);
		this.textPaint.setColor(0xFF666666);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
	{
		int heightMode = MeasureSpec.getMode(heightMeasureSpec);
		switch( heightMode )
		{
			case MeasureSpec.UNSPECIFIED:
				heightSize = heightMeasureSpec;
				break;
			case MeasureSpec.AT_MOST:
				heightSize = 200;
				break;
			case MeasureSpec.EXACTLY:
				heightSize = MeasureSpec.getSize(heightMeasureSpec);
				break;
		}

		int widthMode = MeasureSpec.getMode(widthMeasureSpec);
		switch( widthMode )
		{
			case MeasureSpec.UNSPECIFIED:
				widthSize = widthMeasureSpec;
				break;
			case MeasureSpec.AT_MOST:
				widthSize = 300;
				break;
			case MeasureSpec.EXACTLY:
				widthSize = MeasureSpec.getSize(widthMeasureSpec);
				break;
		}

		this.setMeasuredDimension(widthSize, heightSize);
	}

	@Override
	protected void onDraw(Canvas canvas)
	{
		int viewHeight = this.getMeasuredHeight();
		int viewWidth = this.getMeasuredWidth();

		float yAxisTitleHeight = this.getTextHeight(this.mYAxisTitle, this.textPaint);
		float yAxisTitleWidth = this.getTextWidth(this.mYAxisTitle, this.textPaint);
		float xAxisTitleHeight=  this.getTextHeight(this.mXAxisTitle, this.textPaint);

		//바의 크기와 바 간격 계산
		float chartHeight = viewHeight - this.mTopBlank - this.mBottomBlank - mYTitleTextBlank  - mLabelBlank - xAxisTitleHeight - yAxisTitleHeight;
		float chartWidth = viewWidth - this.mLeftBlank - this.mRightBlank - mTextLeftBlank - mTextRightBlank ;

		float tempBarSpace = chartHeight / ( ( this.mValues.length * 2 ) + 1 );
		float barHeight = tempBarSpace;
		float barBlank = tempBarSpace;
		float chartBottomMargin = barBlank;

		//가장 큰 값의 문자 크기를 가져온다.
		float maxValueTextWidth = this.getTextWidth( String.valueOf(this.mMaxValue) , this.seriesPaint );

		//가장 큰값이 차지하는 바의 크기
		float maxValueBarWidth = chartWidth - this.series_text_blank - maxValueTextWidth;

		//yAxisTextListPaint.setTypeface( Typeface.create(Typeface.DEFAULT, Typeface.BOLD) );

		this.axisPaint.setStyle( Paint.Style.STROKE );

		//차트 외관선 그리기
		float axisLeft = mLeftBlank + mTextLeftBlank ;
		float axisRight = axisLeft + chartWidth;
		float axisTop = mTopBlank + yAxisTitleHeight +  mYTitleTextBlank;
		float axisBottom =axisTop+chartHeight;
		canvas.drawLine(axisLeft , axisTop , axisLeft , axisBottom , this.axisPaint);
		canvas.drawLine(axisLeft , axisBottom , axisRight , axisBottom , this.axisPaint);

		//머리 문자와 꼬리 문자 그리기
		canvas.drawText( this.mYAxisTitle ,  axisLeft - ( yAxisTitleWidth / 2)  , axisTop - mYTitleTextBlank , this.textPaint );
		canvas.drawText( this.mXAxisTitle ,  axisRight - this.getTextWidth( this.mXAxisTitle , this.textPaint ), axisBottom + mLabelBlank+ xAxisTitleHeight,  this.textPaint );

		//계열 그리기
		for(int i = 0 ; i < this.mValues.length ;i++) {

			float yTextHeight = this.getTextHeight(this.mYAxisTextList[i], this.textPaint);
			float yTextWdith = this.getTextWidth(this.mYAxisTextList[i], this.textPaint);
			canvas.drawText(this.mYAxisTextList[i], mLeftBlank+ mTextLeftBlank-mLabelBlank- yTextWdith, (mTopBlank + mYTitleTextBlank + yAxisTitleHeight + chartHeight) - (chartBottomMargin + ((barHeight + barBlank) * i) + (barHeight / 2)) + (yTextHeight / 2), this.textPaint);

			this.seriesPaint.setColor(this.mColors[i]);
			float barTextHeight = this.getTextHeight(String.valueOf(this.mBarWidths[i]), this.seriesPaint);
			float barTextWidth = this.getTextWidth(String.valueOf(this.mBarWidths[i]), this.seriesPaint);


			float top = (mTopBlank + mYTitleTextBlank + yAxisTitleHeight + chartHeight) - (chartBottomMargin + ((barHeight + barBlank) * i)) - barHeight;
			float bottom = (mTopBlank + mYTitleTextBlank + yAxisTitleHeight + chartHeight) - (chartBottomMargin + ((barHeight + barBlank) * i));
			float barRight = axisLeft + ((this.mBarWidths[i] * maxValueBarWidth) / (this.mMaxValue));

			canvas.drawRect(axisLeft, top, barRight, bottom, this.seriesPaint);

			if (this.mBarWidths[i] > 0) {
				canvas.drawText(String.valueOf((int)this.mBarWidths[i]), barRight + this.series_text_blank, (mTopBlank + mYTitleTextBlank + yAxisTitleHeight + chartHeight) - (chartBottomMargin + ((barHeight + barBlank) * i) + (barHeight / 2)) + (barTextHeight / 2), this.seriesPaint);
			}
		}
	}

	public void setYAxisTitle( String yAxisTitle )
	{
		this.mYAxisTitle = yAxisTitle;
	}
	public void setYAxisTextList( String[] yAxisTextList )
	{
		this.mYAxisTextList = yAxisTextList;
	}
	public void setXAxisTitle( String xAxisTitle )
	{
		this.mXAxisTitle = xAxisTitle;
	}

	public void setValues( int[] values )
	{
		this.mValues = values;

		for( int i = 0 ; i < this.mValues.length ; i++ )
		{
			if( this.mMaxValue < mValues[i] )
			{
				this.mMaxValue = mValues[i];
			}
		}

		if( mMaxValue <= this.DEFAULT_MAX_VALUE ){
			this.mMaxValue = this.DEFAULT_MAX_VALUE;
		}
	}

	public void setColors( int[] colors )
	{
		this.mColors = colors;
	}

	private float getTextWidth(String text, Paint paint)
	{
		Rect bounds = new Rect();
		paint.getTextBounds(text, 0, text.length(), bounds);
		return bounds.width();
	}

	private float getTextHeight(String text, Paint paint)
	{
		Rect bounds = new Rect();
		paint.getTextBounds(text, 0, text.length(), bounds);
		return bounds.height();
	}

	public void startAnimation( int duration )
	{
		List<Animator> listValueAnimator = new ArrayList<Animator>();
		this.mHashes = new int[ this.mValues.length ];
		this.mBarWidths = new int[ this.mValues.length ];

		for( int i = 0 ; i < this.mValues.length ; i++ ) {

			int preValue = 0;
			if( mPreValues != null ){
				preValue = mPreValues[i];
			}

			ValueAnimator animator = ValueAnimator.ofInt( preValue , this.mValues[i]);
			this.mHashes[i] = animator.hashCode();
			animator.addUpdateListener(this);
			listValueAnimator.add(animator);
		}

		AnimatorSet animatorSet = new AnimatorSet();
		animatorSet.playTogether( listValueAnimator );
		animatorSet.setDuration( duration );
		animatorSet.start();

		this.mPreValues = this.mValues;
	}

	@Override
	public void onAnimationUpdate(ValueAnimator animation)
	{
		int hash = animation.hashCode();
		int index = this.arraySearch( hash );

		if( index != -1 ) {
			this.mBarWidths[index] = (Integer) animation.getAnimatedValue();
			this.invalidate();
		}
	}

	private int arraySearch( int src )
	{
		int result = -1;
		for( int i = 0 ; i < this.mHashes.length ; i++ )
		{
			if( this.mHashes[i] == src )
			{
				result = i;
				break;
			}
		}
		return result;
	}
}