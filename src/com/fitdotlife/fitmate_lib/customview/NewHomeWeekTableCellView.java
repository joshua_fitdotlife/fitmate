package com.fitdotlife.fitmate_lib.customview;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fitdotlife.fitmate.NewHomeActivity;
import com.fitdotlife.fitmate.R;
import com.fitdotlife.fitmate.newhome.WeekPagerAdapter;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

/**
 * Created by Joshua on 2015-12-18.
 */
@EViewGroup(R.layout.child_newhome_week_table_cell)
public class NewHomeWeekTableCellView extends RelativeLayout{

    @ViewById(R.id.tv_child_newhome_week_table_cell_score)
    TextView tvScore;

    @ViewById( R.id.tv_child_newhome_week_table_cell_day )
    TextView tvDay;

    @ViewById( R.id.img_newhome_week_table_cell_daysel)
    ImageView imgDaySel;

    public NewHomeWeekTableCellView(Context context) {
        super(context);
    }

    public void setScore(double score, String title, OnClickListener clickListener){

        if( score >= 0 ){
            String scoreString = String.format("%d", (int) score);
            this.tvScore.setText( scoreString );
            if( clickListener != null ) {
                this.setOnClickListener(clickListener);
            }
        }else{

            if(score == WeekPagerAdapter.TODAY_AFTER_NODATA){
                tvScore.setVisibility(View.INVISIBLE);
            }else{
                tvScore.setText("0");
                if( clickListener != null ) {
                    this.setOnClickListener(clickListener);
                }
            }
        }

        this.tvDay.setText(title);
    }

    public void setScore(String score , String title , int scoreColor , int titleColor)
    {
        this.tvScore.setTextColor(scoreColor);
        this.tvScore.setText(score);
        this.tvDay.setTextColor(titleColor);
        this.tvDay.setText(title);
    }

    public void setSelectedDay( boolean isSelected )
    {
        if(isSelected){
            tvDay.setTextColor( this.getResources().getColor(R.color.newhome_week_date_sel) );
            imgDaySel.setVisibility( View.VISIBLE );
        }else{
            tvDay.setTextColor( this.getResources().getColor(R.color.newhome_week_date) );
            imgDaySel.setVisibility( View.INVISIBLE );
        }
    }
}