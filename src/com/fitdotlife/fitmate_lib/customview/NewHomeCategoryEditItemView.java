package com.fitdotlife.fitmate_lib.customview;

import android.content.Context;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fitdotlife.fitmate.R;
import com.fitdotlife.fitmate.newhome.CategoryEditInfo;
import com.fitdotlife.fitmate.newhome.CategoryType;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

/**
 * Created by Joshua on 2016-03-10.
 */
@EViewGroup(R.layout.child_newhome_categoryedit_item)
public class NewHomeCategoryEditItemView extends LinearLayout{

    private Context mContext;
    private CategoryType categoryType = null;
    private boolean isVisible = false;
    private int categoryViewIndex = 0;

    @ViewById(R.id.tv_child_newhome_categoryedit_category)
    TextView tvCategory;

    @ViewById(R.id.tv_child_newhome_categoryedit_value_unit)
    TextView tvValueUnit;

    @ViewById(R.id.tv_child_newhome_categoryedit_value)
    TextView tvValue;

    @ViewById(R.id.img_child_newhome_categoryedit_item)
    ImageView imgCategory;

    public NewHomeCategoryEditItemView(Context context) {
        super(context);
        mContext = context;
    }

    public void setCategoryText( String categoryText )
    {
        tvCategory.setText( categoryText );
    }

    public void setValueUnitText( String valueUnitText ){
        tvValueUnit.setText(valueUnitText);
    }

    public void setValue( String valueText ){
        tvValue.setText(valueText);
    }

    public void setImage( int resId){
        imgCategory.setImageResource(resId);
    }

    public void bind( CategoryEditInfo categoryEditInfo){

        tvValueUnit.setText( categoryEditInfo.getValueUnitText()  );
        tvValue.setText( categoryEditInfo.getValueText() );
        tvCategory.setText(categoryEditInfo.getCategoryText());
        isVisible = categoryEditInfo.isVisible();
        categoryType = categoryEditInfo.getCategoryType();
        categoryViewIndex = categoryEditInfo.getViewIndex();

        if( isVisible ){

            imgCategory.setImageResource( R.drawable.newhome_categoryedit_item_select );

        }else{

            imgCategory.setImageResource( R.drawable.newhome_categoryedit_item_normal );
        }

    }

    public void setVisible(boolean selected){
        isVisible = selected;
    }

    public boolean isVisible(){
        return isVisible;
    }

    @Override
    public String toString() {
        return categoryType.ordinal() + "_" + (isVisible ? 1 : 0 ) ;
    }

    public CategoryType getCategoryType(){
        return categoryType;
    }

    public int getCategoryViewIndex() {
        return categoryViewIndex;
    }

    public void setCategoryViewIndex(int categoryViewIndex) {
        this.categoryViewIndex = categoryViewIndex;
    }

}
