package com.fitdotlife.fitmate_lib.customview;//package com.fitdotlife.icaredpilotapp.view;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;

import com.fitdotlife.fitmate.R;

public class LineChartView extends View implements AnimatorUpdateListener
{

	private Context mContext;

	public void setxAxisTextTitle(String xAxisTextTitle) {
		this.xAxisTextTitle = xAxisTextTitle;
	}

	private String xAxisTextTitle = "시간(시)";
    int minutesperPoint=10;
    final float lineStrokeWidth= 4;
	private Paint mLinePaint;
	private Paint mAxisPaint;
	private Paint mXAxisTextPaint;
	private Paint mYAxisTextPaint;

	private int mXValue;
	private int mYValue;

	private float[] mValues = null;
	private float mMaxValue;

	private final int pointRadius = 5;

	private int mTopBlank = 10;
	private int mBottomBlank = 10;
	private int mLeftBlank = 10;
	private int mRightBlank = 10;

	private int mLabelBlank;
	private int mYTitleTextBlank;

	private int mTopTextBlank = 10;
	private int mBottomTextBlank = 10;
	private int mLeftTextBlank = 10;

	private float mAxisTextSize = 0;

    private float mGapX = 0;
    private float mGapY = 0;

    float chartHeight;
    float chartWidth;

	float axisLeft;
	float axisTop;
	float axisBottom;
	float axisRight;


	public LineChartView(Context context)
	{
		super(context);
		this.mContext = context;

		this.init();
	}

	private void init(){

		this.mAxisTextSize = this.getResources().getDimensionPixelSize(R.dimen.week_exercisetime_bar_chart_axis_text_size);
		this.mLeftBlank = this.getResources().getDimensionPixelSize(R.dimen.week_exercisetime_bar_chart_left_blank);
		this.mRightBlank = this.getResources().getDimensionPixelSize(R.dimen.week_exercisetime_bar_chart_right_blank);
		this.mTopBlank = this.getResources().getDimensionPixelSize(R.dimen.week_exercisetime_bar_chart_top_blank);
		this.mBottomBlank = this.getResources().getDimensionPixelSize(R.dimen.week_exercisetime_bar_chart_bottom_blank);

		this.mLabelBlank =this.getResources().getDimensionPixelSize(R.dimen.chartAxisLabelBlank); //3.33dp
		this.mYTitleTextBlank = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 7, this.getResources().getDisplayMetrics());

		this.mLeftTextBlank =this.getResources().getDimensionPixelSize(R.dimen.week_exercisetime_bar_chart_text_left_blank);//24dp
		this.mTopTextBlank = this.getResources().getDimensionPixelSize(R.dimen.week_exercisetime_bar_chart_text_top_blank); //5dp
		this.mBottomTextBlank = this.getResources().getDimensionPixelSize(R.dimen.week_exercisetime_bar_chart_text_bottom_blank);



		this.mLinePaint = new Paint( Paint.ANTI_ALIAS_FLAG );
		this.mLinePaint.setStyle( Paint.Style.STROKE );
		this.mLinePaint.setStrokeWidth(lineStrokeWidth);
		this.mLinePaint.setColor(0xFFFF5033);

		this.mAxisPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		this.mAxisPaint.setColor( 0xFF939393 );
		this.mAxisPaint.setStrokeWidth(1);

		this.mXAxisTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		this.mXAxisTextPaint.setTextSize( this.mAxisTextSize);
		this.mXAxisTextPaint.setColor(0xFF666666);
		this.mXAxisTextPaint.setAntiAlias(true);

		this.mYAxisTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		this.mYAxisTextPaint.setTextSize( this.mAxisTextSize);
		this.mYAxisTextPaint.setColor(0xFF666666);
		this.mYAxisTextPaint.setAntiAlias(true);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec , int heightMeasureSpec)
	{
		int heightMode = MeasureSpec.getMode(heightMeasureSpec);
		int heightSize = 0;

		switch( heightMode )
		{
		case MeasureSpec.UNSPECIFIED:
			heightSize = heightMeasureSpec;
			break;
		case MeasureSpec.AT_MOST:
			heightSize = 200;
			break;
		case MeasureSpec.EXACTLY:
			heightSize = MeasureSpec.getSize(heightMeasureSpec);
			break;
		}

		int widthMode = MeasureSpec.getMode(widthMeasureSpec);
		int widthSize = 0;
		switch( widthMode )
		{
		case MeasureSpec.UNSPECIFIED:
			widthSize = widthMeasureSpec;
			break;
		case MeasureSpec.AT_MOST:
			widthSize = 300;
			break;
		case MeasureSpec.EXACTLY:
			widthSize = MeasureSpec.getSize(widthMeasureSpec);
			break;
		}

		this.setMeasuredDimension(widthSize, heightSize);
	}

	protected void onDraw(Canvas canvas)
	{
		axisLeft =  this.mLeftBlank +  mLeftTextBlank;
		axisRight =this.getMeasuredWidth()- this.mRightBlank;
		chartWidth = axisRight- axisLeft;

		axisTop = this.mTopBlank+ this.mTopTextBlank+this.mYTitleTextBlank;
		axisBottom = this.getMeasuredHeight() - this.mLabelBlank - this.mBottomBlank- this.mBottomTextBlank;

        chartHeight = axisBottom-axisTop;
        this.drawLine( canvas );

	}

	private void drawLine( Canvas canvas )
	{
		//draw axis
		canvas.drawLine( axisLeft , axisTop ,  axisLeft , axisBottom , this.mAxisPaint );
		canvas.drawLine( axisLeft , axisBottom  , axisRight , axisBottom  , this.mAxisPaint );

		if( this.mValues == null ) return;

		float xAxisTextTitleY = axisBottom + this.mLabelBlank + this.getTextHeight(xAxisTextTitle, mXAxisTextPaint);
		mGapY = chartHeight / ( mMaxValue - 1 );
		mGapX = chartWidth / (this.mValues.length);

        //XAxis Text 그리기
        int[] arrXAxisText = new int[]{ 6 , 12 , 18 };
        float x= axisLeft;
        float xAxisTitleGap= chartWidth / 4;

        for( int i = 0 ; i < arrXAxisText.length ; i++ )
        {
            x+=xAxisTitleGap;
			int xAxisTextWidth = this.getTextWidth(String.valueOf( arrXAxisText[i] ) , mXAxisTextPaint );
            canvas.drawText( String.valueOf( arrXAxisText[i] ) , x -xAxisTextWidth/2 , xAxisTextTitleY , mXAxisTextPaint );
        }



        //XAxis Title 그리기
        int xAxisTextTitleWidth = this.getTextWidth(xAxisTextTitle, mXAxisTextPaint);
        float xAxisTextTitleX = axisRight - xAxisTextTitleWidth;
		//float xAxisTextTitleY = this.mTopBlank + chartHeight+this.mLabelBlank + this.getTextHeight(xAxisTextTitle, mXAxisTextPaint);
        canvas.drawText(xAxisTextTitle, xAxisTextTitleX, xAxisTextTitleY, mXAxisTextPaint);

        //yAxis Title 그리기
        String yAxisTextTitle = "MET";
        float yAxisTextTitleX = axisLeft - ( this.getTextWidth(yAxisTextTitle, this.mYAxisTextPaint)/2);
        float yAxisTextTitleY = axisTop - mYTitleTextBlank;
        canvas.drawText(yAxisTextTitle, yAxisTextTitleX, yAxisTextTitleY, mYAxisTextPaint);

        //YAxis Text 그리기
        for (int i = 1; i < mMaxValue; ) {
            String yAxisText = String.valueOf(i);
            int textHeight = this.getTextHeight(yAxisText, mYAxisTextPaint);
            canvas.drawText(String.valueOf(i), axisLeft-mLabelBlank- this.getTextWidth(yAxisText, this.mYAxisTextPaint), axisTop + ( chartHeight - (mGapY * (i - 1)) + (textHeight / 2) ), mYAxisTextPaint);
            i += (int) mMaxValue/3;
        }

        //Path path = new Path();
        float preX = axisLeft;
        float preY = axisBottom;
        boolean isPreData = false;
        float gapX=chartWidth/this.mValues.length;

		float y=0;
        x= preX-gapX;
        for( int i = 0 ; i < this.mValues.length ; i++ )
        {
            x +=gapX;
            if( Float.isNaN(mValues[i]) || this.mValues[i] == 0 ) {
                isPreData = false;
                preY = axisBottom;
                continue;
            }
            y = axisTop + ( chartHeight - ( ( mValues[i] - 1 ) * mGapY ) );

            if(isPreData) {
                canvas.drawLine(preX, preY, x, y, this.mLinePaint);
            }else{
                isPreData = true;
            }

            preX = x;
            preY = y;
        }

        //canvas.drawPath( path , this.mLinePaint );

	}



	public void setValues( float[] values)
	{
        int dataSavingInterval = 86400/values.length;


		if( values == null  ) return;

        int countPerPoint = 60 * minutesperPoint / dataSavingInterval;
        float tempsum =0;
        int count_over1=0;
        mValues= new  float[values.length/countPerPoint];

        int lastindex=0;

        float max=1.0f;
        for(int i=0;i<values.length;i+=countPerPoint){
            count_over1=0;
            tempsum=0;
            for(int count=0;count<countPerPoint;count++){
                if(values[i+count]>=1){
                    tempsum+= values[i+count];
                    count_over1++;
                    lastindex = i+count;
                }
            }
            if(count_over1>0){


                mValues[i/countPerPoint]= tempsum/count_over1;
                if(max< mValues[i/countPerPoint]){
                    max= mValues[i/countPerPoint];
                }
            }
        }


        if(max<6 ) mMaxValue=6;
        else{
            mMaxValue = (int)(max)+1.5f;
        }

        Log.d("lastindex", ""+lastindex);
	}



    private void setYMax( float value )
    {
        if( this.mMaxValue < value )
        {
            this.mMaxValue = value;
        }
    }

	public void startAnimation( int duration )
	{
        ValueAnimator valueAnimator = ValueAnimator.ofInt( 0,this.mValues.length - 1 );
        valueAnimator.addUpdateListener(this);
        valueAnimator.setDuration( duration );
        valueAnimator.start();
	}

	@Override
	public void onAnimationUpdate(ValueAnimator valueAnimator)
	{

	}



	private int getTextHeight(String text, Paint paint)
	{
		Rect bounds = new Rect();
		paint.getTextBounds(text, 0, text.length(), bounds);
		return bounds.height();
	}

	private int getTextWidth(String text, Paint paint)
	{
		Rect bounds = new Rect();
		paint.getTextBounds(text, 0, text.length(), bounds);
		return bounds.width();
	}
}
