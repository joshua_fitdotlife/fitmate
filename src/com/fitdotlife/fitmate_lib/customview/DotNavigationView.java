package com.fitdotlife.fitmate_lib.customview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import com.fitdotlife.fitmate.R;

/**
 * Created by Joshua on 2015-06-17.
 */

public class DotNavigationView extends View {

    private int heightSize = 0;
    private int widthSize = 0;

    private Context mContext = null;

    private float dotSize = 0;
    private float blankSize = 0;
    private int dotNumber = 0;
    private int dotIndex = 0;


    public DotNavigationView(Context context) {
        super(context);
        this.mContext = context;
    }

    public DotNavigationView(Context context, AttributeSet attrs) {
        super(context , attrs);

        this.mContext = context;
        TypedArray a = context.obtainStyledAttributes(attrs , R.styleable.DotNavigationView);

        dotSize = a.getDimension( R.styleable.DotNavigationView_dotSize, 0 );
        blankSize = a.getDimension(R.styleable.DotNavigationView_blankSize , 0);
        dotNumber = a.getInt(R.styleable.DotNavigationView_dotNumber, 0);
        dotIndex = a.getInt(R.styleable.DotNavigationView_dotIndex, 1);

        a.recycle();
    }


    @Override
    protected void onMeasure(int widthMeasureSpec , int heightMeasureSpec)
    {
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        switch( heightMode )
        {
            case MeasureSpec.UNSPECIFIED:
                heightSize = heightMeasureSpec;
                break;
            case MeasureSpec.AT_MOST:
                heightSize = 200;
                break;
            case MeasureSpec.EXACTLY:
                heightSize = MeasureSpec.getSize(heightMeasureSpec);
                break;
        }

        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        switch( widthMode )
        {
            case MeasureSpec.UNSPECIFIED:
                widthSize = widthMeasureSpec;
                break;
            case MeasureSpec.AT_MOST:
                widthSize = 300;
                break;
            case MeasureSpec.EXACTLY:
                widthSize = MeasureSpec.getSize(widthMeasureSpec);
                break;
        }

        this.setMeasuredDimension(widthSize, heightSize);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if( dotNumber ==0 ){return;}
        float centerX = this.getMeasuredWidth() / 2 ;
        float centerY = this.getMeasuredHeight() / 2;

        //시작 위치를 알아온다.
        float halfWidth = ((dotNumber/2) * dotSize ) + (( (dotNumber - 1 ) / 2 ) * blankSize);
        float startX = centerX - halfWidth;

        Paint circlePaint = new Paint();
        for(int i = 0 ; i < dotNumber ;i++){

            if( i == dotIndex -1 ){
                circlePaint.setColor(0xFFA0A0A0);
            }else{
                circlePaint.setColor(0xFF4C4C4C);
            }

            canvas.drawCircle( startX + ( i *(dotSize + blankSize) ) , centerY , dotSize / 2 , circlePaint );
        }
    }

    public void setDotNumber( int dotNum ){
        this.dotNumber = dotNum;
    }

    public void setDotIndex(int dotIndex){
        this.dotIndex = dotIndex;
        this.invalidate();
    }

}
