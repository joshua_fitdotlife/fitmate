package com.fitdotlife.fitmate_lib.customview;

import android.content.Context;
import android.content.Intent;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fitdotlife.fitmate.FriendActivity;

import com.fitdotlife.fitmate.FriendProfileAgreeActivity_;
import com.fitdotlife.fitmate.R;
import com.fitdotlife.fitmate.RefreshListener;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.http.FriendService;
import com.fitdotlife.fitmate_lib.http.NetworkClass;
import com.fitdotlife.fitmate_lib.http.NewsService;
import com.fitdotlife.fitmate_lib.key.FriendStatusType;
import com.fitdotlife.fitmate_lib.key.NewsType;
import com.fitdotlife.fitmate_lib.object.News;
import com.fitdotlife.fitmate_lib.object.UserFriend;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.util.DateUtils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringRes;
import org.androidannotations.rest.spring.annotations.RestService;

import java.util.List;

/**
 * Created by Joshua on 2015-08-03.
 */
@EViewGroup(R.layout.child_activity_friend_requestfriend_listitem_receive_request)
public class RequestFriendReceiveRequestItemView  extends LinearLayout {

    private Context mContext;
    private UserFriend mUserFriend;
    private List<UserFriend> mReceiveList;
    private RefreshListener mRefreshListener = null;
    private int mId = 0;
    private UserInfo mUserInfo = null;

    @ViewById(R.id.tv_child_activity_friend_requestfriend_listitem_receive_request_content)
    TextView tvContent;

    @ViewById(R.id.btn_child_activity_friend_requestfriend_listitem_accept)
    Button btnAccept;

    @RestService
    FriendService friendServiceClient;

    @ViewById(R.id.child_activity_friend_requestfriend_receive_request_profile)
    ImageView imgProfile;

    @StringRes(R.string.alarm_accept_friend)
    String strAcceptFriend;


    @RestService
    NewsService newsServiceClient;

    public RequestFriendReceiveRequestItemView(Context context) {
        super(context);
        this.mContext = context;
    }

    public void setUserFriend( UserFriend userFriend , List<UserFriend> receiveList , int id , String email ){
        newsServiceClient.setRootUrl(NetworkClass.baseURL + "/api");
        friendServiceClient.setRootUrl(NetworkClass.baseURL + "/api");

        this.mUserFriend = userFriend;
        this.mReceiveList = receiveList;
        this.mId = id;
        this.mUserInfo = new FitmateDBManager(this.mContext).getUserInfo();

        String content = String.format( mContext.getString(R.string.friend_requestfriend_request_friend) , userFriend.getName() );
        tvContent.setText( content );

        if( !(mUserFriend.getProfileImagePath() == null ) ) {
            DisplayImageOptions options = new DisplayImageOptions.Builder()
                    .showImageForEmptyUri(R.drawable.profile_img1)
                    .showImageOnFail(R.drawable.profile_img1)
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .displayer(new RoundedBitmapDisplayer(200))
                    .build();

            ImageLoader.getInstance().displayImage(NetworkClass.imageBaseURL + mUserFriend.getProfileImagePath(), imgProfile, options);
        }

    }

    public void setRefreshListener( RefreshListener listener  ){
        this.mRefreshListener = listener;
    }

    @Background
    void changeStatus(  FriendStatusType statusType  ){
        boolean result = friendServiceClient.changeStatus( mUserInfo.getEmail() , mUserFriend.getUserFriendsID() , statusType.getValue());

        if( result ){
            refreshList( statusType );
        }
    }

    @UiThread
    void refreshList(  FriendStatusType statusType ){
        UserFriend userFriend = this.mReceiveList.get(this.mId);
        userFriend.setRequestStatus( statusType.getValue() );

        this.mRefreshListener.refreshList(FriendActivity.FriendModeType.REQUESTFRIEND);
    }

    @Click(R.id.btn_child_activity_friend_requestfriend_listitem_decline)
    void declineClick(){

        this.changeStatus(FriendStatusType.DECLINE);

    }

    @Click(R.id.btn_child_activity_friend_requestfriend_listitem_accept)
    void acceptClick(){

        this.changeStatus(FriendStatusType.ACCEPT);
        //this.addNews();
    }

//    @Background
//    void addNews( ){
//        News myNews = new News();
//        myNews.setContent(String.format(strAcceptFriend, mUserInfo.getName(), mUserFriend.getName()));
//        myNews.setFriendcontent(String.format(strAcceptFriend, mUserInfo.getName(), mUserFriend.getName()));
//        myNews.setType(NewsType.ACCEPT_FRIEND.getValue());
//        myNews.setPublishdate(DateUtils.getUTCTimeMilliseconds() );
//        newsServiceClient.addNews( myNews , mUserInfo.getEmail() );
//    }


    @Click(R.id.ll_child_activity_friend_requestfriend_listitem_receive_request)
    void intentClick(){

        FriendProfileAgreeActivity_.intent(mContext).extra("UserFriend",mUserFriend).startForResult( FriendActivity.FRIEND_REQUESTFRIEND_FRIEND );
    }
}
