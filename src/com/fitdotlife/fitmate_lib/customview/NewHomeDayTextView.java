package com.fitdotlife.fitmate_lib.customview;

import android.animation.ValueAnimator;
import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fitdotlife.fitmate.R;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

/**
 * Created by Joshua on 2015-12-16.
 */
@EViewGroup(R.layout.child_newhome_day_text)
public class NewHomeDayTextView extends LinearLayout implements ValueAnimator.AnimatorUpdateListener{

    private Context mContext;
    private float mValue = 0;
    private float preValue = 0;
    private int mPointNumber = 0;
    private int mDuration = 0;

    @ViewById(R.id.ll_child_newhome_day_text_content)
    LinearLayout llContent;

    @ViewById(R.id.child_newhome_day_text_comment)
    TextView tvComment;

    @ViewById(R.id.child_newhome_day_text_value)
    TextView tvValue;

    @ViewById(R.id.child_newhome_day_text_value_unit)
    TextView tvValueUnit;

    @ViewById(R.id.child_newhome_day_text_category)
    TextView tvCategory;

    public NewHomeDayTextView(Context context) {
        super(context);
        mContext = context;
    }

    public void setCommnetText( String comment ){
        this.tvComment.setText( comment );
    }

    public void setValueUnitText( String valueUnit ){this.tvValueUnit.setText( valueUnit );}

    public void setCategoryText(String title){
        this.tvCategory.setText(title);
    }

    public void setmPointNumber(int pointNumber){ this.mPointNumber = pointNumber; }

    public void setValue( float value ){
        this.mValue = value;
    }


    public void display(  )
    {
        String valueText = null;
        if( mPointNumber > 0 ){
            valueText = String.format( "%." + mPointNumber + "f" , mValue);
        }else{
            valueText = String.format( "%d" , (int) mValue);
        }

        this.tvValue.setText( valueText );

        this.invalidate();
    }

    public void startAnimation( int duration , int pointNumber , boolean startZero )
    {
        mPointNumber = pointNumber;
        mDuration = duration;
        if( pointNumber == 0 ){
            if( mValue < 10 ) {
                mDuration = (int) (mValue * 100);
            }
        }

        startAnimation(startZero);
    }

    public void startAnimation( boolean startZero )
    {
        float startValue = 0;

        if( !startZero ){
            startValue = preValue;
        }

        float animatedValue = mValue;
        preValue = mValue;

        ValueAnimator animator = ValueAnimator.ofFloat( startValue , animatedValue);
        animator.addUpdateListener(this);
        animator.setDuration(mDuration);
        animator.start();
    }

    @Override
    public void onAnimationUpdate(ValueAnimator animation) {

        float value = (float) animation.getAnimatedValue();
        mValue = value;
        display();

        this.invalidate();
    }

    public void setCategoryIntroListener( View.OnClickListener clickListener , int categoryIndex ){
        llContent.setTag( categoryIndex );
        llContent.setOnClickListener( clickListener );
    }

}
