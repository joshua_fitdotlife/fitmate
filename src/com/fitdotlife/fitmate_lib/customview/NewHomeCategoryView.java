package com.fitdotlife.fitmate_lib.customview;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fitdotlife.fitmate.R;
import com.fitdotlife.fitmate.newhome.CategoryEditInfo;
import com.fitdotlife.fitmate.newhome.CategoryType;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

/**
 * Created by Joshua on 2015-12-15.
 */
@EViewGroup(R.layout.child_newhome_category)
public class NewHomeCategoryView extends LinearLayout{

    private Context mContext;
    private CategoryType categoryType = null;

    @ViewById(R.id.tv_child_newhome_category_value)
    TextView tvValue;

    @ViewById(R.id.tv_child_newhome_category_value_unit)
    TextView tvValueUnit;

    @ViewById(R.id.tv_child_newhome_category_name)
    TextView tvCategory;

    @ViewById(R.id.img_child_newhome_category)
    ImageView imgCategory;


    public NewHomeCategoryView(Context context) {
        super(context);
        mContext = context;
    }

    public void setValueText(String valueText){

        //닷이 있는지 확인한다.

        boolean existDot = valueText.contains(".");
        if( existDot ){

            if( valueText.length() >= 5 ){
                tvValue.setTextScaleX( 0.9f );
            }

        }else{
            if( valueText.length() >= 4 ){
                tvValue.setTextScaleX( 0.9f );
            }
        }

        tvValue.setText( valueText );
    }

    public void setValueUnitText(String valueUnitText){
        tvValueUnit.setText( valueUnitText );
    }

    public void setCategoryText( String categoryText ){
        tvCategory.setText(categoryText);
    }

    public void setValueTextColor( int color )
    {
        tvValue.setTextColor( color );
        tvValueUnit.setTextColor(color);
    }

    public void setCategoryTextColor(int color)
    {
        tvCategory.setTextColor(color);
    }

    public void setImage( Drawable imageDrawable ){
        imgCategory.setImageDrawable(imageDrawable);
    }

    public void setImageBackground( Drawable imageDrawable ){imgCategory.setBackground(imageDrawable);}

    public void setSelectListener( View.OnClickListener clickListener){
        imgCategory.setOnClickListener(clickListener);
    }

    public void setCategoryIntroListener( View.OnClickListener clickListener , int categoryIndex ){
        tvCategory.setOnClickListener( clickListener );
        tvCategory.setTag(categoryIndex);
    }



    public CategoryEditInfo getCategoryEditInfo(){

        return  new CategoryEditInfo( tvCategory.getText().toString(), tvValue.getText().toString() , tvValueUnit.getText().toString() );
    }

    public CategoryType getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(CategoryType categoryType) {
        this.categoryType = categoryType;
    }

    public void setViewIndex( int viewIndex ){
        imgCategory.setTag(viewIndex);
    }

    public void copyContents( NewHomeCategoryView categoryView ){
        categoryView.setCategoryText( tvCategory.getText().toString() );
        categoryView.setValueText( tvValue.getText().toString() );
        categoryView.setValueUnitText( tvValueUnit.getText().toString() );
        categoryView.setCategoryType( this.categoryType );
    }

}
