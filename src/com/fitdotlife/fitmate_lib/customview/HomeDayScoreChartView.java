package com.fitdotlife.fitmate_lib.customview;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import org.apache.log4j.Log;

import com.fitdotlife.fitmate.R;


public class HomeDayScoreChartView extends View implements AnimatorUpdateListener
{
    private final String TAG = "fimate";
    private Context mContext;
    private final String animationPropertyString = "AniAchievementValue";

    private int mGoalTextSize = 0;
    private Paint mGoalTextPaint = null;

    private float mLeftBlank = 0;
    private float mRightBlank = 0;
    private float mTopBlank = 0;
    private float mBootomBlank = 0;

    private Paint mArcPaint = null;
    private int mArcColor = 0;
    private Paint mArcBackgroundPaint = null;

    private Paint mComparisonTextPaint = null;
    private float mComparisonTextSize = 0;
    private float mComparisonTextBlank = 0;
    private float mStartComparisonTextBlank = 0;

    private float mRangeTextSize = 0;
    private float mRangeTextBlank = 0;
    private Paint mRangeTextPaint = null;
	private float mArcThickness = 0;

	private int heightSize = 0;
	private int widthSize = 0;

    private int mRangeFrom = 30;
    private int mRangeTo = 60;

    private float mAchievementValue = 0;
    private float mAchievementWeekValue = 0;

    private String mValueUnit = "";
    private String mTitleText = "";
    private String mStatusText = "";
    private String mRateText = "";

    private float mPreviousValue = 0;

    public HomeDayScoreChartView(Context context)
	{
		super(context);
		this.mContext = context;

		this.init();
	}

	public HomeDayScoreChartView(Context context, AttributeSet attrs)
	{
		super(context , attrs);
		this.mContext = context;

        TypedArray a = context.obtainStyledAttributes(attrs , R.styleable.HomeDayScoreChartView);
        int aArcColor = a.getColor(R.styleable.HomeDayScoreChartView_arcColor, 0);
        this.mArcColor = aArcColor;

		this.init();
	}
	
	
	private void init()
	{
        int size = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP , 11.33f , this.getResources().getDisplayMetrics());

        this.mArcThickness = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP , 24 , getResources().getDisplayMetrics());
        this.mComparisonTextSize = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP  , 11 , getResources().getDisplayMetrics());
        this.mComparisonTextBlank = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP , 3 , this.getResources().getDisplayMetrics());
        this.mStartComparisonTextBlank = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP , 5 , this.getResources().getDisplayMetrics());
        this.mGoalTextSize = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP , 8.56f , getResources().getDisplayMetrics());

        this.mRangeTextSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP , 11 , this.getResources().getDisplayMetrics() );
        this.mRangeTextBlank = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP , 8 , this.getResources().getDisplayMetrics());
        this.mRangeTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.mRangeTextPaint.setTextSize( this.mRangeTextSize );
        this.mRangeTextPaint.setColor( 0xFF689d26 );

        this.mGoalTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.mGoalTextPaint.setColor( this.mArcColor );
        this.mGoalTextPaint.setTextSize( this.mGoalTextSize );

        this.mComparisonTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.mComparisonTextPaint.setColor( 0xFFAFAFAF );
        this.mComparisonTextPaint.setTextSize( this.mComparisonTextSize );

        this.mArcPaint = new Paint( Paint.ANTI_ALIAS_FLAG );
        this.mArcPaint.setColor( this.mArcColor );
        this.mArcPaint.setTextSize(27);
        this.mArcPaint.setStyle( Paint.Style.STROKE );
        this.mArcPaint.setStrokeCap(Cap.ROUND);
        this.mArcPaint.setStrokeWidth( this.mArcThickness );

        this.mArcBackgroundPaint = new Paint( Paint.ANTI_ALIAS_FLAG );
        this.mArcBackgroundPaint.setColor( 0xFFE9E9E9 );
        this.mArcBackgroundPaint.setStyle(Paint.Style.STROKE);
        this.mArcBackgroundPaint.setStrokeCap( Cap.ROUND );
        this.mArcBackgroundPaint.setStrokeWidth(this.mArcThickness);

        this.mTopBlank = 0;
        this.mBootomBlank = (this.mArcThickness / 2);
        this.mRightBlank = (this.mArcThickness / 2);
        this.mLeftBlank  = (this.mArcThickness / 2);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec , int heightMeasureSpec)
	{
		int heightMode = MeasureSpec.getMode(heightMeasureSpec);
		switch( heightMode )
		{
		case MeasureSpec.UNSPECIFIED:
			heightSize = heightMeasureSpec;
			break;
		case MeasureSpec.AT_MOST:
			heightSize = 200;
			break;
		case MeasureSpec.EXACTLY:
			heightSize = MeasureSpec.getSize(heightMeasureSpec);
			break;
		}
		
		int widthMode = MeasureSpec.getMode(widthMeasureSpec);
		switch( widthMode )
		{
		case MeasureSpec.UNSPECIFIED:
			widthSize = widthMeasureSpec;
			break;
		case MeasureSpec.AT_MOST:
			widthSize = 300;
			break;
		case MeasureSpec.EXACTLY:
			widthSize = MeasureSpec.getSize(widthMeasureSpec);
			break;
		}
		
		this.setMeasuredDimension(widthSize, heightSize);
	}
	
	protected void onDraw(Canvas canvas)
	{
        canvas.drawColor( Color.WHITE );
        this.drawComparisonText( canvas );
        this.drawAchievementRingArc( canvas );
        this.drawAchievementRangeCircle(canvas);
        this.drawCenterText( canvas );
	}

    private void drawAchievementRangeCircle(Canvas canvas) {

        float center_x , height_y;

        center_x = this.getMeasuredWidth() / 2;
        height_y = this.getMeasuredHeight() - this.mBootomBlank;

        float circleRadius = this.getMeasuredHeight() - this.mBootomBlank - ( this.mArcThickness / 2 ) - this.mRangeTextBlank - this.mRangeTextSize;
        float rangeTextRadius = this.getMeasuredHeight() - this.mBootomBlank - this.mRangeTextSize;
        float innerCircleRadius = TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP , 16.5f , this.getResources().getDisplayMetrics() ) / 2;
        float outerCircleRadius = TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP , 26 , this.getResources().getDisplayMetrics() ) / 2;

        Paint innerCirclePaint = new Paint();

        Paint outerCirclePaint = new Paint();
        outerCirclePaint.setColor( Color.WHITE );

        //최소 범위를 그린다.
        int maxValue = mRangeFrom + mRangeTo;

        int strengthFromDegree = (( 180 * mRangeFrom) / maxValue ) + 180;
        double strengthFromRadian = Math.toRadians( strengthFromDegree );
        float strengthFromX = (float) ( center_x + ( circleRadius * Math.cos( strengthFromRadian ) ));
        float strengthFromY = (float) ( height_y + ( circleRadius * Math.sin( strengthFromRadian ) ));

        if(  this.mAchievementWeekValue  < mRangeFrom ){
            innerCirclePaint.setColor( 0xFFE9E9E9 );
        }else{
            innerCirclePaint.setColor( this.mArcColor );
        }

        canvas.drawCircle(strengthFromX , strengthFromY  , outerCircleRadius , outerCirclePaint);
        canvas.drawCircle(strengthFromX , strengthFromY , innerCircleRadius , innerCirclePaint);

        float strengthFromRangeTextX = (float) (center_x + ( rangeTextRadius * Math.cos( strengthFromRadian ) ));
        float strengthFromRangeTextY = (float) (height_y + ( rangeTextRadius * Math.sin(strengthFromRadian) ));

        int strengthRangeTextWidth = this.getTextWidth( this.mRangeFrom + " " + this.mValueUnit , this.mRangeTextPaint );

        if( strengthFromDegree < 270 ){
            strengthFromRangeTextX -= strengthRangeTextWidth;
        }else{
            strengthFromRangeTextX -= (strengthRangeTextWidth / 2 );
        }

        canvas.drawText( mRangeFrom + " " + this.mValueUnit , strengthFromRangeTextX , strengthFromRangeTextY , this.mRangeTextPaint );


        if( mRangeFrom != mRangeTo) {
            //최대 범위를 그린다.
            int strengthToDegree = ((180 * mRangeTo) / maxValue) + 180;
            double strengthToRadian = Math.toRadians(strengthToDegree);
            float strengthToX = (float) (center_x + (circleRadius * Math.cos(strengthToRadian)));
            float strengthToY = (float) (height_y + (circleRadius * Math.sin(strengthToRadian)));
            if( this.mAchievementWeekValue < mRangeTo ){
                innerCirclePaint.setColor( 0xFFE9E9E9 );
            }else{
                innerCirclePaint.setColor( this.mArcColor );
            }

            canvas.drawCircle(strengthToX, strengthToY, outerCircleRadius, outerCirclePaint);
            canvas.drawCircle(strengthToX, strengthToY, innerCircleRadius, innerCirclePaint);

            float strengthToRangeTextX = (float) (center_x + ( rangeTextRadius * Math.cos( strengthToRadian ) ));
            float strengthToRangeTextY = (float) (height_y + ( rangeTextRadius * Math.sin( strengthToRadian ) ));

            canvas.drawText( mRangeTo + " " + this.mValueUnit , strengthToRangeTextX , strengthToRangeTextY , this.mRangeTextPaint );
        }
    }

    private void drawComparisonText(Canvas canvas) {

        float center_x = this.getMeasuredWidth() / 2;
        int maxValue = this.mRangeFrom + mRangeTo;

        int startNumberTextWidth = this.getTextWidth("0" , this.mComparisonTextPaint);
        float startNumberTextX = center_x - ( this.getMeasuredHeight() - this.mBootomBlank - this.mRangeTextSize - this.mRangeTextBlank ) - startNumberTextWidth - mStartComparisonTextBlank;
        float startNumberTextY = this.getMeasuredHeight() - this.mBootomBlank;
        canvas.drawText("0", startNumberTextX , startNumberTextY , mComparisonTextPaint);

        int endNumberTextWidth = this.getTextWidth( maxValue + "" , this.mComparisonTextPaint);
        float endNumberTextX = center_x + ( this.getMeasuredHeight() - this.mBootomBlank - this.mRangeTextSize - this.mRangeTextBlank ) + mComparisonTextBlank;
        float endNumberTextY = this.getMeasuredHeight() - this.mBootomBlank;
        canvas.drawText(maxValue + "", endNumberTextX , endNumberTextY , mComparisonTextPaint);

    }

    private void drawCenterText(Canvas canvas) {

        int center_x = this.getMeasuredWidth() /2;
        float defalutBlank = this.mRangeTextBlank + mRangeTextSize;

        Paint textPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

        float callorieTextSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP , 14 , getResources().getDisplayMetrics());
        textPaint.setTypeface(Typeface.create(Typeface.DEFAULT , Typeface.BOLD));
        textPaint.setTextSize( callorieTextSize );
        textPaint.setColor( 0xFF444444 );

        int calorieTextWidth = this.getTextWidth( this.mTitleText , textPaint );
        int calorieTextHeight = this.getTextHeight( this.mTitleText , textPaint );
        canvas.drawText(this.mTitleText, center_x - ( calorieTextWidth / 2 )  ,  defalutBlank + mArcThickness + TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP , 29 , getResources().getDisplayMetrics()) + callorieTextSize   , textPaint );

        float calorieValueTextSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP , 32 , getResources().getDisplayMetrics());
        textPaint.setTextSize( calorieValueTextSize );
        textPaint.setColor( this.mArcColor);
        textPaint.setTypeface(Typeface.create(Typeface.DEFAULT , Typeface.BOLD));
        int calorieValueTextWidth = this.getTextWidth( (int)mAchievementWeekValue + " " + this.mValueUnit  , textPaint );
        int calorieValueTextHeight = this.getTextHeight( (int)mAchievementWeekValue + " " + this.mValueUnit , textPaint );
//        if( mAchievementWeekValue < 1 ){
//            this.mAchievementWeekValue = (float) ((this.mAchievementWeekValue * 10d )/10d);
//        }else if( mAchievementWeekValue >= 1 ){
//            this.mAchievementWeekValue = (int) this.mAchievementValue;
//        }

        canvas.drawText( (int)mAchievementWeekValue + " " + this.mValueUnit , center_x - ( calorieValueTextWidth / 2 ) , defalutBlank + mArcThickness + TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP , 30 , getResources().getDisplayMetrics()) + callorieTextSize + calorieValueTextSize , textPaint) ;

//        textPaint.setStyle(Paint.Style.STROKE);
//        textPaint.setStrokeWidth(2);
//        textPaint.setColor( Color.BLACK );
//        canvas.drawLine( center_x - ( calorieValueTextWidth / 2 ) , defalutBlank + mArcThickness + TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP , 40 , getResources().getDisplayMetrics()) + callorieTextSize + calorieValueTextSize , center_x + ( calorieValueTextWidth / 2 ) , defalutBlank + mArcThickness + TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP , 40 , getResources().getDisplayMetrics()) + callorieTextSize + calorieValueTextSize , textPaint );
        BitmapDrawable lineDrawable = (BitmapDrawable) this.getResources().getDrawable(R.drawable.home_graph_line2);
        Bitmap lineBitmap = lineDrawable.getBitmap();
        canvas.drawBitmap(lineBitmap , center_x - ( lineBitmap.getWidth() / 2 ) , defalutBlank + mArcThickness + TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP , 40 , getResources().getDisplayMetrics()) + callorieTextSize + calorieValueTextSize , textPaint );

        float calorieResultTextSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP , 13 , getResources().getDisplayMetrics());
        textPaint.setTextSize( calorieResultTextSize );
        textPaint.setColor(0xFF676767);
        textPaint.setStyle(Paint.Style.FILL);
        textPaint.setTypeface(Typeface.create(Typeface.DEFAULT , Typeface.NORMAL));
        int calorieResultTextWidth = this.getTextWidth( this.mStatusText , textPaint );
        int calorieResultTextHeight = this.getTextHeight(this.mStatusText , textPaint );
        canvas.drawText(this.mStatusText , center_x - ( calorieResultTextWidth / 2 ) , defalutBlank + mArcThickness + TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP , 60 , getResources().getDisplayMetrics()) + calorieValueTextSize + callorieTextSize, textPaint  );

        textPaint.setTextSize( TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP , 13 , getResources().getDisplayMetrics()) );
        textPaint.setColor( 0xFF676767 );
        int calorieRateTextWidth = this.getTextWidth( this.mRateText , textPaint );
        int calorieRateTextHeight = this.getTextHeight( this.mRateText , textPaint );
        canvas.drawText(this.mRateText , center_x - ( calorieRateTextWidth / 2 ) , defalutBlank + mArcThickness + TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP , 63.3f , getResources().getDisplayMetrics()) + calorieValueTextSize + callorieTextSize + calorieResultTextSize, textPaint  );

    }

    private void drawAchievementRingArc(Canvas canvas) {
        float yThickness = this.getMeasuredHeight() - this.mTopBlank - this.mBootomBlank - ( this.mArcThickness / 2 ) - this.mRangeTextSize - this.mRangeTextBlank ;

        float weekAngle = (( 180 * mAchievementWeekValue ) / ( this.mRangeFrom + this.mRangeTo ) ) + 180 ;
        if( weekAngle > 360 ){
            weekAngle = 360;
        }

        drawRingArc( canvas , yThickness , 180 , 180 , this.mArcBackgroundPaint);
        drawRingArc( canvas , yThickness , 180 , weekAngle - 180 , this.mArcPaint);
    }

    private void drawRingArc( Canvas canvas, float thickness ,  float startDegree ,  float endDegree , Paint paint )
    {
        float x , y;

        x = this.getMeasuredWidth() / 2;
        y = this.getMeasuredHeight() - this.mBootomBlank;

        RectF oval = new RectF( x - thickness ,y - thickness , x + thickness , y + thickness );

        Path path = new Path();
        path.moveTo( x - thickness , y );
        path.arcTo(oval, startDegree, endDegree);

        canvas.drawPath(path, paint);
    }
	
	public void startAnimation( )
	{
        long duration = 3000;

        Log.d( TAG , "AchievementValue : " + mAchievementValue );
        Log.d( TAG , "PreviousValue : " + mPreviousValue );

        float valueGap = this.mAchievementValue - this.mPreviousValue;
        if( valueGap < 30 ){
            duration = (long) (valueGap * 100);
        }

        if( valueGap < 0 ){ duration = 0; }

        ValueAnimator animator = ValueAnimator.ofFloat( mPreviousValue , this.mAchievementValue);
        animator.addUpdateListener(this);
        animator.setDuration(duration);
        animator.start();

        this.mPreviousValue = this.mAchievementValue;
	}

//	private void setAniAchievementValue( int aniAchievementValue )
//	{
//		this.mAchievementWeekValue = aniAchievementValue;
//	}

    public void setAchievementValue( double achievementValue ){
        this.mAchievementValue = (float) achievementValue;
    }

    public void setRange( int rangeFrom , int rangeTo ){
        this.mRangeFrom = rangeFrom;
        this.mRangeTo = rangeTo;
    }

	private int getTextWidth(String text, Paint paint)
	{
		Rect bounds = new Rect();
		paint.getTextBounds(text, 0, text.length(), bounds);
		return bounds.width();
	}
	
	private int getTextHeight(String text, Paint paint)
	{
		Rect bounds = new Rect();
		paint.getTextBounds(text, 0, text.length(), bounds);
		return bounds.height();
	}

	@Override
	public void onAnimationUpdate(ValueAnimator animation)
	{
        mAchievementWeekValue = (float) animation.getAnimatedValue();
        this.invalidate();
	}

    public void setValueUnit(String valueUnit ) {
        this.mValueUnit = valueUnit;
    }

    public void setTitleText(String titleText) {
        this.mTitleText = titleText;
    }

    public void setStatusText(String statusText) {
        this.mStatusText = statusText;
    }

    public void setRateText(String rateText) {
        this.mRateText = rateText;
    }
}
