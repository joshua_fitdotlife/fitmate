package com.fitdotlife.fitmate_lib.customview;

import android.content.Context;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fitdotlife.fitmate.FriendActivity;
import com.fitdotlife.fitmate.R;
import com.fitdotlife.fitmate.RefreshListener;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.http.FriendService;
import com.fitdotlife.fitmate_lib.http.NetworkClass;
import com.fitdotlife.fitmate_lib.key.FriendStatusType;
import com.fitdotlife.fitmate_lib.object.UserFriend;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import org.androidannotations.annotations.AfterTextChange;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringRes;
import org.androidannotations.rest.spring.annotations.RestService;

import java.util.List;

/**
 * Created by Joshua on 2015-10-15.
 */
@EViewGroup(R.layout.child_activity_friend_requestfriend_listitem_result)
public class FriendNoRequestView extends LinearLayout{
    private Context mContext;

    @ViewById(R.id.tv_child_activity_friend_requestfriend_listitem_result_content)
    TextView tvContent;

    @ViewById(R.id.child_activity_friend_requestfriend_result_profile)
    ImageView imgProfile;

    @ViewById(R.id.img_child_activity_friend_requestfriend_listitem_result_confirm)
    ImageView imgConfirm;

    public FriendNoRequestView(Context context) {
        super(context);
        mContext = context;
    }

    public void init( String txtDisplay ){
        imgConfirm.setVisibility(View.GONE);
        tvContent.setText(txtDisplay);

        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.profile_img1)
                .showImageOnFail(R.drawable.profile_img1)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .displayer(new RoundedBitmapDisplayer(200))
                .build();

        ImageLoader.getInstance().displayImage( "drawable://" + R.drawable.friend_thumnail_notice , imgProfile, options);
    }
}
