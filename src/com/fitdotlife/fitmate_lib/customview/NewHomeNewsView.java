package com.fitdotlife.fitmate_lib.customview;

import android.content.Context;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fitdotlife.fitmate.R;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.http.NetworkClass;
import com.fitdotlife.fitmate_lib.key.NewsType;
import com.fitdotlife.fitmate_lib.object.News;
import com.fitdotlife.fitmate_lib.object.NewsDayAchieve;
import com.fitdotlife.fitmate_lib.object.NewsFriend;
import com.fitdotlife.fitmate_lib.object.NewsProgramChange;
import com.fitdotlife.fitmate_lib.object.NewsUser;
import com.fitdotlife.fitmate_lib.object.NewsWeekAchieve;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.util.DateUtils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringRes;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Joshua on 2015-12-18.
 */
@EViewGroup(R.layout.child_newhome_news)
public class NewHomeNewsView extends RelativeLayout{

    private Context mContext;

    @ViewById(R.id.tv_child_newhome_news_content)
    TextView tvContent;

    @ViewById(R.id.tv_child_newhome_news_date)
    TextView tvDate;

    @ViewById(R.id.img_child_newhome_news_profile)
    ImageView imgProfile;

    @StringRes(R.string.alarm_program_change)
    String strProgramChange;
    @StringRes(R.string.alarm_friend_program_change)
    String strFriendProgramChange;


    @StringRes(R.string.alarm_today_achieve_success)
    String strTodayAchieveSuccess;
    @StringRes(R.string.alarm_pastday_achieve_success)
    String strPastdayAchieveSuccess;
    @StringRes(R.string.alarm_friend_today_achieve_success)
    String strFriendTodayAchieveSuccess;
    @StringRes(R.string.alarm_friend_pastday_achieve_success)
    String strFriendPastdayAchieveSuccess;

    @StringRes(R.string.alarm_today_achieve_fail)
    String strTodayAchieveFail;
    @StringRes(R.string.alarm_friend_today_achieve_fail)
    String strFriendTodayAchieveFail;

    @StringRes(R.string.alarm_week_achieve_success)
    String strWeekAchieveSuccess;
    @StringRes(R.string.alarm_pastweek_achieve_success)
    String strPastWeekAchieveSuccess;
    @StringRes(R.string.alarm_friend_week_achieve_success)
    String strFriendWeekAchieveSuccess;
    @StringRes(R.string.alarm_friend_pastweek_achieve_success)
    String strFriendPastWeekAchieveSuccess;


    @StringRes(R.string.alarm_week_achieve_fail)
    String strWeekAchieveFail;
    @StringRes(R.string.alarm_friend_week_achieve_fail)
    String strFriendWeekAchieveFail;

    @StringRes(R.string.alarm_accept_friend)
    String strAcceptFriend;

    @StringRes(R.string.news_init_message)
    String strInitMessage;


    public NewHomeNewsView(Context context) {
        super(context);
        this.mContext = context;
    }

    public void setNews( News news ){
        if( news.getAddedContents() != null ) {
            tvContent.setText( this.getContentText(news) );
        }

        Date dateLocalTime = DateUtils.getLocalTime(news.getProducedDate());

        Calendar calendar = Calendar.getInstance();
        long currentTimeMilliSeconds = calendar.getTimeInMillis();
        calendar.setTime(dateLocalTime);
        long procucedTimeMilliSeconds = calendar.getTimeInMillis();

        String timeGapString = null;
        if (DateUtils.isToday(dateLocalTime) ){
            timeGapString = this.getTimeGapString( currentTimeMilliSeconds - procucedTimeMilliSeconds );
        }else{
            SimpleDateFormat dateFormat = new SimpleDateFormat(mContext.getString(R.string.news_pastday_dateformat));
            timeGapString = dateFormat.format(dateLocalTime);
        }

        tvDate.setText(timeGapString);
    }

    private String getContentText( News news ){
        String result = "";
        NewsType newsType = NewsType.getNewsType(news.getType());
        UserInfo userInfo = new FitmateDBManager(mContext).getUserInfo();
        boolean isFriendNews = true;

        if( userInfo.getEmail().equals(news.getOwnerEmail()) ){
            isFriendNews = false;
        }

        switch(newsType){
            case INIT:
                imgProfile.setImageResource( R.drawable.friend_thumnail_notice );
                result = String.format( strInitMessage , userInfo.getName() );
                break;
            case ACHIEVE_DAY_SUCCESS:
                if( news.getOwnerProfileImagePath()!= null ) {
                    this.setProfileImage(NetworkClass.imageBaseURL + news.getOwnerProfileImagePath());
                }else{
                    imgProfile.setImageResource( R.drawable.profile_img1 );
                }

                NewsDayAchieve newsDayAchieveSuccess = (NewsDayAchieve) news.getAddedContents();
                boolean isToday = this.isToday( newsDayAchieveSuccess.getDate() );

                if(isFriendNews){
                    if(isToday)
                    {
                        result = String.format(strFriendTodayAchieveSuccess , news.getOwnerName() );
                    }else{
                        result = String.format(strFriendPastdayAchieveSuccess , news.getOwnerName() , DateUtils.getDateStringForPattern( newsDayAchieveSuccess.getDate() , mContext.getString(R.string.day_date_format))  );
                    }
                }else{
                    if(isToday){
                        result = strTodayAchieveSuccess;
                    }else{
                        result = String.format(strPastdayAchieveSuccess , DateUtils.getDateStringForPattern( newsDayAchieveSuccess.getDate() , mContext.getString(R.string.day_date_format) ) );
                    }
                }
                break;
            case ACHIEVE_DAY_FAIL:
                if( news.getOwnerProfileImagePath()!= null ) {
                    this.setProfileImage(NetworkClass.imageBaseURL + news.getOwnerProfileImagePath());
                }else{
                    imgProfile.setImageResource(R.drawable.profile_img1);
                }
                NewsDayAchieve newsDayAchieveFail = (NewsDayAchieve) news.getAddedContents();
                if(isFriendNews) {
                    result = String.format(strFriendTodayAchieveFail, news.getOwnerName() ,DateUtils.getDateStringForPattern(newsDayAchieveFail.getDate(), mContext.getString(R.string.day_date_format)));
                }else{
                    result = String.format(strTodayAchieveFail, DateUtils.getDateStringForPattern(newsDayAchieveFail.getDate(), mContext.getString(R.string.day_date_format)));
                }
                break;
            case ACHIEVE_WEEK_SUCCESS:
                if( news.getOwnerProfileImagePath()!= null ) {
                    this.setProfileImage(NetworkClass.imageBaseURL + news.getOwnerProfileImagePath());
                }else{
                    imgProfile.setImageResource(R.drawable.profile_img1);
                }
                NewsWeekAchieve newsWeekAchieveSuccess = (NewsWeekAchieve) news.getAddedContents();
                boolean isThisWeek = this.isThisWeek(newsWeekAchieveSuccess.getDate());
                if(isFriendNews){
                    if(isThisWeek){
                        result = String.format(strFriendWeekAchieveSuccess , news.getOwnerName() );
                    }else{
                        result = String.format(strFriendPastWeekAchieveSuccess , news.getOwnerName() , DateUtils.toWeekString(newsWeekAchieveSuccess.getDate() , getResources().getConfiguration().locale  ) );
                    }
                }else{
                    if( isThisWeek ){
                        result = strWeekAchieveSuccess;
                    }else {
                        result = String.format(strPastWeekAchieveSuccess , DateUtils.toWeekString(newsWeekAchieveSuccess.getDate() , getResources().getConfiguration().locale  )  );
                    }
                }

                break;
            case ACHIEVE_WEEK_FAIL:
                if( news.getOwnerProfileImagePath() != null ) {
                    this.setProfileImage(NetworkClass.imageBaseURL + news.getOwnerProfileImagePath());
                }else{
                    imgProfile.setImageResource(R.drawable.profile_img1);
                }
                NewsWeekAchieve newsWeekAchieveFail = (NewsWeekAchieve) news.getAddedContents();
                if(isFriendNews){
                    result = String.format(strFriendWeekAchieveFail, news.getOwnerName() ,DateUtils.toWeekString(newsWeekAchieveFail.getDate(), getResources().getConfiguration().locale), newsWeekAchieveFail.getWeekScore());
                }else {
                    result = String.format(strWeekAchieveFail, DateUtils.toWeekString(newsWeekAchieveFail.getDate(), getResources().getConfiguration().locale), newsWeekAchieveFail.getWeekScore());
                }

                break;
            case COMMENT_NEW:
                break;
            case CHANGE_EXERCISEPROGRAM:
                if( news.getOwnerProfileImagePath() != null ) {
                    this.setProfileImage(NetworkClass.imageBaseURL + news.getOwnerProfileImagePath());
                }else{
                    imgProfile.setImageResource(R.drawable.profile_img1);
                }
                NewsProgramChange newsProgramChange = (NewsProgramChange) news.getAddedContents();
                int resId =  getResources().getIdentifier( newsProgramChange.getChangedExerciseProgramName() , "string", mContext.getPackageName());
                String programName = mContext.getString(resId);
                if(isFriendNews){
                    result = String.format(strFriendProgramChange , newsProgramChange.getUserName()  , programName );
                }else{
                    result = String.format(strProgramChange , programName );
                }

                break;
            case FRIEND_REQUEST_FRIENDSHIP:
                break;
            case FRIEND_REJECT_FRIENDSHIP:
                break;
            case FRIEND_ACCEPT_FRIENDSHIP:
                NewsFriend newsFriend = (NewsFriend) news.getAddedContents();
                NewsUser requestUser =  newsFriend.getRequesterInfo();
                NewsUser responseUser = newsFriend.getResponderInfo();

                if( requestUser.getEmail().equals( userInfo.getEmail() ) ){
                    result = String.format( strAcceptFriend , responseUser.getName() );
                    if( news.getOwnerProfileImagePath() != null ) {
                        this.setProfileImage(NetworkClass.imageBaseURL + responseUser.getProfileImagePath());
                    }else{
                        imgProfile.setImageResource(R.drawable.profile_img1);
                    }
                }else{
                    result = String.format( strAcceptFriend , requestUser.getName() );
                    if( news.getOwnerProfileImagePath() != null ) {
                        this.setProfileImage(NetworkClass.imageBaseURL + requestUser.getProfileImagePath());
                    }else{
                        imgProfile.setImageResource(R.drawable.profile_img1);
                    }
                }

                break;
            case FRIEND_DISMISS_FRIENDSHIP:
                break;
        }


        return result;
    }

    public void setProfileImage( String profileImagePath ){

        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.profile_img1)
                .showImageOnFail(R.drawable.profile_img1)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .displayer(new RoundedBitmapDisplayer(200))
                .build();

        ImageLoader.getInstance().displayImage(profileImagePath, imgProfile, options);
    }

    public void setContent( String content){
        tvContent.setText( content );
    }

    public void setDate( String date ){
        tvDate.setText( date );
    }


    private boolean isThisWeek(String date) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date activityDate = null;

        try {
            activityDate  = dateFormat.parse( date );
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return DateUtils.isThisWeek(activityDate);

    }

    private boolean isToday( String date ){

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date activityDate = null;

        try {
            activityDate  = dateFormat.parse( date );
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return DateUtils.isToday(activityDate);
    }

    private String getTimeGapString( long timegap ){
        String result = null;

        long hourTick = 60 * 60 * 1000;
        long minTick = 60 * 1000;

        if( timegap > hourTick ){ //1시간 이상 차이가 날 때

            int hourGap = (int) (timegap / hourTick);
            result = String.format(mContext.getString(R.string.news_today_before_hour) , hourGap );

        }else if( timegap > minTick ){ // 1분 이상 차이가 날 때
            int minGap = (int)(timegap / minTick );
            result = String.format( mContext.getString(R.string.news_today_before_minute) , minGap );
        }else{ // 1분 미만으로 차이가 날때
            result = mContext.getString(R.string.news_today_justnow);
        }

        return result;
    }


}
