package com.fitdotlife.fitmate_lib.customview;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fitdotlife.fitmate.R;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

/**
 * Created by Joshua on 2015-08-03.
 */
@EViewGroup(R.layout.child_activity_friend_listgroup_title)
public class FriendListTitleView  extends LinearLayout{

    @ViewById(R.id.tv_child_activity_friend_listgroup_title)
    TextView tvTitle;

    public FriendListTitleView(Context context) {
        super(context);
    }

    public void setTitle( String title ){
        tvTitle.setText(title);
    }

}
