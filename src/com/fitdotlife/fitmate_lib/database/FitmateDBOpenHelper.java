package com.fitdotlife.fitmate_lib.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class FitmateDBOpenHelper extends SQLiteOpenHelper {

    private static final String DB_NAME 			                         = "Fitmate.db";
    public static final String DB_TABLE_PROGRAM 		                     = "TExerciseProgram";
    public static final String DB_TABLE_USER_PROGRAM 		                  = "TUserExerciseProgram";
    public static final String DB_TABLE_DAY_ACTIVITY 		                  = "TDayActivity";
    public static final String DB_TABLE_WEEK_ACTIVITY 		  	          = "TWeekActivity";
    public static final String DB_TABLE_FRIEND_DAY_ACTIVITY 		          = "TFriendDayActivity";
    public static final String DB_TABLE_FRIEND_WEEK_ACTIVITY 		  	  = "TFriendWeekActivity";
    public static final String DB_TABLE_USERINFO                            = "TUserInfo";
    public static final String DB_TABLE_DEVICE_RECENT_CONNECTIONINFO    ="DeviceRecentConnectionInfo";
    public static final String DB_TABLE_USERNOTI_SETTING                    ="TUserNotiSetting";


    private static final int DB_VERSION 				= 7;

    private Context mContext = null;
    private static FitmateDBOpenHelper instance = null;

    public static synchronized FitmateDBOpenHelper getHelper( Context context ){
        if( instance == null ){
            instance = new FitmateDBOpenHelper( context );
        }

        return instance;
    }

    private FitmateDBOpenHelper(Context context)
    {
        super(context, DB_NAME , null , DB_VERSION );
        this.mContext = context;
    }

    @Override
    public void onCreate( SQLiteDatabase db )
    {
        db.execSQL( "CREATE TABLE " + DB_TABLE_PROGRAM+ "(" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "name TEXT," +
                "category INTEGER," +
                "strengthfrom FLOAT," +
                "strengthto FLOAT," +
                "timesfromperweek INTEGER," +
                "timestoperweek INTEGER," +
                "minutefrom INTEGER," +
                "minuteto INTEGER," +
                "checkcontinuousexercise INTEGER," +
                "info TEXT," +
                "recommendnumber INTEGER );");



        db.execSQL( "CREATE TABLE " + DB_TABLE_DAY_ACTIVITY + "(" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "userid INTEGER," +
                "activitydate TEXT," +
                "averagemet FLOAT," +
                "caloriebyactivity INTEGER," +
                "fat REAL," +
                "carbohydrate REAL," +
                "distance REAL," +
                "strength_high INTEGER," +
                "strength_medium INTEGER," +
                "strength_low INTEGER," +
                "strength_underlow INTEGER," +
                "achieveoftarget INTEGER," +
                "metarray BLOB," +
                "exerciseprogramid INTEGER," +
                "datasavinginterval INTEGER," +
                "todayscore INTEGER," +
                "predayscore INTEGER," +
                "extrascore INTEGER," +
                "achievemax INTEGER ," +
                "possiblemaxscore INTEGER," +
                "possibletimes INTEGER ," +
                "possiblevalue INTEGER ," +
                "uploaded INTEGER ," +
                "starttime TEXT );");

        db.execSQL("CREATE TABLE " + DB_TABLE_WEEK_ACTIVITY + "(" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "weekstartdate TEXT," +
                "userid INTEGER," +
                "score INTEGER," +
                "avecalorie INTEGER," +
                "avemet FLOAT," +
                "avestrength_high INTEGER," +
                "avestrength_medium INTEGER," +
                "avestrength_low INTEGER," +
                "avestrength_underlow INTEGER," +
                "uploaded INTEGER ," +
                "exerciseprogramid INTEGER );");


        db.execSQL("CREATE TABLE " + DB_TABLE_FRIEND_WEEK_ACTIVITY + "(" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "weekstartdate TEXT," +
                "kindofdata INTEGER," +
                "listofdayachieve TEXT," +
                "listofday TEXT," +
                "rangefrom INTEGER," +
                "rangeto INTEGER," +
                "rangeunittype INTEGER);");

        db.execSQL("CREATE TABLE " + DB_TABLE_USERINFO + "(" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "email NVARCHART(150) NOT NULL , " +
                "password NVARCHAR(50) NULL , " +
                "name NVARCHAR(50) NOT NULL , " +
                "birthdate INTEGER NULL , " +
                "weight REAL DEFAULT 0 ," +
                "height REAL DEFAULT 0 ," +
                "gender INTEGER DEFAULT 2 ," +
                "issi INTEGER NULL," +
                "maxmet INTEGER DEFAULT 0, " +
                "isvo2max INTEGER DEFAULT 0, " +
                "intro NVARCHAR(150) NULL , " +
                "continuedexercisepolicy INTEGER DEFAULT 0, " +
                "continuedexerciseminute INTEGER DEFAULT 0, " +
                "isincentiveuser INTEGER ," +
                "profileimg NVARCHAR(100) ," +
                "wearat INTEGER DEFAULT -1," +
                "lastdatadate INTEGER , " +
                "deviceaddress VARCHAR(20) );");

        //김순동 수정, DB 테이블 추가
        db.execSQL("CREATE TABLE " + DB_TABLE_DEVICE_RECENT_CONNECTIONINFO + "(" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "email TEXT," +
                "deviceaddress VARCHAR(20)," +
                "connectedtime INTEGER," +
                "batteryratio INTEGER);");


        this.createUserNotiSetting( db );
        this.createUserProgram(db);
        this.insertDefaultProgram( db );
    }

    private void createUserNotiSetting( SQLiteDatabase db ){
        db.execSQL("CREATE TABLE " + DB_TABLE_USERNOTI_SETTING + "(" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "mytodayactivitynews INTEGER," +
                "myweekactivitynews INTEGER," +
                "batteryusagenoti INTEGER," +
                "fitmeternotusednoti INTEGER," +
                "fitmeterwearnoti INTEGER," +
                "alarmtime NVARCHAR(50)," +
                "friendnews INTEGER," +
                "friendtodayactivitynews INTEGER," +
                "friendweekactivitynews INTEGER);");

        db.execSQL("INSERT INTO " + DB_TABLE_USERNOTI_SETTING +
                "(mytodayactivitynews , myweekactivitynews , batteryusagenoti , fitmeternotusednoti , fitmeterwearnoti , alarmtime  ,  friendnews , friendtodayactivitynews , friendweekactivitynews) " +
                "VALUES(1 , 1 , 1 , 1 , 1,'08:00' , 1 , 1 , 1);");
    }


    private void  insertDefaultProgram( SQLiteDatabase db ){

        db.execSQL("INSERT INTO " + DB_TABLE_USER_PROGRAM +
                "(id , name,category,strengthfrom,strengthto,timesfromperweek,timestoperweek,minutefrom,minuteto,checkcontinuousexercise,info,recommendnumber,isapplied , defaultprogram , diseasecategory) " +
                "VALUES(0 ,'exercise_promoting_calorie', 7 , 30 , 30 , 7 , 7 , 0 , 0 , 1 , 'exercise_promoting_calorie_introduction' , 0 , 1 , 1 ,  0 );");

        db.execSQL("INSERT INTO " + DB_TABLE_USER_PROGRAM +
                "(id , name,category,strengthfrom,strengthto,timesfromperweek,timestoperweek,minutefrom,minuteto,checkcontinuousexercise,info,recommendnumber,isapplied , defaultprogram , diseasecategory) " +
                "VALUES(1 ,'exercise_promoting_medium_intensity',3,2,3,5,7,30,60,1,'exercise_promoting_medium_intensity_introduction',0 , 0 , 1 ,  0);");

        db.execSQL("INSERT INTO " + DB_TABLE_USER_PROGRAM +
                "(id , name,category,strengthfrom,strengthto,timesfromperweek,timestoperweek,minutefrom,minuteto,checkcontinuousexercise,info,recommendnumber,isapplied , defaultprogram , diseasecategory) " +
                "VALUES(2 ,'exercise_promoting_high_intensity',3,3,3,5,7,30,60,1,'exercise_promoting_high_intensity_introduction',0 , 0 , 1 ,  0);");

        db.execSQL("INSERT INTO " + DB_TABLE_USER_PROGRAM +
                "(id , name,category,strengthfrom,strengthto,timesfromperweek,timestoperweek,minutefrom,minuteto,checkcontinuousexercise,info,recommendnumber,isapplied , defaultprogram , diseasecategory) " +
                "VALUES(3 ,'exercise_acsm_diabetes', 0 , 50 , 80 , 4 , 7 , 20 , 60 , 1 , 'exercise_acsm_diabetes_introduction' , 0 , 0 , 1 ,  1 );");

        db.execSQL("INSERT INTO " + DB_TABLE_USER_PROGRAM +
                "(id , name,category,strengthfrom,strengthto,timesfromperweek,timestoperweek,minutefrom,minuteto,checkcontinuousexercise,info,recommendnumber,isapplied , defaultprogram , diseasecategory) " +
                "VALUES(4 ,'exercise_acsm_obesity', 0 , 40 , 60 , 5 , 7 , 30 , 60 , 1 , 'exercise_acsm_obesity_introduction' , 0 , 0 , 1 ,  1 );");

        db.execSQL("INSERT INTO " + DB_TABLE_USER_PROGRAM +
                "(id , name,category,strengthfrom,strengthto,timesfromperweek,timestoperweek,minutefrom,minuteto,checkcontinuousexercise,info,recommendnumber,isapplied , defaultprogram , diseasecategory) " +
                "VALUES(5 ,'exercise_acsm_endstagemetabolic', 0 , 40 , 80 , 4 , 7 , 20 , 60 , 1 , 'exercise_acsm_endstagemetabolic_introduction' , 0 , 0 , 1 ,  1 );");

        db.execSQL("INSERT INTO " + DB_TABLE_USER_PROGRAM +
                "(id , name,category,strengthfrom,strengthto,timesfromperweek,timestoperweek,minutefrom,minuteto,checkcontinuousexercise,info,recommendnumber,isapplied , defaultprogram , diseasecategory) " +
                "VALUES(6 ,'exercise_acsm_hyperlipidemia', 1 , 40 , 80 , 6 , 7 , 20 , 60 , 1 , 'exercise_acsm_hyperlipidemia_introduction' , 0 , 0 , 1 ,  1 );");

        db.execSQL("INSERT INTO " + DB_TABLE_USER_PROGRAM +
                "(id , name,category,strengthfrom,strengthto,timesfromperweek,timestoperweek,minutefrom,minuteto,checkcontinuousexercise,info,recommendnumber,isapplied , defaultprogram , diseasecategory) " +
                "VALUES(7 ,'exercise_acsm_myocardial_infarction', 0 , 40 , 80 , 3 , 7 , 20 , 60 , 1 , 'exercise_acsm_myocardial_infarction_introduction' , 0 , 0 , 1 ,  2 );");

        db.execSQL("INSERT INTO " + DB_TABLE_USER_PROGRAM +
                "(id , name,category,strengthfrom,strengthto,timesfromperweek,timestoperweek,minutefrom,minuteto,checkcontinuousexercise,info,recommendnumber,isapplied , defaultprogram , diseasecategory) " +
                "VALUES(8 ,'exercise_acsm_revascularization', 0 , 40 , 80 , 4 , 7 , 20 , 60 , 1 , 'exercise_acsm_revascularization_introduction' , 0 , 0 , 1 ,  2);");

        db.execSQL("INSERT INTO " + DB_TABLE_USER_PROGRAM +
                "(id , name,category,strengthfrom,strengthto,timesfromperweek,timestoperweek,minutefrom,minuteto,checkcontinuousexercise,info,recommendnumber,isapplied , defaultprogram , diseasecategory) " +
                "VALUES(9 ,'exercise_acsm_atrial_fibrillation', 0 , 50 , 80 , 3 , 7 , 30 , 45 , 1 , 'exercise_acsm_atrial_fibrillation_introduction' , 0 , 0 , 1 ,  2);");

        db.execSQL("INSERT INTO " + DB_TABLE_USER_PROGRAM +
                "(id , name,category,strengthfrom,strengthto,timesfromperweek,timestoperweek,minutefrom,minuteto,checkcontinuousexercise,info,recommendnumber,isapplied , defaultprogram , diseasecategory) " +
                "VALUES(10 ,'exercise_acsm_chronic_heart_failure', 0 , 40 , 70 , 4 , 7 , 20 , 60 , 1 , 'exercise_acsm_chronic_heart_failure_introduction' , 0 , 0 , 1 ,  2);");

        db.execSQL("INSERT INTO " + DB_TABLE_USER_PROGRAM +
                "(id , name,category,strengthfrom,strengthto,timesfromperweek,timestoperweek,minutefrom,minuteto,checkcontinuousexercise,info,recommendnumber,isapplied , defaultprogram , diseasecategory) " +
                "VALUES(11 ,'exercise_acsm_heart_transplant', 0 , 40 , 80 , 3 , 7 , 30 , 45 , 1 , 'exercise_acsm_heart_transplant_introduction' , 0 , 0 , 1 ,  2 );");

        db.execSQL("INSERT INTO " + DB_TABLE_USER_PROGRAM +
                "(id , name,category,strengthfrom,strengthto,timesfromperweek,timestoperweek,minutefrom,minuteto,checkcontinuousexercise,info,recommendnumber,isapplied ,defaultprogram ,  diseasecategory) " +
                "VALUES(12 ,'exercise_acsm_high_blood_pressure', 1 , 40 , 80 , 4 , 7 , 30 , 60 , 1 , 'exercise_acsm_high_blood_pressure_introduction' , 0 , 0 , 1 ,  2 );");

        db.execSQL("INSERT INTO " + DB_TABLE_USER_PROGRAM +
                "(id , name,category,strengthfrom,strengthto,timesfromperweek,timestoperweek,minutefrom,minuteto,checkcontinuousexercise,info,recommendnumber,isapplied , defaultprogram , diseasecategory) " +
                "VALUES(13 ,'exercise_acsm_peripheral_artery', 0 , 40 , 60 , 4 , 7 , 15 , 60 , 1 , 'exercise_acsm_peripheral_artery_introduction' , 0 , 0 , 1 ,  2 );");

        db.execSQL("INSERT INTO " + DB_TABLE_USER_PROGRAM +
                "(id , name,category,strengthfrom,strengthto,timesfromperweek,timestoperweek,minutefrom,minuteto,checkcontinuousexercise,info,recommendnumber,isapplied , defaultprogram , diseasecategory) " +
                "VALUES(14 ,'exercise_acsm_chronic_restrictive_lung', 6 , 11 , 13 , 3 , 5 , 20 , 60 , 1 , 'exercise_acsm_chronic_restrictive_lung_introduction' , 0 , 0 , 1 ,  3 );");

        db.execSQL("INSERT INTO " + DB_TABLE_USER_PROGRAM +
                "(id , name,category,strengthfrom,strengthto,timesfromperweek,timestoperweek,minutefrom,minuteto,checkcontinuousexercise,info,recommendnumber,isapplied , defaultprogram , diseasecategory) " +
                "VALUES(15 ,'exercise_acsm_heart_lung_transplatation', 6 , 11 , 13 , 3 , 7 , 20 , 30 , 1 , 'exercise_acsm_heart_lung_transplatation_introduction' , 0 , 0 , 1 ,  3);");

        db.execSQL("INSERT INTO " + DB_TABLE_USER_PROGRAM +
                "(id , name,category,strengthfrom,strengthto,timesfromperweek,timestoperweek,minutefrom,minuteto,checkcontinuousexercise,info,recommendnumber,isapplied , defaultprogram , diseasecategory) " +
                "VALUES(16 ,'exercise_acsm_arthritis', 0 , 40 , 60 , 3 , 5 , 5 , 30 , 1 , 'exercise_acsm_arthritis_introduction' , 0 , 0 , 1 ,  4);");

        db.execSQL("INSERT INTO " + DB_TABLE_USER_PROGRAM +
                "(id , name,category,strengthfrom,strengthto,timesfromperweek,timestoperweek,minutefrom,minuteto,checkcontinuousexercise,info,recommendnumber,isapplied , defaultprogram , diseasecategory) " +
                "VALUES(17 ,'exercise_acsm_stroke', 0 , 40 , 70 , 3 , 5 , 20 , 60 , 1 , 'exercise_acsm_stroke_introduction' , 0 , 0 , 1 ,  5 );");

        db.execSQL("INSERT INTO " + DB_TABLE_USER_PROGRAM +
                "(id , name,category,strengthfrom,strengthto,timesfromperweek,timestoperweek,minutefrom,minuteto,checkcontinuousexercise,info,recommendnumber,isapplied , defaultprogram , diseasecategory) " +
                "VALUES(18 ,'exercise_acsm_polio', 0 , 40 , 70 , 3 , 3 , 20 , 30 , 1 , 'exercise_acsm_polio_introduction' , 0 , 0 , 1 ,  5);");

        db.execSQL("INSERT INTO " + DB_TABLE_USER_PROGRAM +
                "(id , name,category,strengthfrom,strengthto,timesfromperweek,timestoperweek,minutefrom,minuteto,checkcontinuousexercise,info,recommendnumber,isapplied , defaultprogram , diseasecategory) " +
                "VALUES(19 ,'exercise_acsm_cerebral_paralysis', 1 , 40 , 85 , 3 , 5 , 20 , 40 , 1 , 'exercise_acsm_cerebral_paralysis_introduction' , 0 , 0 , 1 ,  5);");

        db.execSQL("INSERT INTO " + DB_TABLE_USER_PROGRAM +
                "(id , name,category,strengthfrom,strengthto,timesfromperweek,timestoperweek,minutefrom,minuteto,checkcontinuousexercise,info,recommendnumber,isapplied , defaultprogram , diseasecategory) " +
                "VALUES(20 ,'exercise_acsm_cancer', 1 , 40 , 60 , 3 , 5 , 5 , 60 , 1 , 'exercise_acsm_cancer_introduction' , 0 , 0 ,1 ,  6 );");

        db.execSQL("INSERT INTO " + DB_TABLE_USER_PROGRAM +
                "(id , name,category,strengthfrom,strengthto,timesfromperweek,timestoperweek,minutefrom,minuteto,checkcontinuousexercise,info,recommendnumber,isapplied , defaultprogram , diseasecategory) " +
                "VALUES(21 ,'exercise_acsm_aids', 1 , 40 , 60 , 3 , 4 , 30 , 60 , 1 , 'exercise_acsm_aids_introduction' , 0 , 0 , 1 ,  6);");
    }

    private void createUserProgram( SQLiteDatabase db ){
        db.execSQL("CREATE TABLE " + DB_TABLE_USER_PROGRAM + "(" +
                "id INTEGER, " +
                "name TEXT," +
                "category INTEGER," +
                "strengthfrom FLOAT," +
                "strengthto FLOAT," +
                "timesfromperweek INTEGER," +
                "timestoperweek INTEGER," +
                "minutefrom INTEGER," +
                "minuteto INTEGER," +
                "checkcontinuousexercise INTEGER," +
                "info TEXT," +
                "recommendnumber INTEGER, " +
                "isapplied INTEGER," +
                "defaultprogram INTEGER , " +
                "diseasecategory INTEGER DEFAULT -1);");
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        //버전이 4보다 아래일 때
        if(oldVersion < 4)
        {
            if(oldVersion == 2) {

                db.execSQL("UPDATE " + DB_TABLE_WEEK_ACTIVITY + " SET exerciseprogramid=0");

            }
        }

        //버전이 5보다 작을 때
        if(oldVersion < 5 )
        {

            Cursor c = null;
            c = db.query( FitmateDBOpenHelper.DB_TABLE_USER_PROGRAM , new String[]{"*"}, "isapplied=1",null,null,null,null);
            c.moveToFirst();

            int appliedID = -1;
            if( c.getCount() > 0 ){
                appliedID = c.getInt(c.getColumnIndex("id"));

                if( oldVersion < 4 ) {

                    if (appliedID == 1) {
                        appliedID = 2;
                    } else if (appliedID == 2) {
                        appliedID = 1;
                    }
                }
            }

            //유저 프로그램에 defaultprogram 필드를 생성한다.
            db.execSQL("DROP TABLE " + DB_TABLE_USER_PROGRAM);
            this.createUserProgram(db);
            this.insertDefaultProgram(db);

            if( appliedID > -1 ) {
                db.execSQL("UPDATE " + DB_TABLE_USER_PROGRAM + " SET isapplied=1 where id=" + appliedID);
            }

            //알람 설정 테이블을 추가한다.
            this.createUserNotiSetting(db);

            //사용자 테이블에 profileimg를 추가한다.
            db.execSQL("ALTER TABLE " + DB_TABLE_USERINFO + " ADD COLUMN profileimg NVARCHAR(100)" );
        }

        //버전이 6보다 낮을 때.
        if( oldVersion < 6 ){

            db.execSQL( "ALTER TABLE " + DB_TABLE_DAY_ACTIVITY + " ADD COLUMN fat REAL DEFAULT -1" );
            db.execSQL( "ALTER TABLE " + DB_TABLE_DAY_ACTIVITY + " ADD COLUMN carbohydrate REAL DEFAULT -1" );
            db.execSQL( "ALTER TABLE " + DB_TABLE_DAY_ACTIVITY + " ADD COLUMN distance REAL DEFAULT -1" );


        }

        if( oldVersion < DB_VERSION ){

            db.execSQL("CREATE TABLE " + DB_TABLE_FRIEND_WEEK_ACTIVITY + "(" +
                    "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "weekstartdate TEXT," +
                    "kindofdata INTEGER," +
                    "listofdayachieve TEXT," +
                    "listofday TEXT," +
                    "rangefrom INTEGER," +
                    "rangeto INTEGER," +
                    "rangeunittype TEXT);");

        }

    }
}
