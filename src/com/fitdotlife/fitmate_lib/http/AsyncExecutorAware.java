package com.fitdotlife.fitmate_lib.http;

/**
 * Created by fitlife.soondong on 2015-03-09.
 */
public interface AsyncExecutorAware<T>{
    public void setAsyncExecutor(AsyncExecutor<T> asyncExecutor);
}

