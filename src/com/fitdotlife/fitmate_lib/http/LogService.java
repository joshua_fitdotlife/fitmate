package com.fitdotlife.fitmate_lib.http;

import com.fitdotlife.fitmate_lib.object.LogInfo;

import org.androidannotations.rest.spring.annotations.Body;
import org.androidannotations.rest.spring.annotations.Field;
import org.androidannotations.rest.spring.annotations.Post;
import org.androidannotations.rest.spring.annotations.Rest;
import org.androidannotations.rest.spring.api.RestClientErrorHandling;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

/**
 * Created by Joshua on 2015-09-17.
 */
@Rest(rootUrl = "http://fitlog.azurewebsites.net/api" ,converters = {MappingJackson2HttpMessageConverter.class})
public interface LogService extends RestClientErrorHandling
{
    @Post("/log")
    void sendLog( @Body LogInfo user );
}