package com.fitdotlife.fitmate_lib.http;

import com.fitdotlife.fitmate_lib.object.News;
import com.fitdotlife.fitmate_lib.object.UserInfo_Rest;
import com.fitdotlife.fitmate_lib.object.UserNotiSetting;

import org.androidannotations.rest.spring.annotations.Body;
import org.androidannotations.rest.spring.annotations.Field;
import org.androidannotations.rest.spring.annotations.Get;
import org.androidannotations.rest.spring.annotations.Path;
import org.androidannotations.rest.spring.annotations.Put;
import org.androidannotations.rest.spring.annotations.Rest;
import org.androidannotations.rest.spring.api.RestClientHeaders;
import org.androidannotations.rest.spring.api.RestClientErrorHandling;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import java.util.List;
import java.util.logging.LoggingMXBean;

/**
 * Created by Joshua on 2015-08-19.
 */

@Rest(converters = {MappingJackson2HttpMessageConverter.class})
public interface UserInfoService extends RestClientErrorHandling {
    @Put("/userinfo/UserNotiSetting/{email}/")
    Boolean cuUserNotiSetting( @Body UserNotiSetting news , @Path String email );

    @Get("/userinfo/UserNotiSetting/{email}/")
    UserNotiSetting getUserNotiSetting(@Path String email);

    @Get("/userinfo/UserID/{email}/")
    Integer getUserID( @Path String email );

    @Get("/userinfo/UserInfo/{email}/")
    UserInfo_Rest getUserInfo( @Path String email );

    void setRootUrl(String rootUrl);
}

