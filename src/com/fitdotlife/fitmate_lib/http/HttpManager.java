package com.fitdotlife.fitmate_lib.http;

import android.content.Context;

import org.apache.http.client.methods.HttpPost;

import java.util.WeakHashMap;

/**
 * Created by Joshua on 2015-02-23.
 */
public class HttpManager {

    private final String BASE_URL = NetworkClass.baseURL;

    private Context mContext = null;
    private HttpRequestCallback mCallback = null;

    public HttpManager(Context context, HttpRequestCallback callback)
    {
        this.mContext = context;
        this.mCallback = callback;
    }

    public void sendServer(  WeakHashMap<String, String> parameters , String fileName ,  int responseCode ){

        String url = BASE_URL + fileName;
        HttpRequest request = new HttpRequest( this.mContext , url , HttpPost.METHOD_NAME , parameters , responseCode , this.mCallback  );
        request.requestData();
    }

    public void sendServer( String jsonString , String fileName ,  int responseCode ){
        String url = BASE_URL + fileName;
        NetworkClassThread networkClassThread = new NetworkClassThread( this.mContext , url , jsonString , responseCode , this.mCallback );
        networkClassThread.start();
    }

    public void sendServer(String fileName, int responseCode , String method) {
        String url = BASE_URL + fileName;
        NetworkClassThread networkClassThread = new NetworkClassThread( this.mContext , url , responseCode , method ,  this.mCallback );
        networkClassThread.start();
    }
}
