package com.fitdotlife.fitmate_lib.http;

import org.androidannotations.rest.spring.annotations.Get;
import org.androidannotations.rest.spring.annotations.Path;
import org.androidannotations.rest.spring.annotations.Rest;
import org.androidannotations.rest.spring.api.RestClientHeaders;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

/**
 * Created by Joshua on 2015-11-10.
 */
@Rest( converters = {MappingJackson2HttpMessageConverter.class})
public interface VersionCheckService extends RestClientHeaders
{
    @Get("/versioncheck/NeedSoftwareUpdate/{email}/?version={version}")
    Boolean checkSoftwareUpdate( @Path String email ,@Path String version );

    void setRootUrl(String rootUrl);
}
