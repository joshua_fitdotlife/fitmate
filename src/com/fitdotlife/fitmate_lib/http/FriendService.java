package com.fitdotlife.fitmate_lib.http;

import com.fitdotlife.fitmate_lib.object.FriendAlarmSetting;
import com.fitdotlife.fitmate_lib.object.FriendList;
import com.fitdotlife.fitmate_lib.object.FriendRequestList;
import com.fitdotlife.fitmate_lib.object.UserFriend;
import com.fitdotlife.fitmate_lib.object.UserFriendsNewsSetting;
import com.fitdotlife.fitmate_lib.object.UserInfo;

import org.androidannotations.rest.spring.annotations.Body;
import org.androidannotations.rest.spring.annotations.Field;
import org.androidannotations.rest.spring.annotations.Get;
import org.androidannotations.rest.spring.annotations.Part;
import org.androidannotations.rest.spring.annotations.Path;
import org.androidannotations.rest.spring.annotations.Post;
import org.androidannotations.rest.spring.annotations.Put;
import org.androidannotations.rest.spring.annotations.Rest;
import org.androidannotations.rest.spring.api.RestClientErrorHandling;
import org.androidannotations.rest.spring.api.RestClientHeaders;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import java.util.List;

/**
 *
 * Created by Joshua on 2015-07-28.
 *
 */
@Rest( converters = {MappingJackson2HttpMessageConverter.class} )
public interface FriendService  extends RestClientErrorHandling {

    @Put("/friend/?email={email}&friendemail={friendemail}")
    Integer addFriend(@Path String email , @Path String friendemail);

    @Get("/friendrelation/FriendList/{email}/")
    FriendList getFriendList(  @Path String email );

    @Get("/friendrelation/FriendRequestList/{email}/")
    FriendRequestList getFriendRequestList( @Path String email );

    @Get("/friendrelation?email={email}&searchword={searchword}")
    List<UserFriend> findFriends(@Path String email , @Path String searchword);

    @Get("/friendrelation/RecentFriendCout/{email}/")
    Integer getRecentFriendCount(@Path String email);

    @Get("/friendrelation/RequestFriendCout/{email}/")
    Integer getRequestFriendCount(@Path String email);

    //@Put("/friendrelation?email={email}&userfriendsid={userfriendsid}&requeststatus={requeststatus}")
    @Put("/friendrelation/ChangeStatus/{email}/?userfriendsid={userfriendsid}&requeststatus={requeststatus}")
    Boolean changeStatus( @Path String email , @Path int userfriendsid , @Path int requeststatus );

    @Put("/friendrelation/Confirm/{email}/?userfriendsid={userfriendsid}&requesttype={requesttype}")
    Boolean confirm( @Path String email , @Path int userfriendsid , @Path int requesttype );

    @Put("/friendrelation/SetAlarm/{email}/?alarmlist={alarmlist}")
    void setAlarm( @Path String email , @Path String alarmlist );

    @Put("/friendrelation/UpdateCheckRecentList/{email}/?recentlist={recentlist}")
    void updateCheckRecentList( @Path String email , @Path String recentlist );

    @Put("/friendrelation/UpdateCheckRequestList/{email}/?requestlist={requestlist}")
    void updateCheckRequestList( @Path String email , @Path String requestlist );

    @Get("/friendrelation/UsersFriendsInfo/{email}/?friendEmail={friendEmail}")
    UserFriend getUsersFriendsInfo( @Path String email , @Path String friendEmail );

    @Get("/FriendNewsAlarmSetting/getMyFriendsNewsSetting/{email}/")
    List<UserFriendsNewsSetting> getFriendNewsSetting(@Path String email);

    @Post("/FriendNewsAlarmSetting/setFriendsSettings/{email}/")
    void setFriendNewsSetting( @Path String email , @Body List<FriendAlarmSetting> setting );

    void setRootUrl(String rootUrl);

}