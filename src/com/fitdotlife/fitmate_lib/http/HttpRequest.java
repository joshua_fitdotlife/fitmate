package com.fitdotlife.fitmate_lib.http;

import android.content.Context;
import android.os.Handler;

import java.util.WeakHashMap;

public class HttpRequest {
	
	public static final String POST = "POST";
	public static final String GET = "GET";
	
	public static final int SUCCESS = 0;
	public static final int FAIL = 1;
	
	protected static final int RECEIVE = 0;
	
	private Handler mRequestHandler;
	private Thread mClientThread;
	
	public HttpRequest(Context context, String requestUri, String method, WeakHashMap<String, String> parameters, int responseCode, HttpRequestCallback callback)
	{
		this.mRequestHandler = new ResponseHandler( context , responseCode , callback );
		this.mClientThread = new FitLifeHttpClientThread( requestUri , method , parameters , mRequestHandler ); 
	}
	
	public HttpRequest(Context context, String requestUri, String method, int responseCode, HttpRequestCallback callback)
	{
		this.mRequestHandler = new ResponseHandler( context , responseCode , callback );
		this.mClientThread = new FitLifeHttpClientThread( requestUri , method , mRequestHandler );
	}
	
	public void requestData()
	{
		this.mClientThread.start();
	}

}

