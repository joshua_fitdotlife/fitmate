package com.fitdotlife.fitmate_lib.http;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by Joshua on 2015-03-19.
 */
public class HttpTask extends AsyncTask<HttpUriRequest , Void , String>{

    private final String TAG = "fitmate";
    private final int TIMEOUT = 15000;

    @Override
    protected String doInBackground( HttpUriRequest... params ) {

        String data = null;
        try {
            HttpClient client = new DefaultHttpClient();

            HttpParams httpParams = client.getParams();
            HttpConnectionParams.setConnectionTimeout(httpParams, TIMEOUT);
            HttpConnectionParams.setSoTimeout(httpParams, TIMEOUT);

            HttpUriRequest uriRequest = params[0];
            HttpResponse response = client.execute(uriRequest);

            InputStream ips = response.getEntity().getContent();
            BufferedReader buf = new BufferedReader(new InputStreamReader( ips , "UTF-8" ));

            StringBuilder sb = new StringBuilder();

            int character;
            while( ( character = buf.read() ) != -1 )
            {
                sb.append((char)character);
                String.valueOf(character);
            }

            ips.close();
            data = sb.toString();

        } catch (ClientProtocolException e) {
            Log.d( TAG , "웹에서 데이터를 가져오는 중에 에러가 발생했습니다. - " + e.getMessage() );
            return null;
        } catch (IOException e) {
            Log.d( TAG , "웹에서 데이터를 가져오는 중에 에러가 발생했습니다. - " + e.getMessage() );
            return null;
        }

        return data;
    }

}
