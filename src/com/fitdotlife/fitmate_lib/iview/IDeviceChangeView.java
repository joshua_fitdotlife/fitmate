package com.fitdotlife.fitmate_lib.iview;

/**
 * Created by Joshua on 2015-05-18.
 */
public interface IDeviceChangeView {
    void finishActivity();
    void showResultDialog( final boolean isSuccess , final boolean deleteDeviceData );
    int getWearLocation();
}
