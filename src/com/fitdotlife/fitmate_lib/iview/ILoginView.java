package com.fitdotlife.fitmate_lib.iview;

public interface ILoginView {
	public void navigateToHome();
	public void showResult(String message);
	public void finishActivity();
    public void startProgressDialog();
    public void stopProgressDialog();
	public void showUpdateDialog();
}