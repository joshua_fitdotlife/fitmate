package com.fitdotlife.fitmate_lib.iview;

/**
 * Created by Joshua on 2015-02-10.
 */
public interface IExerciseProgramInfoView {
    public void showResult(String message);
}
