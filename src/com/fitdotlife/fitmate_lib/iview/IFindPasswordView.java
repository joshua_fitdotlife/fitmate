package com.fitdotlife.fitmate_lib.iview;

public interface IFindPasswordView 
{
	void navigateToSearchDialog( String name );
	void showResult(String message);
	void dismisDialog(  );

	void startProgressDialog();
	void stopProgressDialog();
}
