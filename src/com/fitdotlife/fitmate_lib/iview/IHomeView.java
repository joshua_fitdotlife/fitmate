package com.fitdotlife.fitmate_lib.iview;

import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.StrengthInputType;
import com.fitdotlife.fitmate_lib.object.ExerciseProgram;

import java.util.Calendar;

/**
 * Created by Joshua on 2015-03-23.
 */
public interface IHomeView {
    public void displayExerciseProgram(ExerciseProgram program);
    public void displayDayAchievement( int rangeFrom , int rangeTo , double value , StrengthInputType strengthType , int extraScore );
    public void displayWeekAchievement( int predayScore , int todayScore , boolean isAchieveMax , int possibleMaxScore , int possibleTimes , int possibleValue , StrengthInputType strengthType  );
    public void setCalDisValue( double calorie , double distance );

    public void displayFatBurn(float fatBurn, float carbonBurn);
    public void onSyncEnd();
    public void onSyncProgress( int progressPercent );
    public void onBatteryStatusChange(int batteryRatio);
    public void dismissSyncView();
}
