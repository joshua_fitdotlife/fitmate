package com.fitdotlife.fitmate_lib.presenter;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;

import com.fitdotlife.fitmate.model.ActivityDataModel;
import com.fitdotlife.fitmate.model.UserInfoModel;
import com.fitdotlife.fitmate_lib.iview.IDeviceChangeView;
import com.fitdotlife.fitmate_lib.service.ServiceClient;
import com.fitdotlife.fitmate_lib.service.ServiceRequestCallback;
import com.fitdotlife.fitmate_lib.service.bluetooth.FitmeterDeviceDriver;
import com.fitdotlife.fitmate_lib.service.key.MessageType;
import com.fitdotlife.fitmate_lib.service.protocol.FileReadException;
import com.fitdotlife.fitmate_lib.service.protocol.ProtocolManager;
import com.fitdotlife.fitmate_lib.service.protocol.object.SerialCode;
import com.fitdotlife.fitmate_lib.service.protocol.object.SystemInfo_Request;
import com.fitdotlife.fitmate_lib.service.protocol.object.SystemInfo_Response;

import org.apache.log4j.Log;

/**
 * Created by Joshua on 2015-03-26.
 */
public class DeviceChangePresenter implements ServiceRequestCallback {

    private String TAG = "fitmate";
    private Context mContext = null;
    private UserInfoModel userInfoModel = null;
    private ServiceClient serviceClient = null;
    private ProtocolManager protocolManager = null;
    private IDeviceChangeView mView = null;
    private ActivityDataModel dataModel = null;
    private boolean mDeleteDeviceData = false;

    public DeviceChangePresenter( Context context , IDeviceChangeView view ){
        this.mContext = context;
        this.mView = view;
        this.userInfoModel = new UserInfoModel(this.mContext);
        this.dataModel = new ActivityDataModel(this.mContext);
        this.serviceClient = new ServiceClient( this.mContext , this );
        this.serviceClient.open();
    }

    @Override
    public void onServiceResultReceived(int what, Bundle data) {

    }

    @Override
    public void serviceInitializeCompleted() {

    }

    public void stopService(){
        if( this.serviceClient != null ){
            this.serviceClient.stop();
        }
    }

    public void close(){
        if( this.serviceClient != null ) {
            this.serviceClient.close();
        }
    }

    public void changeDevice( BluetoothAdapter bluetoothAdapter , String deviceAddress , BluetoothDevice device  , boolean deleteDeviceData ){
        this.mDeleteDeviceData = deleteDeviceData;

        //서비스 DB에 데이터를 지운다.
        try {
            serviceClient.sendServiceMessage(MessageType.MSG_DELETE_ALLFILES);
        } catch (RemoteException e) {
            e.printStackTrace();
        }


        boolean isSuccess = false;
        for(int i = 0; i< 3; i++)
        {
            FitmeterDeviceDriver mDriver = FitmeterDeviceDriver.ConnectionOnce(mContext, deviceAddress);

            if(mDriver != null)
            {
                try
                {
                    // todo 여기에 데이터 파일 지우기 구현 필요
                    this.protocolManager = new ProtocolManager( this.mContext , mDriver );
                    protocolManager.onLED();
                    Thread.sleep(100);
                    protocolManager.offLED();


                    this.userInfoModel.updateDeviceAddress(deviceAddress);

                    int location = mView.getWearLocation();
                    if(location != -1) {
                        SystemInfo_Response sr = protocolManager.getSystemInfo();

                        SerialCode sc =sr.getSerialCode();

                        SystemInfo_Request dd = sr.getSystemInfo();
                        dd.setWearingPosition(location);

                        boolean setresult = protocolManager.setSystemInfo(dd);
                        if(setresult)
                        {
                            Log.d("PROTOCOL", "SETOK");
                        }
                    }
                    isSuccess = true;

                    try {
                        protocolManager.setCurrentTime();
                    }catch( FileReadException e) {
                        Log.e(TAG,"시간을 설정하는 중에 오류가 발생하였습니다.");
                    }

                    // todo 장치 설정 한번 해서 파일 끊었다 쓰게 해야됨.

                    //기계에 있는 데이터를 지운다.
                    if(mDeleteDeviceData){
                        try {
                            protocolManager.deleteAllFile();
                        } catch (FileReadException e) {
                            Log.e(TAG, "데이터 파일을 지우는 중에 오류가 발생하였습니다.");
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.d("DATACOMM", ex.getMessage());
                }
                finally {
                    mDriver.disconnect();
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    mDriver.close();
                }

                break;
            }
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        //오늘의 데이터를 needMerger로 설정을 해야 함.
        dataModel.setNeedMergeToday();

        mView.showResultDialog(isSuccess, deleteDeviceData);

        //todo : 일단은 장치 변경 후 그냥 끝나는걸로. 추후에 해당 부분의 주석을 해제하고 장치 변경에 맞는 기능을 수행해야됨
        //FitmeterDeviceDriver mDriver = new FitmeterDeviceDriver(this.mContext , bluetoothAdapter , device , this);
        //this.protocolManager = new ProtocolManager( this.mContext , mDriver );
    }

}
