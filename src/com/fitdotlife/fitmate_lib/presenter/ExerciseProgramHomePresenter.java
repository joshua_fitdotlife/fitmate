package com.fitdotlife.fitmate_lib.presenter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Messenger;

import com.fitdotlife.fitmate_lib.iview.IExerciseHomeView;
import com.fitdotlife.fitmate.SetExerciseProgramActivity;
import com.fitdotlife.fitmate.model.ExerciseProgramModel;
import com.fitdotlife.fitmate_lib.object.ExerciseProgram;

import java.util.List;

/**
 * Created by Joshua on 2015-02-10.
 */
public class ExerciseProgramHomePresenter {

    private Context mContext = null;
    private IExerciseHomeView mView = null;
    private ExerciseProgramModel mModel = null;
    private Messenger mMessenger = null;

    public ExerciseProgramHomePresenter( Context context , IExerciseHomeView view  , Messenger messenger )
    {
        this.mContext = context;
        this.mView = view;
        this.mMessenger = messenger;
        this.mModel = new ExerciseProgramModel( context);
    }

    public List<ExerciseProgram> getUserProgramList()
    {
        List<ExerciseProgram> userProgramList =  this.mModel.getUserProgramList();
        return userProgramList;
    }

    public List<ExerciseProgram> getUserProgramList( String searchWord )
    {
        List<ExerciseProgram> userProgramList =  this.mModel.getUserProgramList( searchWord );
        return userProgramList;
    }

    public List<ExerciseProgram> getExerciseProgramList()
    {
        List<ExerciseProgram> programList = this.mModel.getProgramList();
        return programList;
    }

    public List<ExerciseProgram> getExerciseProgramList( String searchWord )
    {
        List<ExerciseProgram> programList = this.mModel.getProgramList( searchWord);
        return programList;
    }

    public void moveSetActivity(Activity activity){
    	Intent intent = new Intent( activity , SetExerciseProgramActivity.class );
    	intent.putExtra( "messenger" , mMessenger);
    	mContext.startActivity(intent);
    }

    public ExerciseProgram getAppliedExerciseProgram(){
        return this.mModel.getAppliedProgram();
    }

}