package com.fitdotlife.fitmate_lib.presenter;

import android.content.Context;

import com.fitdotlife.fitmate.R;
import com.fitdotlife.fitmate.model.UserInfoModel;
import com.fitdotlife.fitmate.model.UserInfoModelResultListener;
import com.fitdotlife.fitmate_lib.iview.IExerciseProgramInfoView;
import com.fitdotlife.fitmate_lib.key.ExerciseProgramType;
import com.fitdotlife.fitmate.model.ExerciseProgramModel;
import com.fitdotlife.fitmate_lib.object.ExerciseProgram;
import com.fitdotlife.fitmate_lib.object.UserInfo;

/**
 * Created by Joshua on 2015-02-10.
 */
public class ExerciseProgramInfoPresenter implements UserInfoModelResultListener {

    private final String TAG = "fitmate";

    private Context mContext = null;
    private IExerciseProgramInfoView mView = null;
    private ExerciseProgramModel mModel = null;
    private UserInfoModel mUserInfoModel = null;
    private int mType = -1;

    public ExerciseProgramInfoPresenter( Context context , IExerciseProgramInfoView view , int exerciseProgramType)
    {
        this.mContext = context;
        this.mView = view;
        this.mType = exerciseProgramType;

        this.mModel = new ExerciseProgramModel(context);
        this.mUserInfoModel = new UserInfoModel(context , this);
    }

    public ExerciseProgram getExerciseProgram( int id )
    {
        ExerciseProgram program = null;

        if( this.mType == ExerciseProgramType.USEREXERCISEPROGRAM ){
            program = this.mModel.getUserExerciseProgram( id );
        }else if( this.mType == ExerciseProgramType.EXERCISEPROGRAM ){
            program = this.mModel.getExerciseProgram( id );
        }

        return program;
    }

    public void applyExerciseProgram( ExerciseProgram exerciseProgram){

        this.mModel.applyExerciseProgram(exerciseProgram);
        this.mUserInfoModel.changeExerciseProgramIDForServer( exerciseProgram.getId() );
        this.mView.showResult(this.mContext.getString(R.string.exercise_detail_apply_success));
    }

    public void modifyExerciseProgram( ExerciseProgram program ){
        this.mModel.modifyExerciseProgram(program);
    }

    public ExerciseProgram getAppliedProgram(){
        return this.mModel.getAppliedProgram();
    }

    @Override
    public void onSuccess(int code) {
        if( code == UserInfoModel.CHANGE_USERINFO_RESPONSE ){

        }
    }

    @Override
    public void onSuccess(int code, boolean result) {

    }

    @Override
    public void onSuccess(int code, UserInfo userInfo) {

    }

    @Override
    public void onFail(int code) {
        if( code == UserInfoModel.CHANGE_USERINFO_RESPONSE ){

        }
    }

    @Override
    public void onErrorOccured(int code, String message) {

    }
}
