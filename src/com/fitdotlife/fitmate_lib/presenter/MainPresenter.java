package com.fitdotlife.fitmate_lib.presenter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;

import com.fitdotlife.fitmate_lib.iview.IMainView;
import com.fitdotlife.fitmate_lib.object.DayActivity;
import com.fitdotlife.fitmate_lib.object.MonthActivity;
import com.fitdotlife.fitmate_lib.object.WeekActivity;
import com.fitdotlife.fitmate_lib.service.ServiceClient;
import com.fitdotlife.fitmate_lib.service.ServiceRequestCallback;
import com.fitdotlife.fitmate_lib.service.key.SyncType;
import com.fitdotlife.fitmate_lib.util.DateUtils;
import com.fitdotlife.fitmate_lib.util.Utils;
import com.fitdotlife.fitmate.R;
import com.fitdotlife.fitmate.model.ActivityDataListener;
import com.fitdotlife.fitmate.model.ActivityDataModel;
import com.fitdotlife.fitmate.model.ExerciseProgramModel;
import com.fitdotlife.fitmate_lib.object.ExerciseProgram;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Joshua on 2015-01-29.
 */
public class MainPresenter implements ServiceRequestCallback, ActivityDataListener{

    private final String TAG = "fitmate";
    private final String RESULT_KEY = "result";
    private final String SUCCESS = "SUCCESS";
    private final String FAIL = "FAIL";
    private final String NONE = "NONE";

    private enum SelectType
    {
        DAY, WEEK , MONTH , NONE
    }

    private enum MoveDateType
    {
        PRE , NEXT
    }

    private Calendar activityCalendar_day = null;
    private Calendar activityCalendar_week = null;
    private Calendar activityCalendar_month= null;
    private SelectType mSelectType = null;
    private MoveDateType mMoveDateType = null;

    private Context mContext = null;
    private IMainView mView = null;
    private ActivityDataModel mActivityDataModel = null;
    private ExerciseProgramModel mProgramModel = null;
    private ServiceClient mServiceClient = null;

    private long mTodayMilliSecond = 0;
    private boolean mInitView = true;

    private boolean isRequestCalculate = false;
    private boolean showDayNoData = true;
    private boolean showWeekNoData = true;
    private boolean showMonthNoData = true;

    public Calendar resetTime (Date d) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(d);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal;
    }

    public MainPresenter( Context context , IMainView view )
    {
        this.mContext = context;
        this.mView = view;
        this.mProgramModel = new ExerciseProgramModel(this.mContext);

        this.mServiceClient = new ServiceClient(this.mContext , this);
        this.mActivityDataModel = new ActivityDataModel( this.mContext , this );
        this.mServiceClient.open();
    }

    public void init( Intent intent ){
        this.mSelectType = SelectType.NONE;
        Calendar today = resetTime(Calendar.getInstance().getTime());
        this.activityCalendar_day = Calendar.getInstance();

        String searchDayDate = intent.getStringExtra("ActivityDate");

        if( searchDayDate != null ) {
            this.activityCalendar_day.setTimeInMillis( DateUtils.getDateFromString_yyyy_MM_dd(searchDayDate).getTime() );
            this.mSelectType = SelectType.DAY;
        }else{
            this.mTodayMilliSecond = today.getTimeInMillis();
            this.activityCalendar_day.setTimeInMillis(today.getTimeInMillis());
        }

        //해당 주의 첫째날로
        this.activityCalendar_week = Calendar.getInstance();
        String searchWeekDate = intent.getStringExtra("WeekStartDate");
        if(searchWeekDate != null){
            this.activityCalendar_week.setTimeInMillis( DateUtils.getDateFromString_yyyy_MM_dd(searchWeekDate).getTime() );
            this.mSelectType = SelectType.WEEK;
        }else{

            this.activityCalendar_week.setTimeInMillis(today.getTimeInMillis());
            this.activityCalendar_week.add(Calendar.DATE, -today.get(Calendar.DAY_OF_WEEK) + 1);

        }

        //해당월의 1일로
        this.activityCalendar_month = Calendar.getInstance();
        this.activityCalendar_month.setTimeInMillis( today.getTimeInMillis());
        activityCalendar_month.set(Calendar.DAY_OF_MONTH, 1);

        this.setInitialView(true);
        if(mSelectType.equals(SelectType.DAY.NONE) || mSelectType.equals(SelectType.DAY)) {
            this.getDayActivity();
        }else if(mSelectType.equals(SelectType.WEEK)){
            this.getWeekActivity();
        }
    }


    public void movePreDate()
    {
        switch( this.mSelectType )
        {
            case DAY:
                this.activityCalendar_day.add( Calendar.DATE , -1 );
                this.getDayActivity();
                break;
            case WEEK:
                this.activityCalendar_week.add( Calendar.DATE , -7 );
                this.getWeekActivity();
                break;
            case MONTH:
                this.activityCalendar_month.add( Calendar.MONTH , -1 );
                this.getMonthActivity();
                break;
        }
    }

    public void moveNextDate()
    {
        switch( this.mSelectType )
        {
            case DAY:
                this.activityCalendar_day.add( Calendar.DATE , 1 );
                this.getDayActivity();
                break;
            case WEEK:
                this.activityCalendar_week.add( Calendar.DATE , 7 );
                this.getWeekActivity();
                break;
            case MONTH:
                this.activityCalendar_month.add( Calendar.MONTH , 1 );
                this.getMonthActivity();
                break;
        }
    }

    public void getDayActivity(){

        //로컬에 DayActivity를 확인한다.
        String activityDate = this.getDateString(this.activityCalendar_day);
        DayActivity dayActivity = this.mActivityDataModel.getDayActivity(activityDate);

        String weekStartDate = this.getWeekStartDate(this.activityCalendar_day);
        WeekActivity weekActivity = this.mActivityDataModel.getWeekActivity(weekStartDate);
        if( weekActivity != null ){ //있다면 보여준다.

            this.displayDayActivity();

        }else{

            if(Utils.isOnline(this.mContext) ) {
                this.mActivityDataModel.getDayActivityFromServer(weekStartDate);
            }else{
                this.displayDayActivity();
            }
        }
    }

    private void displayDayActivity( ){

        String activityDate = this.getDateString(this.activityCalendar_day);
        DayActivity dayActivity = this.mActivityDataModel.getDayActivity(activityDate);

        if( dayActivity == null ){
            if( showDayNoData ) {
                mView.showResult(this.mContext.getString(R.string.activity_no_data));
                showDayNoData = false;
            }
        }

        this.mSelectType = SelectType.DAY;

        int dayOfWeek = activityCalendar_day.get( Calendar.DAY_OF_WEEK );
        int month = activityCalendar_day.get(Calendar.MONTH);
        int day = activityCalendar_day.get(Calendar.DAY_OF_MONTH);
        int year = activityCalendar_day.get(Calendar.YEAR);

        String weekStartDate = this.getWeekStartDate(activityCalendar_day);
        WeekActivity weekActivity = this.mActivityDataModel.getWeekActivity(weekStartDate);
        int weekScore = 0;
        if( weekActivity != null ) {
            weekScore = weekActivity.getScore();
        }

        ExerciseProgram appliedProgram = this.mProgramModel.getAppliedProgram();
        if( mInitView ) {
            mView.displayDayActivity(dayActivity, weekScore, year, month, day, dayOfWeek, appliedProgram);
        }else{
            mView.updateDayActivity(dayActivity , weekScore , appliedProgram);
        }
    }

    public void getWeekActivity(  ){

        int dayOfWeek = activityCalendar_week.get(Calendar.DAY_OF_WEEK);
        Calendar weekCalendar = Calendar.getInstance();
        weekCalendar.setTimeInMillis(this.activityCalendar_week.getTimeInMillis());
        weekCalendar.add(Calendar.DATE, 6);
        String weekEndDate = this.getDateString(weekCalendar);
        String weekStartDate = this.getDateString(activityCalendar_week);

        WeekActivity weekActivity = this.mActivityDataModel.getWeekActivity(weekStartDate);
        if( weekActivity != null ){
            this.displayWeekActivity();
        }else{
            if( Utils.isOnline(this.mContext) ) {
                this.mActivityDataModel.getWeekActivityFromServer(weekStartDate);
            }else{
                this.displayWeekActivity();
            }
        }
    }

    private void displayWeekActivity( ){

        int dayOfWeek = activityCalendar_week.get( Calendar.DAY_OF_WEEK );
        int month = activityCalendar_week.get( Calendar.MONTH );
        int year = activityCalendar_week.get( Calendar.YEAR );
        int day= activityCalendar_week.get(Calendar.DAY_OF_MONTH);


        activityCalendar_week.setMinimalDaysInFirstWeek(1);
        int weekNumber = activityCalendar_week.get(Calendar.WEEK_OF_MONTH);

        Calendar weekCalendar = Calendar.getInstance();
        weekCalendar.setTimeInMillis(this.activityCalendar_week.getTimeInMillis());
        weekCalendar.add(Calendar.DATE, (7 - dayOfWeek));
        String weekEndDate = this.getDateString( weekCalendar );
        weekCalendar.add(Calendar.DATE , -6);
        String weekStartDate = this.getDateString(weekCalendar);

        WeekActivity weekActivity = this.mActivityDataModel.getWeekActivity( weekStartDate );
        if( weekActivity == null )
        {
            if( showWeekNoData ) {
                mView.showResult(this.mContext.getString(R.string.activity_no_data));
                showWeekNoData = false;
            }
        }

        this.mSelectType = SelectType.WEEK;

        int[] arrExerciseTime = new int[7];
        int[] arrCalorie = new int[7];
        String[] arrDayDate = new String[7];

        List<DayActivity> dayActivityList = this.mActivityDataModel.getDayActivityList(weekStartDate, weekEndDate);
        if( dayActivityList != null ) {
            for (int index = 0; index < 7; index++) {
                String searchDate = this.getDateString(weekCalendar);
                arrDayDate[index] = searchDate;
                Iterator<DayActivity> dayActivityIterator = dayActivityList.iterator();

                while (dayActivityIterator.hasNext()) {
                    DayActivity dayActivity = dayActivityIterator.next();
                    if (dayActivity != null) {
                        if (dayActivity.getActivityDate().equals(searchDate)) {
                            arrExerciseTime[index] = dayActivity.getAchieveOfTarget();
                            arrCalorie[index] = dayActivity.getCalorieByActivity();
                        }
                    }
                }

                weekCalendar.add(Calendar.DATE, 1);
            }
        }

        int programId = this.mProgramModel.getAppliedProgram().getId();
        if(weekActivity != null){
            programId = weekActivity.getExerciseProgramID();
        }
        ExerciseProgram appliedProgram = this.mProgramModel.getUserExerciseProgram(programId);

    if( mInitView ) {
        mView.displayWeekActivity(weekActivity, arrExerciseTime, arrCalorie , arrDayDate , year, month, day, weekNumber, appliedProgram);
    }else{
        mView.updateWeekActivity(weekActivity , arrExerciseTime , arrCalorie , appliedProgram);
    }
}


    private String getWeekStartDate(Calendar cal  ) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(cal.getTimeInMillis());
        calendar = resetTime(cal.getTime());
        //calendar.setTimeInMillis(today.getTimeInMillis());
        calendar.add(Calendar.DATE, -calendar.get(Calendar.DAY_OF_WEEK) + 1);

      /*  Calendar weekCalendar = Calendar.getInstance();
        weekCalendar.setTimeInMillis( this.activityCalendar_week.getTimeInMillis() );
        int weekNumber = weekCalendar.get(Calendar.DAY_OF_WEEK);
        weekCalendar.add(Calendar.DATE , -( weekNumber - 1 ) );*/
        String weekStartDate = this.getDateString(calendar);
        return weekStartDate;
        /*Calendar weekCalendar = Calendar.getInstance();
        weekCalendar.setTimeInMillis( this.activityCalendar_week.getTimeInMillis() );
        int weekNumber = weekCalendar.get(Calendar.DAY_OF_WEEK);
        weekCalendar.add(Calendar.DATE , -( weekNumber - 1 ) );
        String weekStartDate = this.getDateString_yyyy_MM_dd(weekCalendar);
        return weekStartDate;*/
    }

    private String getWeekEndDate( ) {

        Calendar weekCalendar = Calendar.getInstance();
        weekCalendar.setTimeInMillis( this.activityCalendar_week.getTimeInMillis() );
     //   int weekNumber = weekCalendar.get(Calendar.DAY_OF_WEEK);
        weekCalendar.add(Calendar.DATE, 6);
        String weekendDate = this.getDateString(weekCalendar);
        return weekendDate;
    }

    public void getMonthActivity(){

        Calendar monthCalendar = Calendar.getInstance();
        monthCalendar.setTimeInMillis(this.activityCalendar_month.getTimeInMillis());
        int lastDay  = monthCalendar.getActualMaximum(Calendar.DATE);
        monthCalendar.set(Calendar.DAY_OF_MONTH, lastDay);
        int monthWeekNumber = monthCalendar.get(Calendar.WEEK_OF_MONTH);
        String monthEndDate = this.getDateString(monthCalendar);
        monthCalendar.set(Calendar.DAY_OF_MONTH, 1);
        int dayOfWeek = monthCalendar.get(Calendar.DAY_OF_WEEK);
        monthCalendar.add(Calendar.DATE, -(dayOfWeek - 1));
        String monthWeekStartDate = this.getDateString(monthCalendar);

        List<WeekActivity> weekActivityList = this.mActivityDataModel.getWeekActivityList(monthWeekStartDate, monthEndDate);
        List<String> noWeekList = this.getMonthNoWeekList();
        if( noWeekList.size() > 0 ){
            if( Utils.isOnline(this.mContext) ) {
                this.mActivityDataModel.getMonthActivityListFromServer( noWeekList);
            }else{
                displayMonthActivity();
            }
        }else{
            displayMonthActivity( );
        }
    }

    private List<String> getMonthNoWeekList(){
        /*
            사용자가 선택한 달에 오늘이 포함되어 있다면
            오늘이 포함된 주까지만 주데이터가 없는 주를 확인한다.
         */
        Calendar monthCalendar = Calendar.getInstance();
        int monthWeekNumber = 0;
        monthCalendar.setTimeInMillis(this.activityCalendar_month.getTimeInMillis());
        int searchMonth = monthCalendar.get(Calendar.MONTH);
        int seachYear = monthCalendar.get(Calendar.YEAR);

        //오늘 년월을 가져온다.
        Calendar todayCalendar = Calendar.getInstance();
        int todayMonth = todayCalendar.get(Calendar.MONTH);
        int todayYear = todayCalendar.get(Calendar.YEAR);

        if( searchMonth == todayMonth && seachYear == todayYear ){
            int todayDate = todayCalendar.get( Calendar.DAY_OF_MONTH );
            monthCalendar.set(Calendar.DAY_OF_MONTH , todayDate);
            monthWeekNumber = monthCalendar.get(Calendar.WEEK_OF_MONTH);
        }else{
            int lastDay  = monthCalendar.getActualMaximum(Calendar.DATE);
            monthCalendar.set(Calendar.DAY_OF_MONTH, lastDay);
            monthWeekNumber = monthCalendar.get(Calendar.WEEK_OF_MONTH);
        }

        monthCalendar.set(Calendar.DAY_OF_MONTH, 1);
        int dayOfWeek = monthCalendar.get(Calendar.DAY_OF_WEEK);
        monthCalendar.add(Calendar.DATE , -( dayOfWeek - 1 ) );

        List<String> noWeekList = new ArrayList<String>();
        for( int i = 0 ; i < monthWeekNumber;i++ ){
            String weekStartDate = this.getDateString( monthCalendar );
            WeekActivity weekActivity = this.mActivityDataModel.getWeekActivity( weekStartDate );

            if( weekActivity == null ){
                noWeekList.add( weekStartDate );
            }

            monthCalendar.add(Calendar.DATE , 7);
        }

        return noWeekList;
    }

    private void displayMonthActivity(  ){

        Calendar monthCalendar = Calendar.getInstance();
        monthCalendar.setTimeInMillis(this.activityCalendar_month.getTimeInMillis());
        int lastDay  = monthCalendar.getActualMaximum(Calendar.DATE);
        monthCalendar.set(Calendar.DAY_OF_MONTH, lastDay);
        int monthWeekNumber = monthCalendar.get( Calendar.WEEK_OF_MONTH );
        String monthEndDate = this.getDateString(monthCalendar);
        monthCalendar.set(Calendar.DAY_OF_MONTH, 1);
        int weekNumber = monthCalendar.get(Calendar.DAY_OF_WEEK);
        monthCalendar.add(Calendar.DATE, -(weekNumber - 1));
        String monthWeekStartDate = this.getDateString(monthCalendar);

        int month = this.activityCalendar_month.get(Calendar.MONTH);
        int year = this.activityCalendar_month.get(Calendar.YEAR);
        int day = this.activityCalendar_month.get(Calendar.DATE);

        List<WeekActivity> weekActivityList = this.mActivityDataModel.getWeekActivityList(monthWeekStartDate, monthEndDate);

        if( weekActivityList == null ){
            if( showMonthNoData ) {
                this.mView.showResult(this.mContext.getString(R.string.activity_no_data));
                showMonthNoData = false;
            }
        }

        this.mSelectType = SelectType.MONTH;

        MonthActivity monthActivity = null;
        if( weekActivityList != null && weekActivityList.size() > 0 ) {

            monthActivity = new MonthActivity();

            int[] arrScore = new int[monthWeekNumber];
            int[] arrCalorie = new int[monthWeekNumber];

            int scoreSum = 0;
            int scoreCount = 0;

            for (int index = 0; index < monthWeekNumber; index++) {

                String activityDate = this.getDateString(monthCalendar);

                Iterator<WeekActivity> weekActivityListIter = weekActivityList.iterator();
                while (weekActivityListIter.hasNext()) {
                    WeekActivity weekActivity = weekActivityListIter.next();

                    if (weekActivity != null) {
                        if (weekActivity.getActivityDate().equals(activityDate)) {
                            int score = weekActivity.getScore();
                            arrScore[index] = score;
                            arrCalorie[index] = weekActivity.getAverageCalorie();
                            scoreSum += score;
                            scoreCount += 1;
                            break;
                        }
                    }
                }
                monthCalendar.add(Calendar.DATE, 7);
            }

            monthActivity.setCalorieList(arrCalorie);
            monthActivity.setScoreList(arrScore);

            int averageScore = 0;
            if (scoreSum > 0) {
                averageScore = scoreSum / scoreCount;
            }
            monthActivity.setAverageScore(averageScore);
        }

        ExerciseProgram appliedProgram = this.mProgramModel.getAppliedProgram();
        if( mInitView ) {
            mView.displayMonthActivity(monthActivity, year, month, day, appliedProgram);
        }else{
            mView.updateMonthActivity( monthActivity , appliedProgram );
        }
    }

    public ExerciseProgram getAppliedExerciseProgram(){
        return this.mProgramModel.getAppliedProgram();
    }

    private String getDateString( Calendar calendar )
    {
        return calendar.get(Calendar.YEAR) + "-" + addZero( calendar.get(Calendar.MONTH) + 1 ) + "-" + addZero( calendar.get(Calendar.DAY_OF_MONTH));
    }

    private String addZero( int calNumber )
    {
        String strNumber = String.valueOf(calNumber);
        if( strNumber.length() < 2 ) strNumber = "0" + strNumber;
        return strNumber;
    }

    public MetInfo convertByteArrayToFloatArray( byte[] metArray )
    {
        MetInfo metInfo = new MetInfo();

        int metArrayLength = 1440;
        float[] metFloatArray = new float[144];
        int averageMet = 0;

        for( int i = 0 ; i < metArrayLength ;i++ )
        {
            float met = (float) ( Math.round( metArray[2 * i ] * 100d  +  metArray[ 2 * i + 1] ) / 100d );

            int index = i / 10;
//            int index = 0;
//            if( i > 0 ){ index = i / 10; }

            metFloatArray[ index ] += met;

            averageMet += met;
        }

        for(int i = 0 ; i < metFloatArray.length ;i++)
        {
            metFloatArray[ i ] /= 10;
        }

        averageMet /= metArrayLength;

        metInfo.metFloatArray = metFloatArray;
        metInfo.averageMet = averageMet;

        return metInfo;
    }

    public void requestCalculateActivity() {
        Log.i(TAG, "requestCalculateActivity()");

        if( this.isRequestCalculate ){
            return;
        }

        isRequestCalculate = true;

        try {
            this.mServiceClient.sendServiceMessage(ServiceClient.MSG_CALCULATE_ACTIVITY);
        } catch (RemoteException e) {
            Log.e(TAG , "계산 요청 중에 에러가 발생했습니다.");
            isRequestCalculate = false;
            this.getData();
        }
    }

    public void close(){
        this.mServiceClient.close();
    }

    class MetInfo
    {
        public float[] metFloatArray;
        public int averageMet;
    }

    public void onCalculateCompleted() {
        isRequestCalculate = false;
        this.getData();
    }

    private void getData(){
        switch( this.mSelectType )
        {
            case DAY:
                this.getDayActivity();
                break;
            case WEEK:
                this.getWeekActivity();
                break;
            case MONTH:
                this.getMonthActivity();
                break;
        }
    }


    public void onSyncEnd() {
        try {
            this.setInitialView(false);
            this.mServiceClient.sendServiceMessage(ServiceClient.MSG_CALCULATE_ACTIVITY);
        } catch (RemoteException e) {
            Log.d(TAG , "동기화 후 데이터 요청에 문제가 생겼습니다.");
            this.getData();
        }
    }

    public void onSyncProgress(int receiveBytes, int totalBytes) {
        this.setInitialViewComplete( true );
    }

    private void setInitialViewComplete( boolean initViewComplete ){
        SharedPreferences pref = this.mContext.getSharedPreferences("fitmateservice", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean( LoginPresenter.INIT_VIEW_COMPLETE_KEY, initViewComplete);
        editor.commit();
    }

    @Override
    public void onDayActivityReceived( ) {
        this.displayDayActivity( );
    }

    @Override
    public void onWeekActivityReceived(  ) {
        displayWeekActivity();
    }

    @Override
    public void onMonthActivityReceived(  ) {
        this.displayMonthActivity();
    }

    @Override
    public void onUploadedData(  boolean isUpload ) {

    }

    private long getTimeMillisecond( String strDate ){

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = format.parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.getTimeInMillis();
    }

    @Override
    public void onServiceResultReceived(int what, Bundle data) {
        if( what == ServiceClient.MSG_CALCULATE_ACTIVITY ){
            this.onCalculateCompleted();
        }else if( what == ServiceClient.MSG_DATA_SYNC ){
            SyncType syncType = SyncType.getSyncType( data.getInt( "synctype" ) );

            if( syncType.equals( SyncType.PROGRESS ) ){
                int recceiveBytes = data.getInt( "receivebyte" );
                int totalBytes = data.getInt( "totalbyte" );
                this.onSyncProgress(recceiveBytes , totalBytes);
            }else if( syncType.equals( SyncType.END )){
                this.onSyncEnd();
            }
        }
    }

    @Override
    public void serviceInitializeCompleted() {

    }

    public void setInitialView( boolean initialView ){
        this.mInitView = initialView;
    }

}
