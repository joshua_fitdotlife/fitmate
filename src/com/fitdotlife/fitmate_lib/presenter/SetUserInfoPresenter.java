package com.fitdotlife.fitmate_lib.presenter;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.fitdotlife.fitmate.ActivityManager;
import com.fitdotlife.fitmate.ActivityReference;
import com.fitdotlife.fitmate.R;
import com.fitdotlife.fitmate.model.UserInfoModel;
import com.fitdotlife.fitmate.model.UserInfoModelResultListener;
import com.fitdotlife.fitmate_lib.iview.ISetUserInfoView;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.service.ServiceClient;
import com.fitdotlife.fitmate_lib.service.ServiceRequestCallback;
import com.fitdotlife.fitmate_lib.service.bluetooth.FitmeterDeviceDriver;
import com.fitdotlife.fitmate_lib.service.protocol.FileReadException;
import com.fitdotlife.fitmate_lib.service.protocol.ProtocolManager;
import com.fitdotlife.fitmate_lib.service.protocol.object.SystemInfo_Request;
import com.fitdotlife.fitmate_lib.service.protocol.object.SystemInfo_Response;
import com.fitdotlife.fitmate_lib.util.Utils;

import org.apache.log4j.Log;

/**
 * Created by Joshua on 2015-01-29.
 */
public class SetUserInfoPresenter implements UserInfoModelResultListener , ServiceRequestCallback, FitmeterDeviceDriver.BLEDeviceStateCallback {

    private String TAG = "fitmate";
    private Context mContext = null;
    private ISetUserInfoView mView = null;
    private UserInfo mUserInfo = null;
    private UserInfoModel mModel = null;

    private String mEncodePassword = null;
    private String mEmail = null;

    private ServiceClient mServiceClient = null;
    private boolean mUsableEmail = false;

    private boolean mCallbackWaiting = false;

    private ProtocolManager mProtocolManager = null;
    private BluetoothAdapter mBluetoothAdapter = null;
    private BluetoothDevice mBluetoothDevice = null;

    public SetUserInfoPresenter( Context context, ISetUserInfoView view) {
        this.mContext = context;
        this.mView = view;
        this.mModel = new UserInfoModel( context , this);

        this.mServiceClient = new ServiceClient( this.mContext , this );
    }

    public void setUserInfo(UserInfo userInfo, BluetoothAdapter bluetoothAdapter, String s, BluetoothDevice bluetoothDevice){

        if(!Utils.isOnline(this.mContext) ) {
            mView.showResult( this.mContext.getString(R.string.common_connect_network) );
            return;
        }

        this.mBluetoothAdapter = bluetoothAdapter;
        this.mBluetoothDevice = bluetoothDevice;

        this.mView.startProgressDialog( this.mContext.getString(R.string.signin_dialog_title) , this.mContext.getString(R.string.signin_dialog_content) );
        this.mEmail = userInfo.getEmail();
        this.mEncodePassword = userInfo.getPassword();
        this.mUserInfo = userInfo;
        this.mModel.registerUserInfo(userInfo);
    }


    public void setUserAccount(UserInfo userInfo)
    {
        this.mView.startProgressDialog( this.mContext.getString(R.string.signin_dialog_title) , this.mContext.getString(R.string.signin_dialog_content) );
        this.mEmail = userInfo.getEmail();
        this.mEncodePassword = userInfo.getPassword();
        this.mUserInfo = userInfo;
        this.mModel.setUserAccount(userInfo);
    }


    public void ismUsableEmail( String email){

        if( !Utils.isOnline( this.mContext ) ){
            mView.showResult(this.mContext.getString(R.string.common_connect_network));
            return;
        }

        this.mView.startProgressDialog( this.mContext.getString(R.string.signin_email_dialog_title_duplicate) , this.mContext.getString(R.string.signin_email_dialog_content_duplicate) );
        this.mModel.isUsableEmail(email);
    }



    @Override
    public void onSuccess(int code) {
        if( code == UserInfoModel.JOIN_MEMBER_RESPONSE ) {

            this.setAutoLogin(true);
            this.setInitExecute(false);
            this.mServiceClient.start();

            for(int i = 0; i< 3; i++)
            {
                FitmeterDeviceDriver mDriver = FitmeterDeviceDriver.ConnectionOnce(mContext, this.mBluetoothDevice.getAddress());

                if(mDriver != null)
                {
                    try
                    {
                        // todo 여기에 데이터 파일 지우기 구현 필요
                        this.mProtocolManager = new ProtocolManager( this.mContext , mDriver );

                        //이전 데이터를 삭제한다.
                        try {
                            mProtocolManager.deleteAllFile();
                        } catch (FileReadException e) {
                            Log.e(TAG, "데이터 파일을 지우는 중에 오류가 발생하였습니다.");
                        }

                        //LED 켜기
                        mProtocolManager.onLED();
                        Thread.sleep(100);
                        mProtocolManager.offLED();

                        //시간 설정
                        try {
                            mProtocolManager.setCurrentTime();
                        }catch( FileReadException e) {
                            Log.e(TAG,"시간을 설정하는 중에 오류가 발생하였습니다.");
                        }

                        //착용위치 설정.
                        SystemInfo_Response sr = mProtocolManager.getSystemInfo();
                        SystemInfo_Request dd = sr.getSystemInfo();
                        dd.setWearingPosition(this.mUserInfo.getWearAt());
                        mProtocolManager.setSystemInfo(dd);

                    }
                    catch (Exception ex)
                    {
                        Log.d("DATACOMM", ex.getMessage());
                    }
                    finally {

                        mDriver.disconnect();
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        mDriver.close();
                    }

                    break;
                }
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            this.mView.stopProgressDialog();

            //설정화면이나 내정보 화면 그 외에 화면을 없앤다.
            ActivityManager.getInstance().finishAllActivity();
            //홈화면을 없앤다.
            ActivityReference.activityReferece.finish();
            this.setSettedUserInfo(true);
            this.mView.moveHomeActivity();
        }else if(code == UserInfoModel.USABLE_EMAIL_RESPONSE){
            this.mView.stopProgressDialog();
            this.mView.setDuplicateResult( true );
            this.mView.showResult(this.mContext.getString(R.string.signin_email_available));
        }else if( code == UserInfoModel.SET_ACCOUNT_RESPONSE ){
            this.setAutoLogin(true);
            this.setInitExecute(false);
            this.setSettedUserInfo(false);
            this.mView.stopProgressDialog();

            //로그인 화면을 없앤다.
            ActivityReference.activityReferece.finish();
            this.mView.moveHomeActivity();
        }
    }

    @Override
    public void onSuccess(int code, boolean result) {

    }

    @Override
    public void onSuccess(int code, UserInfo userInfo) {

    }

    @Override
    public void onFail(int code) {
        if (code == UserInfoModel.JOIN_MEMBER_RESPONSE || code == UserInfoModel.SET_ACCOUNT_RESPONSE) {
            this.mView.stopProgressDialog();
            this.mView.showResult(this.mContext.getString(R.string.signin_setuserinfo_fail));
    }   else if( code == UserInfoModel.USABLE_EMAIL_RESPONSE ){
            this.mView.stopProgressDialog();
            this.mView.setDuplicateResult( false );
            this.mView.showResult(this.mContext.getString(R.string.signin_email_already_use));
        }
    }

    @Override
    public void onErrorOccured(int code, String message) {
        if (code == UserInfoModel.JOIN_MEMBER_RESPONSE || code == UserInfoModel.SET_ACCOUNT_RESPONSE) {
            this.mView.stopProgressDialog();
            this.mView.showResult(this.mContext.getString(R.string.signin_setuserinfo_error));
        }else if( code == UserInfoModel.USABLE_EMAIL_RESPONSE ){
            this.mView.stopProgressDialog();
            this.mView.setDuplicateResult( false );
            this.mView.showResult(this.mContext.getString(R.string.signin_email_duplicate_error));

        }
    }

    @Override
    public void onServiceResultReceived(int what, Bundle data) {

    }

    @Override
    public void serviceInitializeCompleted() {

    }

    private void setAutoLogin( boolean autoLogin ){
        SharedPreferences pref = this.mContext.getSharedPreferences( "fitmateservice" , this.mContext.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean( UserInfo.AUTO_LOGIN_KEY, autoLogin);
        editor.commit();
    }

    @Override
    public void deviceDisconnected(BluetoothGatt gatt) {

    }

    @Override
    public void deviceInitializedErrorOccured(String errorMessage) {

    }

    @Override
    public void deviceInitializedTimeout() {

    }

    @Override
    public void initializeCompleted() {

        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    mProtocolManager.onLED();
                } catch (FileReadException e) {
                    e.printStackTrace();
                }

                try {
                    mProtocolManager.setCurrentTime();
                }catch( FileReadException e) {
                    Log.e(TAG, "시간을 설정하는 중에 오류가 발생하였습니다.");
                }

                try {
                    mProtocolManager.offLED();
                } catch (FileReadException e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }

    private void setInitExecute(boolean initExecute){
        SharedPreferences pref = this.mContext.getSharedPreferences( "fitmateservice" , this.mContext.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean( LoginPresenter.INIT_EXECUTE_KEY , initExecute);
        editor.commit();
    }

    private void setSettedUserInfo(boolean settedUserInfo){
        SharedPreferences pref = this.mContext.getSharedPreferences( "fitmateservice" , this.mContext.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean( LoginPresenter.SET_USERINFO_KEY , settedUserInfo);
        editor.commit();
    }
}
