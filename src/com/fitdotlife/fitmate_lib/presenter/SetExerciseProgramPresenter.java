package com.fitdotlife.fitmate_lib.presenter;

import android.content.Context;
import android.os.Messenger;

import com.fitdotlife.fitmate_lib.iview.ISetExerciseProgramView;
import com.fitdotlife.fitmate.model.ExerciseProgramModel;
import com.fitdotlife.fitmate_lib.object.ExerciseProgram;

public class SetExerciseProgramPresenter
{
	private Context mContext = null;
	private ISetExerciseProgramView mView = null;
    private ExerciseProgramModel mExerciseProgramModel = null;
    private Messenger mMessenger = null;
	
	public SetExerciseProgramPresenter(Context context, ISetExerciseProgramView view)
	{
		this.mContext = context;
		this.mView = view;
        this.mExerciseProgramModel = new ExerciseProgramModel(context );
	}

    public void createUserExerciseProgram(ExerciseProgram exerciseProgramSNU){
        this.mExerciseProgramModel.createUserExerciseProgram(exerciseProgramSNU);
    }

}
