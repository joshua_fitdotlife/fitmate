package com.fitdotlife.fitmate;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseAnalyzer;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.FatAndCarbonhydrateConsumtion;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.StrengthInputType;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.UserInfoForAnalyzer;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.WearingLocation;
import com.fitdotlife.fitmate_lib.customview.AppliedProgramLayout;
import com.fitdotlife.fitmate_lib.customview.HomeCalDisView;
import com.fitdotlife.fitmate_lib.customview.HomeDayScoreChartView;
import com.fitdotlife.fitmate_lib.customview.HomeWeekScoreChartView;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.http.ActivityService;
import com.fitdotlife.fitmate_lib.http.FriendService;
import com.fitdotlife.fitmate_lib.http.NetworkClass;
import com.fitdotlife.fitmate_lib.http.RestHttpErrorHandler;
import com.fitdotlife.fitmate_lib.http.UserInfoService;
import com.fitdotlife.fitmate_lib.object.DayActivity_Rest;
import com.fitdotlife.fitmate_lib.object.ExerciseProgram;
import com.fitdotlife.fitmate_lib.object.UserFriend;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.service.util.Utils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.rest.spring.annotations.RestService;
import org.apache.log4j.Log;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

@EActivity(R.layout.activity_friend_home)
public class FriendHomeActivity extends Activity implements View.OnTouchListener {

    private final int ANIMATION_DURATION = 10000;
    private final int PagerAnimationDuration = 4000;

    private AppliedProgramLayout aplProgramInfo = null;
    private HomeDayScoreChartView dayScoreChart = null;
    private HomeWeekScoreChartView weekScoreChart = null;
    private UserInfo mUserInfo = null;
    private UserFriend mUserFriend = null;

    private FitmateDBManager dbManager;
    private PagerRunnable pagerRunnable = null;
    private ViewPagerAdapter mPagerAdapter = null;
    private String mTodayText = null;

    @ViewById(R.id.vp_activity_friend_home)
    ViewPager mPager;

    @ViewById(R.id.tv_activity_friend_home_day_score)
    TextView tvDayScore;

    @ViewById(R.id.tv_activity_friend_home_extra_score)
    TextView tvExtraScore;

    @ViewById(R.id.tv_activity_friend_home_week_score_result)
    TextView tvWeekScoreResult;

    @ViewById(R.id.tv_activity_friend_home_today_score_result)
    TextView tvTodayScoreResult;

    @ViewById(R.id.tv_activity_friend_home_week_score_more)
    TextView tvMoreScoreResult;

    @ViewById(R.id.tv_activity_friend_home_extra_score_result)
    TextView tvExtraScoreResult;

    @ViewById(R.id.tv_activity_friendhome_fatburn)
    TextView tvFatBurn;

    @ViewById(R.id.tv_activity_friendhome_carbonBurn)
    TextView tvCarbonBurn;

    @RestService
    ActivityService activityServiceClient;

    @RestService
    UserInfoService userInfoServiceClient;

    @RestService
    FriendService friendServiceClient;

    @Bean
    RestHttpErrorHandler restErrorHandler;

    @AfterViews
    void onInit(){

        activityServiceClient.setRootUrl(NetworkClass.baseURL + "/api");
        //activityServiceClient.setRestErrorHandler(restErrorHandler);
        userInfoServiceClient.setRootUrl(NetworkClass.baseURL + "/api");
        userInfoServiceClient.setRestErrorHandler( restErrorHandler );
        friendServiceClient.setRootUrl(NetworkClass.baseURL + "/api");
        friendServiceClient.setRestErrorHandler( restErrorHandler );

        dbManager = new FitmateDBManager(this);
        this.mUserInfo = dbManager.getUserInfo();

        FriendActivityManager.getInstance().addActivity(this);
        this.aplProgramInfo = (AppliedProgramLayout) this.findViewById(R.id.apl_activity_friend_home_appliedprogram_info);

        this.mPager.setPageTransformer(true, new PagerTransformer() );
        this.mPager.setOnTouchListener(this);

        try {
            Field mScroller;
            mScroller = ViewPager.class.getDeclaredField("mScroller");
            mScroller.setAccessible(true);
            //Interpolator interpolator = AnimationUtils.loadInterpolator(this , android.R.anim.overshoot_interpolator);
            AccelerateOvershootInterpolator interpolator = new AccelerateOvershootInterpolator(2.0f,  1.0f);
            FixedSpeedScroller scroller = new FixedSpeedScroller( mPager.getContext(), interpolator );
            // scroller.setFixedDuration(5000);
            mScroller.set(mPager, scroller);
        } catch (NoSuchFieldException e) {
        } catch (IllegalArgumentException e) {
        } catch (IllegalAccessException e) {
        }

        this.pagerRunnable = new PagerRunnable();

        Calendar todayCalendar = Calendar.getInstance();
        todayCalendar.setTimeInMillis( System.currentTimeMillis() );
        String dateString = null;
        SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat( this.getString(R.string.home_calorie_date_text) , Locale.getDefault() );
        dateString =  mSimpleDateFormat.format(todayCalendar.getTime());
        this.mTodayText =  dateString;


        this.dayScoreChart = (HomeDayScoreChartView) this.findViewById(R.id.cv_activity_friend_home_day_score_chart);
        this.weekScoreChart = (HomeWeekScoreChartView) this.findViewById(R.id.cv_activity_friend_home_week_score_chart);

        Intent intent = this.getIntent();
        this.mUserFriend = intent.getParcelableExtra("UserFriend");
        if( mUserFriend != null )
        {
            getDayActivity(mUserFriend.getEmail());

        }else{
            String friendEmail = intent.getStringExtra("FriendEmail");
            //친구 정보를 가져온다.
            getUserFriend(friendEmail);
        }
    }

    @UiThread
    void displayActionBar(){
        View actionBarView = this.findViewById(R.id.ic_activity_friend_home_actionbar);
        actionBarView.setBackgroundColor(this.getResources().getColor(R.color.fitmate_green));
        TextView tvBarTitle = (TextView) actionBarView.findViewById( R.id.tv_custom_action_bar_title );
        tvBarTitle.setText(mUserFriend.getName() + " " + this.getString(R.string.activity_bar_title));

        ImageView imgBack = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_left);
        imgBack.setVisibility(View.GONE);

        ImageView mImgRight = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_right);
        mImgRight.setImageResource(R.drawable.profile);
        mImgRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(FriendHomeActivity.this, FriendProfileActivity_.class);
                intent.putExtra("UserFriend", mUserFriend);
                startActivity(intent);

            }
        });
        mImgRight.setOnTouchListener(this);
    }


    @Background
    void getUserFriend(String friendEmail){

        if(mUserFriend == null) {
            Log.e("fitmate", "frinedEmail : " + friendEmail);
        }

        mUserFriend = friendServiceClient.getUsersFriendsInfo(mUserInfo.getEmail(), friendEmail);

        if(mUserFriend == null) {
            Log.e("fitmate", "NULL");
        }

        getDayActivity(friendEmail);

    }

    @Background
    void getDayActivity( String friendEmail )
    {

        //오늘 날짜를 가져온다.
        Calendar todayCalendar = Calendar.getInstance();
        todayCalendar.setTimeInMillis(System.currentTimeMillis());
        String strTodayDate =  todayCalendar.get(Calendar.YEAR) + "-" + String.format("%02d", todayCalendar.get(Calendar.MONTH) + 1) + "-" + String.format("%02d", todayCalendar.get(Calendar.DATE) );
        DayActivity_Rest dayActivity = activityServiceClient.getDayActivity( mUserInfo.getEmail(), friendEmail, strTodayDate);
        displayActivityInfo(dayActivity);
    }

    @UiThread
    void displayActivityInfo( DayActivity_Rest dayActivity )
    {
        displayActionBar();
        this.aplProgramInfo.setAppliedExerciseProgram(dbManager.getUserExerciProgram(mUserFriend.getExerciseProgramID()));

        //서버로부터 친구가 사용하는 운동 프로그램을 가져온다.
        ExerciseProgram appliedProgram ;
        if(dayActivity!= null){
            appliedProgram = dbManager.getUserExerciProgram(dayActivity.getExerciseProgramID());
        }
        else{
            appliedProgram= dbManager.getUserExerciProgram( mUserFriend.getExerciseProgramID());
        }

        displayExerciseProgram( appliedProgram );
        displayDayAchievement(appliedProgram, dayActivity );
        displayWeekAchievement(appliedProgram, dayActivity );
        displayFatBurn(dayActivity);
        displayDayCalorie(dayActivity);
    }

    private void displayFatBurn(DayActivity_Rest dayActivity ){

        float  fat =0;
        float carbon =0;

        if(dayActivity!=null)
        {

            UserInfoForAnalyzer userInfoForAnalyzer = Utils.getUserInfoForAnalyzer(mUserFriend.getBirthDate(), mUserFriend.isSex(), mUserFriend.getHeight(), mUserFriend.getWeight(), mUserFriend.isSi());

            com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseProgram appliedProgram = dbManager.getExerciseProgramForAnalyzer( mUserFriend.getExerciseProgramID() );
            ExerciseAnalyzer analyzer = new ExerciseAnalyzer(appliedProgram, userInfoForAnalyzer, WearingLocation.getWearingLocation( mUserFriend.getWearingLocation() ) , dayActivity.getDataSavingInterval() );

            byte[] byteMetArray = Base64.decode(dayActivity.getMetArray(), Base64.NO_WRAP);

            float[] metArray = null;
            if (byteMetArray != null) {
                metArray = DayActivity_Rest.convertByteArrayToFloatArray(byteMetArray);
            }

            List<Float> metList= new ArrayList<>();
            for(float f:metArray){
                metList.add(f);
            }
            FatAndCarbonhydrateConsumtion fatBurn = analyzer.getFatandCarbonhydrate(metList, dayActivity.getDataSavingInterval());
            fat =fatBurn.getFatBurned();
            carbon =fatBurn.getCarbonhydrateBurned();
        }

        tvFatBurn.setText(String.format("%.1f g", fat));
        tvCarbonBurn.setText(String.format("%.1f g", carbon));
    }

    public void displayExerciseProgram(ExerciseProgram exerciseProgram) {
        this.aplProgramInfo.setAppliedExerciseProgram(exerciseProgram);
    }

    public void displayDayAchievement( ExerciseProgram program , DayActivity_Rest dayActivity){

        String rateText = "";
        String statusText = "";
        String valueUnit = "";
        String titleText = "";
        int rangeFrom = 0;
        int rangeTo = 0;
        double value = 0;
        int extraScore = 0;

        StrengthInputType strengthType = StrengthInputType.getStrengthType(program.getCategory());
        if( strengthType.equals(StrengthInputType.CALORIE) || strengthType.equals( StrengthInputType.CALORIE_SUM ) ) {
            rangeFrom = (int) program.getStrengthFrom();
            rangeTo = (int) program.getStrengthTo();
            if (dayActivity != null) {
                value = dayActivity.getCalorieByActivity();
            }
            valueUnit  = this.getString(R.string.common_calorie);
            titleText = this.getString(R.string.home_today_bured_calorie);
        }else if( strengthType.equals(StrengthInputType.VS_BMR) ){

            UserInfoForAnalyzer userInfoForAnalyzer = Utils.getUserInfoForAnalyzer( mUserFriend.getBirthDate() , mUserFriend.isSex() , mUserFriend.getHeight() , mUserFriend.getWeight() , mUserFriend.isSi() );

            rangeFrom = (int) (userInfoForAnalyzer.getBMR() * ( program.getStrengthFrom() / 100 ));
            rangeTo = (int) (userInfoForAnalyzer.getBMR() * ( program.getStrengthTo() / 100 ));
            if( dayActivity != null ) {
                value = dayActivity.getCalorieByActivity();
            }
            valueUnit  = this.getString(R.string.common_calorie);
            titleText = this.getString(R.string.home_today_bured_calorie);

        }else{
            rangeFrom = program.getMinuteFrom();
            rangeTo = program.getMinuteTo();
            if( dayActivity != null ) {
                value = dayActivity.getAchieveOfTarget();
            }
            valueUnit  = this.getString(R.string.common_minute);
            titleText = this.getString(R.string.home_activity_time);
        }

        if(value < rangeFrom){
            if(strengthType.equals( StrengthInputType.CALORIE_SUM) || strengthType.equals( StrengthInputType.CALORIE ) || strengthType.equals(StrengthInputType.VS_BMR)){
                if(dayActivity != null)
                    extraScore = dayActivity.getExtraScore();
                statusText = String.format( this.getString(R.string.home_more_calorie) , (int)( rangeFrom - value ));
                rateText = String.format( this.getString(R.string.home_today_gain_point) , extraScore );
            }else{
                if(dayActivity != null)
                    extraScore = dayActivity.getExtraScore();
                statusText = String.format( this.getString(R.string.home_more_exercise) , (int)( rangeFrom - value ));
                rateText = String.format( this.getString(R.string.home_today_gain_point) , extraScore );
            }
        }else if( value >= rangeFrom ){
            statusText = this.getString(R.string.home_achieve_goal);
            rateText = this.getString(R.string.home_conguraturation);
        }

        this.dayScoreChart.setRange( rangeFrom , rangeTo );
        this.dayScoreChart.setValueUnit(valueUnit);
        this.dayScoreChart.setAchievementValue( value );
        this.dayScoreChart.setTitleText( titleText );
        this.dayScoreChart.setStatusText( statusText );
        this.dayScoreChart.setRateText( rateText );
        this.dayScoreChart.startAnimation();
    }

    public void displayWeekAchievement( ExerciseProgram program , DayActivity_Rest dayActivity  )
    {


        int predayScore = 0;
        int todayScore = 0;
        boolean isAchieveMax = false;
        int possibleMaxScore = 0;
        int possibleTimes = 0;
        int possibleValue = 0;

        if(dayActivity != null) {
            predayScore = dayActivity.getPredayScore();
            todayScore = dayActivity.getTodayScore();
            isAchieveMax = dayActivity.isAchieveMax();
            possibleMaxScore = dayActivity.getPossibleMaxscore();
            possibleTimes = dayActivity.getPossibleTimes();
            possibleValue = dayActivity.getPossibleValue();
        }

        StrengthInputType strengthType = StrengthInputType.getStrengthType(program.getCategory());

        this.weekScoreChart.setScore( predayScore , todayScore );
        this.weekScoreChart.setValueUnit(this.getString(R.string.common_point));
        this.weekScoreChart.startAnimation(this.ANIMATION_DURATION);

        //오늘 성취
        this.tvDayScore.setText(String.format(getString(R.string.home_today_achievement_point), todayScore));
        //이번주 성취
        this.tvExtraScore.setText( String.format( this.getString(R.string.home_week_achievement_point) , ( predayScore + todayScore ) ) );

        //this.tvTodayScoreResult.setText( "오늘은 " + todayScore + "점을 획득하셨군요." );
        this.tvTodayScoreResult.setText( String.format( this.getString(R.string.home_today_got_point), todayScore ) );
        //이번주 운동점수는 총 54점입니다.
        //this.tvWeekScoreResult.setText( "오늘까지 " + (predayScore + todayScore ) + "점을 달성했습니다." );
        this.tvWeekScoreResult.setText(String.format(this.getString(R.string.home_week_score_total), predayScore + todayScore));
        this.tvMoreScoreResult.setText(String.format(this.getString(R.string.home_get_more_point), 100 - (predayScore + todayScore)));

        int maxScore = 100;
        if( !isAchieveMax ){ maxScore = possibleMaxScore; }
        String extraText = null;

        if( strengthType.equals( StrengthInputType.CALORIE ) || strengthType.equals(StrengthInputType.VS_BMR) ){
            extraText = String.format( this.getString( R.string.home_calorie_attain_high_point ) , possibleTimes + this.getString(R.string.common_day) + " " + possibleValue + "kcal"  ,  maxScore );
        }else if( strengthType.equals( StrengthInputType.CALORIE_SUM ) ){
            extraText = String.format( this.getString( R.string.home_calorie_attain_high_point ) , possibleValue + "kcal"  , maxScore );
        }else {
            extraText = String.format( this.getString( R.string.home_exercise_attain_high_point ) , possibleTimes + this.getString(R.string.common_times) + " " + possibleValue + this.getString(R.string.common_minute)  , maxScore);
        }
        this.tvExtraScoreResult.setText(extraText);
    }

    public void displayDayCalorie( DayActivity_Rest dayActivity ) {

        double calorie = 0;
        double distance = 0;

        if(dayActivity != null) {

            byte[] byteMetArray = Base64.decode(dayActivity.getMetArray(), Base64.NO_WRAP);

            float[] metArray = null;
            if (byteMetArray != null) {
                metArray = DayActivity_Rest.convertByteArrayToFloatArray(byteMetArray);
            }

            List<Float> metList= new ArrayList<>();
            for(float f:metArray){
                metList.add(f);
            }

            calorie = dayActivity.getCalorieByActivity();
            distance = ExerciseAnalyzer.getDistanceFromMETArray_over2MET( metList , dayActivity.getDataSavingInterval() );

            metList.clear();
            metList = null;
            metArray = null;
        }

        this.mPagerAdapter =  new ViewPagerAdapter(this.getApplicationContext(), calorie, distance , mTodayText );
        this.mPager.setAdapter(mPagerAdapter);
        this.mPager.setCurrentItem(2* 1000 );

        pagerRunnable.setPosition(mPager.getCurrentItem());
        this.mPager.postDelayed(pagerRunnable, PagerAnimationDuration);
    }

    @Click(R.id.btn_activity_friend_home_detail_activity)
    void detailActivityClick(){
        FriendActivityInfoActivity_.intent(this).extra("UserFriend", mUserFriend ).start();
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {

        if(view.getId() == R.id.vp_activity_friend_home) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                mPager.removeCallbacks(pagerRunnable);
            } else if (event.getAction() == MotionEvent.ACTION_UP) {
                pagerRunnable.setPosition(mPager.getCurrentItem());
                mPager.postDelayed(pagerRunnable, PagerAnimationDuration);
            }
        }else{
            ImageView actionbarImageView = (ImageView) view;
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                actionbarImageView.setColorFilter(this.getResources().getColor(R.color.selected_colorfilter) , PorterDuff.Mode.DARKEN );
            } else if (event.getAction() == MotionEvent.ACTION_UP) {
                actionbarImageView.setColorFilter(null);
            }
        }

        return false;
    }

    //칼로리와 이동거리 뷰 관련....
    private class ViewPagerAdapter extends PagerAdapter {

        private LayoutInflater mInflater;
        private double mCalorie;
        private double mDistance;
        private String mCalorieText;
        private String mTodayText;
        private String mCalorieUnitText;
        private String mCalorieTitle;
        private String mDistanceTitle;
        private Drawable mCalorieDrawable;
        private Drawable mDistanceDrawable;
        private FitmateDBManager mDBManager;
        private View[] arrView = new View[3];

        public ViewPagerAdapter(Context applicationContext, double calorie, double distance, String todayText)
        {
            super();
            this.mInflater = LayoutInflater.from(applicationContext);
            this.mCalorie = calorie;
            this.mDistance = distance;
            this.mTodayText = todayText;
            this.mCalorieUnitText = applicationContext.getString(R.string.home_burned_calorie_unit);
            this.mDistanceTitle = getString(R.string.home_distance);
            this.mCalorieTitle = getString(R.string.home_burned_calorie);
            this.mCalorieDrawable = getResources().getDrawable(R.drawable.home_bg_kcal);
            this.mDistanceDrawable = getResources().getDrawable(R.drawable.home_bg_distance);
            this.mDBManager = new FitmateDBManager(applicationContext);

            setCalorieText();
        }

        private void setCalorieText(){
            if( mCalorie < 1 ){
                mCalorie = (mCalorie * 10d )/10d;
                mCalorieText = (int)mCalorie + "";
            } else if(mCalorie == 1){
                mCalorieText = (int)mCalorie + "";
            } else if(mCalorie > 1) {
                mCalorieText = (int)mCalorie + "";
            }
        }

        @Override
        public int getCount() {
            return Integer.MAX_VALUE;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View view = null;

            int realPos = ( position % 2 ) + 1;

            view = mInflater.inflate(R.layout.child_activity_home_hcd , null);
            HomeCalDisView hcdView = (HomeCalDisView) view.findViewById(R.id.child_activity_home_hcd);
            TextView tvDate = (TextView) view.findViewById(R.id.tv_activity_home_caldis_date);
            TextView tvTitle = (TextView) view.findViewById(R.id.tv_activity_home_caldis_title);
            TextView tvValue = (TextView) view.findViewById(R.id.tv_activity_home_caldis_value);
            TextView tvValueUnit = (TextView) view.findViewById(R.id.tv_activity_home_caldis_value_unit);


            if( realPos == 1 ){
                hcdView.setImage(mCalorieDrawable );
                tvTitle.setText( mCalorieTitle);
                tvDate.setText( mTodayText );
                tvValue.setText( mCalorieText );
                tvValueUnit.setText( mCalorieUnitText );
                this.arrView[realPos] = view;

            }
            else if(realPos == 2 )
            {
                String distanceText = null;
                String distanceUnitText = null;
                hcdView.setImage(mDistanceDrawable);
                tvTitle.setText( mDistanceTitle);
                tvDate.setText(mTodayText);
                UserInfo userInfo = mDBManager.getUserInfo();

                if(mUserFriend.isSi() ){

                    if( mDistance >= 1 ) {
                        distanceText = String.format("%.2f", mDistance);
                        distanceUnitText = getString(R.string.common_kilometer);
                    }else {
                        distanceText = String.format("%d", Math.round(mDistance * 1000));
                        distanceUnitText = getString(R.string.common_meter);
                    }

                }else{

                    //킬로미터를 야드로 변환한다.
                    double yard = mDistance * 1093.61;
                    if( yard >= 1760 ){
                        distanceText = String.format("%.2f",  yard / 1760 );
                        distanceUnitText = getString(R.string.common_mile);
                    }else{
                        distanceText = String.format("%d", Math.round(yard));
                        distanceUnitText = getString(R.string.common_yard);
                    }
                }

                tvValue.setText( distanceText );
                tvValueUnit.setText( distanceUnitText );

                this.arrView[realPos] =view;
            }

            container.addView(view);

            return view;
        }
    }

    private class PagerRunnable implements  Runnable
    {

        int mPosition = 0;

        @Override
        public void run() {

            mPager.setCurrentItem(mPosition);
            mPosition--;
            mPager.postDelayed(pagerRunnable , PagerAnimationDuration );

        }

        public void setPosition( int position ){
            this.mPosition = position;
        }
    }

    private class PagerTransformer implements ViewPager.PageTransformer
    {

        private static final float MIN_SCALE = 0.75f;

        public void transformPage(View view, float position) {
            int pageWidth = view.getWidth();

            if (position < -1) { // [-Infinity,-1)
                // This page is way off-screen to the left.
                view.setAlpha(0);

            } else if (position <= 0) { // [-1,0]
                // Use the default slide transition when moving to the left page
                view.setAlpha(1);
                view.setTranslationX(0);
                view.setScaleX(1);
                view.setScaleY(1);

            } else if (position <= 1) { // (0,1]
                // Fade the page out.
                view.setAlpha(1 - position);

                // Counteract the default slide transition
                //view.setTranslationX(pageWidth * -position);
                view.setTranslationX((position) * (pageWidth / 4));

                // Scale the page down (between MIN_SCALE and 1)
                float scaleFactor = MIN_SCALE
                        + (1 - MIN_SCALE) * (1 - Math.abs(position));
                view.setScaleX(scaleFactor);
                view.setScaleY(scaleFactor);

            } else { // (1,+Infinity]
                // This page is way off-screen to the right.
                view.setAlpha(0);
            }
        }
    }
}
