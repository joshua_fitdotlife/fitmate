package com.fitdotlife.fitmate;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Environment;
import android.view.View;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.http.NetworkClass;
import com.fitdotlife.fitmate_lib.http.VersionCheckService;
import com.fitdotlife.fitmate_lib.iview.IMyProfileView;
import com.fitdotlife.fitmate_lib.key.GenderType;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.presenter.MyProfilePresenter;
import com.fitdotlife.fitmate_lib.util.Utils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringRes;
import org.androidannotations.rest.spring.annotations.RestService;
import org.apache.log4j.Log;
import org.springframework.web.client.RestClientException;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;

@EActivity(R.layout.activity_setting)
public class SettingActivity extends Activity implements IMyProfileView{

    public static final String ACTION_USERINFO_CHANGE = "com.fitdotlife.fitmate.USERINFO_CHANGE";

    private UserInfo mUserInfo;
    private FitmateDBManager mDBManager;
    private MyProfilePresenter mPresenter = null;
    private Activity mActivity = null;

    private final int USERINFO_REQUEST_CODE = 0;
    private final int USERINFO_CHANGEDEVICE_CODE = 1;
    private final int USERINFO_CHANGELOCATION_CODE = 2;

    private boolean isShowing = false;
    private ProgressDialog pDialog;

    @ViewById(R.id.tv_activity_setting_name)
    TextView tvName;

    @ViewById(R.id.tv_activity_setting_introself)
    TextView tvIntroSelf;

    @ViewById(R.id.tv_activity_setting_birthdate)
    TextView tvBirthDate;

    @ViewById(R.id.tv_activity_setting_sex)
    TextView tvSex;

    @ViewById(R.id.tv_activity_setting_height)
    TextView tvHeight;

    //@ViewById(R.id.tv_activity_setting_height_unit)
    //TextView tvHeightUnit;

    @ViewById(R.id.tv_activity_setting_weight)
    TextView tvWeight;

    //@ViewById(R.id.tv_activity_setting_weight_unit)
    //TextView tvWeightUnit;

    @ViewById(R.id.tv_activity_setting_wearinglocation)
    TextView tvWearingLocation;

    @ViewById(R.id.tv_activity_setting_connectfitmeter)
    TextView tvConnectFitmeter;

    @StringRes(R.string.common_male)
    String strMale;

    @StringRes(R.string.common_female)
    String strFemale;

    @StringRes(R.string.setting_bar_title)
    String strSetting;

    @StringRes(R.string.common_connect_network)
    String strConnectNetwork;

    @ViewById(R.id.ic_activity_setting_actionbar)
    View actionBarView;

    @ViewById(R.id.rl_activity_setting_progress)
    RelativeLayout rlProgress;
    @RestService
    VersionCheckService versionCheckServiceClient;


    @Override
    protected void onDestroy() {
        super.onDestroy();

        mPresenter.close();

        Intent programChangeIntent = new Intent( ACTION_USERINFO_CHANGE );
        sendBroadcast(programChangeIntent);

    }

    @AfterViews
    void onInit(){

        displayActionBar();

        mDBManager = new FitmateDBManager(this);
        mUserInfo = mDBManager.getUserInfo();
        this.mPresenter = new MyProfilePresenter(this , this);

        this.mActivity = this;

        versionCheckServiceClient.setRootUrl(NetworkClass.baseURL + "/api");

        tvName.setText( mUserInfo.getName() );
        tvIntroSelf.setText(mUserInfo.getIntro());

        //생년월일이 있는지 확인한다.
        if( mUserInfo.getBirthDate() > 0 ) {
            long birthDataTimeMilliSecond = mUserInfo.getBirthDate();
            tvBirthDate.setText(this.getDateString(birthDataTimeMilliSecond));
        }

        //성별이 있는지 확인한다.
        if( !mUserInfo.getGender().equals( GenderType.NONE ) ) {
            if (mUserInfo.getGender() == GenderType.MALE) {
                tvSex.setText(strMale);
            } else if (mUserInfo.getGender() == GenderType.FEMALE) {
                tvSex.setText(strFemale);
            }
        }

        //Si가 있는지 확인한다.
        boolean isSi = mUserInfo.isSI();

        //키가 있는지 확인한다.
        if( mUserInfo.getHeight() > 0 ) {
            String[] valueArray = this.getValueArray( mUserInfo.getHeight() );
            tvHeight.setText( String.format("%s %s", valueArray[0] + "." + valueArray[1], UserInfo.getHeightUnit(isSi)) );
        }else{
            tvHeight.setText( this.getString(R.string.common_no_setting) );
        }
        //몸무게가 있는지 확인한다.
        if(mUserInfo.getWeight() > 0) {
            String[] valueArray = this.getValueArray( mUserInfo.getWeight() );
            tvWeight.setText( String.format("%s %s", valueArray[0] + "." + valueArray[1], UserInfo.getWeightUnit(isSi)) );
        }else{
            tvWeight.setText( this.getString(R.string.common_no_setting) );
        }
        //착용위치가 있는지 확인한다.
        if( mUserInfo.getWearAt() > -1 ) {
            if( mUserInfo.getDeviceAddress() != null && !mUserInfo.getDeviceAddress().equals("") && !mUserInfo.getDeviceAddress().equals("null") ) { //기기등록을 안했다면 기기 변경을 할 수 없다.
                tvWearingLocation.setText(getWearingLocationString(mUserInfo.getWearAt()));
            }else{
                tvWearingLocation.setText(this.getString(R.string.common_no_setting));
            }
        }else{
            tvWearingLocation.setText(this.getString(R.string.common_no_setting));
        }
    }

    private void displayActionBar( ){
        actionBarView.setBackgroundColor(this.getResources().getColor(R.color.fitmate_bar_gray));
        TextView tvBarTitle = (TextView) actionBarView.findViewById( R.id.tv_custom_action_bar_title );
        tvBarTitle.setText(strSetting);

        ImageView imgBack = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_left);
        imgBack.setVisibility(View.GONE);

        ImageView mImgRight = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_right);
        mImgRight.setVisibility(View.GONE);
    }

    @Click(R.id.rl_activity_setting_alarm)
    void alarmSettingClick(){
        if(Utils.isOnline(this) ) {
            AlarmSettingActivity_.intent(this).start();
        }else{
            Toast.makeText(this,strConnectNetwork,Toast.LENGTH_SHORT).show();
        }
    }

    @Click(R.id.ll_activity_setting_change_password)
    void changePasswordClick(){
        ChangePasswordDialog fpDialog = new ChangePasswordDialog(this, this.mUserInfo.getEmail());
        fpDialog.show();
    }

    private String getWearingLocationString( int wearingLocation ){
        String result = null;

        if( wearingLocation == 0 ){
            wearingLocation = 1;
        }

        result = this.getResources().getStringArray(R.array.weraing_location_string)[wearingLocation - 1];
        return result;
    }

    @Click(R.id.ll_activity_setting_userinfo)
    void userInfoClick(){

        Intent userinfoChangeIntent = new Intent(SettingActivity.this , UserInfoChangeActivity.class);
        userinfoChangeIntent.putExtra(UserInfo.EMAIL_KEY, this.mUserInfo.getEmail());
        userinfoChangeIntent.putExtra(UserInfo.NAME_KEY, this.mUserInfo.getName());
        userinfoChangeIntent.putExtra(UserInfo.HELLO_MESSAGE_KEY, this.mUserInfo.getIntro());
        userinfoChangeIntent.putExtra(UserInfo.BIRTHDATEKEY, this.mUserInfo.getBirthDate());
        userinfoChangeIntent.putExtra(UserInfo.SEX_KEY, this.mUserInfo.getGender().getValue());
        userinfoChangeIntent.putExtra(UserInfo.PROFILE_MAGE_KEY, this.mUserInfo.getAccountPhoto());

        this.startActivityForResult(userinfoChangeIntent, USERINFO_REQUEST_CODE);
    }

    @Click(R.id.rl_activity_setting_height)
    void heightClick(){

        if(mUserInfo.getHeight() == 0){
            showNoSettingDialog();
            return;
        }

        boolean heightUnit = mUserInfo.isSI();
        AlertDialog.Builder heightDialog = new AlertDialog.Builder(this);
        heightDialog.setTitle(this.getString(R.string.common_height));
        View numberPickerView = this.getLayoutInflater().inflate(R.layout.dialog_number_picker, null);
        final NumberPicker np = (NumberPicker) numberPickerView.findViewById(R.id.np_dialog_number_picker);
        np.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        final NumberPicker pp = (NumberPicker) numberPickerView.findViewById(R.id.np_dialog_point_picker);
        pp.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        final NumberPicker up = (NumberPicker) numberPickerView.findViewById(R.id.np_dialog_unit_picker);
        up.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        up.setDisplayedValues(new String[]{this.getString(R.string.common_centimeter), this.getString(R.string.common_ft)});

        float height = this.mUserInfo.getHeight();
        np.setMaxValue(300);
        np.setMinValue(1);
        pp.setMaxValue(9);
        pp.setMinValue(0);
        up.setMaxValue(1);
        up.setMinValue(0);

        String[] valueArray = this.getValueArray( height );
        np.setValue( Integer.parseInt( valueArray[0] ) );
        pp.setValue( Integer.parseInt( valueArray[1] ) );

        if( heightUnit )
        {
            up.setValue(0);
        }else{

            up.setValue(1);
        }
        up.setTag( height );

        heightDialog.setView(numberPickerView);

        up.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                boolean heightSi = (newVal == 0);
                float tempHeight = (float) up.getTag();

                if (heightSi) {
                    tempHeight = com.fitdotlife.fitmate_lib.service.util.Utils.ft_to_cm(tempHeight);
                } else {
                    tempHeight = com.fitdotlife.fitmate_lib.service.util.Utils.cm_to_ft(tempHeight);
                }

                String[] valueArray = getValueArray(tempHeight );
                np.setValue( Integer.parseInt( valueArray[0] ) );
                pp.setValue( Integer.parseInt( valueArray[1] ) );

                up.setTag(tempHeight);
            }
        });

        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                up.setTag( (float) (((np.getValue() * 10d) + pp.getValue()) / 10d) );
            }
        });

        pp.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                up.setTag( (float) (((np.getValue() * 10d) + pp.getValue()) / 10d) );
            }
        });

        heightDialog.setNegativeButton(this.getString(R.string.common_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        heightDialog.setPositiveButton(this.getString(R.string.common_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                boolean heightSi = up.getValue() == 0;
                boolean isSi = mUserInfo.isSI();
                float tempWeight = mUserInfo.getWeight();
                String weightUnitString = "";

                if (heightSi != isSi) {
                    //몸무게를 변환한다.
                    if (heightSi) {
                        //파운드에서 킬로그램으로 변환한다.
                        tempWeight = com.fitdotlife.fitmate_lib.service.util.Utils.lb_to_kg( tempWeight );
                        weightUnitString = getString(R.string.common_kilogram);
                    } else {
                        //킬로그램에서 파운드로 변환한다.
                        tempWeight = com.fitdotlife.fitmate_lib.service.util.Utils.kg_to_lb(tempWeight);
                        weightUnitString = getString(R.string.common_pound);
                    }

                    String[] weightValueArray = getValueArray( tempWeight );
                    tvWeight.setText(String.format("%s %s", weightValueArray[0] + "." + weightValueArray[1] , weightUnitString));

                }
                mUserInfo.setHeight((float) up.getTag());
                mUserInfo.setWeight(tempWeight);
                mUserInfo.setIsSI(heightSi);
                mPresenter.modifyUserInfo(mUserInfo);

                String[] heightValueArray = getValueArray( mUserInfo.getHeight() );
                tvHeight.setText(String.format("%s %s", heightValueArray[0] + "." + heightValueArray[1] , up.getDisplayedValues()[up.getValue()]));
                mPresenter.ChangeUserInfo();

            }
        });
        heightDialog.show();
    }

    @Click(R.id.rl_activity_setting_weight)
    void weightClick(){

        if( mUserInfo.getWeight() == 0 ){
            showNoSettingDialog();
            return;
        }

        boolean weightUnit = mUserInfo.isSI();
        final AlertDialog.Builder weightDialog = new AlertDialog.Builder(this);
        weightDialog.setTitle(this.getString(R.string.common_weight));

        View numberPickerViews = this.getLayoutInflater().inflate(R.layout.dialog_number_picker, null);
        final NumberPicker nps = (NumberPicker) numberPickerViews.findViewById(R.id.np_dialog_number_picker);
        nps.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        final NumberPicker pps = (NumberPicker) numberPickerViews.findViewById(R.id.np_dialog_point_picker);
        pps.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        final NumberPicker ups = (NumberPicker) numberPickerViews.findViewById(R.id.np_dialog_unit_picker);
        ups.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        ups.setDisplayedValues(new String[]{this.getString(R.string.common_kilogram), this.getString(R.string.common_pound)});

        float weight = this.mUserInfo.getWeight();
        nps.setMaxValue(500);
        nps.setMinValue(1);
        pps.setMaxValue(9);
        pps.setMinValue(0);
        ups.setMaxValue(1);
        ups.setMinValue(0);

        String[] valueArray = this.getValueArray( weight );
        nps.setValue(Integer.parseInt(valueArray[0]));
        pps.setValue(Integer.parseInt(valueArray[1]));

        if( weightUnit )
        {
            ups.setValue(0);
        }else{

            ups.setValue(1);
        }
        ups.setTag( weight );
        weightDialog.setView(numberPickerViews);

        ups.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {

                boolean weightSi = (newVal == 0);
                float tempWeight = (float) ups.getTag();

                //몸무게를 변환한다.
                if (weightSi) {
                    //파운드에서 킬로그램으로 변환한다.
                    tempWeight = com.fitdotlife.fitmate_lib.service.util.Utils.lb_to_kg(tempWeight);
                } else {
                    //킬로그램에서 파운드로 변환한다.
                    tempWeight = com.fitdotlife.fitmate_lib.service.util.Utils.kg_to_lb(tempWeight);
                }

                String[] valueArray = getValueArray( tempWeight );
                nps.setValue(Integer.parseInt(valueArray[0]));
                pps.setValue(Integer.parseInt(valueArray[1]));

                ups.setTag(tempWeight);
            }
        });

        nps.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                ups.setTag( (float) (((nps.getValue() * 10d) + pps.getValue()) / 10d) );
            }
        });

        pps.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                ups.setTag( (float) (((nps.getValue() * 10d) + pps.getValue()) / 10d) );
            }
        });

        weightDialog.setNegativeButton(this.getString(R.string.common_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        weightDialog.setPositiveButton(this.getString(R.string.common_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                boolean weightSi = ( ups.getValue() == 0 );
                boolean isSi = mUserInfo.isSI();
                float tempHeight = mUserInfo.getHeight();
                String heightUnitString = "";

                if (weightSi != isSi) {
                    //키를 변환한다.
                    if (weightSi) {
                        //인치에서 센티미터로 변환
                        tempHeight = com.fitdotlife.fitmate_lib.service.util.Utils.ft_to_cm(tempHeight);
                        heightUnitString = getString(R.string.common_centimeter);
                    } else {
                        //센티미터에서 인치로 변환한다.
                        tempHeight = com.fitdotlife.fitmate_lib.service.util.Utils.cm_to_ft(tempHeight);
                        heightUnitString = getString(R.string.common_ft);
                    }

                    String[] heightValueArray = getValueArray( tempHeight );
                    tvHeight.setText(String.format("%s %s", heightValueArray[0] + "." + heightValueArray[1] , heightUnitString ));

                }
                mUserInfo.setWeight((Float) ups.getTag());
                mUserInfo.setHeight(tempHeight);
                mUserInfo.setIsSI(weightSi);
                mPresenter.modifyUserInfo(mUserInfo);

                String[] weightValueArray = getValueArray( mUserInfo.getWeight() );
                tvWeight.setText(String.format("%s %s", weightValueArray[0] + "." + weightValueArray[1], ups.getDisplayedValues()[ups.getValue()]));
                mPresenter.ChangeUserInfo();

            }
        });

        weightDialog.show();
    }

    @Click(R.id.rl_activity_setting_wearinglocation)
    void wearingLocationClick(){
        if( mUserInfo.getDeviceAddress() != null && !mUserInfo.getDeviceAddress().equals("") && !mUserInfo.getDeviceAddress().equals("null") ) { //기기등록을 안했다면 기기 변경을 할 수 없다.
            showProgress();
            if (!isShowing) {
                isShowing = true;
                mPresenter.requestStopService(MyProfilePresenter.STOP_FOR_CHANGE_WEARINGLOCATION);
            }
        }else{
            showNoSettingDialog();
        }
    }

    @Click(R.id.rl_activity_setting_changefitmeter)
    void changeFitmeterClick(){
        if( mUserInfo.getDeviceAddress() != null && !mUserInfo.getDeviceAddress().equals("") && !mUserInfo.getDeviceAddress().equals("null") ) { //기기등록을 안했다면 기기 변경을 할 수 없다.
            showProgress();
            if (!isShowing) {
                isShowing = true;
                mPresenter.requestStopService(MyProfilePresenter.STOP_FOR_CHANGE_FITMETER);
            }
        }else{
            showNoSettingDialog();
        }
    }

    private void showProgress(){
        rlProgress.setVisibility( View.VISIBLE );
    }

    @Override
    public void dismissProgress()
    {
        rlProgress.setVisibility( View.INVISIBLE );
    }

    @Click(R.id.rl_activity_setting_firmwareupdate)
    void softwareUpdateClick(){
        if( mUserInfo.getDeviceAddress() != null && !mUserInfo.getDeviceAddress().equals("") && !mUserInfo.getDeviceAddress().equals("null") ) { //기기등록을 안했다면 기기 변경을 할 수 없다.

            if( !Utils.isOnline(this) ){
                Toast.makeText( this , this.getString(R.string.common_connect_network) , Toast.LENGTH_SHORT ).show();
                return;
            }

            //현재 기기의 버전을 가져온다.
            String firmwareVersion = this.getFirmwareVersionSharedPreference(this);

            //현재 버전이 소프트웨어 업데이트가 필요한 버전인지 확인한다.
            checkDownloadFirmware( firmwareVersion );


            //폄웨어를 다운로드 받는다.
            //서비스를 중단한다.
            //기기를 검색하고 연결한다.
            //부트모드로 진입한다.
            //블루투스를 껏다가 다시 켠다.
            //기기 검색을 한다.
            //다운 받은 파일을 기기에 다운로드한다.
            //다운 받은 파일을 삭제한다.

        }else{
            showNoSettingDialog();
        }
    }

    @Background
    void checkDownloadFirmware( String version )
    {
        boolean result = false;
        try {

            result = versionCheckServiceClient.checkSoftwareUpdate(mDBManager.getUserInfo().getEmail(), version);

        }
        catch(RestClientException e)
        {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setPositiveButton( this.getString(R.string.common_ok) , new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();     //닫기
                }
            });
            alert.setMessage("펌웨어 버전을 확인하는 중에 오류가 발생하였습니다.");
            alert.show();
        }

        if( result ){



        }else{
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setPositiveButton( this.getString(R.string.common_ok) , new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();     //닫기
                }
            });
            alert.setMessage("최신 버전입니다.");
            alert.show();
        }

    }


    @OnActivityResult( USERINFO_REQUEST_CODE )
    void onUserInfoResult( int resultCode , Intent data ){
        if (resultCode == RESULT_OK) {

            this.mUserInfo.setName(data.getStringExtra(UserInfo.NAME_KEY));
            this.tvName.setText(this.mUserInfo.getName());

            this.mUserInfo.setIntro(data.getStringExtra(UserInfo.HELLO_MESSAGE_KEY));
            this.tvIntroSelf.setText(this.mUserInfo.getIntro());

            long tempBirthDay = data.getLongExtra(UserInfo.BIRTHDATEKEY, 0);

            if( tempBirthDay > 0 )
            {
                this.mUserInfo.setBirthDate(tempBirthDay);
                this.tvBirthDate.setText(this.getDateString(this.mUserInfo.getBirthDate()));
            }

            GenderType tempGenderType = GenderType.getGenderType(data.getIntExtra(UserInfo.SEX_KEY, 0));
            if( !tempGenderType.equals(GenderType.NONE) ) {
                this.mUserInfo.setGender(tempGenderType);
                if (mUserInfo.getGender() == GenderType.MALE) {
                    tvSex.setText(strMale);
                } else if (mUserInfo.getGender() == GenderType.FEMALE) {
                    tvSex.setText(strFemale);
                }
            }

            this.mDBManager.modifyUserInfo(mUserInfo);
            mPresenter.ChangeUserInfo();
        }
    }

    @OnActivityResult(USERINFO_CHANGEDEVICE_CODE)
    void changeDeviceResult()
    {
        this.isShowing = false;
        mUserInfo = this.mPresenter.getUserInfo();
        this.mPresenter.ChangeUserInfo();
        this.mPresenter.requestRestartService();
    }

    @OnActivityResult(USERINFO_CHANGELOCATION_CODE)
    void changeWearingLocationResult(int resultCode , Intent data ){
        if(resultCode == RESULT_OK)
        {
            int location = data.getIntExtra(LocationChangeActivity.INTENT_LOCATION, 0);
            this.tvWearingLocation.setText( this.getWearingLocationString( location ) );
            mUserInfo.setWearAt(location);
            mPresenter.modifyUserInfo(mUserInfo);
            mPresenter.ChangeUserInfo();
        }

        this.isShowing = false;
        this.mPresenter.requestRestartService();
    }

    @Override
    public void showDeviceChangeActivity() {
        dismissProgress();
        Intent deviceChangeIntent = new Intent( SettingActivity.this , DeviceChangeActivity.class  );
        deviceChangeIntent.putExtra(DeviceChangeActivity.INTENT_LOCATION, mUserInfo.getWearAt());
        this.startActivityForResult(deviceChangeIntent, USERINFO_CHANGEDEVICE_CODE);
    }

    @Override
    public void showWearingLocationActivity() {
        dismissProgress();
        Intent locationChangeIntent = new Intent( SettingActivity.this , LocationChangeActivity.class  );
        locationChangeIntent.putExtra(LocationChangeActivity.INTENT_LOCATION, mUserInfo.getWearAt());
        locationChangeIntent.putExtra(LocationChangeActivity.INTENT_BTADDRESS, mUserInfo.getDeviceAddress());
        this.startActivityForResult(locationChangeIntent, USERINFO_CHANGELOCATION_CODE);
    }

    @Override
    public void setShowing(boolean showing) {
        this.isShowing = showing;
    }

    private String getDateString( long birthUtcTime ){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd");
        Date utcDate = new Date();
        utcDate.setTime(birthUtcTime);
        return dateFormat.format(utcDate);
    }

    private void showNoSettingDialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);     // 여기서 this는 Activity의 this

        String message = this.getString(R.string.userinfo_not_connect_text);
        builder.setMessage(message)        // 메세지 설정
                .setCancelable(true)        // 뒤로 버튼 클릭시 취소 가능 설정
                .setPositiveButton(R.string.userinfo_not_start_fitmeter, new DialogInterface.OnClickListener() {
                    // 취소 버튼 클릭시 설정
                    public void onClick(DialogInterface dialog, int whichButton) {
                        ActivityManager.getInstance().addActivity(mActivity);
                        Intent intent = new Intent(getApplicationContext(), SetUserInfoActivity.class);
                        startActivity(intent);

                        dialog.dismiss();
                    }
                })
                .setNegativeButton(R.string.common_close, new DialogInterface.OnClickListener() {
                    // 확인 버튼 클릭시 설정
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                });

        AlertDialog dialog = builder.create();    // 알림창 객체 생성
        dialog.show();    // 알림창 띄우기
    }

    class DownloadFileFromURL extends AsyncTask<String,String,String>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //프로그레스 다이얼로그를 실행시킨다.
        }

        @Override
        protected String doInBackground(String... params)
        {

            int count;
            try {
                URL url = new URL(params[0]);
                URLConnection connection = url.openConnection();
                connection.connect();

                int lengthOfFile = connection.getContentLength();
                InputStream input = new BufferedInputStream(url.openStream());

                OutputStream output = new FileOutputStream(Environment.getExternalStorageDirectory().toString() + "/firmware");

                byte data[] = new byte[1024];

                long total =0;

                while( (count = input.read()) != -1 ){

                    total += count;
                    publishProgress("" + (int) ((total * 100) / lengthOfFile));
                    output.write(data,0,count);
                }

                output.flush();

                output.close();
                input.close();

            }catch(Exception e)
            {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            //프로그레스 다이얼로그에 퍼센트를 넘긴다.
        }

        @Override
        protected void onPostExecute(String s) {
            //프로그레스 다이얼로그를 없앤다.
        }
    }

    private String getFirmwareVersionSharedPreference( Context context )
    {
        SharedPreferences userInfo = context.getSharedPreferences( "fitmate" , Activity.MODE_PRIVATE );
        String firmwareVersion = userInfo.getString( "firmwareversion" , "" );
        return firmwareVersion;
    }

    private String[] getValueArray( float value ){
        String valueString = String.valueOf( value );
        String[] result = null;
        String[] splitedWeightString = valueString.split("\\.");

        if( splitedWeightString.length > 1 ){
            splitedWeightString[1] = splitedWeightString[1].substring(0,1);
            result = splitedWeightString;
        }else{
            result = new String[]{ splitedWeightString[0] , "0" };
        }

        return result;
    }

}
