package com.fitdotlife.fitmate;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.fitdotlife.fitmate_lib.util.Utils;

/**
 * Created by Joshua on 2016-01-27.
 */
public class NetworkChangeReceiver extends BroadcastReceiver{

    @Override
    public void onReceive(Context context, Intent intent) {
        MyApplication myApplication = (MyApplication) context.getApplicationContext();
        myApplication.notifyNetworkStateChange( Utils.isOnline(context) );
    }
}
