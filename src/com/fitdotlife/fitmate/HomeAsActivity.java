package com.fitdotlife.fitmate;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.StrengthInputType;
import com.fitdotlife.fitmate_lib.customview.AppliedProgramLayout;
import com.fitdotlife.fitmate_lib.customview.DrawerView;
import com.fitdotlife.fitmate_lib.customview.HomeDayScoreChartView;
import com.fitdotlife.fitmate_lib.object.ExerciseProgram;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringRes;

import java.util.Calendar;

@EActivity(R.layout.activity_home_as)
public class HomeAsActivity extends Activity {

    @ViewById(R.id.ic_activity_homeas_actionbar)
    View actionBarView;

    @ViewById(R.id.apl_activity_homeas_appliedprogram_info)
    AppliedProgramLayout aplProgramInfo;

    @ViewById(R.id.tv_activity_homeas_date)
    TextView tvTodayDate;

    @ViewById(R.id.tv_activity_homeas_day_calorie)
    TextView tvDayCalorie;

    @ViewById(R.id.cv_activity_homeas_day_score_chart)
    HomeDayScoreChartView dayScoreChart;

    @StringRes(R.string.exercise_promoting_calorie)
    String strCalorieName;

    @ViewById(R.id.dl_activity_homeas)
    DrawerLayout dlHomeAs;

    @ViewById(R.id.dv_activity_homeas_drawer)
    DrawerView drawerView;


    @AfterViews
    void onInit(){

        ActivityManager.getInstance().addActivity(this);

        ImageView imgLeft = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_left);
        imgLeft.setVisibility(View.GONE);
        ImageView imgSliding = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_right);
        imgSliding.setBackground(this.getResources().getDrawable(R.drawable.icon_slide_selector));
        imgSliding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dlHomeAs.openDrawer(drawerView);
            }
        });
        drawerView.setParentActivity(this);

        ImageView imgBattery = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_right_more);
        imgBattery.setImageResource(R.drawable.battery1);
        imgBattery.setVisibility(View.VISIBLE);

        ExerciseProgram program = new ExerciseProgram();
        program.setCategory(StrengthInputType.CALORIE.Value);
        program.setStrengthFrom(450);
        program.setStrengthTo(450);
        program.setName(strCalorieName);
        program.setMinuteFrom(30);
        program.setMinuteTo(30);
        program.setTimesFrom(7);
        program.setTimesTo(7);
        aplProgramInfo.setAppliedExerciseProgram(program);

        Calendar todayCalendar = Calendar.getInstance();
        todayCalendar.setTimeInMillis(System.currentTimeMillis());
        int month = todayCalendar.get( Calendar.MONTH );
        int day = todayCalendar.get(Calendar.DAY_OF_MONTH);
        int dayOfWeek = todayCalendar.get( Calendar.DAY_OF_WEEK );

        String monthString = this.getResources().getStringArray(R.array.month_char)[ month ];
        String dayString = day + this.getString( R.string.common_day);
        String dayOfWeekString = this.getResources().getStringArray( R.array.week_string )[ dayOfWeek - 1 ];
        this.tvTodayDate.setText(monthString + " " + dayString + " " + dayOfWeekString);

        displayDayAchievement((int)program.getStrengthFrom() , (int)program.getStrengthTo() , 300 , StrengthInputType.CALORIE , 20 );
    }

    private void displayDayAchievement( int rangeFrom , int rangeTo , double value , StrengthInputType strengthType , int extraScore ){

        String rateText = "";
        String statusText = "";
        String valueUnit = "";
        String titleText = "";

        if(strengthType.equals( StrengthInputType.CALORIE_SUM) || strengthType.equals( StrengthInputType.CALORIE ) || strengthType.equals(StrengthInputType.VS_BMR)){

            valueUnit  = this.getString(R.string.common_calorie);
            titleText = this.getString(R.string.home_today_bured_calorie);

        }else{
            valueUnit  = this.getString(R.string.common_minute);
            titleText = this.getString(R.string.home_activity_time);
        }


        if(value < rangeFrom){
            if(strengthType.equals( StrengthInputType.CALORIE_SUM) || strengthType.equals( StrengthInputType.CALORIE ) || strengthType.equals(StrengthInputType.VS_BMR)){
                statusText = String.format( this.getString(R.string.home_more_calorie) , (int)( rangeFrom - value ));
                rateText = String.format( this.getString(R.string.home_today_gain_point) , extraScore );
            }else{
                statusText = String.format( this.getString(R.string.home_more_exercise) , (int)( rangeFrom - value ));
                rateText = String.format( this.getString(R.string.home_today_gain_point) , extraScore );
            }
        }else if( value >= rangeFrom ){
            statusText = this.getString(R.string.home_achieve_goal);
            rateText = this.getString(R.string.home_conguraturation);
        }

        this.dayScoreChart.setRange( rangeFrom , rangeTo );
        this.dayScoreChart.setValueUnit(valueUnit);
        this.dayScoreChart.setAchievementValue( value );
        this.dayScoreChart.setTitleText( titleText );
        this.dayScoreChart.setStatusText( statusText );
        this.dayScoreChart.setRateText( rateText );
        this.dayScoreChart.startAnimation();
    }



    @Click(R.id.btn_activity_homeas_start_fitmeter)
    void startFitmeterClick(){
        Intent intent = new Intent( this.getApplicationContext() , SetUserInfoActivity.class );
        startActivity(intent);
    }


    @Override
    public void onBackPressed() {

        if(dlHomeAs.isDrawerOpen(drawerView)){
            dlHomeAs.closeDrawers();
            return;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);     // 여기서 this는 Activity의 this

// 여기서 부터는 알림창의 속성 설정
        builder.setTitle(getString(R.string.common_quit_title))        // 제목 설정
                .setMessage(getString(R.string.common_quit_content))        // 메세지 설정
                .setCancelable(true)        // 뒤로 버튼 클릭시 취소 가능 설정
                .setPositiveButton(R.string.common_yes, new DialogInterface.OnClickListener() {
                    // 확인 버튼 클릭시 설정
                    public void onClick(DialogInterface dialog, int whichButton) {
                        moveTaskToBack(true);

                        finish();

                        //  android.os.Process.killProcess(android.os.Process.myPid());
                        dialog.cancel();

                        //   MainActivity.this.onBackPressed();
                    }
                })
                .setNegativeButton(R.string.common_no, new DialogInterface.OnClickListener() {
                    // 취소 버튼 클릭시 설정
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.cancel();
                    }
                });


        AlertDialog dialog = builder.create();    // 알림창 객체 생성
        dialog.show();    // 알림창 띄우기

    }


}
