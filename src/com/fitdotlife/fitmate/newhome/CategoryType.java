package com.fitdotlife.fitmate.newhome;

/**
 * Created by Joshua on 2016-01-13.
 */
public enum CategoryType {
    DAILY_ACHIEVE, WEEKLY_ACHIEVE , EXERCISE_TIME , CALORIES , FAT , CARBOHYDRATE , DISTANCE;

    public static int getDefaultValue( CategoryType categoryType )
    {
        int defaultValue = 0;
        switch( categoryType )
        {
            case DAILY_ACHIEVE:
                defaultValue = 0;
                break;
            case WEEKLY_ACHIEVE:
                defaultValue = 0;
                break;
            case EXERCISE_TIME:
                defaultValue = 100;
                break;
            case CALORIES:
                defaultValue = 500;
                break;
            case FAT:
                defaultValue = 10;
                break;
            case CARBOHYDRATE:
                defaultValue = 60;
                break;
            case DISTANCE:
                defaultValue = 10;
                break;
        }

        return defaultValue;
    }

    public static int getPointNumber(  CategoryType categoryType )
    {
        int defaultValue = 0;
        switch( categoryType )
        {
            case DAILY_ACHIEVE:
                defaultValue = 0;
                break;
            case WEEKLY_ACHIEVE:
                defaultValue = 0;
                break;
            case EXERCISE_TIME:
                defaultValue = 0;
                break;
            case CALORIES:
                defaultValue = 0;
                break;
            case FAT:
                defaultValue = 2;
                break;
            case CARBOHYDRATE:
                defaultValue = 2;
                break;
            case DISTANCE:
                defaultValue = 2;
                break;
        }

        return defaultValue;
    }

}
