package com.fitdotlife.fitmate.newhome;

/**
 * Created by Joshua on 2016-01-14.
 */
public class NewHomeConstant {

    public static CategoryType[] DefaultViewOrder = new CategoryType[]
    {
            CategoryType.DAILY_ACHIEVE,
            CategoryType.CALORIES ,
            CategoryType.FAT ,
            CategoryType.DISTANCE
    };

    public static CategoryType[] AllTabViewOrder = new CategoryType[]
    {
            CategoryType.DAILY_ACHIEVE,
            CategoryType.CALORIES ,
            CategoryType.FAT ,
            CategoryType.DISTANCE,
            CategoryType.WEEKLY_ACHIEVE ,
            CategoryType.EXERCISE_TIME ,
            CategoryType.CARBOHYDRATE
    };

}
