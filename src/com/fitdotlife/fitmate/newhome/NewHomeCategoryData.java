package com.fitdotlife.fitmate.newhome;

import android.content.Context;

import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseAnalyzer;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.StrengthInputType;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.UserInfoForAnalyzer;
import com.fitdotlife.fitmate.R;
import com.fitdotlife.fitmate_lib.object.DayActivity;
import com.fitdotlife.fitmate_lib.object.ExerciseProgram;
import com.fitdotlife.fitmate_lib.object.FriendDayActivity;
import com.fitdotlife.fitmate_lib.object.UserFriend;
import com.fitdotlife.fitmate_lib.object.UserInfo;

/**
 * Created by Joshua on 2016-02-02.
 */
public class NewHomeCategoryData {

    private enum RangeUnitType{ MINUTE , CALORIE , WEIGHT_GRAM , DISTANCE }

    private float averageMet = 0;
    private ExerciseAnalyzer.ActivityRank averageMetRank = null;
    private int exerciseTime = 0;
    private ExerciseAnalyzer.ActivityRank exerciseTimeRank = null;
    private int calories = 0;
    private ExerciseAnalyzer.ActivityRank caloriesRank = null;
    private int dayAchieve = 0;
    private ExerciseAnalyzer.ActivityRank dayAchieveRank = null;
    private int weekScore = 0;
    private ExerciseAnalyzer.ActivityRank weekScoreRank = null;
    private double fat = 0;
    private ExerciseAnalyzer.ActivityRank fatRank = null;
    private String fatUnitText = null;
    private double carbohydrate = 0;
    private ExerciseAnalyzer.ActivityRank carbohydrateRank = null;
    private String carbohydrateUnitText = null;
    private double distance = 0;
    private String distanceUnitText = null;

    public String getFatUnitText() {
        return fatUnitText;
    }

    public void setFatUnitText(String fatUnitText) {
        this.fatUnitText = fatUnitText;
    }

    public String getCarbohydrateUnitText() {
        return carbohydrateUnitText;
    }

    public void setCarbohydrateUnitText(String carbohydrateUnitText) {
        this.carbohydrateUnitText = carbohydrateUnitText;
    }

    public String getDistanceUnitText() {
        return distanceUnitText;
    }

    public void setDistanceUnitText(String distanceText) {
        this.distanceUnitText = distanceText;
    }

    public ExerciseAnalyzer.ActivityRank getAverageMetRank() {
        return averageMetRank;
    }

    public void setAverageMetRank(ExerciseAnalyzer.ActivityRank averageMetRank) {
        this.averageMetRank = averageMetRank;
    }

    public ExerciseAnalyzer.ActivityRank getCaloriesRank() {
        return caloriesRank;
    }

    public void setCaloriesRank(ExerciseAnalyzer.ActivityRank caloriesRank) {
        this.caloriesRank = caloriesRank;
    }

    public ExerciseAnalyzer.ActivityRank getCarbohydrateRank() {
        return carbohydrateRank;
    }

    public void setCarbohydrateRank(ExerciseAnalyzer.ActivityRank carbohydrateRank) {
        this.carbohydrateRank = carbohydrateRank;
    }

    public ExerciseAnalyzer.ActivityRank getDayAchieveRank() {
        return dayAchieveRank;
    }

    public void setDayAchieveRank(ExerciseAnalyzer.ActivityRank dayAchieveRank) {
        this.dayAchieveRank = dayAchieveRank;
    }

    public ExerciseAnalyzer.ActivityRank getDistanceRank() {
        return distanceRank;
    }

    public void setDistanceRank(ExerciseAnalyzer.ActivityRank distanceRank) {
        this.distanceRank = distanceRank;
    }

    public ExerciseAnalyzer.ActivityRank getExerciseTimeRank() {
        return exerciseTimeRank;
    }

    public void setExerciseTimeRank(ExerciseAnalyzer.ActivityRank exerciseTimeRank) {
        this.exerciseTimeRank = exerciseTimeRank;
    }

    public ExerciseAnalyzer.ActivityRank getFatRank() {
        return fatRank;
    }

    public void setFatRank(ExerciseAnalyzer.ActivityRank fatRank) {
        this.fatRank = fatRank;
    }

    public ExerciseAnalyzer.ActivityRank getWeekScoreRank() {
        return weekScoreRank;
    }

    public void setWeekScoreRank(ExerciseAnalyzer.ActivityRank weekScoreRank) {
        this.weekScoreRank = weekScoreRank;
    }

    private ExerciseAnalyzer.ActivityRank distanceRank = null;
    private float strengthFrom = 0;
    private float strengthTo = 0;
    private String strengthUnitText = null;

    public float getStrengthFrom() {
        return strengthFrom;
    }

    public void setStrengthFrom(float strengthFrom) {
        this.strengthFrom = strengthFrom;
    }

    public float getStrengthTo() {
        return strengthTo;
    }

    public void setStrengthTo(float strengthTo) {
        this.strengthTo = strengthTo;
    }

    public String getStrengthUnitText() {
        return strengthUnitText;
    }

    public void setStrengthUnitText(String strengthUnitText) {
        this.strengthUnitText = strengthUnitText;
    }

    public float getAverageMet() {
        return averageMet;
    }

    public void setAverageMet(float averageMet) {
        this.averageMet = averageMet;
    }

    public int getCalories() {
        return calories;
    }

    public void setCalories(int calories) {
        this.calories = calories;
    }

    public double getCarbohydrate() {
        return carbohydrate;
    }

    public void setCarbohydrate(double carbohydrate) {
        this.carbohydrate = carbohydrate;
    }

    public int getDayAchieve() {
        return dayAchieve;
    }

    public void setDayAchieve(int dayAchieve) {
        this.dayAchieve = dayAchieve;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public int getExerciseTime() {
        return exerciseTime;
    }

    public void setExerciseTime(int exerciseTime) {
        this.exerciseTime = exerciseTime;
    }

    public double getFat() {
        return fat;
    }

    public void setFat(double fat) {
        this.fat = fat;
    }

    public int getWeekScore() {
        return weekScore;
    }

    public void setWeekScore(int weekScore) {
        this.weekScore = weekScore;
    }

    public static NewHomeCategoryData getNewHomeCategoryData( Context context , DayActivity dayActivity , ExerciseProgram program  , UserInfo userInfo){

        UserInfoForAnalyzer userInfoForAnalyzer = com.fitdotlife.fitmate_lib.service.util.Utils.getUserInfoForAnalyzer(userInfo);
        NewHomeCategoryData newHomeCategoryData = new NewHomeCategoryData();

        float averageMet = dayActivity.getAverageMET();
        newHomeCategoryData.setAverageMet(averageMet);
        newHomeCategoryData.setAverageMetRank(ExerciseAnalyzer.averageMETRank(averageMet));

        int exerciseTime = dayActivity.getStrengthHigh() + dayActivity.getStrengthMedium();
        newHomeCategoryData.setExerciseTime(exerciseTime);
        newHomeCategoryData.setExerciseTimeRank(ExerciseAnalyzer.strengthOverLight(exerciseTime));

        int calories = dayActivity.getCalorieByActivity();
        newHomeCategoryData.setCalories(calories);
        newHomeCategoryData.setCaloriesRank(ExerciseAnalyzer.calorieRank(userInfoForAnalyzer, calories));

        int dayAchieve = dayActivity.getAchieveOfTarget();
        newHomeCategoryData.setDayAchieve(dayAchieve);

        int weekScore = dayActivity.getPredayScore() + dayActivity.getTodayScore();
        newHomeCategoryData.setWeekScore(weekScore);

        double fat = dayActivity.getFat();
        String fatUnitText = null;
        if( userInfo.isSI() ){
            fatUnitText = context.getString( R.string.common_gram );
        }else{
            fat *= 0.035274;
            fatUnitText = context.getString( R.string.common_ounce );
        }
        newHomeCategoryData.setFat(fat);
        newHomeCategoryData.setFatRank(ExerciseAnalyzer.fatRank(userInfo.getWeight(), fat));
        newHomeCategoryData.setFatUnitText( fatUnitText );

        double carbohydrate = dayActivity.getCarbohydrate();
        String carbohydrateUnitText = null;
        if( userInfo.isSI() ){
            carbohydrateUnitText = context.getString( R.string.common_gram );
        }else{
            carbohydrate *= 0.035274;
            carbohydrateUnitText = context.getString( R.string.common_ounce );
        }
        newHomeCategoryData.setCarbohydrate(carbohydrate);
        newHomeCategoryData.setCarbohydrateUnitText( carbohydrateUnitText );


        double distance = dayActivity.getDistance();
        String distanceValueUnitText = null;
        if( userInfo.isSI() ){
            distanceValueUnitText = context.getString(R.string.common_kilometer);
        }else{
            //킬로미터를 야드로 변환한다.
            distance = distance * 0.621371;
            distanceValueUnitText = context.getString(R.string.common_mile);
        }

        newHomeCategoryData.setDistance(distance);
        newHomeCategoryData.setDistanceRank( ExerciseAnalyzer.distanceRank( distance ) );
        newHomeCategoryData.setDistanceUnitText( distanceValueUnitText );

        String strengthTypeUnitText = null;
        float strengthFrom = 0;
        float strengthTo = 0;

        StrengthInputType strengthType = StrengthInputType.getStrengthType(program.getCategory());

        if( strengthType.equals( StrengthInputType.CALORIE ) || strengthType.equals( StrengthInputType.CALORIE_SUM )|| strengthType.equals(StrengthInputType.VS_BMR)) {
            strengthTypeUnitText = context.getResources().getString( R.string.common_calorie );

            strengthFrom= (int) program.getStrengthFrom();
            strengthTo= (int) program.getStrengthFrom();
            int bmr = userInfoForAnalyzer.getBMR();

            if(strengthType.equals(StrengthInputType.VS_BMR)){
                strengthFrom=(int)Math.round(strengthFrom /100.0 * bmr);
                strengthTo=(int)Math.round(strengthTo /100.0 * bmr);
            }
        }else{
            strengthTypeUnitText = context.getResources().getString( R.string.common_minute );
            strengthFrom = program.getMinuteFrom();
            strengthTo = program.getMinuteTo();
        }

        newHomeCategoryData.setStrengthUnitText( strengthTypeUnitText );
        newHomeCategoryData.setStrengthFrom(strengthFrom);
        newHomeCategoryData.setStrengthTo(strengthTo);


        return newHomeCategoryData;
    }

    public static NewHomeCategoryData getNewHomeCategoryData( Context context , FriendDayActivity friendDayActivity , UserFriend userFriend ){
        NewHomeCategoryData newHomeCategoryData = new NewHomeCategoryData();

        newHomeCategoryData.setAverageMet(Float.parseFloat(friendDayActivity.getAveragemet()));
        newHomeCategoryData.setAverageMetRank(ExerciseAnalyzer.ActivityRank.getActivityRank(friendDayActivity.getAveragemetrank()));

        newHomeCategoryData.setExerciseTime(Integer.parseInt(friendDayActivity.getStrengthhigh()) + Integer.parseInt(friendDayActivity.getStrengthmedium()));
        newHomeCategoryData.setExerciseTimeRank(ExerciseAnalyzer.ActivityRank.getActivityRank(friendDayActivity.getStrengthoverlightrank()));

        newHomeCategoryData.setCalories(Integer.parseInt(friendDayActivity.getCalorie()));
        newHomeCategoryData.setCaloriesRank(ExerciseAnalyzer.ActivityRank.getActivityRank(friendDayActivity.getCalorierank()));

        newHomeCategoryData.setDayAchieve(Integer.parseInt(friendDayActivity.getAchieve()));

        newHomeCategoryData.setWeekScore(Integer.parseInt(friendDayActivity.getWeekscore()));

        double fat = Double.parseDouble(friendDayActivity.getFat());
        String fatUnitText = null;
        if( userFriend.isSi() ){
            fatUnitText = context.getString( R.string.common_gram );
        }else{
            fat *= 0.035274;
            fatUnitText = context.getString( R.string.common_ounce );
        }
        newHomeCategoryData.setFat(fat);
        newHomeCategoryData.setFatRank(ExerciseAnalyzer.ActivityRank.getActivityRank(friendDayActivity.getFatrank()));
        newHomeCategoryData.setFatUnitText(fatUnitText);

        double carbohydrate = Double.parseDouble(friendDayActivity.getCarbohydrate());
        String carbohydrateUnitText = null;
        if( userFriend.isSi() ){
            carbohydrateUnitText = context.getString( R.string.common_gram );
        }else{
            carbohydrate *= 0.035274;
            carbohydrateUnitText = context.getString( R.string.common_ounce );
        }
        newHomeCategoryData.setCarbohydrate(carbohydrate);
        newHomeCategoryData.setCarbohydrateUnitText(carbohydrateUnitText );

        double distance = Double.parseDouble(friendDayActivity.getDistance());
        String distanceValueUnitText = null;
        if(  userFriend.isSi() ){
            distanceValueUnitText = context.getString(R.string.common_kilometer);
        }else{
            //킬로미터를 야드로 변환한다.
            distance = distance * 0.621371;
            distanceValueUnitText = context.getString(R.string.common_mile);
        }

        newHomeCategoryData.setDistanceUnitText( distanceValueUnitText );
        newHomeCategoryData.setDistance(Double.parseDouble(friendDayActivity.getDistance()));
        newHomeCategoryData.setDistanceRank(ExerciseAnalyzer.ActivityRank.getActivityRank(friendDayActivity.getDistancerank()));

        newHomeCategoryData.setStrengthUnitText(  getRangeUnitTypeText( context , RangeUnitType.values()[ friendDayActivity.getTargetrangeinfo().getRangeUnitType() ] ) );
        newHomeCategoryData.setStrengthFrom(friendDayActivity.getTargetrangeinfo().getRangeFrom());
        newHomeCategoryData.setStrengthTo(friendDayActivity.getTargetrangeinfo().getRangeTo());

        return newHomeCategoryData;
    }



    private static String getRangeUnitTypeText( Context context , RangeUnitType rangeUnitType ){

        String rangeUnitText = null;

        switch (rangeUnitType){
            case MINUTE:
                rangeUnitText = context.getResources().getString( R.string.common_minute );
                break;
            case CALORIE:
                rangeUnitText = context.getResources().getString( R.string.common_calorie );
                break;
            case WEIGHT_GRAM:
                rangeUnitText = "g";
                break;
            case DISTANCE:
                rangeUnitText = "km";
                break;
        }

        return rangeUnitText;
    }

    public static NewHomeCategoryData getNewHomeCategoryData(Context context)
    {
        NewHomeCategoryData newHomeCategoryData = new NewHomeCategoryData();

        float averageMet = 1.4f;
        newHomeCategoryData.setAverageMet(averageMet);
        newHomeCategoryData.setAverageMetRank(ExerciseAnalyzer.averageMETRank(averageMet));

        int exerciseTime = 30;
        newHomeCategoryData.setExerciseTime(exerciseTime);
        newHomeCategoryData.setExerciseTimeRank(ExerciseAnalyzer.strengthOverLight(exerciseTime));

        int calories = 200;
        newHomeCategoryData.setCalories(calories);
        newHomeCategoryData.setCaloriesRank(ExerciseAnalyzer.ActivityRank.NORMAL);

        int dayAchieve = 50;
        newHomeCategoryData.setDayAchieve(dayAchieve);

        int weekScore = 65;
        newHomeCategoryData.setWeekScore(weekScore);

        double fat = 5.43f;
        newHomeCategoryData.setFat(fat);
        newHomeCategoryData.setFatRank( ExerciseAnalyzer.ActivityRank.NORMAL);

        double carbohydrate = 1.2f;
        newHomeCategoryData.setCarbohydrate(carbohydrate);

        double distance = 3.45f;
        newHomeCategoryData.setDistance( distance );
        newHomeCategoryData.setDistanceRank( ExerciseAnalyzer.distanceRank( distance ) );
        newHomeCategoryData.setDistanceUnitText(context.getString(R.string.common_kilometer));

        String strengthTypeUnitText = context.getResources().getString( R.string.common_calorie );
        float strengthFrom = 250;
        float strengthTo = 250;

        newHomeCategoryData.setStrengthUnitText( strengthTypeUnitText );
        newHomeCategoryData.setStrengthFrom(strengthFrom);
        newHomeCategoryData.setStrengthTo(strengthTo);

        return newHomeCategoryData;
    }

}
