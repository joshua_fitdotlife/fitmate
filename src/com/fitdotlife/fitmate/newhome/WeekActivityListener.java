package com.fitdotlife.fitmate.newhome;

import com.fitdotlife.fitmate_lib.object.FriendWeekActivity;

/**
 * Created by Joshua on 2016-01-13.
 */
public interface WeekActivityListener {

    void OnWeekActivityReceived( );

    void OnWeekActivityReceived( FriendWeekActivity friendWeekActivity);

    void OnWeekActivityReceived( FriendWeekActivity[] friendWeekActivityList);
}
