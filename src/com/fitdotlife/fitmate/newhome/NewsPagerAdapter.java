package com.fitdotlife.fitmate.newhome;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Joshua on 2016-02-02.
 */
public class NewsPagerAdapter extends PagerAdapter{

    private View[] mNewsList = null;
    private Context mContext = null;

    public NewsPagerAdapter(Context context, View[] newsList)
    {
        this.mContext = context;
        this.mNewsList = newsList;
    }

    @Override
    public int getCount() {
        return mNewsList.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = mNewsList[ position ];
        container.addView( view );
        return  view;
    }

}
