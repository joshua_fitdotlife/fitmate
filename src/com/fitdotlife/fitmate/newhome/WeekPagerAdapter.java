package com.fitdotlife.fitmate.newhome;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.fitdotlife.fitmate_lib.customview.NewHomeWeekBarView;
import com.fitdotlife.fitmate_lib.customview.NewHomeWeekBarView_;
import com.fitdotlife.fitmate_lib.customview.NewHomeWeekTableView;
import com.fitdotlife.fitmate_lib.customview.NewHomeWeekTableView_;

import org.apache.log4j.Log;

import java.util.Calendar;

/**
 * Created by Joshua on 2016-02-18.
 */
public abstract class WeekPagerAdapter extends PagerAdapter implements ViewPager.OnPageChangeListener , WeekActivityListener
{
    public enum WeekPagerType{ WEEK_BAR , WEEK_TABLE };
    public enum DirectionType{LEFT , RIGHT}

    public static final int TODAY_AFTER_NODATA = -1;
    public static final int TODAY_BEFORE_NODATA = -2;

    protected WeekPagerType mWeekPagerType = null;

    protected WeekPagerActivityInfo[] mWeekPagerActivityInfoList = null;

    protected View[] mWeekGraphs = null;

    protected Context mContext = null;

    protected int mPagerLength = 0;

    protected CategoryType mCategoryType = null;

    protected boolean mDownloadData = false;

    private View.OnClickListener mWeekCellClickListener = null;

    private WeekMoveListener mWeekMoveListener = null;

    protected int mCurrentPosition = 0;

    protected int mSelectedDayIndex = 0;

    private Calendar mDayCalendar = null;

    protected boolean isMoveWeek = false;

    public interface WeekMoveListener{
        void onWeekMove( long dayMilliseconds , int position , WeekPagerType weekPagerType );
    }

    public WeekPagerAdapter( Context context , int weekPagerLength , CategoryType categoryType , View.OnClickListener weekCellClickListener , WeekMoveListener weekMoveListener , long dayMilliseconds , WeekPagerType weekPagerType , WeekPagerActivityInfo[] weekPagerActivityInfoList )
    {
        this.mContext = context;
        this.mPagerLength = weekPagerLength;

        mWeekGraphs = new View[mPagerLength];

        mCategoryType = categoryType;

        mWeekCellClickListener = weekCellClickListener;

        mWeekMoveListener = weekMoveListener;

        mDayCalendar = Calendar.getInstance();
        mDayCalendar.setTimeInMillis( dayMilliseconds );
        mSelectedDayIndex = mDayCalendar.get( Calendar.DAY_OF_WEEK ) - 1;

        mWeekPagerType = weekPagerType;

        mWeekPagerActivityInfoList = weekPagerActivityInfoList;

        mCurrentPosition = -1;
    }

    @Override
    public int getCount() {
        return mPagerLength;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    public void changeCategoryType( CategoryType categoryType )
    {
        mCategoryType = categoryType;

        if( ( mCurrentPosition + 1 ) < mPagerLength )
        {
            refreshWeekView(mCurrentPosition + 1);
        }

        refreshWeekView(mCurrentPosition);

        if( (mCurrentPosition - 1) >= 0 ){
            refreshWeekView(mCurrentPosition - 1);
        }
    }

    private void refreshWeekView( int position ){
        View weekView = mWeekGraphs[ position];

        getWeekActivityInfo(position);


        WeekPagerActivityInfo nextWeekActivityInfo = mWeekPagerActivityInfoList[ position ];
        double[] weekValues = nextWeekActivityInfo.getValues( mCategoryType );

        if(mCategoryType.equals( CategoryType.WEEKLY_ACHIEVE )){
            refreshWeekTableView((NewHomeWeekTableView) weekView, weekValues );
        }else if( mCategoryType.equals( CategoryType.DAILY_ACHIEVE ) ) {

            float strengthFrom = nextWeekActivityInfo.getStrengthFrom();
            float strengthTo = nextWeekActivityInfo.getStrengthTo();
            refreshWeekBarView((NewHomeWeekBarView) weekView, weekValues , strengthFrom , strengthTo);

        }else {
            refreshWeekBarView((NewHomeWeekBarView) weekView, weekValues);
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected( int position )
    {

        if( mWeekPagerType.equals( WeekPagerType.WEEK_BAR ) ){

            Log.i("fitmate" , "BAR");

        }else{

            Log.i("fitmate" , "TABLE");

        }

        Log.i("fitmate" , "POSITION : " + position);
        Log.i("fitmate" , "MOVEWEEK : " + isMoveWeek);



        if( isMoveWeek )
        {
            if (mCurrentPosition > position) { // 우측으로 이동 , 지난주로 이동
                mDayCalendar.add(Calendar.DATE, -7);
            }

            if (mCurrentPosition < position) { // 좌측이로 이동 , 다음주로 이동
                mDayCalendar.add(Calendar.DATE, 7);
            }

            this.mWeekMoveListener.onWeekMove( mDayCalendar.getTimeInMillis(), position, mWeekPagerType );

            //주 선택 날짜를 다시 그린다.
            NewHomeWeekView preWeekView = (NewHomeWeekView) mWeekGraphs[mCurrentPosition];
            preWeekView.removeSelectDay();

            NewHomeWeekView currentWeekView = (NewHomeWeekView) mWeekGraphs[position];
            currentWeekView.removeSelectDay();
            currentWeekView.setSelectDay(mSelectedDayIndex);

        }else{
            isMoveWeek = true;
        }

        mCurrentPosition = position;

        //getWeekActivityInfo(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    protected abstract void getWeekActivityInfo( int position );

    @Override
    public void OnWeekActivityReceived() {
        mDownloadData = true;
    }

    public void setDayTimeMilliseconds( long dayTimeMilliseconds ){
        mDayCalendar.setTimeInMillis(dayTimeMilliseconds);
    }

    public void setSelectedDayIndex( int selectedDayIndex )
    {
        mSelectedDayIndex = selectedDayIndex;

        if( ( mCurrentPosition + 1 ) < mPagerLength )
        {
            refreshWeekViewSelectedDayIndex(mCurrentPosition + 1 , selectedDayIndex);
        }

        refreshWeekViewSelectedDayIndex(mCurrentPosition, selectedDayIndex);

        if( (mCurrentPosition - 1) >= 0 ){
            refreshWeekViewSelectedDayIndex( mCurrentPosition - 1 , selectedDayIndex);
        }
    }

    /**
     *
     * @param position
     * @param selectedDayIndex
     */
    private void refreshWeekViewSelectedDayIndex( int position , int selectedDayIndex ){
        NewHomeWeekView weekView = (NewHomeWeekView) mWeekGraphs[ position ];

        weekView.removeSelectDay();
        weekView.setSelectDay(selectedDayIndex);
    }

    protected View getWeekGraphView( WeekPagerActivityInfo weekActivityInfo , int position )
    {
        View view = null;

        if( mWeekGraphs[position] == null ) {

            if (mCategoryType.equals(CategoryType.WEEKLY_ACHIEVE)) {
                NewHomeWeekTableView newHomeWeekTableView = NewHomeWeekTableView_.build(mContext);
                displayWeekTableView(newHomeWeekTableView, weekActivityInfo.getValues(mCategoryType), weekActivityInfo.getWeekRange(), mSelectedDayIndex, weekActivityInfo.getTimeMillis());
                view = newHomeWeekTableView;

            } else {
                NewHomeWeekBarView newHomeWeekBarView = NewHomeWeekBarView_.build(mContext);
                displayWeekBarView(newHomeWeekBarView, weekActivityInfo.getValues(mCategoryType), weekActivityInfo.getDayStrings(), weekActivityInfo.getTimeMillis(), mSelectedDayIndex, weekActivityInfo.getStrengthFrom(), weekActivityInfo.getStrengthTo());
                view = newHomeWeekBarView;
            }

            mWeekGraphs[position] = view;
        }else{

            view = mWeekGraphs[position];
            double[] weekValues = weekActivityInfo.getValues( mCategoryType );

            if(mCategoryType.equals( CategoryType.WEEKLY_ACHIEVE )){
                NewHomeWeekTableView tableView = (NewHomeWeekTableView) view;
                tableView.removeSelectDay();
                tableView.setSelectDay( mSelectedDayIndex );
                refreshWeekTableView(tableView, weekValues);
            }else {

                NewHomeWeekBarView barView = (NewHomeWeekBarView) view;
                barView.removeSelectDay();
                barView.setSelectDay( mSelectedDayIndex );

                if (mCategoryType.equals(CategoryType.DAILY_ACHIEVE)) {

                    float strengthFrom = weekActivityInfo.getStrengthFrom();
                    float strengthTo = weekActivityInfo.getStrengthTo();

                    refreshWeekBarView( barView , weekValues, strengthFrom, strengthTo);

                } else {
                    refreshWeekBarView( barView , weekValues);
                }
            }

        }

        return view;
    }

    private void displayWeekBarView( NewHomeWeekBarView weekBarView , double values[] , String[] daySrings , long[] timeMillis , int selectedDayIndex , float strengthFrom , float strengthTo ){
        if( mCategoryType.equals( CategoryType.DAILY_ACHIEVE ) )
        {
            weekBarView.setValues(values, daySrings, selectedDayIndex, mCategoryType, strengthFrom, strengthTo);

        }else {

            weekBarView.setValues(values, daySrings, selectedDayIndex, mCategoryType );

        }
        weekBarView.setDays(timeMillis);
        weekBarView.setListener(mWeekCellClickListener);
        weekBarView.display();
    }

    private void displayWeekTableView( NewHomeWeekTableView weekTableView , double[] values , String weekRange , int selectedDayIndex ,long[] timeMillis ){
        weekTableView.setValues(values, selectedDayIndex);
        weekTableView.setWeekRange(weekRange);
        weekTableView.setListener(mWeekCellClickListener);
        weekTableView.setDays(timeMillis);
        weekTableView.display();
    }

    private void refreshWeekBarView( NewHomeWeekBarView weekBarView , double[] values ){

        weekBarView.setValues(values, mCategoryType);
        weekBarView.refresh();

    }

    private void refreshWeekBarView( NewHomeWeekBarView weekBarView , double[] values , float strengthFrom , float strengthTo ){

        weekBarView.setValues(values, mCategoryType, strengthFrom, strengthTo);
        weekBarView.refresh();

    }

    private void refreshWeekTableView( NewHomeWeekTableView weekTableView , double[] values ){

        weekTableView.setValues(values);
        weekTableView.refresh();

    }

    public void setMoveWeek( boolean isMoveWeek ){
        this.isMoveWeek = isMoveWeek;
    }

    public int getSelectedDayIndex(){
        return this.mSelectedDayIndex;
    }

}
