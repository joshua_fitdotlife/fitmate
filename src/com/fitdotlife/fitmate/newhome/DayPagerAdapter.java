package com.fitdotlife.fitmate.newhome;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Joshua on 2016-01-14.
 */
public class DayPagerAdapter extends PagerAdapter{

    private View[] mDayGraphs = null;
    private Context mContext = null;

    public DayPagerAdapter( Context context , View[] dayGraphs ){
        this.mContext = context;
        this.mDayGraphs = dayGraphs;
    }

    public View[] getDayGraphs(){
        return this.mDayGraphs;
    }

    @Override
    public int getCount() {
        return mDayGraphs.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position)
    {
        View view = mDayGraphs[ position ];
        container.addView(view);
        return  view;
    }
}
