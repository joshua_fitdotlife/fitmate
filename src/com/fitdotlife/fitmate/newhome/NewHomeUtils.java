package com.fitdotlife.fitmate.newhome;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseAnalyzer;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.StrengthInputType;
import com.fitdotlife.fitmate.R;
import com.fitdotlife.fitmate_lib.customview.NewHomeCategoryView;
import com.fitdotlife.fitmate_lib.customview.NewHomeDayCircleView;
import com.fitdotlife.fitmate_lib.customview.NewHomeDayHalfCircleView;
import com.fitdotlife.fitmate_lib.customview.NewHomeDayTextView;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.object.ExerciseProgram;
import com.fitdotlife.fitmate_lib.util.Utils;

import java.util.Calendar;

/**
 * Created by Joshua on 2016-01-14.
 */
public class NewHomeUtils
{

    private Context mContext = null;
    private FitmateDBManager mDBManger = null;
    public PopupWindow programPopup = null;
    private String[] arrCategoryChar;
    private NewHomeProgramClickListener mProgramClickListener;

    public NewHomeUtils(Context context, FitmateDBManager dbManager, String[] arrCategoryChar , NewHomeProgramClickListener programClickListener)
    {
        this.mContext = context;
        this.mDBManger = dbManager;
        this.arrCategoryChar = arrCategoryChar;
        this.mProgramClickListener = programClickListener;
    }

    public static String getComment(Context context , ExerciseAnalyzer.ActivityRank activityRank )
    {
        String comment = null;

        if( activityRank.equals(ExerciseAnalyzer.ActivityRank.VERYGOOD ) ){
            comment = context.getResources().getString( R.string.newhome_comment_verygood );
        }else if( activityRank.equals(ExerciseAnalyzer.ActivityRank.GOOD ) ){
            comment = context.getResources().getString( R.string.newhome_comment_good );
        }else if( activityRank.equals(ExerciseAnalyzer.ActivityRank.NORMAL ) ){
            comment = context.getResources().getString( R.string.newhome_comment_normal );
        }else if( activityRank.equals(ExerciseAnalyzer.ActivityRank.NOTGOOD ) ){
            comment = context.getResources().getString( R.string.newhome_comment_notgood );
        }
        return comment;

    }

    public static String getWeekStartDate( Calendar weekDayCalendar ){
        Calendar weekCalendar = Calendar.getInstance();
        weekCalendar.setTimeInMillis(weekDayCalendar.getTimeInMillis());
        int weekNumber = weekCalendar.get(Calendar.DAY_OF_WEEK);
        weekCalendar.add(Calendar.DATE, -(weekNumber - 1) );
        return getDateString(weekCalendar);
    }

    public static String getDateString( Calendar calendar )
    {
        return calendar.get(Calendar.YEAR) + "-" + String.format("%02d",  calendar.get( Calendar.MONTH ) + 1 ) + "-" + String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH) );
    }

    private void setCategoryViewData( CategoryType categotyType, NewHomeCategoryView categoryView, int viewIndex, String valueText, String valueUnitText , String categoryText , int selectCategoryIndex )
    {
        categoryView.setValueText(valueText);
        categoryView.setValueUnitText(valueUnitText);
        categoryView.setTag(viewIndex);
        categoryView.setCategoryText( categoryText );
        setCategorySelect( categoryView, viewIndex , selectCategoryIndex );

    }

    public void setDayTextViewData( CategoryType categotyType , NewHomeDayTextView dayTextView , float value , String valueUnitText , String commentText , int pointNumber , boolean startZero ){

        dayTextView.setValueUnitText(valueUnitText);
        dayTextView.setCommnetText(commentText);
        dayTextView.setCategoryText(arrCategoryChar[categotyType.ordinal()]);
        dayTextView.setValue(value);
        dayTextView.setmPointNumber(pointNumber);
        dayTextView.display();
        //dayTextView.startAnimation(1000 , startZero);

    }

    private void setCategorySelect( NewHomeCategoryView categoryView , int index , int selectdCategoryIndex )
    {

        if( index == selectdCategoryIndex )
        {
            selectCategoryView( categoryView );
        }else{
            unSelectCategoryView(categoryView);
        }
    }

    public void selectCategoryView( NewHomeCategoryView newHomeCategoryView )
    {
        int[] attrs = new int[]{R.attr.newhomeCategorySelect , R.attr.newhomeCategoryTextSelect , R.attr.newhomeCategoryValueTextSelect };
        TypedArray a = mContext.getTheme().obtainStyledAttributes(attrs);

        Drawable selectDrawable = a.getDrawable(0);
        int selectedCategoryColor = a.getColor( 1, 0 );
        int selectedValueColor = a.getColor(2, 0);
        a.recycle();

        newHomeCategoryView.setImage( selectDrawable );
        newHomeCategoryView.setCategoryTextColor( selectedCategoryColor );
        newHomeCategoryView.setValueTextColor( selectedValueColor ) ;
    }

    public void unSelectCategoryView( NewHomeCategoryView newHomeCategoryView )
    {
        int[] attrs = new int[]{R.attr.newhomeCategoryNormal , R.attr.newhomeCategoryTextNormal , R.attr.newhomeCategoryValueTextNormal };
        TypedArray a = mContext.getTheme().obtainStyledAttributes(attrs);

        Drawable unSelectDrawable = a.getDrawable(0);
        int unSelectedCategoryColor = a.getColor( 1, 0 );
        int unSelectedValueColor = a.getColor(2, 0);
        a.recycle();

        newHomeCategoryView.setImage( unSelectDrawable );
        newHomeCategoryView.setCategoryTextColor( unSelectedCategoryColor );
        newHomeCategoryView.setValueTextColor( unSelectedValueColor );
    }

    public void setDayValue( NewHomeCategoryView categoryView , View dayView , NewHomeCategoryData categoryData , final int exerprogramID , int viewIndex  , int selectCategoryIndex , boolean startZero)
    {
        String valueText = null;
        String valueUnitText = null;
        String categoryText = null;
        CategoryType categoryType = NewHomeConstant.ViewOrder[viewIndex];
        int pointNumber = 0;

        dayView.setTag(viewIndex);

        switch( categoryType )
        {
            case AVERAGE_MET:
                pointNumber = 2;
                float averageMet = categoryData.getAverageMet();
                valueText = String.format("%.2f", averageMet);
                valueUnitText = "MET";
                categoryText = arrCategoryChar[categoryType.ordinal()];

                setDayTextViewData( categoryType , (NewHomeDayTextView) dayView, averageMet, valueUnitText, NewHomeUtils.getComment(mContext, categoryData.getAverageMetRank()) , pointNumber ,startZero);
                break;
            case EXERCISE_TIME:
                int exerciseTime = categoryData.getExerciseTime();

                valueText = String.format("%d", exerciseTime);
                valueUnitText = mContext.getResources().getString(R.string.common_minute);

                categoryText = mContext.getResources().getString( R.string.newhome_category_exercisetime_char );

                setDayTextViewData(categoryType, (NewHomeDayTextView) dayView, exerciseTime, valueUnitText, NewHomeUtils.getComment(mContext, categoryData.getExerciseTimeRank()) , pointNumber , startZero);
                break;
            case CALORIES:
                int calorie = categoryData.getCalories();
                valueText = String.format("%d", calorie);
                valueUnitText = mContext.getResources().getString(R.string.newhome_category_calorie_unit);
                categoryText = arrCategoryChar[categoryType.ordinal()];

                setDayTextViewData(categoryType, (NewHomeDayTextView) dayView, calorie , valueUnitText, NewHomeUtils.getComment(mContext, categoryData.getCaloriesRank()) , pointNumber , startZero );
                break;
            case DAILY_ACHIEVE:
                int dayAchieveValue = categoryData.getDayAchieve();
                categoryText = arrCategoryChar[categoryType.ordinal()];

                float strengthFrom = categoryData.getStrengthFrom();
                float strengthTo = categoryData.getStrengthTo();
                String strengthTypeUnitText = categoryData.getStrengthUnitText();

                int dayAchievePercent = (int) (( dayAchieveValue / strengthFrom ) * 100);
                if( dayAchievePercent > 100 ){
                    dayAchievePercent = 100;
                }
                valueUnitText = "%";
                valueText = dayAchievePercent + "";

                NewHomeDayHalfCircleView newHomeDayHalfCircleView = (NewHomeDayHalfCircleView) dayView;
                newHomeDayHalfCircleView.setCategoryText(arrCategoryChar[categoryType.ordinal()]);
                newHomeDayHalfCircleView.setRange(strengthFrom, strengthTo);
                newHomeDayHalfCircleView.setAchieveValueText(valueUnitText);
                newHomeDayHalfCircleView.setStrengthTypeUnitText(strengthTypeUnitText);
                newHomeDayHalfCircleView.setValue( dayAchieveValue );
                newHomeDayHalfCircleView.setExerciseProgramId(exerprogramID);
                newHomeDayHalfCircleView.setProgramClickeListener( this.mProgramClickListener );
                //애니메이션일 때
                //newHomeDayHalfCircleView.startAnimation( 1000 , startZero );
                newHomeDayHalfCircleView.display();
                break;
            case WEEKLY_ACHIEVE:
                int weekScore = categoryData.getWeekScore();

                valueText = String.format("%d", weekScore);
                valueUnitText = mContext.getResources().getString(R.string.newhome_category_weekly_point_unit);
                categoryText = arrCategoryChar[categoryType.ordinal()];

                NewHomeDayCircleView weekScoreDayCircleView = (NewHomeDayCircleView) dayView;
                weekScoreDayCircleView.setValueText( valueText );
                weekScoreDayCircleView.setValueUnitText(valueUnitText);
                weekScoreDayCircleView.setValue(weekScore);
                weekScoreDayCircleView.setExerciseProgramId(exerprogramID);
                weekScoreDayCircleView.setCategoryText(arrCategoryChar[categoryType.ordinal()]);
                weekScoreDayCircleView.setProgramClickeListener( this.mProgramClickListener );
                weekScoreDayCircleView.display();
                //애니메이션일 때
                //weekScoreDayCircleView.startAnimation( 1000 , startZero );
                break;
            case FAT:
                double fat = categoryData.getFat();

                if( fat >= 100 ){
                    valueText = String.format("%d", (int)fat);
                }else{
                    pointNumber = 2;
                    valueText = String.format("%.2f", (float)fat);
                }

                valueUnitText = "g";
                categoryText = arrCategoryChar[categoryType.ordinal()];

                setDayTextViewData(categoryType, (NewHomeDayTextView) dayView, Float.parseFloat(valueText) , valueUnitText, NewHomeUtils.getComment(mContext, categoryData.getFatRank()) , pointNumber ,startZero);
                break;
            case CARBOHYDRATE:
                double carbon = categoryData.getCarbohydrate();
                valueUnitText = "g";
                categoryText = arrCategoryChar[categoryType.ordinal()];

                if( carbon >= 100 ){
                    valueText = String.format("%d", (int)carbon);
                }else{
                    pointNumber = 2;
                    valueText = String.format("%.2f", (float)carbon );
                }

                setDayTextViewData(categoryType, (NewHomeDayTextView) dayView, Float.parseFloat(valueText) , valueUnitText, "" , pointNumber , startZero );
                break;
            case DISTANCE:
                double distance = categoryData.getDistance();
                pointNumber = 2;

                valueText = String.format("%.2f", (float)distance );
                valueUnitText = categoryData.getDistanceText();
                categoryText = arrCategoryChar[categoryType.ordinal()];

                setDayTextViewData(categoryType, (NewHomeDayTextView) dayView, Float.parseFloat(valueText) , valueUnitText, NewHomeUtils.getComment(mContext, categoryData.getDistanceRank()) , pointNumber ,startZero );
                break;
        }

        setCategoryViewData(categoryType, categoryView, viewIndex, valueText, valueUnitText, categoryText, selectCategoryIndex);
    }



    public void showProgramDialog( int exerciseProgramID )
    {

        //팝업 윈드우를 이용한다.
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View programView = inflater.inflate(R.layout.dialog_program, null);

        TextView tvProgramTitle = (TextView) programView.findViewById(R.id.tv_dialog_program_name);
        TextView tvProgramDescription = (TextView) programView.findViewById( R.id.tv_dialog_program_description );
        LinearLayout llProgramTime = (LinearLayout) programView.findViewById( R.id.ll_dialog_program_exercise_time);
        TextView tvProgramTime = (TextView) programView.findViewById( R.id.tv_dialog_program_exercise_time );
        TextView tvProgramTimes = (TextView) programView.findViewById( R.id.tv_dialog_program_times );
        ImageView imgProgramStrength = (ImageView) programView.findViewById( R.id.img_dialog_program_strength );
        TextView tvProgramStrengthTitle = (TextView) programView.findViewById( R.id.tv_dialog_program_strength_title );
        TextView tvProgramStrength = (TextView) programView.findViewById( R.id.tv_dialog_program_strength );

        String programName = null;
        String programInfo = null;

        ExerciseProgram program = mDBManger.getUserExerciProgram( exerciseProgramID );
        if( program.getDefaultProgram() ){

            int nameResID =  mContext.getResources().getIdentifier(program.getName(), "string", mContext.getPackageName());
            programName = mContext.getResources().getString(nameResID);
            int infoResID =  mContext.getResources().getIdentifier(program.getInfo(), "string", mContext.getPackageName());
            programInfo = mContext.getResources().getString(infoResID);

        }else{

            programName = program.getName();
            programInfo = program.getInfo();
        }

        tvProgramTitle.setText( programName );
        tvProgramDescription.setText( programInfo );

        StrengthInputType strengthType = StrengthInputType.getStrengthType( program.getCategory() );
        String strengthUnit = Utils.getStrengthUnit(strengthType);

        if( strengthType.equals( StrengthInputType.CALORIE ) || strengthType.equals( StrengthInputType.CALORIE_SUM ) || strengthType.equals( StrengthInputType.VS_BMR ) ){

            llProgramTime.setVisibility(View.GONE);
            imgProgramStrength.setImageResource(R.drawable.icon_calories);
            tvProgramStrengthTitle.setText(mContext.getResources().getString(R.string.applied_program_week_calorie));

        }else{

            int timeFrom = program.getMinuteFrom();
            int timeTo = program.getMinuteTo();

            String timeText = null;
            String timeUnit = mContext.getResources().getString(R.string.common_minute);
            if( timeFrom == timeTo ){
                timeText = timeFrom  + timeUnit;
            }else{
                timeText = timeFrom + timeUnit + "-" + timeTo + timeUnit;
            }

            tvProgramTime.setText( timeText );
        }

        int strengthFrom= (int) program.getStrengthFrom();
        int strengthTo= (int) program.getStrengthTo();

        String strengthRageText = null;
        if( strengthFrom == strengthTo ){
            if(strengthType.equals( StrengthInputType.STRENGTH ) ){

                String[] arrStrengthType = mContext.getResources().getStringArray( R.array.exercise_strength_type_char );
                strengthRageText = arrStrengthType[strengthFrom] + Utils.getStrengthUnit(strengthType);

            }else {
                strengthRageText = strengthFrom + Utils.getStrengthUnit(strengthType);
            }
        }
        else{
            if(strengthType.equals( StrengthInputType.STRENGTH ) ){
                String[] arrStrengthType = mContext.getResources().getStringArray( R.array.exercise_strength_type_char );
                strengthRageText = arrStrengthType[strengthFrom] + "-" + arrStrengthType[strengthTo] + " " + strengthUnit;

            }else {
                strengthRageText = strengthFrom + "-"+ strengthTo + " " +strengthUnit;
            }
        }
        tvProgramStrength.setText(strengthRageText);

        int timesFrom = program.getTimesFrom();
        int timesTo = program.getTimesTo();
        String timesText = null;
        String timesUnit = mContext.getResources().getString(R.string.common_day).toLowerCase();
        if( timesFrom == timesTo ){
            timesText = timesFrom + timesUnit;
        }else{
            timesText = timesFrom + timesUnit + "-" + timesTo + timesUnit;
        }
        tvProgramTimes.setText(timesText);

        programPopup = new PopupWindow( programView , WindowManager.LayoutParams.MATCH_PARENT , WindowManager.LayoutParams.MATCH_PARENT);
        WindowManager windowManager = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);

//        Point p = new Point();
//        windowManager.getDefaultDisplay().getSize( p );
//        int screenWidth = p.x;
//        int screenHeight = p.y;
//        int x = screenWidth / 2 - programView.getMeasuredWidth() / 2;
//        int y = screenHeight / 2 - programView.getMeasuredHeight() / 2;

        programPopup.setOutsideTouchable(true);
        programPopup.setTouchable(true);
        programPopup.setBackgroundDrawable(new BitmapDrawable()) ;
        programPopup.setTouchInterceptor(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                programPopup.dismiss();
                return true;
            }
        });
        programPopup.setAnimationStyle(-1);
        //programPopup.showAtLocation(programView, Gravity.NO_GRAVITY, x, y);
        programPopup.showAtLocation(programView, Gravity.CENTER, 0 , 0 );
    }

    public View getDefaultNewsView(){
        ImageView imgNews = new ImageView( mContext );
        imgNews.setLayoutParams( (new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)) );

        //테스트로 넣은것임.
        imgNews.setBackground( mContext.getResources().getDrawable( R.drawable.default_news ) );

        return imgNews;
    }

}
