package com.fitdotlife.fitmate.newhome;

import com.fitdotlife.fitmate_lib.key.RangeType;
import com.fitdotlife.fitmate_lib.object.DayActivity;

/**
 * Created by Joshua on 2016-02-22.
 */
public class WeekPagerActivityInfo {

    private String weekStartDate = "";
    private String weekRange = "";
    private String[] dayStrings = new String[7];
    private long[] timeMillis = new long[7];
    private int selectedDayIndex = -1;
    private int preMonth = -1;
    private float strengthFrom = 0;
    private float strengthTo = 0;
    private RangeType rangeType = null;
    private double[] exerciseTimeValues = null;
    private double[] calorieValues = null;
    private double[] dailyAchieveValues = null;
    private double[] weeklyAchieveValues = null;
    private double[] fatValues = null;
    private double[] carboHydrateValues = null;
    private double[] distanceValues = null;


    public WeekPagerActivityInfo(String weekStartDate ,String weekRange, String[] dayStrings, long[] timeMillis , int preMonth, float strengthFrom, float strengthTo ,
                                 double[] exerciseTimeValues , double[] calorieValues , double[] dailyAchieveValues , double[] weeklyAchieveValues , double[] fatValues , double[] carboHydrateValues , double[] distanceValues) {
        this.weekStartDate = weekStartDate;
        this.dayStrings = dayStrings;
        this.preMonth = preMonth;
        //this.selectedDayIndex = selectedDayIndex;
        this.strengthFrom = strengthFrom;
        this.strengthTo = strengthTo;
        this.timeMillis = timeMillis;
        this.weekRange = weekRange;

        this.exerciseTimeValues = exerciseTimeValues;
        this.calorieValues = calorieValues;
        this.dailyAchieveValues = dailyAchieveValues;
        this.weeklyAchieveValues = weeklyAchieveValues;
        this.fatValues = fatValues;
        this.carboHydrateValues = carboHydrateValues;
        this.distanceValues = distanceValues;

        if( strengthFrom == strengthTo ){
            this.rangeType = RangeType.ONE_RANGE;
        }else{
            this.rangeType = RangeType.TWO_RANGE;
        }
    }

    public WeekPagerActivityInfo(String weekStartDate ,String weekRange, String[] dayStrings, long[] timeMillis , int preMonth, float strengthFrom, float strengthTo ) {
        this.weekStartDate = weekStartDate;
        this.dayStrings = dayStrings;
        this.preMonth = preMonth;
        this.strengthFrom = strengthFrom;
        this.strengthTo = strengthTo;
        this.timeMillis = timeMillis;
        this.weekRange = weekRange;
    }

    public WeekPagerActivityInfo(String weekStartDate ,String weekRange, String[] dayStrings, long[] timeMillis , int preMonth ) {
        this.weekStartDate = weekStartDate;
        this.dayStrings = dayStrings;
        this.preMonth = preMonth;
        this.timeMillis = timeMillis;
        this.weekRange = weekRange;
    }

    public double[] getValues( CategoryType categoryType ){
        double[] values = null;

        switch( categoryType ){
            case EXERCISE_TIME:
                values = exerciseTimeValues;
                break;
            case CALORIES:
                values = calorieValues;
                break;
            case DAILY_ACHIEVE:
                values = dailyAchieveValues;
                break;
            case WEEKLY_ACHIEVE:
                values = weeklyAchieveValues;
                break;
            case FAT:
                values = fatValues;
                break;
            case CARBOHYDRATE:
                values = carboHydrateValues;
                break;
            case DISTANCE:
                values = distanceValues;
                break;
        }
        return values;
    }


    public void setValues( CategoryType categoryType , double[] values ){
        switch( categoryType ){
            case EXERCISE_TIME:
                exerciseTimeValues = values;
                break;
            case CALORIES:
                calorieValues = values;
                break;
            case DAILY_ACHIEVE:
                dailyAchieveValues = values;
                break;
            case WEEKLY_ACHIEVE:
                weeklyAchieveValues = values;
                break;
            case FAT:
                fatValues = values;
                break;
            case CARBOHYDRATE:
                carboHydrateValues = values;
                break;
            case DISTANCE:
                distanceValues = values;
                break;
        }
    }

    public String[] getDayStrings() {
        return dayStrings;
    }

    public int getPreMonth() {
        return preMonth;
    }

    public float getStrengthFrom() {
        return strengthFrom;
    }

    public float getStrengthTo() {
        return strengthTo;
    }

    public long[] getTimeMillis() {
        return timeMillis;
    }

    public String getWeekRange() {
        return weekRange;
    }

    public String getWeekStartDate() {
        return weekStartDate;
    }

    public void setStrengthFrom(float strengthFrom) {
        this.strengthFrom = strengthFrom;
    }

    public void setStrengthTo(float strengthTo) {
        this.strengthTo = strengthTo;
    }

    public RangeType getRangeType() {
        return rangeType;
    }

    public void updateData( int[] childIndexList , DayActivity[] dayActivityList )
    {
        for (int i = 0; i < childIndexList.length ; i++)
        {

            int childIndex = childIndexList[i];
            DayActivity dayActivity = dayActivityList[i];

            if (dayActivity != null)
            {
                exerciseTimeValues[childIndex] = dayActivity.getStrengthHigh() + dayActivity.getStrengthMedium();
                calorieValues[childIndex] = dayActivity.getCalorieByActivity();
                dailyAchieveValues[childIndex] = dayActivity.getAchieveOfTarget();
                weeklyAchieveValues[childIndex] = dayActivity.getTodayScore();
                fatValues[childIndex] = dayActivity.getFat();
                carboHydrateValues[childIndex] = dayActivity.getCarbohydrate();
                distanceValues[childIndex] = dayActivity.getDistance();
            }
        }
    }

}
