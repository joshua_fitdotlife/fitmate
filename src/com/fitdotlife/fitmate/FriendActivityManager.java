package com.fitdotlife.fitmate;

import android.app.Activity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Joshua on 2015-09-02.
 */
public class FriendActivityManager {

    public static final FriendActivityManager friendActivityManager = new FriendActivityManager();

    private List<Activity> listActivity = null;

    private FriendActivityManager()
    {
        listActivity = new ArrayList<Activity>();
    }

    public static FriendActivityManager getInstance()
    {
        return friendActivityManager;
    }

    //custom method
    //add activity
    public void addActivity(Activity activity)
    {
        listActivity.add(activity);
    }

    //remove activity
    public boolean removeActivity(Activity activity)
    {
        return listActivity.remove(activity);
    }

    public void removeAllActivity(){
        listActivity.clear();
    }

    //all activity finish
    public void finishAllActivity()
    {
        for(Activity activity : listActivity)
        {
            activity.finish();
        }
    }

    //Getter, Setter
    public List<Activity> getListActivity()
    {
        return listActivity ;
    }

    public void setListActivity(ArrayList<Activity> listActivity)
    {
        this.listActivity = listActivity ;
    }

}
