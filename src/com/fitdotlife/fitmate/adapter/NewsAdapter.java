package com.fitdotlife.fitmate.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.fitdotlife.fitmate.NewsActivity;
import com.fitdotlife.fitmate_lib.customview.NewsListItemView;
import com.fitdotlife.fitmate_lib.customview.NewsListItemView_;
import com.fitdotlife.fitmate_lib.object.News;

import java.util.List;

/**
 * Created by Joshua on 2015-08-13.
 */
public class NewsAdapter extends AbstractBaseAdapter{

    private Context mContext = null;

    public NewsAdapter(Context context, int layoutResource, List<?> list ) {
        super(context, layoutResource, list);
        mContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        NewsListItemView newsListItemView;
        if( convertView == null ){
            newsListItemView = NewsListItemView_.build(mContext);
        }else{
            newsListItemView = (NewsListItemView) convertView;
        }

        newsListItemView.bind((News) this.mList.get(position));
        return newsListItemView;
    }
}
