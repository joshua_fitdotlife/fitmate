package com.fitdotlife.fitmate.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fitdotlife.fitmate.R;
import com.fitdotlife.fitmate_lib.object.ExerciseProgram;

import java.util.List;

public class ExerciseProgramListAdapter extends AbstractBaseAdapter {

	public ExerciseProgramListAdapter(Context context, int layoutResource, List<?> list)
	{
		super(context, layoutResource, list);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
        ExerciseProgram exerciseProgramSNU = (ExerciseProgram) mList.get(position);
        if( convertView == null )
        {
            convertView = this.mInflater.inflate(mResource , null);
        }

        if(exerciseProgramSNU != null)
        {
            TextView tvName = (TextView) convertView.findViewById(R.id.tv_listview_item_activity_exercisehome_exerciseprogram_name);
            TextView tvOwner = (TextView) convertView.findViewById(R.id.tv_listview_item_activity_exercisehome_exerciseprogram_owner);
            TextView tvRecommendNumber = (TextView) convertView.findViewById(R.id.tv_listview_item_activity_exercisehome_exerciseprogram_recommendnumber);

            tvName.setText( exerciseProgramSNU.getName() );
            tvRecommendNumber.setText( String.valueOf( exerciseProgramSNU.getRecommendNumber() ) );
            convertView.setTag( exerciseProgramSNU.getId());
        }

        return convertView;

	}

}
