package com.fitdotlife.fitmate.firmware;

import java.util.List;
import java.util.UUID;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

/**
 * Created by Joshua on 2015-11-10.
 */
public class SmartNaviService extends Service{

    private final static String TAG = SmartNaviService.class.getSimpleName();

    private BluetoothManager mBluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;
    private String mBluetoothDeviceAddress;
    private BluetoothGatt mBluetoothGatt;
    private int mConnectionState = STATE_DISCONNECTED;

    private static final int STATE_DISCONNECTED = 0;
    private static final int STATE_CONNECTING = 1;
    private static final int STATE_CONNECTED = 2;

    public final static String ACTION_GATT_CONNECTED            = "com.enmsystem.SRC.ACTION_GATT_CONNECTED";
    public final static String ACTION_GATT_DISCONNECTED         = "com.enmsystem.SRC.ACTION_GATT_DISCONNECTED";
    public final static String ACTION_GATT_SERVICES_DISCOVERED  = "com.enmsystem.SRC.ACTION_GATT_SERVICES_DISCOVERED";
    public final static String ACTION_DATA_AVAILABLE            = "com.enmsystem.SRC.ACTION_DATA_AVAILABLE";
    public final static String EXTRA_DATA                       = "com.enmsystem.SRC.EXTRA_DATA";
    public final static String DEVICE_DOES_NOT_SUPPORT_SRC      = "com.enmsystem.SRC.DEVICE_DOES_NOT_SUPPORT_SRC";

    public final static String BATTERY_VALUE                    = "com.enmsystem.SRC.BATTERY_VALUE";
    public final static String KEY_INPUT_VALUE                  = "com.enmsystem.SRC.KEY_INPUT_VALUE";
    public final static String LED_INPUT_DATA                   = "com.enmsystem.SRC.LED_INPUT_DATA";
    public final static String VERSION_INPUT_DATA               = "com.enmsystem.SRC.VERSION_INPUT_DATA";
    public final static String RSSI_VALUE                       = "com.enmsystem.SRC.RSSI_VALUE";

    public static final UUID TX_POWER_UUID = UUID.fromString("00001804-0000-1000-8000-00805f9b34fb");
    public static final UUID TX_POWER_LEVEL_UUID = UUID.fromString("00002a07-0000-1000-8000-00805f9b34fb");
    public static final UUID CCCD = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
    public static final UUID FIRMWARE_REVISON_UUID = UUID.fromString("00002a26-0000-1000-8000-00805f9b34fb");
    public static final UUID DIS_UUID = UUID.fromString("0000180a-0000-1000-8000-00805f9b34fb");

    public static final UUID RX_SERVICE_UUID = UUID.fromString("6e400001-b5a3-f393-e0a9-e50e24dcca9e");
    public static final UUID RX_CHAR_UUID = UUID.fromString("6e400002-b5a3-f393-e0a9-e50e24dcca9e");
    public static final UUID TX_CHAR_UUID = UUID.fromString("6e400003-b5a3-f393-e0a9-e50e24dcca9e");


    public static final UUID BATTERY_SERVICE = UUID.fromString("0000180F-0000-1000-8000-00805f9b34fb");
    public static final UUID BATTERY_CHARACTERISTIC = UUID.fromString("00002A19-0000-1000-8000-00805f9b34fb");

    public static final UUID SMART_NAVI_REMOCON_SERVICE = UUID.fromString("6e400001-b5a3-f393-e0a9-e50e24dcca9e");
    public static final UUID SMART_NAVI_REMOCON_RX_CHARACTERISTIC = UUID.fromString("6e400002-b5a3-f393-e0a9-e50e24dcca9e");
    public static final UUID SMART_NAVI_REMOCON_TX_CHARACTERISTIC = UUID.fromString("6e400003-b5a3-f393-e0a9-e50e24dcca9e");


    // Implements callback methods for GATT events that the app cares about.  For example,
    // connection change and services discovered.
    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {

        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            String intentAction;

            if (newState == BluetoothProfile.STATE_CONNECTED) {
                intentAction = ACTION_GATT_CONNECTED;
                mConnectionState = STATE_CONNECTED;
                broadcastUpdate(intentAction);
                Log.i(TAG, "Connected to GATT server.");
                // Attempts to discover services after successful connection.
                Log.i(TAG, "Attempting to start service discovery:" +
                        mBluetoothGatt.discoverServices());

            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                intentAction = ACTION_GATT_DISCONNECTED;
                mConnectionState = STATE_DISCONNECTED;
                Log.i(TAG, "Disconnected from GATT server.");
                broadcastUpdate(intentAction);
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                Log.w(TAG, "mBluetoothGatt = " + mBluetoothGatt );

                broadcastUpdate(ACTION_GATT_SERVICES_DISCOVERED);
            } else {
                Log.w(TAG, "onServicesDiscovered received: " + status);
            }
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt,BluetoothGattCharacteristic characteristic, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt,BluetoothGattCharacteristic characteristic) {
            broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
        }

        @Override
        public void onReadRemoteRssi(BluetoothGatt gatt, int rssi, int status) {

            final Intent intent = new Intent(ACTION_DATA_AVAILABLE);

//            //;
//            Log.d(TAG, "onReadRemoteRssi() - address [" + gatt.getDevice().getAddress() + "], rssi = " + rssi);

            intent.putExtra(RSSI_VALUE, Integer.toString(rssi));

            sendBroadcast(intent);

            super.onReadRemoteRssi(gatt, rssi, status);
        }
    };

    private void broadcastUpdate(final String action) {
        final Intent intent = new Intent(action);
        sendBroadcast(intent);
    }

    private void broadcastUpdate(final String action,
                                 final BluetoothGattCharacteristic characteristic) {
        final Intent intent = new Intent(action);

        // This is special handling for the Heart Rate Measurement profile.  Data parsing is
        // carried out as per profile specifications:
        // http://developer.bluetooth.org/gatt/characteristics/Pages/CharacteristicViewer.aspx?u=org.bluetooth.characteristic.heart_rate_measurement.xml
        if (TX_CHAR_UUID.equals(characteristic.getUuid())) {

            // Log.d(TAG, String.format("Received TX: %d",characteristic.getValue() ));
            intent.putExtra(EXTRA_DATA, characteristic.getValue());

        } else if (BATTERY_CHARACTERISTIC.equals(characteristic.getUuid())) {

            int battery = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0);
            Log.d(TAG, String.format("battery value : %d", battery ));

            intent.putExtra(BATTERY_VALUE, battery);

        } else if (SMART_NAVI_REMOCON_TX_CHARACTERISTIC.equals(characteristic.getUuid())) {

            int cmd = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0);


            if(cmd == 0x00) {
                intent.putExtra(KEY_INPUT_VALUE, characteristic.getValue());
            } else if(cmd == 0x32 ) {
                intent.putExtra(LED_INPUT_DATA, characteristic.getValue());
            } else if(cmd == 0x42 ) {
                intent.putExtra(VERSION_INPUT_DATA, characteristic.getValue());
            }
        }
        sendBroadcast(intent);
    }

    public class LocalBinder extends Binder {
        SmartNaviService getService() {
            return SmartNaviService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        // After using a given device, you should make sure that BluetoothGatt.close() is called
        // such that resources are cleaned up properly.  In this particular example, close() is
        // invoked when the UI is disconnected from the Service.
        close();
        return super.onUnbind(intent);
    }

    private final IBinder mBinder = new LocalBinder();

    /**
     * Initializes a reference to the local Bluetooth adapter.
     *
     * @return Return true if the initialization is successful.
     */
    public boolean initialize() {
        // For API level 18 and above, get a reference to BluetoothAdapter through
        // BluetoothManager.
        if (mBluetoothManager == null) {
            mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            if (mBluetoothManager == null) {
                Log.e(TAG, "Unable to initialize BluetoothManager.");
                return false;
            }
        }

        mBluetoothAdapter = mBluetoothManager.getAdapter();
        if (mBluetoothAdapter == null) {
            Log.e(TAG, "Unable to obtain a BluetoothAdapter.");
            return false;
        }

        return true;
    }

    /**
     * Connects to the GATT server hosted on the Bluetooth LE device.
     *
     * @param address The device address of the destination device.
     *
     * @return Return true if the connection is initiated successfully. The connection result
     *         is reported asynchronously through the
     *         {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     *         callback.
     */
    public boolean connect(final String address) {
        if (mBluetoothAdapter == null || address == null) {
            Log.w(TAG, "BluetoothAdapter not initialized or unspecified address.");
            return false;
        }

        // Previously connected device.  Try to reconnect.
        if (mBluetoothDeviceAddress != null && address.equals(mBluetoothDeviceAddress)
                && mBluetoothGatt != null) {
            Log.d(TAG, "Trying to use an existing mBluetoothGatt for connection.");
            if (mBluetoothGatt.connect()) {
                mConnectionState = STATE_CONNECTING;
                return true;
            } else {
                return false;
            }
        }

        final BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        if (device == null) {
            Log.w(TAG, "Device not found.  Unable to connect.");
            return false;
        }
        // We want to directly connect to the device, so we are setting the autoConnect
        // parameter to false.
        mBluetoothGatt = device.connectGatt(this, true, mGattCallback);
        Log.d(TAG, "Trying to create a new connection.");
        mBluetoothDeviceAddress = address;
        mConnectionState = STATE_CONNECTING;
        return true;
    }

    /**
     * Disconnects an existing connection or cancel a pending connection. The disconnection result
     * is reported asynchronously through the
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    public void disconnect() {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.disconnect();
        // mBluetoothGatt.close();
    }

    /**
     * After using a given BLE device, the app must call this method to ensure resources are
     * released properly.
     */
    public void close() {
        if (mBluetoothGatt == null) {
            return;
        }
        Log.w(TAG, "mBluetoothGatt closed");
        mBluetoothDeviceAddress = null;
        mBluetoothGatt.close();
        mBluetoothGatt = null;
    }

    /**
     * Request a read on a given {@code BluetoothGattCharacteristic}. The read result is reported
     * asynchronously through the {@code BluetoothGattCallback#onCharacteristicRead(android.bluetooth.BluetoothGatt, android.bluetooth.BluetoothGattCharacteristic, int)}
     * callback.
     *
     * @param characteristic The characteristic to read from.
     */
    public void readCharacteristic(BluetoothGattCharacteristic characteristic) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.readCharacteristic(characteristic);
    }

    /**
     * Enables or disables notification on a give characteristic.
     *
     * @param characteristic Characteristic to act on.
     * @param enabled If true, enable notification.  False otherwise.
     */
    /*
    public void setCharacteristicNotification(BluetoothGattCharacteristic characteristic,
                                              boolean enabled) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.setCharacteristicNotification(characteristic, enabled);


        if (UUID_HEART_RATE_MEASUREMENT.equals(characteristic.getUuid())) {
            BluetoothGattDescriptor descriptor = characteristic.getDescriptor(
                    UUID.fromString(SampleGattAttributes.CLIENT_CHARACTERISTIC_CONFIG));
            descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
            mBluetoothGatt.writeDescriptor(descriptor);
        }
    }*/


    /**
     * Enable Key Button Notification
     *
     */
    public void enableKeyInputNotification() {

        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "enableKeyInputNotification BluetoothAdapter not initialized");
            return;
        }

        BluetoothGattService KeyInputService = mBluetoothGatt.getService(SMART_NAVI_REMOCON_SERVICE);
        if (KeyInputService == null) {
            showMessage("key input service not found!");
            broadcastUpdate(DEVICE_DOES_NOT_SUPPORT_SRC);
            return;
        }
        BluetoothGattCharacteristic KeyInputChar = KeyInputService.getCharacteristic(SMART_NAVI_REMOCON_TX_CHARACTERISTIC);
        if (KeyInputChar == null) {
            showMessage("Key input charateristic not found!");
            broadcastUpdate(DEVICE_DOES_NOT_SUPPORT_SRC);
            return;
        }
        mBluetoothGatt.setCharacteristicNotification(KeyInputChar,true);

        BluetoothGattDescriptor descriptor = KeyInputChar.getDescriptor(CCCD);
        descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
        mBluetoothGatt.writeDescriptor(descriptor);
    }

    public void writeRemoconCharacteristic(byte[] value) {

        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "enableKeyInputNotification BluetoothAdapter not initialized");
            return;
        }

        BluetoothGattService RemoconService = mBluetoothGatt.getService(SMART_NAVI_REMOCON_SERVICE);
        if (RemoconService == null) {
            showMessage("key input service not found!");
            broadcastUpdate(DEVICE_DOES_NOT_SUPPORT_SRC);
            return;
        }

        BluetoothGattCharacteristic RemoconChar = RemoconService.getCharacteristic(SMART_NAVI_REMOCON_RX_CHARACTERISTIC);
        if (RemoconChar == null) {
            showMessage("control rx charateristic not found!");
            broadcastUpdate(DEVICE_DOES_NOT_SUPPORT_SRC);
            return;
        }

        RemoconChar.setValue(value);
        boolean status = mBluetoothGatt.writeCharacteristic(RemoconChar);

        //Log.d(TAG, "write RemoconChar - status=" + status);
//        Log.i("KEY_VAL", "SEND " + value[1]);
    }


    /**
     * Enabl Battery Notification
     *
     */
    public void enableBatteryNotification() {

        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "enableKeyInputNotification BluetoothAdapter not initialized");
            return;
        }

        BluetoothGattService BatteryService = mBluetoothGatt.getService(BATTERY_SERVICE);
        if (BatteryService == null) {
            showMessage("Battery service not found!");
            broadcastUpdate(DEVICE_DOES_NOT_SUPPORT_SRC);
            return;
        }
        BluetoothGattCharacteristic BatteryChar = BatteryService.getCharacteristic(BATTERY_CHARACTERISTIC);
        if (BatteryChar == null) {
            showMessage("Battery charateristic not found!");
            broadcastUpdate(DEVICE_DOES_NOT_SUPPORT_SRC);
            return;
        }
        mBluetoothGatt.setCharacteristicNotification(BatteryChar,true);

        BluetoothGattDescriptor descriptor = BatteryChar.getDescriptor(CCCD);
        descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
        mBluetoothGatt.writeDescriptor(descriptor);
    }


//    /**
//     * Enable TXNotification
//     *
//     * @return
//     */
//    public void enableTXNotification()
//    {
//    	/*
//    	if (mBluetoothGatt == null) {
//    		showMessage("mBluetoothGatt null" + mBluetoothGatt);
//    		broadcastUpdate(DEVICE_DOES_NOT_SUPPORT_SRC);
//    		return;
//    	}
//    		*/
//    	BluetoothGattService RxService = mBluetoothGatt.getService(RX_SERVICE_UUID);
//    	if (RxService == null) {
//            showMessage("Rx service not found!");
//            broadcastUpdate(DEVICE_DOES_NOT_SUPPORT_SRC);
//            return;
//        }
//    	BluetoothGattCharacteristic TxChar = RxService.getCharacteristic(TX_CHAR_UUID);
//        if (TxChar == null) {
//            showMessage("Tx charateristic not found!");
//            broadcastUpdate(DEVICE_DOES_NOT_SUPPORT_SRC);
//            return;
//        }
//        mBluetoothGatt.setCharacteristicNotification(TxChar,true);
//
//        BluetoothGattDescriptor descriptor = TxChar.getDescriptor(CCCD);
//        descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
//        mBluetoothGatt.writeDescriptor(descriptor);
//    }
//
//
//
//    public void writeRXCharacteristic(byte[] value)
//    {
//    	BluetoothGattService RxService = mBluetoothGatt.getService(RX_SERVICE_UUID);
//    	showMessage("mBluetoothGatt null"+ mBluetoothGatt);
//    	if (RxService == null) {
//            showMessage("Rx service not found!");
//            broadcastUpdate(DEVICE_DOES_NOT_SUPPORT_SRC);
//            return;
//        }
//    	BluetoothGattCharacteristic RxChar = RxService.getCharacteristic(RX_CHAR_UUID);
//        if (RxChar == null) {
//            showMessage("Rx charateristic not found!");
//            broadcastUpdate(DEVICE_DOES_NOT_SUPPORT_SRC);
//            return;
//        }
//        RxChar.setValue(value);
//    	boolean status = mBluetoothGatt.writeCharacteristic(RxChar);
//
//        Log.d(TAG, "write TXchar - status=" + status);
//    }

    private void showMessage(String msg) {
        Log.e(TAG, msg);
    }

    /**
     * Retrieves a list of supported GATT services on the connected device. This should be
     * invoked only after {@code BluetoothGatt#discoverServices()} completes successfully.
     *
     * @return A {@code List} of supported services.
     */
    public List<BluetoothGattService> getSupportedGattServices() {
        if (mBluetoothGatt == null) return null;

        return mBluetoothGatt.getServices();
    }

    public boolean readRssi() {
        if (mBluetoothGatt == null) return false;

        mBluetoothGatt.readRemoteRssi();
        return true;
    }

}
