package com.fitdotlife.fitmate.firmware;

import android.app.Notification;
import android.app.NotificationManager;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.fitdotlife.fitmate.R;

import java.lang.reflect.Method;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class FirmUpdateActivity extends Activity {

    private static final String TAG = "MainActivity";
    private static final boolean D = true;

    public static final String EXTRA_OPEN_DFU = "no.nordicsemi.android.nrtbeacon.extra.open_dfu";
    // public final static String FILE_PATH=
    // "file:///android_asset/src100_sdk441.hex";
    Context mContext = null;

    private static final byte SEND_KEYINPUT_NOTI    = (byte)0x01;
    private static final byte SEND_BATTERY_NOTI     = (byte)0x02;
    private static final byte SEND_LINK_LOSS_WRITE  = (byte)0x04;
    private static final byte SEND_REMOCON_WRITE    = (byte)0x08;

    private ProgressBar mProgressBar;
    private TextView address,tv_progress;
    public static TextView conn;

    private BluetoothAdapter mBluetoothAdapter;

    ArrayList<BluetoothModel> main_List = null;
    ArrayAdapter<BluetoothModel> main_Adapter = null;

    ListView srcListView = null;
    ListView dfuListView = null;
    private DeviceAdapter srcDeviceAdapter;
    private DeviceAdapter dfuDeviceAdapter;
    List<BluetoothDevice> srcDeviceList;
    List<BluetoothDevice> dfuDeviceList;
    Map<String, Integer> devRssiValues;

    private String mFilePath;
    private Uri mFileStreamUri;
    private String mInitFilePath;
    private Uri mInitFileStreamUri;
    private int mFileType;
    private int mFileTypeTmp; // This value is being used when user is selecting
    // a file not to overwrite the old value (in
    // case he/she will cancel selecting file)

    private SmartNaviService mService = null;
    private BluetoothDevice mDevice = null;

    //    private Button btn_bootmode = null;
    private SharedPreferences preferences = null;

    boolean  mIsUploading = false;
    boolean  mIsBootMode = false;

    private static final String NBEECON_UPDATE_SHAREDPREFERENCES = "nbeecon_update_sharedpreferences";
    private static final String ADDRESS = "preferences_address";

    private final BroadcastReceiver mDfuUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, final Intent intent) {
            // DFU is in progress or an error occurred
            final String action = intent.getAction();

            if (DfuService.BROADCAST_PROGRESS.equals(action)) {
                final int progress = intent.getIntExtra(DfuService.EXTRA_DATA,0);
                Log.i("mDfuUpdateReceiver", "Progress : " + progress);
                updateProgressBar(progress, false);
            } else if (DfuService.BROADCAST_ERROR.equals(action)) {
                final int error = intent.getIntExtra(DfuService.EXTRA_DATA, 0);
                updateProgressBar(error, true);

                Log.i("mDfuUpdateReceiver", "Error : " + error);

                // We have to wait a bit before canceling notification. This is
                // called before DfuService creates the last notification.
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // if this activity is still open and upload process was
                        // completed, cancel the notification
                        final NotificationManager manager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
                        manager.cancel(DfuService.NOTIFICATION_ID);
                    }
                }, 200);
            }
            //*********************//
            if (action.equals(SmartNaviService.ACTION_GATT_CONNECTED)) {
                Log.d(TAG, "mHandler.");
                runOnUiThread(new Runnable() {
                    public void run() {
                    }
                });
            }

            //*********************//
            if (action.equals(SmartNaviService.ACTION_GATT_DISCONNECTED)) {
                Log.d(TAG, "mHandler.SMARTNAVI_DISCONNECT_MSG");
                runOnUiThread(new Runnable() {
                    public void run() {
                    }
                });
            }


            //*********************//
            if (action.equals(SmartNaviService.ACTION_GATT_SERVICES_DISCOVERED)) {
                Log.d(TAG, "mHandler.SMARTNAVI_READY_MSG");
                runOnUiThread(new Runnable() {
                    public void run() {
                    }
                });
            }
            //*********************//
            if (action.equals(SmartNaviService.DEVICE_DOES_NOT_SUPPORT_SRC)){
//                showMessage("Device doesn't support SRC-100. Disconnecting");
                mService.disconnect();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_firm_update);

        mContext = getApplicationContext();

        final BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        populateList();
        service_init();

        //Dfu 라이브러리로 보내는 값
        mFileType = DfuService.TYPE_APPLICATION;
        mFilePath = "src100_sdk441_151.hex";//파일 위치
        mFileStreamUri = null;
        mInitFilePath = null;
        mInitFileStreamUri = null;

//		mBluetoothAdapter.startLeScan(mLeScanCallback);

        mProgressBar = (ProgressBar) findViewById(R.id.progress);
        address = (TextView) findViewById(R.id.address);
        conn = (TextView) findViewById(R.id.conn);
        tv_progress = (TextView) findViewById(R.id.tv_progress);

        preferences = mContext.getSharedPreferences(NBEECON_UPDATE_SHAREDPREFERENCES, Context.MODE_PRIVATE);
        address.setText(preferences.getString(ADDRESS, "연결된 디바이스 없음"));

        Button btn_refresh = (Button) findViewById(R.id.btn_refresh);
        btn_refresh.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ListRefresh();
            }
        });

        CheckBox btn_scan_on_off = (CheckBox) findViewById(R.id.btn_scan_on_off);
        btn_scan_on_off.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                final  boolean f = isChecked;

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        if(f){
                            mBluetoothAdapter.startLeScan(mLeScanCallback);
                        }else{
                            mBluetoothAdapter.stopLeScan(mLeScanCallback);
                        }
                    }
                }).start();


            }
        });

        CheckBox ch_bluetooth_on_off = (CheckBox) findViewById(R.id.btn_bluetooth_on_off);
        ch_bluetooth_on_off.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    BluetoothAdapter.getDefaultAdapter().enable();
                }else{
                    BluetoothAdapter.getDefaultAdapter().disable();
                }
            }
        });

        if(BluetoothAdapter.getDefaultAdapter().getState() == BluetoothAdapter.STATE_ON){
            ch_bluetooth_on_off.setChecked(true);
        }else if(BluetoothAdapter.getDefaultAdapter().getState() == BluetoothAdapter.STATE_OFF){
            ch_bluetooth_on_off.setChecked(false);
        }

        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        final LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(FirmUpdateActivity.this);
        broadcastManager.registerReceiver(mDfuUpdateReceiver, makeDfuUpdateIntentFilter());
    }

    private static IntentFilter makeDfuUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(DfuService.BROADCAST_PROGRESS);
        intentFilter.addAction(DfuService.BROADCAST_ERROR);
        return intentFilter;
    }

    @Override
    protected void onPause() {
        super.onPause();
        final LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(FirmUpdateActivity.this);
        broadcastManager.unregisterReceiver(mDfuUpdateReceiver);
    }

    @Override
    protected void onDestroy() {
        // SmartNaviService 서비스 bind 해제
        unbindService(mServiceConnection);
        // BLEStatusChangeReceiver 등록 해제
        unregisterReceiver(BLEStatusChangeReceiver);
        // SmartNaviService 서비스 stop
        mBluetoothAdapter.stopLeScan(mLeScanCallback);
        stopService(new Intent(this, SmartNaviService.class));
        super.onDestroy();
    }

    private void setBootMode(){
        if(mIsBootMode == true)
            return;


        mIsBootMode = true;

        tv_progress.setText("2. BOOT MODE 진입");

        try {
            Thread.sleep(200);
            byte[] value = new byte[4];
            value[0] = (byte)0xf5;
            value[1] = (byte)0xb2;
            value[2] = (byte)0xb2;
            value[3] = (byte)0xfa;

            mService.writeRemoconCharacteristic(value);
            mIsUploading = false;

            Thread.sleep(2000);
            mService.disconnect();

            Set<BluetoothDevice> pairedDevices = BluetoothAdapter.getDefaultAdapter().getBondedDevices();
            if (pairedDevices.size() > 0) {
                for (BluetoothDevice device : pairedDevices) {
                    try {
                        if(mDevice != null && device.getAddress() != null && device.getAddress().equals(mDevice.getAddress())) {
                            Log.i(TAG, "bluetooth remove bond name = [" + device.getName() + "]");
                            unpairDevice(BluetoothAdapter.getDefaultAdapter().getRemoteDevice(device.getAddress()));
                            break;
                        }
                    } catch (NullPointerException e) {
                        Log.e(TAG, "device.getName() == null");
                    }
                }
            }

            BluetoothOnOff();

        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void ListRefresh(){
        srcDeviceList.clear();
        srcDeviceAdapter.notifyDataSetChanged();
        dfuDeviceList.clear();
        dfuDeviceAdapter.notifyDataSetChanged();
    }

    private void populateList() {
		/* Initialize device list container */
        Log.d(TAG, "populateList");
        srcDeviceList = new ArrayList<BluetoothDevice>();
        dfuDeviceList = new ArrayList<BluetoothDevice>();
        srcDeviceAdapter = new DeviceAdapter(this, srcDeviceList);
        dfuDeviceAdapter = new DeviceAdapter(this, dfuDeviceList);
        devRssiValues = new HashMap<String, Integer>();
        srcListView = (ListView) findViewById(R.id.src_devices);
        dfuListView = (ListView) findViewById(R.id.dfu_devices);

        srcListView.setAdapter(srcDeviceAdapter);
        srcListView.setOnItemClickListener(mSrcDeviceClickListener);
        dfuListView.setAdapter(dfuDeviceAdapter);
        dfuListView.setOnItemClickListener(mDfuDeviceClickListener);
    }

    private void service_init() {
        // SmartNaviService 서비스 시작
        Log.d(TAG, "service_init() mService= " + mService);
        Intent bindIntent = new Intent(mContext, SmartNaviService.class);
        startService(bindIntent);
        bindService(bindIntent, mServiceConnection, Context.BIND_AUTO_CREATE);

        this.registerReceiver(BLEStatusChangeReceiver, makeGattUpdateIntentFilter());
    }

    private AdapterView.OnItemClickListener mSrcDeviceClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            tv_progress.setText("1. 시작");
            mDevice = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(srcDeviceList.get(position).getAddress());
            Log.d(TAG, "... onActivityResultdevice.address==" + mDevice + "mserviceValue" + mService);

            if(mDevice != null)
                mService.connect(mDevice.getAddress());

            address.setText(mDevice.getAddress());

            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(ADDRESS, mDevice.getAddress());
            editor.commit();
        }
    };

    private AdapterView.OnItemClickListener mDfuDeviceClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final Intent service = new Intent(mContext, DfuService.class);
            service.putExtra(DfuService.EXTRA_DEVICE_ADDRESS, dfuDeviceList.get(position).getAddress());
            service.putExtra(DfuService.EXTRA_DEVICE_NAME, dfuDeviceList.get(position).getName());
            service.putExtra(DfuService.EXTRA_FILE_MIME_TYPE,mFileType == DfuService.TYPE_AUTO ? DfuService.MIME_TYPE_ZIP : DfuService.MIME_TYPE_OCTET_STREAM);
            service.putExtra(DfuService.EXTRA_FILE_TYPE,DfuService.TYPE_APPLICATION);
            service.putExtra(DfuService.EXTRA_FILE_PATH, mFilePath);
            service.putExtra(DfuService.EXTRA_FILE_URI, mFileStreamUri);
            service.putExtra(DfuService.EXTRA_INIT_FILE_PATH, mInitFilePath);
            service.putExtra(DfuService.EXTRA_INIT_FILE_URI, mInitFileStreamUri);
            mContext.startService(service);
            mService.disconnect();
        }
    };

    private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {

        @Override
        public void onLeScan(final BluetoothDevice device, final int rssi, byte[] scanRecord) {
            if (device.getName() != null && device.getName().length() > 3) {
                // 이름을 add하기 전에 이름이 "SRC"로 시작하지않으면 넣지 않는다.\
                if(device.getName().substring(0,3).equals("FIT") && rssi > -65){
                    //if (isSlowAdvertising(scanRecord) == true)
                    {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    //if(!preferences.getString(ADDRESS, "연결된 디바이스 없음").equals(device.getAddress()))
                                    {
//            								if(rssi > -70){
                                        addDevice(device, rssi);
//            								}
                                    }
                                } catch (Exception e) {
                                    Log.e(TAG, "onLeScan Exception : " + e);
                                    // LogData.FileWrite(String.valueOf(NbeeconReceiver.STATUS_BT_ERROR), "DeviceListActivity.onLeScan, exception, " + e);
                                }
                            }
                        });
                    }
                }else if(device.getName().substring(0,3).equals("Dfu")){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {

//    							if(address.getText().toString().equals(device.getAddress()) && mIsUploading == false){
//    						        tv_progress.setText("3. DFU 시작");
//
//    								//addDevice(device, rssi);
//    							    mIsUploading = true;
//    							    mIsBootMode = false;
//    				                final Intent service = new Intent(mContext, DfuService.class);
//    				                service.putExtra(DfuService.EXTRA_DEVICE_ADDRESS, device.getAddress());
//    				                service.putExtra(DfuService.EXTRA_DEVICE_NAME, device.getName());
//    				                service.putExtra(DfuService.EXTRA_FILE_MIME_TYPE,mFileType == DfuService.TYPE_AUTO ? DfuService.MIME_TYPE_ZIP : DfuService.MIME_TYPE_OCTET_STREAM);
//    				                service.putExtra(DfuService.EXTRA_FILE_TYPE,DfuService.TYPE_APPLICATION);
//    				                service.putExtra(DfuService.EXTRA_FILE_PATH, mFilePath);
//    				                service.putExtra(DfuService.EXTRA_FILE_URI, mFileStreamUri);
//    				                service.putExtra(DfuService.EXTRA_INIT_FILE_PATH, mInitFilePath);
//    				                service.putExtra(DfuService.EXTRA_INIT_FILE_URI, mInitFileStreamUri);
//    				                mContext.startService(service);
//    				                mService.disconnect();
//    							} else if(address.getText().toString().equals("연결된 디바이스 없음")){
                                addDevice(device, rssi);
//    							} else {
//    								addDevice(device, rssi);
//    							}

                            } catch (Exception e) {
                                Log.e(TAG, "onLeScan Exception : " + e);
                                // LogData.FileWrite(String.valueOf(NbeeconReceiver.STATUS_BT_ERROR), "DeviceListActivity.onLeScan, exception, " + e);
                            }
                        }
                    });
                }
            }
        }
    };

    private ServiceConnection mServiceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder rawBinder) {
            mService = ((SmartNaviService.LocalBinder) rawBinder).getService();
            Log.d(TAG, "onServiceConnected mService= " + mService);
            //mService.setActivityHandler(mHandler);
            mService.initialize();
        }

        public void onServiceDisconnected(ComponentName classname) {
            mService = null;
        }
    };

    /**
     * Parses the advertising package obtained by BLE device
     *
     * @param data
     *            the data obtained (EIR data). Read Bluetooth Core
     *            Specification v4.0 (Core_V4.0.pdf -&gt; Vol.3 -&gt; Part C
     *            -&gt; Section 8) for details
     * @return the parsed temperature data
     * @throws ParseException
     *             thrown when the given data does not fit the expected format
     */
    public static boolean isSlowAdvertising(final byte[] data) {

        // direct adv 인경우 data가 모두 0으로 들어옴.
        if (data[0] == 0)
            return false;

        final byte FLAGS = 0x01; // "Flags" Data Type (See section 18.1 of
        // Core_V4.0.pdf)
		/*
		 * First byte of each EIR Data Structure has it's length (1 octet).
		 * There comes the EIR Data Type (n bytes) and (length - n bytes) of
		 * data. See Core_V4.0.pdf -> Vol.3 -> Part C -> Section 8 for details
		 */
        for (int i = 0; i < data.length;) {
            final int eirLength = data[i];

            // check whether there is no more to read
            if (eirLength == 0)
                break;

            final byte eirDataType = data[++i];
            switch ((byte) eirDataType) {
                case FLAGS:
                    // do nothing
                    final byte flags = data[++i];

                    // FLAGS 비트 정보
                    // ------------------------------------------------------------------------------
                    // Bit
                    // ------------------------------------------------------------------------------
                    // 0 | LE Limited Discoverable Mode
                    // 1 | LE General Discoverable Mode
                    // 2 | BR/EDR Not Supported (i.e bit 37 of LMP Extended Feature
                    // bits Page 0)
                    // 3 | Simultaneous LE and BR/EDR to Same Device Capable
                    // (Controller) (i.e. bit 49 of LMP Extended Feature bits Page
                    // 0)
                    // 4 | Simultaneous LE and BR/EDR to Same Device Capable (Host)
                    // (i.e. bit 66 of LMP Extended Feature bits Page 1
                    // 5..7 | Reserved
                    // ------------------------------------------------------------------------------
                    // slow advertising : 0x05
                    // fast advertising : 0x04
                    // ------------------------------------------------------------------------------

                    // Log.i(TAG, "FLAGS  =          " +
                    // NbeeconReceiver.byteToHex(flags));
                    if ((byte) flags == 0x05) // Slow Device 이면 true를 리턴한다.
                        return true;
                    else
                        return false;
                    // break;

                default:
                    break;
            }
            i += eirLength;
        }

        // Log.w(TAG, "not slow advertising!!!");
        return false;
    }

    private boolean m_IsRunningNotify = false;

    private void addDevice(BluetoothDevice device, int rssi) {
        boolean deviceFound = false;

        if(device.getName().substring(0, 3).equals("FIT")){
            for (BluetoothDevice listDev : srcDeviceList) {
                if (listDev.getAddress().equals(device.getAddress())) {
                    deviceFound = true;
                    break;
                }
            }
            if (!deviceFound) {
                srcDeviceList.add(device);
                srcDeviceAdapter.notifyDataSetChanged();
            }
            devRssiValues.put(device.getAddress(), rssi);
        }else{
            for (BluetoothDevice listDev : dfuDeviceList) {
                if (listDev.getAddress().equals(device.getAddress())) {
                    deviceFound = true;
                    break;
                }
            }
            if (!deviceFound) {
                dfuDeviceList.add(device);
                dfuDeviceAdapter.notifyDataSetChanged();
            }
            devRssiValues.put(device.getAddress(), rssi);
        }
        if(deviceFound) {
            if (!m_IsRunningNotify) {
                m_IsRunningNotify = true;
                mHandler.sendEmptyMessage(NOTIFY_DATA_SET_CHANED_MSG);
            }
        }
    }

    // 기본 블루투스에 디바이스 등록 해제
    private void unpairDevice(BluetoothDevice device) {
        try {
            Method m = device.getClass()
                    .getMethod("removeBond", (Class[]) null);
            m.invoke(device, (Object[]) null);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }

    public void BluetoothOnOff() throws InterruptedException{
        mBluetoothAdapter.stopLeScan(mLeScanCallback);
        BluetoothAdapter.getDefaultAdapter().disable();

        while(BluetoothAdapter.getDefaultAdapter().isEnabled() == true) {
            Thread.sleep(500);
            Log.i("BluetoothOnOff", "bt enabled !!!!!!!!!!!");
        }

        Thread.sleep(500);

        BluetoothAdapter.getDefaultAdapter().enable();

        while(BluetoothAdapter.getDefaultAdapter().isEnabled() == false) {
            Thread.sleep(500);
            Log.i("BluetoothOnOff", "bt disable !!!!!!!!!!!");
        }


        Thread.sleep(500);
        mBluetoothAdapter.startLeScan(mLeScanCallback);
    }

    private static final int NOTIFY_DATA_SET_CHANED_MSG = 100;

    private Handler mHandler = new Handler() {

        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case NOTIFY_DATA_SET_CHANED_MSG:
                    if (m_IsRunningNotify) {
                        srcDeviceAdapter.notifyDataSetChanged();
                        dfuDeviceAdapter.notifyDataSetChanged();
                        mHandler.sendEmptyMessageDelayed(NOTIFY_DATA_SET_CHANED_MSG, 500);
                    }
                    break;
            }
        };
    };


    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(SmartNaviService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(SmartNaviService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(SmartNaviService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(SmartNaviService.ACTION_DATA_AVAILABLE);
        intentFilter.addAction(SmartNaviService.DEVICE_DOES_NOT_SUPPORT_SRC);
        return intentFilter;
    }

    /**
     * 블루투스 결합 상태 변경시 : BluetoothDevice.ACTION_BOND_STATE_CHANGED
     * 블루투스 상태 변경시      : BluetoothAdapter.ACTION_STATE_CHANGED
     *
     * Nordic 키보드에서는 발생하지 않음
     */
    private BroadcastReceiver BLEStatusChangeReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            final Intent mIntent = intent;

            //*********************//
            if (action.equals(SmartNaviService.ACTION_GATT_CONNECTED)) {
                Log.d(TAG, "mHandler.ACTION_GATT_CONNECTED");
                runOnUiThread(new Runnable() {
                    public void run() {
                    }
                });
                srcListView.setEnabled(false);
            }

            //*********************//
            if (action.equals(SmartNaviService.ACTION_GATT_DISCONNECTED)) {
                Log.d(TAG, "mHandler.SMARTNAVI_DISCONNECT_MSG");
                runOnUiThread(new Runnable() {
                    public void run() {
                    }
                });
                conn.setText("DISCONNECT");
                conn.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
                ListRefresh();
            }


            //*********************//
            if (action.equals(SmartNaviService.ACTION_GATT_SERVICES_DISCOVERED)) {
                Log.d(TAG, "mHandler.SMARTNAVI_READY_MSG");
                runOnUiThread(new Runnable() {
                    public void run() {
                    }
                });
                conn.setText("CONNECTED");
                conn.setTextColor(getResources().getColor(android.R.color.holo_blue_dark));
//                btn_bootmode.performClick();
                setBootMode();
//                srcListView.setEnabled(false);
            }
            //*********************//
            if (action.equals(SmartNaviService.DEVICE_DOES_NOT_SUPPORT_SRC)){
//                showMessage("Device doesn't support SRC-100. Disconnecting");
                mService.disconnect();
            }
        }
    };

    private void updateProgressBar(final int progress, final boolean error) {
        switch (progress) {
            case DfuService.PROGRESS_CONNECTING:
                mProgressBar.setIndeterminate(true);
                mProgressBar.setVisibility(View.VISIBLE);
                //dfuListView.setEnabled(false);
//			mUploadPercentageView.setText(R.string.dfu_status_connecting);
                break;
            case DfuService.PROGRESS_STARTING:
                mProgressBar.setIndeterminate(true);
//			mUploadPercentageView.setText(R.string.dfu_status_starting);
                break;
            case DfuService.PROGRESS_ENABLING_DFU_MODE:
                mProgressBar.setIndeterminate(true);
//			mUploadPercentageView.setText(R.string.dfu_status_switching_to_dfu);
                break;
            case DfuService.PROGRESS_VALIDATING:
                mProgressBar.setIndeterminate(true);
//			mUploadPercentageView.setText(R.string.dfu_status_validating);
                break;
            case DfuService.PROGRESS_DISCONNECTING:
                mProgressBar.setIndeterminate(true);
//			mUploadPercentageView.setText(R.string.dfu_status_disconnecting);
                break;
            case DfuService.PROGRESS_COMPLETED:
                mProgressBar.setVisibility(View.INVISIBLE);
                notiSound(true);
                srcListView.setEnabled(true);
//			mUploadPercentageView.setText(R.string.dfu_status_completed);
                // let's wait a bit until we cancel the notification. When canceled immediately it will be recreated by service again.
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
//					showFileTransferSuccessMessage();

                        // if this activity is still open and upload process was completed, cancel the notification
                        final NotificationManager manager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
                        manager.cancel(DfuService.NOTIFICATION_ID);

//					try {
                        ListRefresh();
                        //BluetoothOnOff();
                        dfuListView.setEnabled(true);
//					} catch (InterruptedException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
                    }
                }, 200);
                break;
            case DfuService.PROGRESS_ABORTED:
                notiSound(false);
                srcListView.setEnabled(true);
//			mUploadPercentageView.setText(R.string.dfu_status_aborted);
                // let's wait a bit until we cancel the notification. When canceled immediately it will be recreated by service again.
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
//					onUploadCanceled();

                        // if this activity is still open and upload process was completed, cancel the notification
                        final NotificationManager manager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
                        manager.cancel(DfuService.NOTIFICATION_ID);
                        dfuListView.setEnabled(true);
                    }
                }, 200);
                break;
            default:
                mProgressBar.setIndeterminate(false);
                if (error) {
//				showErrorMessage(progress);

                    mProgressBar.setProgress(progress);
                } else {
                    mProgressBar.setProgress(progress);
//				mUploadPercentageView.setText(getString(R.string.progress, progress));
                }
                break;
        }
    }

    class DeviceAdapter extends BaseAdapter {
        Context context;
        List<BluetoothDevice> devices;
        LayoutInflater inflater;

        public DeviceAdapter(Context context, List<BluetoothDevice> devices) {
            this.context = context;
            inflater = LayoutInflater.from(context);
            this.devices = devices;
        }

        @Override
        public int getCount() {
            return devices.size();
        }

        @Override
        public Object getItem(int position) {
            return devices.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewGroup vg;

            if (convertView != null) {
                vg = (ViewGroup) convertView;
            } else {
                vg = (ViewGroup) inflater.inflate(R.layout.list_name, null);
            }

            try {
                BluetoothDevice device = devices.get(position);
                final TextView tv_address = ((TextView) vg.findViewById(R.id.tv_address));
                final TextView tv_rssi = (TextView) vg.findViewById(R.id.tv_rssi);

                byte rssival = (byte) devRssiValues.get(device.getAddress()).intValue();
                if (rssival != 0) {
                    tv_rssi.setText("Rssi = " + String.valueOf(rssival));
                }
                tv_address.setText(device.getAddress());

            } catch (Exception e) {
                Log.e(TAG, "DeviceAdapter.getView, Exception, " + e);
            }
            return vg;
        }
    }

    public void notiSound(boolean soundflag){
        NotificationManager noti = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        Notification notification = new Notification();

        //연결시 폴더에 연결음이 있으면 변경하여 출력한다.
        if(soundflag){
            //성공
            //notification.sound = Uri.parse("android.resource://"+ getPackageName() + "/" + R.raw.dingdong);
            tv_progress.setText("성공");
        }else{
            //실패list_name.xml
            //notification.sound = Uri.parse("android.resource://"+ getPackageName() + "/" + R.raw.dingdong2);
            tv_progress.setText("실패");
        }

        noti.notify(8000, notification);
    }

}
