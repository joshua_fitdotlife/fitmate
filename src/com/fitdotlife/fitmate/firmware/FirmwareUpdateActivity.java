package com.fitdotlife.fitmate.firmware;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.fitdotlife.fitmate.R;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.http.VersionCheckService;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.util.Utils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringRes;
import org.androidannotations.rest.spring.annotations.RestService;
import org.apache.log4j.Log;
import org.springframework.web.client.RestClientException;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLConnection;
import java.util.Set;

@EActivity(R.layout.activity_firmware_update)
public class FirmwareUpdateActivity extends Activity {

    private enum FirmwareUpdateState
    {
        INIT_RECOVERY , BATTERY_LOW , NETWORK_NOT_AVAILABLE , LATEST_FIRMWARE ,
        SERVER_DOWNLOAD_READY , SERVER_DOWNLOAD_ERROR ,
        DEVICE_DOWNLOAD_READY , DEVICE_DOWNLOAD_COMPLETE , DEVICE_DOWNLOAD_ERROR
    }

    private static final String TAG = "fitmate";

    public static final String EXTRA_OPEN_DFU = "no.nordicsemi.android.nrtbeacon.extra.open_dfu";
    private static final String NBEECON_UPDATE_SHAREDPREFERENCES = "nbeecon_update_sharedpreferences";
    private static final String ADDRESS = "preferences_address";

    private static final byte SEND_KEYINPUT_NOTI    = (byte)0x01;
    private static final byte SEND_BATTERY_NOTI     = (byte)0x02;
    private static final byte SEND_LINK_LOSS_WRITE  = (byte)0x04;
    private static final byte SEND_REMOCON_WRITE    = (byte)0x08;

    private boolean mIsUploading = false;
    private boolean mIsBootMode = false;

    private Context mContext = null;

    private ProgressDialog mProgressDialog = null;

    private FitmateDBManager mDBManager = null;
    private UserInfo mUserInfo = null;
    private FirmwareUpdateState mFirmwareUpdateState;

    private BluetoothAdapter mBluetoothAdapter = null;
    private BluetoothDevice mDevice = null;
    private SmartNaviService mService = null;

    @ViewById(R.id.ic_activity_firmware_update_actionbar)
    View actionBarView;

    @StringRes(R.string.firmware_update_init_recovery)
    String strInitRecovery;

    @StringRes(R.string.firmware_update_battery_low)
    String strBatteryLow;

    @StringRes(R.string.firmware_update_network_not_available)
    String strNetworkNotAvailable;

    @StringRes(R.string.firmware_update_lastest_update)
    String strLastestUpdate;

    @StringRes(R.string.firmware_update_server_download_ready)
    String strServerDownloadReady;

    @StringRes(R.string.firmware_update_server_download_error)
    String strServerDownloadError;

    @StringRes(R.string.firmware_update_device_download_ready)
    String strDeviceDownloadReady;

    @StringRes(R.string.firmware_update_device_download_error)
    String strDeviceDownloadError;

    @StringRes(R.string.firmware_update_device_download_complete)
    String strDeviceDownloadComplete;

    @StringRes(R.string.firmware_update_next)
    String strUpdateNext;

    @StringRes(R.string.firmware_update_start_update)
    String strStartUpdate;

    @StringRes(R.string.firmware_update_start_download)
    String strStartDownload;

    @StringRes(R.string.firmware_update_exit)
    String strUpdateExit;

    @StringRes(R.string.firmware_update_cancel)
    String strUpdateCancel;

    @StringRes(R.string.firmware_update_retry)
    String strUpdateRetry;

    @RestService
    VersionCheckService versionCheckServiceClient;

    private final BroadcastReceiver mDfuUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            final String action = intent.getAction();

            if(DfuService.BROADCAST_PROGRESS.equals(action) ){

                final int progress = intent.getIntExtra(DfuService.EXTRA_DATA , 0);
                android.util.Log.i("mDfuUpdateReceiver", "Progress : " + progress);
                updateProgressBar(progress, false);

            }else if( DfuService.BROADCAST_ERROR.equals(action) ){

            }
        }
    };

    private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {

            if(mDevice.getName() != null && mDevice.getName().length() > 3 ){

            }
        }
    };

    private ServiceConnection mServiceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder rawBinder) {
            mService = ((SmartNaviService.LocalBinder) rawBinder).getService();
            mService.initialize();
        }

        public void onServiceDisconnected(ComponentName classname)
        {
            mService = null;
        }
    };

    private void updateProgressBar( final int progress , final boolean error )
    {
        switch( progress )
        {
            case DfuService.PROGRESS_CONNECTING:
                mProgressDialog.setIndeterminate(true);
                break;
            case DfuService.PROGRESS_STARTING:
                mProgressDialog.setIndeterminate(true);
                break;
            case DfuService.PROGRESS_ENABLING_DFU_MODE:
                mProgressDialog.setIndeterminate(true);
                break;
            case DfuService.PROGRESS_VALIDATING:
                mProgressDialog.setIndeterminate(true);
                break;
            case DfuService.PROGRESS_DISCONNECTING:
                mProgressDialog.setIndeterminate(true);
                break;
            case DfuService.PROGRESS_COMPLETED:

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        final NotificationManager manager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
                        manager.cancel(DfuService.NOTIFICATION_ID);
                    }
                }, 200);
                break;
            case DfuService.PROGRESS_ABORTED:
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        final NotificationManager manager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
                        manager.cancel(DfuService.NOTIFICATION_ID);
                    }
                }, 200);
                break;
            default:
                mProgressDialog.setIndeterminate(false);
                if (error) {
                    mProgressDialog.setProgress(progress);
                } else {
                    mProgressDialog.setProgress(progress);
                }
                break;
        }
    }

    @AfterViews
    void Init()
    {
        this.mContext = this.getApplicationContext();
        this.mDBManager = new FitmateDBManager(this);
        this.mUserInfo = this.mDBManager.getUserInfo();

        final BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        this.displayActionBar();
        //보유 최신 펌웨어 표시
        //복구모드인지 확인
    }

    @Override
    protected void onDestroy() {
        // SmartNaviService 서비스 bind 해제
        unbindService(mServiceConnection);
        // BLEStatusChangeReceiver 등록 해제
        unregisterReceiver(BLEStatusChangeReceiver);
        // SmartNaviService 서비스 stop
        mBluetoothAdapter.stopLeScan(mLeScanCallback);
        stopService(new Intent(this, SmartNaviService.class));
        super.onDestroy();
    }

    private void displayActionBar(){
        actionBarView.setBackgroundColor(this.getResources().getColor(R.color.fitmate_blue));
        TextView tvBarTitle = (TextView) actionBarView.findViewById(R.id.tv_custom_action_bar_title);
        tvBarTitle.setText(this.getString(R.string.firmware_update_title));
        ImageView imgLeft = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_left);
        imgLeft.setVisibility(View.GONE);
        ImageView imgFriendSet = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_right_more);
        imgFriendSet.setVisibility(View.GONE);
    }

    private void service_init()
    {
        Intent bindIntent = new Intent(mContext, SmartNaviService.class);
        startService(bindIntent);
        bindService(bindIntent, mServiceConnection, Context.BIND_AUTO_CREATE);

        this.registerReceiver(BLEStatusChangeReceiver, makeGattUpdateIntentFilter());
    }

    private BroadcastReceiver BLEStatusChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            String action = intent.getAction();
            final Intent mIntent = intent;

            //*********************//
            if (action.equals(SmartNaviService.ACTION_GATT_CONNECTED)) {
                runOnUiThread(new Runnable() {
                    public void run() {
                    }
                });
            }

            //*********************//
            if (action.equals(SmartNaviService.ACTION_GATT_DISCONNECTED)) {
                runOnUiThread(new Runnable() {
                    public void run() {
                    }
                });
            }


            //*********************//
            if (action.equals(SmartNaviService.ACTION_GATT_SERVICES_DISCOVERED)) {
                runOnUiThread(new Runnable() {
                    public void run() {
                    }
                });
                setBootMode();
            }
            //*********************//
            if (action.equals(SmartNaviService.DEVICE_DOES_NOT_SUPPORT_SRC)){
                mService.disconnect();
            }

        }
    };

    private void setBootMode()
    {
        if(mIsBootMode == true)
            return;

        mIsBootMode = true;

        try {
            Thread.sleep(200);
            byte[] value = new byte[4];
            value[0] = (byte)0xf5;
            value[1] = (byte)0xb2;
            value[2] = (byte)0xb2;
            value[3] = (byte)0xfa;

            mService.writeRemoconCharacteristic(value);
            mIsUploading = false;

            Thread.sleep(2000);
            mService.disconnect();

            Set<BluetoothDevice> pairedDevices = BluetoothAdapter.getDefaultAdapter().getBondedDevices();
            if (pairedDevices.size() > 0) {
                for (BluetoothDevice device : pairedDevices) {
                    try {
                        if(mDevice != null && device.getAddress() != null && device.getAddress().equals(mDevice.getAddress())) {
                            unpairDevice(BluetoothAdapter.getDefaultAdapter().getRemoteDevice(device.getAddress()));
                            break;
                        }
                    } catch (NullPointerException e) {
                        android.util.Log.e(TAG, "device.getName() == null");
                    }
                }
            }

            BluetoothOnOff();

        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private static IntentFilter makeGattUpdateIntentFilter()
    {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(SmartNaviService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(SmartNaviService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(SmartNaviService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(SmartNaviService.ACTION_DATA_AVAILABLE);
        intentFilter.addAction(SmartNaviService.DEVICE_DOES_NOT_SUPPORT_SRC);
        return intentFilter;
    }

    // 기본 블루투스에 디바이스 등록 해제
    private void unpairDevice(BluetoothDevice device) {
        try {
            Method m = device.getClass()
                    .getMethod("removeBond", (Class[]) null);
            m.invoke(device, (Object[]) null);
        } catch (Exception e) {
            android.util.Log.e(TAG, e.getMessage());
        }
    }

    public void BluetoothOnOff() throws InterruptedException{
        mBluetoothAdapter.stopLeScan(mLeScanCallback);
        BluetoothAdapter.getDefaultAdapter().disable();

        while(BluetoothAdapter.getDefaultAdapter().isEnabled() == true) {
            Thread.sleep(500);
            android.util.Log.i("BluetoothOnOff", "bt enabled !!!!!!!!!!!");
        }

        Thread.sleep(500);

        BluetoothAdapter.getDefaultAdapter().enable();

        while(BluetoothAdapter.getDefaultAdapter().isEnabled() == false) {
            Thread.sleep(500);
            android.util.Log.i("BluetoothOnOff", "bt disable !!!!!!!!!!!");
        }


        Thread.sleep(500);
        mBluetoothAdapter.startLeScan(mLeScanCallback);
    }

    @Click(R.id.btn_firmware_update_checkversion)
    void checkVersionClick(){

        if( !Utils.isOnline(this) ){
            this.showConfirmDialog(strNetworkNotAvailable, strUpdateExit, strUpdateRetry);
        }
    }

    @Background
    void checkFirmwareUpdate( String firmwareVersion )
    {
        boolean needUpdate = false;
        try {
            needUpdate = versionCheckServiceClient.checkSoftwareUpdate(mUserInfo.getEmail(), firmwareVersion);
        }catch(RestClientException e){ }

        if( needUpdate ){

            mFirmwareUpdateState = FirmwareUpdateState.SERVER_DOWNLOAD_READY;
            this.showConfirmDialog( strDeviceDownloadReady , strUpdateCancel , strStartDownload );

        }else{

            mFirmwareUpdateState = FirmwareUpdateState.LATEST_FIRMWARE;
            this.showInfoDialog(strLastestUpdate);

        }
    }

    @Click(R.id.btn_firmware_update)
    void firmwaredUpdateClick()
    {

        //배터리를 확인하다.
        int batteryRatio =mDBManager.getLastBatteryRatio( mUserInfo.getEmail() );

        if( batteryRatio < 50 )
        {
           this.showInfoDialog( strBatteryLow );
            return;
        }

        //기기의 펌웨어 버전 확인
        String strDeviceFirmwareVersion = this.getFirmwareVersionSharedPreference(this);
        //서버에서 다운로드한 파일의 버전
        String strDownloadFirmwareVersion = "0.0";

        int deviceFirmwareVersion = Integer.parseInt( strDeviceFirmwareVersion.replace("." , ""));
        //서버에서 다운로드한 파일의 버전
        int downloadFirmwareVersion = Integer.parseInt( strDownloadFirmwareVersion.replace("." , ""));


        if( deviceFirmwareVersion < downloadFirmwareVersion )
        {

            this.mFirmwareUpdateState = FirmwareUpdateState.DEVICE_DOWNLOAD_READY;
            showConfirmDialog(strDeviceDownloadReady, strUpdateNext, strStartDownload);
        }
        else
        {
            this.mFirmwareUpdateState = FirmwareUpdateState.LATEST_FIRMWARE;
            showInfoDialog(strLastestUpdate);
        }
    }

    @UiThread
    void showInfoDialog( String message )
    {
        AlertDialog.Builder alert = new AlertDialog.Builder( this );
        alert.setMessage(message);
        alert.setTitle(R.string.firmware_update_title);
        alert.setPositiveButton(this.getString(R.string.common_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();     //닫기
                finish();
            }
        });

        alert.show();
    }

    @UiThread
    void showConfirmDialog( String message , String okText , String cancelText  )
    {
        final AlertDialog.Builder alert_confirm = new AlertDialog.Builder( this );
        alert_confirm.setTitle( this.getString(R.string.firmware_update_title) );
        alert_confirm.setMessage( message ).setCancelable(false).setPositiveButton(  okText  ,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        confirmOk();
                        dialog.dismiss();
                    }
                }).setNegativeButton(cancelText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        confirmCancel();
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = alert_confirm.create();
        alert.show();
    }

    void showProgressDialog( String message )
    {
        mProgressDialog = new ProgressDialog(mContext);
        mProgressDialog.setProgressStyle( ProgressDialog.STYLE_HORIZONTAL );
        mProgressDialog.setMessage(message);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    void closeProgressDialog()
    {
        if ( mProgressDialog != null ) {
            if( mProgressDialog.isShowing() ) {
                mProgressDialog.dismiss();
            }
        }
    }


    private void confirmOk( ){

        switch( mFirmwareUpdateState )
        {
            case INIT_RECOVERY:
                break;
            case BATTERY_LOW:
                break;
            case NETWORK_NOT_AVAILABLE:
                break;
            case LATEST_FIRMWARE:
                break;
            case SERVER_DOWNLOAD_READY:
                break;
            case SERVER_DOWNLOAD_ERROR:
                break;
            case DEVICE_DOWNLOAD_READY:

                break;
            case DEVICE_DOWNLOAD_ERROR:
                break;
            case DEVICE_DOWNLOAD_COMPLETE:
                break;
        }
    }

    private void confirmCancel(){
        this.finish();
    }

    private String getFirmwareVersionSharedPreference( Context context )
    {
        SharedPreferences userInfo = context.getSharedPreferences( "fitmate" , Activity.MODE_PRIVATE );
        String firmwareVersion = userInfo.getString( "firmwareversion" , "" );
        return firmwareVersion;
    }

    class DownloadFileFromURL extends AsyncTask<String,String,String>
    {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //프로그레스 다이얼로그를 실행시킨다.
        }

        @Override
        protected String doInBackground(String... params)
        {

            int count;
            try {
                URL url = new URL(params[0]);
                URLConnection connection = url.openConnection();
                connection.connect();

                int lengthOfFile = connection.getContentLength();
                InputStream input = new BufferedInputStream(url.openStream());

                OutputStream output = new FileOutputStream(Environment.getExternalStorageDirectory().toString() + "/firmware");

                byte data[] = new byte[1024];

                long total =0;

                while( (count = input.read()) != -1 ){

                    total += count;
                    publishProgress("" + (int) ((total * 100) / lengthOfFile));
                    output.write(data,0,count);
                }

                output.flush();

                output.close();
                input.close();

            }catch(Exception e)
            {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            //프로그레스 다이얼로그에 퍼센트를 넘긴다.
        }

        @Override
        protected void onPostExecute(String s) {
            //프로그레스 다이얼로그를 없앤다.
        }
    }
}
