package com.fitdotlife.fitmate.firmware;


import android.app.Activity;

import no.nordicsemi.android.dfu.DfuBaseService;

/**
 * Created by Joshua on 2015-11-10.
 */

public class DfuService extends DfuBaseService{

    @Override
    protected Class<? extends Activity> getNotificationTarget() {
        return null;
    }
}
