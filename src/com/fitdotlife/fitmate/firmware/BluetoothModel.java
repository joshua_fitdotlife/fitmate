package com.fitdotlife.fitmate.firmware;

/**
 * Created by Joshua on 2015-11-10.
 */
public class BluetoothModel {

    private String address;
    private int button;
    private int battery;
    private String delay;
    private int delaycount;

    public BluetoothModel(String address, int button, int battery) {

        this.address = address;
        this.button = button;
        this.battery = battery;
    }

    public String getAddress() {
        return address;
    }

    public int getButton() {
        return button;
    }

    public int getBattery() {
        return battery;
    }

    public String getDelay() {
        return delay;
    }

    public int getDelayCount() {
        return delaycount;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setButton(int button) {
        this.button = button;
    }

    public void setBattery(int battery) {
        this.battery = battery;
    }

    public void setDelay(String delay) {
        this.delay = delay;
    }

    public void setDelayCount(int delaycount) {
        this.delaycount = delaycount;
    }

}
