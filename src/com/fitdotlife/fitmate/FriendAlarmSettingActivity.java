package com.fitdotlife.fitmate;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fitdotlife.fitmate_lib.customview.FriendAlarmSetListItem;
import com.fitdotlife.fitmate_lib.customview.FriendAlarmSetListItem_;
import com.fitdotlife.fitmate_lib.customview.FriendListTitleView;
import com.fitdotlife.fitmate_lib.customview.FriendListTitleView_;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.http.FriendService;
import com.fitdotlife.fitmate_lib.http.NetworkClass;
import com.fitdotlife.fitmate_lib.http.RestHttpErrorHandler;
import com.fitdotlife.fitmate_lib.object.FriendAlarmSetting;
import com.fitdotlife.fitmate_lib.object.UserFriendsNewsSetting;
import com.fitdotlife.fitmate_lib.util.Utils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringRes;
import org.androidannotations.rest.spring.annotations.RestService;
import org.springframework.web.client.RestClientException;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@EActivity(R.layout.activity_friend_alarm_setting)
public class FriendAlarmSettingActivity extends Activity implements NetWorkChangeListener{

    private boolean isAllSelected = false;
    private List<UserFriendsNewsSetting> mFriendNewsSettingList = null;
    private List<FriendAlarmSetListItem> mItemList = null;
    private String mUserEmail = null;
    private InputMethodManager imm;

    private MyApplication myApplication = null;

    @RestService
    FriendService friendServiceClient;

    @ViewById(R.id.ic_activity_friend_actionbar)
    View actionBarView;

    @ViewById(R.id.ll_activity_friend_alarm_setting_content)
    LinearLayout llContent;

    @ViewById(R.id.btn_activity_friend_alarm_setting_select_all)
    Button btnSelectAll;

    @ViewById(R.id.btn_activity_friend_alarm_setting_complete)
    Button btnComplate;

    @ViewById(R.id.etx_activity_friend_alarm_setting_search_word)
    EditText etxSearch;

    @StringRes(R.string.friend_receivealarm_select_all)
    String strReceiveAlarmSelect;

    @StringRes(R.string.friend_receivealarm_release_all)
    String strReceiveAlarmRelease;

    @StringRes(R.string.friend_receivealarm_title)
    String strReceiveAlarmTitle;

    @StringRes(R.string.friend_findfriend_write_friendname)
    String strFindFriendWriteFriendName;

    @StringRes(R.string.common_connect_network)
    String strConnectNetwork;

    @Bean
    RestHttpErrorHandler restErrorHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        myApplication = (MyApplication) this.getApplication();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        myApplication.deleteNetworkChangeListener(this);
    }

    @AfterViews
    void init() {

        FitmateDBManager dbManger = new FitmateDBManager(this);
        this.mUserEmail = dbManger.getUserInfo().getEmail();
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        friendServiceClient.setRootUrl(NetworkClass.baseURL + "/api");
        friendServiceClient.setRestErrorHandler(restErrorHandler);

        this.etxSearch.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                etxSearch.setFocusableInTouchMode(true);
                return false;
            }
        });

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        this.displayActionBar();
        //친구 목록을 가져온다.
        this.mItemList = new ArrayList<FriendAlarmSetListItem>();
        if (Utils.isOnline(this)){
            this.getFriendList();
        }else{
            Toast.makeText(this , strConnectNetwork , Toast.LENGTH_SHORT).show();
            myApplication.addNetworkChangeListener(this);
        }

    }

    @Background
    void getFriendList( ){
        try {

            this.mFriendNewsSettingList = friendServiceClient.getFriendNewsSetting(mUserEmail);
            if( mFriendNewsSettingList != null ) {
                displayFriendNewsSettingList(mFriendNewsSettingList);
            }

        }catch(RestClientException e){
            Log.e("fitmate", e.getMessage());
        }
    }

    private void displayActionBar(  ){
        actionBarView.setBackgroundColor(this.getResources().getColor(R.color.fitmate_blue));
        TextView tvBarTitle = (TextView) actionBarView.findViewById( R.id.tv_custom_action_bar_title );
        tvBarTitle.setText(strReceiveAlarmTitle);
        ImageView imgBack = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_left);
        imgBack.setVisibility(View.GONE);

        ImageView mImgRight = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_right);
        mImgRight.setVisibility(View.INVISIBLE);
    }

    @UiThread
    public void displayFriendNewsSettingList(List<UserFriendsNewsSetting> friendsNewsSettingsList){

        String groupTitle = "";

        for( UserFriendsNewsSetting userFriendsNewsSetting : friendsNewsSettingsList  )
        {

            if( !userFriendsNewsSetting.getFriendInfo().getNameSep().equals("") ) {
                String jaeum = userFriendsNewsSetting.getFriendInfo().getNameSep().substring(0, 1);

                if (!groupTitle.equals(jaeum)) {
                    FriendListTitleView titleView = FriendListTitleView_.build(this);
                    titleView.setTitle(jaeum);

                    llContent.addView(titleView);
                    groupTitle = jaeum;
                }
            }

            FriendAlarmSetListItem listItem = FriendAlarmSetListItem_.build(this);
            listItem.setFriendNewsSetting(userFriendsNewsSetting);
            llContent.addView(listItem);
            this.mItemList.add( listItem );
        }
    }

    @Click(R.id.btn_activity_friend_alarm_setting_select_all)
    void selectAllClick(){
        if( isAllSelected ){
            isAllSelected = false;
            btnSelectAll.setText( strReceiveAlarmSelect );
            for( FriendAlarmSetListItem listItem : this.mItemList ){
                listItem.setChecked(false);
            }

        }else{
            isAllSelected = true;
            btnSelectAll.setText(strReceiveAlarmRelease);
            for( FriendAlarmSetListItem listItem : this.mItemList ){
                listItem.setChecked(true);
            }
        }
    }

    @Click(R.id.btn_activity_friend_alarm_setting_complete)
    void completeClick(){

        if( this.mFriendNewsSettingList.size() > 0 ) {

            //JSONArray jsonArray = new JSONArray();
            List<FriendAlarmSetting> alarmSettingList = new ArrayList<FriendAlarmSetting>();
            for( UserFriendsNewsSetting userFriendsNewsSetting : mFriendNewsSettingList){

//                JSONObject obj = new JSONObject();
//                try{
//
//                    obj.put("friendEmail" , userFriendsNewsSetting.getFriendInfo().getEmail());
//                    obj.put("sendMyNews" , userFriendsNewsSetting.isSendMyNews());
//                    obj.put("receiveFriendsNews",userFriendsNewsSetting.isReceiveFriendsNews());
//
//                }catch( JSONException e){ }
//                jsonArray.put(obj);

                FriendAlarmSetting friendAlarmSetting = new FriendAlarmSetting();
                friendAlarmSetting.setFriendEmail( userFriendsNewsSetting.getFriendInfo().getEmail() );
                friendAlarmSetting.setSendMyNews(userFriendsNewsSetting.isSendMyNews());
                friendAlarmSetting.setReceiveFriendsNews(userFriendsNewsSetting.isReceiveFriendsNews());

                alarmSettingList.add( friendAlarmSetting );

            }

            this.setAlarm( alarmSettingList );
        }

    }

    @Click(R.id.img_activity_friend_alarm_setting_search)
    void searchClick(){

        if( this.etxSearch.getText().length() == 0 ){
            this.etxSearch.requestFocus();
            Toast.makeText(this, strFindFriendWriteFriendName , Toast.LENGTH_SHORT).show();
            return;
        }

        this.etxSearch.setFocusable(false);
        String searchWord = this.etxSearch.getText().toString();

        ArrayList<UserFriendsNewsSetting> friendList = new ArrayList<UserFriendsNewsSetting>();
        Iterator<UserFriendsNewsSetting> friendListIter = mFriendNewsSettingList.iterator();
        while( friendListIter.hasNext() ){
            UserFriendsNewsSetting userFriend = friendListIter.next();
            if( userFriend.getFriendInfo().getName() .contains(searchWord) ){
                friendList.add( userFriend );
            }
        }

        llContent.removeAllViews();

        if (mFriendNewsSettingList.size() > 0) {
            displayFriendNewsSettingList(friendList);
        }

        imm.hideSoftInputFromWindow(etxSearch.getWindowToken(), 0);
    }

    @Background
    void setAlarm( List<FriendAlarmSetting> userNewsSettingList ){
        try {
            String email = new FitmateDBManager(this).getUserInfo().getEmail();
            friendServiceClient.setFriendNewsSetting(email , userNewsSettingList );
            setResult(RESULT_OK);
            finish();
        }catch ( RestClientException e ){
            Log.e("fitmate", e.getMessage());
        }
    }

    @Override
    public void OnNetworkAvailable() {

        this.getFriendList();

    }
}
