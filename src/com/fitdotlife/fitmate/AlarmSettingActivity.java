package com.fitdotlife.fitmate;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.http.NetworkClass;
import com.fitdotlife.fitmate_lib.http.RestHttpErrorHandler;
import com.fitdotlife.fitmate_lib.http.UserInfoService;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.object.UserNotiSetting;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringRes;
import org.androidannotations.rest.spring.annotations.RestService;
import org.springframework.web.client.RestClientException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

@EActivity(R.layout.activity_alarm_setting)
public class AlarmSettingActivity extends Activity {

    private final int USERINFO_CHANGEALARM_CODE = 0;

    private UserInfo mUserInfo = null;
    private UserNotiSetting mUserNotiSetting = null;
    private FitmateDBManager mDBManager = null;

    @ViewById(R.id.ic_activity_setting_alarm_actionbar)
    View actionBarView;

    @ViewById(R.id.sw_activity_alarm_setting_mynews_today_activity)
    Switch swMyNewsTodayActivity;

    @ViewById(R.id.sw_activity_alarm_setting_mynews_week_activity)
    Switch swMyNewsWeekActivity;

    @ViewById(R.id.sw_activity_alarm_setting_mynews_battery)
    Switch swMyNewsBattery;

    @ViewById(R.id.sw_activity_alarm_setting_mynews_fitmeter_notuse)
    Switch swMyNewsFitmeterNotUse;

    @ViewById(R.id.sw_activity_alarm_mynews_setting_wear)
    Switch swMyNewsFitmeterWear;

    @ViewById(R.id.sw_activity_alarm_setting_friendnews_report)
    Switch swFriendNewsReport;

    @ViewById(R.id.sw_activity_alarm_setting_friendnews_today_activity)
    Switch swFriendTodayActivity;

    @ViewById(R.id.sw_activity_alarm_setting_friendnews_week_activity)
    Switch swFriendWeekActivity;

    @ViewById(R.id.tv_activity_alarm_setting_alarm_set_title)
    TextView tvAlarmTimeTitle;

    @ViewById(R.id.tv_activity_alarm_setting_alarm_set)
    TextView tvAlarmTime;


    @ViewById(R.id.rl_activity_alarm_setting_receive_alarm)
    RelativeLayout rlReceiveAlarm;

    @ViewById(R.id.rl_activity_alarm_setting_alarmtime)
    RelativeLayout rlAlarmTime;

    @ViewById(R.id.tv_activity_alarm_setting_receive_alarm)
    TextView tvReceiveAlarm;

    @StringRes(R.string.setting_alarm_bar_title)
    String strSettingAlarmBarTitle;

    @RestService
    UserInfoService userInfoServiceClient;

    @Bean
    RestHttpErrorHandler restErrorHandler;

    @AfterViews
    void onInit(){

        userInfoServiceClient.setRootUrl(NetworkClass.baseURL + "/api");

        this.mDBManager = new FitmateDBManager(this);
        this.mUserInfo = this.mDBManager.getUserInfo();

        displayActionBar();

        getUserNotiSetting();

        swFriendNewsReport.setOnCheckedChangeListener(new Switch.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                swFriendTodayActivity.setChecked(isChecked);
                swFriendWeekActivity.setChecked(isChecked);

                swFriendTodayActivity.setEnabled(isChecked);
                swFriendWeekActivity.setEnabled(isChecked);
                rlReceiveAlarm.setEnabled( isChecked );

                if ( isChecked ) {
                    tvReceiveAlarm.setTextColor(0xFF3C3C3C);
                } else {
                    tvReceiveAlarm.setTextColor(0x303C3C3C);
                }
            }
        });

        swMyNewsFitmeterWear.setOnCheckedChangeListener(new Switch.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                rlAlarmTime.setEnabled(isChecked);

                if(isChecked) {
                    tvAlarmTimeTitle.setTextColor( 0xFF3C3C3C );
                    tvAlarmTime.setTextColor(0xFF3C3C3C);
                }else{
                    tvAlarmTimeTitle.setTextColor( 0x303C3C3C );
                    tvAlarmTime.setTextColor(0x303C3C3C);
                }
            }
        });
    }

    @Background
    void getUserNotiSetting(){

        try {
            UserNotiSetting userNotiSetting = userInfoServiceClient.getUserNotiSetting(mUserInfo.getEmail());

            if( userNotiSetting != null ){
                mUserNotiSetting = userNotiSetting;
                this.mDBManager.setUserNotiSetting( userNotiSetting );

            }else{
                mUserNotiSetting = new UserNotiSetting();
            }

            displayUserNotiSetting();

        }catch ( RestClientException e ){
            Log.e("fitmate", e.getMessage());
            displayMessage( "설정을 가져오는 중에 오류가 발생하였습니다." );
            finish();
        }
    }

    @UiThread
    void displayMessage( String message ){
        Toast.makeText(this , message , Toast.LENGTH_SHORT).show();
    }

    @UiThread
    void displayUserNotiSetting(  ){

        swMyNewsTodayActivity.setChecked( mUserNotiSetting.isMytodayactivitynews() );
        swMyNewsWeekActivity.setChecked(mUserNotiSetting.isMyweekactivitynews() );
        swMyNewsBattery.setChecked(mUserNotiSetting.isBatteryusagenoti());
        swMyNewsFitmeterNotUse.setChecked(mUserNotiSetting.isFitmeternotusednoti());
        swMyNewsFitmeterWear.setChecked(mUserNotiSetting.isFitmeterwearnoti());
        tvAlarmTime.setText(mUserNotiSetting.getAlarmtime());
        rlAlarmTime.setEnabled(mUserNotiSetting.isFitmeterwearnoti());

        swFriendNewsReport.setChecked(mUserNotiSetting.isFriendnews());
        swFriendTodayActivity.setChecked(mUserNotiSetting.isFriendtodayactivitynews());
        swFriendWeekActivity.setChecked(mUserNotiSetting.isFriendweekactivitynews());

        if( mUserNotiSetting.isFriendnews() == false )
        {
            swFriendTodayActivity.setEnabled(false);
            swFriendWeekActivity.setEnabled(false);
            rlReceiveAlarm.setEnabled(false);
            tvReceiveAlarm.setTextColor( 0x303C3C3C );
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        updateUserNotiSetting();
    }

    @Background
    void updateUserNotiSetting(){

        boolean result = false;
        try {

            AlarmSettingActivity.setChangeNewsSettingPreference(this, mUserNotiSetting.isMytodayactivitynews() != swMyNewsTodayActivity.isChecked());
            mUserNotiSetting.setMytodayactivitynews( swMyNewsTodayActivity.isChecked() );
            AlarmSettingActivity.setChangeNewsSettingPreference( this , mUserNotiSetting.isMyweekactivitynews() != swMyNewsWeekActivity.isChecked()  );
            mUserNotiSetting.setMyweekactivitynews(swMyNewsWeekActivity.isChecked());

            mUserNotiSetting.setBatteryusagenoti(swMyNewsBattery.isChecked());
            mUserNotiSetting.setFitmeternotusednoti(swMyNewsFitmeterNotUse.isChecked());
            mUserNotiSetting.setFitmeterwearnoti(swMyNewsFitmeterWear.isChecked());
            mUserNotiSetting.setAlarmtime( tvAlarmTime.getText().toString() );

            if(mUserNotiSetting.isFriendnews() == swFriendNewsReport.isChecked() ){ //친구 뉴스가 같다면

                if( swFriendNewsReport.isChecked() ){
                    AlarmSettingActivity.setChangeNewsSettingPreference(this, mUserNotiSetting.isFriendtodayactivitynews() != swFriendTodayActivity.isChecked());
                    AlarmSettingActivity.setChangeNewsSettingPreference(this, mUserNotiSetting.isFriendweekactivitynews() != swFriendWeekActivity.isChecked());
                }
            }else{ //달라졌다면

                AlarmSettingActivity.setChangeNewsSettingPreference(this, mUserNotiSetting.isFriendtodayactivitynews() != swFriendTodayActivity.isChecked());
                AlarmSettingActivity.setChangeNewsSettingPreference(this, mUserNotiSetting.isFriendweekactivitynews() != swFriendWeekActivity.isChecked());

            }

            mUserNotiSetting.setFriendnews(swFriendNewsReport.isChecked());
            mUserNotiSetting.setFriendtodayactivitynews(swFriendTodayActivity.isChecked());
            mUserNotiSetting.setFriendweekactivitynews(swFriendWeekActivity.isChecked());

            result = userInfoServiceClient.cuUserNotiSetting(mUserNotiSetting, mUserInfo.getEmail());
        }catch(RestClientException e){
            result = false;
        }

        showUpdateResult(result);
    }

    @UiThread
    void showUpdateResult( boolean result ){
        if( result){
            Toast.makeText(this , "저장되었습니다.", Toast.LENGTH_SHORT);
            mDBManager.setUserNotiSetting( mUserNotiSetting );
        }else{
            Toast.makeText(this , "저장 중에 문제가 발생했습니다.", Toast.LENGTH_SHORT);
        }
    }

    private void displayActionBar( ){
        actionBarView.setBackgroundColor(this.getResources().getColor(R.color.fitmate_bar_gray));
        TextView tvBarTitle = (TextView) actionBarView.findViewById( R.id.tv_custom_action_bar_title );
        tvBarTitle.setText(strSettingAlarmBarTitle);

        ImageView imgBack = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_left);
        imgBack.setVisibility(View.GONE);

        ImageView mImgRight = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_right);
        mImgRight.setVisibility(View.GONE);
    }

    @Click(R.id.rl_activity_alarm_setting_alarmtime)
    void alarmTimeClick(){

        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm", Locale.US);
        try {
            cal.setTime(sdf.parse( tvAlarmTime.getText().toString() ));
        } catch (ParseException e) {
            e.printStackTrace();
            cal = null;
        }

        final Dialog alarmDialog = new Dialog( this , R.style.CustomDialog );
        alarmDialog.setContentView( R.layout.dialog_time_select );

        final TimePicker tp = (TimePicker) alarmDialog.findViewById(R.id.timePicker);
        tp.setCurrentHour( cal.get(Calendar.HOUR_OF_DAY) );
        tp.setCurrentMinute(cal.get(Calendar.MINUTE));

        alarmDialog.findViewById(R.id.img_search_email_dialog_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alarmDialog.dismiss();
            }
        });
        alarmDialog.findViewById(R.id.btn_search_email).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvAlarmTime.setText( String.format("%02d:%02d", tp.getCurrentHour(), tp.getCurrentMinute()) );
                alarmDialog.dismiss();
            }
        });
        alarmDialog.show();

    }

    @OnActivityResult(USERINFO_CHANGEALARM_CODE)
    void alarmSetResult( int resultCode , Intent data )
    {
        String alarmTime = data.getStringExtra("alarmtime");
        tvAlarmTime.setText(alarmTime);
    }

    @Click(R.id.rl_activity_alarm_setting_receive_alarm)
    void showReceiveAlarmActivity(){
        FriendAlarmSettingActivity_.intent( AlarmSettingActivity.this ).start();
    }

    public static boolean getChangeNewsSettingPreference( Context context )
    {
        SharedPreferences userInfo = context.getSharedPreferences( "fitmate" , Activity.MODE_PRIVATE  );
        return userInfo.getBoolean( "changenewssetting" , true);
    }

    public static void setChangeNewsSettingPreference( Context context , boolean isSet)
    {
        SharedPreferences userInfo = context.getSharedPreferences( "fitmate" , Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = userInfo.edit();
        editor.putBoolean( "changenewssetting" , isSet);
        editor.commit();
    }

}
