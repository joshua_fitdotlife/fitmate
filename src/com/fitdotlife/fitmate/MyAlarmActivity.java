package com.fitdotlife.fitmate;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;

import com.fitdotlife.fitmate_lib.object.UserInfo;

import java.util.Calendar;


public class MyAlarmActivity extends Activity{
    private final String TAG = "fitmate - " + MyAlarmActivity.class.getSimpleName();

    private TextView tvAlarm = null;
    private Switch swAlarm = null;
    Activity act = null;
    private String alarmTime = null;
    private Intent intent = new Intent();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm);

        act = this;

        final Calendar cal = UserInfo.getCalendarFromString(UserInfo.getAlarmTimeSharedPreference(act));

        this.tvAlarm = (TextView) this.findViewById(R.id.textView2);
        tvAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog mDialog = new Dialog(act , R.style.CustomDialog);
                mDialog.setContentView(R.layout.dialog_time_select);

                final TimePicker tp = ((TimePicker)mDialog.findViewById(R.id.timePicker));
                tp.setCurrentHour(cal.get(Calendar.HOUR_OF_DAY));
                tp.setCurrentMinute(cal.get(Calendar.MINUTE));

                mDialog.findViewById(R.id.img_search_email_dialog_close).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        tvAlarm.setText(String.format(getString(R.string.myprofile_alarm_time) + ": %02d:%02d", tp.getCurrentHour(), tp.getCurrentMinute()));
                        cal.set(Calendar.HOUR_OF_DAY, tp.getCurrentHour());
                        cal.set(Calendar.MINUTE, tp.getCurrentMinute());
                        UserInfo.saveAlarmTimeInSharedPreference(act, UserInfo.getTimeFromCalendar(cal));
                        alarmTime = String.format("%02d:%02d", tp.getCurrentHour(), tp.getCurrentMinute());
                        intent.putExtra("alarmtime", alarmTime);
                        setResult(RESULT_OK, intent);

                        mDialog.dismiss();
                    }
                });

                mDialog.findViewById(R.id.btn_search_email).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        tvAlarm.setText(String.format(getString(R.string.myprofile_alarm_time) + ": %02d:%02d", tp.getCurrentHour(), tp.getCurrentMinute()));
                        cal.set(Calendar.HOUR_OF_DAY, tp.getCurrentHour());
                        cal.set(Calendar.MINUTE, tp.getCurrentMinute());
                        UserInfo.saveAlarmTimeInSharedPreference(act, UserInfo.getTimeFromCalendar(cal));
                        alarmTime = String.format("%02d:%02d", tp.getCurrentHour(), tp.getCurrentMinute());
                        intent.putExtra("alarmtime", alarmTime);
                        setResult(RESULT_OK, intent);

                        //todo 시간 설정 구현 필요
                        mDialog.dismiss();
                    }
                });
                mDialog.show();
            }
        });

        tvAlarm.setText(String.format(getString(R.string.myprofile_alarm_time) + ": %02d:%02d", cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE)));
        alarmTime = String.format("%02d:%02d", cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE) );
        intent.putExtra("alarmtime", alarmTime);
        setResult(RESULT_OK, intent);

        this.swAlarm = (Switch) this.findViewById(R.id.switch1);
        swAlarm.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                UserInfo.saveAlarmSetInSharedPreference(act, swAlarm.isChecked());
                swAlarm.setChecked(UserInfo.getAlarmSetSharedPreference(act));
            }
        });

        swAlarm.setChecked(UserInfo.getAlarmSetSharedPreference(act));

        //액션바 설정
        ActionBar actionBar = getActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setDisplayUseLogoEnabled(false);
        View actionBarView = this.getLayoutInflater().inflate( R.layout.custom_action_bar_image, null );
        TextView tvBarTitle = (TextView) actionBarView.findViewById( R.id.tv_custom_action_bar_title );
        ImageView imgLeft = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_left);
        imgLeft.setVisibility(View.INVISIBLE);
        ImageView imgRight = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_right);
        imgRight.setVisibility(View.INVISIBLE);
        tvBarTitle.setText( this.getString(R.string.myprofile_alarm_setting) );
        actionBar.setCustomView(actionBarView);
        actionBar.setDisplayShowCustomEnabled(true);
    }
}
