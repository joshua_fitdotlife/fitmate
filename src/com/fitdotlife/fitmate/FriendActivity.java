package com.fitdotlife.fitmate;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fitdotlife.Preprocess;
import com.fitdotlife.fitmate_lib.customview.DrawerView;
import com.fitdotlife.fitmate_lib.customview.FindFriendListItem;
import com.fitdotlife.fitmate_lib.customview.FindFriendListItem_;
import com.fitdotlife.fitmate_lib.customview.FriendListTitleView;
import com.fitdotlife.fitmate_lib.customview.FriendListTitleView_;
import com.fitdotlife.fitmate_lib.customview.FriendNoContentView;
import com.fitdotlife.fitmate_lib.customview.FriendNoContentView_;
import com.fitdotlife.fitmate_lib.customview.FriendNoRequestView;
import com.fitdotlife.fitmate_lib.customview.FriendNoRequestView_;
import com.fitdotlife.fitmate_lib.customview.MyFriendListItem;
import com.fitdotlife.fitmate_lib.customview.MyFriendListItem_;
import com.fitdotlife.fitmate_lib.customview.RequestFriendReceiveRequestItemView;
import com.fitdotlife.fitmate_lib.customview.RequestFriendReceiveRequestItemView_;
import com.fitdotlife.fitmate_lib.customview.RequestFriendResultItemView;
import com.fitdotlife.fitmate_lib.customview.RequestFriendResultItemView_;
import com.fitdotlife.fitmate_lib.customview.RequestFriendSendRequestItemView;
import com.fitdotlife.fitmate_lib.customview.RequestFriendSendRequestItemView_;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.http.FriendService;
import com.fitdotlife.fitmate_lib.http.NetworkClass;
import com.fitdotlife.fitmate_lib.http.RestHttpErrorHandler;
import com.fitdotlife.fitmate_lib.http.UserInfoService;
import com.fitdotlife.fitmate_lib.key.FriendRequestType;
import com.fitdotlife.fitmate_lib.key.FriendStatusType;
import com.fitdotlife.fitmate_lib.object.FriendList;
import com.fitdotlife.fitmate_lib.object.FriendRequestList;
import com.fitdotlife.fitmate_lib.object.UserFriend;
import com.fitdotlife.fitmate_lib.object.UserFriendCheckedItem;
import com.fitdotlife.fitmate_lib.object.UserNotiSetting;
import com.fitdotlife.fitmate_lib.presenter.LoginPresenter;
import com.fitdotlife.fitmate_lib.util.Utils;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringRes;
import org.androidannotations.rest.spring.annotations.RestService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.client.RestClientException;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
@EActivity(R.layout.activity_friend)
public class FriendActivity extends Activity implements RefreshListener, View.OnTouchListener , NetWorkChangeListener
{

    public static final int FRIEND_MYFRIEND_RESULT = 0;
    public static final int FRIEND_FINDFRIEND_FRIEND = 1;
    public static final int FRIEND_REQUEST_ADD = 2;
    public static final int FRIEND_REQUESTFRIEND_FRIEND = 3;

    public static final String FRIEND_DELETED_FRIEND_EMAIL_KEY = "deletedfriendemail";

    public enum FriendModeType { NOSELECT , MYFRIEND , FINDFRIEND , REQUESTFRIEND }

    private FriendModeType mFriendMode = FriendModeType.NOSELECT;
    private FriendRequestList mRequestList;

    private boolean myFriendSearchMode = false;

    private List<UserFriendCheckedItem> mRecentIDList = new ArrayList<UserFriendCheckedItem>();
    private List<UserFriendCheckedItem> mRequestCheckList = new ArrayList<UserFriendCheckedItem>();

    private String mUserEmail = null;
    private int mUserID = 0;
    private FitmateDBManager mDBManager = null;
    private UserNotiSetting mUserNotiSetting = null;

    private ArrayList<UserFriend> mRecentList = null;
    private ArrayList<UserFriend> mFriendList = null;
    private InputMethodManager imm;

    private ProgressDialog mDialog = null;

    private MyApplication myApplication = null;

    @ViewById(R.id.tv_activity_friend_select_myfriend)
    TextView tvMyFriend;

    @ViewById(R.id.tv_activity_friend_select_findfriend)
    TextView tvFindFriend;

    @ViewById(R.id.tv_activity_friend_select_requestfriend)
    TextView tvRequestFriend;

    @ViewById(R.id.ll_activity_friend_select_myfriend)
    LinearLayout llMyFriendMark;

    @ViewById(R.id.ll_activity_friend_select_findfriend)
    LinearLayout llFindFrendMark;

    @ViewById(R.id.ll_activity_friend_select_requestfriend)
    LinearLayout llRequestFriendMark;

    @ViewById(R.id.ic_activity_friend_actionbar)
    View actionBarView;

    @ViewById(R.id.ll_activity_friend_content)
    LinearLayout llContent;

    @ViewById(R.id.etx_activity_friend_search_word)
    EditText etxSearch;

    @RestService
    FriendService friendServiceClient;

    @RestService
    UserInfoService userServiceClient;

    @StringRes(R.string.friend_common_myfriend)
    String strMyFriendTitle;

    @StringRes(R.string.friend_common_findfriend)
    String strFindFriendTitle;

    @StringRes(R.string.friend_common_requestfriend)
    String strRequestFriendTitle;

    @StringRes(R.string.friend_findfriend_recent_friend)
    String strRecentFriend;

    @ViewById(R.id.ll_activity_friend_select_myfriend_badge_count)
    LinearLayout llMyFriendBadge;

    @ViewById(R.id.tv_activity_friend_select_myfriend_badge_count)
    TextView tvMyFriendBadgeCount;

    @ViewById(R.id.ll_activity_friend_select_requestfriend_badge_count)
    LinearLayout llRequestFriendBadge;

    @ViewById(R.id.tv_activity_friend_select_requestfriend_badge_count)
    TextView tvRequestFriendBadgeCount;

    @ViewById(R.id.dl_activity_friend)
    DrawerLayout dlFriend;

    @ViewById(R.id.dv_activity_friend_drawer)
    DrawerView drawerView;

    @StringRes(R.string.friend_myfriend_getting_myfriend)
    String strGettingMyFrined;

    @StringRes(R.string.friend_findfriend_finding_friend)
    String strFindingFriend;

    @StringRes(R.string.friend_requestfriend_getting_friendlist)
    String strGettingRequestFriendList;

    @StringRes(R.string.common_connect_network)
    String strConnectNetwork;

    @ViewById(R.id.ll_activity_friend_search)
    LinearLayout llSearch;

    @ViewById(R.id.rl_activity_friend_progress)
    RelativeLayout rlProgress;

    @Bean
    RestHttpErrorHandler restErrorHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        myApplication = (MyApplication) this.getApplication();
        if(!Preprocess.IsTest) {
            Tracker t = myApplication.getTracker(MyApplication.TrackerName.APP_TRACKER);
            t.setScreenName("Friend");
            t.send(new HitBuilders.AppViewBuilder().build());
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(!Preprocess.IsTest) {
            GoogleAnalytics.getInstance(this).reportActivityStart(this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(!Preprocess.IsTest) {
            GoogleAnalytics.getInstance(this).reportActivityStop(this);
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        myApplication.deleteNetworkChangeListener(this);
    }

    @AfterViews
    void init(){

        friendServiceClient.setRootUrl(NetworkClass.baseURL + "/api");
        friendServiceClient.setRestErrorHandler(restErrorHandler);
        userServiceClient.setRootUrl(NetworkClass.baseURL + "/api");
        userServiceClient.setRestErrorHandler(restErrorHandler);

        ActivityManager.getInstance().addActivity(this);
        this.imm =  (InputMethodManager)getSystemService( Context.INPUT_METHOD_SERVICE );

        this.mDBManager = new FitmateDBManager(this);
        this.mUserEmail = this.mDBManager.getUserInfo().getEmail();
        this.mUserNotiSetting = this.mDBManager.getUserNotiSetting();

        this.etxSearch.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                etxSearch.setFocusableInTouchMode(true);
                return false;
            }
        });

        this.drawerView.setParentActivity(this);
        this.drawerView.setTag(this.getResources().getString(R.string.gnb_friend));
        this.drawerView.setDrawerLayout(dlFriend);

        this.mFriendMode = FriendModeType.MYFRIEND;

        //액션바를 보여준다.
        this.displayActionBar();

        if(Utils.isOnline(this) ){
            this.getUserID();
            this.getRequestFriendCount();

            //this.startProgressDialog(strMyFriendTitle, strGettingMyFrined);
            showProgressView();
            this.getFriendList(true);
            //this.stopProgressDialog();
            dismissProgressView();
        }else{
            showToast(strConnectNetwork);
            myApplication.addNetworkChangeListener(this);
        }

    }

    @Background
    void getUserID(){
        try{
            this.mUserID = userServiceClient.getUserID(mUserEmail);
        }catch(RestClientException e){
            Log.e("fitmate", e.getMessage());
        }
    }

    private void displayActionBar(){
        actionBarView.setBackgroundColor(this.getResources().getColor(R.color.fitmate_blue));
        TextView tvBarTitle = (TextView) actionBarView.findViewById( R.id.tv_custom_action_bar_title );
        tvBarTitle.setText(this.getString(R.string.friend_bar_title));
        ImageView imgLeft = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_left);
        imgLeft.setBackgroundResource(R.drawable.icon_home_selector);
        imgLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityManager.getInstance().finishAllActivity();
                showNewHomeActivity();
            }
        });

        ImageView imgSliding = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_right);
        imgSliding.setImageResource(R.drawable.icon_slide);
        imgSliding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dlFriend.openDrawer(drawerView);
            }
        });
        imgSliding.setOnTouchListener(this);

        ImageView imgFriendSet = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_right_more);
        imgFriendSet.setImageResource(R.drawable.icon_friendset);
        imgFriendSet.setOnTouchListener(this);
        imgFriendSet.setVisibility(View.VISIBLE);
        imgFriendSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mUserNotiSetting = mDBManager.getUserNotiSetting();

                if( mUserNotiSetting.isFriendnews()) {

                    Intent bundle = new Intent();
                    FriendAlarmSettingActivity_.intent(FriendActivity.this).start();

                }else{

                    AlertDialog.Builder builder = new AlertDialog.Builder( FriendActivity.this );     // 여기서 this는 Activity의 this
                    builder.setTitle(getString( R.string.friend_receivealarm_title ) )        // 제목 설정
                            .setMessage(getString(R.string.friend_receivealarm_content))        // 메세지 설정
                            .setCancelable(true)        // 뒤로 버튼 클릭시 취소 가능 설정
                            .setPositiveButton(R.string.common_on, new DialogInterface.OnClickListener() {
                                // 확인 버튼 클릭시 설정
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    mUserNotiSetting.setFriendnews(true);
                                    mUserNotiSetting.setFriendtodayactivitynews(true);
                                    mUserNotiSetting.setFriendweekactivitynews(true);
                                    updateUserNotiSetting();
                                    dialog.cancel();
                                }
                            })
                            .setNegativeButton(R.string.common_off, new DialogInterface.OnClickListener() {
                                // 취소 버튼 클릭시 설정
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    dialog.cancel();
                                }
                            });

                    AlertDialog dialog = builder.create();    // 알림창 객체 생성
                    dialog.show();    // 알림창 띄우기

                }

            }
        });
    }

    @Background
    void getFriendList( boolean isDisplay ){

        try {

            FriendList friendList = friendServiceClient.getFriendList(mUserEmail);
            if( friendList != null){
                displayMyFriendList(friendList, isDisplay);
            }
            else{

            }

        }catch(RestClientException e){
            Log.e("fitmate", e.getMessage());
        }
    }

    @Background
    void findFriendListSearch(){

        if(!Utils.isOnline(this)){
            this.showToast(strConnectNetwork);
            return;
        }

        try {

            List<UserFriend> friendList = friendServiceClient.findFriends(mUserEmail, etxSearch.getText().toString());

            if( friendList != null ) {
                if (friendList.size() > 0) {
                    displayFindFriendList(friendList);
                } else {
                    this.showToast(this.getResources().getString(R.string.friend_findfriend_noresult));
                }
            }

        }catch(RestClientException e) {
            Log.e("fitmate", e.getMessage());
        }
    }

    @Background
    void getFriendReuestList(){

        try {
            FriendRequestList requestList = friendServiceClient.getFriendRequestList(mUserEmail);

            if(requestList != null) {
                this.mRequestList = requestList;
                displayFriendRequestList(requestList);
            }
        }catch(RestClientException e) {
            Log.e("fitmate", e.getMessage());
        }
    }

    @Background
    void getRecentFriendCount(){
        try{
            int recentFriendCount = friendServiceClient.getRecentFriendCount(mUserEmail);
            if( recentFriendCount > 0  ){
                this.displayRecentFriendCount(recentFriendCount);
            }
        }catch ( RestClientException e ){
            Log.e("fitmate", e.getMessage());
        }
    }

    @Background
    void getRequestFriendCount(){
        try{
            int requestFriendCount = friendServiceClient.getRequestFriendCount(mUserEmail);
            if( requestFriendCount > 0  ){
                this.displayRequestFriendCount(requestFriendCount);
            }
        }catch ( RestClientException e ){
            Log.e("fitmate", e.getMessage());
        }
    }

    @UiThread
    void displayRequestFriendCount( int requestFriendCount ){

        this.llRequestFriendBadge.setVisibility(View.VISIBLE);
        this.tvRequestFriendBadgeCount.setText(requestFriendCount + "");

    }


    @UiThread
    void displayRecentFriendCount( int recentFriendCount ){

        this.llMyFriendBadge.setVisibility(View.VISIBLE);
        this.tvMyFriendBadgeCount.setText(recentFriendCount + "");

    }


    @UiThread
    void displayFriendRequestList( FriendRequestList  requestList ){

        llContent.removeAllViews();
        //검색을 없앤다.
        this.llSearch.setVisibility( View.GONE );

        //받은 신청
        FriendListTitleView receiveTitleView = FriendListTitleView_.build(this);
        receiveTitleView.setTitle(this.getString(R.string.friend_requestfriend_receive_request));
        llContent.addView(receiveTitleView);

        if( requestList != null )
        {
            List<UserFriend> receiveList = requestList.getReceiveList();
            if( receiveList != null )
            {
                if(receiveList.size() > 0 ) {
                    for (int location = 0; location < receiveList.size(); location++) {
                        UserFriend userFriend = receiveList.get(location);
                        FriendStatusType friendStatusType = FriendStatusType.getFriendStatusType(userFriend.getRequestStatus());
                        if (friendStatusType.equals(FriendStatusType.REQUEST)) {

                            RequestFriendReceiveRequestItemView receiveRequestItemView = RequestFriendReceiveRequestItemView_.build(this);
                            receiveRequestItemView.setUserFriend(userFriend, receiveList, location, this.mUserEmail);
                            receiveRequestItemView.setRefreshListener(this);
                            llContent.addView(receiveRequestItemView);

                        } else if (friendStatusType.equals(FriendStatusType.ACCEPT)) {

                            RequestFriendResultItemView resultView = RequestFriendResultItemView_.build(this);
                            resultView.setUserFriend(userFriend, receiveList, location, friendStatusType, FriendRequestType.RECEIVEREQUEST.getValue());
                            resultView.setRefreshListener(this);
                            llContent.addView(resultView);

                        } else if (friendStatusType.equals(FriendStatusType.DECLINE)) {

                            RequestFriendResultItemView resultView = RequestFriendResultItemView_.build(this);
                            resultView.setUserFriend(userFriend, receiveList, location, friendStatusType, FriendRequestType.RECEIVEREQUEST.getValue());
                            resultView.setRefreshListener(this);
                            llContent.addView(resultView);
                        }

                        if (!userFriend.isCheckedRequestFriend()) {
                            UserFriendCheckedItem checkedItem = new UserFriendCheckedItem();
                            checkedItem.setRequestvalue(FriendRequestType.RECEIVEREQUEST.getValue());
                            checkedItem.setId(userFriend.getUserFriendsID());
                            this.mRequestCheckList.add(checkedItem);
                        }
                    }
                }else{
                    FriendNoRequestView noRequestContentView = FriendNoRequestView_.build(this);
                    noRequestContentView.init( this.getString(R.string.friend_requestfriend_no_receive_request) );
                    llContent.addView(noRequestContentView);
                }
            }else{

                FriendNoRequestView noRequestContentView = FriendNoRequestView_.build(this);
                noRequestContentView.init( this.getString(R.string.friend_requestfriend_no_receive_request) );
                llContent.addView(noRequestContentView);
            }

        }
        //보낸 신청

        FriendListTitleView sendTitleView = FriendListTitleView_.build(this);
        sendTitleView.setTitle(this.getString(R.string.friend_requestfriend_send_request));
        llContent.addView(sendTitleView);
        if(requestList != null)
        {
            List<UserFriend> sendList = requestList.getSendList();
            if(sendList != null) {

                if( sendList.size() > 0 )
                {
                    for (int location = 0; location < sendList.size(); location++) {
                        UserFriend userFriend = sendList.get(location);
                        FriendStatusType friendStatusType = FriendStatusType.getFriendStatusType(userFriend.getRequestStatus());
                        if (friendStatusType.equals(FriendStatusType.REQUEST)) {

                            RequestFriendSendRequestItemView sendRequestView = RequestFriendSendRequestItemView_.build(this);
                            sendRequestView.setUserFriend(userFriend, sendList, location, mUserEmail);
                            sendRequestView.setRefreshListener(this);
                            llContent.addView(sendRequestView);

                        } else if (friendStatusType.equals(FriendStatusType.ACCEPT)) {

                            RequestFriendResultItemView resultView = RequestFriendResultItemView_.build(this);
                            resultView.setUserFriend(userFriend, sendList, location, friendStatusType, FriendRequestType.SENDREQUEST.getValue());
                            resultView.setRefreshListener(this);
                            llContent.addView(resultView);
                        } else if (friendStatusType.equals(FriendStatusType.CANCLE)) {

                            RequestFriendResultItemView resultView = RequestFriendResultItemView_.build(this);
                            resultView.setUserFriend(userFriend, sendList, location, friendStatusType, FriendRequestType.SENDREQUEST.getValue());
                            resultView.setRefreshListener(this);
                            llContent.addView(resultView);
                        }

                        if (!userFriend.isCheckedRequestUser()) {
                            UserFriendCheckedItem checkedItem = new UserFriendCheckedItem();
                            checkedItem.setRequestvalue(FriendRequestType.SENDREQUEST.getValue());
                            checkedItem.setId(userFriend.getUserFriendsID());
                            this.mRequestCheckList.add(checkedItem);
                        }
                    }
                }else{
                    FriendNoRequestView noRequestContentView = FriendNoRequestView_.build(this);
                    noRequestContentView.init( this.getString(R.string.friend_requestfriend_no_send_request) );
                    llContent.addView(noRequestContentView);
                }
            }else{
                FriendNoRequestView noRequestContentView = FriendNoRequestView_.build(this);
                noRequestContentView.init( this.getString(R.string.friend_requestfriend_no_send_request) );
                llContent.addView(noRequestContentView);
            }
        }

        this.updateRequestIDList();
    }

    @UiThread
    void displayNoContent(){
        FriendNoContentView noContentView = FriendNoContentView_.build(this);
        noContentView.setMoveListener(new FriendNoContentView.MoveListener() {
            @Override
            public void clickMoveFindFreind() {
                selectFindFriendList();
            }
        });

        llContent.addView(noContentView);
    }

    @UiThread
    void displayMyFriendList( FriendList friendList , boolean isDisplay ){

        if( friendList.getFriendList().size() == 0 && friendList.getRecentList().size()==0 ){
            llContent.removeAllViews();
            this.llSearch.setVisibility(View.GONE);
            displayNoContent();
            return;
        }

        this.etxSearch.setText("");
        this.etxSearch.setHint(R.string.friend_myfriend_search);

        List<UserFriend> recentList = friendList.getRecentList();
        List<UserFriend> jaeumList = friendList.getFriendList();

        if( recentList.size() > 0 || jaeumList.size() > 0 )
        {

            llContent.removeAllViews();
            this.llSearch.setVisibility(View.VISIBLE);

            if (recentList.size() > 0) {
                this.mRecentList = (ArrayList<UserFriend>) recentList;

                if (isDisplay) {
                    this.displayRecentList(recentList);
                }
            }

            if (jaeumList.size() > 0) {
                this.mFriendList = (ArrayList<UserFriend>) jaeumList;

                if (isDisplay) {
                    displayJaeumList(jaeumList);
                }
            } else {
                if (isDisplay) {
                    displayNoContent();
                }
            }
        }
    }



    @UiThread
    void displayRecentList( List<UserFriend> friendList ){

        FriendListTitleView titleView = FriendListTitleView_.build(this);
        titleView.setTitle(strRecentFriend);
        llContent.addView(titleView);

        for( UserFriend userFriend : friendList  ) {

            MyFriendListItem listItem = MyFriendListItem_.build(this);
            listItem.setUserFriend(userFriend);
            listItem.setBackgroundColor(0xFFE6F7FD);
            llContent.addView(listItem);

            UserFriendCheckedItem checkedItem = new UserFriendCheckedItem();
            checkedItem.setId( userFriend.getUserFriendsID() );
            checkedItem.setRequestvalue(userFriend.getRequestType());
            this.mRecentIDList.add(checkedItem);
        }

        this.updateRecentIDList();
    }

    @UiThread
    void displayJaeumList( List<UserFriend> friendList ){
        String groupTitle = "";

        for( UserFriend userFriend : friendList  ) {
            if (!groupTitle.equals(userFriend.getJaeum())) {
                FriendListTitleView titleView = FriendListTitleView_.build(this);
                titleView.setTitle(userFriend.getJaeum());

                llContent.addView(titleView);
                groupTitle = userFriend.getJaeum();
            }

            MyFriendListItem listItem = MyFriendListItem_.build(this);
            listItem.setUserFriend(userFriend);
            llContent.addView(listItem);
        }
    }

    @UiThread
    void displayFindFriendList( List<UserFriend> friendList )
    {
        llContent.removeAllViews();

        for( UserFriend userFriend : friendList  ){
            FindFriendListItem listItem = FindFriendListItem_.build(this);
            listItem.setUserFriend(userFriend, mUserEmail, mUserID);
            listItem.setRefreshListener(this);
            llContent.addView(listItem);
        }
    }

    @UiThread
    void showToast(String message)
    {
        Toast.makeText(this , message , Toast.LENGTH_SHORT).show();
    }

    @Click(R.id.rl_activity_friend_select_myfriend )
    void selectMyFriendList(){

        Log.d("fitmate" ,  "내 친구 클릭");

        imm.hideSoftInputFromWindow(etxSearch.getWindowToken(), 0);
        myFriendSearchMode = false;

        this.mFriendMode = FriendModeType.MYFRIEND;
        this.llMyFriendBadge.setVisibility(View.INVISIBLE);

        this.tvMyFriend.setTextColor(Color.BLACK);
        this.tvFindFriend.setTextColor(0xFFA9A9A9);
        this.tvRequestFriend.setTextColor(0xFFA9A9A9);

        this.llMyFriendMark.setBackgroundColor(this.getResources().getColor(R.color.fitmate_blue));
        this.llFindFrendMark.setBackgroundColor(Color.WHITE);
        this.llRequestFriendMark.setBackgroundColor(Color.WHITE);

        if( Utils.isOnline(this)) {
            this.getRequestFriendCount();
            this.startProgressDialog(strMyFriendTitle, strGettingMyFrined);
            this.getFriendList(true);
            this.stopProgressDialog();
        }else{
            this.showToast(strConnectNetwork);
        }

    }

    @Click(R.id.rl_activity_friend_select_findfriend)
    void selectFindFriendList(){

        Log.d("fitmate" ,  "친구 검색 클릭");
        imm.hideSoftInputFromWindow(etxSearch.getWindowToken(), 0);
        //this.checkUpdate();

        this.mFriendMode = FriendModeType.FINDFRIEND;

        this.tvMyFriend.setTextColor(0xFFA9A9A9);
        this.tvFindFriend.setTextColor(Color.BLACK);
        this.tvRequestFriend.setTextColor(0xFFA9A9A9);

        this.llMyFriendMark.setBackgroundColor(Color.WHITE);
        this.llFindFrendMark.setBackgroundColor(this.getResources().getColor(R.color.fitmate_blue));
        this.llRequestFriendMark.setBackgroundColor(Color.WHITE);

        this.llSearch.setVisibility(View.VISIBLE);
        this.etxSearch.setText("");
        this.etxSearch.setHint(R.string.friend_findfriend_newfriend_search);

        if(Utils.isOnline(this))
        {
            this.getRequestFriendCount();
            this.getRecentFriendCount();
        }

        llContent.removeAllViews();
    }

    @Click(R.id.rl_activity_friend_select_requestfriend)
    void selectRequestFriend(){

        Log.d("fitmate" ,  "친구 신청 목록 클릭");
        imm.hideSoftInputFromWindow(etxSearch.getWindowToken(), 0);
        //this.checkUpdate();

        this.mFriendMode = FriendModeType.REQUESTFRIEND;

        this.llRequestFriendBadge.setVisibility(View.INVISIBLE);

        this.tvMyFriend.setTextColor(0xFFA9A9A9);
        this.tvFindFriend.setTextColor(0xFFA9A9A9);
        this.tvRequestFriend.setTextColor(Color.BLACK);

        this.etxSearch.setText("");

        this.llMyFriendMark.setBackgroundColor(Color.WHITE);
        this.llFindFrendMark.setBackgroundColor(Color.WHITE);
        this.llRequestFriendMark.setBackgroundColor(this.getResources().getColor(R.color.fitmate_blue));

        if( Utils.isOnline(this) ) {
            this.getRecentFriendCount();
            this.startProgressDialog(strRequestFriendTitle, strGettingRequestFriendList);
            this.getFriendReuestList();
            this.stopProgressDialog();
        }else{
            this.showToast(strConnectNetwork);
        }
    }

    private void checkUpdate(){
        if( mFriendMode.equals( FriendModeType.MYFRIEND ) ){
            this.updateRecentIDList();
        }

        if( mFriendMode.equals( FriendModeType.REQUESTFRIEND ) ){
            this.updateRequestIDList();
        }
    }

    @Background
    void updateRecentIDList(){
        if( mRecentIDList.size() > 0 ) {

            JSONArray jsonArray = new JSONArray();
            for( UserFriendCheckedItem checkedItem : mRecentIDList  ){

                JSONObject obj = new JSONObject();
                try{

                    obj.put("id" , checkedItem.getId());
                    obj.put("requestvalue" , checkedItem.getRequestvalue() );
                }catch( JSONException e){

                }
                jsonArray.put( obj );
            }

            friendServiceClient.updateCheckRecentList(mUserEmail, jsonArray.toString());
            this.mRecentIDList.clear();
        }
    }

    @Background
    void updateRequestIDList( ){
        if( mRequestCheckList.size() > 0 ) {
            JSONArray jsonArray = new JSONArray();
            for( UserFriendCheckedItem checkedItem : mRequestCheckList  ){

                JSONObject obj = new JSONObject();
                try{

                    obj.put("id" , checkedItem.getId());
                    obj.put("requestvalue" , checkedItem.getRequestvalue() );
                }catch( JSONException e){

                }
                jsonArray.put( obj );
            }

            friendServiceClient.updateCheckRequestList(mUserEmail, jsonArray.toString());
            this.mRequestCheckList.clear();
        }
    }

    @Click
    void img_activity_friend_search(){
        if( this.etxSearch.getText().length() == 0 ){
            this.etxSearch.requestFocus();
            Toast.makeText(this , this.getString(R.string.friend_findfriend_write_friendname) , Toast.LENGTH_SHORT).show();
            return;
        }

        this.etxSearch.setFocusable(false);
        if( mFriendMode.equals( FriendModeType.FINDFRIEND ) ) {
            this.startProgressDialog(strFindFriendTitle, strFindingFriend);
            findFriendListSearch();
            this.stopProgressDialog();
        }else if( mFriendMode.equals( FriendModeType.MYFRIEND ) ){

            //친구가 없을 때 검색이 안되게 함.
            if( mFriendList == null )
            {
                imm.hideSoftInputFromWindow(etxSearch.getWindowToken(), 0);
                return;
            }

            String searchWord = this.etxSearch.getText().toString();
            this.myFriendSearchMode = true;

            ArrayList<UserFriend> recentList = new ArrayList<UserFriend>();
            if( mRecentList != null ) {
                Iterator<UserFriend> recentListIter = mRecentList.iterator();
                while (recentListIter.hasNext()) {
                    UserFriend userFriend = recentListIter.next();
                    if (userFriend.getName().contains(searchWord)) {
                        recentList.add(userFriend);
                    }
                }
            }

            ArrayList<UserFriend> friendList = new ArrayList<UserFriend>();
            Iterator<UserFriend> friendListIter = mFriendList.iterator();
            while( friendListIter.hasNext() ){
                UserFriend userFriend = friendListIter.next();
                if( userFriend.getName().contains( searchWord ) ){
                    friendList.add(userFriend);
                }
            }

            this.mRecentList = recentList;
            this.mFriendList = friendList;


            llContent.removeAllViews();
            if (mRecentList.size() > 0) {
                this.displayRecentList(recentList);
            }

            if (mFriendList.size() > 0) {
                displayJaeumList(friendList);
            }
        }

        imm.hideSoftInputFromWindow(etxSearch.getWindowToken(), 0);
    }


    @Override
    public void onBackPressed() {
        if(dlFriend.isDrawerOpen(drawerView)){
            dlFriend.closeDrawers();
            return;
        }



//        AlertDialog.Builder builder = new AlertDialog.Builder(this);     // 여기서 this는 Activity의 this
//
//        builder.setTitle(getString(R.string.common_quit_title))        // 제목 설정
//                .setMessage(getString(R.string.common_quit_content))        // 메세지 설정
//                .setCancelable(true)        // 뒤로 버튼 클릭시 취소 가능 설정
//                .setPositiveButton(R.string.common_yes, new DialogInterface.OnClickListener() {
//                    // 확인 버튼 클릭시 설정
//                    public void onClick(DialogInterface dialog, int whichButton) {
//                        moveTaskToBack(true);
//                        finish();
//                        dialog.cancel();
//                    }
//                })
//                .setNegativeButton(R.string.common_no, new DialogInterface.OnClickListener() {
//                    // 취소 버튼 클릭시 설정
//                    public void onClick(DialogInterface dialog, int whichButton) {
//                        dialog.cancel();
//                    }
//                });
//
//        AlertDialog dialog = builder.create();    // 알림창 객체 생성
//        dialog.show();    // 알림창 띄우기

        ActivityManager.getInstance().finishAllActivity();
        showNewHomeActivity();
    }


    @Override
    public void refreshList( FriendModeType modeType ) {
        if( modeType.equals( FriendModeType.FINDFRIEND ) ) {

            this.getRequestFriendCount();

        }else if(modeType.equals( FriendModeType.REQUESTFRIEND )){
            this.getRecentFriendCount();
            this.displayFriendRequestList(this.mRequestList);
        }

    }

//    @OnActivityResult(FriendActivity.FRIEND_REQUEST_ADD)
//    void onAddResult( int resultCode ){
//        if( this.mFriendMode.equals(FriendModeType.FINDFRIEND) ) {
//            this.getRequestFriendCount();
//            this.findFriendListSearch();
//        }else if( this.mFriendMode.equals( FriendModeType.REQUESTFRIEND ) ){
//            this.startProgressDialog( strRequestFriendTitle , strGettingRequestFriendList );
//            this.getFriendReuestList();
//            this.stopProgressDialog();
//        }
//    }


    @OnActivityResult(FriendActivity.FRIEND_REQUEST_ADD)
    void onFindFriendAddResult( int resultCode , Intent data ){
        if(data!= null)
        {
            boolean changeStatus = data.getBooleanExtra("changestatus", false);
            if (changeStatus) {
                if (mFriendMode.equals(FriendModeType.FINDFRIEND)) {
                    this.getRequestFriendCount();
                    this.findFriendListSearch();
                } else if (mFriendMode.equals(FriendModeType.REQUESTFRIEND)) {
                    this.startProgressDialog(strRequestFriendTitle, strGettingRequestFriendList);
                    this.getFriendReuestList();
                    this.stopProgressDialog();
                }
            }
        }
    }

    @OnActivityResult( FriendActivity.FRIEND_FINDFRIEND_FRIEND )
    void onFindFriendResult( int resultCode ){
        String deleteFriendEmail = this.getDeletedFriendEmail();
        if( deleteFriendEmail != null ) {
            this.getRequestFriendCount();
            this.findFriendListSearch();
        }
    }




//    @OnActivityResult(FriendActivity.FRIEND_REQUEST_SELECT)
//    void onSelectResult( int resultCode ){
//        this.getRecentFriendCount();
//        this.getFriendReuestList();
//    }

//    @OnActivityResult(FriendActivity.FRIEND_REQUEST_ALARM)
//    void onAlarmResult( int resultCode ){
//        if( this.mFriendMode.equals(FriendModeType.FINDFRIEND) || this.mFriendMode.equals( FriendModeType.REQUESTFRIEND ) ) {
//            //this.getFriendList( false );
//        }else if( this.mFriendMode.equals( FriendModeType.MYFRIEND ) ){
//            this.startProgressDialog(strMyFriendTitle , strGettingMyFrined);
//            this.getFriendList( true );
//            this.stopProgressDialog();;
//        }
//    }

    @OnActivityResult( FriendActivity.FRIEND_MYFRIEND_RESULT )
    void onMyFriendResult(){
        String deleteFriendEmail = this.getDeletedFriendEmail();
        if( deleteFriendEmail != null )
        {

            if(myFriendSearchMode){
                if( mRecentList != null ) {

                    UserFriend deleteRecentFriend = null;
                    Iterator<UserFriend> recentListIter = mRecentList.iterator();
                    while (recentListIter.hasNext()) {
                        UserFriend userFriend = recentListIter.next();
                        if (userFriend.getEmail().equals( deleteFriendEmail )) {
                            deleteRecentFriend = userFriend;
                        }
                    }

                    if(deleteRecentFriend != null){
                        mRecentList.remove(deleteRecentFriend);
                    }
                }

                UserFriend deleteFriend = null;
                Iterator<UserFriend> friendListIter = mFriendList.iterator();
                while( friendListIter.hasNext() ){
                    UserFriend userFriend = friendListIter.next();
                    if (userFriend.getEmail().equals(deleteFriendEmail)) {
                        deleteFriend = userFriend;
                    }
                }

                if( deleteFriend != null ){
                    mFriendList.remove(deleteFriend);
                }

                llContent.removeAllViews();
                if (mRecentList.size() > 0) {
                    this.displayRecentList(mRecentList);
                }

                if (mFriendList.size() > 0) {
                    displayJaeumList(mFriendList);
                }

            }else{

                this.startProgressDialog(strMyFriendTitle , strGettingMyFrined);
                this.getFriendList( true );
                this.stopProgressDialog();

            }
        }
    }

    @OnActivityResult(FriendActivity.FRIEND_REQUESTFRIEND_FRIEND)
    void onRquestFriendFriendResult(){
        this.startProgressDialog( strRequestFriendTitle , strGettingRequestFriendList );
        this.getFriendReuestList();
        this.stopProgressDialog();
    }

    @OnActivityResult( DrawerView.REQUEST_PROFILE_IMAGE )
    void onProfileImageChangeResult(){
        this.drawerView.refreshProfileImage();
    }



    public void startProgressDialog( String title , String content ){
        mDialog =  new ProgressDialog(this);
        mDialog.setTitle( title );
        mDialog.setMessage( content );
        mDialog.setIndeterminate( true );
        mDialog.setCancelable( false );
        mDialog.show();
    }

    public void stopProgressDialog(){
        if( this.mDialog != null ) {
            this.mDialog.dismiss();
        }
    }

    private void showProgressView(){

        rlProgress.setVisibility(View.VISIBLE);

    }

    private void dismissProgressView(){

        rlProgress.setVisibility(View.INVISIBLE);

    }


    private String getDeletedFriendEmail(){
        SharedPreferences pref = this.getSharedPreferences( "fitmateservice" , Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        return pref.getString(FriendActivity.FRIEND_DELETED_FRIEND_EMAIL_KEY, null);
    }

    @Background
    void updateUserNotiSetting(){

        boolean result = false;
        try {

            result = userServiceClient.cuUserNotiSetting(mUserNotiSetting, mUserEmail );

        }catch(RestClientException e){
            result = false;
        }

        showUpdateResult(result);
    }

    @UiThread
    void showUpdateResult(boolean result){
        if(result) {
            mDBManager.setUserNotiSetting(mUserNotiSetting);
            Intent bundle = new Intent();
            FriendAlarmSettingActivity_.intent(FriendActivity.this).start();
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        ImageView actionbarImageView = (ImageView) view;
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            actionbarImageView.setColorFilter(this.getResources().getColor(R.color.selected_colorfilter) , PorterDuff.Mode.DARKEN );
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            actionbarImageView.setColorFilter(null);
        }

        return false;
    }

    @Override
    public void OnNetworkAvailable()
    {
        this.getUserID();
        this.getRequestFriendCount();

        //this.startProgressDialog(strMyFriendTitle, strGettingMyFrined);
        showProgressView();
        this.getFriendList(true);
        //this.stopProgressDialog();
        dismissProgressView();
    }

    private boolean getSettedUserInfo()
    {
        SharedPreferences pref = this.getSharedPreferences("fitmateservice", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        return pref.getBoolean( LoginPresenter.SET_USERINFO_KEY , false);
    }

    private void showNewHomeActivity(){
        Intent intent = new Intent();

        if( getSettedUserInfo() ) {
            intent.setClass( this , NewHomeActivity_.class );
            intent.putExtra(NewHomeActivity.HOME_LAUNCH_KEY, NewHomeActivity.LaunchMode.FROM_OTHERMENU.ordinal());
        }else{
            intent.setClass( this , NewHomeAsActivity_.class );
        }

        startActivity( intent );
    }

}