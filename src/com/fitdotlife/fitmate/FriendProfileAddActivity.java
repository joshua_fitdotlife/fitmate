package com.fitdotlife.fitmate;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.fitdotlife.fitmate_lib.customview.FriendProfileCommonView;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.http.FriendService;
import com.fitdotlife.fitmate_lib.http.NetworkClass;
import com.fitdotlife.fitmate_lib.http.RestHttpErrorHandler;
import com.fitdotlife.fitmate_lib.key.FriendStatusType;
import com.fitdotlife.fitmate_lib.object.UserFriend;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringRes;
import org.androidannotations.rest.spring.annotations.RestService;
import org.springframework.web.client.RestClientException;

@EActivity( R.layout.activity_friend_profile_add )
public class FriendProfileAddActivity extends Activity {

    private String friendEmail;
    private int mInsertID;
    private FriendStatusType statusType;
    private UserInfo mUserInfo;

    @RestService
    FriendService friendServiceClient;

    @ViewById(R.id.txt_activity_friend_profile_add_introself)
    TextView txtIntroSelf;

    @ViewById(R.id.fpv_activity_friend_profile_add)
    protected FriendProfileCommonView commonView;

    @ViewById(R.id.ic_activity_friend_actionbar)
    View actionBarView;

    @ViewById(R.id.btn_activity_friend_profile_addfriend)
    Button btnAdd;

    @StringRes(R.string.friend_friendprofile_addfriend)
    String addFriend;

    @StringRes(R.string.friend_friendprofile_canclerequest)
    String cancleFriend;

    @Bean
    RestHttpErrorHandler restErrorHandler;

    @AfterViews
    void Init(){

        friendServiceClient.setRootUrl(NetworkClass.baseURL + "/api");
        friendServiceClient.setRestErrorHandler( restErrorHandler );

        FitmateDBManager dbManager = new FitmateDBManager(this);
        mUserInfo = dbManager.getUserInfo();

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        UserFriend userFriend = extras.getParcelable("UserFriend");

        String imageName = userFriend.getProfileImagePath();
        if( !( imageName == null ) ) {
            commonView.setProfileImage(imageName);
        }

        String name =  userFriend.getName();
        commonView.setName( name);
        this.displayActionBar(name);

        txtIntroSelf.setText(userFriend.getIntroYourSelf());
        this.friendEmail = userFriend.getEmail();
        this.statusType = FriendStatusType.getFriendStatusType( userFriend.getRequestStatus());
        this.mInsertID = userFriend.getUserFriendsID();

        if( statusType.equals(FriendStatusType.REQUEST) ){
            btnAdd.setText(cancleFriend);
        } else if(statusType.equals(FriendStatusType.NOVALUE)){
            btnAdd.setText(addFriend);
        }
    }

    private void displayActionBar( String name ){
        actionBarView.setBackgroundColor(this.getResources().getColor(R.color.fitmate_blue));
        TextView tvBarTitle = (TextView) actionBarView.findViewById( R.id.tv_custom_action_bar_title );
        tvBarTitle.setText(name + " " + this.getString(R.string.friend_friendprofile_title));
        ImageView imgBack = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_left);
        imgBack.setVisibility(View.GONE);

        ImageView mImgRight = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_right);
        mImgRight.setVisibility(View.GONE);
    }

    @Click(R.id.btn_activity_friend_profile_addfriend)
    void addFriendClick(  ){

        if( btnAdd.getText().toString().equals( addFriend ) ){

            this.addFriend();

        }else{

            this.cancleFriend();

        }

    }

    @Background
    void cancleFriend(){
        try {
            String email = mUserInfo.getEmail();
            boolean result = friendServiceClient.changeStatus(email, mInsertID , FriendStatusType.CANCLE.getValue() );

            if (result) {
                statusType = FriendStatusType.CANCLE;
                mInsertID = 0;
                this.setAddButtonText(addFriend);
                Intent intent = new Intent();
                intent.putExtra( "changestatus" , true );
                this.setResult( FriendActivity.FRIEND_REQUEST_ADD , intent );
            }
        }catch( RestClientException e ){

        }
    }

    @Background
    void addFriend(){
        try {
            String email = mUserInfo.getEmail();
            int addID = friendServiceClient.addFriend(email, friendEmail);

            if(addID > 0){
                statusType = FriendStatusType.REQUEST;
                this.mInsertID = addID;
                this.setAddButtonText(cancleFriend);
                Intent intent = new Intent();
                intent.putExtra( "changestatus" , true );
                this.setResult(FriendActivity.FRIEND_REQUEST_ADD, intent);
            }

        }catch(RestClientException e){

        }
    }

    @UiThread
    void setAddButtonText( String buttonText ){
        btnAdd.setText( buttonText );
    }

}