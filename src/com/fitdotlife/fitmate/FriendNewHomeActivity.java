package com.fitdotlife.fitmate;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fitdotlife.fitmate.newhome.CategoryType;
import com.fitdotlife.fitmate.newhome.DayPagerAdapter;
import com.fitdotlife.fitmate.newhome.NewHomeCategoryData;
import com.fitdotlife.fitmate.newhome.NewHomeConstant;
import com.fitdotlife.fitmate.newhome.NewHomeProgramClickListener;
import com.fitdotlife.fitmate.newhome.NewHomeUtils;
import com.fitdotlife.fitmate.newhome.WeekActivityListener;
import com.fitdotlife.fitmate.newhome.WeekPagerActivityInfo;
import com.fitdotlife.fitmate.newhome.WeekPagerAdapter;
import com.fitdotlife.fitmate_lib.customview.NewHomeCategoryView;
import com.fitdotlife.fitmate_lib.customview.NewHomeCategoryView_;
import com.fitdotlife.fitmate_lib.customview.NewHomeDayCircleView_;
import com.fitdotlife.fitmate_lib.customview.NewHomeDayHalfCircleView_;
import com.fitdotlife.fitmate_lib.customview.NewHomeDayTextView_;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.http.ActivityService;
import com.fitdotlife.fitmate_lib.http.FriendService;
import com.fitdotlife.fitmate_lib.http.NetworkClass;
import com.fitdotlife.fitmate_lib.http.UserInfoService;
import com.fitdotlife.fitmate_lib.key.RangeType;
import com.fitdotlife.fitmate_lib.object.DayAchieve;
import com.fitdotlife.fitmate_lib.object.FriendDayActivity;
import com.fitdotlife.fitmate_lib.object.FriendWeekActivity;
import com.fitdotlife.fitmate_lib.object.UserFriend;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.util.DateUtils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringArrayRes;
import org.androidannotations.annotations.rest.RestService;
import org.apache.log4j.Log;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

;

/**
 * Created by Joshua on 2015-12-22.
 */
@EActivity( R.layout.activity_friend_newhome)
public class FriendNewHomeActivity extends Activity implements WeekPagerAdapter.WeekMoveListener
{
    public static final int TODAY_AFTER_NODATA = -1;
    public static final int TODAY_BEFORE_NODATA = -2;

    private int mWeekPagerLength = 0;

    private int[] mCategoryWeekPosition = new int[ NewHomeConstant.ViewOrder.length ];

    private View calendarMain = null;
    private PopupWindow calendarPopup = null;
    private PopupWindow programPopup = null;
    private int selectdCategoryIndex = 3;

    private Calendar mDayCalendar = null;
    private Calendar mTodayCalendar = null;
    private int mTodayWeekNumber = 0;
    private Calendar mWeekCalendar = null;
    private FitmateDBManager mDBManager = null;
    private UserInfo mUserInfo = null;
    private UserFriend mUserFriend = null;

    private DayPagerAdapter mDayPagerAdapter = null;

    private WeekPagerActivityInfo[] mWeekPagerActivityInfoList = null;

    private FriendNewHomeWeekPagerAdapter mWeekBarPagerAdapter = null;
    private FriendNewHomeWeekPagerAdapter mWeekTablePagerAdapter = null;

    private NewHomeUtils newHomeUtils = null;

    @ViewById(R.id.rl_activity_friend_newhome_actionbar)
    RelativeLayout rlActionBar;

    @ViewById(R.id.tv_activity_friend_newhome_name)
    TextView tvName;

    @ViewById(R.id.tv_activity_friend_newhome_weekday)
    TextView tvWeekDay;

    @ViewById(R.id.tv_activity_friend_newhome_date)
    TextView tvDate;

    @ViewById(R.id.hsv_activity_friend_newhome_category)
    HorizontalScrollView hsvCategory;

    @ViewById(R.id.ll_activity_friend_newhome_category)
    LinearLayout llCategory;

    @StringArrayRes(R.array.newhome_category_char)
    String[] arrCategoryChar;

    @ViewById(R.id.vp_activity_friend_newhome_day)
    ViewPager vpDay;

    @ViewById(R.id.vp_activity_friend_newhome_week_bar)
    ViewPager vpWeekBar;

    @ViewById(R.id.vp_activity_friend_newhome_week_table)
    ViewPager vpWeekTable;

    @ViewById(R.id.rl_activity_friend_newhome_week)
    RelativeLayout rlWeek;

    @ViewById(R.id.rl_activity_friend_newhome_week_progress)
    RelativeLayout rlWeekProgress;

    @ViewById(R.id.img_activity_friend_newhome_profile)
    ImageView imgProfile;

    @RestService
    ActivityService activityServiceClient;

    @RestService
    UserInfoService userInfoServiceClient;

    @RestService
    FriendService friendServiceClient;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.setTheme(R.style.OrangeTheme);

        activityServiceClient.setRootUrl(NetworkClass.baseURL + "/api");
        userInfoServiceClient.setRootUrl(NetworkClass.baseURL + "/api");
        friendServiceClient.setRootUrl(NetworkClass.baseURL + "/api");

        this.mDBManager = new FitmateDBManager(this);
        this.mUserInfo = this.mDBManager.getUserInfo();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @AfterViews
    void init()
    {

        this.newHomeUtils = new NewHomeUtils(this , mDBManager , arrCategoryChar , new ProgramClickListener() );
        FriendActivityManager.getInstance().addActivity(this);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());

        mTodayCalendar = Calendar.getInstance();
        mTodayCalendar.setTimeInMillis(calendar.getTimeInMillis());
        mTodayWeekNumber = mTodayCalendar.get(Calendar.DAY_OF_WEEK);

        this.mWeekCalendar = Calendar.getInstance();
        this.mWeekCalendar.setTimeInMillis(calendar.getTimeInMillis());
        int weekNumber = mWeekCalendar.get( Calendar.DAY_OF_WEEK );
        mWeekCalendar.add(Calendar.DATE, -(weekNumber - 1));

        //주간 그래프의 크기를 결정한다.
        try {

            Calendar endCalendar = Calendar.getInstance();
            endCalendar.set( mWeekCalendar.get(Calendar.YEAR) , mWeekCalendar.get( Calendar.MONTH ) , mWeekCalendar.get(Calendar.DATE) );
            long diffDates = DateUtils.diffOfDate( endCalendar );
            mWeekPagerLength = ((int) (diffDates / 7) + 1);
        } catch (Exception e) {
            mWeekPagerLength = 1000000;
        }

        //주간 그래프의 주 정보 배열을 초기화한다.
        mWeekPagerActivityInfoList = new WeekPagerActivityInfo[ mWeekPagerLength ];


        Intent intent = this.getIntent();
        this.mUserFriend = intent.getParcelableExtra("UserFriend");

        if( mUserFriend != null )
        {

            mDayCalendar = Calendar.getInstance();
            mDayCalendar.setTimeInMillis(calendar.getTimeInMillis());

            showProgressView();
            displayFriendInfo();
            getFriendDayAcivity(true);
            int position = getWeekDiff();
            getFriendWeekActivity( position );
            //displayWeekData( true );

        }else{
            long activityDate = intent.getLongExtra( "ActivityDate" , calendar.getTimeInMillis() );

            mDayCalendar = Calendar.getInstance();
            mDayCalendar.setTimeInMillis( activityDate );

            String friendEmail = intent.getStringExtra( "FriendEmail" );
            getUserFriend(friendEmail);
        }

        setDateText();
    }

    /**
     * 프로파일 이미지를 설정한다.
     */
    private void setProfileImage( ){
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.icon_home_profile)
                .showImageOnFail(R.drawable.icon_home_profile)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .displayer(new RoundedBitmapDisplayer( 200 ))
                .build();

        ImageLoader.getInstance().displayImage(NetworkClass.imageBaseURL + mUserFriend.getProfileImagePath(), imgProfile, options);

    }

    @Background
    void getUserFriend(String friendEmail){

        if(mUserFriend == null) {
            Log.e("fitmate", "frinedEmail : " + friendEmail);
        }

        mUserFriend = friendServiceClient.getUsersFriendsInfo(mUserInfo.getEmail(), friendEmail);

        if(mUserFriend == null) {
            Log.e("fitmate", "NULL");

        }else {

            showProgressView();
            displayFriendInfo();
            getFriendDayAcivity(true);
            int position = getWeekDiff();
            getFriendWeekActivity(position );
            //displayWeekData( true );
        }
    }

    @UiThread
    void displayFriendInfo(){
        setProfileImage();
        tvName.setText(mUserFriend.getName());
    }

    @Background
    void getFriendDayAcivity( boolean initialDisplay )
    {
        //친구 일간 데이터를 가져온다.
        FriendDayActivity friendDayActivity = activityServiceClient.getFriendDayActivity(mUserInfo.getEmail(), mUserFriend.getEmail(), getDateString(mDayCalendar), false);
        displayDayData(friendDayActivity, initialDisplay);
    }

    @Background
    void getFriendWeekActivity( int position ){
        mWeekBarPagerAdapter = new FriendNewHomeWeekPagerAdapter( this, mWeekPagerLength , NewHomeConstant.ViewOrder[selectdCategoryIndex] , new WeekCellClickListener() , this , mDayCalendar.getTimeInMillis() , WeekPagerAdapter.WeekPagerType.WEEK_BAR , mWeekPagerActivityInfoList );
        mWeekPagerActivityInfoList[position]  = mWeekBarPagerAdapter.getFriendWeekActivityInfoList( position );
        mWeekPagerActivityInfoList[position-1] = mWeekBarPagerAdapter.getFriendWeekActivityInfoList( position - 1 );
        mWeekTablePagerAdapter = new FriendNewHomeWeekPagerAdapter(this , mWeekPagerLength , CategoryType.WEEKLY_ACHIEVE , new WeekCellClickListener() , this , mDayCalendar.getTimeInMillis() , WeekPagerAdapter.WeekPagerType.WEEK_TABLE , mWeekPagerActivityInfoList);

        displayWeekData(true);
    }

    @Background
    void getFriendWeekActivity( Calendar weekCalendar , CategoryType categoryType ){

        weekCalendar.add(Calendar.DATE, -(2 * 7));

        for(int i = 0 ; i < 5 ; i++) {

            String weekStartDate = getDateString( weekCalendar );
            if( !mDBManager.isExistFriendWeekActivity( weekStartDate , categoryType.ordinal() ) ){

                FriendWeekActivity friendWeekActivity = activityServiceClient.getFriendWeekActivity(mUserInfo.getEmail(), mUserFriend.getEmail(), categoryType.ordinal(), getDateString(weekCalendar), false);
                if (friendWeekActivity.getNodata().equals("false")) {
                    mDBManager.setFriendWeekActivity(weekStartDate, friendWeekActivity);
                }
            }

            weekCalendar.add(Calendar.DATE , 7);
        }
    }

    @Background
    void getFriendWeekActivity( String weekStartDate , CategoryType categoryType , WeekActivityListener listener ){

        FriendWeekActivity friendWeekActivity = activityServiceClient.getFriendWeekActivity(mUserInfo.getEmail(), mUserFriend.getEmail(), categoryType.ordinal(), weekStartDate, false);
        listener.OnWeekActivityReceived(friendWeekActivity);
    }

    @Background
    void getFriendWeekActivityList( String weekStartDate ,  WeekActivityListener listener ){

        FriendWeekActivity[] friendWeekActivityList = new FriendWeekActivity[ NewHomeConstant.ViewOrder.length ];

        for( int i = 0 ; i < NewHomeConstant.ViewOrder.length ;i++ ) {
            FriendWeekActivity friendWeekActivity = activityServiceClient.getFriendWeekActivity(mUserInfo.getEmail(), mUserFriend.getEmail(), NewHomeConstant.ViewOrder[i].ordinal() , weekStartDate, false);
            friendWeekActivityList[NewHomeConstant.ViewOrder[i].ordinal()] = friendWeekActivity;
        }

        listener.OnWeekActivityReceived(friendWeekActivityList);
    }



    @UiThread
    void displayDayData( FriendDayActivity friendDayActivity , boolean initialDisplay )
    {
        int exerprogramId = Integer.parseInt(friendDayActivity.getExprogramid());

        if(initialDisplay) {
            addDayData(friendDayActivity , exerprogramId );
        }else{
            updateDayData(friendDayActivity, exerprogramId);
        }

    }

    private void addDayData( FriendDayActivity friendDayActivity , int exerprogramId)
    {

        View[] dayGraphs = new View[ NewHomeConstant.ViewOrder.length ];

        for( int viewIndex = 0 ; viewIndex < NewHomeConstant.ViewOrder.length ;viewIndex++ )
        {
            CategoryType categoryType = NewHomeConstant.ViewOrder[viewIndex];

            NewHomeCategoryView categoryView = NewHomeCategoryView_.build(this);
            View dayView = null;
            if( categoryType.equals( CategoryType.DAILY_ACHIEVE ) ){
                dayView = NewHomeDayHalfCircleView_.build(this);
            }else if(categoryType.equals( CategoryType.WEEKLY_ACHIEVE )){
                dayView = NewHomeDayCircleView_.build(this);
            }else{
                dayView = NewHomeDayTextView_.build(this);
            }

            categoryView.setOnClickListener(new CategoryClickListener());
            newHomeUtils.setDayValue(categoryView, dayView, NewHomeCategoryData.getNewHomeCategoryData(this, friendDayActivity, mUserFriend), exerprogramId, viewIndex, selectdCategoryIndex, true);

            dayGraphs[viewIndex] = dayView;
            llCategory.addView( categoryView );
        }


        mDayPagerAdapter = new DayPagerAdapter(this , dayGraphs);
        vpDay.setAdapter(mDayPagerAdapter);
        vpDay.setCurrentItem(selectdCategoryIndex);
        vpDay.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(final int position) {
                newHomeUtils.unSelectCategoryView((NewHomeCategoryView) llCategory.getChildAt(selectdCategoryIndex));
                int preSelectedCategoryIndex = selectdCategoryIndex;
                selectdCategoryIndex = position;
                NewHomeCategoryView newHomeCategoryView = (NewHomeCategoryView) llCategory.getChildAt(selectdCategoryIndex);
                newHomeUtils.selectCategoryView(newHomeCategoryView);

                if (preSelectedCategoryIndex > selectdCategoryIndex) { //카테고리를 왼쪽에서 오른쪽으로 이동하는 경우

                    int viewLeft = newHomeCategoryView.getLeft();
                    if (hsvCategory.getScrollX() > viewLeft) {
                        hsvCategory.smoothScrollTo(viewLeft, 0);
                    }

                } else if (preSelectedCategoryIndex < selectdCategoryIndex) { //카테고리를 오른족에서 왼쪽으로 이동하는 경우

                    int viewRight = newHomeCategoryView.getRight();
                    if (viewRight > (hsvCategory.getScrollX() + hsvCategory.getRight())) {

                        hsvCategory.smoothScrollTo(viewRight - hsvCategory.getRight(), 0);
                    }
                }

//                    //일 그래프를 애니메이션 시킨다.
                CategoryType categoryType = NewHomeConstant.ViewOrder[selectdCategoryIndex];
//                    DayPagerAdapter dayPagerAdapter = (DayPagerAdapter) vpDay.getAdapter();
//                    View view = dayPagerAdapter.getDayGraphs()[selectdCategoryIndex];
//                    if( categoryType.equals( CategoryType.WEEKLY_ACHIEVE ) ){
//
//                        NewHomeDayCircleView dayCircleView = (NewHomeDayCircleView)view;
//                        dayCircleView.startAnimation( true );
//
//                    }else if( categoryType.equals( CategoryType.DAILY_ACHIEVE ) ){
//
//                        NewHomeDayHalfCircleView dayHalfCircleView = (NewHomeDayHalfCircleView) view;
//                        dayHalfCircleView.startAnimation( true );
//
//                    }else {
//                        NewHomeDayTextView dayTextView = (NewHomeDayTextView) view;
//                        dayTextView.startAnimation( true );
//                    }

                if(categoryType.equals( CategoryType.WEEKLY_ACHIEVE ))
                {
                    vpWeekBar.setVisibility( View.INVISIBLE );
                    vpWeekTable.setVisibility(View.VISIBLE);
                    vpWeekTable.setCurrentItem( vpWeekBar.getCurrentItem() );

                }else{
                    vpWeekTable.setVisibility(View.INVISIBLE);
                    vpWeekBar.setVisibility(View.VISIBLE);
                    vpWeekBar.setCurrentItem( vpWeekTable.getCurrentItem() );
                    mWeekBarPagerAdapter.changeCategoryType( categoryType );
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }


    private void updateDayData(FriendDayActivity friendDayActivity, int exerprogramId){

        for( int viewIndex = 0 ; viewIndex < NewHomeConstant.ViewOrder.length ; viewIndex++ )
        {
            CategoryType categoryType = NewHomeConstant.ViewOrder[viewIndex];

            NewHomeCategoryView categoryView = (NewHomeCategoryView) llCategory.getChildAt( viewIndex );
            View dayView = mDayPagerAdapter.getDayGraphs()[viewIndex];
            newHomeUtils.setDayValue(categoryView, dayView, NewHomeCategoryData.getNewHomeCategoryData(this, friendDayActivity, mUserFriend), exerprogramId, viewIndex, selectdCategoryIndex, true);
        }
    }

    @UiThread
    void displayWeekData( boolean initDisplay )
    {
        if( initDisplay )
        {

            vpWeekBar.addOnPageChangeListener(mWeekBarPagerAdapter);
            vpWeekBar.setAdapter(mWeekBarPagerAdapter);
            vpWeekBar.setCurrentItem(mWeekPagerLength);

            vpWeekTable.addOnPageChangeListener(mWeekTablePagerAdapter);
            vpWeekTable.setAdapter(mWeekTablePagerAdapter);
            vpWeekTable.setCurrentItem(mWeekPagerLength);

            CategoryType categoryType = NewHomeConstant.ViewOrder[ selectdCategoryIndex ];
            if( categoryType.equals( CategoryType.WEEKLY_ACHIEVE ) ){

                vpWeekBar.setVisibility( View.INVISIBLE );
                vpWeekTable.setVisibility(View.VISIBLE);

            }else{

                vpWeekBar.setVisibility( View.VISIBLE );
                vpWeekTable.setVisibility(View.INVISIBLE);
            }

        }else{

            int position = getWeekDiff();

            vpWeekBar.setCurrentItem (position + 1);
            mWeekBarPagerAdapter.setMoveWeek( false );
            mWeekBarPagerAdapter.notifyDataSetChanged();

            vpWeekTable.setCurrentItem( position + 1);
            mWeekTablePagerAdapter.setMoveWeek( false );
            mWeekTablePagerAdapter.notifyDataSetChanged();
        }

        if( rlWeekProgress.getVisibility() == View.VISIBLE ){
            dismissProgressView();
        }
    }

    private int getWeekDiff(){

        Calendar weekCalendar = Calendar.getInstance();
        weekCalendar.setTimeInMillis(mDayCalendar.getTimeInMillis());
        int weekNumber = weekCalendar.get(Calendar.DAY_OF_WEEK);
        weekCalendar.add(Calendar.DATE, -(weekNumber - 1));

        long diff = mWeekCalendar.getTimeInMillis() - weekCalendar.getTimeInMillis();
        long diffDays = diff / (24 * 60 * 60 * 1000);

        int weekDiff = (int) (diffDays / 7);

        return mWeekPagerLength - ( weekDiff + 1 );
    }


    @Click(R.id.img_activity_friend_newhome_profile)
    void profileClick(){

        Intent intent = new Intent(FriendNewHomeActivity.this, FriendProfileActivity_.class);
        intent.putExtra("UserFriend", mUserFriend);
        startActivity(intent);

    }

    @Click(R.id.ll_activity_friend_newhome_calendar)
    void calendarClick(){

        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.calendarMain = inflater.inflate(R.layout.calendar_view, null);
        this.calendarPopup = new PopupWindow( calendarMain, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT );
        this.calendarPopup.setAnimationStyle(-1);

        //팝업의 y좌표를 가져온다.
        //상단의 statusbar 아래에서 보여줘야 하기 때문에
        //홈에서 제일 상단의 뷰의 위치의 y좌표부터 팝업을 보여주는 것으로 함.
        int[] loc = new int[2];
        rlActionBar.getLocationOnScreen(loc);

        this.calendarPopup.showAtLocation( calendarMain , Gravity.CENTER , 0 , loc[1] );

        CalendarView calendarView = (CalendarView) calendarMain.findViewById( R.id.cv_activity_newhome );
        calendarView.setDate( mDayCalendar.getTimeInMillis() );
        calendarView.setMaxDate(mTodayCalendar.getTimeInMillis());
        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {

            private int preYear = mDayCalendar.get(Calendar.YEAR);
            private int preMonth = mDayCalendar.get(Calendar.MONTH);
            private int preDay = mDayCalendar.get(Calendar.DAY_OF_MONTH);

            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                if (preYear != year || preMonth != month || preDay != dayOfMonth) {
                    mDayCalendar.set(year, month, dayOfMonth);
                    changeDay();
                    calendarPopup.dismiss();
                }
            }
        });
    }

    private void setDateText(){

        String weekDay = null;
        weekDay = this.getResources().getStringArray(R.array.week_string)[ mDayCalendar.get( Calendar.DAY_OF_WEEK ) - 1 ];

        tvWeekDay.setText(weekDay);
        tvDate.setText(DateUtils.getDateStringForPattern(mDayCalendar.getTime(), this.getString(R.string.newhome_date_format)));
    }

    private String getDateString( Calendar calendar )
    {
        return calendar.get(Calendar.YEAR) + "-" + String.format("%02d",  calendar.get( Calendar.MONTH ) + 1 ) + "-" + String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH) );
    }

    private void changeDay()
    {
        showProgressView();
        setDateText();

        int selectedDayIndex = mDayCalendar.get( Calendar.DAY_OF_WEEK );
        mWeekBarPagerAdapter.setSelectedDayIndex(selectedDayIndex - 1);
        mWeekTablePagerAdapter.setSelectedDayIndex(selectedDayIndex - 1);

        getFriendDayAcivity(false);
        displayWeekData(false);
    }

    @UiThread
    void showProgressView()
    {
        Log.i("fitmate" , "show Process View");
        rlWeekProgress.setVisibility(View.VISIBLE);
    }

    @UiThread
    void dismissProgressView()
    {
        Log.i("fitmate" , "dismiss Process View");
        rlWeekProgress.setVisibility(View.INVISIBLE);
    }

    private class CategoryClickListener implements View.OnClickListener
    {
        @Override
        public void onClick( View view )
        {
            vpDay.setCurrentItem((int) view.getTag());
        }

    }

    private class WeekCellClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {

            long selectedDayTimeMillis = (long) view.getTag( R.string.newhome_weekview_time_key );
            int index = (int) view.getTag(R.string.newhome_weekview_cellindex_key);
            mDayCalendar.setTimeInMillis(selectedDayTimeMillis);

            setDateText();
            getFriendDayAcivity(false);

            mWeekBarPagerAdapter.setDayTimeMilliseconds(selectedDayTimeMillis);
            mWeekBarPagerAdapter.setSelectedDayIndex(index);
            mWeekTablePagerAdapter.setDayTimeMilliseconds(selectedDayTimeMillis );
            mWeekTablePagerAdapter.setSelectedDayIndex(index);

        }
    }

    @Override
    public void onBackPressed() {

        if( newHomeUtils.programPopup != null)
        {
            if (newHomeUtils.programPopup.isShowing()) {
                newHomeUtils.programPopup.dismiss();
                newHomeUtils.programPopup = null;
                return;
            }
        }

        if( calendarPopup != null )
        {
            if( calendarPopup.isShowing() )
            {
                calendarPopup.dismiss();
                return;
            }
        }

        super.onBackPressed();
    }

    private class ProgramClickListener implements NewHomeProgramClickListener {

        @Override
        public void programClick(int exerprogramID) {

            newHomeUtils.showProgramDialog( exerprogramID );

        }
    }

    private class WeekPagerChangeListener implements ViewPager.OnPageChangeListener
    {
        private CategoryType mCategoryType = null;

        public WeekPagerChangeListener( CategoryType categoryType ){
            this.mCategoryType = categoryType;
        }

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected( int position ) {

            mCategoryWeekPosition[selectdCategoryIndex] = position;

            int prevIndex = mWeekPagerLength - (position + 1);
            Calendar weekCalendar = Calendar.getInstance();
            weekCalendar.setTimeInMillis( mWeekCalendar.getTimeInMillis() );
            weekCalendar.add(Calendar.DATE, -(prevIndex * 7));

            getFriendWeekActivity( weekCalendar , mCategoryType );
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    }


    private class FriendNewHomeWeekPagerAdapter extends WeekPagerAdapter {

        private FriendWeekActivity mFriendWeekActivity = null;
        private FriendWeekActivity[] mFriendWeekActivityList = null;

        public FriendNewHomeWeekPagerAdapter(Context context, int weekPagerLength, CategoryType categoryType, View.OnClickListener weekCellClickListener, WeekMoveListener weekMoveListener, long dayMilliseconds, WeekPagerType weekPagerType, WeekPagerActivityInfo[] weekPagerActivityInfoList) {
            super(context, weekPagerLength, categoryType, weekCellClickListener, weekMoveListener, dayMilliseconds, weekPagerType, weekPagerActivityInfoList);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position)
        {
            View view = null;

            WeekPagerActivityInfo weekActivityInfo = null;

            if (mWeekPagerActivityInfoList[position] == null) {

                showProgressView();
                weekActivityInfo = getFriendWeekActivityInfoList(position);
                mWeekPagerActivityInfoList[position] = weekActivityInfo;


            } else {

                weekActivityInfo = mWeekPagerActivityInfoList[position];
                double[] values = weekActivityInfo.getValues(mCategoryType);
                if (values == null) {
                    double[] activityValues = this.getWeekPagerActivityValues(position);
                    weekActivityInfo.setValues(mCategoryType, activityValues);
                }
            }

            view = getWeekGraphView(weekActivityInfo , position);
            container.addView(view);

            if( rlWeekProgress.getVisibility() == View.VISIBLE ){
                dismissProgressView();
            }
            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object){

            container.removeView((View) object);

        }

        public WeekPagerActivityInfo getFriendWeekActivityInfoList(int position)
        {
            int weekIndex = mPagerLength - (position + 1);

            Calendar weekCalendar = Calendar.getInstance();
            weekCalendar.setTimeInMillis(mWeekCalendar.getTimeInMillis());
            weekCalendar.add(Calendar.DATE, -(weekIndex * 7));
            String weekStartDate = newHomeUtils.getDateString(weekCalendar);

            getFriendWeekActivityList(weekStartDate, this);
            while (true) {

                if (mDownloadData) {
                    mDownloadData = false;
                    break;
                }

                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            return getWeekActivityInfoList( mFriendWeekActivityList , weekCalendar , weekStartDate );
        }

        private WeekPagerActivityInfo getWeekActivityInfoList(FriendWeekActivity[] friendWeekActivityList , Calendar weekCalendar , String weekStartDate )
        {

            float strengthFrom = 0;
            float strengthTo =0;
            String weekRange = "";
            String[] dayStrings = new String[7];
            long[] timeMillis = new long[7];
            int selectedDayIndex = -1;
            int[] dayAchieveIndexList = new int[]{0,0,0,0,0,0,0,0};
            int preMonth = -1;
            RangeType rangeType = null;

            double[][] values = {{0,0,0,0,0,0,0} , {0,0,0,0,0,0,0} , {0,0,0,0,0,0,0} , {0,0,0,0,0,0,0} ,
                    {0,0,0,0,0,0,0} , {0,0,0,0,0,0,0} , {0,0,0,0,0,0,0} , {0,0,0,0,0,0,0} };

            for (int i = 0; i < 7 ; i++)
            {
                for( int j = 0; j < NewHomeConstant.ViewOrder.length ;j++ )
                {

                    FriendWeekActivity friendWeekActivity = friendWeekActivityList[j];

                    if (friendWeekActivity != null)
                    {
                        if( i == 0 ){

                            if( friendWeekActivity.getTargetrangeinfo() != null ){
                                strengthFrom = friendWeekActivity.getTargetrangeinfo().getRangeFrom();
                                strengthTo = friendWeekActivity.getTargetrangeinfo().getRangeTo();

                            }
                        }

                        List<DayAchieve> dayAchieveList = friendWeekActivity.getListofdayachieve();

                        if (dayAchieveList != null) {
                            if (dayAchieveList.size() > 0) {
                                //날짜를 비교한다.
                                String currentDayString = NewHomeUtils.getDateString(weekCalendar);
                                if (currentDayString.equals(dayAchieveList.get( dayAchieveIndexList[j]).getDate())) {
                                    try {
                                        values[j][i] = Double.parseDouble(dayAchieveList.get(dayAchieveIndexList[j]).getAchieve());

                                    } catch (NumberFormatException e) {
                                        values[j][i] = 0;
                                    }
                                    if (dayAchieveIndexList[j] < (dayAchieveList.size() - 1)) {
                                        dayAchieveIndexList[j]++;
                                    }
                                } else {
                                    values[j][i] = setNoValue(weekCalendar);
                                }
                            } else {
                                values[j][i] = setNoValue(weekCalendar);
                            }
                        } else {
                            values[j][i] = setNoValue(weekCalendar);
                        }

                    } else {
                        values[j][i] = setNoValue(weekCalendar);
                    }
                }

                timeMillis[i] = weekCalendar.getTimeInMillis();

                int currentMonth = weekCalendar.get(Calendar.MONTH);
                if( preMonth != currentMonth ) {
                    dayStrings[i] = DateUtils.getDateStringForPattern(weekCalendar.getTime(), getResources().getString(R.string.newhome_week_bar_date_format));
                    preMonth = currentMonth;
                }else{
                    dayStrings[i] = DateUtils.getDateStringForPattern(weekCalendar.getTime(), getResources().getString(R.string.newhome_week_bar_date_simple_format));
                }

                if( i == 0 ){
                    weekRange += DateUtils.getDateStringForPattern( weekCalendar.getTime() , getResources().getString( R.string.newhome_week_table_week_range_format ) );
                }

                if( i == values.length -1 )
                {
                    weekRange += " - " + DateUtils.getDateStringForPattern( weekCalendar.getTime() , getResources().getString( R.string.newhome_week_table_week_range_format ) );
                }

                weekCalendar.add(Calendar.DATE, 1);
            }

            WeekPagerActivityInfo weekActivityInfo = new WeekPagerActivityInfo( weekStartDate , weekRange , dayStrings , timeMillis , preMonth ,strengthFrom , strengthTo,
                    values[CategoryType.AVERAGE_MET.ordinal()],
                    values[CategoryType.EXERCISE_TIME.ordinal()],
                    values[CategoryType.CALORIES.ordinal()],
                    values[CategoryType.DAILY_ACHIEVE.ordinal()],
                    values[CategoryType.WEEKLY_ACHIEVE.ordinal()],
                    values[CategoryType.FAT.ordinal()],
                    values[CategoryType.CARBOHYDRATE.ordinal()],
                    values[CategoryType.DISTANCE.ordinal()]);

            return weekActivityInfo;
        }


//        private WeekPagerActivityInfo getWeekActivityInfo(FriendWeekActivity friendWeekActivity , Calendar weekCalendar , String weekStartDate , CategoryType categoryType ){
//
//            String weekRange = "";
//            double[] values = new double[]{0,0,0,0,0,0,0};
//            String[] dayStrings = new String[7];
//            long[] timeMillis = new long[7];
//            int selectedDayIndex = -1;
//            int dayAchieveIndex = 0;
//            int preMonth = -1;
//
//            List<DayAchieve> dayAchieveList = null;
//            if( friendWeekActivity != null ) {
//                dayAchieveList = friendWeekActivity.getListofdayachieve();
//            }
//
//            for (int i = 0; i < values.length; i++)
//            {
//                for( int j = 0; j < NewHomeConstant.ViewOrder.length ;j++ ) {
//
//
//                    if (friendWeekActivity != null) {
//                        if (dayAchieveList != null) {
//                            if (dayAchieveList.size() > 0) {
//                                //날짜를 비교한다.
//                                String currentDayString = NewHomeUtils.getDateString(weekCalendar);
//                                if (currentDayString.equals(dayAchieveList.get(dayAchieveIndex).getDate())) {
//                                    try {
//                                        values[i] = Double.parseDouble(dayAchieveList.get(dayAchieveIndex).getAchieve());
//
//                                    } catch (NumberFormatException e) {
//                                        values[i] = 0;
//                                    }
//                                    if (dayAchieveIndex < (dayAchieveList.size() - 1)) {
//                                        dayAchieveIndex++;
//                                    }
//                                } else {
//                                    this.setNoValue(weekCalendar);
//                                }
//                            } else {
//                                this.setNoValue(weekCalendar);
//                            }
//                        } else {
//                            this.setNoValue(weekCalendar);
//                        }
//
//                    } else {
//                        this.setNoValue(weekCalendar);
//                    }
//
//                }
//
//                timeMillis[i] = weekCalendar.getTimeInMillis();
//
//                int currentMonth = weekCalendar.get(Calendar.MONTH);
//                if( preMonth != currentMonth ) {
//                    dayStrings[i] = DateUtils.getDateStringForPattern(weekCalendar.getTime(), getResources().getString(R.string.newhome_week_bar_date_format));
//                    preMonth = currentMonth;
//                }else{
//                    dayStrings[i] = DateUtils.getDateStringForPattern(weekCalendar.getTime(), getResources().getString(R.string.newhome_week_bar_date_simple_format));
//                }
//
//                if( i == 0 ){
//                    weekRange += DateUtils.getDateStringForPattern( weekCalendar.getTime() , getResources().getString( R.string.newhome_week_table_week_range_format ) );
//                }
//
//                if( i == values.length -1 )
//                {
//                    weekRange += " - " + DateUtils.getDateStringForPattern( weekCalendar.getTime() , getResources().getString( R.string.newhome_week_table_week_range_format ) );
//                }
//
//                weekCalendar.add(Calendar.DATE, 1);
//            }
//
//            WeekPagerActivityInfo weekActivityInfo = new WeekPagerActivityInfo( weekStartDate , weekRange , dayStrings , timeMillis , preMonth);
//            weekActivityInfo.setValues( categoryType , values );
//
//            float strengthFrom = 0;
//            float strengthTo =0;
//            if(friendWeekActivity != null)
//            {
//                if( friendWeekActivity.getTargetrangeinfo() != null ){
//                    strengthFrom = friendWeekActivity.getTargetrangeinfo().getRangeFrom();
//                    weekActivityInfo.setStrengthFrom( strengthFrom );
//                }
//
//                if(friendWeekActivity.getTargetrangeinfo() != null) {
//                    strengthTo = friendWeekActivity.getTargetrangeinfo().getRangeTo();
//                    weekActivityInfo.setStrengthTo(strengthTo);
//                }
//            }
//
//            return weekActivityInfo;
//        }


        private double[] getWeekPagerActivityValues(int position)
        {
            int weekIndex = mPagerLength - (position + 1);

            Calendar weekCalendar = Calendar.getInstance();
            weekCalendar.setTimeInMillis(mWeekCalendar.getTimeInMillis());
            weekCalendar.add(Calendar.DATE, -(weekIndex * 7));
            String weekStartDate = newHomeUtils.getDateString(weekCalendar);

            getFriendWeekActivity( weekStartDate, mCategoryType, this );
            while (true) {

                if (mDownloadData) {
                    mDownloadData = false;
                    break;
                }

                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }

            FriendWeekActivity friendWeekActivity = mFriendWeekActivity;

            double[] values = new double[]{0,0,0,0,0,0,0};
            int dayAchieveIndex = 0;

            List<DayAchieve> dayAchieveList = null;
            if( friendWeekActivity != null ) {
                dayAchieveList = friendWeekActivity.getListofdayachieve();
            }

            for (int i = 0; i < values.length; i++)
            {
                if( friendWeekActivity != null )
                {
                    if(dayAchieveList != null)
                    {
                        if (dayAchieveList.size() > 0) {
                            //날짜를 비교한다.
                            String currentDayString = getDateString(weekCalendar);
                            if ( currentDayString.equals( dayAchieveList.get(dayAchieveIndex).getDate()) )
                            {
                                try {
                                    values[i] = Double.parseDouble(dayAchieveList.get(dayAchieveIndex).getAchieve());

                                }catch(NumberFormatException e){
                                    values[i] = 0;
                                }
                                if (dayAchieveIndex < (dayAchieveList.size() - 1)) {
                                    dayAchieveIndex++;
                                }

                            }
                            else{

                                this.setNoValue(weekCalendar);
                            }
                        }
                        else{
                            this.setNoValue(weekCalendar);
                        }
                    }else{
                        this.setNoValue(weekCalendar);
                    }

                }else{
                    this.setNoValue(weekCalendar);
                }

                weekCalendar.add(Calendar.DATE, 1);
            }

            return values;
        }

        @Override
        protected void getWeekActivityInfo(int position) {

            WeekPagerActivityInfo weekActivityInfo = null;
            if (mWeekPagerActivityInfoList[position] == null) {

                mWeekPagerActivityInfoList[position] = getFriendWeekActivityInfoList(position);

            } else {

                weekActivityInfo = mWeekPagerActivityInfoList[position];
                double[] values = weekActivityInfo.getValues(mCategoryType);
                if (values == null) {
                    double[] activityValues = this.getWeekPagerActivityValues(position);
                    weekActivityInfo.setValues(mCategoryType, activityValues);
                }
            }
        }

        @Override
        public void OnWeekActivityReceived(FriendWeekActivity friendWeekActivity) {
            mFriendWeekActivity = friendWeekActivity;
            mDownloadData = true;
        }

        @Override
        public void OnWeekActivityReceived(FriendWeekActivity[] friendWeekActivityList) {
            mFriendWeekActivityList = friendWeekActivityList;
            mDownloadData = true;
        }

        private double setNoValue( Calendar weekCalendar)
        {
            double value = 0;
            boolean todayAfter = DateUtils.isAfterDay(weekCalendar.getTime(), new Date());
            if (todayAfter) { //오늘이후 시간이라면 데이터를 없앤다.
                value = TODAY_AFTER_NODATA;
            } else {            //오늘이전 시간이라면 회색으로 보이도록 한다.
                value = TODAY_BEFORE_NODATA;
            }

            return value;
        }
    }

    @UiThread
    void moveWeek( long dayMilliseconds , int position , WeekPagerAdapter.WeekPagerType weekPagerType ){

        //주간 그래프에 따라 일 날짜를 변경시킨다.
        mDayCalendar.setTimeInMillis(dayMilliseconds);

        setDateText();
        //일 그래프를 다시 그린다.
        getFriendDayAcivity(false);

//        if( weekPagerType.equals(WeekPagerAdapter.WeekPagerType.WEEK_BAR ) ){
//
//            vpWeekTable.setCurrentItem( position , false );
//            mWeekTablePagerAdapter.moveWeek( dayMilliseconds, position );
//
//        }else if( weekPagerType.equals(WeekPagerAdapter.WeekPagerType.WEEK_TABLE ) ){
//
//            vpWeekBar.setCurrentItem( position , false);
//            mWeekBarPagerAdapter.moveWeek(dayMilliseconds , position );
//        }

//        dismissProgressView();

    }

    @Override
    public void onWeekMove( long dayMilliseconds , int position , WeekPagerAdapter.WeekPagerType weekPagerType ) {

        moveWeek(dayMilliseconds, position, weekPagerType);

    }

}
