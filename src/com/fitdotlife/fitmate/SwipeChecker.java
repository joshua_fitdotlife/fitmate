package com.fitdotlife.fitmate;

import android.content.Context;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import org.apache.log4j.Log;

/**
 * Created by Joshua on 2015-10-16.
 */
public class SwipeChecker implements View.OnTouchListener{

    private final GestureDetector gestureDetector;
    private View mView;
    private OnSwipeTouchLisener mListener;

    public SwipeChecker(Context context, View view) {
        this.mView = view;
        this.mView.setOnTouchListener(this);
        gestureDetector = new GestureDetector(context, new GestureListener());
    }

    public void setOnSwipeTouchListener( OnSwipeTouchLisener listener ){
        this.mListener = listener;
    }


    public boolean onTouch(View view, MotionEvent event) {
        return gestureDetector.onTouchEvent(event);
    }

    private final class GestureListener extends GestureDetector.SimpleOnGestureListener {

        private static final int SWIPE_DISTANCE_THRESHOLD = 60;
        private static final int SWIPE_VELOCITY_THRESHOLD = 100;

        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            float distanceX = e2.getX() - e1.getX();
            float distanceY = e2.getY() - e1.getY();
            if (Math.abs(distanceX) > Math.abs(distanceY) && Math.abs(distanceX) > SWIPE_DISTANCE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                if (distanceX > 0) {
                    Log.e("fitmate", "Right");
                    mListener.onSwipeRight(mView);
                }
                else {
                    Log.e("fitmate", "Left");
                    mListener.onSwipeLeft(mView);
                }
                return false;
            }
            return false;
        }
    }
}
