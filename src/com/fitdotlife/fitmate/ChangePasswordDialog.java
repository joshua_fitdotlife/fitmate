package com.fitdotlife.fitmate;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.http.NetworkClass;
import com.fitdotlife.fitmate_lib.key.CommonKey;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.presenter.FindPasswordPresenter;
import com.fitdotlife.fitmate_lib.util.Utils;
import com.fitdotlife.fitmate.model.UserInfoModel;
import com.fitdotlife.fitmate.model.UserInfoModelResultListener;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.apache.log4j.Log;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class ChangePasswordDialog implements OnClickListener
{
	private Button btnSearch = null;
	private Button btnCancel = null;
	private ImageView imgClose = null;
	private EditText etxSearchContent = null;
	private EditText etxNewPassword = null;
	private EditText etxNewPasswordMore = null;
	private Context mContext = null;
	private FindPasswordPresenter mPresenter = null;
	private Dialog mDialog = null;

	String email = "";

	FitmateDBManager dbManager = null;

	final static String TAG = " ChangePasswordDiaglog";

	public ChangePasswordDialog(Context context, String email)
	{
		this.mContext = context;
		this.email = email;

		this.mModel = new UserInfoModel(mContext, new UserInfoModelResultListener() {
			@Override
			public void onSuccess(int code) {
				mtvCurrentpasswordError.setVisibility(View.INVISIBLE);
			}

			@Override
			public void onSuccess(int code, boolean result) {

			}

			@Override
			public void onSuccess(int code, UserInfo userInfo)
			{
				Log.d(TAG, "서버 로그인 성공");
				if( code == UserInfoModel.LOGIN_RESPONSE_CODE ) {
					//로컬 DB에 서버의 사용자 정보를 저장한다.

				}

				mtvCurrentpasswordError.setVisibility(View.INVISIBLE);
			}

			@Override
			public void onFail(int code) {
				mtvCurrentpasswordError.setVisibility(View.VISIBLE);
			}

			@Override
			public void onErrorOccured(int code, String message) {
				mtvCurrentpasswordError.setVisibility(View.VISIBLE);
			}

		});

		dbManager = new FitmateDBManager(mContext);
		//this.mPresenter = new FindPasswordPresenter( this.mContext , this );
	}

	TextWatcher tw = new TextWatcher() {
		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {

		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {

		}

		@Override
		public void afterTextChanged(Editable s) {

			if(etxNewPassword.getText().toString().equals(etxNewPasswordMore.getText().toString()))
			{
				mtvNewpasswordError.setVisibility(View.INVISIBLE);
			}
			else
			{
				mtvNewpasswordError.setVisibility(View.VISIBLE);
			}
		}
	};

	TextView mtvNewpasswordError = null;
	TextView mtvCurrentpasswordError = null;

	private UserInfoModel mModel = null;
	public void show(){
		this.mDialog = new Dialog(this.mContext , R.style.CustomDialog);
		this.mDialog.setContentView( R.layout.dialog_change_password );

		this.etxSearchContent = (EditText) mDialog.findViewById(R.id.etx_dialog_search_password_search_content);
		this.etxNewPassword = (EditText) mDialog.findViewById(R.id.editText);
		this.etxNewPasswordMore = (EditText) mDialog.findViewById(R.id.editText2);



		etxSearchContent.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if( !hasFocus ){
					//todo focusout 되면 비밀번호 확인하기 구현 필요

					String mEncodePassword = Utils.encodePassword( etxSearchContent.getText().toString() );
					mModel.serverLogin( email , mEncodePassword );

				}
			}
		});

		etxNewPassword.addTextChangedListener(tw);
		etxNewPasswordMore.addTextChangedListener(tw);

		mtvNewpasswordError = (TextView)mDialog.findViewById(R.id.textView7);
		mtvNewpasswordError.setVisibility(View.INVISIBLE);

		mtvCurrentpasswordError = (TextView)mDialog.findViewById(R.id.textView5);
		mtvCurrentpasswordError.setVisibility(View.INVISIBLE);

		this.btnSearch = (Button) mDialog.findViewById(R.id.btn_search_email);
		this.btnSearch.setOnClickListener(this);

		this.btnCancel = (Button) mDialog.findViewById(R.id.btn_search_email_dialog_close);
		this.btnCancel.setOnClickListener(this);

		this.imgClose = (ImageView) mDialog.findViewById(R.id.img_search_email_dialog_close);
		this.imgClose.setOnClickListener(this);

		this.mDialog.show();
	}

	public static final MediaType JSON
			= MediaType.parse("application/json; charset=utf-8");

	OkHttpClient client = new OkHttpClient();

	String post(String url, String json) throws IOException {
		client.setConnectTimeout(10, TimeUnit.SECONDS);
		client.setWriteTimeout(10, TimeUnit.SECONDS);
		client.setReadTimeout(10, TimeUnit.SECONDS);

		RequestBody body = RequestBody.create(JSON, json);
		Request request = new Request.Builder()
				.url(url)
				.post(body)
				.build();
		client.newCall(request).enqueue(new Callback() {
			@Override
			public void onFailure(Request request, IOException e) {
				//ShowMessage("변경 실패 : " + e.getMessage());
				ConfirmBox(
						BoxType.Retry,
						mContext.getResources().getString(R.string.myprofile_dialog_change_title),
						mContext.getResources().getString(R.string.myprofile_dialog_change_fail) + " : " + mContext.getResources().getString(R.string.myprofile_dialog_change_fail_internet) ,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {

							}
						},
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								CloseDialog();
							}
						});

				stopProgressDialog();
			}

			@Override
			public void onResponse(final Response response) throws IOException {

				final String str = response.message();
				// 내용
				try {
					String sss = response.body().string();

					JSONObject jsonObject = new JSONObject( sss );
					String result = jsonObject.getString(CommonKey.RESPONSE_VALUE_KEY);

					if( result.equals( CommonKey.SUCCESS ) ){
						//네트워크 버전

						//ShowMessage("변경을 성공하였습니다.");

						ConfirmBox(
								BoxType.Ok,
								mContext.getResources().getString(R.string.myprofile_dialog_change_title),
								mContext.getResources().getString(R.string.myprofile_dialog_change_success),
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {

									}
								},
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										CloseDialog();
									}
								});

						// todo 로컬 디비 내용 변경시키는거 구현 필요
						//mModel.setPassword(email, mEncodePw)
						dbManager.setPassword(email, mEncodePw);
						CloseDialog();

					}else if(result.equals(CommonKey.FAIL)){

						String reason  = jsonObject.getString(CommonKey.COMMENT_VALUE_KEY);

						throw new Exception(reason);
					}
				} catch (Exception e) {
					//ShowMessage("변경 실패 : " + e.getMessage());

					ConfirmBox(
							BoxType.Retry,
							mContext.getResources().getString(R.string.myprofile_dialog_change_title),
							mContext.getResources().getString(R.string.myprofile_dialog_change_fail) + " : " + e.getMessage(),
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {

								}
							},
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									CloseDialog();
								}
							});
					e.printStackTrace();
				}

				stopProgressDialog();
			}
		});

		return "";
	}

	void CloseDialog()
	{
		mDialog.dismiss();
	}

	void ShowMessage(final String str)
	{
		Handler mHandler = new Handler(Looper.getMainLooper());
		mHandler.postDelayed(new Runnable() {
			@Override
			public void run() {
				// 내용
				try {
					Toast.makeText(mContext, str, Toast.LENGTH_SHORT).show();
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		}, 0);
	}

	private void setAutoLogin( boolean autoLogin ){
		SharedPreferences pref = mContext.getSharedPreferences("fitmateservice", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = pref.edit();
		editor.putBoolean(UserInfo.AUTO_LOGIN_KEY, autoLogin);
		editor.commit();
	}

	String mEncodePw = "";


	enum BoxType{
		YesNo,
		Retry,
		Ok
	}

	void ConfirmBox(final BoxType bt,final String title, final String content, final DialogInterface.OnClickListener okCallback, final DialogInterface.OnClickListener cancleCallback)
	{

		Handler mHandler = new Handler(Looper.getMainLooper());
		mHandler.postDelayed(new Runnable() {
			@Override
			public void run() {
				// 내용



		switch (bt)
		{
			case YesNo:

				new AlertDialog.Builder(mContext)
						.setTitle(title)
						.setMessage(content)
						.setIcon(android.R.drawable.ic_dialog_alert)
						.setPositiveButton(android.R.string.yes, okCallback).show();

				break;
			case Retry:

				new AlertDialog.Builder(mContext)
						.setTitle(title)
						.setMessage(content)
						.setIcon(android.R.drawable.ic_dialog_alert)
						.setPositiveButton(R.string.myprofile_dialog_button_retry, okCallback)
						.setNegativeButton(R.string.myprofile_dialog_button_cancel, cancleCallback).show();


				break;
			case Ok:

				new AlertDialog.Builder(mContext)
						.setTitle(title)
						.setMessage(content)
						.setIcon(android.R.drawable.ic_dialog_alert)
						.setPositiveButton(android.R.string.yes, okCallback).show();
				break;
		}
			}
		}, 0);

	}

	@Override
	public void onClick(View view)
	{
		switch( view.getId() )
		{
		case R.id.btn_search_email:
			if( this.validInput() )
			{
				//this.mPresenter.findUser( this.etxSearchContent.getText().toString() );

				startProgressDialog();

				String currentPw = Utils.encodePassword(etxSearchContent.getText().toString());
				String newPw = Utils.encodePassword(etxNewPassword.getText().toString());
				mEncodePw = newPw;

				try {
					post(NetworkClass.baseURL + "ChangePassword.aspx?" + String.format("email=%s&pw=%s&newpw=%s", email, currentPw, newPw), String.format("{\n" +
							"  \"email\":\"%s\",\n" +
							"  \"pw\":\"%s\",\n" +
							"  \"newpw\":\"%s\"\n" +
							"}", email, currentPw, newPw));
				} catch (IOException e) {

					ConfirmBox(
							BoxType.Retry,
							mContext.getResources().getString(R.string.myprofile_dialog_change_title),
							mContext.getResources().getString(R.string.myprofile_dialog_change_fail) + " : " + mContext.getResources().getString(R.string.myprofile_dialog_change_fail_internet),
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {

								}
							},
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									CloseDialog();
								}
							});
					e.printStackTrace();
				}

			}
			break;
		case R.id.btn_search_email_dialog_close:
			this.mDialog.dismiss();
			break;
		case R.id.img_search_email_dialog_close:
			this.mDialog.dismiss();
			break;
		}
	}

	private boolean validInput()
	{
		if( this.etxSearchContent.getText().length() < 4 )
		{
			this.showMessage(this.mContext.getString(R.string.myprofile_change_password_length));
			this.etxSearchContent.requestFocus();
			return false;
		}

		if( this.etxNewPassword.getText().length() < 4 )
		{
			this.showMessage(this.mContext.getString(R.string.myprofile_change_password_length));
			this.etxNewPassword.requestFocus();
			return false;
		}


		if(!etxNewPassword.getText().toString().equals(etxNewPasswordMore.getText().toString()))
		{
			ConfirmBox(
					BoxType.Retry,
					mContext.getResources().getString(R.string.myprofile_dialog_change_title),
					mContext.getResources().getString(R.string.myprofile_dialog_change_fail) + " : " + mContext.getResources().getString(R.string.myprofile_dialog_change_newpassworng),
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {

						}
					},
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							CloseDialog();
						}
					});

			//this.showMessage(this.mContext.getString(R.string.activity_login_change_require_new));
			etxNewPassword.setText("");
			etxNewPasswordMore.setText("");
			etxNewPassword.requestFocus();
			return false;
		}

		return true;
	}


	private ProgressDialog mProcessDialog = null;

	//@Override
	public void startProgressDialog(){
		this.mProcessDialog =  new ProgressDialog(this.mContext);
		mProcessDialog.setTitle("비밀번호 전송");
		mProcessDialog.setMessage("비밀번호 전송중입니다...");
		mProcessDialog.setIndeterminate( true );
		mProcessDialog.setCancelable( false );
		mProcessDialog.show();
	}

	//@Override
	public void stopProgressDialog(){
		if( this.mProcessDialog != null ) {
			this.mProcessDialog.dismiss();
		}
	}

	//@Override
	public void showResult(String message)
	{
		this.showMessage(message);
	}
	
	private void showMessage( String message )
	{
		Toast.makeText(this.mContext, message, Toast.LENGTH_LONG).show();
	}
	
	
}
