package com.fitdotlife.fitmate;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.app.Activity;

import com.fitdotlife.fitmate_lib.customview.DotNavigationView;
import com.fitdotlife.fitmate_lib.customview.RoundedImageView;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.image.ImageUtil;
import com.fitdotlife.fitmate_lib.iview.ISetUserInfoView;
import com.fitdotlife.fitmate_lib.util.Utils;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.presenter.SetUserInfoPresenter;
import com.soundcloud.android.crop.Crop;

import org.apache.log4j.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class SetAccountActivity extends Activity implements OnClickListener , ISetUserInfoView, View.OnFocusChangeListener, CompoundButton.OnCheckedChangeListener {
    private String TAG = "fitmate";

    protected static final int CAMERA_REQUEST = 2;

    private UserInfo mUserInfo = null;

    private LayoutInflater inflater = null;
    private ScrollView llScroll = null;
    private LinearLayout llContainer = null;
    private LinearLayout llContainerMatch = null;
    private DotNavigationView dotNavigationView;

    private ImageView imgClose = null;
    private ImageView imgBack = null;
    private Button btnNext = null;

    //회원정보
    private EditText etxNickName = null;
    private EditText etxEmail = null;
    private EditText etxPassword = null;
    private EditText etxPasswordConfirm = null;
    private TextView tvServiceAgreement = null;
    private boolean isServiceAgree = false;
    private boolean mCheckDuplicateEmail = false;

    //사진과 자기소개
    private RoundedImageView imgAccountPhoto = null;
    private EditText etxInroduction = null;

    private SetUserInfoPresenter mPresenter = null;

    private int mStep = -1;
    private View[] mChildViewList = null;

    private InputMethodManager inputMethodManager = null;
    private ProgressDialog mDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_set_account);

        this.mPresenter = new SetUserInfoPresenter( this , this );
        FitmateDBManager mDBManager = new FitmateDBManager( this.getApplicationContext() );
        this.inputMethodManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);

        this.inflater =  this.getLayoutInflater();
        this.llScroll = (ScrollView) this.findViewById(R.id.scr_activity_set_account_scroll );
        this.llContainer = (LinearLayout) this.findViewById(R.id.ll_activity_set_account_content );
        this.llContainerMatch = (LinearLayout) this.findViewById(R.id.ll_activity_set_account_container_match );
        this.dotNavigationView = (DotNavigationView) this.findViewById(R.id.dnv_child_activity_set_account_navigation);

        this.btnNext = (Button) this.findViewById(R.id.btn_set_account_next );
        this.btnNext.setOnClickListener(this);

        this.imgClose = (ImageView) this.findViewById(R.id.img_activity_set_account_close );
        this.imgClose.setOnClickListener(this);
        this.imgBack = (ImageView) this.findViewById(R.id.img_activity_set_account_back );
        this.imgBack.setOnClickListener(this);

        this.mUserInfo = new UserInfo();

        this.mChildViewList = new View[2];
        this.mChildViewList[0] = this.inflater.inflate( R.layout.child_set_account_step_1 , this.llContainer, false );

        // 서비스 제공 동의 관련
        final ImageView ivCheck = (ImageView)this.mChildViewList[0].findViewById(R.id.service_checkbox_iv);
        this.mChildViewList[0].findViewById(R.id.service_checkbox_iv).setOnClickListener(new OnClickListener() {
            boolean checked = false;
            @Override
            public void onClick(View v) {

                if(!checked)
                {
                    checked = true;
                    isServiceAgree = true;
                    ivCheck.setImageResource(R.drawable.checkbox_sel);
                }
                else
                {
                    checked = false;
                    isServiceAgree = false;
                    ivCheck.setImageResource(R.drawable.checkbox);
                }
            }
        });
        //service_check

        etxNickName = (EditText) this.mChildViewList[0].findViewById(R.id.etx_child_set_account_nickname);
        etxEmail = (EditText) this.mChildViewList[0].findViewById(R.id.etx_child_set_account_email);
        etxEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                mCheckDuplicateEmail = false;
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        etxEmail.setOnFocusChangeListener(this);

        etxPassword = (EditText) this.mChildViewList[0].findViewById(R.id.etx_child_set_account_password);
        etxPasswordConfirm = (EditText) this.mChildViewList[0].findViewById(R.id.etx_child_set_account_password_confirm);
        this.tvServiceAgreement = (TextView) this.mChildViewList[0].findViewById(R.id.tv_child_set_account_service_agreement);
        this.tvServiceAgreement.setText(Html.fromHtml(this.getString(R.string.signin_account_agree_term)));
        this.tvServiceAgreement.setOnClickListener(this);

        this.mChildViewList[1] = this.inflater.inflate( R.layout.child_set_account_step_2 , this.llContainer, false );

        this.imgAccountPhoto = (RoundedImageView)this.mChildViewList[1].findViewById(R.id.img_child_set_account_photo);

        final Context mContext = this;

        this.imgAccountPhoto.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(SetAccountActivity.this, ImageUtil.class);
                startActivityForResult(i, CAMERA_REQUEST);
            }
        });

        this.etxInroduction = (EditText) this.mChildViewList[1].findViewById(R.id.etx_child_set_account_introduction);


        final Activity ctx = this;
        this.moveFrontView();

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

    }

    public static class TimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            // Do something with the time chosen by the user
        }
    }


    private void moveFrontView()
    {
        this.mStep += 1;
        drawChildView();
    }

    private void moveBackView()
    {
        this.mStep -= 1;
        drawChildView();
    }

    private void drawChildView()
    {
        if( mStep == -1 ){
            this.finish();
            return;
        }

        this.llContainerMatch.removeAllViews();
        this.llContainer.removeAllViews();

        this.dotNavigationView.setDotIndex( mStep + 1 );
        this.llContainer.addView(this.mChildViewList[mStep]);

        this.llScroll.scrollTo(0, 0);
        this.llContainer.scrollTo(0, 0);
        this.llContainerMatch.scrollTo(0, 0);


        if( this.mStep == 0 )
        {
            this.imgBack.setVisibility(View.INVISIBLE);
            this.inputMethodManager.hideSoftInputFromWindow(this.etxNickName.getWindowToken() , 0);
        }
        else
        {
            this.imgBack.setVisibility(View.VISIBLE);
        }

        if( mStep == 1 ){
            this.inputMethodManager.hideSoftInputFromWindow(this.etxInroduction.getWindowToken() , 0);
            this.btnNext.setText(this.getString(R.string.common_finish).toUpperCase());
        }

    }

    String selectedImagePath;

    @Override
    public void onClick(View view)
    {
        switch( view.getId() )
        {
            case R.id.btn_set_account_next:
                if( this.validCheckThenSave() )
                {
                    if( mStep == 1 )
                    {
                        mUserInfo.setAccountPhoto(selectedImagePath);

                        this.mPresenter.setUserAccount(mUserInfo);
                        return;
                    }

                    this.moveFrontView();
                }
                break;
            case R.id.img_activity_set_account_close:
                this.finish();
                break;
            case R.id.img_activity_set_account_back:
                this.moveBackView();
                break;
            case R.id.tv_child_set_account_service_agreement:
                Intent serviceAgreementIntent = new Intent( SetAccountActivity.this , ServiceAgreementActivity.class );
                this.startActivity( serviceAgreementIntent );
                break;
        }
    }

    private boolean validCheckThenSave()
    {
        if( mStep == 0 ){

            if( this.etxNickName.getText().length() == 0 )
            {
                Toast.makeText(this, this.getResources().getString(  R.string.signin_nickname_input ) , Toast.LENGTH_LONG ).show();
                this.etxNickName.requestFocus();
                return false;
            }

            if( this.etxEmail.getText().length() == 0 )
            {
                Toast.makeText(this, this.getResources().getString(  R.string.signin_email_input ) , Toast.LENGTH_LONG ).show();
                this.etxEmail.requestFocus();
                return false;
            }

            String emailPattern ="^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
            String email = this.etxEmail.getText().toString();
            if( !email.matches(emailPattern) ) {
                Toast.makeText(this, this.getString(R.string.signin_email_different_formats), Toast.LENGTH_LONG).show();
                this.etxEmail.requestFocus();
                return false;
            }

            if( !this.mCheckDuplicateEmail ){
                mPresenter.ismUsableEmail( this.etxEmail.getText().toString() );
                return false;
            }

            if( this.etxPassword.getText().length() == 0 ){
                Toast.makeText(this, this.getResources().getString(  R.string.signin_account_input_password ) , Toast.LENGTH_LONG ).show();
                this.etxPassword.requestFocus();
                return false;
            }

            if( this.etxPassword.getText().length() < 4 ){
                Toast.makeText(this, R.string.signin_account_input_password_fourcharacter , Toast.LENGTH_LONG ).show();
                this.etxPassword.requestFocus();
                return false;
            }

            if( this.etxPasswordConfirm.getText().length() == 0 ){
                Toast.makeText(this, this.getResources().getString(  R.string.signin_account_input_password_confirm ) , Toast.LENGTH_LONG ).show();
                this.etxPasswordConfirm.requestFocus();
                return false;
            }

            if( !this.etxPassword.getText().toString().equals( this.etxPasswordConfirm.getText().toString() ) ){
                Toast.makeText(this, R.string.signin_account_different_password , Toast.LENGTH_LONG ).show();
                this.etxPasswordConfirm.requestFocus();
                return false;
            }

            if(!isServiceAgree)
            {
                Toast.makeText(this, R.string.signin_account_let_agree_term , Toast.LENGTH_LONG ).show();
                return false;
            }

            this.mUserInfo.setName( this.etxNickName.getText().toString() );
            this.mUserInfo.setEmail( this.etxEmail.getText().toString() );
            this.mUserInfo.setPassword(Utils.encodePassword( this.etxPassword.getText().toString() ) );

        }else if( mStep == 1 ){

            String intro = "";
            if( this.etxInroduction.getText().length() > 0 ){
                intro = this.etxInroduction.getText().toString();
            }
            this.mUserInfo.setIntro( intro );

        }

        return true;
    }


    @Override
    public void moveHomeActivity() {
        this.finish();

        Intent intent = new Intent(SetAccountActivity.this, NewHomeAsActivity_.class);
        this.startActivity(intent);

        Intent helpIntent = new Intent( SetAccountActivity.this , HelpActivity.class );
        helpIntent.putExtra( HelpActivity.MODE_KEY, HelpActivity.HOME_HELP_MODE );
        helpIntent.putExtra(HelpActivity.VISIBLE_KEY, true);
        this.startActivity(helpIntent);
    }

    @Override
    public void showResult(String message) {
        Toast.makeText(this,message,Toast.LENGTH_LONG).show();
    }

    @Override
    public void finishActivity() {
        this.finish();
    }

    private Date getDate(String dateString)
    {
        Date date = null;
        SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat( "yyyyMMdd", Locale.getDefault() );
        try
        {
            date =  mSimpleDateFormat.parse(dateString);
        }
        catch (ParseException e) {}

        return date;
    }

    private String getDateString( Date date ){
        String dateString = null;
        SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat( "yyyyMMdd", Locale.getDefault() );
        dateString =  mSimpleDateFormat.format(date);
        return dateString;
    }

    public void startProgressDialog( String title , String message ){
        this.mDialog =  new ProgressDialog(this);
        mDialog.setTitle(title);
        mDialog.setMessage( message );
        mDialog.setIndeterminate( true );
        mDialog.setCancelable( false );
        mDialog.show();
    }

    public void stopProgressDialog(){
        if( this.mDialog != null ) {
            this.mDialog.dismiss();
        }
    }

    @Override
    public void setDuplicateResult(boolean duplicateResult) {
        this.mCheckDuplicateEmail = duplicateResult;
    }

    private void showDialog( String message ){
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);

        dialog.setTitle("")
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton( this.getString(R.string.signin_device_research) , new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //startScan();
                        mStep -= 1;
                        btnNext.setEnabled(true);
                        btnNext.setBackgroundColor(getResources().getColor(R.color.fitmate_green));
                        drawChildView();
                        dialog.dismiss();
                    }
                })
                .show();
    }

    @Override
    public void onFocusChange(View view, boolean hasFocus) {

        if( view.getId() == R.id.etx_child_set_account_email  ){
            if( !hasFocus ){

                String emailPattern ="^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
                String email = this.etxEmail.getText().toString();
                if( !email.matches(emailPattern) ) {
                    Toast.makeText(this, this.getString(R.string.signin_email_different_formats) , Toast.LENGTH_LONG).show();
                    //this.etxEmail.requestFocus();
                    return;
                }

                this.mPresenter.ismUsableEmail(this.etxEmail.getText().toString());

            }
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode != Crop.REQUEST_CROP) {
            if (requestCode == CAMERA_REQUEST) {

                Log.i(TAG, "Image selected");
                Uri uri = (Uri) data.getExtras().get(ImageUtil.PICTURE_URI);
                imgAccountPhoto.setImageBitmap(uri);
                selectedImagePath = uri.toString();
                Log.i(TAG, "Image saved");
            }
        }

    }

    @Override
    public void onBackPressed() {
        this.moveBackView();
    }
}
