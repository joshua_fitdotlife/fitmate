package com.fitdotlife.fitmate;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.fitdotlife.fitmate.adapter.NewsAdapter;
import com.fitdotlife.fitmate_lib.customview.DrawerView;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.http.NetworkClass;
import com.fitdotlife.fitmate_lib.http.NewsService;
import com.fitdotlife.fitmate_lib.http.RestHttpErrorHandler;
import com.fitdotlife.fitmate_lib.key.CommonKey;
import com.fitdotlife.fitmate_lib.key.ExerciseProgramType;
import com.fitdotlife.fitmate_lib.key.NewsType;
import com.fitdotlife.fitmate_lib.object.ExerciseProgram;
import com.fitdotlife.fitmate_lib.object.News;
import com.fitdotlife.fitmate_lib.object.NewsDayAchieve;
import com.fitdotlife.fitmate_lib.object.NewsFriend;
import com.fitdotlife.fitmate_lib.object.NewsInit;
import com.fitdotlife.fitmate_lib.object.NewsProgramChange;
import com.fitdotlife.fitmate_lib.object.NewsResult;
import com.fitdotlife.fitmate_lib.object.NewsWeekAchieve;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.presenter.LoginPresenter;
import com.fitdotlife.fitmate_lib.util.DateUtils;
import com.fitdotlife.fitmate_lib.util.Utils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringRes;
import org.androidannotations.rest.spring.annotations.RestService;
import org.springframework.web.client.RestClientException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@EActivity(R.layout.activity_news)
public class NewsActivity extends Activity implements AdapterView.OnItemClickListener, AbsListView.OnScrollListener, View.OnTouchListener , NetWorkChangeListener
{
    private String mUserEmail = null;
    private int loadingTimes = 0;
    private boolean mLockListView;
    private List<News> mNewsList = null;
    private NewsAdapter newsAdapter = null;
    private ProgressDialog mProgressDialog;
    private int mViewPosition = -1;
    private MyApplication myApplication = null;

    @ViewById(R.id.ic_activity_news_actionbar)
    View actionBarView;

    @RestService
    NewsService newsServiceClient;

    @ViewById(R.id.lv_activity_news_list)
    ListView lvNewList;

    @ViewById(R.id.dl_activity_news)
    DrawerLayout dlNews;

    @ViewById(R.id.dv_activity_news_drawer)
    DrawerView drawerView;

    @StringRes(R.string.common_connect_network)
    String strConnectNetwork;

    @Bean
    RestHttpErrorHandler restErrorHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        myApplication = (MyApplication) this.getApplication();

        if(!Preprocess.IsTest) {
            Tracker t = myApplication.getTracker(MyApplication.TrackerName.APP_TRACKER);
            t.setScreenName("News");
            t.send(new HitBuilders.AppViewBuilder().build());
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(!Preprocess.IsTest) {
            GoogleAnalytics.getInstance(this).reportActivityStart(this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(!Preprocess.IsTest) {
            GoogleAnalytics.getInstance(this).reportActivityStop(this);
        }
    }

    @AfterViews
    void doInit()
    {

        newsServiceClient.setRootUrl(NetworkClass.baseURL + "/api");
        newsServiceClient.setRestErrorHandler(restErrorHandler);
        this.mNewsList = new ArrayList<News>();

        ActivityManager.getInstance().addActivity(this);

        mViewPosition = getIntent().getIntExtra("viewposition" , -1);

        this.mUserEmail = new FitmateDBManager(this).getUserInfo().getEmail();
        this.displayActionBar();

        mLockListView = true;

        lvNewList.setOnItemClickListener(this);
        lvNewList.setOnScrollListener(this);
        newsAdapter = new NewsAdapter(getApplicationContext(), R.layout.child_activity_news_listitem, mNewsList);
        lvNewList.setAdapter(newsAdapter);

        this.drawerView.setParentActivity(this);
        this.drawerView.setTag(this.getResources().getString(R.string.gnb_news));
        this.drawerView.setDrawerLayout( dlNews );

        if(Utils.isOnline(this)) {
            this.showProgressDialog();
            this.addNewsList();
        }else{
            Toast.makeText(this, strConnectNetwork, Toast.LENGTH_SHORT ).show();
            myApplication.addNetworkChangeListener(this);
        }
    }

    void showProgressDialog(){
        mProgressDialog = ProgressDialog.show(this ,"",this.getString(R.string.common_wait),true,false);
    }

    @UiThread
    void closeProgressDialog(){
        if ( mProgressDialog!=null ) {
        if( mProgressDialog.isShowing() ) {
            mProgressDialog.dismiss();
        }
    }
}


    @Override
    protected void onDestroy() {

        if(mProgressDialog != null){
            if(mProgressDialog.isShowing()){
                mProgressDialog.dismiss();
            }
        }

        this.myApplication.deleteNetworkChangeListener(this);

        super.onDestroy();
    }

    private void displayActionBar(){
        actionBarView.setBackgroundColor(this.getResources().getColor(R.color.fitmate_purple));
        TextView tvBarTitle = (TextView) actionBarView.findViewById( R.id.tv_custom_action_bar_title );
        tvBarTitle.setText(this.getString(R.string.news_bar_title));
        ImageView imgLeft = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_left);
        imgLeft.setBackgroundResource(R.drawable.icon_home_selector);
        imgLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityManager.getInstance().finishAllActivity();
                showNewHomeActivity();
            }
        });

        ImageView imgSliding = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_right);
        imgSliding.setBackground(this.getResources().getDrawable(R.drawable.icon_slide_selector));
        imgSliding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dlNews.openDrawer(drawerView);
            }
        });
        imgSliding.setOnTouchListener(this);
    }


    @Background
    void getNewsList(){
        try{
            NewsResult newsResult =  newsServiceClient.getNewsList(this.mUserEmail , loadingTimes);

            if( newsResult.getResponsevalue().equals(CommonKey.SUCCESS ) ) {

                List<News> newsList = newsResult.getNewsList();
                if( newsList != null )
                {
                    if( newsList.size() > 0 )
                    {
                        for (int i = 0; i < newsList.size(); i++) {
                                News news = newsList.get(i);
                                if (news.getAddedContents() != null) {
                                    mNewsList.add(newsList.get(i));
                            }
                        }
                        mLockListView = false;
                    }else{
                        News news = new News();
                        news.setType( NewsType.INIT.getValue() );
                        news.setAddedContents(new NewsInit());
                        news.setProducedDate(new Date().getTime());
                        mNewsList.add(news);
                        mLockListView = true;
                    }
                }else{
                    News news = new News();
                    news.setType( NewsType.INIT.getValue() );
                    news.setAddedContents(new NewsInit());
                    news.setProducedDate(new Date().getTime());
                    mNewsList.add(news);
                    mLockListView = true;
                }
            }
        }catch( RestClientException e )
        {
            Log.e("fitmate", e.getMessage());
        }

        this.closeProgressDialog();
        this.updateAdapter();
    }

    private News getDefaultNews(){
        News news = new News();
        news.setType(NewsType.INIT.getValue());
        news.setAddedContents(new NewsInit());
        news.setProducedDate(new Date().getTime());
        return news;
    }


//    @Background
//    void getNewsList(){
//        try{
//            NewsResult newsResult =  newsServiceClient.getNewsList(this.mUserEmail , loadingTimes);
//
//            if( newsResult.getResponsevalue().equals(CommonKey.SUCCESS ) ) {
//
//                List<News> newsList = newsResult.getNewsList();
//                if( newsList != null )
//                {
//                    if( newsList.size() > 0 )
//                    {
//                        for (int i = 0; i < newsList.size(); i++) {
//                                News news = newsList.get(i);
//                                if (news.getAddedContents() != null) {
//                                    mNewsList.add(newsList.get(i));
//                            }
//                        }
//                        mLockListView = false;
//                    }else{
//                        News news = new News();
//                        news.setType( NewsType.INIT.getValue() );
//                        news.setAddedContents(new NewsInit());
//                        news.setProducedDate(new Date().getTime());
//                        mNewsList.add(news);
//                        mLockListView = true;
//                    }
//                }else{
//                    News news = new News();
//                    news.setType( NewsType.INIT.getValue() );
//                    news.setAddedContents(new NewsInit());
//                    news.setProducedDate(new Date().getTime());
//                    mNewsList.add(news);
//                    mLockListView = true;
//                }
//            }
//        }catch( RestClientException e )
//        {
//            Log.e("fitmate", e.getMessage());
//        }
//
//        this.closeProgressDialog();
//        this.updateAdapter();
//    }

    @Override
    public void onBackPressed() {

        if( dlNews.isDrawerOpen(drawerView) ){
            dlNews.closeDrawers();
            return;
        }

//        AlertDialog.Builder builder = new AlertDialog.Builder(this);     // 여기서 this는 Activity의 this
//
//        builder.setTitle(getString(R.string.common_quit_title))        // 제목 설정
//                .setMessage(getString(R.string.common_quit_content))        // 메세지 설정
//                .setCancelable(true)        // 뒤로 버튼 클릭시 취소 가능 설정
//                .setPositiveButton(R.string.common_yes, new DialogInterface.OnClickListener() {
//                    // 확인 버튼 클릭시 설정
//                    public void onClick(DialogInterface dialog, int whichButton) {
//                        moveTaskToBack(true);
//                        finish();
//                        dialog.cancel();
//                    }
//                })
//                .setNegativeButton(R.string.common_no, new DialogInterface.OnClickListener() {
//                    // 취소 버튼 클릭시 설정
//                    public void onClick(DialogInterface dialog, int whichButton) {
//                        dialog.cancel();
//                    }
//                });
//
//        AlertDialog dialog = builder.create();    // 알림창 객체 생성
//        dialog.show();    // 알림창 띄우기

        ActivityManager.getInstance().finishAllActivity();
        showNewHomeActivity();
    }

    @OnActivityResult(DrawerView.REQUEST_PROFILE_IMAGE)
     void onChangeProfileImageResult(){
        this.drawerView.refreshProfileImage();
    }

    @OnActivityResult(DrawerView.REQUEST_SETTING)
    void onChangeNewsSettingResult( )
    {
        boolean changeNewsSetting = AlarmSettingActivity.getChangeNewsSettingPreference(this);
        if( changeNewsSetting ){

            loadingTimes = 0;
            this.mNewsList.clear();
            this.showProgressDialog();
            this.addNewsList();

        }
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        News news = (News) parent.getItemAtPosition(position);
        String email = news.getOwnerEmail();

        NewsType newsType = NewsType.getNewsType(news.getType());

        if( newsType.equals(NewsType.ACHIEVE_DAY_SUCCESS) ||  newsType.equals(NewsType.ACHIEVE_DAY_FAIL) || newsType.equals(NewsType.ACHIEVE_WEEK_SUCCESS) || newsType.equals(NewsType.ACHIEVE_WEEK_FAIL) ){

            //친구인지 나인지 확인한다.
            UserInfo userInfo = new FitmateDBManager(this).getUserInfo();

            if( userInfo.getEmail().equals(news.getOwnerEmail()) ){ //나의 뉴스 라면

                Intent bundle = new Intent();
                String activityDate = null;
                String friendEmail = null;

                if( newsType.equals(NewsType.ACHIEVE_DAY_SUCCESS) ||  newsType.equals(NewsType.ACHIEVE_DAY_FAIL) ){

                    NewsDayAchieve newsDayAchieve = (NewsDayAchieve) news.getAddedContents();
                    activityDate = newsDayAchieve.getDate();
                }else{
                    NewsWeekAchieve newsWeekAchieve = (NewsWeekAchieve) news.getAddedContents();
                    activityDate = newsWeekAchieve.getDate();
                }

                ActivityManager.getInstance().finishAllActivity();
                showNewHomeActivity( DateUtils.getDateFromString_yyyy_MM_dd(activityDate).getTime() );

            }else{ //친구 뉴스 라면

                Intent bundle = new Intent();
                String activityDate = null;
                String friendEmail = null;

                if( newsType.equals(NewsType.ACHIEVE_DAY_SUCCESS) ||  newsType.equals(NewsType.ACHIEVE_DAY_FAIL) ){

                    NewsDayAchieve newsDayAchieve = (NewsDayAchieve) news.getAddedContents();
                    activityDate =  newsDayAchieve.getDate();
                    friendEmail = news.getOwnerEmail();

                }else{
                    NewsWeekAchieve newsWeekAchieve = (NewsWeekAchieve) news.getAddedContents();
                    activityDate =  newsWeekAchieve.getDate();
                    friendEmail = news.getOwnerEmail();
                }

                bundle.putExtra("ActivityDate" ,  DateUtils.getDateFromString_yyyy_MM_dd( activityDate ).getTime()  );
                bundle.putExtra("FriendEmail", friendEmail);

                FriendNewHomeActivity_.intent(this).extras(bundle).start();
            }
        }
        else if(newsType.equals(NewsType.CHANGE_EXERCISEPROGRAM))
        {

            NewsProgramChange newsProgramChange = (NewsProgramChange) news.getAddedContents();
            int programId = (int) newsProgramChange.getChangedExerciseProgramID();
            Intent intent = new Intent( NewsActivity.this , ExerciseProgramInfoActivity_.class );
            intent.putExtra( ExerciseProgram.ID_KEY, programId );
            intent.putExtra("exercisetype", ExerciseProgramType.USEREXERCISEPROGRAM);
            intent.putExtra( "Friend" , ( newsProgramChange.getUserEmail() != mUserEmail) );
            this.startActivity(intent);

        }else if(newsType.equals( NewsType.FRIEND_ACCEPT_FRIENDSHIP )){

            String friendEmail = null;
            NewsFriend newsFriend = (NewsFriend) news.getAddedContents();

            if(newsFriend.getRequesterInfo().getEmail().equals( mUserEmail )){
                friendEmail = newsFriend.getResponderInfo().getEmail();
            }else{
                friendEmail = newsFriend.getRequesterInfo().getEmail();
            }

            Intent bundle = new Intent();
            bundle.putExtra("ActivityDate" ,  newsFriend.getDate()  );
            bundle.putExtra("FriendEmail" , friendEmail );

            FriendNewHomeActivity_.intent(this).extras(bundle).start();
        }
    }

    @UiThread
    void updateAdapter( ){

        // 모든 데이터를 로드하여 적용하였다면 어댑터에 알리고
        // 리스트뷰의 락을 해제합니다.
        newsAdapter.notifyDataSetChanged();
        if( mViewPosition != -1 ) {
            lvNewList.setSelection(mViewPosition);
            mViewPosition = -1;
        }

    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

        int count = totalItemCount - visibleItemCount;

        if( firstVisibleItem >= count && totalItemCount != 0 && mLockListView == false ){
            loadingTimes++;
            this.showProgressDialog();
            this.addNewsList();
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        ImageView actionbarImageView = (ImageView) view;
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            actionbarImageView.setColorFilter(this.getResources().getColor(R.color.selected_colorfilter) , PorterDuff.Mode.DARKEN );
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            actionbarImageView.setColorFilter(null);
        }

        return false;
    }

    @Background
    void addNewsList(){

        mLockListView = true;

        try{
            NewsResult newsResult =  newsServiceClient.getNewsList(this.mUserEmail , loadingTimes);

            if(newsResult != null) {

                if (newsResult.getResponsevalue().equals(CommonKey.SUCCESS)) {

                    List<News> newsList = newsResult.getNewsList();
                    if (newsList != null)
                    {
                        if (newsList.size() > 0) {
                            for (int i = 0; i < newsList.size(); i++) {
                                News news = newsList.get(i);
                                if (news.getAddedContents() != null) {
                                    mNewsList.add(newsList.get(i));
                                }
                            }

                            mLockListView = false;

                        } else {
                            News news = new News();
                            news.setType(NewsType.INIT.getValue());
                            news.setAddedContents(new NewsInit());
                            news.setProducedDate(new Date().getTime());
                            mNewsList.add(news);
                            mLockListView = true;
                        }
                    } else {
                        News news = new News();
                        news.setType(NewsType.INIT.getValue());
                        news.setAddedContents(new NewsInit());
                        news.setProducedDate(new Date().getTime());
                        mNewsList.add(news);

                        mLockListView = true;
                    }
                }
            }
        }catch( RestClientException e )
        {
            Log.e("fitmate", e.getMessage());

            News news = this.getDefaultNews();
            mNewsList.add(news);
            mLockListView = true;
        }


        updateAdapter();
        closeProgressDialog();
    }

    @Override
    public void OnNetworkAvailable() {
        this.showProgressDialog();
        this.addNewsList();
    }

    private boolean getSettedUserInfo()
    {
        SharedPreferences pref = this.getSharedPreferences("fitmateservice", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        return pref.getBoolean( LoginPresenter.SET_USERINFO_KEY , false);
    }

    private void showNewHomeActivity( )
    {
        Intent intent = new Intent();

        if( getSettedUserInfo() ) {
            intent.setClass( this , NewHomeActivity_.class );
            intent.putExtra(NewHomeActivity.HOME_LAUNCH_KEY, NewHomeActivity.LaunchMode.FROM_OTHERMENU.ordinal());
        }else{
            intent.setClass( this , NewHomeAsActivity_.class );
        }

        startActivity( intent );
    }

    private void showNewHomeActivity( long newsTimeMilliseconds ){
        Intent intent = new Intent( this , NewHomeActivity_.class );
        intent.putExtra( NewHomeActivity.HOME_LAUNCH_KEY , NewHomeActivity.LaunchMode.FROM_NEWSCLICK.ordinal());
        intent.putExtra( NewHomeActivity.HOME_NEWSDATE_KEY , newsTimeMilliseconds);
        startActivity(intent);
    }
}
