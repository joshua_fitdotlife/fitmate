package com.fitdotlife.fitmate;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.fitdotlife.fitmate_lib.customview.DotNavigationView;


public class HelpActivity extends Activity {

    public static final String MODE_KEY = "mode";
    public static final String VISIBLE_KEY = "visible";
    public static final String HOME_NOT_VIEW_KEY = "homenotview";
    public static final String ACTIVITY_NOT_VIEW_KEY = "activitynotview";
    public static final String EXERCISE_NOT_VIEW_KEY = "exercisenotview";

    public static final int HOME_HELP_MODE = 0;
    public static final int ACTIVITY_HELP_MODE = 1;
    public static final int EXERCISE_HELP_MODE = 2;

    private int mMode = -1;
    private ViewPager mPager = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        mMode = this.getIntent().getIntExtra(MODE_KEY, -1);
        boolean isVisible = this.getIntent().getBooleanExtra(VISIBLE_KEY, false);


        this.mPager = (ViewPager) this.findViewById(R.id.vp_activity_help);
        this.mPager.setAdapter(new ViewPagerAdapter( this , mMode , isVisible));
    }

    private class ViewPagerAdapter extends PagerAdapter implements View.OnClickListener {

        private LayoutInflater mLayoutInflater = null;
        private int viewMode = -1;

        private String[] mArrTitle = null;
        private String[] mArrContent = null;
        private TypedArray[] mArrImgs = null;
        private Activity mActivity = null;
        private int mCount = 0;
        private boolean mVisible = false;
        private String mNotViewKey = null;

        public ViewPagerAdapter( Activity context , int mode , boolean isVisible ){
            super();
            this.mLayoutInflater = LayoutInflater.from(context);
            this.viewMode = mode;
            this.mActivity = context;
            this.mVisible = isVisible;

            if( viewMode == HelpActivity.HOME_HELP_MODE ){

                this.mArrTitle = context.getResources().getStringArray(R.array.home_help_title);
                this.mCount = this.mArrTitle.length;
                this.mArrContent = context.getResources().getStringArray(R.array.home_help_content);
                this.mNotViewKey = HOME_NOT_VIEW_KEY;

                this.mArrImgs = new TypedArray[mCount];
                for( int i = 0 ; i < mCount ;i++ ) {
                   int resId =  context.getResources().getIdentifier( "home_help_images_" + (i + 1) , "array" ,context.getPackageName());
                    mArrImgs[i]  = context.getResources().obtainTypedArray(resId);
                }

            }else if( viewMode == HelpActivity.ACTIVITY_HELP_MODE){

                this.mArrTitle = context.getResources().getStringArray(R.array.activity_help_title);
                this.mCount = this.mArrTitle.length;
                this.mArrContent = context.getResources().getStringArray(R.array.activity_help_content);
                this.mNotViewKey = ACTIVITY_NOT_VIEW_KEY;
                this.mArrImgs = new TypedArray[mCount];
                for( int i = 0 ; i < mCount ;i++ ) {
                    int resId =  context.getResources().getIdentifier( "activity_help_images_" + (i + 1) , "array" ,context.getPackageName());
                    mArrImgs[i]  = context.getResources().obtainTypedArray(resId);
                }

            }else if(viewMode == HelpActivity.EXERCISE_HELP_MODE){

                this.mArrTitle = context.getResources().getStringArray(R.array.exercise_help_title);
                this.mCount = this.mArrTitle.length;
                this.mArrContent = context.getResources().getStringArray(R.array.exercise_help_content);
                this.mNotViewKey = EXERCISE_NOT_VIEW_KEY;
                this.mArrImgs = new TypedArray[mCount];
                for( int i = 0 ; i < mCount ;i++ ) {
                    int resId =  context.getResources().getIdentifier( "exercise_help_images_" + (i + 1) , "array" ,context.getPackageName());
                    mArrImgs[i]  = context.getResources().obtainTypedArray(resId);
                }
            }
        }

        @Override
        public int getCount() {
            return this.mCount;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View view = null;

            view = mLayoutInflater.inflate(R.layout.child_activity_help , null);
            ViewFlipper vfImage = (ViewFlipper) view.findViewById(R.id.vf_child_activity_help);

            TypedArray imgs = this.mArrImgs[position];
            if( imgs.length() > 1 ) {
                for (int i = 0; i < imgs.length(); i++) {
                    ImageView imageView = new ImageView(mActivity);
                    imageView.setImageResource(imgs.getResourceId(i, -1));
                    vfImage.addView(imageView);
                }
                vfImage.setAutoStart(true);
                vfImage.setInAnimation(AnimationUtils.loadAnimation(mActivity, R.anim.push_left_in));
                vfImage.setOutAnimation(AnimationUtils.loadAnimation(mActivity, R.anim.push_right_out));
                vfImage.setFlipInterval(2500);
            }else {
                ImageView imageView = new ImageView(mActivity);
                imageView.setImageResource(imgs.getResourceId(0, -1));
                vfImage.addView(imageView);
            }

            DotNavigationView navigationView = (DotNavigationView) view.findViewById(R.id.dnv_child_activity_help_navigation);
            navigationView.setDotNumber( mCount );
            navigationView.setDotIndex(position + 1);
            TextView txtTitle = (TextView) view.findViewById(R.id.txt_child_activity_help_title);
            txtTitle.setText(this.mArrTitle[position]);
            TextView txtContent = (TextView) view.findViewById(R.id.txt_child_activity_help_content);
            //txtContent.setText(this.mArrContent[position]);
            txtContent.setText(Html.fromHtml( this.mArrContent[position] ));
            TextView txtSkip = (TextView) view.findViewById(R.id.txt_child_activity_help_skip);
            txtSkip.setOnClickListener(this);
            ImageView imgPrevious = (ImageView) view.findViewById(R.id.img_child_activity_help_previous);
            imgPrevious.setOnClickListener(this);
            ImageView imgNext = (ImageView) view.findViewById(R.id.img_child_activity_help_next);
            imgNext.setOnClickListener(this);

            final ImageView imgCheckNotView = (ImageView) view.findViewById(R.id.img_child_activity_help_chkecknotview);
            //imgCheckNotView.setOnClickListener(this);
            LinearLayout llCheckNotView = (LinearLayout) view.findViewById(R.id.ll_child_activity_help_checknotview);
            llCheckNotView.setOnClickListener(new View.OnClickListener() {

                boolean checked = false;
                @Override
                public void onClick(View v) {
                    if(!checked)
                    {
                        checked = true;
                        setNotView(true);
                        imgCheckNotView.setImageResource(R.drawable.checkbox_sel);
                        finish();
                    }
                    else
                    {
                        checked = false;
                        setNotView(false);
                        imgCheckNotView.setImageResource(R.drawable.checkbox);
                    }
                }
            });

            if( !mVisible ){
                txtSkip.setVisibility(View.INVISIBLE);
                llCheckNotView.setVisibility(View.INVISIBLE);
            }

            if( position == 0 ){
                imgPrevious.setVisibility(View.INVISIBLE);
            }
            if( position == ( mCount - 1 ) ){
                imgNext.setVisibility(View.INVISIBLE);
            }


            container.addView(view , 0);
            return view;
        }

        private void setNotView( boolean isNotView ){
            SharedPreferences pref = mActivity.getSharedPreferences( "fitmateservice" , Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = pref.edit();
            editor.putBoolean( this.mNotViewKey , isNotView);
            editor.commit();
        }

        @Override
        public Parcelable saveState() {
            //return super.saveState();
            return null;
        }

        @Override
        public void startUpdate(ViewGroup container) {
            //super.startUpdate(container);
        }

        @Override
        public void finishUpdate(ViewGroup container) {
            //super.finishUpdate(container);
        }

        @Override
        public void onClick(View view) {
            switch( view.getId() ){
                case R.id.txt_child_activity_help_skip:
                    this.mActivity.finish();
                    break;
                case R.id.img_child_activity_help_previous:

                    int cIndex = mPager.getCurrentItem();
                    if( cIndex >= 1 ){
                        mPager.setCurrentItem(--cIndex);
                    }
                    break;
                case R.id.img_child_activity_help_next:

                    int currentIndex = mPager.getCurrentItem();
                    if( currentIndex < ( mCount - 1 ) ){
                        mPager.setCurrentItem( ++currentIndex );
                    }
                    break;
            }
        }
    }

}
