package com.fitdotlife.fitmate;

/**
 * Created by Joshua on 2015-08-03.
 */
public interface RefreshListener {
    void refreshList( FriendActivity.FriendModeType modeType );
}
