package com.fitdotlife.fitmate;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.text.format.DateFormat;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.fitdotlife.Preprocess;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ContinuousCheckPolicy;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseAnalyzer;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.FatAndCarbonhydrateConsumtion;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.StrengthInputType;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.UserInfoForAnalyzer;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.WearingLocation;
import com.fitdotlife.fitmate_lib.customview.ArcChartView;
import com.fitdotlife.fitmate_lib.customview.Day24HourCalorieChartView;
import com.fitdotlife.fitmate_lib.customview.DayCalorieBarChartView;
import com.fitdotlife.fitmate_lib.customview.DayHMLIntensityBarChartView;
import com.fitdotlife.fitmate_lib.customview.DrawerView;
import com.fitdotlife.fitmate_lib.customview.LineChartView;
import com.fitdotlife.fitmate_lib.customview.MonthCalorieBarChartView;
import com.fitdotlife.fitmate_lib.customview.MonthPointBarChartView;
import com.fitdotlife.fitmate_lib.customview.MonthPointView;
import com.fitdotlife.fitmate_lib.customview.RoundedRectangleChartView;
import com.fitdotlife.fitmate_lib.customview.WeekCalorieBarChartView;
import com.fitdotlife.fitmate_lib.customview.WeekExerciseTimeBarChartView;
import com.fitdotlife.fitmate_lib.customview.WeekPointView;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.iview.IMainView;
import com.fitdotlife.fitmate_lib.key.ExerciseProgramType;
import com.fitdotlife.fitmate_lib.object.DayActivity;
import com.fitdotlife.fitmate_lib.object.ExerciseProgram;
import com.fitdotlife.fitmate_lib.object.MonthActivity;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.object.WeekActivity;
import com.fitdotlife.fitmate_lib.presenter.LoginPresenter;
import com.fitdotlife.fitmate_lib.presenter.MainPresenter;
import com.fitdotlife.fitmate_lib.util.DateUtils;
import com.fitdotlife.fitmate_lib.util.Utils;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class MainActivity extends Activity implements IMainView, OnClickListener, View.OnTouchListener {
    private final int ANIMATION_DURATION = 3000;

    private enum DataStateType
    {
        AVAILABLE , NONE
    }

    private MainPresenter mainPresenter = null;
    private DataStateType mDataStateType = DataStateType.NONE;

    private LinearLayout llMenu = null;
    private ScrollView scrMain = null;

    private TextView btnSelectDay = null;
    private TextView btnSelectWeek = null;
    private TextView btnSelectMonth = null;
    private RelativeLayout rlSelectDay = null;
    private RelativeLayout rlSelectWeek = null;
    private RelativeLayout rlSelectMonth = null;
    private LinearLayout llSelectDayMark = null;
    private LinearLayout llSelectWeekMark = null;
    private LinearLayout llSelectMonthMark = null;


    private TextView tvActivityDate = null;

    private TextView tvPreDate = null;
    private TextView tvNextDate = null;
    private TextView tvAppliedProgram = null;

    private LinearLayout llActivityList_1 = null;
    private LinearLayout llActivityListChart_1 = null;
    private ImageView imgActivityList_1 = null;
    private TextView tvActivityListTitle_1 = null;
    private LinearLayout llLine_1 = null;


    private LinearLayout llActivityList_1_1 = null;
    private LinearLayout llActivityListChart_1_1 = null;
    private ImageView imgActivityList_1_1 = null;
    private TextView tvActivityListTitle_1_1 = null;
    private LinearLayout llLine_1_1 = null;

    private LinearLayout llActivityList_2 = null;
    private LinearLayout llActivityListChart_2 = null;
    private ImageView imgActivityList_2 = null;
    private TextView tvActivityListTitle_2 = null;
    private LinearLayout llLine_2 = null;

    private LinearLayout llActivityList_3 = null;
    private LinearLayout llActivityListChart_3 = null;
    private ImageView imgActivityList_3 = null;
    private TextView tvActivityListTitle_3 = null;

    private View activityChartView_1 = null;
    private View activityChartView_1_1 = null;
    private View activityChartView_2 = null;
    private View activityChartView_3 = null;

    private ProgressDialog mDialog = null;

    private ArcChartView mDayScoreArcChartView = null;
    private LineChartView mDayMetLineChartView = null;
    private DayCalorieBarChartView mDayCalorieBarChartView = null;
    private Day24HourCalorieChartView mDayHourCalorieBarChartView = null;
    private RoundedRectangleChartView mDayTimeMetRoundRectangleChartView = null;
    private DayHMLIntensityBarChartView mDayHmlIntensityBarChartView = null;

    private WeekExerciseTimeBarChartView mWeekExerciseTimeBarChartView = null;
    private WeekPointView mWeekPointOvalChartView = null;
    private WeekCalorieBarChartView mWeekCalorieBarChartView = null;

    private MonthPointBarChartView mMonthlyScoreBarChartView = null;
    private MonthPointView mMonthPointChartView = null;
    private MonthCalorieBarChartView mMonthlyCalorieBarChartView = null;

    private TextView tvFat = null;
    private TextView tvCarbohydrate = null;

    private boolean isShowMessage = false;
    private boolean isNews = false;

    private DrawerLayout dlMain = null;
    private DrawerView drawerView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(!Preprocess.IsTest) {
            Tracker t = ((MyApplication) this.getApplication()).getTracker(MyApplication.TrackerName.APP_TRACKER);
            t.setScreenName("Activity");
            t.send(new HitBuilders.AppViewBuilder().build());
        }

        this.btnSelectDay= (TextView) this.findViewById(R.id.btn_activity_main_select_day);
        this.llSelectDayMark = (LinearLayout) this.findViewById(R.id.ll_activity_main_select_day_mark);
        this.rlSelectDay = (RelativeLayout) this.findViewById(R.id.rl_activity_main_select_day);
        this.rlSelectDay.setOnClickListener(this);

        this.btnSelectWeek = (TextView) this.findViewById(R.id.btn_activity_main_select_week);
        this.llSelectWeekMark = (LinearLayout) this.findViewById(R.id.ll_activity_main_select_week_mark);
        this.rlSelectWeek = (RelativeLayout) this.findViewById(R.id.rl_activity_main_select_week);
        this.rlSelectWeek.setOnClickListener(this);

        this.btnSelectMonth = (TextView) this.findViewById(R.id.btn_activity_main_select_month);
        this.llSelectMonthMark = (LinearLayout) this.findViewById(R.id.ll_activity_main_select_month_mark);
        this.rlSelectMonth = (RelativeLayout) this.findViewById(R.id.rl_activity_main_select_month);
        this.rlSelectMonth.setOnClickListener(this);

        this.tvActivityDate = (TextView) this.findViewById(R.id.tv_activity_main_activitydate);
        this.tvPreDate = (TextView) this.findViewById(R.id.tv_activity_main_pre_date);
        this.tvPreDate.setOnClickListener(this);
        this.tvNextDate = (TextView) this.findViewById(R.id.tv_activity_main_next_date);
        this.tvNextDate.setOnClickListener(this);

        this.tvAppliedProgram = (TextView) this.findViewById(R.id.tv_activity_main_applied_program);
        this.tvAppliedProgram.setSelected(true);
        this.tvAppliedProgram.setOnClickListener(this);

        this.llActivityList_1 = (LinearLayout) this.findViewById( R.id.ll_activity_main_activity_list_1 );
        this.llActivityList_1.setOnClickListener(this);

        this.llActivityListChart_1 = (LinearLayout) this.findViewById(R.id.ll_activity_main_activity_list_1_chart);
        this.imgActivityList_1 = (ImageView) this.findViewById(R.id.img_activity_main_activity_list_1_img);
        this.tvActivityListTitle_1 = (TextView) this.findViewById(R.id.tv_activity_main_activity_list_1_title);
        this.llLine_1 = (LinearLayout) this.findViewById(R.id.ll_activity_main_line_1);

        this.llActivityList_1_1 = (LinearLayout) this.findViewById( R.id.ll_activity_main_activity_list_1_1 );
        this.llActivityList_1_1.setOnClickListener(this);

        this.llActivityListChart_1_1 = (LinearLayout) this.findViewById(R.id.ll_activity_main_activity_list_1_1_chart);
        this.imgActivityList_1_1 = (ImageView) this.findViewById(R.id.img_activity_main_activity_list_1_1_img);
        this.tvActivityListTitle_1_1 = (TextView) this.findViewById(R.id.tv_activity_main_activity_list_1_1_title);
        this.llLine_1_1 = (LinearLayout) this.findViewById(R.id.ll_activity_main_line_1_1);

        this.llActivityList_2 = (LinearLayout) this.findViewById(R.id.ll_activity_main_activity_list_2);
        this.llActivityList_2.setOnClickListener(this);

        this.llActivityListChart_2 = (LinearLayout) this.findViewById(R.id.ll_activity_main_activity_list_2_chart);
        this.imgActivityList_2 = (ImageView) this.findViewById(R.id.img_activity_main_activity_list_2_img);
        this.tvActivityListTitle_2 = (TextView) this.findViewById(R.id.tv_activity_main_activity_list_2_title);
        this.llLine_2 = (LinearLayout) this.findViewById(R.id.ll_activity_main_line_2);

        this.llActivityList_3 = (LinearLayout) this.findViewById(R.id.ll_activity_main_activity_list_3);
        this.llActivityList_3.setOnClickListener(this);

        this.llActivityListChart_3 = (LinearLayout) this.findViewById(R.id.ll_activity_main_activity_list_3_chart);
        this.imgActivityList_3 = (ImageView) this.findViewById(R.id.img_activity_main_activity_list_3_img);
        this.tvActivityListTitle_3 = (TextView) this.findViewById(R.id.tv_activity_main_activity_list_3_title);

        this.dlMain = (DrawerLayout) this.findViewById(R.id.dl_activity_main);
        this.drawerView = (DrawerView) this.findViewById(R.id.dv_activity_main_drawer);
        this.drawerView.setParentActivity(this);
        this.drawerView.setTag(this.getResources().getString(R.string.gnb_activity));
        this.drawerView.setDrawerLayout( dlMain );

        this.scrMain = (ScrollView) this.findViewById(R.id.scr_activity_main);

        isNews = getIntent().getBooleanExtra("News",false);
        if( !isNews ) {
            ActivityManager.getInstance().addActivity(this);
        } else{
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) scrMain.getLayoutParams();
            layoutParams.bottomMargin = 0;
            scrMain.setLayoutParams(layoutParams);
        }

        View actionBarView = this.findViewById(R.id.ic_activity_main_actionbar);
        actionBarView.setBackgroundColor(this.getResources().getColor(R.color.fitmate_orange));
        TextView tvBarTitle = (TextView) actionBarView.findViewById( R.id.tv_custom_action_bar_title );
        ImageView imgLeft = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_left);
        imgLeft.setBackgroundResource(R.drawable.icon_home_selector);
        imgLeft.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityManager.getInstance().finishAllActivity();
                showNewHomeActivity();
            }
        });

        ImageView imgSliding = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_right);
        if(isNews){
            imgSliding.setVisibility(View.GONE);
        }else{
            imgSliding.setImageResource( R.drawable.icon_slide );
            imgSliding.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dlMain.openDrawer(drawerView);
                }
            });
            imgSliding.setOnTouchListener(this);
        }

        tvBarTitle.setText(this.getString(R.string.activity_bar_title));

        this.mainPresenter = new MainPresenter(this , this);
        this.mainPresenter.init( this.getIntent() );
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.mainPresenter.close();
    }

    @Override
    protected void onResume() {
        super.onResume();

        this.mainPresenter.requestCalculateActivity();
    }

    @Override
    public void onClick( View view )
    {
        switch( view.getId() )
        {
            case R.id.img_custom_action_bar_right:
                this.mainPresenter.requestCalculateActivity();
                break;
            case R.id.rl_activity_main_select_day:
                //this.mainPresenter.requestDayActivity();
                this.mainPresenter.setInitialView(true);
                this.mainPresenter.getDayActivity();
                break;
            case R.id.rl_activity_main_select_week:
                //this.mainPresenter.requestWeekActivity();
                this.mainPresenter.setInitialView(true);
                this.mainPresenter.getWeekActivity();
                break;
            case R.id.rl_activity_main_select_month:
                this.mainPresenter.setInitialView(true);
                //this.mainPresenter.requestMonthActivity();
                this.mainPresenter.getMonthActivity();
                break;
            case R.id.tv_activity_main_pre_date:
                //this.mainPresenter.movePreDate();
                View moveToPre= findViewById(R.id.tv_activity_main_pre_date);
                moveToPre.setEnabled(false);
                this.mainPresenter.setInitialView(true);
                this.mainPresenter.movePreDate();
                break;
            case R.id.tv_activity_main_next_date:
                View moveToNext= findViewById(R.id.tv_activity_main_next_date);
                moveToNext.setEnabled(false);
                //this.mainPresenter.moveNextDate();
                this.mainPresenter.setInitialView(true);
                this.mainPresenter.moveNextDate();
                break;
            case R.id.tv_activity_main_applied_program:
                int programId = (int) this.tvAppliedProgram.getTag();
                Intent intent = new Intent( MainActivity.this , ExerciseProgramInfoActivity_.class );
                intent.putExtra( ExerciseProgram.ID_KEY, programId );
                intent.putExtra("exercisetype" , ExerciseProgramType.USEREXERCISEPROGRAM);
                this.startActivity(intent);
                break;
        }
    }


    private void setVisible_NextButton(boolean setVisible){
        View moveToNext= findViewById(R.id.tv_activity_main_next_date);
        if(moveToNext!=null)
       if(setVisible){
           moveToNext.setVisibility(View.VISIBLE);
       }else{
           moveToNext.setVisibility(View.INVISIBLE);
       }
    }


    @Override
    public void displayDayActivity( DayActivity dayActivity, int weekScore  , int year, int month, int day, int dayOfWeek , ExerciseProgram appliedProgram )
    {
        View moveToPre= findViewById(R.id.tv_activity_main_pre_date);
        moveToPre.setEnabled(true);
        View moveToNext= findViewById(R.id.tv_activity_main_next_date);
        moveToNext.setEnabled(true);

        String programName = null;
        if(appliedProgram.getDefaultProgram())
        {
            int resId =  getResources().getIdentifier(appliedProgram.getName(), "string", getPackageName());
            programName = getString(resId);
        }
        else
        {
            programName = appliedProgram.getName();
        }

        this.tvAppliedProgram.setText(programName);
        this.tvAppliedProgram.setTag(appliedProgram.getId());

        //this.btnSelectDay.setBackgroundColor( this.getResources().getColor(R.color.fitmate_orange) );

        this.btnSelectDay.setTextColor(Color.BLACK);
        this.btnSelectWeek.setTextColor(0xFFA9A9A9);
        this.btnSelectMonth.setTextColor(0xFFA9A9A9);

        this.llSelectDayMark.setBackgroundColor(this.getResources().getColor(R.color.fitmate_orange));
        this.llSelectWeekMark.setBackgroundColor(Color.WHITE);
        this.llSelectMonthMark.setBackgroundColor(Color.WHITE);


        Calendar calendar =Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(calendar.MONTH, month);
        calendar.set(calendar.DATE, day);


        this.tvActivityDate.setText( DateFormat.format( this.getString(R.string.activity_day_date_format)  ,calendar.getTime()) );

        if(DateUtils.isToday(calendar)){
            setVisible_NextButton(false);
        }else{
            setVisible_NextButton(true);
        }

        this.imgActivityList_1.setImageResource(R.drawable.icon_medal);
        this.tvActivityListTitle_1.setText(this.getString(R.string.activity_day_achievement));

        this.llActivityListChart_1_1.setVisibility(View.VISIBLE);
        this.llActivityList_1_1.setVisibility(View.VISIBLE);
        this.llLine_1_1.setVisibility(View.VISIBLE);

        //this.imgActivityList_2.setImageResource( R.drawable.icon_chart );
        this.tvActivityListTitle_2.setText( this.getString(R.string.activity_day_calorie_consumption) );

        this.llActivityListChart_3.setVisibility(View.VISIBLE );
        this.llActivityList_3.setVisibility(View.VISIBLE );

        this.imgActivityList_3.setImageResource( R.drawable.icon_chart );
        this.tvActivityListTitle_3.setText( this.getString(R.string.activity_day_intensity) );

        if(dayActivity != null) {

            this.mDataStateType = DataStateType.AVAILABLE;

            activityChartView_1 = this.getLayoutInflater().inflate(R.layout.child_day_achievement_chart, null);
            LinearLayout llScoreChart = (LinearLayout) activityChartView_1.findViewById(R.id.ll_child_day_achievement_activity_chart);
            LinearLayout llAverageMetChart = (LinearLayout) activityChartView_1.findViewById(R.id.ll_child_day_achievement_average_chart);

            mDayScoreArcChartView = new ArcChartView(this);
            mDayScoreArcChartView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
            mDayScoreArcChartView.setWeekValue(weekScore);

            Calendar mToday = Calendar.getInstance();

            mDayScoreArcChartView.setIsToday(mToday.get(Calendar.YEAR) == year && mToday.get(Calendar.MONTH) == month && mToday.get(Calendar.DAY_OF_MONTH) == day );    // 오늘인지 과거인지 확인하는 함수
            mDayScoreArcChartView.setWeekUnit(this.getString(R.string.common_point));

            StrengthInputType strengthType = StrengthInputType.getStrengthType( appliedProgram.getCategory() );

            if( strengthType.equals( StrengthInputType.CALORIE ) || strengthType.equals( StrengthInputType.CALORIE_SUM )|| strengthType.equals(StrengthInputType.VS_BMR)) {


                UserInfo userInfo = new FitmateDBManager(this).getUserInfo();

                UserInfoForAnalyzer userInfoForAnalyzer = com.fitdotlife.fitmate_lib.service.util.Utils.getUserInfoForAnalyzer(userInfo);


                int calorieFrom= (int) appliedProgram.getStrengthFrom();
                int calorieTo= (int) appliedProgram.getStrengthFrom();
                int bmr = userInfoForAnalyzer.getBMR();
                int max = 0;
                if(strengthType.equals(StrengthInputType.VS_BMR)){
                    calorieFrom=(int)Math.round(calorieFrom /100.0 * bmr);
                    calorieTo=(int)Math.round(calorieTo /100.0 * bmr);
                }

                if(calorieFrom==calorieTo){
                    max= calorieFrom*2;
                }
                else{
                    max = (calorieFrom+calorieTo);
                }

                mDayScoreArcChartView.setRangMax(max);


                mDayScoreArcChartView.setTodayValue(dayActivity.getCalorieByActivity());
                mDayScoreArcChartView.setTodayUnit(this.getString(R.string.common_calorie), StrengthInputType.getStrengthType(appliedProgram.getCategory()));
                mDayScoreArcChartView.setTodayRange((int) appliedProgram.getStrengthFrom(), (int) appliedProgram.getStrengthTo());
            }else{
                mDayScoreArcChartView.setRangMax(appliedProgram.getMinuteTo());
                mDayScoreArcChartView.setTodayValue(dayActivity.getAchieveOfTarget());
                mDayScoreArcChartView.setTodayUnit(this.getString(R.string.common_minute), StrengthInputType.getStrengthType(appliedProgram.getCategory()));
                mDayScoreArcChartView.setTodayRange(appliedProgram.getMinuteFrom(), appliedProgram.getMinuteTo());
            }
            mDayScoreArcChartView.startAnimation(this.ANIMATION_DURATION);
            llScoreChart.addView(mDayScoreArcChartView);

            mDayMetLineChartView = new LineChartView(this);
            mDayMetLineChartView.setxAxisTextTitle(this.getString(R.string.activity_day_metchart_time));
            mDayMetLineChartView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

            mDayMetLineChartView.setValues(dayActivity.getMetArray());
            llAverageMetChart.addView(mDayMetLineChartView);


            activityChartView_1_1 = this.getLayoutInflater().inflate(R.layout.child_day_activity_fat_carbo_chart, null);
            this.tvFat = (TextView) activityChartView_1_1.findViewById(R.id.tv_child_day_activity_fat);
            this.tvCarbohydrate = (TextView) activityChartView_1_1.findViewById(R.id.tv_child_day_activity_carbohydrate);

            ContinuousCheckPolicy policy = ContinuousCheckPolicy.Loosely_SumOverXExercise;
            FitmateDBManager dbManager= new FitmateDBManager(this);
            UserInfo userInfo = dbManager.getUserInfo();
            UserInfoForAnalyzer userInfoForAnalyzer = com.fitdotlife.fitmate_lib.service.util.Utils.getUserInfoForAnalyzer( userInfo );
            com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseProgram programforAnalyzer = dbManager.getAppliedExerciseProgram();
            ExerciseAnalyzer analyzer = new ExerciseAnalyzer(programforAnalyzer, userInfoForAnalyzer, WearingLocation.WAIST , dayActivity.getDataSaveInterval());

            float[] metArray= dayActivity.getMetArray();
            List<Float> metList= new ArrayList<>();
            for(float f:metArray){
                metList.add(f);
            }
            FatAndCarbonhydrateConsumtion fatBurn = analyzer.getFatandCarbonhydrate(metList, dayActivity.getDataSaveInterval());
            float fat =fatBurn.getFatBurned();
            float carbon =fatBurn.getCarbonhydrateBurned();

            if( fat >= 100) {
                tvFat.setText(String.format("%d g", Math.round(fat)));
            }else{
                tvFat.setText(String.format("%.1f g", fat));
            }

            if( carbon >= 100 ) {
                tvCarbohydrate.setText(String.format("%d g", Math.round(carbon)));
            }else{
                tvCarbohydrate.setText(String.format("%.1f g", carbon));
            }

            activityChartView_2 = this.getLayoutInflater().inflate(R.layout.child_day_activity_metabolism_chart, null);
            LinearLayout CalorieChart24 = (LinearLayout) activityChartView_2.findViewById(R.id.ll_child_day_achievement_calorie_chart);

            //24시간 칼로리 소모량 그리는 차트
            //일일 칼로리 소모량 바 1개 그리는 차트
            mDayCalorieBarChartView = new DayCalorieBarChartView(this);
            mDayCalorieBarChartView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
            mDayCalorieBarChartView.setValue(dayActivity.getCalorieByActivity());
            mDayCalorieBarChartView.startAnimation(this.ANIMATION_DURATION);
            CalorieChart24.addView( mDayCalorieBarChartView );

            LinearLayout llCalorieChart = (LinearLayout) activityChartView_2.findViewById(R.id.ll_child_day_24calorie_chart);
            mDayHourCalorieBarChartView = new Day24HourCalorieChartView(this);
            mDayHourCalorieBarChartView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
            mDayHourCalorieBarChartView.setXAxisTitle(this.getString(R.string.activity_day_caloriechart_time));
            if( dayActivity.getMetArray() == null || dayActivity.getMetArray().length == 0  ){

            }else {
                int[] caloreArray = getDay24CalorieArray(dayActivity.getMetArray());

                mDayHourCalorieBarChartView.setValues(caloreArray);
                mDayHourCalorieBarChartView.startAnimation(this.ANIMATION_DURATION);
                llCalorieChart.addView(mDayHourCalorieBarChartView);
            }

            activityChartView_3 = this.getLayoutInflater().inflate(R.layout.child_day_activity_intensity_chart, null);
            LinearLayout llExerciseAverageChart = (LinearLayout) activityChartView_3.findViewById(R.id.ll_child_day_activity_intensity_average_exercisetime_chart);

            mDayTimeMetRoundRectangleChartView = new RoundedRectangleChartView(this);
            mDayTimeMetRoundRectangleChartView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
            mDayTimeMetRoundRectangleChartView.setExerciseTime(dayActivity.getStrengthHigh() + dayActivity.getStrengthMedium());
            mDayTimeMetRoundRectangleChartView.setAverageMET(((int) (dayActivity.getAverageMET() * 100d)) / 100d);
            mDayTimeMetRoundRectangleChartView.setLeftText(this.getString(R.string.activity_day_intensitytimechart_1), this.getString(R.string.activity_day_intensitytimechart_2));
            mDayTimeMetRoundRectangleChartView.setRightText(this.getString(R.string.activity_day_averagemetchart_1), this.getString(R.string.activity_day_averagemetchart_2));
            llExerciseAverageChart.addView(mDayTimeMetRoundRectangleChartView);

            LinearLayout llHMLIntensityChart = (LinearLayout) activityChartView_3.findViewById(R.id.ll_child_day_activity_intensity_strength_chart);
            mDayHmlIntensityBarChartView = new DayHMLIntensityBarChartView(this);
            mDayHmlIntensityBarChartView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
            int[] colors = new int[]{this.getResources().getColor(R.color.fitmate_blue), this.getResources().getColor(R.color.fitmate_cyan), this.getResources().getColor(R.color.fitmate_orange)};
            int[] values = new int[]{ dayActivity.getStrengthLow() , dayActivity.getStrengthMedium() , dayActivity.getStrengthHigh() };

            String[] arrYText = new String[]{
                    this.getString(R.string.activity_day_hmlchart_low),
                    this.getString(R.string.activity_day_hmlchart_medium),
                    this.getString(R.string.activity_day_hmlchart_high)
            };

            mDayHmlIntensityBarChartView.setYAxisTitle(this.getString(R.string.activity_day_hmlchart_intensity));
            mDayHmlIntensityBarChartView.setXAxisTitle(this.getString(R.string.activity_day_hmlchart_time));
            mDayHmlIntensityBarChartView.setYAxisTextList(arrYText);
            mDayHmlIntensityBarChartView.setValues(values);
            mDayHmlIntensityBarChartView.setColors(colors);
            mDayHmlIntensityBarChartView.startAnimation(this.ANIMATION_DURATION);
            llHMLIntensityChart.addView(mDayHmlIntensityBarChartView);

            this.llActivityListChart_1.removeAllViews();
            this.llActivityListChart_1.addView(activityChartView_1);

            this.llActivityListChart_1_1.removeAllViews();
            this.llActivityListChart_1_1.addView(activityChartView_1_1);

            this.llActivityListChart_2.removeAllViews();
            this.llActivityListChart_2.addView(activityChartView_2);

            this.llActivityListChart_3.removeAllViews();
            this.llActivityListChart_3.addView(activityChartView_3);

        }else{
            this.mDataStateType = DataStateType.NONE;
            this.llActivityListChart_1.removeAllViews();
            this.llActivityListChart_1_1.removeAllViews();
            this.llActivityListChart_2.removeAllViews();
            this.llActivityListChart_3.removeAllViews();
        }
    }

    @Override
    public void updateDayActivity(DayActivity dayActivity, int weekScore, ExerciseProgram appliedProgram) {

        if( dayActivity != null ){
            mDayScoreArcChartView.setWeekValue(weekScore);

            StrengthInputType strengthType = StrengthInputType.getStrengthType( appliedProgram.getCategory() );
            if( strengthType.equals( StrengthInputType.CALORIE ) || strengthType.equals( StrengthInputType.CALORIE_SUM) || strengthType.equals(StrengthInputType.VS_BMR) ) {

                mDayScoreArcChartView.setTodayValue(dayActivity.getCalorieByActivity());
            }else{

                mDayScoreArcChartView.setTodayValue(dayActivity.getAchieveOfTarget());
            }
            mDayScoreArcChartView.startAnimation(this.ANIMATION_DURATION);

            mDayMetLineChartView.setValues(dayActivity.getMetArray());
            mDayMetLineChartView.invalidate();

            //지방과 탄수화물
            ContinuousCheckPolicy policy = ContinuousCheckPolicy.Loosely_SumOverXExercise;
            FitmateDBManager dbManager= new FitmateDBManager(this);
            UserInfo userInfo = dbManager.getUserInfo();
            UserInfoForAnalyzer userInfoForAnalyzer = com.fitdotlife.fitmate_lib.service.util.Utils.getUserInfoForAnalyzer(userInfo);

            com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseProgram programforAnalyzer = dbManager.getAppliedExerciseProgram();
            ExerciseAnalyzer analyzer = new ExerciseAnalyzer(programforAnalyzer, userInfoForAnalyzer, WearingLocation.WAIST , dayActivity.getDataSaveInterval());


            float[] metArray= dayActivity.getMetArray();
            List<Float> metList= new ArrayList<>();
            for(float f:metArray){
                metList.add(f);
            }

            FatAndCarbonhydrateConsumtion fatBurn = analyzer.getFatandCarbonhydrate(metList, dayActivity.getDataSaveInterval());
            float fat =fatBurn.getFatBurned();
            float carbon =fatBurn.getCarbonhydrateBurned();

            if( fat >= 100) {
                tvFat.setText(String.format("%d g", Math.round(fat)));
            }else{
                tvFat.setText(String.format("%.1f g", fat));
            }

            if( carbon >= 100 ) {
                tvCarbohydrate.setText(String.format("%d g", Math.round(carbon)));
            }else{
                tvCarbohydrate.setText(String.format("%.1f g", carbon));
            }

            //24시간 칼로리 소모량 그리는 차트
            //일일 칼로리 소모량 바 1개 그리는 차트
            mDayCalorieBarChartView.setValue(dayActivity.getCalorieByActivity());
            mDayCalorieBarChartView.startAnimation(this.ANIMATION_DURATION);

            if( dayActivity.getMetArray() == null || dayActivity.getMetArray().length == 0  ){

            }else {
                int[] caloreArray = getDay24CalorieArray(dayActivity.getMetArray());

                mDayHourCalorieBarChartView.setValues(caloreArray);
                mDayHourCalorieBarChartView.startAnimation(this.ANIMATION_DURATION);
            }

            mDayTimeMetRoundRectangleChartView.setExerciseTime(dayActivity.getStrengthHigh() + dayActivity.getStrengthMedium());
            mDayTimeMetRoundRectangleChartView.setAverageMET(((int) (dayActivity.getAverageMET() * 100d)) / 100d);
            mDayTimeMetRoundRectangleChartView.invalidate();

            int[] values = new int[]{ dayActivity.getStrengthLow() , dayActivity.getStrengthMedium() , dayActivity.getStrengthHigh() };
            mDayHmlIntensityBarChartView.setValues(values);
            mDayHmlIntensityBarChartView.startAnimation(this.ANIMATION_DURATION);
        }

    }

    @Override
    public void displayWeekActivity( WeekActivity weekActivity , int[] arrExerciseTime , int[] arrCalorie ,  String[] arrDayDate , int year, int month, int day, int weekNumber, ExerciseProgram appliedProgram)
    {

        View moveToPre= findViewById(R.id.tv_activity_main_pre_date);
        moveToPre.setEnabled(true);
        View moveToNext= findViewById(R.id.tv_activity_main_next_date);
        moveToNext.setEnabled(true);
        String programName = null;
        if(appliedProgram.getDefaultProgram())
        {
            int resId =  getResources().getIdentifier(appliedProgram.getName(), "string", getPackageName());
            programName = getString(resId);
        }
        else
        {
            programName = appliedProgram.getName();
        }


        this.tvAppliedProgram.setText(programName);
        this.tvAppliedProgram.setTag(appliedProgram.getId());

        this.btnSelectDay.setTextColor(0xFFA9A9A9);
        this.btnSelectWeek.setTextColor(Color.BLACK);
        this.btnSelectMonth.setTextColor(0xFFA9A9A9);

        this.llSelectDayMark.setBackgroundColor(Color.WHITE);
        this.llSelectWeekMark.setBackgroundColor(this.getResources().getColor(R.color.fitmate_orange));
        this.llSelectMonthMark.setBackgroundColor(Color.WHITE);


        //    SimpleDateFormat format3 =new SimpleDateFormat("EEE, MMM yyyy");

        Calendar calendar =Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(calendar.MONTH, month);
        calendar.set(calendar.DATE, day);


        Locale current = getResources().getConfiguration().locale;
        //this.tvActivityDate.setText(NewHomeUtils.toWeekString(weekNumber, calendar, current));
        this.tvActivityDate.setText( String.format(
                this.getString(R.string.activity_week_date_string_format),
                DateFormat.format(this.getString(R.string.activity_week_date_format), calendar.getTime()) , this.getResources().getStringArray(R.array.week_ordinal)[ weekNumber - 1 ] ));

        if(DateUtils.isThisWeek(calendar)){
            setVisible_NextButton(false);
        }else{
            setVisible_NextButton(true);
        }

        //�쒕룞��紐⑸줉
        this.imgActivityList_1.setImageResource( R.drawable.icon_medal );
        this.tvActivityListTitle_1.setText(this.getResources().getString(R.string.activity_week_achievement) );

        this.llActivityListChart_1_1.setVisibility(View.GONE);
        this.llActivityList_1_1.setVisibility(View.GONE);
        this.llLine_1_1.setVisibility(View.GONE);

        this.imgActivityList_2.setImageResource( R.drawable.icon_chart );
        this.tvActivityListTitle_2.setText(this.getResources().getString(R.string.activity_week_calorie_consumption) );

        this.llActivityListChart_3.setVisibility(View.GONE);
        this.llActivityList_3.setVisibility(View.GONE);

        if( weekActivity != null ) {

            this.mDataStateType = DataStateType.AVAILABLE;
            activityChartView_1 = this.getLayoutInflater().inflate(R.layout.child_daily_achievement_chart, null);
            LinearLayout llScoreBarChart = (LinearLayout) activityChartView_1.findViewById(R.id.ll_child_daily_achievement_bar_chart);

            mWeekExerciseTimeBarChartView = new WeekExerciseTimeBarChartView(this);
            mWeekExerciseTimeBarChartView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
            int[] values;

            String[] arrYText = this.getResources().getStringArray(R.array.week_char);
            for( int i = 0 ; i < arrDayDate.length ; i++ ){
                arrDayDate[i] = DateUtils.getDateStringForPattern(arrDayDate[i], this.getString(R.string.activity_week_day_date_format));
            }

            if(appliedProgram.getCategory() == StrengthInputType.CALORIE.Value || appliedProgram.getCategory() == StrengthInputType.CALORIE_SUM.Value) {
                mWeekExerciseTimeBarChartView.setYAxisTitle(this.getString(R.string.common_calorie));
                values = arrCalorie;
                mWeekExerciseTimeBarChartView.setValues(values);
                mWeekExerciseTimeBarChartView.setRange((int) appliedProgram.getStrengthFrom(), (int) appliedProgram.getStrengthTo());
            } else if( appliedProgram.getCategory() == StrengthInputType.VS_BMR.Value ){
                FitmateDBManager dbManager = new FitmateDBManager(this);
                UserInfo userInfo = dbManager.getUserInfo();
                UserInfoForAnalyzer userInfoForAnalyzer = com.fitdotlife.fitmate_lib.service.util.Utils.getUserInfoForAnalyzer(userInfo);
                int bmr = userInfoForAnalyzer.getBMR();

                mWeekExerciseTimeBarChartView.setYAxisTitle(this.getString(R.string.common_calorie));
                values = arrCalorie;
                mWeekExerciseTimeBarChartView.setValues(values);
                mWeekExerciseTimeBarChartView.setRange( (int) (bmr * (appliedProgram.getStrengthFrom() / 100)) , (int) (bmr * (appliedProgram.getStrengthTo() / 100)));

            } else{
                mWeekExerciseTimeBarChartView.setYAxisTitle(this.getString(R.string.common_minute));
                values = arrExerciseTime;
                mWeekExerciseTimeBarChartView.setValues(values);
                mWeekExerciseTimeBarChartView.setRange((int) appliedProgram.getMinuteFrom() , (int) appliedProgram.getMinuteTo() );
            }

            mWeekExerciseTimeBarChartView.setXAxisDateList(arrDayDate);
            mWeekExerciseTimeBarChartView.setXAxisTextList(arrYText);



            mWeekExerciseTimeBarChartView.startAnimation(this.ANIMATION_DURATION);
            llScoreBarChart.addView(mWeekExerciseTimeBarChartView);

            LinearLayout llOvalChart = (LinearLayout) activityChartView_1.findViewById(R.id.ll_child_daily_achievement_oval_chart);
            mWeekPointOvalChartView = new WeekPointView(this);
            mWeekPointOvalChartView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
            mWeekPointOvalChartView.setColor(this.getResources().getColor(R.color.fitmate_cyan));
            mWeekPointOvalChartView.setPoint(weekActivity.getScore());
            llOvalChart.addView(mWeekPointOvalChartView);

            this.activityChartView_2 = this.getLayoutInflater().inflate(R.layout.child_daily_activity_metabolism_chart, null);
            LinearLayout llCalorieChart = (LinearLayout) activityChartView_2.findViewById(R.id.ll_child_daily_activity_calorie_bar_chart);
            mWeekCalorieBarChartView = new WeekCalorieBarChartView(this);
            mWeekCalorieBarChartView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
            int[] calorieArray = arrCalorie;
            mWeekCalorieBarChartView.setYAxisTitle(this.getString(R.string.common_calorie));
            mWeekCalorieBarChartView.setXAxisTextList(arrYText);
            mWeekCalorieBarChartView.setXAxisDateList(arrDayDate);
            mWeekCalorieBarChartView.setValues(calorieArray);

            mWeekCalorieBarChartView.startAnimation(ANIMATION_DURATION);
            llCalorieChart.addView(mWeekCalorieBarChartView);

            this.llActivityListChart_1.removeAllViews();
            this.llActivityListChart_1.addView(activityChartView_1);

            this.llActivityListChart_2.removeAllViews();
            this.llActivityListChart_2.addView(activityChartView_2);

        }else{
            this.mDataStateType = DataStateType.NONE;
            this.llActivityListChart_1.removeAllViews();
            this.llActivityListChart_2.removeAllViews();
        }
    }

    @Override
    public void updateWeekActivity(WeekActivity weekActivity, int[] arrExerciseTime, int[] arrCalorie, ExerciseProgram appliedProgram) {
        if( weekActivity != null ){

                int[] values;
                if(appliedProgram.getCategory() == StrengthInputType.CALORIE.Value || appliedProgram.getCategory() == StrengthInputType.CALORIE_SUM.Value || appliedProgram.getCategory() == StrengthInputType.VS_BMR.Value) {
                    values = arrCalorie;
                }else{
                    values = arrExerciseTime;
                }

                mWeekExerciseTimeBarChartView.setValues(values);
                mWeekExerciseTimeBarChartView.startAnimation(this.ANIMATION_DURATION);

                mWeekPointOvalChartView.setPoint(weekActivity.getScore());
                mWeekPointOvalChartView.invalidate();

                int[] calorieArray = arrCalorie;
                mWeekCalorieBarChartView.setValues(calorieArray);
                mWeekCalorieBarChartView.startAnimation(ANIMATION_DURATION);
        }
    }

    @Override
    public void displayMonthActivity( MonthActivity monthActivity , int year , int month , int day, ExerciseProgram appliedProgram)
    {

        View moveToPre= findViewById(R.id.tv_activity_main_pre_date);
        moveToPre.setEnabled(true);
        View moveToNext= findViewById(R.id.tv_activity_main_next_date);
        moveToNext.setEnabled(true);
        String programName = null;
        if(appliedProgram.getDefaultProgram())
        {
            int resId =  getResources().getIdentifier(appliedProgram.getName(), "string", getPackageName());
            programName = getString(resId);
        }
        else
        {
            programName = appliedProgram.getName();
        }

        this.tvAppliedProgram.setText(programName);
        this.tvAppliedProgram.setTag(appliedProgram.getId());

        this.btnSelectDay.setTextColor(0xFFA9A9A9);
        this.btnSelectWeek.setTextColor(0xFFA9A9A9);
        this.btnSelectMonth.setTextColor(Color.BLACK);

        this.llSelectDayMark.setBackgroundColor(Color.WHITE);
        this.llSelectWeekMark.setBackgroundColor(Color.WHITE);
        this.llSelectMonthMark.setBackgroundColor(this.getResources().getColor(R.color.fitmate_orange));

        Calendar calendar =Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(calendar.MONTH, month);
        calendar.set(calendar.DATE, day);


        Locale current = getResources().getConfiguration().locale;
        this.tvActivityDate.setText(Utils.toMonthString(calendar, current));

        if(DateUtils.isThisMonth(calendar)){
            setVisible_NextButton(false);
        }else{
            setVisible_NextButton(true);
        }
        //   this.tvActivityDate.setText( this.getMonthChar(month) + " " +  year );

        this.imgActivityList_1.setImageResource( R.drawable.icon_medal );
        this.tvActivityListTitle_1.setText( this.getString(R.string.activity_month_achievement) );

        this.llActivityListChart_1_1.setVisibility(View.GONE);
        this.llActivityList_1_1.setVisibility(View.GONE);
        this.llLine_1_1.setVisibility(View.GONE);

        this.imgActivityList_2.setImageResource( R.drawable.icon_chart );
        this.tvActivityListTitle_2.setText( this.getString(R.string.activity_month_calorie_consumption) );

        this.llActivityListChart_3.setVisibility(View.GONE);
        this.llActivityList_3.setVisibility(View.GONE);

        if(monthActivity != null) {

            this.mDataStateType = DataStateType.AVAILABLE;
            activityChartView_1 = this.getLayoutInflater().inflate(R.layout.child_monthly_achievement_chart, null);
            LinearLayout llScoreBarChart = (LinearLayout) activityChartView_1.findViewById(R.id.ll_child_monthly_achievement_bar_chart);

            mMonthlyScoreBarChartView = new MonthPointBarChartView(this);
            mMonthlyScoreBarChartView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
            int[] scoreValues = monthActivity.getScoreList();
            String[] arrXText = new String[ scoreValues.length ];

            for( int i = 0 ; i < scoreValues.length ;i++ ){
                arrXText[i] = this.getWeekNumberChar( i + 1 );
            }

            mMonthlyScoreBarChartView.setYAxisTitle(this.getString(R.string.common_score));
            mMonthlyScoreBarChartView.setXAxisTextList(arrXText);
            mMonthlyScoreBarChartView.setValues(scoreValues);
            mMonthlyScoreBarChartView.startAnimation(this.ANIMATION_DURATION);
            llScoreBarChart.addView(mMonthlyScoreBarChartView);

            LinearLayout llOvalChart = (LinearLayout) activityChartView_1.findViewById(R.id.ll_child_monthly_achievement_oval_chart);
            mMonthPointChartView = new MonthPointView(this);
            mMonthPointChartView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
            mMonthPointChartView.setColor(this.getResources().getColor(R.color.fitmate_orange));
            mMonthPointChartView.setPoint(monthActivity.getAverageScore());
            llOvalChart.addView(mMonthPointChartView);

            this.activityChartView_2 = this.getLayoutInflater().inflate(R.layout.child_monthly_activity_metabolism_chart, null);
            LinearLayout llMetabolismBarChart = (LinearLayout) this.activityChartView_2.findViewById(R.id.ll_child_monthly_activity_metabolism_bar_chart);

            mMonthlyCalorieBarChartView = new MonthCalorieBarChartView(this);
            mMonthlyCalorieBarChartView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
            int[] metaValues = monthActivity.getCalorieList();
            mMonthlyCalorieBarChartView.setYAxisTitle(this.getString(R.string.common_calorie));
            mMonthlyCalorieBarChartView.setXAxisTextList(arrXText);
            mMonthlyCalorieBarChartView.setValues(metaValues);

            mMonthlyCalorieBarChartView.startAnimation(this.ANIMATION_DURATION);
            llMetabolismBarChart.addView(mMonthlyCalorieBarChartView);

            this.llActivityListChart_1.removeAllViews();
            this.llActivityListChart_1.addView(activityChartView_1);

            this.llActivityListChart_2.removeAllViews();
            this.llActivityListChart_2.addView(activityChartView_2);

        }else{
            this.mDataStateType = DataStateType.NONE;
            this.llActivityListChart_1.removeAllViews();
            this.llActivityListChart_2.removeAllViews();
        }

    }

    @Override
    public void updateMonthActivity(MonthActivity monthActivity, ExerciseProgram appliedProgram) {
        if( monthActivity != null ){

            int[] scoreValues = monthActivity.getScoreList();
            mMonthlyScoreBarChartView.setValues(scoreValues);
            mMonthlyScoreBarChartView.startAnimation(this.ANIMATION_DURATION);

            mMonthPointChartView.setPoint(monthActivity.getAverageScore());
            mMonthPointChartView.invalidate();

            int[] metaValues = monthActivity.getCalorieList();
            mMonthlyCalorieBarChartView.setValues(metaValues);
            mMonthlyCalorieBarChartView.startAnimation(this.ANIMATION_DURATION);
        }
    }

    private String getWeekNumberChar( int weekNumber )
    {
        return Utils.toWeekString(weekNumber,  getResources().getConfiguration().locale);

        /*String weekNumberString = "";

        if( weekNumber == 1 )
        {
            weekNumberString = weekNumber + "st";
        }
        else if( weekNumber == 2 )
        {
            weekNumberString = weekNumber + "nd";
        }
        else if( weekNumber == 3 )
        {
            weekNumberString = weekNumber + "rd";
        }
        else
        {
            weekNumberString = weekNumber + "th";
        }

        return weekNumberString;*/
    }

    @Override
    public void showResult(String message)
    {
        if( isShowMessage ) {
            Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        }
    }

//    @Override
//    public void startProgressDialog(){
//        this.mDialog =  new ProgressDialog(this);
//        mDialog.setTitle("데이터 분석");
//        mDialog.setMessage("데이터 분석중입니다...");
//        mDialog.setIndeterminate( true );
//        mDialog.setCancelable( false );
//        mDialog.show();
//    }
//
//    @Override
//    public void stopProgressDialog(){
//        if( this.mDialog != null ) {
//            this.mDialog.dismiss();
//        }
//    }

    @Override
    public void moveOtherActivity(Intent intent) {
        this.startActivity(intent);
    }

    private int[] getDay24CalorieArray(float[] metarray) {

        FitmateDBManager dbManager = new FitmateDBManager(this);

        float weight =  dbManager.getUserInfo().getWeight();
        int[] result = new int[24];

        int datasavinginterval= 86400 / metarray.length;

        int count=60*60/datasavinginterval;
        int calsum=0;



        for(int i=0;i<metarray.length;i+=count)
        {
            for(int j=0;j<count;j++)
            {
                if(metarray[i+j]>1) {
                    calsum += ExerciseAnalyzer.Calculate_CalorieFromMET(datasavinginterval, weight, metarray[i+j]);
                }
            }
            result[i/count]= Math.round(calsum/1000.f);
            calsum=0;
        }
        return result;
    }

    @Override
    public void onBackPressed() {

        if( dlMain.isDrawerOpen( drawerView )){
            dlMain.closeDrawers();
            return;
        }

        if(isNews) {
            finish();
            return;
        }


//        AlertDialog.Builder builder = new AlertDialog.Builder(this);     // 여기서 this는 Activity의 this
//
//        // 여기서 부터는 알림창의 속성 설정
//        builder.setTitle(getString(R.string.common_quit_title))        // 제목 설정
//                .setMessage(getString(R.string.common_quit_content))        // 메세지 설정
//                .setCancelable(true)        // 뒤로 버튼 클릭시 취소 가능 설정
//                .setPositiveButton(R.string.common_yes, new DialogInterface.OnClickListener() {
//                    // 확인 버튼 클릭시 설정
//                    public void onClick(DialogInterface dialog, int whichButton) {
//                        moveTaskToBack(true);
//
//                        finish();
//
//                        //  android.os.Process.killProcess(android.os.Process.myPid());
//                        dialog.cancel();
//
//                        //   MainActivity.this.onBackPressed();
//                    }
//                })
//                .setNegativeButton(R.string.common_no, new DialogInterface.OnClickListener() {
//                    // 취소 버튼 클릭시 설정
//                    public void onClick(DialogInterface dialog, int whichButton) {
//                        dialog.cancel();
//                    }
//                });
//
//        AlertDialog dialog = builder.create();    // 알림창 객체 생성
//        dialog.show();    // 알림창 띄우기

        ActivityManager.getInstance().finishAllActivity();
        showNewHomeActivity();
    }



    @Override
    protected void onStart() {
        super.onStart();

        this.isShowMessage = true;
        if(!Preprocess.IsTest) {
            GoogleAnalytics.getInstance(this).reportActivityStart(this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        this.isShowMessage = false;

        if(!Preprocess.IsTest) {
            GoogleAnalytics.getInstance(this).reportActivityStop(this);
        }
    }

    private boolean getSettedUserInfo()
    {
        SharedPreferences pref = this.getSharedPreferences("fitmateservice", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        return pref.getBoolean( LoginPresenter.SET_USERINFO_KEY , false);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == DrawerView.REQUEST_PROFILE_IMAGE){
            this.drawerView.refreshProfileImage();
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        ImageView actionbarImageView = (ImageView) view;
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            actionbarImageView.setColorFilter(this.getResources().getColor(R.color.selected_colorfilter) , PorterDuff.Mode.DARKEN );
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            actionbarImageView.setColorFilter(null);
        }

        return false;
    }

    //백버튼을 누르거나 액션바에 홈 버튼을 누를 때 홈 화면을 띄운다.
    private void showNewHomeActivity(){
        Intent intent = new Intent();

        if(getSettedUserInfo()) {
            intent.setClass( this , NewHomeActivity_.class );
            intent.putExtra(NewHomeActivity.HOME_LAUNCH_KEY, NewHomeActivity.LaunchMode.FROM_OTHERMENU.ordinal());
        }else{
            intent.setClass( this , NewHomeAsActivity_.class );
        }

        startActivity( intent );
    }

}