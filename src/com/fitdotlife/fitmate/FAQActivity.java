package com.fitdotlife.fitmate;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;


public class FAQActivity extends Activity implements View.OnClickListener {

    private WebView wvFAQ = null;
    private ImageView imgClose = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);

        this.wvFAQ = (WebView) this.findViewById(R.id.wv_faq);
        this.wvFAQ.getSettings().setJavaScriptEnabled(true);
        this.wvFAQ.setWebViewClient(new WebViewClient());
        String htmlPath = "file:///android_asset/faq-" + this.getString(R.string.local_prefix) + ".html";
        this.wvFAQ.loadUrl(htmlPath);

        this.imgClose = (ImageView) this.findViewById(R.id.img_activity_faq_close);
        this.imgClose.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch( view.getId() ){
            case R.id.img_activity_faq_close:
                this.finish();
                break;
        }
    }
}
