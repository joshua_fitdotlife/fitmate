package com.fitdotlife.fitmate;

import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.WearingLocation;
import com.fitdotlife.fitmate_lib.service.bluetooth.FitmeterDeviceDriver;
import com.fitdotlife.fitmate_lib.service.protocol.ProtocolManager;
import com.fitdotlife.fitmate_lib.service.protocol.object.SerialCode;
import com.fitdotlife.fitmate_lib.service.protocol.object.SystemInfo_Request;
import com.fitdotlife.fitmate_lib.service.protocol.object.SystemInfo_Response;

import org.apache.log4j.Log;

import java.util.Hashtable;


public class LocationChangeActivity extends Activity implements View.OnClickListener  {

    private String TAG = "fitmate - " + LocationChangeActivity.class.getSimpleName();
    private static final int ENABLE_BT_REQUEST_ID = 1;
    private final int RSSI_VALUE = -60;
    private final int RSSI_VALUE_UNDER = -35;
    private final String FITMETER_NAME ="FITMETERBLE";

    public final String INTENT_KEY = "KEY";
    public static final String INTENT_LOCATION = "LOCATION";
    public static final String INTENT_BTADDRESS = "ADDRESS";

    private LinearLayout llMarkContainer = null;
    private LayoutInflater inflater = null;
    private ScrollView scrContainer = null;
    private ImageView imgClose = null;
    private ImageView imgBack = null;

    private Button btnNext = null;

    private int mStep = -1;
    private View[] mChildViewList = null;
    private View[] mMarkViewList = null;

    private Handler mHandler = new Handler();

    private TextView tvScanTime = null;

    private int mTimerValue = 0;

    private ProtocolManager mProtocolManager = null;
    String BTAddress = "";


    class FindFitmeter implements BluetoothAdapter.LeScanCallback{

        boolean overrideRssi = false;
        private Hashtable<String, BluetoothDevice> mDeviceList = new Hashtable<String,BluetoothDevice>();
        @Override
        public void onLeScan(final BluetoothDevice device, final int rssi, byte[] scanRecord) {
            String deviceName = device.getName();
            if (deviceName != null) {
                if( deviceName.equals(FITMETER_NAME) ) {
                    if (rssi < RSSI_VALUE_UNDER && rssi > RSSI_VALUE || overrideRssi) {
                        if (!mDeviceList.contains(device)) {
                            mDeviceList.put(device.getAddress(), device);
                        }
                    }
                }
            }
        }

        private BluetoothManager mBluetoothManager = null;
        private BluetoothAdapter mBluetoothAdapter = null;

        public FindFitmeter()
        {
            this.mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            this.mBluetoothAdapter = this.mBluetoothManager.getAdapter();

        }

        private boolean startScan(String Address, int msec_100, boolean overrideRssi)
        {
            this.overrideRssi = overrideRssi;
            //this.addScaningTimeout();
            this.mDeviceList.clear();
            this.mBluetoothAdapter.startLeScan(this);

            for(int i = 0; i < msec_100; i++)
            {
                if(mDeviceList.containsKey(Address))
                {
                    Log.d("FindFitmeter", "장치 검색 완료");
                    finalize();
                    return true;
                }
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            Log.d("FindFitmeter", "장치 검색 실패");
            finalize();
            return false;
        }

        protected void finalize()
        {
            mBluetoothAdapter.stopLeScan(this);
        }
    }


    private ImageView relativeLayout_wearing;

    private TextView selectedTextView_wrist;
    private TextView selectedTextView_waist;
    private TextView selectedTextView_upperarm;
    private TextView selectedTextView_panspocket;
    private TextView selectedTextView_ankle;

    private WearingLocation wearAt= WearingLocation.WAIST;

    public void SetWear(WearingLocation wear)
    {
        wearAt = wear;

        switch(wear)
        {
            case WAIST:
                relativeLayout_wearing.setImageResource(R.drawable.wear5);
                break;
            case UPPERARM:
                relativeLayout_wearing.setImageResource(R.drawable.wear2);
                break;
            case PANTSPOCKET:
                relativeLayout_wearing.setImageResource(R.drawable.wear4);
                break;
            case WRIST:
                relativeLayout_wearing.setImageResource(R.drawable.wear1);
                break;
            case ANKLE:
                relativeLayout_wearing.setImageResource(R.drawable.wear3);
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_change);

        this.btnNext = (Button) this.findViewById(R.id.btn_activity_device_change_next);
        this.btnNext.setOnClickListener(this);

        this.imgBack = (ImageView) this.findViewById(R.id.img_activity_device_change_back);
        this.imgBack.setOnClickListener(this);
        this.imgClose = (ImageView) this.findViewById(R.id.img_activity_device_change_close);
        this.imgClose.setOnClickListener(this);

        this.inflater =  this.getLayoutInflater();
        this.llMarkContainer = (LinearLayout) this.findViewById(R.id.ll_activity_device_change_step_mark );
        this.scrContainer = (ScrollView) this.findViewById(R.id.scr_activity_device_change_container );

        this.mChildViewList = new View[4];
        this.mMarkViewList = new View[4];


        this.mChildViewList[0] = this.inflater.inflate( R.layout.child_device_change_step_4 , this.scrContainer , false);

        int changePosition = 0;


        this.relativeLayout_wearing =(ImageView) this.mChildViewList[changePosition].findViewById(R.id.relativelay_wearing);

        selectedTextView_waist= (TextView) this.mChildViewList[changePosition].findViewById(R.id.wear_waist);
        selectedTextView_waist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wearAt= WearingLocation.WAIST;

                //     recycleView(relativeLayout_wearing);
                relativeLayout_wearing.setImageResource(R.drawable.wear5);
                //  relativeLayout_wearing.setBackground(new BitmapDrawable(getResources(), BitmapFactory.decodeResource(getResources(), R.drawable.wear5)));
            }
        });
        selectedTextView_upperarm= (TextView) this.mChildViewList[changePosition].findViewById(R.id.wear_upperarm);
        selectedTextView_upperarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                wearAt= WearingLocation.UPPERARM;
                //  relativeLayout_wearing.setBackground(getResources().getDrawable(R.drawable.wear2));

                //    recycleView(relativeLayout_wearing);
                relativeLayout_wearing.setImageResource( R.drawable.wear2);
                //  relativeLayout_wearing.setBackground(new BitmapDrawable(getResources(), BitmapFactory.decodeResource(getResources(), R.drawable.wear2)));
            }
        });
        selectedTextView_panspocket= (TextView) this.mChildViewList[changePosition].findViewById(R.id.wear_pantspocket);
        selectedTextView_panspocket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wearAt= WearingLocation.PANTSPOCKET;
                //relativeLayout_wearing.setBackground(getResources().getDrawable(R.drawable.wear4));
                //    recycleView(relativeLayout_wearing);
                relativeLayout_wearing.setImageResource(R.drawable.wear4);
                //relativeLayout_wearing.setBackground(new BitmapDrawable(getResources(), BitmapFactory.decodeResource(getResources(), R.drawable.wear4)));
            }
        });
        selectedTextView_wrist= (TextView) this.mChildViewList[changePosition].findViewById(R.id.wear_wrist);
        selectedTextView_wrist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wearAt= WearingLocation.WRIST;
                //      relativeLayout_wearing.setBackground(getResources().getDrawable(R.drawable.wear1));
                //    recycleView(relativeLayout_wearing);
                //    relativeLayout_wearing.setBackground(new BitmapDrawable(getResources(), BitmapFactory.decodeResource(getResources(), R.drawable.wear1)));
                relativeLayout_wearing.setImageResource(R.drawable.wear1);
            }
        });
        selectedTextView_ankle= (TextView) this.mChildViewList[changePosition].findViewById(R.id.wear_ankle);
        selectedTextView_ankle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wearAt= WearingLocation.ANKLE;
                //   recycleView(relativeLayout_wearing);
                //  relativeLayout_wearing.setBackground(new BitmapDrawable(getResources(), BitmapFactory.decodeResource(getResources(), R.drawable.wear3)));
                relativeLayout_wearing.setImageResource(R.drawable.wear3);
            }
        });

        Intent intent = getIntent();
        if(intent != null)
        {
            int loc = intent.getIntExtra(INTENT_LOCATION, 0);
            BTAddress = intent.getStringExtra(INTENT_BTADDRESS);
            SetWear(WearingLocation.getWearingLocation(loc));
        }

        final Activity mCon = this;

        final FindFitmeter ff = new FindFitmeter();
        startProgressDialog( this.getString(R.string.myprofile_change_location_connection_title) ,  this.getString(R.string.myprofile_change_location_connection_content)  );
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                Log.d("FindFitmeter", "장치 검색 시작");
                if(!ff.startScan(BTAddress, 50, true))    // 3초 검색
                {
                    Handler mHandler = new Handler(Looper.getMainLooper());
                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), getString(R.string.myprofile_change_location_connect_request), Toast.LENGTH_SHORT).show();
                            mCon.finish();
                        }
                    }, 0);
                }
                stopProgressDialog();
            }
        }).start();

        this.moveFrontView();
    }

    private void moveFrontView()
    {
        this.mStep += 1;
        drawChildView();
    }

    private void moveBackView()
    {
        if( mStep == 1 ){
            this.btnNext.setEnabled(true);
            this.btnNext.setBackgroundColor(this.getResources().getColor(R.color.fitmate_green));
        }

        if( mStep == 2 ) {
            this.mStep -= 2;
            this.btnNext.setText(this.getString(R.string.common_next));
        }else {
            this.mStep -= 1;
        }
        drawChildView();

    }

    private void drawChildView() {

        if( mStep == -1 ){
            this.finish();
            return;
        }

        this.scrContainer.removeAllViews();
        this.llMarkContainer.removeAllViews();

        if(this.mStep == 1)
        {
            return;
        }

        this.scrContainer.addView( this.mChildViewList[ mStep ] );


        /*
        if( mStep == 1 ){
            this.btnNext.setEnabled(true);
            this.btnNext.setBackgroundColor( this.getResources().getColor(R.color.fitmate_green) );
        }
        */

        if( this.mStep == 0 )
        {
            this.btnNext.setEnabled(true);
            this.btnNext.setBackgroundColor( this.getResources().getColor(R.color.fitmate_green) );

            this.btnNext.setText(this.getString(R.string.common_finish).toUpperCase());

            this.imgClose.setVisibility(View.INVISIBLE);
            this.imgBack.setVisibility(View.INVISIBLE);
        }

    }


    @Override
    public void onClick(View view) {
        switch( view.getId() ){
            case R.id.btn_activity_device_change_next:
                if( mStep == 0 ){

                    startProgressDialog(this.getString(R.string.myprofile_change_location)  , this.getString(R.string.myprofile_changing_location));

                    new Thread(new Runnable() {
                        @Override
                        public void run() {

                            Context mContext = getBaseContext();
                            boolean isSuccess = false;
                            for(int i = 0; i< 3; i++)
                            {
                                FitmeterDeviceDriver mDriver = FitmeterDeviceDriver.ConnectionOnce(mContext, BTAddress);

                                if(mDriver != null)
                                {
                                    try
                                    {
                                        // todo 여기에 데이터 파일 지우기 구현 필요
                                        ProtocolManager protocolManager = new ProtocolManager( mContext , mDriver );

                                        SystemInfo_Response sr = protocolManager.getSystemInfo();

                                        SerialCode sc =sr.getSerialCode();

                                        SystemInfo_Request dd = sr.getSystemInfo();
                                        dd.setWearingPosition(wearAt.Value);

                                        boolean setresult = protocolManager.setSystemInfo(dd);
                                        if(setresult)
                                        {
                                            Log.d("PROTOCOL", "SETOK");
                                        }

                                        isSuccess = true;

                                    }
                                    catch (Exception ex)
                                    {
                                        Log.d("DATACOMM", ex.getMessage());
                                    }
                                    finally {
                                        mDriver.disconnect();
                                        try {
                                            Thread.sleep(100);
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                        mDriver.close();
                                    }

                                    break;
                                }
                                try {
                                    Thread.sleep(100);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }

                            stopProgressDialog();

                            if(isSuccess) {
                                Intent intent = new Intent();
                                intent.putExtra(INTENT_LOCATION, wearAt.Value);
                                setResult(Activity.RESULT_OK, intent);
                                Handler mHandler = new Handler(Looper.getMainLooper());
                                mHandler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getApplicationContext(), getString(R.string.myprofile_change_location_success), Toast.LENGTH_SHORT).show();
                                    }
                                }, 0);
                                finish();
                            }
                            else
                            {
                                Handler mHandler = new Handler(Looper.getMainLooper());
                                mHandler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getApplicationContext(), getString(R.string.myprofile_change_location_connection_title), Toast.LENGTH_SHORT).show();
                                    }},0);
                            }

                        }
                    }).start();



                    return;
                }
                this.moveFrontView();
                break;
            case R.id.img_activity_device_change_back:
                this.moveBackView();
                break;

            case R.id.img_activity_device_change_close:
                if( mStep ==0 ) {

                }
                this.finish();
                break;

        }
    }


    ProgressDialog mDialog;
    public void startProgressDialog( String title , String message ){
        this.mDialog =  new ProgressDialog(this);
        mDialog.setTitle(title);
        mDialog.setMessage(message);
        mDialog.setIndeterminate(true);
        mDialog.setCancelable(false);
        mDialog.show();
    }

    public void stopProgressDialog(){
        if( this.mDialog != null ) {
            this.mDialog.dismiss();
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        this.moveBackView();
    }
}
