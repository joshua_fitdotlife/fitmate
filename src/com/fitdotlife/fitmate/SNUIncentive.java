package com.fitdotlife.fitmate;

import android.os.Parcel;
import android.os.Parcelable;

import com.fitdotlife.fitmate.model.SNU_EARN;
import com.fitdotlife.fitmate.model.WeightAchieve;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class SNUIncentive implements Parcelable, Serializable
{
    public Calendar startdate;
    public int goal_1;
    public int goal_2;
    public int goal_3;
    public int[] CalorieList;

    public static final String KEY="SNUIncentive";

    public static final int earnPerDay=1000;
    public static final int completeBonus= 3000;
    public static final int weightGoalPrize_1= 50000;
    public static final int weightGoalPrize_2= 50000;
    public static final int weightGoalPrize_3= 100000;

    public SNUIncentive(){
        CalorieList = new int[84];
    }

    public Calendar GetLastDate(){
        Calendar lastDate= Calendar.getInstance();
        lastDate.setTimeInMillis(startdate.getTimeInMillis());
        lastDate.add(Calendar.DAY_OF_MONTH, 7*12-1);
        return lastDate;
    }
    private Calendar getWeekStartDate(Calendar date) {
        Calendar calendar = Calendar.getInstance();
        long week= 7*86400000;
        long firstDate= startdate.getTimeInMillis();

        while(firstDate+week<date.getTimeInMillis()){
            firstDate+=week;
        }
        calendar.setTimeInMillis(firstDate);
        return calendar;
    }
    public boolean[] getWeeksIncentiveData(Calendar date, int calorieFrom) {

        boolean[] result= new boolean[7];

        Calendar weekStartDate = getWeekStartDate(date);
        Calendar weekend= Calendar.getInstance();
        weekend.setTimeInMillis(weekStartDate.getTimeInMillis());
        weekend.add(Calendar.DAY_OF_MONTH,6);

        Date weekEndDate = weekend.getTime();
        Long current = weekStartDate.getTimeInMillis();
        int calorie=0;

        int index = findIndex(weekStartDate);

        int count=0;
        for(int i=0;i<7;i++)
        {
            result[i]=false;

            if(CalorieList[i+index] >=calorieFrom)  result[i]=true;
        }
        return result;
    }
    private int findIndex(Calendar dateToFind) {

        int countOfDays= daysBetween(dateToFind.getTime(), startdate.getTime());
        if(countOfDays<0) {
            countOfDays = -1 * countOfDays;
        }
        return countOfDays;
    }
    public int daysBetween(Date d1, Date d2){

        d2= resetTime(d2);
        d1= resetTime(d1);

        return (int)( (d2.getTime() - d1.getTime()) / (1000 * 60 * 60 * 24));
    }


    /**
     *
     * @param thisWeekAchieve
     * @param todayDayofweek :일요일 1, 월2
     * @return
     */
    public String getweeksIncentiveEarnComment(boolean[] thisWeekAchieve, int todayDayofweek) {
        String result;
        int earned= 0;
       // todayDayofweek--;
        int achieveCount= 0;
        boolean todayAchieve=thisWeekAchieve[todayDayofweek];
        int remainDate= 7- todayDayofweek;
        if(todayAchieve==true) remainDate--;
        for(int i=0;i<thisWeekAchieve.length;i++){
            if(thisWeekAchieve[i]){
                achieveCount++;
                earned+= earnPerDay;
            }
        }
        //주의 마지막날이고 마지막날 달성한 경우
        if(remainDate==0){
            if(achieveCount==todayDayofweek+1){
                result =String.format("이번주 목표를 모두 달성하였습니다. 보너스 포함하여 %,d원을 획득 하셨습니다.",(7*earnPerDay+completeBonus));
            }else{
                result =String.format("이번주 획득 금액은 %,d원 입니다.",earned);
            }
        }else{
            //현재까지 모두 달생했으면
            if(achieveCount==todayDayofweek+1){
                result =String.format("앞으로 남은 %d일 동안 매일 목표를 달성하면 \n주간 총 %,d원을 획득 할 수 있습니다.", remainDate, (7*earnPerDay+completeBonus));
            }else if(achieveCount== todayDayofweek && todayAchieve==false){
                result =String.format("앞으로 남은 %d일 동안 매일 목표를 달성하면\n주간 총 %,d원을 획득 할 수 있습니다.", remainDate, (7*earnPerDay+completeBonus));
             //   result ="앞으로 남은 "+remainDate+"일 동안 매일 목표를 달성하면 보너스 포함"+ (7*earnPerDay+completeBonus)+"원을 획득 할 수 있습니다.";
            }
            else{
                result =String.format("앞으로 남은 %d일 동안 매일 목표를 달성하면 \n주간 총 %,d원을 획득 할 수 있습니다.", remainDate, (earned + remainDate*earnPerDay));
             //   result ="앞으로 남은 "+remainDate+"일 동안 매일 목표를 달성하면 주간 총 "+(earned + remainDate*earnPerDay)+"원을 획득 할 수 있습니다.";
            }
        }

        return result;
    }

    public int getWeekNumber(Calendar today) {

       Date Today=today.getTime();

       int weeks = getWeeksBetween(startdate.getTime(), Today);
        if(weeks>12) return 12;
       return weeks;
    }

    public int getWeeksBetween (Date a, Date b) {
        a = resetTime(a);
        b = resetTime(b);

        Calendar cal = new GregorianCalendar();
        cal.setTime(a);

        if(cal.getTimeInMillis() == b.getTime()) return 1;
        int weeks = 0;
        while (cal.getTime().before(b)) {
            // add another week
            cal.add(Calendar.WEEK_OF_YEAR, 1);
            weeks++;
            if(cal.getTimeInMillis()==b.getTime())
            {
                weeks++;
                break;
            }
        }
        return weeks;
    }

    public Date resetTime (Date d) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(d);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public boolean[][] getAchieveData( int calorieFrom){

        boolean[][] weeksData=new boolean [12][7];
        for(int i=0;i<12;i++){
            boolean [] aweek =new boolean[7];
            weeksData[i]=aweek;
            for(int j=0;j<7;j++){
                aweek[j]=false;
                if(CalorieList[i*7+j]>=calorieFrom) {
                    aweek[j]=true;
                }
            }
        }
        return  weeksData;
    }

    public SNU_EARN getSNUEarn(Calendar today, int calorieFrom) {

        boolean ispast=false;

        if(daysBetween(Calendar.getInstance().getTime(), GetLastDate().getTime())<0){
            ispast=true;
        }
        SNU_EARN earn= new SNU_EARN();
        int weekNum= getWeekNumber(today)-1;

        //미래일 경우
        if(weekNum<0){
            earn.current=0;
            earn.lost=0;
            earn.futurePossible= 12*(earnPerDay*7+completeBonus)+ weightGoalPrize_1+weightGoalPrize_2+weightGoalPrize_3;
            return earn;
        }
        int todayNum= daysBetween(startdate.getTime(), today.getTime());
        todayNum =todayNum%7;
        if(ispast)todayNum =6;

        boolean[][] weeksData=getAchieveData(calorieFrom);


        earn.current=0;
        earn.futurePossible=0;
        earn.lost=0;

        if(goal_1 ==WeightAchieve.NOTYET.Value ){
            earn.futurePossible+= weightGoalPrize_1;
        }else if(goal_1 == WeightAchieve.SUCCESS.Value){
            earn.current += weightGoalPrize_1;
        }else{
            earn.lost +=    weightGoalPrize_1;
        }
        if(goal_2 ==WeightAchieve.NOTYET.Value ){
            earn.futurePossible+= weightGoalPrize_2;
        }else if(goal_2 == WeightAchieve.SUCCESS.Value){
            earn.current += weightGoalPrize_2;
        }else{
            earn.lost +=    weightGoalPrize_2;
        }
        if(goal_3 ==WeightAchieve.NOTYET.Value ){
            earn.futurePossible+= weightGoalPrize_3;
        }else if(goal_3 == WeightAchieve.SUCCESS.Value){
            earn.current += weightGoalPrize_3;
        }else{
            earn.lost +=    weightGoalPrize_3;
        }


        /*if(goal_1 ==WeightAchieve.NOTYET.Value ){
            earn.futurePossible += weightGoalPrize_1;
        }
        if(goal_2==WeightAchieve.NOTYET.Value) {
            earn.futurePossible += weightGoalPrize_2;
        }
        if(goal_3== WeightAchieve.NOTYET.Value){
            earn.futurePossible +=weightGoalPrize_3;
        }

        if(weekNum>=3){
             if(goal_1== WeightAchieve.FAIL.Value){
                earn.lost += weightGoalPrize_1;
            }else if(goal_1 != WeightAchieve.NOTYET.Value)
            {
                earn.current +=weightGoalPrize_1;
            }
        }
        if(weekNum>=7){
            if(goal_2== WeightAchieve.FAIL.Value){
                earn.lost += weightGoalPrize_2;
            }else if(goal_2 != WeightAchieve.NOTYET.Value){
                earn.current +=weightGoalPrize_2;
            }
        }
        if(weekNum>=11){
            if(goal_3== WeightAchieve.FAIL.Value){
                earn.lost += weightGoalPrize_3;
            }else if(goal_3 != WeightAchieve.NOTYET.Value){
                earn.current +=weightGoalPrize_3;
            }
        }*/

        for(int i=0;i<12;i++)
        {
            boolean[] week = weeksData[i];

            if(i<weekNum || ispast){
                //지난주
                int count=0;
                for(int j=0;j<week.length;j++)
                {
                    if(week[j]) count++;
                }
                earn.current += count * earnPerDay;
                if(count==7) {
                    earn.current+= completeBonus;
                }else{
                    earn.lost += (7-count) *earnPerDay +completeBonus;
                }
            }
            else if(i==weekNum){
                int count=0;
                int lost=0;

                for(int j=0;j<todayNum;j++)
                {
                    if(week[j]) {
                        count++;
                    }else{
                        lost++;
                    }
                }
                earn.current+= count * earnPerDay;
                earn.lost+=lost*earnPerDay;
                /*if(lost>0){
                    earn.lost+=completeBonus;
                }
                else
                {
                    if(week[todayNum]){
                        earn.current += earnPerDay;
                        count++;
                    }
                    if(count==7){
                        earn.current +=completeBonus;
                    }else{
                        earn.futurePossible+=completeBonus;
                    }
                }*/
                if(week[todayNum]){
                    earn.current += earnPerDay;
                    count++;
                }
                if(count==7){
                    earn.current +=completeBonus;
                }else{
                    earn.futurePossible+=completeBonus;
                }

                int futre= 7-count-lost;
                earn.futurePossible += futre*earnPerDay;
               /* if(!ispast){
                    int future= 7-todayNum;
                    if(week[todayNum]==true){
                        earn.current += earnPerDay;
                        future--;
                    }
                    earn.futurePossible+= future*earnPerDay;
                }*/

               /* if(count==7) earn.current+=completeBonus;
                else{
                    if(lost==0){
                        earn.futurePossible+=completeBonus;
                    }
                }
                if(lost>0) {
                    earn.lost +=completeBonus;
                    earn.futurePossible -=completeBonus;
                }*/
            //현재주
            }else{
                //남은주
                earn.futurePossible +=7*earnPerDay +completeBonus;
            }
        }
        if(earn.futurePossible + earn.current+ earn.lost !=320000){
            return earn;
        }
        return earn;
    }
      @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(startdate.getTimeInMillis());
        dest.writeInt(this.goal_1);
        dest.writeInt(this.goal_2);
        dest.writeInt(this.goal_3);
        dest.writeIntArray(CalorieList);
    }
    private SNUIncentive(Parcel source) {
        // TODO Auto-generated constructor stub
        startdate= Calendar.getInstance();
        startdate.setTimeInMillis(source.readLong());
        this.goal_1 = source.readInt();
        this.goal_2 =  source.readInt();
        this.goal_3 =  source.readInt();
        this.CalorieList = new int[84];
        source.readIntArray(this.CalorieList);
    }
    public static final Parcelable.Creator<SNUIncentive> CREATOR = new Parcelable.Creator<SNUIncentive>() {

        @Override
        public SNUIncentive createFromParcel(Parcel in) {
            return new SNUIncentive(in);
        }

        @Override
        public SNUIncentive[] newArray(int size) {
            // TODO Auto-generated method stub
            return new SNUIncentive[size];
        }
    };
}



