package com.fitdotlife.fitmate;

import android.app.ActionBar;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fitdotlife.fitmate_lib.http.NetworkClass;
import com.fitdotlife.fitmate_lib.key.GenderType;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;


public class UserInfoChangeActivity extends Activity implements View.OnClickListener {

    private EditText etxBirthDay = null;
    private TextView tvEmail = null;
    private EditText etxNickname = null;
    private EditText etxIntrodcution = null;
    private EditText etxGender = null;
    private ImageView imgProfile = null;

    private String mEmail = null;
    private String mNickname = null;
    private String mIntro = null;
    private long mBirthDay = 0;
    private GenderType mGender = null;

    private int mSelectedGender = 0;

    private OnDateSetListener listener = new OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

            etxBirthDay.setText(year + "년 " + ( monthOfYear + 1 )+ "월 " + dayOfMonth +"일");
            mBirthDay = getMilliSecond(year , monthOfYear , dayOfMonth);

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info_change);

        Bundle userInfoBundle = this.getIntent().getExtras();

        this.mEmail = userInfoBundle.getString(UserInfo.EMAIL_KEY );
        this.mNickname = userInfoBundle.getString(UserInfo.NAME_KEY);
        this.mIntro = userInfoBundle.getString(UserInfo.HELLO_MESSAGE_KEY);
        this.mBirthDay = userInfoBundle.getLong( UserInfo.BIRTHDATEKEY );

        this.mGender = GenderType.getGenderType( userInfoBundle.getInt( UserInfo.SEX_KEY ));
        this.imgProfile = (ImageView) this.findViewById(R.id.img_activity_userinfo_change_picture);

        String imageName = userInfoBundle.getString(UserInfo.PROFILE_MAGE_KEY);


        String imgUri = null;
        if(imageName != null) {
            if (imageName.startsWith("file://")) {
                imgUri = imageName;
            } else {
                imgUri = NetworkClass.imageBaseURL + imageName;
            }
        }else{
            imgUri = "drawable://" + R.drawable.profile_img1;
        }

        int cornerRadius = imgProfile.getLayoutParams().width / 2;
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.profile_img1)
                .showImageOnFail(R.drawable.profile_img1)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .displayer(new RoundedBitmapDisplayer( cornerRadius ))
                .build();

        ImageLoader.getInstance().displayImage( imgUri , imgProfile, options);

        this.tvEmail = (TextView) this.findViewById(R.id.tv_activity_userinfo_change_email);
        this.tvEmail.setText( this.mEmail );

        this.etxNickname = (EditText) this.findViewById(R.id.etx_activity_userinfo_change_nickname);
        this.etxNickname.setText( this.mNickname );

        this.etxIntrodcution = (EditText) this.findViewById(R.id.etx_activity_userinfo_change_Introduction);
        this.etxIntrodcution.setText( this.mIntro );

        this.etxBirthDay = (EditText) this.findViewById(R.id.etx_activity_userinfo_change_birthday);
        if( this.mBirthDay > 0 ) {

            this.etxBirthDay.setText(this.getDateString(this.mBirthDay));
            this.etxBirthDay.setOnClickListener(this);

        }


        this.etxGender  = (EditText) this.findViewById(R.id.etx_activity_userinfo_change_gender);
        if(!this.mGender.equals(GenderType.NONE)) {
            this.etxGender.setText(this.mGender.getGenderString(this));
            this.etxGender.setOnClickListener(this);
        }

        ActionBar actionBar = getActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setDisplayUseLogoEnabled(false);
        View actionBarView = this.getLayoutInflater().inflate( R.layout.custom_action_bar_text, null );
        actionBarView.setBackgroundColor(0xFF3C3C3C);
        TextView tvBarTitle = (TextView) actionBarView.findViewById( R.id.tv_custom_action_bar_title );
        TextView tvLeft = (TextView) actionBarView.findViewById(R.id.tv_custom_action_bar_left);
        tvLeft.setOnClickListener(this);
        TextView tvRight = (TextView) actionBarView.findViewById(R.id.tv_custom_action_bar_right);
        tvRight.setOnClickListener(this);
        tvBarTitle.setText(this.getString(R.string.myprofile_change_profile));
        tvLeft.setText(this.getString(R.string.common_cancel));
        tvRight.setText(this.getString(R.string.myprofile_save));
        actionBar.setCustomView(actionBarView);
        actionBar.setDisplayShowCustomEnabled(true);

    }


    @Override
    public void onClick(View view) {
        switch( view.getId() ) {
            case R.id.etx_activity_userinfo_change_birthday:
                Calendar birthDayCalendar = Calendar.getInstance();
                birthDayCalendar.setTimeInMillis( mBirthDay );
                DatePickerDialog datePickerDialog = new DatePickerDialog(this , listener , birthDayCalendar.get(Calendar.YEAR) , birthDayCalendar.get(Calendar.MONTH), birthDayCalendar.get(Calendar.DATE) );
                datePickerDialog.show();
                break;
            case R.id.tv_custom_action_bar_left:
                this.finish();
                this.setResult( RESULT_CANCELED );
                break;
            case R.id.tv_custom_action_bar_right:
                Intent intent = new Intent();

                String nickName = "";
                if( this.etxNickname.length() == 0 ){

                    Toast.makeText(this, this.getResources().getString(R.string.signin_nickname_input), Toast.LENGTH_LONG).show();
                    this.etxNickname.requestFocus();
                    return;
                }

                if( this.etxNickname.length() > 0 ){
                    nickName = this.etxNickname.getText().toString();
                }
                intent.putExtra(UserInfo.NAME_KEY , nickName );

                String introduction = "";
                if( this.etxIntrodcution.length() > 0 ){
                    introduction = this.etxIntrodcution.getText().toString();
                }
                intent.putExtra( UserInfo.HELLO_MESSAGE_KEY , introduction );

                intent.putExtra(UserInfo.BIRTHDATEKEY, this.mBirthDay);
                intent.putExtra(UserInfo.SEX_KEY , this.mGender.getValue());

                this.setResult( RESULT_OK , intent );
                this.finish();
                break;
            case R.id.etx_activity_userinfo_change_gender:
//                AlertDialog.Builder dialog = new AlertDialog.Builder(this);
//                dialog.setTitle("성별");
//                dialog.setItems(new String[]{ "여성" , "남성" }, new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int selectIndex) {
//
//                        mGender = GenderType.getGenderType(selectIndex);
//                        if( mGender.equals(GenderType.MALE) ){
//                            imgProfile.setImageBitmap( getCircleBitmap( R.drawable.profile_img5 ) );
//                        }else if( mGender.equals( GenderType.FEMALE ) ){
//                            imgProfile.setImageBitmap( getCircleBitmap( R.drawable.profile_img2 ) );
//                        }
//                        etxGender.setText(mGender.getGenderString( UserInfoChangeActivity.this ));
//                    }
//                });
//                dialog.show();
                break;

        }
    }

    private Bitmap getCircleBitmap(int resourceId) {
        Bitmap bitmap = this.drawableToBitmap(this.getResources().getDrawable(resourceId));
        Bitmap output = Bitmap.createBitmap( bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);
        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        int size = (bitmap.getWidth()/2);
        canvas.drawCircle(size, size, size, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return output;
    }

    public Bitmap drawableToBitmap(Drawable drawable) {
        if (drawable == null) {
            return null;
        } else if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    private String getDateString( long dateMilliSecond ){
        String birthDateFormat = this.getString(R.string.dateformat);
        SimpleDateFormat dateFormat = new SimpleDateFormat(birthDateFormat);
        Date utcDate = new Date();
        utcDate.setTime( dateMilliSecond );
        return dateFormat.format( utcDate );
    }

    private long getMilliSecond( int year , int month , int day ){
        TimeZone utcZone = TimeZone.getTimeZone("UTC");
        Calendar birthDayCalendar = Calendar.getInstance( utcZone );

        birthDayCalendar.set(Calendar.YEAR , year);
        birthDayCalendar.set(Calendar.MONTH , month);
        birthDayCalendar.set( Calendar.DATE , day );
        birthDayCalendar.set( Calendar.HOUR , 0 );
        birthDayCalendar.set( Calendar.MINUTE , 0 );
        birthDayCalendar.set( Calendar.SECOND , 0 );
        birthDayCalendar.set( Calendar.MILLISECOND , 0 );

        return birthDayCalendar.getTimeInMillis();
    }

}