package com.fitdotlife.fitmate;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.fitdotlife.fitmate_lib.service.FitmateService;
import com.fitdotlife.fitmate_lib.service.alarm.AlarmService;

/**
 * Created by Joshua on 2015-05-29.
 */
public class BootCompleteReceiver extends BroadcastReceiver{

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent serviceIntent = new Intent( context , FitmateService.class);
        context.startService(serviceIntent);

        Intent alarmIntent = new Intent( context , AlarmService.class);
        context.startService(alarmIntent);

    }

}
