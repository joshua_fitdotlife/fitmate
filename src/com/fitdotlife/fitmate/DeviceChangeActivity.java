package com.fitdotlife.fitmate;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.fitdotlife.fitmate_lib.iview.IDeviceChangeView;
import com.fitdotlife.fitmate_lib.presenter.DeviceChangePresenter;
import com.fitdotlife.fitmate_lib.service.protocol.ProtocolManager;
import com.fitdotlife.fitmate_lib.util.BluetoothDeviceUtil;

import java.util.Hashtable;


public class DeviceChangeActivity extends Activity implements View.OnClickListener , BluetoothAdapter.LeScanCallback, IDeviceChangeView {

    private String TAG = "fitmate - " + DeviceChangeActivity.class.getSimpleName();
    public static final String INTENT_LOCATION = "LOCATION";
    private static final int ENABLE_BT_REQUEST_ID = 1;
    private final String FITMETER_NAME ="FITMETERBLE";

    private LinearLayout llMarkContainer = null;
    private LayoutInflater inflater = null;
    private ScrollView scrContainer = null;
    private ImageView imgClose = null;
    private ImageView imgBack = null;

    private Button btnNext = null;

    private int mStep = -1;
    private View[] mChildViewList = null;
    private View[] mMarkViewList = null;

    private BluetoothManager mBluetoothManager = null;
    private BluetoothAdapter mBluetoothAdapter = null;
    private Handler mHandler = new Handler();

    private TextView tvScanTime = null;

    private int mTimerValue = 0;
    private DeviceChangePresenter mPresenter = null;

    private ProtocolManager mProtocolManager = null;

    private boolean isScanned = false;

    private Handler mTimerHander = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            if( isScanned ) {
                tvScanTime.setText(5 - mTimerValue + "");

                if (mTimerValue < 5) {

                    if (mTimerValue == 2) {
                        mBluetoothAdapter.startLeScan(DeviceChangeActivity.this);
                        Log.d("DeviceChangeActivity", "scan start");
                    }

                    mTimerHander.sendEmptyMessageDelayed(0, 1000);
                }else if (mTimerValue == 5) {
                    checkFoundDevice();
                }

                mTimerValue++;
            }else{
                stopScan();
            }
        }
    };

    private int Location = -1;

    private ImageView relativeLayout_wearing;

    private TextView selectedTextView_wrist;
    private TextView selectedTextView_waist;
    private TextView selectedTextView_upperarm;
    private TextView selectedTextView_panspocket;
    private TextView selectedTextView_ankle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_change);

        Intent intent = getIntent();
        if(intent != null)
        {
            Location = intent.getIntExtra(INTENT_LOCATION, -1);
            //SetWear(WearingLocation.getWearingLocation(loc));
        }


        this.mPresenter = new DeviceChangePresenter(this , this);

        this.btnNext = (Button) this.findViewById(R.id.btn_activity_device_change_next);
        this.btnNext.setOnClickListener(this);

        this.imgBack = (ImageView) this.findViewById(R.id.img_activity_device_change_back);
        this.imgBack.setOnClickListener(this);
        this.imgClose = (ImageView) this.findViewById(R.id.img_activity_device_change_close);
        this.imgClose.setOnClickListener(this);

        this.inflater =  this.getLayoutInflater();
        this.llMarkContainer = (LinearLayout) this.findViewById(R.id.ll_activity_device_change_step_mark );
        this.scrContainer = (ScrollView) this.findViewById(R.id.scr_activity_device_change_container );

        this.mChildViewList = new View[2];
        this.mMarkViewList = new View[2];

        this.mChildViewList[0] = this.inflater.inflate( R.layout.child_device_change_step_1, this.scrContainer  , false);
        this.mMarkViewList[0]  = this.inflater.inflate(R.layout.child_step_mark_1of2, this.llMarkContainer , false);

        this.mChildViewList[1] = this.inflater.inflate( R.layout.child_device_change_step_3, this.scrContainer , false);
        this.mMarkViewList[1]  = this.inflater.inflate(R.layout.child_step_mark_2of2, this.llMarkContainer, false);

        this.tvScanTime = (TextView) this.mChildViewList[1].findViewById(R.id.tv_child_device_change_scan_time);
        this.moveFrontView();
    }

    private void moveFrontView()
    {
        this.mStep += 1;
        drawChildView();
    }

    private void moveBackView()
    {
        if( mStep == 1 ){
            this.isScanned = false;
        }

        this.mStep -= 1;
        drawChildView();
    }

    private void drawChildView() {

        if( mStep == -1 ){
            this.finish();
            return;
        }

        this.scrContainer.removeAllViews();
        this.llMarkContainer.removeAllViews();

        this.scrContainer.addView( this.mChildViewList[ mStep ] );
        this.llMarkContainer.addView( this.mMarkViewList[ mStep ] );

        if(mStep == 0)
        {
            this.btnNext.setVisibility(View.VISIBLE);
        }

        if(mStep == 1){

            this.btnNext.setVisibility(View.GONE);

            this.mBluetoothManager = (BluetoothManager) this.getSystemService(Context.BLUETOOTH_SERVICE);
            this.mBluetoothAdapter = this.mBluetoothManager.getAdapter();

            this.startScan();
        }
    }

    private boolean isBtEnabled()
    {
        final BluetoothManager manager = (BluetoothManager) this.getSystemService(Context.BLUETOOTH_SERVICE);
        if( manager == null ) return false;

        final BluetoothAdapter adapter = manager.getAdapter();
        if( adapter == null ) return false;

        return adapter.isEnabled();
    }

    @Override
    public void onClick(View view) {
        switch( view.getId() ){
            case R.id.btn_activity_device_change_next:

                if( mStep == 0 ) {

                    if (this.isBtEnabled() == false) {
                        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                        this.startActivityForResult(enableBtIntent, this.ENABLE_BT_REQUEST_ID);

                        return;
                    }
                }

                this.moveFrontView();
                break;
            case R.id.img_activity_device_change_back:
                this.moveBackView();
                break;

            case R.id.img_activity_device_change_close:
                if( mStep ==1 ) {
                    this.isScanned = false;
                }
                this.finish();
                break;

        }
    }

    @Override
    public void onLeScan(final BluetoothDevice device, final int rssi, byte[] scanRecord) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String deviceName = device.getName();
                if (deviceName != null) {
                    if( deviceName.equals(FITMETER_NAME) ) {
                        BluetoothDeviceUtil.checkList(device, rssi);
                    }
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if( requestCode == ENABLE_BT_REQUEST_ID )
        {
            if( resultCode == Activity.RESULT_CANCELED )
            {
                Toast.makeText(this, this.getString(R.string.myprofile_fitmeter_bluetooth_on), Toast.LENGTH_LONG).show();
            }else if( resultCode == RESULT_OK ){
                moveFrontView();
            }
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.close();
    }

    private void startScan()
    {
        //this.addScaningTimeout();
        this.isScanned = true;
        this.mTimerValue = 0;
        this.mTimerHander.sendEmptyMessage(0);
        BluetoothDeviceUtil.clearList();

        this.isScanned = true;
    }

    private void stopScan()
    {
        this.mBluetoothAdapter.stopLeScan(this);
    }

    String foundedAddress = "";

    private void checkFoundDevice(){
        this.stopScan();

        String [] s = BluetoothDeviceUtil.firstDeviceAddressStrings(SetUserInfoActivity.SCAN_COUNT_VALUE, SetUserInfoActivity.RSSI_VALUE);

        int foundDeviceSize = 0;
        if(s != null)
        {
            foundDeviceSize = s.length;
        }

        if( foundDeviceSize == 0 ){
            this.showFindDeviceDialog(this.getString(R.string.signin_device_find_result_no));

        }else if( foundDeviceSize == 1 ){
            foundedAddress = s[0];
            this.showDeleteDataDialog();
        }else{
            this.showFindDeviceDialog(this.getString(R.string.signin_device_find_result_one_more));
        }
    }

    private void showFindDeviceDialog(String message){
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);

        dialog.setTitle(this.getString(R.string.signin_device_find_result))
                .setMessage(message)
                .setCancelable(false)
            .setNeutralButton(this.getString(R.string.common_cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finishActivity();
                    dialog.dismiss();
                }
            })
            .setPositiveButton(this.getString(R.string.signin_device_research), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //mStep = 0;
                    //drawChildView();
                    //dialog.dismiss();
                    startScan();
                }
            }).show();
    }

    private void showDeleteDataDialog(){
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);

        dialog.setTitle(this.getString(R.string.myprofile_delete_fitmeter_data_title))
                .setMessage(this.getString(R.string.myprofile_delete_fitmeter_data_content))
                .setCancelable(false)
                .setNegativeButton(R.string.common_no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        finish();
                        dialog.dismiss();
                    }
                })
                .setPositiveButton(R.string.common_yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        mPresenter.changeDevice(mBluetoothAdapter, foundedAddress, BluetoothDeviceUtil.getBluetoothDevice(foundedAddress), true);
                        dialog.dismiss();
                    }
                })
                .show();
    }

    @Override
    public void finishActivity() {
        this.finish();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        this.moveBackView();
    }

    @Override
    public void showResultDialog( final boolean isSuccess , final boolean deleteDeviceData ){

        AlertDialog.Builder dialog = new AlertDialog.Builder(this);

        if( isSuccess ){

            dialog.setTitle( this.getString(R.string.myprofile_change_fitmeter_result) )
                    .setMessage(this.getString(R.string.myprofile_change_fitmeter_success))
                    .setCancelable(false)
                    .setPositiveButton( this.getString(R.string.common_ok) , new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            finishActivity();
                            dialog.dismiss();

                        }
                    }).show();
        }else{

            dialog.setTitle( this.getString(R.string.myprofile_change_fitmeter_result) )
                    .setMessage( this.getString(R.string.myprofile_change_fitmeter_fail) )
                    .setCancelable(false)
                    .setPositiveButton(this.getString(R.string.signin_device_research), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            mPresenter.changeDevice(mBluetoothAdapter, foundedAddress, BluetoothDeviceUtil.getBluetoothDevice(foundedAddress), true);
                            dialog.dismiss();

                        }
                    })
                    .setNegativeButton(this.getString(R.string.common_cancel) , new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {


                            finishActivity();
                            dialog.dismiss();


                        }
                    }).show();
        }
    }

    @Override
    public int getWearLocation() {
        return Location;
    }


}