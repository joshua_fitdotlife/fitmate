package com.fitdotlife.fitmate;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.WearingLocation;
import com.fitdotlife.fitmate_lib.customview.DotNavigationView;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.iview.ISetUserInfoView;
import com.fitdotlife.fitmate_lib.key.GenderType;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.presenter.SetUserInfoPresenter;
import com.fitdotlife.fitmate_lib.service.util.Utils;
import com.fitdotlife.fitmate_lib.util.BluetoothDeviceUtil;
import com.fitdotlife.fitmate_lib.util.DateUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class SetUserInfoActivity extends Activity implements OnClickListener , ISetUserInfoView, BluetoothAdapter.LeScanCallback, CompoundButton.OnCheckedChangeListener {
    private String TAG = "fitmate";

    private static final int ENABLE_BT_REQUEST_ID = 1;
    private final float DEFAULT_HEIGHT = 170;
    private final float DEFAULT_WEIGHT = 70;

    private static final long SCANNING_TIMEOUT = 5 * 1000;
    public static final int SCAN_COUNT_VALUE = 0;
    public static final int RSSI_VALUE = -70;
    private final int RSSI_VALUE_UNDER = -35;
    private final String FITMETER_NAME = "FITMETERBLE";

    private int MAN = 1;
    private int WOMAN = 0;

    private LayoutInflater inflater = null;
    private ScrollView llScroll = null;
    private LinearLayout llContainer = null;
    private LinearLayout llContainerMatch = null;
    private DotNavigationView dotNavigationView = null;

    private ImageView imgClose = null;
    private ImageView imgBack = null;

    //성별 정보
    private ImageView imgMan = null;
    private ImageView imgWoman = null;

    //체력관련 정보
    private EditText etxBirthDay = null;
    private TextView tvWeight = null;
    private TextView tvHeight = null;
    private Button btnNext = null;

    private int mStep = -1;
    private UserInfo mUserInfo = null;
    private View[] mChildViewList = null;

    private int mSelectedGender = -1;

    private SetUserInfoPresenter mPresenter = null;


    private BluetoothManager mBluetoothManager = null;
    private BluetoothAdapter mBluetoothAdapter = null;
    private Handler mHandler = new Handler();

    private InputMethodManager inputMethodManager = null;
    private boolean isDeviceSelected = false;

    private TextView tvPressButton = null;

    private ProgressDialog mDialog = null;
    private TextView tvScanTime = null;
    private int mTimerValue = 0;

    private ImageView relativeLayout_wearing;

    private TextView selectedTextView_wrist;
    private TextView selectedTextView_waist;
    private TextView selectedTextView_upperarm;
    private TextView selectedTextView_panspocket;
    private TextView selectedTextView_ankle;

    private WearingLocation wearAt= WearingLocation.WAIST;

    private LinearLayout llMainContainer = null;

    private boolean isSelectWearingLocation = false;
    private boolean isScaned = false;

    private float mTempHeight = 0;
    private float mTempWeight = 0;

    private Handler mTimerHander = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            if( isScaned ) {
                tvScanTime.setText(5 - mTimerValue + "");

                if (mTimerValue < 5) {

                    if(mTimerValue==2){
                        mBluetoothAdapter.startLeScan(SetUserInfoActivity.this);
                        android.util.Log.d("DeviceChangeActivity", "scan start");
                    }

                    mTimerHander.sendEmptyMessageDelayed(0, 1000);
                }else if (mTimerValue == 5) {
                    checkFoundDevice();
                }


                mTimerValue++;
            }else{
                stopScan();
            }
        }
    };

    boolean isServiceAgree = false;



    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_set_user_info);

        this.mPresenter = new SetUserInfoPresenter( this , this );
        FitmateDBManager mDBManager = new FitmateDBManager( this.getApplicationContext() );
        this.inputMethodManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);

        this.llMainContainer = (LinearLayout) this.findViewById(R.id.ll_activity_set_user_info_container);
        this.inflater =  this.getLayoutInflater();
        this.llScroll = (ScrollView) this.findViewById(R.id.scr_set_userinfo_scroll );
        this.llContainer = (LinearLayout) this.findViewById(R.id.scr_set_userinfo_container );
        this.llContainerMatch = (LinearLayout) this.findViewById(R.id.scr_set_userinfo_container_match );
        this.dotNavigationView = (DotNavigationView) this.findViewById(R.id.dnv_child_activity_set_userinfo_navigation);
        this.btnNext = (Button) this.findViewById(R.id.btn_set_userinfo_next );
        this.btnNext.setOnClickListener(this);

        this.imgClose = (ImageView) this.findViewById(R.id.img_activity_set_userinfo_close );
        this.imgClose.setOnClickListener(this);
        this.imgBack = (ImageView) this.findViewById(R.id.img_activity_set_userinfo_back );
        this.imgBack.setOnClickListener(this);

        this.mUserInfo = mDBManager.getUserInfo();
        this.mUserInfo.setIsSI(true);

        this.mChildViewList = new View[5];


         final Context mContext = this;


        this.mChildViewList[0] = this.inflater.inflate( R.layout.child_set_userinfo_step_1 , this.llContainer, false );

        this.imgMan = (ImageView) this.mChildViewList[0].findViewById(R.id.img_child_set_userinfo_man);
        this.imgMan.setOnClickListener(this);
        this.imgWoman = (ImageView) this.mChildViewList[0].findViewById(R.id.img_child_set_userinfo_woman);
        this.imgWoman.setOnClickListener(this);

        this.mChildViewList[1] = this.inflater.inflate( R.layout.child_set_userinfo_step_2 , this.llContainer, false );

        this.etxBirthDay = (EditText) this.mChildViewList[1].findViewById(R.id.etx_child_set_userinfo_birthday);
        this.tvHeight = (TextView) this.mChildViewList[1].findViewById(R.id.tv_child_set_userinfo_height);
        this.tvHeight.setOnClickListener(this);
        this.tvWeight = (TextView) this.mChildViewList[1].findViewById(R.id.tv_child_set_userinfo_weight);
        this.tvWeight.setOnClickListener(this);

        final Activity ctx = this;


        this.mChildViewList[2] = this.inflater.inflate( R.layout.child_device_change_step_1 , this.llContainerMatch, false );

        this.tvPressButton = (TextView) this.mChildViewList[2].findViewById(R.id.tv_child_device_change_press_button);


        this.mChildViewList[3] = this.inflater.inflate(R.layout.child_device_change_step_3 , this.llContainer, false);

        this.tvScanTime = (TextView) this.mChildViewList[3].findViewById(R.id.tv_child_device_change_scan_time);


        this.mChildViewList[4] = this.inflater.inflate(R.layout.child_device_change_step_4 , this.llContainerMatch, false);

        int changePosition = 4;

        final Context activityContext = this;

        this.relativeLayout_wearing =(ImageView) this.mChildViewList[changePosition].findViewById(R.id.relativelay_wearing);
        this.relativeLayout_wearing.setImageResource(R.drawable.wear0);

        selectedTextView_waist= (TextView) this.mChildViewList[changePosition].findViewById(R.id.wear_waist);
        selectedTextView_waist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isSelectWearingLocation = true;
                wearAt= WearingLocation.WAIST;

           //     recycleView(relativeLayout_wearing);
                relativeLayout_wearing.setImageResource(R.drawable.wear5);
              //  relativeLayout_wearing.setBackground(new BitmapDrawable(getResources(), BitmapFactory.decodeResource(getResources(), R.drawable.wear5)));
            }
        });
        selectedTextView_upperarm= (TextView) this.mChildViewList[changePosition].findViewById(R.id.wear_upperarm);
        selectedTextView_upperarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isSelectWearingLocation = true;
                wearAt= WearingLocation.UPPERARM;
                //  relativeLayout_wearing.setBackground(getResources().getDrawable(R.drawable.wear2));

            //    recycleView(relativeLayout_wearing);
                relativeLayout_wearing.setImageResource( R.drawable.wear2);
              //  relativeLayout_wearing.setBackground(new BitmapDrawable(getResources(), BitmapFactory.decodeResource(getResources(), R.drawable.wear2)));
            }
        });
        selectedTextView_panspocket= (TextView) this.mChildViewList[changePosition].findViewById(R.id.wear_pantspocket);
        selectedTextView_panspocket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isSelectWearingLocation = true;
                wearAt= WearingLocation.PANTSPOCKET;
                //relativeLayout_wearing.setBackground(getResources().getDrawable(R.drawable.wear4));
            //    recycleView(relativeLayout_wearing);
                relativeLayout_wearing.setImageResource(R.drawable.wear4);
                //relativeLayout_wearing.setBackground(new BitmapDrawable(getResources(), BitmapFactory.decodeResource(getResources(), R.drawable.wear4)));
            }
        });
        selectedTextView_wrist= (TextView) this.mChildViewList[changePosition].findViewById(R.id.wear_wrist);
        selectedTextView_wrist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isSelectWearingLocation = true;
                wearAt= WearingLocation.WRIST;
                //      relativeLayout_wearing.setBackground(getResources().getDrawable(R.drawable.wear1));
            //    recycleView(relativeLayout_wearing);
            //    relativeLayout_wearing.setBackground(new BitmapDrawable(getResources(), BitmapFactory.decodeResource(getResources(), R.drawable.wear1)));
                relativeLayout_wearing.setImageResource(R.drawable.wear1);
            }
        });
        selectedTextView_ankle= (TextView) this.mChildViewList[changePosition].findViewById(R.id.wear_ankle);
        selectedTextView_ankle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isSelectWearingLocation = true;
                wearAt= WearingLocation.ANKLE;
             //   recycleView(relativeLayout_wearing);
              //  relativeLayout_wearing.setBackground(new BitmapDrawable(getResources(), BitmapFactory.decodeResource(getResources(), R.drawable.wear3)));
                relativeLayout_wearing.setImageResource(R.drawable.wear3);
            }
        });



        //this.mStep = 0;
        this.moveFrontView();

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

    }

    public static class TimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            // Do something with the time chosen by the user
        }
    }


    private void moveFrontView()
    {
        this.mStep += 1;
        drawChildView();
    }

    private void moveBackView()
    {
        if( mStep == 3 ){
            this.isScaned = false;
            this.btnNext.setEnabled(true);
            this.btnNext.setBackgroundColor(this.getResources().getColor(R.color.fitmate_green));
        }

        if( mStep == 4 ) {
            this.mStep -= 2;
            this.btnNext.setText(this.getString(R.string.common_next));
        }else {
            this.mStep -= 1;
        }
        drawChildView();
    }

    private void drawChildView()
    {
        if( mStep == -1 ){
            this.finish();
            return;
        }

        this.llContainerMatch.removeAllViews();
        this.llContainer.removeAllViews();

        this.dotNavigationView.setDotIndex(mStep + 1);

        switch (mStep)
        {
            case 2:
            case 4:
                this.llContainerMatch.addView(this.mChildViewList[mStep]);
                break;
            default:
                this.llContainer.addView(this.mChildViewList[mStep]);
                break;
        }


        this.llScroll.scrollTo(0, 0);
        this.llContainer.scrollTo(0, 0);
        this.llContainerMatch.scrollTo(0, 0);

        if( mStep == 0 ){

            this.imgBack.setVisibility(View.INVISIBLE);
            this.inputMethodManager.hideSoftInputFromWindow(this.imgMan.getWindowToken() , 0);

        }
        else{
            this.imgBack.setVisibility(View.VISIBLE);
        }

        if(mStep == 1){
            this.btnNext.setText( this.getString(R.string.common_next) );
        }

        if( this.mStep == 2 )
        {
            this.inputMethodManager.hideSoftInputFromWindow( this.tvPressButton.getWindowToken() , 0);
        }

        if(this.mStep == 3 ){

            this.btnNext.setEnabled(false);
            this.btnNext.setBackgroundColor( this.getResources().getColor(R.color.fitmate_gray) );

            this.mBluetoothManager = (BluetoothManager) this.getSystemService(Context.BLUETOOTH_SERVICE);
            this.mBluetoothAdapter = this.mBluetoothManager.getAdapter();
            BluetoothDeviceUtil.clearList();
            startScan();
        }

        if( this.mStep == 4 ){

            this.btnNext.setEnabled(true);
            this.btnNext.setBackgroundColor( this.getResources().getColor(R.color.fitmate_green) );
            this.btnNext.setText( this.getString(R.string.common_finish).toUpperCase() );

        }
    }

    @Override
    public void onClick(View view)
    {
        switch( view.getId() )
        {
            case R.id.tv_child_set_userinfo_height:

                boolean heightUnit = mUserInfo.isSI();
                final AlertDialog.Builder heightDialog = new AlertDialog.Builder(this);
                heightDialog.setTitle(this.getString(R.string.common_height));
                View numberPickerView = this.getLayoutInflater().inflate(R.layout.dialog_number_picker, null);
                final NumberPicker np = (NumberPicker) numberPickerView.findViewById(R.id.np_dialog_number_picker);
                np.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
                final NumberPicker pp = (NumberPicker) numberPickerView.findViewById(R.id.np_dialog_point_picker);
                pp.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
                final NumberPicker up = (NumberPicker) numberPickerView.findViewById( R.id.np_dialog_unit_picker );
                up.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
                up.setDisplayedValues(new String[]{this.getString(R.string.common_centimeter), this.getString(R.string.common_ft)});

                float height = mUserInfo.getHeight();
                np.setMaxValue(300);
                np.setMinValue(1);
                pp.setMaxValue(9);
                pp.setMinValue(0);
                up.setMaxValue(1);
                up.setMinValue(0);

                if( height > 0 ) {

                    String[] valueArray = this.getValueArray( height );
                    np.setValue( Integer.parseInt( valueArray[0] ) );
                    pp.setValue( Integer.parseInt( valueArray[1] ) );

                    up.setTag( height );

                }else{

                    if( mUserInfo.isSI() ) {
                        np.setValue((int) DEFAULT_HEIGHT);
                        up.setTag(DEFAULT_HEIGHT);
                    }else{
                        float dHeight = Utils.cm_to_ft( DEFAULT_HEIGHT );
                        dHeight = Math.round( dHeight );
                        np.setValue((int) dHeight);
                        up.setTag( dHeight );
                    }
                }

                if( heightUnit )
                {
                    up.setValue(0);
                }
                else
                {
                    up.setValue(1);
                }
                heightDialog.setView(numberPickerView);

                up.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                    @Override
                    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                        boolean heightSi = (newVal == 0);
                        float tempHeight = (float) up.getTag();

                        //몸무게를 변환한다.
                        if (heightSi) {
                            //파운드에서 킬로그램으로 변환한다.
                            tempHeight = com.fitdotlife.fitmate_lib.service.util.Utils.ft_to_cm(tempHeight);
                        } else {
                            //킬로그램에서 파운드로 변환한다.
                            tempHeight = com.fitdotlife.fitmate_lib.service.util.Utils.cm_to_ft(tempHeight);
                        }

                        String[] valueArray = getValueArray( tempHeight );
                        np.setValue( Integer.parseInt( valueArray[0] ) );
                        pp.setValue( Integer.parseInt( valueArray[1]) );

                        up.setTag(tempHeight);
                    }
                });

                np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                    @Override
                    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                        up.setTag( (float) (((np.getValue() * 10d) + pp.getValue()) / 10d) );
                    }
                });

                pp.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                    @Override
                    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                        up.setTag( (float) (((np.getValue() * 10d) + pp.getValue()) / 10d) );
                    }
                });

                heightDialog.setNegativeButton(this.getString(R.string.common_cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                heightDialog.setPositiveButton(this.getString(R.string.common_ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        boolean heightSi = ( up.getValue() == 0 );
                        boolean isSi = mUserInfo.isSI();
                        float tempWeight = mUserInfo.getWeight();
                        String weightUnitString = "";

                        if (heightSi != isSi) {
                            //몸무게를 변환한다.
                            if( tempWeight > 0 )
                            {
                                if (heightSi) {
                                    //파운드에서 킬로그램으로 변환한다.
                                    tempWeight = Utils.lb_to_kg(tempWeight);
                                    weightUnitString = getString(R.string.common_kilogram);
                                } else {
                                    //킬로그램에서 파운드로 변환한다.
                                    tempWeight = Utils.kg_to_lb(tempWeight);
                                    weightUnitString = getString(R.string.common_pound);
                                }

                                String[] weightValueArray = getValueArray( tempWeight );
                                tvWeight.setText(String.format("%s %s", weightValueArray[0] + "." + weightValueArray[1] , weightUnitString));
                            }

                        }

                        mUserInfo.setHeight((Float) up.getTag());
                        mUserInfo.setWeight(tempWeight);
                        mUserInfo.setIsSI(heightSi);

                        String[] heightValueArray = getValueArray( mUserInfo.getHeight() );
                        tvHeight.setText(String.format("%s %s", heightValueArray[0] + "." + heightValueArray[1] , up.getDisplayedValues()[up.getValue()]));

                    }
                });
                heightDialog.show();
                break;

            case R.id.tv_child_set_userinfo_weight:

                boolean weightUnit = mUserInfo.isSI();
                AlertDialog.Builder weightDialog = new AlertDialog.Builder(this);
                weightDialog.setTitle(this.getString(R.string.common_weight));
                final View weightNumberPickerView = this.getLayoutInflater().inflate(R.layout.dialog_number_picker, null);
                final NumberPicker weightNP = (NumberPicker) weightNumberPickerView.findViewById(R.id.np_dialog_number_picker);
                weightNP.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
                final NumberPicker weightPP = (NumberPicker) weightNumberPickerView.findViewById(R.id.np_dialog_point_picker);
                weightPP.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
                final NumberPicker weightUP = (NumberPicker) weightNumberPickerView.findViewById(R.id.np_dialog_unit_picker);
                weightUP.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
                weightUP.setDisplayedValues(new String[]{this.getString(R.string.common_kilogram), this.getString(R.string.common_pound)});

                float weight = this.mUserInfo.getWeight();
                weightNP.setMaxValue(500);
                weightNP.setMinValue(1);
                weightPP.setMaxValue(9);
                weightPP.setMinValue(0);
                weightUP.setMaxValue(1);
                weightUP.setMinValue(0);

                if( weightUnit )
                {
                    weightUP.setValue(0);
                }else{
                    weightUP.setValue(1);
                }

                if( weight > 0 ) {

                    String[] valueArray = this.getValueArray( weight );
                    weightNP.setValue(Integer.parseInt(valueArray[0]));
                    weightPP.setValue(Integer.parseInt(valueArray[1]));

                    weightUP.setTag( weight );

                }else{
                    if( mUserInfo.isSI() ) {
                        weightNP.setValue((int) DEFAULT_WEIGHT);
                        weightUP.setTag(DEFAULT_WEIGHT);
                    }else{
                        float dWeight = Utils.kg_to_lb( DEFAULT_WEIGHT );
                        dWeight = Math.round( dWeight );
                        weightNP.setValue((int) dWeight);
                        weightUP.setTag( dWeight );
                    }
                }

                weightUP.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                    @Override
                    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                        boolean weightSi = (newVal == 0);
                        float tempWeight = (float) weightUP.getTag();

                        //몸무게를 변환한다.
                        if (weightSi) {
                            //파운드에서 킬로그램으로 변환한다.
                            tempWeight = com.fitdotlife.fitmate_lib.service.util.Utils.lb_to_kg(tempWeight);
                        } else {
                            //킬로그램에서 파운드로 변환한다.
                            tempWeight = com.fitdotlife.fitmate_lib.service.util.Utils.kg_to_lb(tempWeight);
                        }

                        String[] valueArray = getValueArray( tempWeight );
                        weightNP.setValue(Integer.parseInt(valueArray[0]));
                        weightPP.setValue(Integer.parseInt(valueArray[1]));

                        weightUP.setTag(tempWeight);
                    }
                });

                weightNP.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                    @Override
                    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                        weightUP.setTag((float) (((weightNP.getValue() * 10d) + weightPP.getValue()) / 10d) );
                    }
                });

                weightPP.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                    @Override
                    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                        weightUP.setTag((float) (((weightNP.getValue() * 10d) + weightPP.getValue()) / 10d) );
                    }
                });

                weightDialog.setView(weightNumberPickerView);
                weightDialog.setNegativeButton(this.getString(R.string.common_cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });



                weightDialog.setPositiveButton(this.getString(R.string.common_ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        boolean weightSi = ( weightUP.getValue() ==0 );
                        boolean isSi = mUserInfo.isSI();
                        float tempHeight = mUserInfo.getHeight();
                        String heightUnitString = "";

                        if (weightSi != isSi ) {
                            //키를 변환한다.
                            if( tempHeight > 0 )
                            {
                                if (weightSi) {
                                    tempHeight = Utils.ft_to_cm(tempHeight);
                                    heightUnitString = getString(R.string.common_centimeter);
                                } else {
                                    tempHeight = Utils.cm_to_ft(tempHeight);
                                    heightUnitString = getString(R.string.common_ft);
                                }

                                String[] heightValueArray = getValueArray( tempHeight );
                                tvHeight.setText(String.format("%s %s", heightValueArray[0] + "." + heightValueArray[1] , heightUnitString ));
                            }

                        }

                        mUserInfo.setWeight((Float) weightUP.getTag());
                        mUserInfo.setHeight(tempHeight);
                        mUserInfo.setIsSI(weightSi);

                        String[] weightValueArray = getValueArray( mUserInfo.getWeight() );
                        tvWeight.setText(String.format("%s %s", weightValueArray[0] + "." + weightValueArray[1], weightUP.getDisplayedValues()[weightUP.getValue()]));
                    }
                });
                weightDialog.show();

                break;
            case R.id.btn_set_userinfo_next:
                if( this.validCheckThenSave() )
                {
                    if( mStep == 4 )
                    {
                        mUserInfo.setWearAt(wearAt.Value);
                        this.mPresenter.setUserInfo( mUserInfo , mBluetoothAdapter ,  foundedAddress , BluetoothDeviceUtil.getBluetoothDevice(foundedAddress)  );
                        return;
                    }

                    this.moveFrontView();
                }
                break;
            case R.id.img_activity_set_userinfo_close:

                if( mStep ==3 ) {
                    this.isScaned = false;
                }

                this.finish();
                break;
            case R.id.img_activity_set_userinfo_back:
                this.moveBackView();
                break;
            case R.id.img_child_set_userinfo_man:
                this.imgMan.setImageResource( R.drawable.man_over );
                this.imgWoman.setImageResource( R.drawable.woman );
                this.mSelectedGender = MAN;
                break;
            case R.id.img_child_set_userinfo_woman:
                this.imgMan.setImageResource( R.drawable.man);
                this.imgWoman.setImageResource( R.drawable.woman_over );
                this.mSelectedGender = WOMAN;
                break;
            case R.id.tv_child_set_account_service_agreement:
                Intent serviceAgreementIntent = new Intent( SetUserInfoActivity.this , ServiceAgreementActivity.class );
                this.startActivity( serviceAgreementIntent );
                break;
        }
    }

    private boolean validCheckThenSave()
    {

        if( mStep == 0 )
        {
            if( this.mSelectedGender == -1 )
            {
                Toast.makeText( this , this.getResources().getString(R.string.signin_gender_select) , Toast.LENGTH_LONG ).show();
                return false;
            }

            this.mUserInfo.setGender(GenderType.getGenderType(this.mSelectedGender ));
        }
        else if( mStep == 1 )
        {
            if (this.etxBirthDay.getText().length() == 0) {
                Toast.makeText(this, this.getResources().getString(R.string.signin_birthday_input), Toast.LENGTH_LONG).show();
                this.etxBirthDay.requestFocus();
                return false;
            }

            if (this.etxBirthDay.getText().length() != 8) {
                Toast.makeText(this, this.getResources().getString(R.string.signin_birthday_do_not_match_digits), Toast.LENGTH_LONG).show();
                this.etxBirthDay.requestFocus();
                return false;
            }

            //년도를 잘라낸다.
            int birthYear = Integer.parseInt(this.etxBirthDay.getText().toString().substring(0, 4));
            Calendar calendar = Calendar.getInstance();
            int currentYear = calendar.get(Calendar.YEAR);
            if ((currentYear - birthYear) > 150) {
                Toast.makeText(this, R.string.signin_birthday_wrong_year , Toast.LENGTH_LONG).show();
                this.etxBirthDay.requestFocus();
                return false;
            }

            if (currentYear < birthYear) {
                Toast.makeText(this, R.string.signin_birthday_wrong_year, Toast.LENGTH_LONG).show();
                this.etxBirthDay.requestFocus();
                return false;
            }

            //월을 잘라낸다
            int birthMonth = Integer.parseInt(this.etxBirthDay.getText().toString().substring(4, 6));
            if (birthMonth < 1 || birthMonth > 12) {
                Toast.makeText(this, R.string.signin_birthday_wrong_month, Toast.LENGTH_LONG).show();
                this.etxBirthDay.requestFocus();
                return false;
            }

            //일을 잘라낸다.
            int birthDay = Integer.parseInt(this.etxBirthDay.getText().toString().substring(6, 8));
            calendar.set(birthYear, birthMonth - 1, 1);
            int lastDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
            if (birthDay < 1 || birthDay > lastDay) {
                Toast.makeText(this, R.string.signin_birthday_wrong_day, Toast.LENGTH_LONG).show();
                this.etxBirthDay.requestFocus();
                return false;
            }

            if (this.tvHeight.getText().toString().trim().length() == 0) {
                Toast.makeText(this, this.getResources().getString(R.string.signin_height_input), Toast.LENGTH_LONG).show();
                this.tvHeight.requestFocus();
                return false;
            }

            if (this.tvWeight.getText().toString().trim().length() == 0) {
                Toast.makeText(this, this.getResources().getString(R.string.signin_weight_input), Toast.LENGTH_LONG).show();
                this.tvWeight.requestFocus();
                return false;
            }

            long longUtcTime = DateUtils.convertDateStringtoUTCTick(this.etxBirthDay.getText().toString() , "yyyyMMdd");
            this.mUserInfo.setBirthDate( longUtcTime);

            mUserInfo.setIncentiveUser(false);
        }else if( mStep == 2 ){
            if(this.isBtEnabled() == false)
            {
                Intent enableBtIntent = new Intent( BluetoothAdapter.ACTION_REQUEST_ENABLE );
                this.startActivityForResult(enableBtIntent , this.ENABLE_BT_REQUEST_ID);
                return false;
            }
        }else if( mStep ==4 ){
            if( !this.isSelectWearingLocation ){
                Toast.makeText(this, this.getString(R.string.signin_wearinglocation_select) , Toast.LENGTH_LONG ).show();
                return false;
            }
        }

        return true;
    }

    @Override
    public void onLeScan(final BluetoothDevice device, final int rssi, byte[] scanRecord) {

            String deviceName = device.getName();

            if (deviceName != null) {
                if (deviceName.equals(FITMETER_NAME)) {
                    BluetoothDeviceUtil.checkList(device, rssi);

                }
            }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void startScan()
    {
        this.isScaned = true;
        BluetoothDeviceUtil.clearList();
        this.mTimerValue = 0;
        this.mTimerHander.sendEmptyMessage(0);

    }

    private void stopScan()
    {
        this.mBluetoothAdapter.stopLeScan(this);
    }

    @Override
    public void moveHomeActivity() {
        this.finish();

        Intent intent = new Intent(SetUserInfoActivity.this, NewHomeActivity_.class );
        this.startActivity(intent);
    }

    @Override
    public void showResult(String message) {
        Toast.makeText(this,message,Toast.LENGTH_LONG).show();
    }

    @Override
    public void finishActivity() {
        this.finish();
    }

    private Date getDate(String dateString)
    {
        Date date = null;
        SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat( "yyyyMMdd", Locale.getDefault() );
        try
        {
            date =  mSimpleDateFormat.parse(dateString);
        }
        catch (ParseException e) {}

        return date;
    }

    private String getDateString( Date date ){
        String dateString = null;
        SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat( "yyyyMMdd", Locale.getDefault() );
        dateString =  mSimpleDateFormat.format(date);
        return dateString;
    }

    public void startProgressDialog( String title , String message ){
        this.mDialog =  new ProgressDialog(this);
        mDialog.setTitle(title);
        mDialog.setMessage( message );
        mDialog.setIndeterminate( true );
        mDialog.setCancelable( false );
        mDialog.show();
    }

    public void stopProgressDialog(){
        if( this.mDialog != null ) {
            this.mDialog.dismiss();
        }
    }

    @Override
    public void setDuplicateResult(boolean duplicateResult) {

    }

    String foundedAddress = "";

    private void checkFoundDevice(){
        this.stopScan();

        String [] s = BluetoothDeviceUtil.firstDeviceAddressStrings(SCAN_COUNT_VALUE, RSSI_VALUE);



        int foundDeviceSize = 0;
        if(s != null)
        {
            foundDeviceSize = s.length;
        }

        if( foundDeviceSize == 0 ){
            this.showDialog(this.getString(R.string.signin_device_find_result_no));

        }else if( foundDeviceSize == 1 ){
            foundedAddress = s[0];
            this.mUserInfo.setDeviceAddress((String) s[0]);
            moveFrontView();
        }else{
            this.showDialog(this.getString(R.string.signin_device_find_result_one_more));
        }
    }

    private void showDialog( String message ){
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);

        dialog.setTitle("")
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton( this.getString(R.string.signin_device_research) , new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //startScan();
                        mStep -= 1;
                        btnNext.setEnabled(true);
                        btnNext.setBackgroundColor(getResources().getColor(R.color.fitmate_green));
                        drawChildView();
                        dialog.dismiss();
                    }
                })
                .show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){

        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == ENABLE_BT_REQUEST_ID){
            if( resultCode == RESULT_OK ){
                moveFrontView();
            }else if(resultCode == RESULT_CANCELED){
                Toast.makeText(this , this.getString(R.string.signin_fitmeter_bluetooth_on) , Toast.LENGTH_LONG).show();
            }
        }

    }

    @Override
    public void onBackPressed() {
        this.moveBackView();
    }

    private boolean isBtEnabled()
    {
        final BluetoothManager manager = (BluetoothManager) this.getSystemService(Context.BLUETOOTH_SERVICE);
        if( manager == null ) return false;

        final BluetoothAdapter adapter = manager.getAdapter();
        if( adapter == null ) return false;

        return adapter.isEnabled();
    }

    private String[] getValueArray( float value ){
        String valueString = String.valueOf( value );
        String[] result = null;
        String[] splitedWeightString = valueString.split("\\.");

        if( splitedWeightString.length > 1 ){
            splitedWeightString[1] = splitedWeightString[1].substring(0,1);
            result = splitedWeightString;
        }else{
            result = new String[]{ splitedWeightString[0] , "0" };
        }

        return result;
    }

}