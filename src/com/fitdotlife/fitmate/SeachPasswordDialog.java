package com.fitdotlife.fitmate;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.fitdotlife.fitmate_lib.iview.IFindPasswordView;
import com.fitdotlife.fitmate_lib.presenter.FindPasswordPresenter;

public class SeachPasswordDialog implements OnClickListener, IFindPasswordView
{
	private Button btnSearch = null;
	private Button btnCancel = null;
	private ImageView imgClose = null;
	private EditText etxSearchContent = null;
	private Context mContext = null;
	private FindPasswordPresenter mPresenter = null;
	private Dialog mDialog = null;

	public SeachPasswordDialog( Context context  )
	{
		this.mContext = context;
		this.mPresenter = new FindPasswordPresenter( this.mContext , this );
	}

	public void show(){


		this.mDialog = new Dialog(this.mContext , R.style.CustomDialog);
		this.mDialog.setContentView( R.layout.dialog_search_password );

		this.etxSearchContent = (EditText) mDialog.findViewById(R.id.etx_dialog_search_password_search_content);

		this.btnSearch = (Button) mDialog.findViewById(R.id.btn_search_email);
		this.btnSearch.setOnClickListener(this);

		this.btnCancel = (Button) mDialog.findViewById(R.id.btn_search_email_dialog_close);
		this.btnCancel.setOnClickListener(this);

		this.imgClose = (ImageView) mDialog.findViewById(R.id.img_search_email_dialog_close);
		this.imgClose.setOnClickListener(this);

		this.mDialog.show();
	}

	@Override
	public void onClick(View view)
	{
		switch( view.getId() )
		{
		case R.id.btn_search_email:
			if( this.validInput() )
			{
				this.mPresenter.findUser( this.etxSearchContent.getText().toString() );
			}
			break;
		case R.id.btn_search_email_dialog_close:
			this.mDialog.dismiss();
			break;
		case R.id.img_search_email_dialog_close:
			this.mDialog.dismiss();
			break;
		}
	}

	@Override
	public void navigateToSearchDialog( String name )
	{
		SendPasswordDialog sendPasswordDialog = new SendPasswordDialog( this.mContext );
		sendPasswordDialog.show(name , this.etxSearchContent.getText().toString());

		this.mDialog.dismiss();
	}
	
	private boolean validInput()
	{
		if( this.etxSearchContent.getText().length() == 0 )
		{
			this.showMessage(this.mContext.getString(R.string.find_password_input_email));
			this.etxSearchContent.requestFocus();
			return false;
		}
		return true;
	}


	@Override
	public void showResult(String message)
	{
		this.showMessage(message);
	}

	@Override
	public void dismisDialog() {
		if( this.mDialog != null ){
			this.mDialog.dismiss();
		}
	}

	@Override
	public void startProgressDialog() {

	}

	@Override
	public void stopProgressDialog() {

	}

	private void showMessage( String message )
	{
		Toast.makeText(this.mContext, message, Toast.LENGTH_LONG).show();
	}
	
	
}
