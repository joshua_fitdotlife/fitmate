package com.fitdotlife.fitmate;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.StrengthInputType;
import com.fitdotlife.fitmate.model.UserInfoModel;
import com.fitdotlife.fitmate_lib.customview.AppliedProgramLayout;
import com.fitdotlife.fitmate_lib.customview.DrawerView;
import com.fitdotlife.fitmate_lib.customview.HomeCalDisView;
import com.fitdotlife.fitmate_lib.customview.HomeDayScoreChartView;
import com.fitdotlife.fitmate_lib.customview.HomeWeekScoreChartView;
import com.fitdotlife.fitmate_lib.customview.MenuPopupView;
import com.fitdotlife.fitmate_lib.customview.StripeView;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.iview.IHomeView;
import com.fitdotlife.fitmate_lib.key.ExerciseProgramType;
import com.fitdotlife.fitmate_lib.object.DayActivity;
import com.fitdotlife.fitmate_lib.object.ExerciseProgram;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.presenter.HomePresenter;
import com.fitdotlife.fitmate_lib.presenter.LoginPresenter;

import org.apache.log4j.Log;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;


public class HomeActivity extends Activity implements View.OnClickListener , IHomeView , View.OnTouchListener {


    private static final int PAGE_LEFT = 0;
    private static final int PAGE_MIDDLE = 1;
    private static final int PAGE_RIGHT = 2;

    private int mSelectedPageIndex = 1;

    private enum CALDISTYPE
    {
        CALORIE , DISTANCE
    }

    private final int PagerAnimationDuration = 4000;
    private CALDISTYPE mCalDisType = CALDISTYPE.CALORIE;
    private boolean isRequestAndView = false;

    private final int ANIMATION_DURATION = 10000;
    private static final int ENABLE_BT_REQUEST_ID = 1;

    private ViewPager mPager = null;
    private PagerRunnable pagerRunnable = null;
    private ViewPagerAdapter mPagerAdapter = null;

    private RelativeLayout llMenu = null;
    private LinearLayout llExercise = null;

    private int appliedExerciseProgramId = -1;
    private HomePresenter mPresenter = null;

    private AppliedProgramLayout aplProgramInfo = null;

    private HomeDayScoreChartView dayScoreChart = null;
    private HomeWeekScoreChartView weekScoreChart = null;

    private TextView tvDayScore = null;
    private TextView tvExtraScore = null;
    FitmateDBManager dbManager ;

    private TextView tvWeekScoreResult = null;
    private TextView tvTodayScoreResult = null;
    private TextView tvMoreScoreResult = null;
    private TextView tvExtraScoreResult = null;

    private DayActivity mDayActivity = null;

    private ProgressDialog mDialog = null;

    private LinearLayout llDataSyncContainer = null;
    private LinearLayout mDataSync = null;
    private TextView tvDataSync = null;

    private ImageView mImgBattery = null;
    private PopupWindow mPopupWindow;
    private MenuPopupView menuPopupView = null;
    private StripeView svDataSync = null;

    private Calendar recentConnectedDate;

    private Handler mHandler = new Handler();
    private DrawerLayout dlHome = null;
    private DrawerView drawerView = null;
    private String mTodayText = null;

    private boolean isDisconnectedStatusOverOneDay(){
        if( Calendar.getInstance().getTimeInMillis() -recentConnectedDate.getTimeInMillis() >86400000 ) return true;
        return false;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);


        PackageManager m = getPackageManager();
        String s = getPackageName();
        PackageInfo p = null;
        try {
            p = m.getPackageInfo(s, 0);
            s = p.applicationInfo.dataDir;

            //filePath = s + filePath;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }


        this.findViewById(R.id.parentView).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (mPopupWindow != null && mPopupWindow.isShowing()) {
                    mPopupWindow.dismiss();
                    mPopupWindow = null;
                    return true;
                }
                return false;
            }
        });

        File f = new File(".");
        File ff [] = f.listFiles();

        Log.d("AAA", "len " + ff.length);
        Log.i("fitmate", "HomeActivity - onCreate()");

        dbManager = new FitmateDBManager(this);

        this.mPresenter = new HomePresenter(this , this);

        ActivityManager.getInstance().addActivity(this);

        this.aplProgramInfo = (AppliedProgramLayout) this.findViewById(R.id.apl_activity_home_appliedprogram_info);


        //this.hcdView = (HomeCalDisView) this.findViewById(R.id.hcd_activity_home);
        //this.swipeChecker = new SwipeChecker(this,this.hcdView);
        //this.swipeChecker.setOnSwipeTouchListener(this);
        //this.tvDayCalDisValue = (TextView) this.findViewById(R.id.tv_activity_home_caldis_value);
        //this.tvTodayDate = (TextView) this.findViewById(R.id.tv_activity_home_caldis_date);
        //this.tvDayCalDisTitle = (TextView) this.findViewById(R.id.tv_activity_home_caldis_title);

        this.mPager = (ViewPager) this.findViewById(R.id.vp_activity_home);
        this.mPager.setPageTransformer(true, new PagerTransformer());
        this.mPager.setOnTouchListener(this);

        try {
            Field mScroller;
            mScroller = ViewPager.class.getDeclaredField("mScroller");
            mScroller.setAccessible(true);
            //Interpolator interpolator = AnimationUtils.loadInterpolator(this , android.R.anim.overshoot_interpolator);
            AccelerateOvershootInterpolator interpolator = new AccelerateOvershootInterpolator(2.0f,  1.0f);
            FixedSpeedScroller scroller = new FixedSpeedScroller( mPager.getContext(), interpolator );
            // scroller.setFixedDuration(5000);
            mScroller.set(mPager, scroller);
        } catch (NoSuchFieldException e) {
        } catch (IllegalArgumentException e) {
        } catch (IllegalAccessException e) {
        }

        this.pagerRunnable = new PagerRunnable();

        Calendar todayCalendar = Calendar.getInstance();
        todayCalendar.setTimeInMillis( System.currentTimeMillis() );
        String dateString = null;
        SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat( this.getString(R.string.home_calorie_date_text) , Locale.getDefault() );
        dateString =  mSimpleDateFormat.format(todayCalendar.getTime());
        this.mTodayText =  dateString;


        this.llMenu = (RelativeLayout) this.findViewById( R.id.home_global_menu );

        //운동 프로그램 이동으로 이동을 위해
        //this.llExercise = (LinearLayout) this.findViewById(R.id.ll_activity_home_exercise);
        //this.llExercise.setOnClickListener(this);

        this.dayScoreChart = (HomeDayScoreChartView) this.findViewById(R.id.cv_activity_home_day_score_chart);
        this.weekScoreChart = (HomeWeekScoreChartView) this.findViewById(R.id.cv_activity_home_week_score_chart);
        //this.weekScoreChart.setScore( 43,11 );
        this.weekScoreChart.startAnimation( this.ANIMATION_DURATION );

        this.tvDayScore = (TextView) this.findViewById(R.id.tv_activity_home_day_score);
        this.tvExtraScore = (TextView) this.findViewById(R.id.tv_activity_home_extra_score);

        this.tvWeekScoreResult = (TextView) this.findViewById(R.id.tv_activity_home_week_score_result);
        this.tvTodayScoreResult = (TextView) this.findViewById(R.id.tv_activity_home_today_score_result);
        this.tvMoreScoreResult = (TextView) this.findViewById(R.id.tv_activity_home_week_score_more);

        this.tvExtraScoreResult = (TextView) this.findViewById(R.id.tv_activity_home_extra_score_result);

        this.svDataSync = (StripeView) this.findViewById(R.id.sv_activity_home_data_sync);


        this.dlHome = (DrawerLayout) this.findViewById(R.id.dl_activity_home);
        this.drawerView = (DrawerView) this.findViewById(R.id.dv_activity_home_drawer);
        this.drawerView.setParentActivity(this);

        //액션바 설정
        View actionBarView = this.findViewById(R.id.ic_activity_home_actionbar);
        ImageView imgLeft = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_left);
        imgLeft.setVisibility(View.GONE);
        final ImageView imgSliding = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_right);
        imgSliding.setImageResource(R.drawable.icon_slide);
        imgSliding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dlHome.openDrawer(drawerView);

                if (mPopupWindow != null && mPopupWindow.isShowing()) {
                    mPopupWindow.dismiss();
                    mPopupWindow = null;
                }
            }
        });
        imgSliding.setOnTouchListener(this);

        mImgBattery = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_right_more);
        mImgBattery.setVisibility(View.VISIBLE);
        mImgBattery.setImageResource(R.drawable.battery1);
        mImgBattery.setOnClickListener(this);

        String email = dbManager.getUserInfo().getEmail();
        long time=  dbManager.getLastConnectedTime(email);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);

        int batteryRatio =dbManager.getLastBatteryRatio(email);

        setRecentConnectedTime( calendar);
        setBattery(batteryRatio);

        new Thread(new Runnable() {
            @Override
            public void run() {
                // todo 사진 관련 DB 정보는 서버에서 가져오지 않도록 수정 필요. 홈 엑티비티 켜졌을 때 한번만 확인하도록..

                UserInfo userInfo = dbManager.getUserInfo();
                String email = userInfo.getEmail();
                String pwd = userInfo.getPassword();

                try {
                    String ss = UserInfoModel.getUserInfo(email, pwd);

                    if(!ss.equals(""))
                    {
                        if(UserInfo.getPhotoUrlSharedPreference(getApplicationContext()) == null || (UserInfo.getPhotoUrlSharedPreference(getApplicationContext()) != null &&  !UserInfo.getPhotoUrlSharedPreference(getApplicationContext()).equals(ss)))
                        {
                            //UserInfoModel.getImage(ss, getApplicationContext());
                            //userInfo.setAccountPhoto(ss);
                            UserInfo.savePhotoUrlInSharedPreference(getApplicationContext(), ss);
                            userInfo.setAccountPhoto(ss);
                            dbManager.modifyUserInfo(userInfo);
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
        }).start();
    }

    private long startDateForGetIncentive;


    public Date resetTime (Date d) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(d);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    private boolean isTodaySuccessAlready=false;


    @Override
    protected void onResume() {
        super.onResume();
        Log.i("fitmate", "HomeActivity - onResume()");

        this.isRequestAndView = true;

        if( !this.getCheckBluetoothOn() ) { // 블루투스를 체크 했는지.

            if (!isBtEnabled()) { //블루투스가 켜져 있지 않다면

                if( !this.getRequestBluetoothOn() ) {

                    Log.i("fitmate", "HomeActivity - request bluetooth on");
                    Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    this.startActivityForResult(enableBtIntent, this.ENABLE_BT_REQUEST_ID);
                    this.setRequestBluetoothOn(true);
                }

            }else{
                this.showSyncView();
                this.startTimer();
                this.setCheckBluetoothOn(true);
                this.mPresenter.resetGetData();
                this.mPresenter.requestCalculateActivity();
            }
        }else{
            this.mPresenter.getDayActivity();
        }
    }


    @Override
    protected void onPause()
    {
        super.onPause();
        Log.i("fitmate", "HomeActivity - onStop()");
        this.isRequestAndView = false;

        if( this.mDialog != null ){
            this.mDialog.dismiss();
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        this.isRequestAndView = false;
        Log.i("fitmate", "HomeActivity - onStop()");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i("fitmate", "HomeActivity - onStart()");
    }


    @Override
    public void onClick(View view) {
        switch( view.getId() ){
            case R.id.img_custom_action_bar_right_more:
                if (mPopupWindow != null && mPopupWindow.isShowing()) {
                    return;

                }

                View popupView = getLayoutInflater().inflate(R.layout.connected_device_popup, null);


                /**
                 * LayoutParams WRAP_CONTENT를 주면 inflate된 View의 사이즈 만큼의
                 * PopupWinidow를 생성한다.
                 */
                mPopupWindow = new PopupWindow(popupView,
                        WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);


                /**
                 * showAsDropDown(anchor, xoff, yoff)
                 * @View anchor : anchor View를 기준으로 바로 아래 왼쪽에 표시.
                 * @예외 : 하지만 anchor View가 화면에 가장 하단 View라면 시스템이
                 * 자동으로 위쪽으로 표시되게 한다.
                 * xoff, yoff : anchor View를 기준으로 PopupWindow가 xoff는 x좌표,
                 * yoff는 y좌표 만큼 이동된 위치에 표시되게 한다.
                 * @int xoff : -숫자(화면 왼쪽으로 이동), +숫자(화면 오른쪽으로 이동)
                 * @int yoff : -숫자(화면 위쪽으로 이동), +숫자(화면 아래쪽으로 이동)
                 * achor View 를 덮는 것도 가능.
                 * 화면바깥 좌우, 위아래로 이동 가능. (짤린 상태로 표시됨)
                 */

                mPopupWindow.setAnimationStyle(-1); // 애니메이션 설정(-1:설정, 0:설정안함)
                //int xOffset = (int)TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP , 75 , this.getResources().getDisplayMetrics() );

                final float scale = getResources().getDisplayMetrics().density;

            //    mPopupWindow.showAsDropDown(view, 0,0);
             //   mPopupWindow.showAsDropDown(view, (int) (-200* scale + 0.5f) , (int) (-14* scale + 0.5f));


                mPopupWindow.showAtLocation(view, Gravity.NO_GRAVITY,(int)(10* scale + 0.5f), (int)(50* scale + 0.5f) );

                /**
                 * showAtLocation(parent, gravity, x, y)
                 * @praent : PopupWindow가 생성될 parent View 지정
                 * View v = (View) findViewById(R.id.btn_click)의 형태로 parent 생성
                 * @gravity : parent View의 Gravity 속성 지정 Popupwindow 위치에 영향을 줌.
                 * @x : PopupWindow를 (-x, +x) 만큼 좌,우 이동된 위치에 생성
                 * @y : PopupWindow를 (-y, +y) 만큼 상,하 이동된 위치에 생성
                 */
//          mPopupWindow.showAtLocation(popupView, Gravity.NO_GRAVITY, 0, 0);

                //      mPopupWindow.showAtLocation(popupView, Gravity.LEFT, 0, 0);
                TextView textView_date=(TextView)popupView.findViewById(R.id.connectedTime);
                TextView textView_battery=(TextView)popupView.findViewById(R.id.batteryRemainRate);
                ((ImageView) popupView.findViewById(R.id.popupClose)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mPopupWindow != null && mPopupWindow.isShowing()) {
                            mPopupWindow.dismiss();
                            mPopupWindow=null;
                        }
                    }
                });


               FitmateDBManager dbManager = new FitmateDBManager(this);

                String email = dbManager.getUserInfo().getEmail();
               long time=  dbManager.getLastConnectedTime(email);

               Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(time);


                int batteryRatio =dbManager.getLastBatteryRatio(email);

                setRecentConnectedTime( calendar);
                setBattery(batteryRatio);

                /**
                 * update() 메서드를 통해 PopupWindow의 좌우 사이즈, x좌표, y좌표
                 * anchor View까지 재설정 해줄수 있습니다.
                 */
//          mPopupWindow.update(anchor, xoff, yoff, width, height)(width, height);

                break;
            case R.id.ll_child_applied_program_info_exercise_info:

                Intent intent = new Intent( HomeActivity.this , ExerciseProgramInfoActivity_.class );
                intent.putExtra( ExerciseProgram.ID_KEY, this.appliedExerciseProgramId );
                intent.putExtra("exercisetype" , ExerciseProgramType.USEREXERCISEPROGRAM );
                this.startActivity( intent );
                break;
            default:
                break;
        }
    }


    private void setBattery(int batteryRatio){

        int imageId;

        if(isDisconnectedStatusOverOneDay())
        {
            imageId = R.drawable.battery_disabled;
        }
        else if(batteryRatio<20){
            imageId= R.drawable.battery1;
        }
        else if(batteryRatio<40){
            imageId = R.drawable.battery2;
        }else if(batteryRatio<60){
            imageId = R.drawable.battery3;
        }
        else if(batteryRatio<80){
            imageId = R.drawable.battery4;
        }
        else{
            imageId = R.drawable.battery5;
        }


        mImgBattery.setImageResource(imageId);

        if(mPopupWindow!=null){
            View popupView = mPopupWindow.getContentView();

            TextView textView_battery_title=(TextView)popupView.findViewById(R.id.batteryRemainRateTitle);
            TextView textView_battery=(TextView)popupView.findViewById(R.id.batteryRemainRate);
            textView_battery.setText(" " + batteryRatio + "%");

            if(batteryRatio==-1) {
                textView_battery.setText(this.getString(R.string.home_not_connection));
            }

            if(imageId==R.drawable.battery_disabled)
            {
                int colorid = getResources().getColor(R.color.connectedDevicetextColor_disabled);
                textView_battery.setTextColor(colorid);
                textView_battery_title.setTextColor(colorid);

            }else{
                int colorid = getResources().getColor(R.color.connectedDevicetextColor);
                textView_battery.setTextColor(colorid);
                textView_battery_title.setTextColor(colorid);
            }
        }
    }


    private void setRecentConnectedTime(Calendar calendar){
        recentConnectedDate= calendar;
        //하루 이상 차이나면 연결 아이콘을 disabled로 변경
        if(isDisconnectedStatusOverOneDay() )
        {
            if(mImgBattery !=null) mImgBattery.setImageResource(R.drawable.battery_disabled);
        }
        if(mPopupWindow!=null) {
            View popupView = mPopupWindow.getContentView();
            TextView textView_date = (TextView) popupView.findViewById(R.id.connectedTime);
            Locale current = getResources().getConfiguration().locale;
            String language = current.getLanguage();
            if (language.equals("ko")) {
                SimpleDateFormat format3 = new SimpleDateFormat("yyyy년 MM월 dd일 HH시 mm분 ss초");
                String dateString=format3.format(calendar.getTime());
                if(dateString.contains("1970")){
                    dateString=this.getString(R.string.home_not_connection);
                }


                textView_date.setText(dateString);

            } else {
                SimpleDateFormat format3 = new SimpleDateFormat("HH:mm:ss dd, MMM, yyyy");
                String dateString=format3.format(calendar.getTime());
                if(dateString.contains("1970")){
                    dateString="didn't have ever connected";
                }

                textView_date.setText(dateString);
            }
        }
    }

    @Override
    public void displayExerciseProgram(ExerciseProgram appliedExerciseProgramSNU) {
        if( appliedExerciseProgramSNU.getId() != this.appliedExerciseProgramId )
        {
            this.appliedExerciseProgramId = appliedExerciseProgramSNU.getId();
            this.aplProgramInfo.setOnClickListener(this);
            this.aplProgramInfo.setAppliedExerciseProgram(appliedExerciseProgramSNU);
        }
    }


//    @Override
//    public void displayDayAchievement( double calorie , int achieveOfTarget , ExerciseProgram program , int extraScore )
//    {
    @Override
    public void displayFatBurn(float fatBurn, float carbonBurn)
    {
        TextView fat = (TextView)findViewById(R.id.textView_fatBurn);
        if( fatBurn >= 100) {
            fat.setText(String.format("%d g", Math.round(fatBurn)));
        }else{
            fat.setText(String.format("%.1f g", fatBurn));
        }

        TextView carbon = (TextView)findViewById(R.id.textView_carbonBurn);
        if( carbonBurn >= 100 ) {
            carbon.setText(String.format("%d g", Math.round(carbonBurn)));
        }else{
            carbon.setText(String.format("%.1f g", carbonBurn));
        }

    }

    @Override
    public void displayDayAchievement( int rangeFrom , int rangeTo , double value , StrengthInputType strengthType , int extraScore ){

        String rateText = "";
        String statusText = "";
        String valueUnit = "";
        String titleText = "";

        if(strengthType.equals( StrengthInputType.CALORIE_SUM) || strengthType.equals( StrengthInputType.CALORIE ) || strengthType.equals(StrengthInputType.VS_BMR)){

            valueUnit  = this.getString(R.string.common_calorie);
            titleText = this.getString(R.string.home_today_bured_calorie);

        }else{
            valueUnit  = this.getString(R.string.common_minute);
            titleText = this.getString(R.string.home_activity_time);
        }


        if(value < rangeFrom){
            if(strengthType.equals( StrengthInputType.CALORIE_SUM) || strengthType.equals( StrengthInputType.CALORIE ) || strengthType.equals(StrengthInputType.VS_BMR)){
                statusText = String.format( this.getString(R.string.home_more_calorie) , (int)( rangeFrom - value ));
                rateText = String.format( this.getString(R.string.home_today_gain_point) , extraScore );
            }else{
                statusText = String.format( this.getString(R.string.home_more_exercise) , (int)( rangeFrom - value ));
                rateText = String.format( this.getString(R.string.home_today_gain_point) , extraScore );
            }
        }else if( value >= rangeFrom ){
            statusText = this.getString(R.string.home_achieve_goal);
            rateText = this.getString(R.string.home_conguraturation);
        }

        this.dayScoreChart.setRange( rangeFrom , rangeTo );
        this.dayScoreChart.setValueUnit(valueUnit);
        this.dayScoreChart.setAchievementValue(value);
        this.dayScoreChart.setTitleText( titleText );
        this.dayScoreChart.setStatusText(statusText);
        this.dayScoreChart.setRateText(rateText);
        this.dayScoreChart.startAnimation();
    }

    @Override
    public void displayWeekAchievement( int predayScore , int todayScore , boolean isAchieveMax , int possibleMaxScore , int possibleTimes , int possibleValue , StrengthInputType strengthType  ) {

        this.weekScoreChart.setScore( predayScore , todayScore );
        this.weekScoreChart.setValueUnit(this.getString(R.string.common_point));
        this.weekScoreChart.startAnimation(this.ANIMATION_DURATION);

        //오늘 성취
        this.tvDayScore.setText(String.format(getString(R.string.home_today_achievement_point), todayScore));
        //이번주 성취
        this.tvExtraScore.setText( String.format( this.getString(R.string.home_week_achievement_point) , ( predayScore + todayScore ) ) );

        //this.tvTodayScoreResult.setText( "오늘은 " + todayScore + "점을 획득하셨군요." );
        this.tvTodayScoreResult.setText(String.format(this.getString(R.string.home_today_got_point), todayScore));
        //이번주 운동점수는 총 54점입니다.
        //this.tvWeekScoreResult.setText( "오늘까지 " + (predayScore + todayScore ) + "점을 달성했습니다." );
        this.tvWeekScoreResult.setText(String.format(this.getString(R.string.home_week_score_total), predayScore + todayScore));
        this.tvMoreScoreResult.setText(String.format(this.getString(R.string.home_get_more_point), 100 - (predayScore + todayScore)));

        int maxScore = 100;
        if( !isAchieveMax ){ maxScore = possibleMaxScore; }
        String extraText = null;

        if( strengthType.equals( StrengthInputType.CALORIE ) || strengthType.equals(StrengthInputType.VS_BMR) ){
            extraText = String.format( this.getString( R.string.home_calorie_attain_high_point ) , possibleTimes + this.getString(R.string.common_day) + " " + possibleValue + "kcal"  ,  maxScore );
        }else if( strengthType.equals( StrengthInputType.CALORIE_SUM ) ){
            extraText = String.format( this.getString( R.string.home_calorie_attain_high_point ) , possibleValue + "kcal"  , maxScore );
        }else {
            extraText = String.format( this.getString( R.string.home_exercise_attain_high_point ) , possibleTimes + this.getString(R.string.common_days) + " " + possibleValue + this.getString(R.string.common_minute)  , maxScore);
        }
        this.tvExtraScoreResult.setText(extraText);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if( requestCode == ENABLE_BT_REQUEST_ID )
        {
            Log.i("fitmate" , "HomeActivity - response bluetooth" +  resultCode );
            this.setCheckBluetoothOn(true);
            if( resultCode == RESULT_CANCELED ){
                this.setInitialViewComplete(true);
                this.mPresenter.requestCalculateActivity();
            }else if(resultCode == RESULT_OK){
                this.showSyncView();
                this.startTimer();
            }
        }
        else if(requestCode == DrawerView.REQUEST_PROFILE_IMAGE){

            this.drawerView.refreshProfileImage();
        }
    }

    @Override
    public void setCalDisValue( double calorie , double distance ){

        if( this.mPagerAdapter != null ){

            this.mPagerAdapter.updateValues( calorie, distance );

        }else{

            this.mPagerAdapter =  new ViewPagerAdapter(this.getApplicationContext(), calorie, distance , mTodayText );
            this.mPager.setAdapter(mPagerAdapter);
            this.mPager.setCurrentItem(2* 1000 );

            pagerRunnable.setPosition(mPager.getCurrentItem());
            this.mPager.postDelayed(pagerRunnable, PagerAnimationDuration);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("fitmate" , "HomeActivity - onDestroy()" );
        this.mPresenter.close();
        this.mPresenter = null;
    }


    @Override
    public void onSyncEnd() {
        //this.llDataSyncContainer.removeAllViews();
        //this.mDataSync = null;
        this.svDataSync.setVisibility(View.GONE);
        if( isRequestAndView ) {
            this.mPresenter.requestCalculateActivity();
            setRecentConnectedTime( Calendar.getInstance() );
        }

    }

    @Override
    public void onSyncProgress(int progressPercent) {

        if( this.svDataSync.getVisibility() == View.GONE ){
            this.svDataSync.setVisibility(View.VISIBLE);
        }

        this.svDataSync.setText( this.getString(R.string.home_download_analyzing) + " " + progressPercent + "%");
    }

    public void dismissSyncView(){
        if( this.svDataSync.getVisibility() == View.VISIBLE ) {
            this.svDataSync.setVisibility(View.GONE);
        }
    }

    public void showSyncView(){
        if( !this.getInitialViewComplete() ) {

            if (this.svDataSync.getVisibility() == View.GONE) {
                this.svDataSync.setVisibility(View.VISIBLE);
            }

            this.svDataSync.setText(this.getString(R.string.home_initializing));
        }
    }

    @Override
    public void onBatteryStatusChange(int batteryRatio) {
        setBattery(batteryRatio);
    }

    private boolean isBtEnabled()
    {
        final BluetoothManager manager = (BluetoothManager) this.getSystemService(Context.BLUETOOTH_SERVICE);
        if( manager == null ) return false;

        final BluetoothAdapter adapter = manager.getAdapter();
        if( adapter == null ) return false;
        return adapter.isEnabled();
    }

    @Override
         protected void onRestart() {
        super.onRestart();
        Log.i("fitmate", "HomeActivity - onRestart()");

        this.setServerUpdate(true);
        this.mPresenter.requestCalculateActivity();
    }

    private void setServerUpdate( boolean serverUpdate ){
        SharedPreferences pref = this.getSharedPreferences( "fitmateservice" , Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(DayActivity.DAY_ACTIVITY_SERVER_UPDATE_KEY, serverUpdate);
        editor.commit();
    }

    private void setCheckBluetoothOn( boolean checkBluetoothOn ){
        SharedPreferences pref = this.getSharedPreferences("fitmateservice", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(LoginPresenter.CHECK_BLUETOOTHON_KEY, checkBluetoothOn);
        editor.commit();
    }

    private Boolean getCheckBluetoothOn(  ){
        SharedPreferences pref = this.getSharedPreferences("fitmateservice", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        return pref.getBoolean(LoginPresenter.CHECK_BLUETOOTHON_KEY, false);
    }

    private void setRequestBluetoothOn( boolean requestBluetoothOn ){
        SharedPreferences pref = this.getSharedPreferences("fitmateservice", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(LoginPresenter.REQUEST_BLUETOOTHON_KEY, requestBluetoothOn);
        editor.commit();
    }

    private Boolean getRequestBluetoothOn(  ){
        SharedPreferences pref = this.getSharedPreferences("fitmateservice", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        return pref.getBoolean(LoginPresenter.REQUEST_BLUETOOTHON_KEY, false);
    }

    private Boolean getInitialViewComplete(  ){
        SharedPreferences pref = this.getSharedPreferences("fitmateservice", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        return pref.getBoolean(LoginPresenter.INIT_VIEW_COMPLETE_KEY, false);
    }

    private void setInitialViewComplete( boolean initViewComplete ){
        SharedPreferences pref = this.getSharedPreferences("fitmateservice", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean( LoginPresenter.INIT_VIEW_COMPLETE_KEY, initViewComplete);
        editor.commit();
    }

    private void startTimer(){
        this.mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                setInitialViewComplete(true);
                svDataSync.setVisibility(View.GONE);
            }
        }, 30000);
    }

    @Override
    public void onBackPressed() {

        if(dlHome.isDrawerOpen(drawerView)){
            dlHome.closeDrawers();
            return;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);     // 여기서 this는 Activity의 this

// 여기서 부터는 알림창의 속성 설정
        builder.setTitle(getString(R.string.common_quit_title))        // 제목 설정
                .setMessage(getString(R.string.common_quit_content))        // 메세지 설정
                .setCancelable(true)        // 뒤로 버튼 클릭시 취소 가능 설정
                .setPositiveButton(R.string.common_yes, new DialogInterface.OnClickListener() {
                // 확인 버튼 클릭시 설정
                public void onClick(DialogInterface dialog, int whichButton) {
                    moveTaskToBack(true);

                    finish();

                    //  android.os.Process.killProcess(android.os.Process.myPid());
                    dialog.cancel();

                    //   MainActivity.this.onBackPressed();
                }
                })
                .setNegativeButton(R.string.common_no, new DialogInterface.OnClickListener() {
                    // 취소 버튼 클릭시 설정
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.cancel();
                    }
            });


        AlertDialog dialog = builder.create();    // 알림창 객체 생성
        dialog.show();    // 알림창 띄우기

    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {

        if(view.getId() == R.id.vp_activity_home) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                mPager.removeCallbacks(pagerRunnable);
            } else if (event.getAction() == MotionEvent.ACTION_UP) {
                pagerRunnable.setPosition(mPager.getCurrentItem());
                mPager.postDelayed(pagerRunnable, PagerAnimationDuration);
            }
        }else{

            ImageView actionbarImageView = (ImageView) view;
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                actionbarImageView.setColorFilter(this.getResources().getColor(R.color.selected_colorfilter) , PorterDuff.Mode.DARKEN );
            } else if (event.getAction() == MotionEvent.ACTION_UP) {
                actionbarImageView.setColorFilter(null);
            }
        }

        return false;
    }

    //칼로리와 이동거리 뷰 관련....
    private class ViewPagerAdapter extends PagerAdapter {

        private LayoutInflater mInflater;
        private double mCalorie;
        private double mDistance;
        private String mCalorieText;
        private String mTodayText;
        private String mCalorieUnitText;
        private String mCalorieTitle;
        private String mDistanceTitle;
        private Drawable mCalorieDrawable;
        private Drawable mDistanceDrawable;
        private FitmateDBManager mDBManager;
        private View[] arrView = new View[3];

        public ViewPagerAdapter(Context applicationContext, double calorie, double distance, String todayText)
        {
            super();
            this.mInflater = LayoutInflater.from(applicationContext);
            this.mCalorie = calorie;
            this.mDistance = distance;
            this.mTodayText = todayText;
            this.mCalorieUnitText = applicationContext.getString(R.string.home_burned_calorie_unit);
            this.mDistanceTitle = getString(R.string.home_distance);
            this.mCalorieTitle = getString(R.string.home_burned_calorie);
            this.mCalorieDrawable = getResources().getDrawable(R.drawable.home_bg_kcal);
            this.mDistanceDrawable = getResources().getDrawable(R.drawable.home_bg_distance);
            this.mDBManager = new FitmateDBManager(applicationContext);

            setCalorieText();
        }

        private void setCalorieText(){
            if( mCalorie < 1 ){
                mCalorie = (mCalorie * 10d )/10d;
                mCalorieText = (int)mCalorie + "";
            } else if(mCalorie == 1){
                mCalorieText = (int)mCalorie + "";
            } else if(mCalorie > 1) {
                mCalorieText = (int)mCalorie + "";
            }
        }

        @Override
        public int getCount() {
            return Integer.MAX_VALUE;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View view = null;

            int realPos = ( position % 2 ) + 1;

            view = mInflater.inflate(R.layout.child_activity_home_hcd , null);
            HomeCalDisView hcdView = (HomeCalDisView) view.findViewById(R.id.child_activity_home_hcd);
            TextView tvDate = (TextView) view.findViewById(R.id.tv_activity_home_caldis_date);
            TextView tvTitle = (TextView) view.findViewById(R.id.tv_activity_home_caldis_title);
            TextView tvValue = (TextView) view.findViewById(R.id.tv_activity_home_caldis_value);
            TextView tvValueUnit = (TextView) view.findViewById(R.id.tv_activity_home_caldis_value_unit);


            if( realPos == 1 ){
                hcdView.setImage(mCalorieDrawable );
                tvTitle.setText( mCalorieTitle);
                tvDate.setText( mTodayText );
                tvValue.setText( mCalorieText );
                tvValueUnit.setText( mCalorieUnitText );
                this.arrView[realPos] = view;
            }
            else if(realPos == 2 )
            {
                String distanceText = null;
                String distanceUnitText = null;
                hcdView.setImage(mDistanceDrawable);
                tvTitle.setText( mDistanceTitle);
                tvDate.setText(mTodayText);
                UserInfo userInfo = mDBManager.getUserInfo();

                if(userInfo.isSI()){

                    if( mDistance >= 1 ) {
                        distanceText = String.format("%.2f", mDistance);
                        distanceUnitText = getString(R.string.common_kilometer);
                    }else {
                        distanceText = String.format("%d", Math.round(mDistance * 1000));
                        distanceUnitText = getString(R.string.common_meter);
                    }

                }else{

                    //킬로미터를 야드로 변환한다.
                    double yard = mDistance * 1093.61;
                    if( yard >= 1760 ){
                        distanceText = String.format("%.2f",  yard / 1760 );
                        distanceUnitText = getString(R.string.common_mile);
                    }else{
                        distanceText = String.format("%d", Math.round(yard));
                        distanceUnitText = getString(R.string.common_yard);
                    }
                }

                tvValue.setText( distanceText );
                tvValueUnit.setText( distanceUnitText );

                this.arrView[realPos] =view;
            }

            container.addView(view);

            return view;
        }

        public void updateValues( double calorie , double distance )
        {
            View calorieView = this.arrView[1];
            View distanceView = this.arrView[2];
            this.mCalorie = calorie;
            this.mDistance = distance;

            setCalorieText();
            TextView tvCalorieValue = (TextView) calorieView.findViewById(R.id.tv_activity_home_caldis_value);
            tvCalorieValue.setText( mCalorieText );

            String distanceText = null;
            String distanceUnitText = null;
            UserInfo userInfo = mDBManager.getUserInfo();
            if(userInfo.isSI()){

                if( mDistance >= 1 ) {
                    distanceText = String.format("%.2f", mDistance);
                    distanceUnitText = getString(R.string.common_kilometer);
                }else {
                    distanceText = String.format("%d", Math.round(mDistance * 1000));
                    distanceUnitText = getString(R.string.common_meter);
                }

            }else{

                //킬로미터를 야드로 변환한다.
                double yard = mDistance * 1093.61;
                if( yard >= 1760 ){
                    distanceText = String.format("%.2f",  yard / 1760 );
                    distanceUnitText = getString(R.string.common_mile);
                }else{
                    distanceText = String.format("%d", Math.round(yard));
                    distanceUnitText = getString(R.string.common_yard);
                }
            }

            TextView tvDistanceValue = (TextView) distanceView.findViewById(R.id.tv_activity_home_caldis_value);
            tvDistanceValue.setText( distanceText );
            TextView tvDistanceValueUnit = (TextView) distanceView.findViewById(R.id.tv_activity_home_caldis_value_unit);
            tvDistanceValueUnit.setText( distanceUnitText );
        }
    }

    private class PagerRunnable implements  Runnable
    {

        int mPosition = 0;

        @Override
        public void run() {

            mPager.setCurrentItem(mPosition);
            mPosition--;
            mPager.postDelayed(pagerRunnable , PagerAnimationDuration );
        }

        public void setPosition( int position ){
            this.mPosition = position;
        }
    }

    private class PagerTransformer implements ViewPager.PageTransformer
    {

        private static final float MIN_SCALE = 0.75f;

        public void transformPage(View view, float position) {
            int pageWidth = view.getWidth();

            if (position < -1) { // [-Infinity,-1)
                // This page is way off-screen to the left.
                view.setAlpha(0);

            } else if (position <= 0) { // [-1,0]
                // Use the default slide transition when moving to the left page
                view.setAlpha(1);
                view.setTranslationX(0);
                view.setScaleX(1);
                view.setScaleY(1);

            } else if (position <= 1) { // (0,1]
                // Fade the page out.
                view.setAlpha(1 - position);

                // Counteract the default slide transition
                //view.setTranslationX(pageWidth * -position);
                view.setTranslationX((position) * (pageWidth / 4));

                // Scale the page down (between MIN_SCALE and 1)
                float scaleFactor = MIN_SCALE
                        + (1 - MIN_SCALE) * (1 - Math.abs(position));
                view.setScaleX(scaleFactor);
                view.setScaleY(scaleFactor);

            } else { // (1,+Infinity]
                // This page is way off-screen to the right.
                view.setAlpha(0);
            }
        }
    }

}
