package com.fitdotlife.fitmate;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.fitdotlife.fitmate_lib.customview.AppliedProgramSummaryLayout;
import com.fitdotlife.fitmate_lib.customview.DrawerView;
import com.fitdotlife.fitmate_lib.http.NetworkClass;
import com.fitdotlife.fitmate_lib.image.ImageUtil;
import com.fitdotlife.fitmate_lib.iview.IMyProfileView;
import com.fitdotlife.fitmate_lib.key.CommonKey;
import com.fitdotlife.fitmate_lib.key.GenderType;
import com.fitdotlife.fitmate_lib.object.ExerciseProgram;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.presenter.MyProfilePresenter;
import com.fitdotlife.fitmate_lib.util.Utils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;


public class MyProfileActivity extends Activity implements IMyProfileView, View.OnClickListener {

    private final String TAG = "fitmate - " + MyProfileActivity.class.getSimpleName();

    private final int USERINFO_REQUEST_CODE = 0;
    private final int USERINFO_CHANGEDEVICE_CODE = 1;
    private final int USERINFO_CHANGELOCATION_CODE = 2;
    private final int USERINFO_CHANGEALARM_CODE = 3;
    protected static final int CAMERA_REQUEST = 4;
    private Activity mActivity = null;
    //private LinearLayout llMenu = null;
    private ImageView imgProfile = null;

    private LinearLayout llUserInfo = null;
    private TextView tvName = null;
    private ImageView imgGender = null;
    private ImageView imgGenderDot = null;
    private TextView tvBirthDate = null;
    private ImageView imgBirthDateDot = null;
    private TextView tvEmail = null;
    private TextView tvIntro = null;
    private TextView tvProgramName = null;
    private AppliedProgramSummaryLayout appliedProgramLayout = null;


    private TextView tvHeight = null;
    private TextView tvAlarm = null;
    private TextView tvWeight = null;
    private TextView tvVO2MaxLevel = null;


    private FrameLayout flAlarm = null;
    private FrameLayout flHeight = null;
    private FrameLayout flWeight = null;
    private FrameLayout flVO2MaxLevel = null;
    private FrameLayout flWearingLocation = null;
    private FrameLayout flChangeFitmeter = null;
    private FrameLayout flLogout = null;

    private TextView tvWearingLocation = null;
    private TextView tvChangeFitmeter = null;
    private ImageView imgWearingLocation = null;

    private MyProfilePresenter mPresenter = null;
    private UserInfo mUserInfo = null;

    private boolean isShowing = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);

        this.mPresenter = new MyProfilePresenter(this , this);
        ActivityManager.getInstance().addActivity(this);

        this.mActivity = this;
        this.mUserInfo = this.mPresenter.getUserInfo();

        //this.llMenu = (LinearLayout) this.findViewById(R.id.activity_global_menu);

        this.llUserInfo = (LinearLayout) this.findViewById(R.id.ll_activity_my_profile_user_info);
        this.llUserInfo.setOnClickListener(this);

        final Activity act = this;

        this.imgProfile = (ImageView) this.findViewById(R.id.img_activity_my_profile_picture);
        imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!Utils.isOnline(act) ) {
                    //mView.showResult("인터넷을 연결하여 주십시오.");
                    Toast.makeText(act, getString(R.string.common_connect_network), Toast.LENGTH_SHORT).show();
                    return;
                }

                Intent i = new Intent(MyProfileActivity.this, ImageUtil.class);
                startActivityForResult(i, CAMERA_REQUEST);
            }
        });

        this.tvName = (TextView) this.findViewById(R.id.tv_activity_my_profile_name);
        this.tvName.setText( this.mUserInfo.getName() );


        this.imgGenderDot = (ImageView) this.findViewById(R.id.img_activity_my_profile_gender_dot);
        this.imgGender = (ImageView) this.findViewById(R.id.img_activity_my_profile_gender);
        final AQuery aq = new AQuery(act);

        if(this.mUserInfo.getGender().equals( GenderType.NONE )){
            this.imgGender.setVisibility(View.GONE);
            this.imgGenderDot.setVisibility(View.GONE);
        }
        else
        {
            if (this.mUserInfo.getGender() == GenderType.MALE) {
                this.imgGender.setImageResource(R.drawable.profile_sex2);
            } else if (this.mUserInfo.getGender() == GenderType.FEMALE) {
                this.imgGender.setImageResource(R.drawable.profile_sex1);
            }
        }


//        try {
//            String ss = UserInfo.getPhotoUrlSharedPreference(getApplicationContext());
//            //String ss = mUserInfo.getAccountPhoto();
//
//            if(ss != null)
//            {
//                if(!(ss.equals("") || ss.equals("null"))) {
//                    final String finalSs = ss;
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            BitmapAjaxCallback cb = new BitmapAjaxCallback();
//                            cb.url(NetworkClass.imageBaseURL + finalSs).rotate(true);
//                            aq.id( imgProfile.getId() ).image(cb);
//                        }
//                    });
//                }
//            }
//        }
//        catch (Exception ex)
//        {
//
//        }

        String imageName = mUserInfo.getAccountPhoto();
        if( imageName != null )
        {
            String imgUri = null;
            if(imageName.startsWith("file://")){
                imgUri = imageName;
            }
            else{
                imgUri = NetworkClass.imageBaseURL + imageName;
            }

            int cornerRadius = imgProfile.getLayoutParams().width / 2;
            DisplayImageOptions options = new DisplayImageOptions.Builder()
                    .showImageForEmptyUri(R.drawable.profile_img1)
                    .showImageOnFail(R.drawable.profile_img1)
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .displayer(new RoundedBitmapDisplayer( cornerRadius ))
                    .build();

            ImageLoader.getInstance().displayImage( imgUri , imgProfile, options);
        }

        this.tvBirthDate = (TextView) this.findViewById(R.id.tv_activity_my_profile_birthdate);
        this.imgBirthDateDot = (ImageView) this.findViewById(R.id.img_activity_my_profile_birthdate_dot);
        long birthDataTimeMilliSecond = this.mUserInfo.getBirthDate();
        if(birthDataTimeMilliSecond > 0) {
            this.tvBirthDate.setText(this.getDateString(birthDataTimeMilliSecond));
        }else{
            this.tvBirthDate.setVisibility(View.GONE);
            this.imgBirthDateDot.setVisibility(View.GONE);
        }

        this.tvEmail = (TextView) this.findViewById(R.id.tv_activity_my_profile_email);
        this.tvEmail.setText( this.mUserInfo.getEmail() );

        this.tvIntro = (TextView) this.findViewById(R.id.tv_activity_my_profile_intro);
        if( this.mUserInfo.getIntro() != null ) {
            this.tvIntro.setText( this.mUserInfo.getIntro() );
        }

        ExerciseProgram program = this.mPresenter.getAppliedExerciseProgram();
        this.tvProgramName = (TextView) this.findViewById(R.id.tv_activity_my_profile_program_name);
        int resId =  getResources().getIdentifier(program.getName(), "string", getPackageName());
        String programName = getString(resId);
        this.tvProgramName.setText(programName);
        this.appliedProgramLayout = (AppliedProgramSummaryLayout) this.findViewById(R.id.apl_activity_my_profile_appliedprogram_info);
        this.appliedProgramLayout.setAppliedExerciseProgram(program);

        //Si가 있는지 확인한다.
        boolean isSi = mUserInfo.isSI();

        this.flHeight = (FrameLayout) this.findViewById(R.id.fl_activity_my_profile_height);
        this.tvHeight = (TextView) this.findViewById(R.id.tv_activity_my_profile_height);
        this.flHeight.setOnClickListener(this);

        if( mUserInfo.getHeight() > 0 ) {

            String heightString = String.valueOf( mUserInfo.getHeight() );
            String[] splitedHeightString = heightString.split("\\.");
            if( splitedHeightString.length > 1 ){
                tvHeight.setText(String.format("%s %s", splitedHeightString[0] + "." + splitedHeightString[1].substring(0, 1), UserInfo.getHeightUnit(isSi)) );
            }else{
                tvHeight.setText(String.format("%s %s", splitedHeightString[0] + ".0", UserInfo.getHeightUnit(isSi)));
            }

        }else{
            this.tvHeight.setText(this.getString(R.string.common_no_setting));
        }

        this.flWeight = (FrameLayout) this.findViewById( R.id.fl_activity_my_profile_weight );
        this.tvWeight = (TextView) this.findViewById(R.id.tv_activity_my_profile_weight);
        this.flWeight.setOnClickListener(this);
        if(mUserInfo.getWeight() > 0) {

            String weightString = String.valueOf( mUserInfo.getWeight() );
            String[] splitedWeightString = weightString.split("\\.");
            if( splitedWeightString.length > 1 ){
                tvWeight.setText(String.format("%s %s", splitedWeightString[0] + "." + splitedWeightString[1].substring(0, 1), UserInfo.getWeightUnit(isSi)));
            }else{
                tvWeight.setText(String.format("%s %s", splitedWeightString[0] + ".0", UserInfo.getWeightUnit(isSi)));
            }

        }else{
            this.tvWeight.setText(this.getString(R.string.common_no_setting));
        }


        this.flWearingLocation = (FrameLayout) this.findViewById(R.id.fl_activity_my_profile_wearinglocation);
        this.tvWearingLocation = (TextView) this.findViewById(R.id.tv_activity_my_profile_wearinglocation);
        this.flWearingLocation.setOnClickListener(this);
        if( mUserInfo.getDeviceAddress() != null && !mUserInfo.getDeviceAddress().equals("") && !mUserInfo.getDeviceAddress().equals("null") ) { //기기등록을 안했다면 기기 변경을 할 수 없다.
            this.tvWearingLocation.setText(this.getWearingLocationString(mUserInfo.getWearAt()));
        }else{
            this.tvWearingLocation.setText(this.getString(R.string.common_no_setting));
        }

        this.flChangeFitmeter = (FrameLayout) this.findViewById(R.id.fl_activity_my_profile_change_fitmeter);
        this.tvChangeFitmeter = (TextView) this.findViewById(R.id.tv_activity_my_profile_change_fitmeter);
        this.flChangeFitmeter.setOnClickListener(this);
        if( mUserInfo.getDeviceAddress() != null && !mUserInfo.getDeviceAddress().equals("") && !mUserInfo.getDeviceAddress().equals("null") ) { //기기등록을 안했다면 기기 변경을 할 수 없다.
        }else{
            this.tvChangeFitmeter.setText(this.getString(R.string.common_no_setting));
        }

        this.flLogout = (FrameLayout) this.findViewById(R.id.fl_activity_my_profile_logout);
        this.flLogout.setOnClickListener(this);

        this.flAlarm = (FrameLayout) this.findViewById(R.id.fl_activity_my_profile_alarm);

        this.tvAlarm = (TextView) this.findViewById(R.id.tv_myprofile_alarm);
        tvAlarm.setText(UserInfo.getAlarmSetSharedPreference(this)?this.getString(R.string.common_on) :this.getString(R.string.common_off));

        View actionBarView = this.findViewById(R.id.ic_activity_main_actionbar);
        actionBarView.setBackgroundColor( 0xFF3C3C3C );
        TextView tvBarTitle = (TextView) actionBarView.findViewById( R.id.tv_custom_action_bar_title );
        ImageView imgLeft = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_left);
        imgLeft.setVisibility(View.GONE);
        ImageView mImgRight = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_right);
        mImgRight.setImageResource(R.drawable.home_btn_sync);
        mImgRight.setOnClickListener(this);
        mImgRight.setVisibility(View.GONE);
        tvBarTitle.setText( R.string.myprofile_bar_title );

    }

    private Bitmap getCircleBitmap(int resourceId) {
        Bitmap bitmap = this.drawableToBitmap(this.getResources().getDrawable(resourceId));
        Bitmap output = Bitmap.createBitmap( bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);
        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        int size = (bitmap.getWidth()/2);
        canvas.drawCircle(size, size, size, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return output;
    }

    public Bitmap drawableToBitmap(Drawable drawable) {
        if (drawable == null) {
            return null;
        } else if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    @Override
    public void onClick(View view) {
        switch( view.getId() ){
            case R.id.fl_activity_my_profile_height:

                if( mUserInfo.getHeight() <= 0 ){
                    showNoSettingDialog();
                    return;
                }

                if( !Utils.isOnline(this) ){
                    Toast.makeText(this , this.getString(R.string.myprofile_connect_network)  , Toast.LENGTH_SHORT).show();
                    return;
                }

                boolean heightUnit = mUserInfo.isSI();
                AlertDialog.Builder heightDialog = new AlertDialog.Builder(this);
                heightDialog.setTitle(this.getString(R.string.common_height));
                View numberPickerView = this.getLayoutInflater().inflate(R.layout.dialog_number_picker, null);
                final NumberPicker np = (NumberPicker) numberPickerView.findViewById(R.id.np_dialog_number_picker);
                np.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
                final NumberPicker pp = (NumberPicker) numberPickerView.findViewById(R.id.np_dialog_point_picker);
                pp.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
                final NumberPicker up = (NumberPicker) numberPickerView.findViewById(R.id.np_dialog_unit_picker);
                up.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
                up.setDisplayedValues(new String[]{this.getString(R.string.common_centimeter), this.getString(R.string.common_ft)});

                float height = this.mUserInfo.getHeight();
                np.setMaxValue(300);
                np.setMinValue(1);
                pp.setMaxValue(9);
                pp.setMinValue(0);
                up.setMaxValue(1);
                up.setMinValue(0);

                String heightString = String.valueOf( height );
                String[] splitedHeightString = heightString.split("\\.");
                if( splitedHeightString.length > 1 ){
                    np.setValue( Integer.parseInt( splitedHeightString[0] ) );
                    pp.setValue(  Integer.parseInt( splitedHeightString[1].substring( 0 , 1) ) );
                }else{
                    np.setValue( Integer.parseInt( splitedHeightString[0] ) );
                    pp.setValue(0);
                }

                if( heightUnit )
                {
                    up.setValue(0);
                }else{

                    up.setValue(1);
                }
                up.setTag( height );
                heightDialog.setView(numberPickerView);

                up.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                    @Override
                    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                        boolean heightSi = (newVal == 0);
                        float tempHeight = (float) up.getTag();

                        //몸무게를 변환한다.
                        if (heightSi) {
                            //파운드에서 킬로그램으로 변환한다.
                            tempHeight = com.fitdotlife.fitmate_lib.service.util.Utils.ft_to_cm(tempHeight);
                        } else {
                            //킬로그램에서 파운드로 변환한다.
                            tempHeight = com.fitdotlife.fitmate_lib.service.util.Utils.cm_to_ft(tempHeight);
                        }

                        String heightString = String.valueOf( tempHeight );
                        String[] splitedHeightString = heightString.split("\\.");
                        if( splitedHeightString.length > 1 ){
                            np.setValue( Integer.parseInt( splitedHeightString[0] ) );
                            pp.setValue(  Integer.parseInt( splitedHeightString[1].substring( 0 , 1) ) );
                        }else{
                            np.setValue( Integer.parseInt( splitedHeightString[0] ) );
                            pp.setValue(0);
                        }

                        up.setTag(tempHeight);

                    }
                });

                np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                    @Override
                    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                        up.setTag( (float) (((np.getValue() * 10d) + pp.getValue()) / 10d) );
                    }
                });

                pp.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                    @Override
                    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                        up.setTag( (float) (((np.getValue() * 10d) + pp.getValue()) / 10d) );
                    }
                });

                heightDialog.setNegativeButton(this.getString(R.string.common_cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                heightDialog.setPositiveButton(this.getString(R.string.common_ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        boolean heightSi = up.getValue() == 0;
                        boolean isSi = mUserInfo.isSI();
                        float tempWeight = mUserInfo.getWeight();
                        String weightUnitString = "";

                        if (heightSi != isSi) {
                            //몸무게를 변환한다.
                            if (heightSi) {
                                //파운드에서 킬로그램으로 변환한다.
                                tempWeight = com.fitdotlife.fitmate_lib.service.util.Utils.lb_to_kg(tempWeight);
                                weightUnitString = getString(R.string.common_kilogram);
                            } else {
                                //킬로그램에서 파운드로 변환한다.
                                tempWeight = com.fitdotlife.fitmate_lib.service.util.Utils.kg_to_lb(tempWeight);
                                weightUnitString = getString(R.string.common_pound);
                            }

                            String weightString = String.valueOf( tempWeight );
                            String[] splitedWeightString = weightString.split("\\.");
                            if( splitedWeightString.length > 1 ){
                                tvWeight.setText(String.format("%s %s", splitedWeightString[0] + "." + splitedWeightString[1].substring(0, 1), weightUnitString ));
                            }else{
                                tvWeight.setText(String.format("%s %s", splitedWeightString[0] + ".0", weightUnitString ));
                            }

                        }
                        mUserInfo.setHeight((float) up.getTag());
                        mUserInfo.setWeight(tempWeight);
                        mUserInfo.setIsSI(heightSi);
                        mPresenter.modifyUserInfo(mUserInfo);

                        String heightString = String.valueOf( mUserInfo.getHeight() );
                        String[] splitedHeightString = heightString.split("\\.");
                        if( splitedHeightString.length > 1 ){
                            tvHeight.setText(String.format("%s %s", splitedHeightString[0] + "." + splitedHeightString[1].substring(0, 1), up.getDisplayedValues()[up.getValue()]));
                        }else{
                            tvHeight.setText(String.format("%s %s", splitedHeightString[0] + ".0", up.getDisplayedValues()[up.getValue()]));
                        }

                        mPresenter.ChangeUserInfo();

                    }
                });
                heightDialog.show();
                break;
            case R.id.fl_activity_my_profile_weight:

                if( mUserInfo.getWeight() <= 0 ){
                    showNoSettingDialog();
                    return;
                }

                if( !Utils.isOnline(this) ){
                    Toast.makeText(this , this.getString(R.string.myprofile_connect_network)  , Toast.LENGTH_SHORT).show();
                    return;
                }

                boolean weightUnit = mUserInfo.isSI();
                final AlertDialog.Builder weightDialog = new AlertDialog.Builder(this);
                weightDialog.setTitle(this.getString(R.string.common_weight));
                View numberPickerViews = this.getLayoutInflater().inflate(R.layout.dialog_number_picker, null);
                final NumberPicker nps = (NumberPicker) numberPickerViews.findViewById(R.id.np_dialog_number_picker);
                nps.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
                final NumberPicker pps = (NumberPicker) numberPickerViews.findViewById(R.id.np_dialog_point_picker);
                pps.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
                final NumberPicker ups = (NumberPicker) numberPickerViews.findViewById(R.id.np_dialog_unit_picker);
                ups.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
                ups.setDisplayedValues(new String[]{this.getString(R.string.common_kilogram), this.getString(R.string.common_pound)});

                final float weight = this.mUserInfo.getWeight();

                nps.setMaxValue(500);
                nps.setMinValue(1);
                pps.setMaxValue(9);
                pps.setMinValue(0);
                ups.setMaxValue(1);
                ups.setMinValue(0);

                String weightString = String.valueOf( weight );
                String[] splitedWeightString = weightString.split("\\.");
                if( splitedWeightString.length > 1 ){
                    nps.setValue( Integer.parseInt( splitedWeightString[0] ) );
                    pps.setValue(  Integer.parseInt( splitedWeightString[1].substring( 0 , 1) ) );
                }else{
                    nps.setValue( Integer.parseInt( splitedWeightString[0] ) );
                    pps.setValue(0);
                }

                if( weightUnit )
                {
                    ups.setValue(0);
                }else{

                    ups.setValue(1);
                }
                ups.setTag( weight );
                weightDialog.setView(numberPickerViews);
                ups.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                    @Override
                    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                        boolean weightSi = (newVal == 0);
                        float tempWeight = (float) ups.getTag();

                        //몸무게를 변환한다.
                        if (weightSi) {
                            //파운드에서 킬로그램으로 변환한다.
                            tempWeight = com.fitdotlife.fitmate_lib.service.util.Utils.lb_to_kg(tempWeight);
                        } else {
                            //킬로그램에서 파운드로 변환한다.
                            tempWeight = com.fitdotlife.fitmate_lib.service.util.Utils.kg_to_lb(tempWeight);
                        }

                        String weightString = String.valueOf( tempWeight );
                        String[] splitedWeightString = weightString.split("\\.");
                        if( splitedWeightString.length > 1 ){
                            nps.setValue( Integer.parseInt( splitedWeightString[0] ) );
                            pps.setValue(  Integer.parseInt( splitedWeightString[1].substring( 0 , 1) ) );
                        }else{
                            nps.setValue( Integer.parseInt( splitedWeightString[0] ) );
                            pps.setValue(0);
                        }

                        ups.setTag(tempWeight);
                    }
                });

                nps.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                    @Override
                    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                        ups.setTag( (float) (((nps.getValue() * 10d) + pps.getValue()) / 10d) );
                    }
                });

                pps.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                    @Override
                    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                        ups.setTag( (float) (((nps.getValue() * 10d) + pps.getValue()) / 10d) );
                    }
                });

                weightDialog.setNegativeButton(this.getString(R.string.common_cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                weightDialog.setPositiveButton(this.getString(R.string.common_ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        boolean weightSi = (ups.getValue() == 0);
                        boolean isSi = mUserInfo.isSI();
                        float tempHeight = mUserInfo.getHeight();
                        String heightUnitString = "";

                        if (weightSi != isSi) {
                            if (weightSi) {
                                //인치에서 센티미터로 변환
                                tempHeight = com.fitdotlife.fitmate_lib.service.util.Utils.ft_to_cm(tempHeight);
                                heightUnitString = getString(R.string.common_centimeter);
                            } else {
                                //센티미터에서 인치로 변환한다.
                                tempHeight = com.fitdotlife.fitmate_lib.service.util.Utils.cm_to_ft(tempHeight);
                                heightUnitString = getString(R.string.common_ft);
                            }

                            String heightString = String.valueOf( tempHeight );
                            String[] splitedHeightString = heightString.split("\\.");
                            if( splitedHeightString.length > 1 ){
                                tvHeight.setText(String.format("%s %s", splitedHeightString[0] + "." + splitedHeightString[1].substring(0, 1), heightUnitString ));
                            }else{
                                tvHeight.setText(String.format("%s %s", splitedHeightString[0] + ".0", heightUnitString ));
                            }

                        }

                        mUserInfo.setWeight((Float) ups.getTag());
                        mUserInfo.setHeight(tempHeight);
                        mUserInfo.setIsSI(weightSi);
                        mPresenter.modifyUserInfo(mUserInfo);

                        String weightString = String.valueOf( mUserInfo.getWeight() );
                        String[] splitedWeightString = weightString.split("\\.");
                        if( splitedWeightString.length > 1 ){
                            tvWeight.setText(String.format("%s %s", splitedWeightString[0] + "." + splitedWeightString[1].substring(0, 1), ups.getDisplayedValues()[ups.getValue()]));
                        }else{
                            tvWeight.setText(String.format("%s %s", splitedWeightString[0] + ".0", ups.getDisplayedValues()[ups.getValue()]));
                        }

                        mPresenter.ChangeUserInfo();

                    }
                });

                weightDialog.show();

                break;
            case R.id.fl_activity_my_profile_wearinglocation:

                if( mUserInfo.getDeviceAddress() != null && !mUserInfo.getDeviceAddress().equals("") && !mUserInfo.getDeviceAddress().equals("null") ) { //기기등록을 안했다면 기기 변경을 할 수 없다.

                    if (!Utils.isOnline(this)) {
                        Toast.makeText(this, this.getString(R.string.myprofile_connect_network), Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (!isShowing) {
                        isShowing = true;
                        mPresenter.requestStopService(MyProfilePresenter.STOP_FOR_CHANGE_WEARINGLOCATION);
                    }
                }else{
                    showNoSettingDialog();
                }

                break;
            case R.id.fl_activity_my_profile_change_fitmeter:
                // todo 서비스 중지
                if( mUserInfo.getDeviceAddress() != null && !mUserInfo.getDeviceAddress().equals("") && !mUserInfo.getDeviceAddress().equals("null") ) { //기기등록을 안했다면 기기 변경을 할 수 없다.

                    if (!Utils.isOnline(this)) {
                        Toast.makeText(this, this.getString(R.string.myprofile_connect_network), Toast.LENGTH_LONG).show();
                        return;
                    }

                    if (!isShowing) {
                        isShowing = true;
                        mPresenter.requestStopService(MyProfilePresenter.STOP_FOR_CHANGE_FITMETER);
                    }
                }else{
                    showNoSettingDialog();
                }
                break;
            case R.id.fl_activity_my_profile_logout:
                //임시로 비밀번호 변경으로
                //FragmentManager fm = this.getFragmentManager();
                if( !Utils.isOnline(this) ){
                    Toast.makeText(this , this.getString(R.string.myprofile_connect_network)  , Toast.LENGTH_SHORT).show();
                    return;
                }

                ChangePasswordDialog fpDialog = new ChangePasswordDialog(this, this.mUserInfo.getEmail());
                fpDialog.show();

                break;
            case R.id.ll_activity_my_profile_user_info:
                if( !Utils.isOnline(this) ){
                    Toast.makeText(this , this.getString(R.string.myprofile_connect_network)  , Toast.LENGTH_SHORT).show();
                    return;
                }

                Intent userinfoChangeIntent = new Intent(MyProfileActivity.this , UserInfoChangeActivity.class);
                userinfoChangeIntent.putExtra(UserInfo.EMAIL_KEY , this.mUserInfo.getEmail());
                userinfoChangeIntent.putExtra(UserInfo.NAME_KEY ,this.mUserInfo.getName());
                userinfoChangeIntent.putExtra(UserInfo.HELLO_MESSAGE_KEY , this.mUserInfo.getIntro());
                userinfoChangeIntent.putExtra(UserInfo.BIRTHDATEKEY , this.mUserInfo.getBirthDate());
                userinfoChangeIntent.putExtra(UserInfo.SEX_KEY , this.mUserInfo.getGender().getValue());
                userinfoChangeIntent.putExtra(UserInfo.PROFILE_MAGE_KEY , this.mUserInfo.getAccountPhoto());

                this.startActivityForResult(userinfoChangeIntent, USERINFO_REQUEST_CODE);
                //this.startActivity(userinfoChangeIntent);
                break;
            case R.id.fl_activity_my_profile_alarm:
                Intent alarmChangeIntent = new Intent(MyProfileActivity.this , MyAlarmActivity.class);
                //this.startActivityForResult(alarmChangeIntent, USERINFO_REQUEST_CODE);
                this.startActivityForResult(alarmChangeIntent, USERINFO_CHANGEALARM_CODE);
                break;


                }
        }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if( requestCode == this.USERINFO_REQUEST_CODE ) {
            if (resultCode == RESULT_OK) {

                this.mUserInfo.setName(data.getStringExtra(UserInfo.NAME_KEY));
                this.tvName.setText(this.mUserInfo.getName());

                this.mUserInfo.setIntro(data.getStringExtra(UserInfo.HELLO_MESSAGE_KEY));
                this.tvIntro.setText(this.mUserInfo.getIntro());

                long tempBirthDay = data.getLongExtra(UserInfo.BIRTHDATEKEY, 0);
                if(tempBirthDay > 0 ) {
                    this.mUserInfo.setBirthDate(data.getLongExtra(UserInfo.BIRTHDATEKEY, 0));
                    this.tvBirthDate.setText(this.getDateString(this.mUserInfo.getBirthDate()));
                }

                GenderType tempGenderType = GenderType.getGenderType(data.getIntExtra(UserInfo.SEX_KEY, 0));
                if( !tempGenderType.equals(GenderType.NONE) ) {
                    this.mUserInfo.setGender(tempGenderType);
                    if (this.mUserInfo.getGender() == GenderType.MALE) {
                        this.imgGender.setImageResource(R.drawable.profile_sex2);
                        //this.imgProfile.setImageBitmap( this.getCircleBitmap( R.drawable.profile_img5 ) );

                    } else if (this.mUserInfo.getGender() == GenderType.FEMALE) {
                        this.imgGender.setImageResource(R.drawable.profile_sex1);
                        //this.imgProfile.setImageBitmap( this.getCircleBitmap( R.drawable.profile_img2 ) );

                    }
                }

                mPresenter.modifyUserInfo(mUserInfo);
                mPresenter.ChangeUserInfo();
            }
        }
        else if(requestCode == this.USERINFO_CHANGEDEVICE_CODE)
        {
            if(resultCode == RESULT_OK)
            {

            }

            this.isShowing = false;
            mUserInfo = this.mPresenter.getUserInfo();
            this.mPresenter.ChangeUserInfo();
            this.mPresenter.requestRestartService();
        }
        else if(requestCode == this.USERINFO_CHANGELOCATION_CODE)
        {
            if(resultCode == RESULT_OK)
            {
                int location = data.getIntExtra(LocationChangeActivity.INTENT_LOCATION, 0);
                this.tvWearingLocation.setText( this.getWearingLocationString( location ) );
                mUserInfo.setWearAt(location);
                mPresenter.modifyUserInfo(mUserInfo);
                mPresenter.ChangeUserInfo();
            }

            this.isShowing = false;
            this.mPresenter.requestRestartService();
        }
        else if(requestCode == this.USERINFO_CHANGEALARM_CODE)
        {
            boolean isAlarm = UserInfo.getAlarmSetSharedPreference(this);
            tvAlarm.setText(isAlarm ? this.getString(R.string.common_on) : this.getString(R.string.common_off));
        }
        else if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {

            Uri uri = (Uri) data.getExtras().get(ImageUtil.PICTURE_URI);
            DisplayImageOptions options = new DisplayImageOptions.Builder()
                    .showImageForEmptyUri(R.drawable.profile_img1)
                    .showImageOnFail(R.drawable.profile_img1)
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .displayer(new RoundedBitmapDisplayer(300))
                    .build();

            ImageLoader.getInstance().displayImage(uri.toString(), imgProfile, options);

            mUserInfo.setAccountPhoto( uri.toString() );
            mPresenter.modifyUserInfo(mUserInfo);

            aync_multipart(uri);

//            try {
//
//                String ss = UserInfoModel.getUserInfo( mUserInfo.getEmail() , mUserInfo.getPassword());
//
//                if(!ss.equals(""))
//                {
//                    if(UserInfo.getPhotoUrlSharedPreference(getApplicationContext()) == null || (UserInfo.getPhotoUrlSharedPreference(getApplicationContext()) != null &&  !UserInfo.getPhotoUrlSharedPreference(getApplicationContext()).equals(ss)))
//                    {
//                        UserInfo.savePhotoUrlInSharedPreference(getApplicationContext(), ss);
//                    }
//
//                    mUserInfo.setAccountPhoto(ss);
//                }
//            } catch (IOException e) {
//                e.printStackTrace();
//            }

        }
    }

    static OkHttpClient client = new OkHttpClient();
    String post(String url, RequestBody body, final int mResponseCode) throws IOException {
        client.setConnectTimeout(10, TimeUnit.SECONDS);
        client.setWriteTimeout(10, TimeUnit.SECONDS);
        client.setReadTimeout(10, TimeUnit.SECONDS);
        boolean result = false;

        final Request request = new Request.Builder()
                .header("Authorization", "Client-ID " + ":DDD")
                .url(url)
                .post(body)
                .build();

        Handler mHandler = new Handler(Looper.getMainLooper());
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // 내용
                client.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(Request request, IOException e) {
                        //ShowMessage("변경 실패 : " + e.getMessage());

                        Log.e(TAG, e.getMessage());
                        Toast.makeText(getApplicationContext(), getString(R.string.myprofile_dont_send_photo), Toast.LENGTH_SHORT).show();

                        Handler mHandler = new Handler(Looper.getMainLooper());
                        mHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                //onHttpResponse(null, mResponseCode, HttpRequest.FAIL);
                            }},0);
                    }

                    @Override
                    public void onResponse(final Response response) throws IOException {
                        Handler mHandler = new Handler(Looper.getMainLooper());
                        mHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                if(response.code() == 200) {

                                    if(response.code() == 200) {

                                        String data = null;
                                        try {
                                            data = response.body().string();

                                            JSONObject jsonObject = new JSONObject(data);
                                            String result = jsonObject.getString(CommonKey.RESPONSE_VALUE_KEY);

                                            if (result.equals(CommonKey.SUCCESS)) {
                                                String url = jsonObject.getString("url");
                                                if(url != null && url != "")
                                                {
                                                    UserInfo.savePhotoUrlInSharedPreference(getApplicationContext(), url);
                                                    mUserInfo.setAccountPhoto( url );
                                                }
                                            }
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        //onHttpResponse(data, mResponseCode, HttpRequest.SUCCESS);
                                    }
                                    else
                                    {
                                        //onHttpResponse(null, mResponseCode, HttpRequest.FAIL);
                                    }

                                }
                            }
                        },5000);
                    }
                });




            }}, 0);
        return "";
    }

    private void aync_multipart(Uri uri){

        String url = NetworkClass.baseURL + "/ChangeUserInfo.aspx?";
        Map<String, Object> params = new HashMap<String, Object>();
        url += String.format("%s=%s&%s=%s", "email", mUserInfo.getEmail(), "pw", mUserInfo.getPassword());

        try {

            RequestBody body = null;
            if(false)
            {
                body = RequestBody.create(MediaType.parse("image/jpg"), new File("/storage/emulated/0/DCIM/Camera/20150529_172213.jpg"));
            }
            else {
                String sss = uri.toString();
                if(sss.startsWith("file://"))
                {
                    sss = (String) sss.subSequence(7, sss.length());
                }
                File f = new File(sss);
                if(!f.exists())
                    return;
                body = new MultipartBuilder()
                        .type(MultipartBuilder.FORM)
                        .addFormDataPart("Files", f.getName(),
                                RequestBody.create(MediaType.parse("image/jpg"), f))
                        .build();
            }

            post(url, body, 0);



        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }



    }

    public void callback(String url, String html, AjaxStatus status) {
        Log.d(TAG, " EEEE");
        //showResult(html);
    }

    private void setAutoLogin( boolean autoLogin ){
        SharedPreferences pref = this.getSharedPreferences( "fitmateservice" , Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(UserInfo.AUTO_LOGIN_KEY, autoLogin);
        editor.commit();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.mPresenter.close();
        this.mPresenter.modifyUserInfo(this.mUserInfo);

        this.setResult(DrawerView.REQUEST_PROFILE_IMAGE);

        Intent programChangeIntent = new Intent( SettingActivity.ACTION_USERINFO_CHANGE );
        sendBroadcast(programChangeIntent);

    }

    private String getWearingLocationString( int wearingLocation ){
        String result = null;

        if( wearingLocation == 0 ){
            wearingLocation = 1;
        }

        result = this.getResources().getStringArray(R.array.weraing_location_string)[wearingLocation - 1];
        return result;
    }

    @Override
    public void showDeviceChangeActivity(){
        Intent deviceChangeIntent = new Intent( MyProfileActivity.this , DeviceChangeActivity.class  );
        deviceChangeIntent.putExtra(DeviceChangeActivity.INTENT_LOCATION, mUserInfo.getWearAt());
        this.startActivityForResult(deviceChangeIntent, USERINFO_CHANGEDEVICE_CODE);
    }

    @Override
    public void showWearingLocationActivity() {
        Intent locationChangeIntent = new Intent( MyProfileActivity.this , LocationChangeActivity.class  );
        locationChangeIntent.putExtra(LocationChangeActivity.INTENT_LOCATION, mUserInfo.getWearAt());
        locationChangeIntent.putExtra(LocationChangeActivity.INTENT_BTADDRESS, mUserInfo.getDeviceAddress());
        this.startActivityForResult(locationChangeIntent, USERINFO_CHANGELOCATION_CODE);
    }

    public void setShowing( boolean showing ){
        this.isShowing = showing;
    }

    @Override
    public void dismissProgress() {

    }

    private String getDateString( long birthUtcTime ){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd");
        Date utcDate = new Date();
        utcDate.setTime(birthUtcTime);
        return dateFormat.format(utcDate);
    }

    private void showNoSettingDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);     // 여기서 this는 Activity의 this

        String message = this.getString(R.string.userinfo_not_connect_text);
        builder.setMessage(message)        // 메세지 설정
                .setCancelable(true)        // 뒤로 버튼 클릭시 취소 가능 설정
                .setNegativeButton(R.string.common_close, new DialogInterface.OnClickListener() {
                    // 확인 버튼 클릭시 설정
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                })
                .setPositiveButton(R.string.userinfo_not_start_fitmeter, new DialogInterface.OnClickListener() {
                    // 취소 버튼 클릭시 설정
                    public void onClick(DialogInterface dialog, int whichButton) {
                        Intent intent = new Intent(getApplicationContext(), SetUserInfoActivity.class);
                        startActivity(intent);

                        dialog.dismiss();
                    }
                });

        AlertDialog dialog = builder.create();    // 알림창 객체 생성
        dialog.show();    // 알림창 띄우기
    }




}
