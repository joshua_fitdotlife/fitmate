package com.fitdotlife.fitmate;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.fitdotlife.fitmate_lib.customview.AppliedProgramLayout;
import com.fitdotlife.fitmate_lib.customview.AppliedProgramSummaryLayout;
import com.fitdotlife.fitmate_lib.customview.FriendProfileCommonView;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.http.FriendService;
import com.fitdotlife.fitmate_lib.http.NetworkClass;
import com.fitdotlife.fitmate_lib.http.RestHttpErrorHandler;
import com.fitdotlife.fitmate_lib.key.FriendRequestType;
import com.fitdotlife.fitmate_lib.key.FriendStatusType;
import com.fitdotlife.fitmate_lib.object.ExerciseProgram;
import com.fitdotlife.fitmate_lib.object.UserFriend;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.rest.spring.annotations.RestService;
import org.springframework.web.client.RestClientException;

import java.util.Calendar;

@EActivity(R.layout.activity_friend_profile)
public class FriendProfileActivity extends Activity implements View.OnTouchListener {

    private FitmateDBManager mDBManager;
    private AppliedProgramSummaryLayout aplProgramInfo = null;
    private UserFriend mUserFriend = null;

    @ViewById(R.id.img_activity_friend_profile_gender)
    ImageView imgGender;

    @ViewById(R.id.tv_activity_friend_profile_birthdate)
    TextView tvBirthDate;

    @ViewById(R.id.tv_activity_friend_profile_email)
    TextView tvEmail;

    @ViewById(R.id.tv_activity_friend_profile_intro)
    TextView tvIntro;

    @ViewById(R.id.tv_activity_friend_profile_program_name)
    TextView tvProgramName;

    @ViewById(R.id.fpv_activity_friend_profile)
    protected FriendProfileCommonView commonView;

    @ViewById(R.id.ic_activity_friend_actionbar)
    View actionBarView;

    @RestService
    FriendService friendServiceClient;

    @Bean
    RestHttpErrorHandler restErrorHandler;

    @AfterViews
    void init()
    {
        friendServiceClient.setRootUrl(NetworkClass.baseURL + "/api");
        friendServiceClient.setRestErrorHandler( restErrorHandler );

        this.mDBManager = new FitmateDBManager(this);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        mUserFriend = extras.getParcelable("UserFriend");

        FriendActivityManager.getInstance().addActivity(this);

        String imageName = mUserFriend.getProfileImagePath();
        if( !( imageName == null ) ) {
            commonView.setProfileImage(imageName);
        }

        String name =  mUserFriend.getName();
        commonView.setName(name);
        this.displayActionBar( name );

        tvIntro.setText( mUserFriend.getIntroYourSelf() );
        this.tvEmail.setText( mUserFriend.getEmail() );

        boolean sex = false;
         sex = mUserFriend.isSex();

        if( sex ){
            imgGender.setImageResource( R.drawable.profile_sex2 );
        }else{
            imgGender.setImageResource( R.drawable.profile_sex1 );
        }

        String strBirthdate = mUserFriend.getBirthDate();
        strBirthdate = strBirthdate.substring(0,10);
        this.tvBirthDate.setText(strBirthdate);

        ExerciseProgram exerciseProgram = this.mDBManager.getUserExerciProgram( mUserFriend.getExerciseProgramID() );

        String programName = null;
        if(exerciseProgram.getDefaultProgram())
        {
            int resId =  getResources().getIdentifier(exerciseProgram.getName(), "string", getPackageName());
            programName = getString(resId);
        }
        else
        {
            programName = exerciseProgram.getName();
        }

        tvProgramName.setText( programName );
        this.aplProgramInfo = (AppliedProgramSummaryLayout) this.findViewById(R.id.apl_activity_friend_profile_appliedprogram_info);
        this.aplProgramInfo.setAppliedExerciseProgram(exerciseProgram);
    }

    private void displayActionBar( String name ){
        actionBarView.setBackgroundColor(this.getResources().getColor(R.color.fitmate_blue));
        TextView tvBarTitle = (TextView) actionBarView.findViewById( R.id.tv_custom_action_bar_title );
        tvBarTitle.setText(name + " " + this.getString(R.string.friend_friendprofile_title));
        ImageView imgBack = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_left);
        imgBack.setVisibility(View.GONE);

        ImageView mImgRight = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_right);
        mImgRight.setImageResource(R.drawable.friend_del);
        mImgRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(FriendProfileActivity.this);     // 여기서 this는 Activity의 this

                builder.setTitle(getString(R.string.friend_friendprofile_delete_friend_title))        // 제목 설정
                        .setMessage(getString(R.string.friend_friendprofile_delete_friend_content))        // 메세지 설정
                        .setCancelable(true)        // 뒤로 버튼 클릭시 취소 가능 설정
                        .setPositiveButton(R.string.common_yes, new DialogInterface.OnClickListener() {
                            // 확인 버튼 클릭시 설정
                            public void onClick(DialogInterface dialog, int whichButton) {
                                deleteFriend();
                                dialog.cancel();
                            }
                        })
                        .setNegativeButton(R.string.common_no, new DialogInterface.OnClickListener() {
                            // 취소 버튼 클릭시 설정
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.cancel();
                            }
                        });

                AlertDialog dialog = builder.create();    // 알림창 객체 생성
                dialog.show();    // 알림창 띄우기
            }
        });
        mImgRight.setOnTouchListener(this);
    }

    @Background
    void deleteFriend(){
        boolean result = false;

        try {

            result = friendServiceClient.changeStatus(mDBManager.getUserInfo().getEmail(), mUserFriend.getUserFriendsID(), FriendStatusType.DELETE.getValue() );

        }catch( RestClientException e){
            Log.e("fitmate", e.getMessage());
        }
        if(result) {
            finishActivity(mUserFriend.getEmail());
        }
    }

    @UiThread
    void finishActivity( String friendEmail ){

        this.setDeletedFriendEmail(friendEmail);
        FriendActivityManager.getInstance().finishAllActivity();

    }

    private void setDeletedFriendEmail( String friendEmail ){
        SharedPreferences pref = this.getSharedPreferences( "fitmateservice" , Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(FriendActivity.FRIEND_DELETED_FRIEND_EMAIL_KEY, friendEmail);
        editor.commit();
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        ImageView actionbarImageView = (ImageView) view;
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            actionbarImageView.setColorFilter(this.getResources().getColor(R.color.selected_colorfilter) , PorterDuff.Mode.DARKEN );
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            actionbarImageView.setColorFilter(null);
        }

        return false;
    }
}
