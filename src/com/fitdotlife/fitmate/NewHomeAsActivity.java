package com.fitdotlife.fitmate;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fitdotlife.Preprocess;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.UserInfoForAnalyzer;
import com.fitdotlife.fitmate.newhome.CategoryType;
import com.fitdotlife.fitmate.newhome.DayPagerAdapter;
import com.fitdotlife.fitmate.newhome.NewHomeCategoryData;
import com.fitdotlife.fitmate.newhome.NewHomeConstant;
import com.fitdotlife.fitmate.newhome.NewHomeProgramClickListener;
import com.fitdotlife.fitmate.newhome.NewHomeUtils;
import com.fitdotlife.fitmate.newhome.WeekPagerActivityInfo;
import com.fitdotlife.fitmate.newhome.WeekPagerAdapter;
import com.fitdotlife.fitmate_lib.customview.DrawerView;
import com.fitdotlife.fitmate_lib.customview.NewHomeCategoryView;
import com.fitdotlife.fitmate_lib.customview.NewHomeCategoryView_;
import com.fitdotlife.fitmate_lib.customview.NewHomeDayCircleView_;
import com.fitdotlife.fitmate_lib.customview.NewHomeDayHalfCircleView_;
import com.fitdotlife.fitmate_lib.customview.NewHomeDayTextView_;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.object.DayActivity;
import com.fitdotlife.fitmate_lib.object.ExerciseProgram;
import com.fitdotlife.fitmate_lib.object.FriendWeekActivity;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.util.DateUtils;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringArrayRes;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Joshua on 2015-12-22.
 */
@EActivity(R.layout.activity_newhome_as)
public class NewHomeAsActivity extends Activity
{
    private Calendar mTodayCalendar = null;
    private int mTodayWeekNumber = 0;
    private Calendar mDayCalendar = null;
    private Calendar mWeekCalendar = null;

    private FitmateDBManager mDBManager = null;
    private UserInfo mUserInfo = null;

    private int selectdCategoryIndex = 3;

    private int[] mCategoryDefaultValues = new int[]{ 2, 100 , 500 , 0 , 0, 10 , 60 , 10 };

    private NewHomeUtils newHomeUtils = null;
    private NewHomeAsWeekPagerAdaper mWeekPagerAdapter = null;
    private WeekPagerActivityInfo[] mWeekPagerActivityInfoList = new WeekPagerActivityInfo[1];

    @ViewById(R.id.tv_activity_newhome_as_weekday)
    TextView tvWeekDay;

    @ViewById(R.id.tv_activity_newhome_as_date)
    TextView tvDate;

    @ViewById(R.id.ll_activity_newhome_as_category)
    LinearLayout llCategory;

    @ViewById(R.id.hsv_activity_newhome_as_category)
    HorizontalScrollView hsvCategory;

    @ViewById(R.id.vp_activity_newhome_as_day)
    ViewPager vpDay;

    @ViewById(R.id.vp_activity_newhome_as_week)
    ViewPager vpWeek;

    @StringArrayRes(R.array.newhome_category_char)
    String[] arrCategoryChar;

    @ViewById(R.id.vp_activity_newhome_as_news)
    ViewPager vpNews;

    @ViewById(R.id.dl_activity_newhome_as)
    DrawerLayout dlNewHome;

    @ViewById(R.id.dv_activity_newhome_as_drawer)
    DrawerView drawerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTheme(R.style.OrangeTheme);

        if(!Preprocess.IsTest) {
            Tracker t = ((MyApplication) this.getApplication()).getTracker(MyApplication.TrackerName.APP_TRACKER);
            t.setScreenName("HomeAs");
            t.send(new HitBuilders.AppViewBuilder().build());
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(!Preprocess.IsTest) {
            GoogleAnalytics.getInstance(this).reportActivityStart(this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(!Preprocess.IsTest) {
            GoogleAnalytics.getInstance(this).reportActivityStop(this);
        }
    }

    @Override
    public void onBackPressed() {

        if( dlNewHome.isDrawerOpen( drawerView ) )
        {
            dlNewHome.closeDrawers();
            return;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);     // 여기서 this는 Activity의 this

        builder.setTitle(getString(R.string.common_quit_title))        // 제목 설정
                .setMessage(getString(R.string.common_quit_content))        // 메세지 설정
                .setCancelable(true)        // 뒤로 버튼 클릭시 취소 가능 설정
                .setPositiveButton(R.string.common_yes, new DialogInterface.OnClickListener() {
                    // 확인 버튼 클릭시 설정
                    public void onClick(DialogInterface dialog, int whichButton) {
                        moveTaskToBack(true);
                        finish();
                        dialog.cancel();
                    }
                })
                .setNegativeButton(R.string.common_no, new DialogInterface.OnClickListener() {
                    // 취소 버튼 클릭시 설정
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.cancel();
                    }
                });

        AlertDialog dialog = builder.create();    // 알림창 객체 생성
        dialog.show();    // 알림창 띄우기

    }

    @AfterViews
    void init()
    {
        newHomeUtils = new NewHomeUtils( this , mDBManager , arrCategoryChar , new ProgramClickListener() );

        ActivityReference.activityReferece = this;

        this.mDBManager = new FitmateDBManager(this);
        this.mUserInfo = this.mDBManager.getUserInfo();

        //날짜 관련 뷰
        Calendar calendar = Calendar.getInstance();
        calendar.setTime( new Date() );

        mTodayCalendar = Calendar.getInstance();
        mTodayCalendar.setTimeInMillis(calendar.getTimeInMillis());

        mDayCalendar = Calendar.getInstance();
        mDayCalendar.setTimeInMillis( calendar.getTimeInMillis() );

        this.mWeekCalendar = Calendar.getInstance();
        this.mWeekCalendar.setTimeInMillis(calendar.getTimeInMillis());
        int weekNumber = mWeekCalendar.get(Calendar.DAY_OF_WEEK);
        mWeekCalendar.add(Calendar.DATE, -(weekNumber - 1));

        setDateText();

        //사이드 메뉴
        this.drawerView.setParentActivity(this);
        this.drawerView.setTag(this.getResources().getString(R.string.gnb_home));
        this.drawerView.setDrawerLayout(dlNewHome);

        addDayData();
        addWeekData();
        addNews();

    }

    private void addNews(){
        View[] newsViews = new View[]{newHomeUtils.getDefaultNewsView()};

        NewsPagerAdapter newsAdapter = new NewsPagerAdapter(this , newsViews);
        vpNews.setAdapter( newsAdapter );
    }

    private void addWeekData(){

        mWeekPagerAdapter = new NewHomeAsWeekPagerAdaper(this , NewHomeConstant.ViewOrder[selectdCategoryIndex] , mDayCalendar.getTimeInMillis() , WeekPagerAdapter.WeekPagerType.WEEK_BAR , mWeekPagerActivityInfoList );
        vpWeek.setAdapter( mWeekPagerAdapter );
    }

    private void addDayData()
    {
        View[] dayGraphs = new View[NewHomeConstant.ViewOrder.length ];

        for( int viewIndex = 0 ; viewIndex < NewHomeConstant.ViewOrder.length ;viewIndex++ ) {
            CategoryType categoryType = NewHomeConstant.ViewOrder[viewIndex];

            NewHomeCategoryView categoryView = NewHomeCategoryView_.build(this);
            View dayView = null;

            if (categoryType.equals(CategoryType.DAILY_ACHIEVE)) {
                dayView = NewHomeDayHalfCircleView_.build(this);
            } else if (categoryType.equals(CategoryType.WEEKLY_ACHIEVE)) {
                dayView = NewHomeDayCircleView_.build(this);
            } else {
                dayView = NewHomeDayTextView_.build(this);
            }

            categoryView.setOnClickListener(new CategoryClickListener());
            UserInfoForAnalyzer analyzer = com.fitdotlife.fitmate_lib.service.util.Utils.getUserInfoForAnalyzer(mUserInfo);
            newHomeUtils.setDayValue(categoryView, dayView, NewHomeCategoryData.getNewHomeCategoryData(this), 0 , viewIndex, selectdCategoryIndex , false);

            dayGraphs[viewIndex] = dayView;
            llCategory.addView(categoryView);
        }

        final DayPagerAdapter dayPagerAdapter = new DayPagerAdapter(this , dayGraphs);
        vpDay.setAdapter(dayPagerAdapter);
        vpDay.setCurrentItem(selectdCategoryIndex);
        vpDay.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {

                newHomeUtils.unSelectCategoryView((NewHomeCategoryView) llCategory.getChildAt(selectdCategoryIndex));
                int preSelectedCategoryIndex = selectdCategoryIndex;
                selectdCategoryIndex = position;
                NewHomeCategoryView newHomeCategoryView = (NewHomeCategoryView) llCategory.getChildAt(selectdCategoryIndex);
                newHomeUtils.selectCategoryView(newHomeCategoryView);

                if (preSelectedCategoryIndex > selectdCategoryIndex) { //카테고리를 왼쪽에서 오른쪽으로 이동하는 경우

                    int viewLeft = newHomeCategoryView.getLeft();
                    if (hsvCategory.getScrollX() > viewLeft) {
                        hsvCategory.smoothScrollTo(viewLeft, 0);
                    }

                } else if (preSelectedCategoryIndex < selectdCategoryIndex) { //카테고리를 오른족에서 왼쪽으로 이동하는 경우

                    int viewRight = newHomeCategoryView.getRight();
                    if (viewRight > (hsvCategory.getScrollX() + hsvCategory.getRight())) {

                        hsvCategory.smoothScrollTo(viewRight - hsvCategory.getRight(), 0);
                    }
                }

                CategoryType categoryType = NewHomeConstant.ViewOrder[ selectdCategoryIndex ];
                // 주 그래프를 선택한다.
                mWeekPagerAdapter.changeCategoryType(categoryType);
                mWeekPagerAdapter.notifyDataSetChanged();

            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

    }

    private class ProgramClickListener implements NewHomeProgramClickListener {

        @Override
        public void programClick(int exerprogramID) {

            ExerciseProgram program = mDBManager.getAppliedProgram();
            newHomeUtils.showProgramDialog( program.getId() );
        }
    }

    @Click(R.id.img_activity_newhome_as_drawer)
    void drawerClick(){

        dlNewHome.openDrawer(drawerView);
    }

    private void setDateText(){

        Date activityDate = mTodayCalendar.getTime();
        Date todayDate = new Date();

        String weekDay = null;
        weekDay = this.getResources().getStringArray(R.array.week_string)[ mTodayCalendar.get( Calendar.DAY_OF_WEEK ) - 1 ];

        tvWeekDay.setText(weekDay);
        tvDate.setText(DateUtils.getDateStringForPattern(mTodayCalendar.getTime(), this.getString(R.string.newhome_date_format)));
    }

    private class CategoryClickListener implements View.OnClickListener{
        @Override
        public void onClick(View view)
        {
            vpDay.setCurrentItem( (int) view.getTag() );
        }
    }

    private class NewsPagerAdapter extends PagerAdapter
    {
        private View[] mNewsList = null;
        private Context mContext = null;

        public NewsPagerAdapter( Context context , View[] newsList )
        {
            this.mContext = context;
            this.mNewsList = newsList;
        }

        @Override
        public int getCount() {
            return mNewsList.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View view = mNewsList[ position ];
            container.addView( view );
            return  view;
        }
    }

    private String getDateString( Calendar calendar )
    {
        return calendar.get(Calendar.YEAR) + "-" + String.format("%02d",  calendar.get( Calendar.MONTH ) + 1 ) + "-" + String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH) );
    }

    @Click(R.id.btn_activity_newhomeas_start_fitmeter)
    void startFitmeterClick(){
        Intent intent = new Intent( this.getApplicationContext() , SetUserInfoActivity.class );
        startActivity(intent);
    }

    private class NewHomeAsWeekPagerAdaper extends WeekPagerAdapter
    {

        public NewHomeAsWeekPagerAdaper(Context context, CategoryType categoryType , long dayMilliseconds, WeekPagerType  weekPagerType , WeekPagerActivityInfo[] weekPagerActivityInfoList ) {
            super(context, 1, categoryType, null , null, dayMilliseconds , weekPagerType , weekPagerActivityInfoList );
        }

        private WeekPagerActivityInfo getWeekPagerActivityInfo(int position) {

            int weekIndex = mPagerLength - (position + 1);

            Calendar weekCalendar = Calendar.getInstance();
            weekCalendar.setTimeInMillis(mWeekCalendar.getTimeInMillis());
            weekCalendar.add(Calendar.DATE, -(weekIndex * 7));
            String weekStartDate = newHomeUtils.getDateString(weekCalendar);

            String weekRange = "";
            double[] averageMetValues = new double[]{ 0 , 0 , 0 , 0 , 0 , 0 , 0 };
            double[] exerciseTimeValues = new double[]{ 0 , 0 , 0 , 0 , 0 , 0 , 0 };
            double[] calorieValues = new double[]{ 0 , 0 , 0 , 0 , 0 , 0 , 0 };
            double[] dailyAchieveValues = new double[]{ 0 , 0 , 0 , 0 , 0 , 0 , 0 };
            double[] weeklyAchieveValues = new double[]{ 0 , 0 , 0 , 0 , 0 , 0 , 0 };
            double[] fatValues = new double[]{ 0 , 0 , 0 , 0 , 0 , 0 , 0 };
            double[] carboHydrateValues = new double[]{ 0 , 0 , 0 , 0 , 0 , 0 , 0 };
            double[] distanceValues = new double[]{ 0 , 0 , 0 , 0 , 0 , 0 , 0 };
            String[] dayStrings = new String[7];
            long[] timeMillis = new long[7];
            int selectedDayIndex = -1;
            int preMonth = -1;

            for (int i = 0; i < averageMetValues.length; i++)
            {

                String activityDate = newHomeUtils.getDateString(weekCalendar);
                DayActivity dayActivity = mDBManager.getDayActivity(activityDate);

                timeMillis[i] = weekCalendar.getTimeInMillis();

                int currentMonth = weekCalendar.get(Calendar.MONTH);
                if( preMonth != currentMonth ) {
                    dayStrings[i] = DateUtils.getDateStringForPattern(weekCalendar.getTime(), getResources().getString(R.string.newhome_week_bar_date_format));
                    preMonth = currentMonth;
                }else{
                    dayStrings[i] = DateUtils.getDateStringForPattern(weekCalendar.getTime(), getResources().getString(R.string.newhome_week_bar_date_simple_format));
                }

                //선택된 날짜인지 확인한다.
                boolean isSameDay = DateUtils.isSameDay( mDayCalendar, weekCalendar );
                if (isSameDay) {
                    selectedDayIndex = i;
                    mSelectedDayIndex = i;
                }

                if( i == 0 ){
                    weekRange += DateUtils.getDateStringForPattern( weekCalendar.getTime() , getResources().getString( R.string.newhome_week_table_week_range_format ) );
                }

                if( i == averageMetValues.length -1 ){
                    weekRange += " - " + DateUtils.getDateStringForPattern( weekCalendar.getTime() , getResources().getString( R.string.newhome_week_table_week_range_format ) );
                }

                weekCalendar.add(Calendar.DATE, 1);
            }

            float strengthFrom = 0;
            float strengthTo = 0;

            WeekPagerActivityInfo weekActivityInfo = new WeekPagerActivityInfo( weekStartDate , weekRange , dayStrings , timeMillis , preMonth , strengthFrom , strengthTo
                    ,averageMetValues , exerciseTimeValues , calorieValues , dailyAchieveValues , weeklyAchieveValues , fatValues , carboHydrateValues , distanceValues);
            return weekActivityInfo;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object){

            //mWeekGraphs[position] = null;
            container.removeView((View) object);

        }

        @Override
        protected void getWeekActivityInfo(int position) { }

        @Override
        public void OnWeekActivityReceived(FriendWeekActivity friendWeekActivity) {

        }

        @Override
        public void OnWeekActivityReceived(FriendWeekActivity[] friendWeekActivityList) {

        }
    }

}
