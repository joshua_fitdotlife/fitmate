package com.fitdotlife.fitmate;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fitdotlife.fitmate_lib.iview.IFindPasswordView;
import com.fitdotlife.fitmate_lib.presenter.FindPasswordPresenter;

public class SendPasswordDialog implements OnClickListener, IFindPasswordView {
	private Button btnSend = null;
	private Button btnFindAgain = null;
	private ImageView imgClose = null;
	private Context mContext = null;
	private TextView tvCheckUser = null;
	private TextView tvEmail = null;
	private FindPasswordPresenter mPresenter = null;
	private ProgressDialog mProcessDialog = null;

	private Dialog mDialog = null;
    public SendPasswordDialog( Context context ){
		this.mContext = context;
		this.mPresenter = new FindPasswordPresenter( this.mContext , this );
	}

	public void show(String name , String email){
		this.mDialog = new Dialog(this.mContext , R.style.CustomDialog);
		this.mDialog.setContentView(R.layout.dialog_send_password);

		this.btnSend = (Button) mDialog.findViewById(R.id.btn_send_password_dialog_send);
		this.btnSend.setOnClickListener(this);

		this.btnFindAgain = (Button) mDialog.findViewById(R.id.btn_send_password_dialog_find);
		this.btnFindAgain.setOnClickListener(this);

		this.imgClose = (ImageView) mDialog.findViewById(R.id.img_send_password_dialog_close);
		this.imgClose.setOnClickListener(this);

		this.tvEmail = (TextView) mDialog.findViewById(R.id.tv_send_password_email);
		this.tvEmail.setText(email);

		this.tvCheckUser = (TextView) mDialog.findViewById(R.id.tv_send_password_dialog_check_user);
		String checkUserString = String.format( this.mContext.getString( R.string.find_password_are_you ) , name );
		this.tvCheckUser.setText(checkUserString);

		this.mDialog.show();
	}

	@Override
	public void onClick(View view )
	{
		switch( view.getId() )
		{
		case R.id.btn_send_password_dialog_send:
			this.sendEmail();
			break;
		case R.id.btn_send_password_dialog_find:
			SeachPasswordDialog seachPasswordDialog = new SeachPasswordDialog(this.mContext);
			seachPasswordDialog.show();
			break;
		case R.id.img_send_password_dialog_close:
			this.mDialog.dismiss();
			break;
		}
	}
	
	private void sendEmail() {
		this.mPresenter.sendPasswordEmail( this.tvEmail.getText().toString() );
	}

	@Override
	public void navigateToSearchDialog(String name) {

	}

	@Override
	public void showResult(String message) {
		Toast.makeText(this.mContext, message, Toast.LENGTH_LONG).show();
	}

	@Override
	public void dismisDialog() {
		if( this.mDialog != null ){
			this.mDialog.dismiss();
		}
	}

	@Override
	public void startProgressDialog(){
		this.mProcessDialog =  new ProgressDialog(this.mContext);
		mProcessDialog.setTitle(this.mContext.getString(R.string.find_password_send_mail_dialog_title));
		mProcessDialog.setMessage(this.mContext.getString(R.string.find_password_send_mail_dialog_content));
		mProcessDialog.setIndeterminate( true );
		mProcessDialog.setCancelable( false );
		mProcessDialog.show();
	}

	@Override
	public void stopProgressDialog(){
		if( this.mProcessDialog != null ) {
			this.mProcessDialog.dismiss();
		}
	}

}
