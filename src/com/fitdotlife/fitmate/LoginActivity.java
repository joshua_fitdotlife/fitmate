package com.fitdotlife.fitmate;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.fitdotlife.fitmate_lib.iview.ILoginView;
import com.fitdotlife.fitmate_lib.presenter.LoginPresenter;

import java.io.IOException;

public class LoginActivity extends Activity implements ILoginView, OnClickListener
{
    private LoginPresenter mPresenter = null;

    private Button btnLogin = null;
    private EditText etxID = null;
    private EditText etxPassword = null;

    private TextView tvFindPassword = null;
    private TextView tvSignIn = null;

    private ProgressDialog mDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mPresenter = new LoginPresenter( this , this );

        this.btnLogin = (Button) this.findViewById( R.id.btn_login );
        this.btnLogin.setOnClickListener(this);
        this.etxID = (EditText) this.findViewById(R.id.etx_id);
        this.etxPassword = (EditText) this.findViewById(R.id.etx_pwd);

        this.tvFindPassword = (TextView) this.findViewById(R.id.tv_find_password);
        this.tvFindPassword.setOnClickListener(this);
        this.tvSignIn = (TextView) this.findViewById(R.id.tv_sign_in);
        this.tvSignIn.setOnClickListener(this);

       /* Calendar birthCalendar = Calendar.getInstance();
        birthCalendar.set(Calendar.YEAR,1977);
        birthCalendar.set(Calendar.MONTH,7);
        birthCalendar.set(Calendar.DATE, 15);

        long datelong = birthCalendar.getTime().getTime();*/
    }

    @Override
    public void navigateToHome()
    {
        Intent intent = null;
        if( getSettedUserInfo() ) {
            //intent = new Intent(LoginActivity.this, NewHomeAsActivity_.class);
            intent = new Intent(LoginActivity.this, NewHomeActivity_.class);
            //intent = new Intent(LoginActivity.this, HomeActivity.class);
        }else{
            intent = new Intent(LoginActivity.this, NewHomeAsActivity_.class);
        }

        this.startActivity(intent);

        if( !getHomeNotView() ){
            this.showTutorialActivity();
        }
    }

    private boolean getHomeNotView()
    {
        SharedPreferences pref = getSharedPreferences( "fitmateservice" , Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        return pref.getBoolean( HelpActivity.HOME_NOT_VIEW_KEY  , false);
    }

    private void showHelpActivity( ){
        Intent intent = new Intent( LoginActivity.this , HelpActivity.class );
        intent.putExtra( HelpActivity.MODE_KEY, HelpActivity.HOME_HELP_MODE );
        intent.putExtra( HelpActivity.VISIBLE_KEY, true );
        this.startActivity(intent);
    }

    private void showTutorialActivity(){
        Intent intent = new Intent( LoginActivity.this , NewHomeTutorialActivity_.class );
        this.startActivity(intent);
    }


    @Override
    public void showResult(String message)
    {
        Toast.makeText(this, message , Toast.LENGTH_LONG ).show();
    }

    public void finishActivity()
    {
        this.finish();
    }

    @Override
    public void onClick(View view)
    {
        switch( view.getId() )
        {
            case R.id.btn_login:

                if( this.validInput() )
                {
                    this.mPresenter.login(  this.etxID.getText().toString() , this.etxPassword.getText().toString() );
                }
                break;
            case R.id.tv_find_password:

                SeachPasswordDialog seachPasswordDialog = new SeachPasswordDialog(this);
                seachPasswordDialog.show();

                break;
            case R.id.tv_sign_in:

                //Intent intent = new Intent( LoginActivity.this , SetUserInfoActivity.class );
                Intent intent = new Intent( LoginActivity.this , SetAccountActivity.class );
                ActivityReference.activityReferece = this;
                this.startActivity(intent);

                break;
        }
    }

    private boolean validInput()
    {
        if(this.etxID.getText().length() == 0)
        {
            Toast.makeText( this, this.getString(R.string.login_input_email), Toast.LENGTH_SHORT ).show();
            this.etxID.requestFocus();
            return false;
        }

        if( this.etxPassword.getText().length() == 0 )
        {
            Toast.makeText( this, this.getString(R.string.login_input_password) , Toast.LENGTH_SHORT ).show();
            this.etxPassword.requestFocus();
            return false;
        }

        return true;
    }

    public void startProgressDialog(){
        this.mDialog =  new ProgressDialog(this);
        mDialog.setTitle( this.getString(R.string.login_dialog_title) );
        mDialog.setMessage( this.getString(R.string.login_dialog_content) );
        mDialog.setIndeterminate( true );
        mDialog.setCancelable( false );
        mDialog.show();
    }

    public void stopProgressDialog(){
        if( this.mDialog != null ) {
            this.mDialog.dismiss();
        }
    }

    @Override
    public void showUpdateDialog() {

        AlertDialog.Builder updateDialog = new AlertDialog.Builder(this);
        updateDialog.setTitle( getString(R.string.required_update_title) );
        updateDialog.setCancelable(false)
;        updateDialog.setMessage(getString(R.string.required_update_content));

        updateDialog.setNegativeButton( getString(R.string.common_cancel) , new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        updateDialog.setPositiveButton( getString(R.string.common_update)  , new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                Intent updateIntent = new Intent(Intent.ACTION_VIEW);
                updateIntent.setData(Uri.parse("market://details?id=" + getPackageName() ));
                startActivity(updateIntent);
                finish();
            }
        });
        updateDialog.show();

    }

    @Override
    protected void onDestroy() {

        if( this.mDialog != null ){
            this.mDialog.dismiss();
        }

        super.onDestroy();
    }

    private boolean getSettedUserInfo(){
        SharedPreferences pref = this.getSharedPreferences("fitmateservice", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        return pref.getBoolean( LoginPresenter.SET_USERINFO_KEY , false);
    }

}
