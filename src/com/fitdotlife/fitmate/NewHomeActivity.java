package com.fitdotlife.fitmate;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.util.Base64;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CalendarView;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fitdotlife.Preprocess;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ContinuousCheckPolicy;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseAnalyzer;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.FatAndCarbonhydrateConsumtion;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.StrengthInputType;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.UserInfoForAnalyzer;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.WearingLocation;
import com.fitdotlife.fitmate.model.ActivityDataListener;
import com.fitdotlife.fitmate.model.ActivityDataModel;
import com.fitdotlife.fitmate.newhome.CategoryEditInfo;
import com.fitdotlife.fitmate.newhome.CategoryType;
import com.fitdotlife.fitmate.newhome.DayPagerAdapter;
import com.fitdotlife.fitmate.newhome.NewHomeCategoryData;
import com.fitdotlife.fitmate.newhome.NewHomeProgramClickListener;
import com.fitdotlife.fitmate.newhome.NewHomeUtils;
import com.fitdotlife.fitmate.newhome.NewHomeWeekView;
import com.fitdotlife.fitmate.newhome.NewsPagerAdapter;
import com.fitdotlife.fitmate.newhome.WeekActivityListener;
import com.fitdotlife.fitmate.newhome.WeekPagerActivityInfo;
import com.fitdotlife.fitmate.newhome.WeekPagerAdapter;
import com.fitdotlife.fitmate_lib.customview.BatteryCircleView;
import com.fitdotlife.fitmate_lib.customview.BatteryPopupCircleView;
import com.fitdotlife.fitmate_lib.customview.DrawerView;
import com.fitdotlife.fitmate_lib.customview.NewHomeCategoryEditItemView;
import com.fitdotlife.fitmate_lib.customview.NewHomeCategoryEditItemView_;
import com.fitdotlife.fitmate_lib.customview.NewHomeCategoryView;
import com.fitdotlife.fitmate_lib.customview.NewHomeCategoryView_;
import com.fitdotlife.fitmate_lib.customview.NewHomeDayCircleView_;
import com.fitdotlife.fitmate_lib.customview.NewHomeDayHalfCircleView_;
import com.fitdotlife.fitmate_lib.customview.NewHomeDayTextView_;
import com.fitdotlife.fitmate_lib.customview.NewHomeNewsView;
import com.fitdotlife.fitmate_lib.customview.NewHomeNewsView_;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.http.ActivityService;
import com.fitdotlife.fitmate_lib.http.NetworkClass;
import com.fitdotlife.fitmate_lib.http.NewsService;
import com.fitdotlife.fitmate_lib.key.CommonKey;
import com.fitdotlife.fitmate_lib.object.DayActivity;
import com.fitdotlife.fitmate_lib.object.DayActivity_Rest;
import com.fitdotlife.fitmate_lib.object.ExerciseProgram;
import com.fitdotlife.fitmate_lib.object.FriendWeekActivity;
import com.fitdotlife.fitmate_lib.object.News;
import com.fitdotlife.fitmate_lib.object.NewsResult;
import com.fitdotlife.fitmate_lib.object.ScoreClass;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.object.WeekActivity;
import com.fitdotlife.fitmate_lib.object.WeekActivityWithDayActivityList;
import com.fitdotlife.fitmate_lib.presenter.LoginPresenter;
import com.fitdotlife.fitmate_lib.service.ServiceClient;
import com.fitdotlife.fitmate_lib.service.ServiceRequestCallback;
import com.fitdotlife.fitmate_lib.service.key.SyncType;
import com.fitdotlife.fitmate_lib.service.protocol.object.TimeInfo;
import com.fitdotlife.fitmate_lib.util.DateUtils;
import com.fitdotlife.fitmate_lib.util.Utils;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringArrayRes;
import org.androidannotations.rest.spring.annotations.RestService;
import org.apache.log4j.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.client.RestClientException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.WeakHashMap;

@EActivity(R.layout.activity_newhome)
public class NewHomeActivity extends Activity implements NetWorkChangeListener , WeekPagerAdapter.WeekMoveListener
{
    public static final String VIEWORDER_KEY = "vieworder";
    public static final String SELECTEDCATEGORYINDEX_KEY = "selectedcategoryindex";

    public enum LaunchMode { START , FROM_OTHERMENU , FROM_NEWSCLICK};
    private LaunchMode mLaunchMode = null;

    public static final String HOME_LAUNCH_KEY = "homelaunch";
    public static final String HOME_NEWS_KEY = "homenews";
    public static final String HOME_NEWSDATE_KEY = "homenewsdate";

    private final int PagerAnimationDuration = 4000;
    private int mWeekPagerLength = 0;

    private final int ENABLE_BT_REQUEST_ID = 1;
    private final int MAX_RECEIVE_BYTE_LENGTH = 512;

    private final String TAG = "fitmate - " + NewHomeActivity.class.getSimpleName();

    private View calendarMain = null;
    private PopupWindow calendarPopup = null;

    private int selectedCategoryIndex = 0;

    private Calendar mTodayCalendar = null;
    private int mTodayWeekNumber = 0;
    private Calendar mDayCalendar = null;
    private Calendar mWeekCalendar = null;

    private FitmateDBManager mDBManager = null;
    private UserInfo mUserInfo = null;

    private boolean isRequestCalculate = false;

    private ServiceClient mServiceClient = null;
    private ActivityDataModel mActivityDataModel = null;

    //Activity가 끝나는 시점에 서비스에게 계산 요청을 하지 않도록 막는 변수임.
    //onStop이나 onPause 시에 이 변수를 false로 설정. onResume 일 때 true로 설정.
    //onSyncInfo 함수에서 이 변수를 확인하고 계산 요청 유무를 판단한다.
    private boolean isRequestAndView = false;

    private DayPagerAdapter mDayPagerAdapter = null;
    private boolean mInitialDayDisplay = true;
    private boolean mInitialWeekDisplay = true;

    private Handler mTimerHandler = new Handler();
    private MyApplication myApplication = null;

    private NewHomeUtils newHomeUtils  = null;
    public PopupWindow batteryPopup = null;

    private PagerRunnable pagerRunnable = new PagerRunnable();
    private WeekPagerActivityInfo[] mWeekPagerActivityInfoList = null;
    private NewHomeWeekPagerAdapter mWeekBarPagerAdapter = null;
    private NewHomeWeekPagerAdapter mWeekTablePagerAdapter = null;

    private NewHomeCategoryView mCategoryEditCategoryView = null;

    //카테고리 뷰의 위치에 해당 카테고리 타입을 저장하고 있는 배열.
    //다시 말하자면 ViewIndex에 카테고리 타입을 저장하고 있음.
    public CategoryType[] mCategoryViewOrder = null;

    //카테고리 수정용 카테고리 배열.
    NewHomeCategoryView[] categoryViews = new NewHomeCategoryView[ CategoryType.values().length ];
    NewHomeCategoryView[] categoryEditViews = new NewHomeCategoryView[ CategoryType.values().length ];

    View[] dayViews = new View[ CategoryType.values().length ];

    @RestService
    NewsService newsServiceClient;

    @ViewById(R.id.vp_activity_newhome_day)
    ViewPager vpDay;

    @ViewById(R.id.vp_activity_newhome_week_bar)
    ViewPager vpWeekBar;

    @ViewById(R.id.vp_activity_newhome_week_table)
    ViewPager vpWeekTable;

    @ViewById(R.id.rl_activity_newhome_week)
    RelativeLayout rlWeek;

    @ViewById(R.id.vp_activity_newhome_news)
    ViewPager vpNews;

    @ViewById(R.id.tv_activity_newhome_date)
    TextView tvDate;

    @ViewById(R.id.tv_activity_newhome_weekday)
    TextView tvWeekDay;

    @ViewById(R.id.tv_activity_newhome_date)
    TextView newHomeDate;

    @ViewById(R.id.hsv_activity_newhome_category)
    HorizontalScrollView hsvCategory;

    @ViewById(R.id.ll_activity_newhome_category)
    LinearLayout llCategory;

    @StringArrayRes(R.array.newhome_category_char)
    String[] arrCategoryChar;

    @StringArrayRes(R.array.newhome_dayview_char)
    String[] arrDayViewChar;

    @ViewById(R.id.img_activity_newhome_sync)
    ImageView imgSync;

    @ViewById(R.id.dl_activity_newhome)
    DrawerLayout dlNewHome;

    @ViewById(R.id.dv_activity_newhome_drawer)
    DrawerView drawerView;

    @ViewById(R.id.rl_activity_newhome_actionbar_battery)
    RelativeLayout rlBattery;

    @ViewById(R.id.rl_activity_newhome_actionbar_sync)
    RelativeLayout rlSync;

    @ViewById(R.id.img_activity_newhome_drawer)
    ImageView imgDrawer;

    @ViewById(R.id.bcv_activity_newhome_battery)
    BatteryCircleView batteryCircleView;

    @ViewById(R.id.img_activity_newhome_battery_led)
    ImageView imgBatteryLed;

    @ViewById(R.id.tv_activity_newhome_battery_value)
    TextView tvBatteryValue;

    @ViewById(R.id.tv_activity_newhome_sync_percent)
    TextView tvSyncPercent;

    @ViewById(R.id.rl_activity_newhome_actionbar)
    RelativeLayout rlActionBar;

    @ViewById(R.id.rl_activity_newhome_actionbar_categoryedit)
    RelativeLayout rlCategoryEditActionBar;

    @ViewById(R.id.ll_activity_newhome_program)
    LinearLayout llProgram;

    @ViewById(R.id.rl_activity_newhome_program)
    RelativeLayout rlProgram;

    @ViewById(R.id.rl_activity_newhome_week_progress)
    RelativeLayout rlWeekProgress;

    @ViewById(R.id.ll_activity_newhome_categoryedit_popup)
    LinearLayout llCategoryEditPopup;

    @ViewById( R.id.gv_activity_newhome_categoryedit_popup  )
    GridView gvCategoryEditPopup;

    @ViewById(R.id.tv_activity_newhome_actionbar_categoryedit_save)
    TextView tvCategoryEditSave;

    @ViewById(R.id.hsv_activity_newhome_categoryedit_category)
    HorizontalScrollView hsvCategoryEdit;

    @ViewById(R.id.ll_activity_newhome_categoryedit_category)
    LinearLayout llCategoryEdit;

    @RestService
    ActivityService activityServiceClient;

    //운동 프로그램이 변경되는 것을 감지하여 일 주 그래프를 업데이트 한다.
//    private BroadcastReceiver mProgramChangetReceiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent)
//        {
//            displayDayData( false );
//            displayWeekData();
//        }
//    };

    //사용자 정보가 변경되는 것을 감지하여 일 주 그래프를 업데이트한다.
    private BroadcastReceiver mUserInfoChangeReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent)
        {
            mUserInfo = mDBManager.getUserInfo();
            displayDayData( false );
            displayWeekData();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i("fitmate", "View onCreate()");

        this.setTheme(R.style.OrangeTheme);
        //this.setTheme( R.style.PurpleTheme);

        IntentFilter programChangeFilter = new IntentFilter();
        programChangeFilter.addAction(ExerciseProgramInfoActivity.ACTION_PROGRAM_CHANGE);
        this.registerReceiver( mProgramChangetReceiver , programChangeFilter  );

        //홈 화면을 계속 띄우는 걸로 변경되어서 아래 코드가 필요없음.
        //운동 프로그램이 변경되는 것을 감지하여 일 주 그래프를 업데이트 한다.
//        IntentFilter programChangeFilter = new IntentFilter();
//        programChangeFilter.addAction(ExerciseProgramInfoActivity.ACTION_PROGRAM_CHANGE);
//        this.registerReceiver( mProgramChangetReceiver , programChangeFilter  );
        //사용자 정보가 변경되는 것을 감지하여 일 주 그래프를 업데이트한다.
        IntentFilter userinfoChangeFilter = new IntentFilter();
        userinfoChangeFilter.addAction( SettingActivity.ACTION_USERINFO_CHANGE );
        this.registerReceiver( mUserInfoChangeReceiver , userinfoChangeFilter );

        ActivityManager.getInstance().addActivity(this);
        myApplication = (MyApplication) this.getApplication();

        Intent intent = this.getIntent();
        mLaunchMode = mLaunchMode.values()[ intent.getIntExtra( HOME_LAUNCH_KEY , 0 )];

        if( !Preprocess.IsTest) {
            Tracker t = myApplication.getTracker(MyApplication.TrackerName.APP_TRACKER);
            t.setScreenName("Home");
            t.send(new HitBuilders.AppViewBuilder().build());
        }
        
        activityServiceClient.setRootUrl(NetworkClass.baseURL + "/api");

        this.mDBManager = new FitmateDBManager(this);
        this.mUserInfo = this.mDBManager.getUserInfo();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime( new Date() );

        mTodayCalendar = Calendar.getInstance();
        mTodayCalendar.setTimeInMillis( calendar.getTimeInMillis() );
        mTodayWeekNumber = mTodayCalendar.get(Calendar.DAY_OF_WEEK);

        mDayCalendar = Calendar.getInstance();
        mDayCalendar.setTimeInMillis( calendar.getTimeInMillis() );

        this.mWeekCalendar = Calendar.getInstance();
        this.mWeekCalendar.setTimeInMillis(calendar.getTimeInMillis());
        int weekNumber = mWeekCalendar.get(Calendar.DAY_OF_WEEK);
        mWeekCalendar.add(Calendar.DATE, -(weekNumber - 1));

        this.mActivityDataModel = new ActivityDataModel(this, new ActivityDataListener()
        {
            @Override
            public void onDayActivityReceived() {}

            @Override
            public void onWeekActivityReceived() {}

            @Override
            public void onMonthActivityReceived() {}

            @Override
            public void onUploadedData(boolean isUpload) {}
        });

        this.mServiceClient = new ServiceClient(this, new ServiceRequestCallback() {
            @Override
            public void onServiceResultReceived(int what, Bundle data)
            {
                if( what == ServiceClient.MSG_CALCULATE_ACTIVITY ){
                    String weekUpdateList = null;

                    if( data != null )
                    {
                        weekUpdateList = data.getString("recalculateweeklist");
                    }

                    final String finalWeekUpdateList = weekUpdateList;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            calculateCompleted(finalWeekUpdateList);
                        }
                    });

                }else if( what == ServiceClient.MSG_DATA_SYNC ){
                    SyncType syncType = SyncType.getSyncType( data.getInt( "synctype" ) );

                    if( syncType.equals( SyncType.PROGRESS ) ){
                        int receiveBytes = data.getInt( "receivebyte" );
                        int totalBytes = data.getInt( "totalbyte" );
                        if (totalBytes > MAX_RECEIVE_BYTE_LENGTH )
                        {
                            int progressPercent = ((receiveBytes * 100) / totalBytes);
                            tvSyncPercent.setText( progressPercent + "" );
                        }else{
                            dismissSyncView();
                        }
                    }
                    else if( syncType.equals( SyncType.END ))
                    {
                        dismissSyncView();
                        if( isRequestAndView ){
                            requestCalculateActivity();
                            //setRecentConnectedTime( Calendar.getInstance() );
                        }
                    }
                }
                else if(what== ServiceClient.MSG_BATTERYRATE){
                    int batteryRatio= data.getInt("battery");
                    setBattery( batteryRatio );
                }
                else if(what== ServiceClient.MSG_STOP_SERVICE){
                    Toast.makeText( getApplicationContext() , "STOP 완료", Toast.LENGTH_SHORT ).show();
                }
            }

            @Override
            public void serviceInitializeCompleted() {

            }
        });
        this.mServiceClient.open();
        this.mServiceClient.start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("fitmate", "View onResum");
        this.isRequestAndView = true;

        //이 함수로 진입할 수 있는 경우는 아래와 같이 4가지이다.
        //1. 새로 시작할 때
        //2. 활동, 운동 프로그램, 친구, 뉴스 로부터 이동해서 왔을 때 , 백버튼, 액션바에 홈버튼, 사이드 메뉴에 홈 버튼.
        //3. Background에서 ForeGround로 이동할 때
        //4. 뉴스에서 나의 뉴스를 클릭했을 때

        if( mLaunchMode.equals( LaunchMode.FROM_NEWSCLICK) )
        {
            mDayCalendar.setTimeInMillis( this.getIntent().getLongExtra(HOME_NEWSDATE_KEY, System.currentTimeMillis()) );
            setDateText();
            getWeekActivity();
            setHomeNews(false);

            showBatteryView();
            if(isBtEnabled()){

                this.requestCalculateActivity();
            }

        }
        else if(mLaunchMode.equals( LaunchMode.START ))
        {

            if (!this.getCheckBluetoothOn())
            { // 블루투스를 체크를 안했다면

                if (!isBtEnabled()) { //블루투스가 켜져 있지 않다면

                    if (!this.getRequestBluetoothOn()) {
                        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                        this.startActivityForResult(enableBtIntent, this.ENABLE_BT_REQUEST_ID);
                        this.setRequestBluetoothOn(true);
                    }
                } else {
                    this.showSyncView();
                    this.startTimer();
                    this.setCheckBluetoothOn(true);

                    this.resetGetData();
                    this.requestCalculateActivity();
                }
            }
        }else if(mLaunchMode.equals( LaunchMode.FROM_OTHERMENU )){

            showBatteryView();
            if(isBtEnabled()){
                this.requestCalculateActivity();
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(!Preprocess.IsTest) {
            GoogleAnalytics.getInstance(this).reportActivityStart(this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(!Preprocess.IsTest) {
            GoogleAnalytics.getInstance(this).reportActivityStop(this);
        }
    }

    /**
     * 액티비티가 꺼질 때 이벤트 핸들러
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();

        myApplication.deleteNetworkChangeListener(this);
        this.mServiceClient.close();

        //운동 프로그램이나 사용자 정보가 변경되면 일 주 그래프를 업데이트한다.
        //this.unregisterReceiver(mProgramChangetReceiver);
        this.unregisterReceiver(mUserInfoChangeReceiver);

        writeSelectedCategoryIndex();
    }

    private void writeSelectedCategoryIndex( ){
        SharedPreferences pref = this.getSharedPreferences("fitmate", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(SELECTEDCATEGORYINDEX_KEY, selectedCategoryIndex);
        editor.commit();
    }

    /**
     * 백버튼 눌렀을 때 이벤트 핸들러.
     */
    @Override
    public void onBackPressed() {

        if( dlNewHome.isDrawerOpen(drawerView) ){
            dlNewHome.closeDrawers();
            return;
        }

        if( newHomeUtils.programPopup != null)
        {
            if (newHomeUtils.programPopup.isShowing()) {
                newHomeUtils.programPopup.dismiss();
                newHomeUtils.programPopup = null;
                return;
            }
        }

        if( newHomeUtils.categoryIntroPopup != null ){

            if (newHomeUtils.categoryIntroPopup.isShowing()) {
                newHomeUtils.categoryIntroPopup.dismiss();
                newHomeUtils.categoryIntroPopup = null;
                return;
            }
        }

        if( calendarPopup != null )
        {
            if( calendarPopup.isShowing() )
            {
                calendarPopup.dismiss();
                return;
            }
        }

        if( batteryPopup != null ){
            if( batteryPopup.isShowing() ){
                batteryPopup.dismiss();
                return;
            }
        }

        if( hsvCategoryEdit.getVisibility()  == View.VISIBLE ){
            cancelCategoryEdit();
            return;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);     // 여기서 this는 Activity의 this

        builder.setTitle(getString(R.string.common_quit_title))        // 제목 설정
                .setMessage(getString(R.string.common_quit_content))        // 메세지 설정
                .setCancelable(true)        // 뒤로 버튼 클릭시 취소 가능 설정
                .setPositiveButton(R.string.common_yes, new DialogInterface.OnClickListener() {
                    // 확인 버튼 클릭시 설정
                    public void onClick(DialogInterface dialog, int whichButton) {
                        moveTaskToBack(true);
                        finish();
                        dialog.cancel();
                    }
                })
                .setNegativeButton(R.string.common_no, new DialogInterface.OnClickListener() {
                    // 취소 버튼 클릭시 설정
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.cancel();
                    }
                });

        AlertDialog dialog = builder.create();    // 알림창 객체 생성
        dialog.show();    // 알림창 띄우기
    }

    /**
     * 사용자가 블루투스를 켰는지 확인하는 이벤트 핸들러.
     * @param resultCode
     */
    @OnActivityResult(ENABLE_BT_REQUEST_ID)
    void btResult( int resultCode )
    {
        Log.i("fitmate", "HomeActivity - response bluetooth" + resultCode);
        this.setCheckBluetoothOn(true);
        if( resultCode == RESULT_CANCELED ){
            this.setInitialViewComplete(true);
            showBatteryView();
            //데이터를 가져온다.
        }else if(resultCode == RESULT_OK){
            //블루투스가 사용자에 의해 방금 켜졌기 때문에 계산 요청을 하지 않는다.
            this.showSyncView();
            this.startTimer();
        }
    }

    @AfterViews
    void init()
    {
        mCategoryViewOrder = Utils.readTabViewOrder(this);
        this.selectedCategoryIndex = Utils.readSelectedCategoryIndex( this );
        newsServiceClient.setRootUrl(NetworkClass.baseURL + "/api");

        this.newHomeUtils = new NewHomeUtils( this , mDBManager , arrCategoryChar , arrDayViewChar , new ProgramClickListener() );
        //날짜 관련 뷰
        setDateText();

        //사이드 메뉴
        this.drawerView.setParentActivity(this);
        this.drawerView.setTag(this.getResources().getString(R.string.gnb_home));
        this.drawerView.setDrawerLayout(dlNewHome);

        //ActivityManager.getInstance().addActivity(this);

        //주간 그래프의 크기를 결정한다.
        try {

            Calendar endCalendar = Calendar.getInstance();
            endCalendar.set( mWeekCalendar.get(Calendar.YEAR) , mWeekCalendar.get( Calendar.MONTH ) , mWeekCalendar.get(Calendar.DATE) );
            long diffDates = DateUtils.diffOfDate( endCalendar );
            mWeekPagerLength = ((int) (diffDates / 7) + 1);
        } catch (Exception e) {
            mWeekPagerLength = 1000000;
        }

        //주간 그래프의 주 정보 배열을 초기화한다.
        mWeekPagerActivityInfoList = new WeekPagerActivityInfo[ mWeekPagerLength ];

        //배터를 설정한다.
        int batteryRatio = this.mDBManager.getLastBatteryRatio( mUserInfo.getEmail() );
        setBattery(batteryRatio);

        if( Utils.isOnline(this) ){
            this.getNewsList();
        }else{
            myApplication.addNetworkChangeListener(this);
            this.displayNews( null );
        }

        getWeekActivity();
    }

    private void setBattery( int batteryRatio )
    {
        batteryCircleView.setBatteryValue(batteryRatio);
        batteryCircleView.invalidate();

        if( batteryRatio < 21 ){
            imgBatteryLed.setImageResource(R.drawable.batter_light_empty);
        }else if( batteryRatio < 51 ){
            imgBatteryLed.setImageResource( R.drawable.batter_light_medium );
        }else{
            imgBatteryLed.setImageResource(R.drawable.batter_light_full);
        }

        String batteryValueStrng = "!";
        if( batteryRatio > 0 ){
            batteryValueStrng = batteryRatio + "";
        }
        tvBatteryValue.setText(batteryValueStrng);
    }

    private void setDateText(){

        Date activityDate = mDayCalendar.getTime();
        Date todayDate = new Date();

        String weekDay = null;
        weekDay = this.getResources().getStringArray(R.array.week_string)[mDayCalendar.get(Calendar.DAY_OF_WEEK) - 1];

        tvWeekDay.setText(weekDay);
        tvDate.setText(DateUtils.getDateStringForPattern(mDayCalendar.getTime(), this.getString(R.string.newhome_date_format)));
    }

    //저장되어 있는 카테고리 View 순서를 가져온다.
    private void getViewOrder()
    {

    }

    /**
     * 주간 데이터를 가져온다.
     * 로컬 DB에 주간 데이터가 없으면 서버로부터 가져온다.
     */
    @Background
    void getWeekActivity( )
    {
        String weekStartDate = newHomeUtils.getWeekStartDate(mDayCalendar);
        WeekActivity weekActivity = mDBManager.getWeekActivity(weekStartDate);

        if( weekActivity == null ){

            WeekActivityWithDayActivityList weekActivityWithDayActivityList = null;
            try {
                weekActivityWithDayActivityList = activityServiceClient.getWeekActivityWithDayActivityList(mUserInfo.getEmail(), weekStartDate);
            }catch( RestClientException e){  }

            if(weekActivityWithDayActivityList != null)
            {
                if( !weekActivityWithDayActivityList.getResponseValue().equalsIgnoreCase("nodata") ){
                    setWeekInfoDayActivityList( weekActivityWithDayActivityList );
                }
            }
        }

        display();
    }

    @Background
    void getWeekActivity( String weekStartDate , WeekActivityListener listener ){

        WeekActivityWithDayActivityList weekActivityWithDayActivityList = null;
        try {
            weekActivityWithDayActivityList = activityServiceClient.getWeekActivityWithDayActivityList(mUserInfo.getEmail(), weekStartDate);
        }catch( org.springframework.web.client.RestClientException e ){ }

        if(weekActivityWithDayActivityList != null) {
            if (!weekActivityWithDayActivityList.getResponseValue().equals("nodata")) {
                setWeekInfoDayActivityList(weekActivityWithDayActivityList);
            }
        }

        listener.OnWeekActivityReceived();

    }


    @UiThread
    void display()
    {
        displayDayData( true );
        displayWeekData();

        if( rlWeekProgress.getVisibility() == View.VISIBLE ){
            dismissProgressView();
        }
    }

    private void displayDayData(boolean startZero)
    {

        DayActivity dayActivity = mDBManager.getDayActivity(newHomeUtils.getDateString(mDayCalendar));

        if( dayActivity != null ) {
            if (dayActivity.getFat() < 0) {
                FatCarboDistance fatCarboDistance = this.calculateFatCarboDistance(dayActivity);
                dayActivity.setFat(fatCarboDistance.fat);
                dayActivity.setCarbohydrate(fatCarboDistance.carbohydrate);
                dayActivity.setDistance(fatCarboDistance.distance);
            }
        }else{
            dayActivity = new DayActivity();
        }

        ExerciseProgram program = getExerciseProgram();

        if( mInitialDayDisplay ){
            addDayData(dayActivity, program, startZero);
            mInitialDayDisplay = false;
        }else{
            updateDayData(dayActivity, program, startZero);
        }

    }

    private ExerciseProgram getExerciseProgram()
    {

        ExerciseProgram program = null;
        String weekStartDate = newHomeUtils.getWeekStartDate( mDayCalendar );
        WeekActivity weekActivity = mDBManager.getWeekActivity(weekStartDate);

        if( weekActivity != null ) {
            program = mDBManager.getUserExerciProgram(weekActivity.getExerciseProgramID());
        }else{
            program = mDBManager.getAppliedProgram();
        }

        return program;
    }

    private void addDayData( DayActivity dayActivity , ExerciseProgram program , boolean startZero )
    {

        for( int categoryIndex = 0 ; categoryIndex < CategoryType.values().length ; categoryIndex++ ){
            CategoryType categoryType = CategoryType.values()[ categoryIndex ];

            NewHomeCategoryView categoryView = NewHomeCategoryView_.build(this);
            categoryView.setSelectListener(new CategoryClickListener());
            View dayView = null;

            if( categoryType.equals( CategoryType.DAILY_ACHIEVE ) ){
                dayView = NewHomeDayHalfCircleView_.build(this);
            }else if(categoryType.equals( CategoryType.WEEKLY_ACHIEVE )){
                dayView = NewHomeDayCircleView_.build(this);
            }else{
                dayView = NewHomeDayTextView_.build(this);
            }

            newHomeUtils.setDayValue(categoryView, dayView, NewHomeCategoryData.getNewHomeCategoryData(this, dayActivity, program, mUserInfo), program.getId(), categoryType,startZero);
            newHomeUtils.unSelectCategoryView( categoryView );

            categoryViews[ categoryType.ordinal() ] = categoryView;
            dayViews[ categoryType.ordinal() ] = dayView;
        }

        View[] dayGraphs = new View[ mCategoryViewOrder.length ];
        for( int viewIndex = 0 ; viewIndex < mCategoryViewOrder.length ; viewIndex++ )
        {
            CategoryType categoryType = mCategoryViewOrder[ viewIndex ];
            NewHomeCategoryView categoryView = categoryViews[ categoryType.ordinal() ];
            View dayView = dayViews[categoryType.ordinal() ];

            dayView.setTag(viewIndex);
            categoryView.setViewIndex(viewIndex);
            newHomeUtils.setCategorySelect(categoryView, viewIndex , selectedCategoryIndex);

            dayGraphs[viewIndex] = dayView;
            llCategory.addView(categoryView);
        }

        //카테고리 수정버튼을 추가한다.
        this.mCategoryEditCategoryView = NewHomeCategoryView_.build(this);
        StateListDrawable categoryEditSelector = new StateListDrawable();
        categoryEditSelector.addState( new int[]{android.R.attr.state_pressed} , getResources().getDrawable( R.drawable.category_edit_sel ) );
        categoryEditSelector.addState(new int[]{}, getResources().getDrawable(R.drawable.category_edit));
        mCategoryEditCategoryView.setImageBackground(categoryEditSelector);
        mCategoryEditCategoryView.setCategoryText(getResources().getString(R.string.tab_setting));
        mCategoryEditCategoryView.setOnClickListener(new CategoryEditClickListener());
        mCategoryEditCategoryView.setCategoryTextColor(Color.WHITE);
        llCategory.addView(mCategoryEditCategoryView);

        llCategory.post(new Runnable() {
            @Override
            public void run() {
                changeCategoryViewGravity( llCategory , hsvCategory );
            }
        });

        mDayPagerAdapter = new DayPagerAdapter(this , dayGraphs);
        vpDay.setAdapter(mDayPagerAdapter);
        vpDay.setCurrentItem(selectedCategoryIndex);
        vpDay.setOnPageChangeListener(new ViewPager.OnPageChangeListener()
        {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                newHomeUtils.unSelectCategoryView((NewHomeCategoryView) llCategory.getChildAt(selectedCategoryIndex));
                int preSelectedCategoryIndex = selectedCategoryIndex;
                selectedCategoryIndex = position;
                NewHomeCategoryView newHomeCategoryView = (NewHomeCategoryView) llCategory.getChildAt(selectedCategoryIndex);
                newHomeUtils.selectCategoryView(newHomeCategoryView);

                if( preSelectedCategoryIndex > selectedCategoryIndex){ //카테고리를 왼쪽에서 오른쪽으로 이동하는 경우

                    int viewLeft = newHomeCategoryView.getLeft();
                    if( hsvCategory.getScrollX() > viewLeft )
                    {
                        hsvCategory.smoothScrollTo( viewLeft , 0);
                    }

                }else if(preSelectedCategoryIndex < selectedCategoryIndex){ //카테고리를 오른족에서 왼쪽으로 이동하는 경우

                    int viewRight = newHomeCategoryView.getRight();
                    if( viewRight > ( hsvCategory.getScrollX() + hsvCategory.getRight() ) ) {

                        hsvCategory.smoothScrollTo( viewRight - hsvCategory.getRight()  , 0);
                    }
                }

//                //일 그래프를 애니메이션 시킨다.
                CategoryType preCategoryType = mCategoryViewOrder[ preSelectedCategoryIndex ];
                CategoryType categoryType = mCategoryViewOrder[selectedCategoryIndex];
//                DayPagerAdapter dayPagerAdapter = (DayPagerAdapter) vpDay.getAdapter();
//                View view = dayPagerAdapter.getDayGraphs()[selectedCategoryIndex];
//                if( categoryType.equals( CategoryType.WEEKLY_ACHIEVE ) ){
//
//                    NewHomeDayCircleView dayCircleView = (NewHomeDayCircleView)view;
//                    dayCircleView.startAnimation( true );
//
//                }else if( categoryType.equals( CategoryType.DAILY_ACHIEVE ) ){
//
//                    NewHomeDayHalfCircleView dayHalfCircleView = (NewHomeDayHalfCircleView) view;
//                    dayHalfCircleView.startAnimation( true );
//
//                }else {
//                    NewHomeDayTextView dayTextView = (NewHomeDayTextView) view;
//                    dayTextView.startAnimation( true );
//                }


                // 주 그래프를 선택한다.
                if( categoryType.equals( CategoryType.WEEKLY_ACHIEVE ) )
                {

                    vpWeekBar.setVisibility( View.INVISIBLE );
                    vpWeekTable.setVisibility(View.VISIBLE);
                    mWeekTablePagerAdapter.setDayTimeMilliseconds( mDayCalendar.getTimeInMillis() );
                    mWeekTablePagerAdapter.setSelectedDayIndex( mWeekBarPagerAdapter.getSelectedDayIndex() );

                    if( vpWeekTable.getCurrentItem() != vpWeekBar.getCurrentItem() )
                    {

                        mWeekTablePagerAdapter.setMoveWeek(false);
                        vpWeekTable.setCurrentItem( vpWeekBar.getCurrentItem() );

                    }

                }else if( preCategoryType.equals( CategoryType.WEEKLY_ACHIEVE ) ){

                    vpWeekBar.setVisibility(View.VISIBLE);
                    vpWeekTable.setVisibility(View.INVISIBLE);
                    mWeekBarPagerAdapter.setDayTimeMilliseconds(mDayCalendar.getTimeInMillis());
                    mWeekBarPagerAdapter.setSelectedDayIndex( mWeekTablePagerAdapter.getSelectedDayIndex() );
                    mWeekBarPagerAdapter.changeCategoryType(categoryType);
                    if( vpWeekTable.getCurrentItem() != vpWeekBar.getCurrentItem() ){

                        mWeekBarPagerAdapter.setMoveWeek(false);
                        vpWeekBar.setCurrentItem( vpWeekTable.getCurrentItem() );

                    }

                }else{

                    mWeekBarPagerAdapter.changeCategoryType(categoryType);

                }
            }

            @Override
            public void onPageScrollStateChanged(int state)
            {

            }
        });

    }

    private void updateDayData( DayActivity dayActivity , ExerciseProgram program , boolean startZero ){

        for( int categoryIndex = 0 ; categoryIndex < CategoryType.values().length ; categoryIndex++ )
        {
            NewHomeCategoryView categoryView = categoryViews[categoryIndex];
            View dayView = dayViews[ CategoryType.values()[categoryIndex].ordinal()];
            newHomeUtils.setDayValue(categoryView, dayView, NewHomeCategoryData.getNewHomeCategoryData(this, dayActivity, program, mUserInfo), program.getId(), CategoryType.values()[categoryIndex], startZero);
        }

    }

    /**
     * 주간 데이터를 그린다.
     */
    private void displayWeekData(){

        if(mInitialWeekDisplay){
            addWeekData();
            mInitialWeekDisplay = false;
        }else{
            updateWeekData();
        }
    }

    private void addWeekData(){

        CategoryType barCategoryType = mCategoryViewOrder[ selectedCategoryIndex ];
        if(mCategoryViewOrder[ selectedCategoryIndex ].equals( CategoryType.WEEKLY_ACHIEVE ))
        {
            barCategoryType = CategoryType.DAILY_ACHIEVE;
        }

        mWeekBarPagerAdapter = new NewHomeWeekPagerAdapter( this, mWeekPagerLength , barCategoryType , new WeekCellClickListener() , this , mDayCalendar.getTimeInMillis() , WeekPagerAdapter.WeekPagerType.WEEK_BAR , mWeekPagerActivityInfoList );
        mWeekBarPagerAdapter.setMoveWeek( false );
        vpWeekBar.addOnPageChangeListener(mWeekBarPagerAdapter);
        vpWeekBar.setAdapter(mWeekBarPagerAdapter);
        vpWeekBar.setCurrentItem(mWeekPagerLength);

        mWeekTablePagerAdapter = new NewHomeWeekPagerAdapter(this , mWeekPagerLength , CategoryType.WEEKLY_ACHIEVE , new WeekCellClickListener() , this , mDayCalendar.getTimeInMillis() , WeekPagerAdapter.WeekPagerType.WEEK_TABLE , mWeekPagerActivityInfoList);
        mWeekTablePagerAdapter.setMoveWeek( false );
        vpWeekTable.addOnPageChangeListener(mWeekTablePagerAdapter);
        vpWeekTable.setAdapter(mWeekTablePagerAdapter);
        vpWeekTable.setCurrentItem(mWeekPagerLength);

        CategoryType categoryType = mCategoryViewOrder[selectedCategoryIndex];
        if( categoryType.equals( CategoryType.WEEKLY_ACHIEVE ) ){

            vpWeekBar.setVisibility(View.INVISIBLE);
            vpWeekTable.setVisibility(View.VISIBLE);

            }else{

            vpWeekBar.setVisibility( View.VISIBLE );
            vpWeekTable.setVisibility(View.INVISIBLE);

            }
        }

    private int getWeekDiff(){

        Calendar weekCalendar = Calendar.getInstance();
        weekCalendar.setTimeInMillis(mDayCalendar.getTimeInMillis());
        int weekNumber = weekCalendar.get(Calendar.DAY_OF_WEEK);
        weekCalendar.add(Calendar.DATE, -(weekNumber - 1));

        long diff = mWeekCalendar.getTimeInMillis() - weekCalendar.getTimeInMillis();
        long diffDays = diff / (24 * 60 * 60 * 1000);

        int weekDiff = (int) (diffDays / 7);

        return mWeekPagerLength - ( weekDiff + 1 );
        }

    private void updateWeekData()
        {
        int position = getWeekDiff();
        int selectedDayIndex = mDayCalendar.get( Calendar.DAY_OF_WEEK );

        if( vpWeekBar.getVisibility() == View.VISIBLE ) {

            mWeekBarPagerAdapter.setDayTimeMilliseconds(mDayCalendar.getTimeInMillis());
            mWeekBarPagerAdapter.setSelectedDayIndex(selectedDayIndex - 1);
            mWeekBarPagerAdapter.setMoveWeek(false);
            vpWeekBar.setCurrentItem(position);

        }else if( vpWeekTable.getVisibility()  == View.VISIBLE ) {

            mWeekTablePagerAdapter.setDayTimeMilliseconds(mDayCalendar.getTimeInMillis());
            mWeekTablePagerAdapter.setSelectedDayIndex(selectedDayIndex - 1);
            mWeekTablePagerAdapter.setMoveWeek(false);
            vpWeekTable.setCurrentItem(position);

        }
    }

    @Click(R.id.ll_activity_newhome_calendar)
    void calendarClick(){

        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.calendarMain = inflater.inflate(R.layout.calendar_view, null);
        this.calendarPopup = new PopupWindow( calendarMain, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        this.calendarPopup.setAnimationStyle(-1);
        //this.calendarPopup.showAsDropDown(imgClendar);

        //팝업의 y좌표를 가져온다.
        //상단의 statusbar 아래에서 보여줘야 하기 때문에
        //홈에서 제일 상단의 뷰의 위치의 y좌표부터 팝업을 보여주는 것으로 함.
        int[] loc = new int[2];
        rlActionBar.getLocationOnScreen(loc);

        this.calendarPopup.showAtLocation(calendarMain, Gravity.CENTER, 0, loc[1]);

        final CalendarView calendarView = (CalendarView) calendarMain.findViewById(R.id.cv_activity_newhome);
        calendarView.setDate(mDayCalendar.getTimeInMillis(), false, true);
        calendarView.setMaxDate(mTodayCalendar.getTimeInMillis());
        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {

            private int preYear = mDayCalendar.get(Calendar.YEAR);
            private int preMonth = mDayCalendar.get(Calendar.MONTH);
            private int preDay = mDayCalendar.get(Calendar.DAY_OF_MONTH);

            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {

                if (preYear != year || preMonth != month || preDay != dayOfMonth) {

                    mDayCalendar.set(year, month, dayOfMonth);
                    changeDay();

                    calendarPopup.dismiss();
                }
            }

        });
    }

    @Click(R.id.img_activity_newhome_drawer)
    void drawerClick(){

        dlNewHome.openDrawer(drawerView);

    }

    private boolean isBtEnabled()
    {
        final BluetoothManager manager = (BluetoothManager) this.getSystemService(Context.BLUETOOTH_SERVICE);
        if( manager == null ) return false;

        final BluetoothAdapter adapter = manager.getAdapter();
        if( adapter == null ) return false;
        return adapter.isEnabled();
    }

    public void requestCalculateActivity()
    {
        if( this.isRequestCalculate )
        {
            return;
        }

        isRequestCalculate = true;


        try {
            this.mServiceClient.sendServiceMessage(ServiceClient.MSG_CALCULATE_ACTIVITY);

        } catch (RemoteException e) {
            isRequestCalculate = false;
            //this.init();
            //TODO 현재 사용자가 보고 있는 뷰를 확인하여 오늘과 관련된 날자면 주, 일을 업데이트 해야한다.
        }
    }

    //홈 초기 실행시에 빠른 계산을 위해 서비스의 프로세스가 sleep 중이면
    //슬립 프로세스를 죽이고 장치연결부터 다시 시작하도록 한다.
    public void resetGetData(){
        try {
            this.mServiceClient.sendServiceMessage(ServiceClient.MSG_RESET_GET_DATA);
        } catch (RemoteException e) {
            android.util.Log.e(TAG, "데이터 쓰레드를 다시 요청하는 중에 에러가 발생했습니다..");
            requestCalculateActivity();
        }
    }

    @Background
    void getNewsList(){

        try {
        NewsResult newsResult =  newsServiceClient.getNewsList(mUserInfo.getEmail(), 0);
        displayNews(newsResult);
        }catch( RestClientException e ){
            Log.e("fitamte" , "GetNewsList : " + e.getMessage());
        }
    }

    @UiThread
    void displayNews( NewsResult newsResult ){

        View[] newsViews = null;

        if(newsResult != null )
        {
            if( newsResult.getResponsevalue().equals(CommonKey.SUCCESS ) )
            {
                List<News> newsList = newsResult.getNewsList();
                if( newsList != null ) {
                    if (newsList.size() > 0) {

                        newsViews = new View[newsList.size()];
                        for (int i = 0; i < newsList.size(); i++) {
                            News news = newsList.get(i);

                            NewHomeNewsView newHomeNewsView = NewHomeNewsView_.build(this);
                            newHomeNewsView.setNews(news);
                            newHomeNewsView.setTag( news.getId() );
                            newHomeNewsView.setOnClickListener(new NewsClickListener());
                            newsViews[ i ] = newHomeNewsView;
                        }
                    } else {
                        newsViews = new View[]{newHomeUtils.getDefaultNewsView()};
                    }
                }else{
                    newsViews = new View[]{newHomeUtils.getDefaultNewsView()};
                }
            }else{
                newsViews = new View[]{newHomeUtils.getDefaultNewsView()};
            }
        }else{
            newsViews = new View[]{newHomeUtils.getDefaultNewsView()};
        }

        NewsPagerAdapter newsPagerAdapter = new NewsPagerAdapter(this , newsViews);
        vpNews.setAdapter(newsPagerAdapter);

        //vpNews.setPageTransformer(true, new ZoomOutPageTransformer());

        pagerRunnable.setPosition( vpNews.getCurrentItem() );
        pagerRunnable.setCount(newsPagerAdapter.getCount());
        vpNews.postDelayed( pagerRunnable , PagerAnimationDuration );
        vpNews.setTag(0);
    }

    private void setCheckBluetoothOn( boolean checkBluetoothOn ){
        SharedPreferences pref = this.getSharedPreferences("fitmateservice", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(LoginPresenter.CHECK_BLUETOOTHON_KEY, checkBluetoothOn);
        editor.commit();
    }

    private Boolean getCheckBluetoothOn(  ){
        SharedPreferences pref = this.getSharedPreferences("fitmateservice", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        return pref.getBoolean(LoginPresenter.CHECK_BLUETOOTHON_KEY, false);
    }

    private Boolean getRequestBluetoothOn(  ){
        SharedPreferences pref = this.getSharedPreferences("fitmateservice", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        return pref.getBoolean(LoginPresenter.REQUEST_BLUETOOTHON_KEY, false);
    }

    private void setRequestBluetoothOn( boolean requestBluetoothOn ){
        SharedPreferences pref = this.getSharedPreferences("fitmateservice", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(LoginPresenter.REQUEST_BLUETOOTHON_KEY, requestBluetoothOn);
        editor.commit();
    }

    private Boolean getInitialViewComplete(  ){
        SharedPreferences pref = this.getSharedPreferences("fitmateservice", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        return pref.getBoolean(LoginPresenter.INIT_VIEW_COMPLETE_KEY, false);
    }

    private void setInitialViewComplete( boolean initViewComplete ){
        SharedPreferences pref = this.getSharedPreferences("fitmateservice", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(LoginPresenter.INIT_VIEW_COMPLETE_KEY, initViewComplete);
        editor.commit();
    }

    private boolean getServerUpdate( ){
        SharedPreferences pref = this.getSharedPreferences("fitmateservice", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        return pref.getBoolean( DayActivity.DAY_ACTIVITY_SERVER_UPDATE_KEY, false);
    }

    private void setServerUpdate( boolean serverUpdate) {
        SharedPreferences pref = this.getSharedPreferences("fitmateservice", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(DayActivity.DAY_ACTIVITY_SERVER_UPDATE_KEY, serverUpdate);
        editor.commit();
    }

    public void dismissSyncView()
    {
        imgSync.clearAnimation();
        rlSync.setVisibility(View.GONE);
        showBatteryView();
    }

    private void showBatteryView(){
        rlBattery.setVisibility(View.VISIBLE);
    }

    public void showSyncView() {
        //싱크뷰 돌리는 애니메이션.
        rlSync.setVisibility(View.VISIBLE);
        imgSync.startAnimation(AnimationUtils.loadAnimation(this, R.anim.rotate_sync));
        tvSyncPercent.setText("0");
    }

    public void calculateCompleted(final String weekUpdateList)
    {
        //앱에 분석된 활동량결과를 서버에 업로드한다.
        if( this.getServerUpdate() )
        {
            if(Utils.isOnline(this) ) {

                Log.i(TAG, "앱 서버 동기화");
                List<DayActivity> uploadDayActivityList = mDBManager.getNotUploadDayActivity();
                List<WeekActivity> uploadWeekActivityList = mDBManager.getNotUploadWeekActivity();

                setServerUpdate(false);
                mActivityDataModel.uploadActivityList(uploadDayActivityList, uploadWeekActivityList);
            }
        }

        if(weekUpdateList == null){
            return;
        }

        isRequestCalculate = false;

        NewHomeWeekPagerAdapter updateWeekPager = null;
        if( vpWeekBar.getVisibility() == View.VISIBLE ){
            updateWeekPager = mWeekBarPagerAdapter;
        }else if( vpWeekTable.getVisibility() == View.VISIBLE ){
            updateWeekPager = mWeekTablePagerAdapter;
        }

        if( updateWeekPager == null ){
            return;
        }

        //업데이트 된 주목록에 현재 날짜가 포함 되는지 포함된다면 일 데이터를 업데이트한다.
        //업데이트 된 주 목록에 현재 주간 그래프 날짜가 포함된다면 주 데이터를 업데이트한다.
        String currentWeekStartDate = newHomeUtils.getWeekStartDate( mDayCalendar );
        CategoryType categoryType = mCategoryViewOrder[selectedCategoryIndex];
        String[] weekStringList =  updateWeekPager.getWeekStringList();

        if( weekUpdateList != null )
        {
            try {
                JSONArray jsonWeekArray = new JSONArray(weekUpdateList);
                for( int weekUpdateIndex = 0 ; weekUpdateIndex < jsonWeekArray.length() ; weekUpdateIndex++ ){

                    JSONObject jsonWeekObject = jsonWeekArray.getJSONObject( weekUpdateIndex );
                    String weekUpdateDate = jsonWeekObject.getString("weekdate");

                    //서비스로부터 받은 주시작 날짜와 현재 보여지고 있는 일의 주시작 날짜가 같다면 일 그래프를 업데이트한다.
                    if( weekUpdateDate.equals(currentWeekStartDate) )
                    {
                        displayDayData(false);
                    }

                                JSONArray jsonDayArray = jsonWeekObject.getJSONArray("daylist");

                    int[] childIndexList = new int[ jsonDayArray.length() ];
                    double[] valueList = new double[jsonDayArray.length()];
                    WeakHashMap<String , DayActivity> dayMap = new WeakHashMap<String , DayActivity>();
                                for( int daysIndex = 0 ; daysIndex < jsonDayArray.length() ; daysIndex++ )
                                {
                                    JSONObject jsonDayObject = jsonDayArray.getJSONObject( daysIndex );
                                    String dayDate = jsonDayObject.getString("daydate");
                                    int dayIndex = jsonDayObject.getInt("dayindex");

                        childIndexList[daysIndex] = dayIndex - 1;

                                    DayActivity dayActivity = dayMap.get(dayDate);
                                    if( dayActivity == null ) {
                                        dayActivity = mDBManager.getDayActivity( dayDate );
                                        dayMap.put( dayDate , dayActivity );
                                    }

                        valueList[daysIndex]  = getDayValue( categoryType , dayActivity );
                                }

                    int position = updateWeekPager.getPosition( weekUpdateDate );
                    if( position > -1 ) {
                        mWeekPagerActivityInfoList[ position ].updateData(childIndexList, dayMap.values().toArray(new DayActivity[dayMap.values().size()]));
                            }

                    //업데이트 된 주 목록에 현재 주간 그래프 날짜가 포함된다면 주 데이터를 업데이트한다.
                    if( weekStringList != null )
                    {
                        for (int weekIndex = 0; weekIndex < weekStringList.length; weekIndex++)
                        {

                            if (weekStringList[weekIndex] != null) {
                                String weekStartDate = weekStringList[weekIndex];
                                if (weekUpdateDate.equals(weekStartDate)) {
                                    NewHomeWeekView newHomeWeekView = (NewHomeWeekView) updateWeekPager.getView(weekStartDate);
                                    newHomeWeekView.updateView(childIndexList, valueList);
                                    weekStringList[weekIndex] = null;
                                    break;
                        }
                    }
                }

                    }
                 }

            }catch( JSONException e){

            }
        }

        android.util.Log.i(TAG, "Request Complete");
    }

    @Override
    public void OnNetworkAvailable() {
        getNewsList();

        }

        @Override
    public void onWeekMove( long dayMilliseconds , int position , WeekPagerAdapter.WeekPagerType weekPagerType ) {
        //주간 그래프에 따라 일 날짜를 변경시킨다.
        mDayCalendar.setTimeInMillis( dayMilliseconds );

        //일 그래프를 다시 그린다.
        setDateText();
        displayDayData(false);

    }

    private double getDayValue( CategoryType categoryType , DayActivity dayActivity )
    {
        double value = 0;
        switch( categoryType ){
            case EXERCISE_TIME:
                value = dayActivity.getStrengthHigh() + dayActivity.getStrengthMedium();
                break;
            case CALORIES:
                value = dayActivity.getCalorieByActivity();
                break;
            case DAILY_ACHIEVE:
                value = dayActivity.getAchieveOfTarget();
                break;
            case WEEKLY_ACHIEVE:
                value = dayActivity.getTodayScore();
                break;
            case FAT:
                double fat = dayActivity.getFat();
                if( fat >= 0 ){
                    value = dayActivity.getFat();
                }else{
                    value = this.calculateFatCarboDistance( dayActivity ).fat;
                }
                break;
            case CARBOHYDRATE:
                double carbo = dayActivity.getCarbohydrate();
                if( carbo >= 0 ){
                    value = dayActivity.getCarbohydrate();
                }else{
                    value = this.calculateFatCarboDistance( dayActivity ).carbohydrate;
                }
                break;
            case DISTANCE:
                double distance = dayActivity.getDistance();
                if( distance >= 0 ) {
                    value = dayActivity.getDistance();
                }else{
                    value = this.calculateFatCarboDistance( dayActivity ).distance;
                }

                break;
        }

        return value;
    }

    private FatCarboDistance calculateFatCarboDistance( DayActivity dayActivity )
    {
        UserInfoForAnalyzer userInfoForAnalyzer = com.fitdotlife.fitmate_lib.service.util.Utils.getUserInfoForAnalyzer( mUserInfo );
        ContinuousCheckPolicy policy = ContinuousCheckPolicy.Loosely_SumOverXExercise;
        com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseProgram appliedProgram = mDBManager.getAppliedExerciseProgram();
        ExerciseAnalyzer analyzer = new ExerciseAnalyzer(appliedProgram, userInfoForAnalyzer, WearingLocation.WAIST , dayActivity.getDataSaveInterval());

        float fat = 0;
        float carbon = 0;
        double distance = 0;

        float[] metArray= dayActivity.getMetArray();
        List<Float> metList= new ArrayList<>();
        for(float f:metArray){
            metList.add(f);
        }
        FatAndCarbonhydrateConsumtion fatBurn = analyzer.getFatandCarbonhydrate(metList, dayActivity.getDataSaveInterval());
        fat =fatBurn.getFatBurned();
        carbon =fatBurn.getCarbonhydrateBurned();
        distance = ExerciseAnalyzer.getDistanceFromMETArray_over2MET(metList, dayActivity.getDataSaveInterval());

        mDBManager.setFatAndCarbonAndDistance(dayActivity.getActivityDate(), fat, carbon, distance);

        FatCarboDistance fatCarboDistance = new FatCarboDistance();
        fatCarboDistance.fat = fat;
        fatCarboDistance.carbohydrate = carbon;
        fatCarboDistance.distance = distance;
        return fatCarboDistance;
    }


    private class CategoryClickListener implements View.OnClickListener
    {
        @Override
        public void onClick(View view)
        {
            vpDay.setCurrentItem( (int) view.getTag() );
        }
    }

    private class WeekCellClickListener implements View.OnClickListener
    {
        @Override
        public void onClick(View view) {

            long selectedDayTimeMillis = (long) view.getTag( R.string.newhome_weekview_time_key );
            int index = (int) view.getTag(R.string.newhome_weekview_cellindex_key);
            mDayCalendar.setTimeInMillis(selectedDayTimeMillis);

            setDateText();
            displayDayData(false);

            if( vpWeekBar.getVisibility() == View.VISIBLE ) {
                mWeekBarPagerAdapter.setDayTimeMilliseconds(selectedDayTimeMillis);
                mWeekBarPagerAdapter.setSelectedDayIndex(index);
            }else if(vpWeekTable.getVisibility() == View.VISIBLE) {
                mWeekTablePagerAdapter.setDayTimeMilliseconds( selectedDayTimeMillis );
                mWeekTablePagerAdapter.setSelectedDayIndex(index);
            }
        }
    }


    private void changeDay()
    {
        showProgressView();
        setDateText();
        getWeekActivity();
    }

    private void setWeekInfoDayActivityList(WeekActivityWithDayActivityList weekActivityWithDayActivityList)
    {

        //데이터를 저장한다.
        WeekActivity weekActivity = new WeekActivity();
        weekActivity.setActivityDate( weekActivityWithDayActivityList.getWeekStartDate() );
        weekActivity.setScore(weekActivityWithDayActivityList.getScore());
        weekActivity.setAverageCalorie(weekActivityWithDayActivityList.getAveCalorie());
        weekActivity.setAverageMET(weekActivityWithDayActivityList.getAveMET());
        weekActivity.setAverageStrengthHigh(weekActivityWithDayActivityList.getAveStrengthHigh());
        weekActivity.setAverageStrengthMedium(weekActivityWithDayActivityList.getAveStrengthMedium());
        weekActivity.setAverageStrengthLow(weekActivityWithDayActivityList.getAveStrengthLow());
        weekActivity.setAverageStrengthUnderLow(weekActivityWithDayActivityList.getAveStrengthUnderLow());
        weekActivity.setExerciseProgramID(weekActivityWithDayActivityList.getExerciseProgramID());

        mDBManager.setWeekActivity(weekActivity, FitmateDBManager.UPLOADED);

        List<DayActivity_Rest> daysList = weekActivityWithDayActivityList.getDaysList();
        if( daysList != null ) {
            List<DayActivity> dayActivityList = new ArrayList<DayActivity>();
            List<Integer> CalorieList = new ArrayList<Integer>();
            for (int index = 0; index < daysList.size(); index++) {
                DayActivity_Rest dayActivity_rest = daysList.get(index);
                DayActivity dayActivity = new DayActivity();
                dayActivity.setStartTime(new TimeInfo(dayActivity_rest.getDate()));
                dayActivity.setCalorieByActivity(dayActivity_rest.getCalorieByActivity());
                CalorieList.add(dayActivity_rest.getCalorieByActivity());
                dayActivity.setAchieveOfTarget( dayActivity_rest.getAchieveOfTarget() );
                dayActivity.setDataSaveInterval(dayActivity_rest.getDataSavingInterval());
                dayActivity.setAverageMET(dayActivity_rest.getAverageMet());
                dayActivity.setHmlStrengthTime(new int[]{dayActivity_rest.getStrengthUnderLow(), dayActivity_rest.getStrengthLow(), dayActivity_rest.getStrengthMedium(), dayActivity_rest.getStrengthHigh()});

                byte[] byteMetArray = Base64.decode(dayActivity_rest.getMetArray(), Base64.NO_WRAP);
                float[] metArray = null;
                if (byteMetArray != null) {
                    metArray = DayActivity.convertByteArrayToFloatArray(byteMetArray);
                }

                dayActivity.setMetArray(metArray);

                float fat = 0;
                float carboHydrate = 0;
                double distance = 0;

                if(dayActivity_rest.getFat() == 0)
                {
                    FatCarboDistance fatCarboDistance = this.calculateFatCarboDistance(dayActivity);
                    fat = fatCarboDistance.fat;
                    carboHydrate = fatCarboDistance.carbohydrate;
                    distance = fatCarboDistance.distance;
                }else{
                    fat = dayActivity_rest.getFat();
                    carboHydrate = dayActivity.getCarbohydrate();
                    distance = dayActivity_rest.getDistance();
                }

                dayActivity.setFat( fat );
                dayActivity.setCarbohydrate( carboHydrate );
                dayActivity.setDistance( distance );

                dayActivityList.add(dayActivity);
            }

            ExerciseProgram exerciseProgram = this.mDBManager.getUserExerciProgram(weekActivity.getExerciseProgramID());
            UserInfo userInfo = mDBManager.getUserInfo();
            UserInfoForAnalyzer userInfoForAnalyzer = com.fitdotlife.fitmate_lib.service.util.Utils.getUserInfoForAnalyzer(userInfo);

            com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseProgram program = Utils.ConvertExerciseProgram(exerciseProgram);
            ExerciseAnalyzer analyzer = new ExerciseAnalyzer(program, userInfoForAnalyzer, WearingLocation.WRIST, 10);
            StrengthInputType strengthType = program.getStrengthInputType();

            List<ScoreClass> scoreClassList = null;
            if (strengthType.equals(StrengthInputType.CALORIE) || strengthType.equals(StrengthInputType.VS_BMR)) {
                scoreClassList = Utils.CalculateDayScores_Calorie(CalorieList, analyzer);
            } else if (strengthType.equals(StrengthInputType.CALORIE_SUM)) {
                scoreClassList = Utils.CalculateDayScores_CalorieSUM(CalorieList, analyzer);
            } else {
                scoreClassList = Utils.CalculateDayScores_Strength(dayActivityList, analyzer);
            }

            for (int i = 0; i < dayActivityList.size(); i++)
            {
                DayActivity act = dayActivityList.get(i);
                ScoreClass score = scoreClassList.get(i);
                act.setPredayScore(score.preDayAchieve);
                act.setTodayScore(score.todayGetPoint);
                act.setExtraScore(score.todayPossibleMorePoint);
                act.setAchieveMax(score.isAchieveMax);
                act.setPossibleMaxScore(score.possibleMaxScore);
                act.setPossibleTimes(score.possibleTimes);
                act.setPossibleValue(score.possibleValue);
                android.util.Log.d("recalcul_point", String.format("pre:%d, today:%d, extra:%d ", score.preDayAchieve, score.todayGetPoint, score.todayPossibleMorePoint));
                this.mDBManager.setDayActivity(act);
            }
        }
    }

    private class NewsClickListener implements View.OnClickListener{

        @Override
        public void onClick(View view)
        {
            Intent intent = new Intent( NewHomeActivity.this , NewsActivity_.class );
            intent.putExtra("newsid" , (int)view.getTag() );
            startActivity(intent);

            ActivityManager.getInstance().finishAllActivity();

            if( vpNews.getTag() == 0 ) {
                vpNews.removeCallbacks(pagerRunnable);
                vpNews.setTag(1);
            }
        }
    }

    private class ProgramClickListener implements NewHomeProgramClickListener{

        @Override
        public void programClick(int exerprogramID) {

            String weekStartDate =NewHomeUtils.getWeekStartDate( mDayCalendar );
            WeekActivity weekActivity = mDBManager.getWeekActivity( weekStartDate );
            newHomeUtils.showProgramDialog(weekActivity.getExerciseProgramID());

        }
    }

    private void showProgressView(){
        rlWeekProgress.setVisibility(View.VISIBLE);
    }

    private void dismissProgressView(){
        rlWeekProgress.setVisibility(View.INVISIBLE);
    }

    private Boolean getHomeNews(  ){
        SharedPreferences pref = this.getSharedPreferences("fitmate", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        return pref.getBoolean(NewHomeActivity.HOME_NEWS_KEY, false);
    }

    private void setHomeNews( boolean isNews ){
        SharedPreferences pref = this.getSharedPreferences("fitmate", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean( NewHomeActivity.HOME_NEWS_KEY, isNews);
        editor.commit();
    }

    private class FatCarboDistance{
        public float fat;
        public float carbohydrate;
        public double distance;
    }

    private void startTimer(){
        this.mTimerHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                setInitialViewComplete(true);
                dismissSyncView();
            }
        }, 30000);
    }

    @Click(R.id.rl_activity_newhome_actionbar_battery)
    void showBatteryPopup()
    {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View batteryView = inflater.inflate(R.layout.dialog_battery, null);
        BatteryPopupCircleView bpcv = (BatteryPopupCircleView) batteryView.findViewById(R.id.bpcv_dialog_battery_popup);
        TextView tvBatteryValue = (TextView) batteryView.findViewById(R.id.tv_dialog_battery_value);
        TextView tvReccontedTime = (TextView) batteryView.findViewById(R.id.tv_dialog_battery_reconnet_time);

        long connectedTime = mDBManager.getLastConnectedTime( mUserInfo.getEmail() );
        if( Calendar.getInstance().getTimeInMillis()   - connectedTime > 86400000 ){

            //하루이상 연결되지 않았음.
            tvBatteryValue.setText( this.getResources().getString(R.string.newhome_battery_popup_not_connected) );
            tvBatteryValue.setTextSize(TypedValue.COMPLEX_UNIT_SP , 15);
            tvReccontedTime.setText(this.getResources().getString(R.string.newhome_battery_popup_not_connected));
            bpcv.setBatteryValue(0);

        }else{

            int batteryRatio = mDBManager.getLastBatteryRatio( mUserInfo.getEmail() );
            this.setBattery( batteryRatio );
            tvBatteryValue.setText( batteryRatio + "%" );
            tvBatteryValue.setTextSize(TypedValue.COMPLEX_UNIT_SP , 29);
            SimpleDateFormat format = new SimpleDateFormat(this.getResources().getString( R.string.newhome_battery_popup_reconnected_date_format ));
            Calendar connectedCalendar = Calendar.getInstance();
            connectedCalendar.setTimeInMillis(connectedTime );
            tvReccontedTime.setText(format.format(connectedCalendar.getTime()) );
            bpcv.setBatteryValue( batteryRatio );
        }

        batteryPopup = new PopupWindow( batteryView , WindowManager.LayoutParams.MATCH_PARENT , WindowManager.LayoutParams.MATCH_PARENT , true);
        WindowManager windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);

//        Point p = new Point();
//        windowManager.getDefaultDisplay().getSize( p );
//        int screenWidth = p.x;
//        int screenHeight = p.y;
//        int x = screenWidth / 2 - batteryView.getMeasuredWidth() / 2;
//        int y = screenHeight / 2 - batteryView.getMeasuredHeight() / 2;

        batteryPopup.setOutsideTouchable(true);
        batteryPopup.setTouchable(true);
        batteryPopup.setBackgroundDrawable(new BitmapDrawable()) ;
        batteryPopup.setTouchInterceptor(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                batteryPopup.dismiss();
                return true;
            }
        });
        batteryPopup.setAnimationStyle(-1);
        //batteryPopup.showAtLocation(batteryView, Gravity.TOP, x, y);
        batteryPopup.showAtLocation(  batteryView  , Gravity.CENTER , 0, 0);

    }

    private class PagerRunnable implements  Runnable
    {

        int mPosition = 0;
        int mCount = 0;

        @Override
        public void run() {

            mPosition++;

            if( mPosition < mCount )
            {
                vpNews.setCurrentItem( mPosition );
                vpNews.postDelayed(pagerRunnable, PagerAnimationDuration);
            }else{

                vpNews.setCurrentItem( 0 );

            }
        }

        public void setPosition( int position )
        {
            this.mPosition = position;
        }

        public void setCount( int count )
        {
            this.mCount = count;
        }
    }

    private class NewHomeWeekPagerAdapter extends WeekPagerAdapter
    {

        private String[] mWeekStringArray = null;
        private HashMap<String , View> mViewMap = new HashMap< String , View >();

        public NewHomeWeekPagerAdapter(Context context, int weekPagerLength, CategoryType categoryType , View.OnClickListener weekCellClickListener , WeekMoveListener weekMoveListener , long dayMilliseconds , WeekPagerType weekPagerType , WeekPagerActivityInfo[] weekPagerActivityInfoList) {

            super(context, weekPagerLength, categoryType, weekCellClickListener, weekMoveListener, dayMilliseconds, weekPagerType, weekPagerActivityInfoList);
            mWeekStringArray = new String[ mWeekPagerLength ];

        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            View view = null;

            WeekPagerActivityInfo weekActivityInfo = null;
            if (mWeekPagerActivityInfoList[position] == null) {

                weekActivityInfo = getWeekPagerActivityInfo(position);
                mWeekPagerActivityInfoList[position] = weekActivityInfo;

            } else {

                weekActivityInfo = mWeekPagerActivityInfoList[position];
            }

            view = getWeekGraphView(weekActivityInfo , position);
            addItem(weekActivityInfo.getWeekStartDate(), view);

            container.addView(view);

            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object){

            removeItem(position, object);
            container.removeView((View) object);

        }


        private WeekPagerActivityInfo getWeekPagerActivityInfo(int position) {

            int weekIndex = mPagerLength - (position + 1);

            Calendar weekCalendar = Calendar.getInstance();
            weekCalendar.setTimeInMillis( mWeekCalendar.getTimeInMillis() );
            weekCalendar.add(Calendar.DATE, -(weekIndex * 7));
            String weekStartDate = NewHomeUtils.getDateString(weekCalendar);

            mWeekStringArray[position] = weekStartDate;
            WeekActivity weekActivity = mDBManager.getWeekActivity(weekStartDate);

            if( weekActivity == null )
            {
                getWeekActivity( weekStartDate , this );

                while(true){
                    if(mDownloadData){
                        mDownloadData = false;
                        break;
                    }

                    try {
                        Thread.sleep( 10 );
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                weekActivity = mDBManager.getWeekActivity( weekStartDate );
            }

            String weekRange = "";
            double[] exerciseTimeValues = new double[]{ 0 , 0 , 0 , 0 , 0 , 0 , 0 };
            double[] calorieValues = new double[]{ 0 , 0 , 0 , 0 , 0 , 0 , 0 };
            double[] dailyAchieveValues = new double[]{ 0 , 0 , 0 , 0 , 0 , 0 , 0 };
            double[] weeklyAchieveValues = new double[]{ 0 , 0 , 0 , 0 , 0 , 0 , 0 };
            double[] fatValues = new double[]{ 0 , 0 , 0 , 0 , 0 , 0 , 0 };
            double[] carboHydrateValues = new double[]{ 0 , 0 , 0 , 0 , 0 , 0 , 0 };
            double[] distanceValues = new double[]{ 0 , 0 , 0 , 0 , 0 , 0 , 0 };

            String[] dayStrings = new String[7];
            long[] timeMillis = new long[7];
            int selectedDayIndex = -1;
            int preMonth = -1;

            for (int i = 0; i < dailyAchieveValues.length ; i++)
            {
                String activityDate = newHomeUtils.getDateString(weekCalendar);
                DayActivity dayActivity = mDBManager.getDayActivity(activityDate);

                if (dayActivity != null)
                {
                    exerciseTimeValues[i] = dayActivity.getStrengthHigh() + dayActivity.getStrengthMedium();
                    calorieValues[i] = dayActivity.getCalorieByActivity();
                    dailyAchieveValues[i] = dayActivity.getAchieveOfTarget();

                    weeklyAchieveValues[i] = dayActivity.getTodayScore();
                    double fat = dayActivity.getFat();
                    if( fat >= 0 ){
                        fatValues[i] = dayActivity.getFat();
                    }else{
                        fatValues[i] = calculateFatCarboDistance(dayActivity).fat;
                    }


                    double carbo = dayActivity.getCarbohydrate();
                    if( carbo >= 0 ){
                        carboHydrateValues[i] = dayActivity.getCarbohydrate();
                    }else{
                        carboHydrateValues[i] = calculateFatCarboDistance(dayActivity).carbohydrate;
                    }

                    double distance = dayActivity.getDistance();
                    if( distance >= 0 ) {
                        distanceValues[i] = dayActivity.getDistance();
                    }else{
                        distanceValues[i] = calculateFatCarboDistance(dayActivity).distance;
                    }

                } else {

                    boolean todayAfter = DateUtils.isAfterDay(weekCalendar.getTime(), new Date());
                    if (todayAfter) { //오늘이후 시간이라면 데이터를 없앤다.
                        exerciseTimeValues[i] = TODAY_AFTER_NODATA;
                        calorieValues[i] = TODAY_AFTER_NODATA;
                        dailyAchieveValues[i] = TODAY_AFTER_NODATA;
                        weeklyAchieveValues[i] = TODAY_AFTER_NODATA;
                        fatValues[i] = TODAY_AFTER_NODATA;
                        carboHydrateValues[i] = TODAY_AFTER_NODATA;
                        distanceValues[i] = TODAY_AFTER_NODATA;
                    } else {            //오늘이전 시간이라면 회색으로 보이도록 한다.
                        exerciseTimeValues[i] = TODAY_BEFORE_NODATA;
                        calorieValues[i] = TODAY_BEFORE_NODATA;
                        dailyAchieveValues[i] = TODAY_BEFORE_NODATA;
                        weeklyAchieveValues[i] = TODAY_BEFORE_NODATA;
                        fatValues[i] = TODAY_BEFORE_NODATA;
                        carboHydrateValues[i] = TODAY_BEFORE_NODATA;
                        distanceValues[i] = TODAY_BEFORE_NODATA;
                    }
                }

                timeMillis[i] = weekCalendar.getTimeInMillis();

                int currentMonth = weekCalendar.get(Calendar.MONTH);
                if( preMonth != currentMonth ) {
                    dayStrings[i] = DateUtils.getDateStringForPattern(weekCalendar.getTime(), getResources().getString(R.string.newhome_week_bar_date_format));
                    preMonth = currentMonth;
                }else{
                    dayStrings[i] = DateUtils.getDateStringForPattern(weekCalendar.getTime(), getResources().getString(R.string.newhome_week_bar_date_simple_format));
                }

                if( i == 0 ){
                    weekRange += DateUtils.getDateStringForPattern( weekCalendar.getTime() , getResources().getString( R.string.newhome_week_table_week_range_format ) );
                }

                if( i == dailyAchieveValues.length -1 ){
                    weekRange += " - " + DateUtils.getDateStringForPattern( weekCalendar.getTime() , getResources().getString( R.string.newhome_week_table_week_range_format ) );
                }

                weekCalendar.add(Calendar.DATE, 1);
            }

            float strengthFrom = 0;
            float strengthTo = 0;

            ExerciseProgram program = null;
            if ( weekActivity != null ){
                program = mDBManager.getUserExerciProgram( weekActivity.getExerciseProgramID() );
            }else{
                program = mDBManager.getAppliedProgram();
            }

            StrengthInputType strengthType = StrengthInputType.getStrengthType( program.getCategory() );
            if( strengthType.equals( StrengthInputType.CALORIE ) || strengthType.equals( StrengthInputType.CALORIE_SUM )|| strengthType.equals(StrengthInputType.VS_BMR))
            {

                UserInfoForAnalyzer userInfoForAnalyzer = com.fitdotlife.fitmate_lib.service.util.Utils.getUserInfoForAnalyzer(mUserInfo);

                strengthFrom= (int) program.getStrengthFrom();
                strengthTo= (int) program.getStrengthFrom();
                int bmr = userInfoForAnalyzer.getBMR();

                if(strengthType.equals(StrengthInputType.VS_BMR)){
                    strengthFrom=(int)Math.round(strengthFrom /100.0 * bmr);
                    strengthTo=(int)Math.round(strengthTo /100.0 * bmr);
                }

            }else{
                strengthFrom = program.getMinuteFrom();
                strengthTo = program.getMinuteTo();
            }

            WeekPagerActivityInfo weekActivityInfo = new WeekPagerActivityInfo( weekStartDate , weekRange , dayStrings , timeMillis , preMonth , strengthFrom , strengthTo
                    , exerciseTimeValues , calorieValues , dailyAchieveValues , weeklyAchieveValues , fatValues , carboHydrateValues , distanceValues);

            return weekActivityInfo;
        }


        private void addItem( String weekStartDate , View view ) {
            mViewMap.put(weekStartDate, view);
        }

        @Override
        protected void getWeekActivityInfo(int position) {

            int weekIndex = mPagerLength - (position + 1);

            Calendar weekCalendar = Calendar.getInstance();
            weekCalendar.setTimeInMillis(mWeekCalendar.getTimeInMillis());
            weekCalendar.add(Calendar.DATE, -(weekIndex * 7));
            String weekStartDate = newHomeUtils.getDateString(weekCalendar);

            if( mWeekPagerActivityInfoList[position] == null ){

                mWeekPagerActivityInfoList[position] = getWeekPagerActivityInfo( position );

            }

        }

        private void removeItem(int position, Object object) {
            mViewMap.remove(mWeekStringArray[position]);
            mWeekStringArray[ position ] = null;
        }

        public String[] getWeekStringList()
        {
            String[] weekStringList = null;

            if( (mCurrentPosition - 1) >= 0 ){

                weekStringList = new String[2];
                weekStringList[0] = mWeekStringArray[ mCurrentPosition - 1 ];
                weekStringList[1] = mWeekStringArray[ mCurrentPosition];

            }else if( ( mCurrentPosition + 1 ) < mPagerLength ) {

                weekStringList = new String[2];
                weekStringList[0] = mWeekStringArray[ mCurrentPosition];
                weekStringList[1] = mWeekStringArray[ mCurrentPosition + 1];

            }else{

                weekStringList = new String[3];
                weekStringList[0] = mWeekStringArray[ mCurrentPosition - 1];
                weekStringList[1] = mWeekStringArray[ mCurrentPosition];
                weekStringList[2] = mWeekStringArray[ mCurrentPosition + 1];

            }

            return weekStringList;
        }

        public int getPosition( String weekStartDate ){

            int position = -1;
            if( mWeekStringArray != null ){

                for( int i = mWeekStringArray.length ; i > 0 ; i--  ){

                    if( weekStartDate.equals( mWeekStringArray[i - 1] ) ){
                        position = i-1;
                        break;
                    }
                }

            }else{
                position = -1;
            }

            return position;

        }

        public View getView( String weekDate ){
            return  mViewMap.get(weekDate);
        }


        @Override
        public void OnWeekActivityReceived(FriendWeekActivity friendWeekActivity) {

        }

        @Override
        public void OnWeekActivityReceived(FriendWeekActivity[] friendWeekActivityList) {

        }
    }

    @Click(R.id.tv_activity_newhome_actionbar_categoryedit_cancel)
    void categoryEditCancelClick()
    {
        cancelCategoryEdit();
    }

    private void cancelCategoryEdit(){

        rlActionBar.setVisibility( View.VISIBLE );
        hsvCategory.setVisibility( View.VISIBLE );

        rlCategoryEditActionBar.setVisibility( View.GONE );
        llCategoryEditPopup.setVisibility(View.GONE);
        hsvCategoryEdit.setVisibility( View.GONE );


        llCategoryEdit.removeAllViews();
//        for( int categoryIndex = 0 ; categoryIndex < CategoryType.values().length ; categoryIndex++ ){
//            CategoryType categoryType = CategoryType.values()[ categoryIndex ];
//            NewHomeCategoryView categoryView = categoryViews[ categoryType.ordinal() ];
//            categoryView.setVisible(false);
//        }
//
//        for( int viewIndex = 0 ; viewIndex < mCategoryViewOrder.length ; viewIndex++ ){
//            CategoryType categoryType = mCategoryViewOrder[ viewIndex ];
//            NewHomeCategoryView categoryView = categoryViews[ categoryType.ordinal() ];
//            categoryView.setVisible( true );
//            llCategory.addView( categoryView );
//        }

        llCategory.addView( mCategoryEditCategoryView );
        Animation slide_down = AnimationUtils.loadAnimation(NewHomeActivity.this, R.anim.slide_down);
        llCategoryEditPopup.startAnimation(slide_down);

    }

    @Click(R.id.tv_activity_newhome_actionbar_categoryedit_save)
    void categoryEditSaveClick()
    {
        //저장을 누를 때 최소한 1개는 선택되어 있어야 함.
        if( llCategoryEdit.getChildCount() < 1 ) {

            AlertDialog.Builder builder = new AlertDialog.Builder( NewHomeActivity.this );

            builder.setMessage(getString( R.string.tab_setting_select_one) )
                    .setCancelable(true)        // 뒤로 버튼 클릭시 취소 가능 설정
                    .setPositiveButton(R.string.common_ok, new DialogInterface.OnClickListener() {
                        // 확인 버튼 클릭시 설정
                        public void onClick(DialogInterface dialog, int whichButton) {

                            dialog.cancel();
                        }
                    });
            AlertDialog dialog = builder.create();    // 알림창 객체 생성
            dialog.show();    // 알림창 띄우기

            return;
        }


        rlActionBar.setVisibility( View.VISIBLE );
        hsvCategory.setVisibility( View.VISIBLE );

        rlCategoryEditActionBar.setVisibility(View.GONE);
        llCategoryEditPopup.setVisibility(View.GONE);
        hsvCategoryEdit.setVisibility( View.GONE );

        //이전에 선택된 카테고리 타입을 가져온다.
        CategoryType preSelectCategoryType = mCategoryViewOrder[ selectedCategoryIndex ];

        mCategoryViewOrder = new CategoryType[ llCategoryEdit.getChildCount() ];
        View[] dayGraphs = new View[ llCategoryEdit.getChildCount() ];
        StringBuilder strViewOrder = new StringBuilder();

        //selectedCategoryIndex 초기화
        selectedCategoryIndex = 0;

        llCategory.removeAllViews();
        for( int viewIndex = 0 ; viewIndex < llCategoryEdit.getChildCount() ;viewIndex++ )
        {
            NewHomeCategoryView categoryEditView = (NewHomeCategoryView) llCategoryEdit.getChildAt( viewIndex );
            CategoryType categoryType = categoryEditView.getCategoryType();

            NewHomeCategoryView categoryView = categoryViews[ categoryType.ordinal() ];
            categoryView.setViewIndex(viewIndex);

            llCategory.addView( categoryView );

            //이전에 선택되었던 카테고리가 있다면 그 ViewIndex를 selectedCategoryIndex로 설정한다.
            if( preSelectCategoryType.equals( categoryType ) )
            {
                selectedCategoryIndex = viewIndex;
            }

            //ViewOrder 생성
            mCategoryViewOrder[ viewIndex ] = categoryType;

            //DayAdater용 View 배열 만듬.
            View dayView = dayViews[ categoryType.ordinal() ];
            dayView.setTag( viewIndex );
            dayGraphs[viewIndex] = dayView;

            //액티비티 초기에 읽어들일 ViewOrder 문자열을 만든다.
            strViewOrder.append(categoryType.ordinal() ).append(",");
        }

        llCategoryEdit.removeAllViews();

        //카테고리를 선택한다.
        newHomeUtils.selectCategoryView((NewHomeCategoryView) llCategory.getChildAt(selectedCategoryIndex));

        //일 Adapter를 다시 설정한다.
        mDayPagerAdapter = new DayPagerAdapter(this , dayGraphs);
        vpDay.setAdapter(mDayPagerAdapter);
        vpDay.setCurrentItem(selectedCategoryIndex);

        //카테고리 수정버튼을 추가한다.
        llCategory.addView(mCategoryEditCategoryView);

        changeCategoryViewGravity( llCategory , hsvCategory );

        //현재 선택된 카테고리 타입을 가져온다.
        CategoryType categoryType = mCategoryViewOrder[selectedCategoryIndex];

        if( !categoryType.equals( preSelectCategoryType ) )
        {
            if( categoryType.equals( CategoryType.WEEKLY_ACHIEVE ) )
            {

                vpWeekBar.setVisibility( View.INVISIBLE );
                vpWeekTable.setVisibility(View.VISIBLE);
                mWeekTablePagerAdapter.setDayTimeMilliseconds( mDayCalendar.getTimeInMillis() );
                mWeekTablePagerAdapter.setSelectedDayIndex( mWeekBarPagerAdapter.getSelectedDayIndex() );

                if( vpWeekTable.getCurrentItem() != vpWeekBar.getCurrentItem() )
                {

                    mWeekTablePagerAdapter.setMoveWeek(false);
                    vpWeekTable.setCurrentItem( vpWeekBar.getCurrentItem() );

                }

            }else if( preSelectCategoryType.equals( CategoryType.WEEKLY_ACHIEVE ) ){

                vpWeekBar.setVisibility(View.VISIBLE);
                vpWeekTable.setVisibility(View.INVISIBLE);
                mWeekBarPagerAdapter.setDayTimeMilliseconds(mDayCalendar.getTimeInMillis());
                mWeekBarPagerAdapter.setSelectedDayIndex( mWeekTablePagerAdapter.getSelectedDayIndex() );
                mWeekBarPagerAdapter.changeCategoryType(categoryType);
                if( vpWeekTable.getCurrentItem() != vpWeekBar.getCurrentItem() ){

                    mWeekBarPagerAdapter.setMoveWeek(false);
                    vpWeekBar.setCurrentItem( vpWeekTable.getCurrentItem() );

                }

            }else{

                mWeekBarPagerAdapter.changeCategoryType(categoryType);

            }
        }

        //슬라이드 업 애니메이션
        Animation slide_down = AnimationUtils.loadAnimation(NewHomeActivity.this, R.anim.slide_down);
        llCategoryEditPopup.startAnimation(slide_down);

        //HomeActivity 초기에 카테고리 순서를 읽어올 수 있도록 카테고리 순서를 문자열로 저장한다.
        SharedPreferences pref = this.getSharedPreferences("fitmate", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(NewHomeActivity.VIEWORDER_KEY, strViewOrder.toString());
        editor.commit();
    }


    private class CategoryEditClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {

            rlActionBar.setVisibility( View.GONE );
            hsvCategory.setVisibility( View.GONE );

            rlCategoryEditActionBar.setVisibility(View.VISIBLE);
            llCategoryEditPopup.setVisibility(View.VISIBLE);
            hsvCategoryEdit.setVisibility(View.VISIBLE);

            llCategoryEdit.removeAllViews();
            llCategory.removeView(mCategoryEditCategoryView);

            tvCategoryEditSave.setAlpha(0.2f);
            tvCategoryEditSave.setEnabled(false);

            CategoryEditInfo[] categoryEditInfos = new CategoryEditInfo[ categoryViews.length ];
            categoryEditViews = new NewHomeCategoryView[ CategoryType.values().length ];
            for( int i = 0 ; i < categoryViews.length ; i++ )
            {
                NewHomeCategoryView categoryView = categoryViews[i];
                CategoryEditInfo categoryEditInfo = categoryView.getCategoryEditInfo();
                categoryEditInfo.setCategoryType( CategoryType.values()[i] );
                categoryEditInfos[i] = categoryEditInfo;
            }

            for( int viewIndex = 0 ; viewIndex < llCategory.getChildCount() ;viewIndex++ )
            {
                NewHomeCategoryView categoryView = (NewHomeCategoryView) llCategory.getChildAt( viewIndex );
                NewHomeCategoryView categoryEditView = NewHomeCategoryView_.build( NewHomeActivity.this );
                categoryView.copyContents( categoryEditView );
                newHomeUtils.unSelectCategoryView(categoryEditView);
                llCategoryEdit.addView(categoryEditView);
                categoryEditViews[ categoryView.getCategoryType().ordinal() ] = categoryEditView;

                CategoryEditInfo categoryEditInfo = categoryEditInfos[ categoryView.getCategoryType().ordinal() ];
                categoryEditInfo.setVisible(true);
            }

            llCategoryEdit.post(new Runnable() {
                @Override
                public void run() {
                    changeCategoryViewGravity(llCategoryEdit, hsvCategoryEdit);
                }
            });

            gvCategoryEditPopup.setAdapter(new CategoryEditAdapter(NewHomeActivity.this, categoryEditInfos));
            gvCategoryEditPopup.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    NewHomeCategoryEditItemView categoryEditItemView = (NewHomeCategoryEditItemView) view;
                    boolean isVisible = categoryEditItemView.isVisible();
                    if (isVisible) {

                        //카테고리 수정 삭제뷰에서 없어짐.
                        NewHomeCategoryView newHomeCategoryView = categoryViews[ categoryEditItemView.getCategoryType().ordinal() ];
                        newHomeUtils.unSelectCategoryView( newHomeCategoryView );

                        llCategoryEdit.removeView(categoryEditViews[categoryEditItemView.getCategoryType().ordinal()]);
                        changeCategoryViewGravity( llCategoryEdit , hsvCategoryEdit );

                        categoryEditItemView.setImage( R.drawable.newhome_categoryedit_item_normal );
                        categoryEditItemView.setVisible(false);

                        tvCategoryEditSave.setAlpha(1);
                        tvCategoryEditSave.setEnabled(true);

                    } else {
                        //카테고리 수정 삭제 뷰에서 다시 보임.
                        NewHomeCategoryView categoryView = categoryViews[ categoryEditItemView.getCategoryType().ordinal() ];

                        NewHomeCategoryView categoryEditView = NewHomeCategoryView_.build( NewHomeActivity.this );
                        categoryView.copyContents(categoryEditView);
                        newHomeUtils.unSelectCategoryView(categoryEditView);
                        llCategoryEdit.addView(categoryEditView);
                        categoryEditViews[ categoryEditView.getCategoryType().ordinal() ] = categoryEditView;

                        changeCategoryViewGravity(llCategoryEdit, hsvCategoryEdit);

                        categoryEditItemView.setImage(R.drawable.newhome_categoryedit_item_select);
                        categoryEditItemView.setVisible(true);
                        categoryEditItemView.setCategoryViewIndex( llCategoryEdit.getChildCount() - 1 );

                        tvCategoryEditSave.setAlpha(1);
                        tvCategoryEditSave.setEnabled(true);
                    }
                }
            });

            Animation slide_up = AnimationUtils.loadAnimation(  NewHomeActivity.this , R.anim.slide_up );
            slide_up.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) { }

                @Override
                public void onAnimationEnd(Animation animation) {

                }

                @Override
                public void onAnimationRepeat(Animation animation) { }
            });
            llCategoryEditPopup.startAnimation(slide_up);
        }
    }

    private class CategoryEditAdapter extends BaseAdapter {

        private Context mContext = null;
        private CategoryEditInfo[] arrCategoryEditInfo = null;

        public CategoryEditAdapter(Context context , CategoryEditInfo[] categoryEditInfos)
        {

            this.mContext = context;
            this.arrCategoryEditInfo = categoryEditInfos;
        }

        @Override
        public int getCount() {
            return arrCategoryEditInfo.length;
        }

        @Override
        public Object getItem(int position) {
            return arrCategoryEditInfo[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            NewHomeCategoryEditItemView newHomeCategoryEditItemView = null;
            if( convertView == null ){
                newHomeCategoryEditItemView = NewHomeCategoryEditItemView_.build(mContext);
            }else{
                newHomeCategoryEditItemView = (NewHomeCategoryEditItemView) convertView;
            }
            newHomeCategoryEditItemView.bind( arrCategoryEditInfo[ position ] );
            return newHomeCategoryEditItemView;
        }
    }

    private void changeCategoryViewGravity( LinearLayout llView , HorizontalScrollView hsvView )
    {

        int viewWidthSum = 0;
        for( int i = 0 ; i < llView.getChildCount() ;i++ ){
            viewWidthSum += llView.getChildAt( i ).getMeasuredWidth();
        }

        if( viewWidthSum < hsvView.getMeasuredWidth() )
        {
            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) llView.getLayoutParams();
            params.width = LinearLayout.LayoutParams.MATCH_PARENT;
            params.gravity = Gravity.CENTER_HORIZONTAL;

            llView.setLayoutParams(params);

        }else{

            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) llView.getLayoutParams();
            params.width = LinearLayout.LayoutParams.WRAP_CONTENT;
            params.gravity = Gravity.NO_GRAVITY;

            llView.setLayoutParams(params);
        }
    }

    @Click(R.id.img_activity_newhome_program)
    void programClick(){

        ExerciseProgram program = getExerciseProgram();
        newHomeUtils.showProgramDialog( program.getId() );

    }

}
