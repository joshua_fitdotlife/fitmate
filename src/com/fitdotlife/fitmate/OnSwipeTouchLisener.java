package com.fitdotlife.fitmate;

import android.view.View;

/**
 * Created by Joshua on 2015-10-16.
 */
public interface OnSwipeTouchLisener {
    void onSwipeLeft(View view);
    void onSwipeRight( View view );
}