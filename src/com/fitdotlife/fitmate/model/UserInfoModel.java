package com.fitdotlife.fitmate.model;


import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;

import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.http.FitLifeHttpClient;
import com.fitdotlife.fitmate_lib.http.HttpManager;
import com.fitdotlife.fitmate_lib.http.HttpRequest;
import com.fitdotlife.fitmate_lib.http.HttpRequestCallback;
import com.fitdotlife.fitmate_lib.http.NetworkClass;
import com.fitdotlife.fitmate_lib.image.ImageUtil;
import com.fitdotlife.fitmate_lib.imodel.IUserInfoModel;
import com.fitdotlife.fitmate_lib.key.CommonKey;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.presenter.LoginPresenter;
import com.soundcloud.android.crop.CropUtil;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Headers;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.WeakHashMap;
import java.util.concurrent.TimeUnit;

/**
 * Created by Joshua on 2015-01-29.
 */
public class UserInfoModel implements  IUserInfoModel , HttpRequestCallback  {

    private final String TAG  = "fitmate";

    public static final int LOGIN_RESPONSE_CODE = 0;
    public static final int JOIN_MEMBER_RESPONSE = 1;
    public static final int USABLE_EMAIL_RESPONSE = 2;
    public static final int CHANGE_USERINFO_RESPONSE = 3;
    public static final int FIND_ACCOUNT_RESPONSE = 4;
    public static final int SEND_PASSWORD_RESPONSE = 5;
    public static final int SET_ACCOUNT_RESPONSE = 6;
    public static final int VERSION_CHECK_RESPONSE = 7;

    private final String LOGIN_FILE_NAME = "WebForm_login.aspx";
    private final String JOIN_MEMBER_FILE_NAME ="joinMember.aspx";
    private final String IS_USABLE_EMAIL_FILE_NAME = "isUsableEmail.aspx";
    private final String CHANGE_USERINFO_FILE_NAME = "ChangeUserInfo.aspx";
    private final String FIND_ACCOUNT_FILE_NAME = "FindAccount";
    private final String SEND_PASSWORD_FILE_NAME = "FindAccount";
    private final String SET_ACCOUNT_FILE_NAME = "SetAccount.aspx";
    private final String VERSION_CHECK_FILE_NAME = "VersionCheck";

    private UserInfo mUserInfo = null;
    private Context mContext = null;
    private FitmateDBManager mDBManager = null;
    private UserInfoModelResultListener mListener = null;
    private HttpManager mHttpManager = null;

    private String mEmail = null;
    private String mPassword = null;

    public UserInfoModel( Context context ){

        this.mContext = context;
        this.mDBManager = new FitmateDBManager(this.mContext);

    }

    public UserInfoModel( Context context , UserInfoModelResultListener listener ){

        this.mContext = context;
        this.mDBManager = new FitmateDBManager(this.mContext);
        this.mListener = listener;
        this.mHttpManager = new HttpManager( this.mContext , this );
    }



    public static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");

    static OkHttpClient client = new OkHttpClient();

    public static void getImage(String url, Context ctx)
    {
       String  urls = NetworkClass.baseURL + url;

        Request request = new Request.Builder()
                .url(urls)
                .build();

        Response response = null;
        try {
            response = client.newCall(request).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {

            byte [] ddd = response.body().bytes();
            InputStream is = new ByteArrayInputStream(ddd);
            BitmapFactory.Options dbo = new BitmapFactory.Options();
            dbo.inJustDecodeBounds = true;

            Bitmap bmp = BitmapFactory.decodeByteArray(ddd, 0, ddd.length);

            Uri saveUri = ImageUtil.getOutputPhotoFileUri();
            OutputStream outputStream = null;
            try {
                if(bmp == null)
                    throw new Exception("bmp file is null");

                File nf = new File(ImageUtil.getOutputPhotoFileUri().toString());

                outputStream = ctx.getContentResolver().openOutputStream(saveUri);
                if (outputStream != null ) {
                    bmp.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
                }

            } catch (IOException e) {
                //setResultException(e);
                org.apache.log4j.Log.e("Cannot open file: " + saveUri, e.getMessage());
            }
            catch (Exception e)
            {
                org.apache.log4j.Log.e("Cannot photo file save: " + saveUri, e.getMessage());
            }
            finally {
                CropUtil.closeSilently(outputStream);
            }

            /*
            CropUtil.copyExifRotation(
                    CropUtil.getFromMediaUri(this, getContentResolver(), sourceUri),
                    CropUtil.getFromMediaUri(this, getContentResolver(), saveUri)
            );
            */
            is.close();



        } catch (IOException e) {
            e.printStackTrace();
        }


        Headers responseHeaders = response.headers();
        for (int i = 0; i < responseHeaders.size(); i++) {
            System.out.println(responseHeaders.name(i) + ": " + responseHeaders.value(i));
        }

        //System.out.println(response.body().string());
    }

    public static String getUserInfo(String id, String Passwd) throws IOException {
        client.setConnectTimeout(10, TimeUnit.SECONDS);
        client.setWriteTimeout(10, TimeUnit.SECONDS);
        client.setReadTimeout(10, TimeUnit.SECONDS);

        String url = NetworkClass.baseURL + "GetUserInfo.aspx";

        String qry = String.format("?email=%s&pw=%s", id, Passwd);
        url += qry;

        final Request request = new Request.Builder()
                .header("Authorization", "Client-ID " + ":DDD")
                .url(url)
                .get()
                .build();

        Handler mHandler = new Handler(Looper.getMainLooper());
        final boolean[] isRun = {true};
        final String[] imgurl = {""};
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // 내용
                client.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(Request request, IOException e) {
                        isRun[0] = false;
                        Handler mHandler = new Handler(Looper.getMainLooper());
                        mHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                //onHttpResponse(null, mResponseCode, HttpRequest.FAIL);
                            }},0);
                    }

                    @Override
                    public void onResponse(final Response response) throws IOException {
                        Handler mHandler = new Handler(Looper.getMainLooper());
                        mHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if(response.code() == 200) {
                                    String data = null;
                                    try {
                                        data = response.body().string();

                                        JSONObject jsonObject = new JSONObject( data );
                                        String result = jsonObject.getString( CommonKey.RESPONSE_VALUE_KEY );

                                        if( result.equals( CommonKey.SUCCESS ) ) {

                                            imgurl[0] = jsonObject.getString("profileimg");
                                            //네트워크 버전
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    //onHttpResponse(data, mResponseCode, HttpRequest.SUCCESS);
                                }
                                else
                                {
                                    //onHttpResponse(null, mResponseCode, HttpRequest.FAIL);
                                }

                                isRun[0] = false;
                            }},0);
                    }
                });

            }}, 0);

        for(int i = 0 ; i < 50 && isRun[0] == true; i++)
        {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }


        return imgurl[0];
    }


    String post(String url, RequestBody body, final int mResponseCode) throws IOException {
        client.setConnectTimeout(10, TimeUnit.SECONDS);
        client.setWriteTimeout(10, TimeUnit.SECONDS);
        client.setReadTimeout(10, TimeUnit.SECONDS);

        final Request request = new Request.Builder()
                .header("Authorization", "Client-ID " + ":DDD")
                .url(url)
                .post(body)
                .build();

        Handler mHandler = new Handler(Looper.getMainLooper());
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // 내용
                client.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(Request request, IOException e) {
                        //ShowMessage("변경 실패 : " + e.getMessage());

                        Handler mHandler = new Handler(Looper.getMainLooper());
                        mHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                onHttpResponse(null, mResponseCode, HttpRequest.FAIL);
                            }},0);
                    }

                    @Override
                    public void onResponse(final Response response) throws IOException {
                        Handler mHandler = new Handler(Looper.getMainLooper());
                        mHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                if(response.code() == 200) {

                                    String data = null;
                                    try {
                                        data = response.body().string();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    onHttpResponse(data, mResponseCode, HttpRequest.SUCCESS);
                                }
                                else
                                {
                                    onHttpResponse(null, mResponseCode, HttpRequest.FAIL);
                                }
                            }},0);
                    }
                });

            }}, 0);
        return "";
    }





    public String getHttpPost( String uri , WeakHashMap<String , String> parameters ) throws UnsupportedEncodingException
    {
        StringBuilder sb = new StringBuilder();
        if( parameters != null )
        {

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            Iterator<String> parametersIter = parameters.keySet().iterator();

            while( parametersIter.hasNext() )
            {
                String key = parametersIter.next();
                String val = parameters.get(key);

                sb.append(String.format("%s=%s&", key, val));
            }
        }

        return uri + "?" + sb.toString();
    }


    public void setUserAccount( UserInfo userInfo )
    {

        this.mUserInfo = userInfo;
        FitLifeHttpClient client = new FitLifeHttpClient();

        try {
            WeakHashMap<String , String> parameters = userInfo.toAccountParameters(mDBManager.getAppliedProgram().getId());
            parameters.put("isTick", "false");

            String uri = getHttpPost(NetworkClass.baseURL + this.SET_ACCOUNT_FILE_NAME, userInfo.toAccountParameters( mDBManager.getAppliedProgram().getId() ));


            RequestBody body = null;
            if(false)
            {
                body = RequestBody.create(MediaType.parse("image/jpg"), new File("/storage/emulated/0/DCIM/Camera/20150529_172213.jpg"));
            }
            else {
                if(userInfo.getAccountPhoto() != null && !userInfo.getAccountPhoto().equals("")) {
                    String sss = userInfo.getAccountPhoto();
                    if(sss.startsWith("file://"))
                    {
                        sss = (String) sss.subSequence(7, sss.length());
                    }
                    File f = new File(sss);
                    body = new MultipartBuilder()
                            .type(MultipartBuilder.FORM)
                            .addFormDataPart("Files", f.getName(),
                                    RequestBody.create(MediaType.parse("image/jpg"), f))
                            .build();
                }
                else
                {
                    body = RequestBody.create(null, "");
                }
            }

            post(uri, body, SET_ACCOUNT_RESPONSE);



        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void setUserInfo( UserInfo userInfo ) {
        this.mUserInfo = userInfo;
        //this.mHttpManager.sendServer( userInfo.toParameters() , this.JOIN_MEMBER_FILE_NAME , JOIN_MEMBER_RESPONSE );


        FitLifeHttpClient client = new FitLifeHttpClient();

        try {

            WeakHashMap<String , String> parameters = userInfo.toParameters(mDBManager.getAppliedProgram().getId());
            parameters.put("isTick" , "false");
            String uri = getHttpPost(NetworkClass.baseURL + this.JOIN_MEMBER_FILE_NAME, parameters);


            RequestBody body = null;
            if(false)
            {
                body = RequestBody.create(MediaType.parse("image/jpg"), new File("/storage/emulated/0/DCIM/Camera/20150529_172213.jpg"));
            }
            else {
                if(userInfo.getAccountPhoto() != null && !userInfo.getAccountPhoto().equals("")) {
                    String sss = userInfo.getAccountPhoto();
                    if(sss.startsWith("file://"))
                    {
                        sss = (String) sss.subSequence(7, sss.length());
                    }
                    File f = new File(sss);
                    body = new MultipartBuilder()
                            .type(MultipartBuilder.FORM)
                            .addFormDataPart("Files", f.getName(),
                                    RequestBody.create(MediaType.parse("image/jpg"), f))
                            .build();
                }
                else
                {
                    body = RequestBody.create(null, "");
                }
            }

            post(uri, body, JOIN_MEMBER_RESPONSE);



        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public void setUserInfoToLocalDB( UserInfo userInfo ){
        this.mDBManager.setUserInfo(userInfo);
    }

    public void findUser( String email ) {

        String fileName = "api/" + FIND_ACCOUNT_FILE_NAME + "?email=" + email;
        this.mHttpManager.sendServer(fileName, FIND_ACCOUNT_RESPONSE, HttpGet.METHOD_NAME);
    }

    public boolean localLogin( String id, String pwd ){

        UserInfo userInfo = this.mDBManager.getUserInfo();

        if( userInfo != null ) {
            if (userInfo.getEmail().equals(id) && userInfo.getPassword().equals(pwd)) {
                return true;
            } else {
                return false;
            }
        }else {
            return false;
        }

    }

    public void registerUserInfo(){
        UserInfo userInfo = this.mDBManager.getUserInfo();
        WeakHashMap<String , String> parameters = userInfo.toParameters(mDBManager.getAppliedProgram().getId());
        parameters.put("isTick" , "false");
        this.mHttpManager.sendServer( parameters , this.CHANGE_USERINFO_FILE_NAME , CHANGE_USERINFO_RESPONSE );
    }

    public void registerUserInfo(UserInfo userInfo){
        userInfo.setAccountPhoto( this.mDBManager.getUserInfo().getAccountPhoto() );
        this.mUserInfo = userInfo;
        WeakHashMap<String , String> parameters = userInfo.toParameters( mDBManager.getAppliedProgram().getId() );
        parameters.put("isTick" , "false");
        this.mHttpManager.sendServer( parameters , this.CHANGE_USERINFO_FILE_NAME , JOIN_MEMBER_RESPONSE );
    }

    public void changeExerciseProgramIDForServer(  int exercieProgramID  ){
        UserInfo userInfo = this.mDBManager.getUserInfo();

        WeakHashMap<String , String> parameters = new WeakHashMap<String, String>();
        parameters.put(UserInfo.EMAIL_KEY, userInfo.getEmail() );
        parameters.put(UserInfo.PASSWORD_KEY, userInfo.getPassword() );
        parameters.put(UserInfo.EXERCISE_PROGRAM_ID_KEY , exercieProgramID + "");

        this.mHttpManager.sendServer( parameters , this.CHANGE_USERINFO_FILE_NAME , CHANGE_USERINFO_RESPONSE );
    }


    public void serverLogin( String id, String pwd ){

        this.mEmail = id;
        this.mPassword = pwd;

        WeakHashMap<String , String > parameters = new WeakHashMap<>();
        parameters.put( UserInfo.EMAIL_KEY , id );
        parameters.put( UserInfo.PASSWORD_KEY , pwd );
        parameters.put("isTick" , "false");

        this.mHttpManager.sendServer( parameters , this.LOGIN_FILE_NAME , LOGIN_RESPONSE_CODE );
    }

    @Override
    public UserInfo getUserInfo(  ) {
        return this.mDBManager.getUserInfo( );
    }

    @Override
    public void updateDeviceAddress(String deviceAdrress) {

        this.mDBManager.updateDeviceAddress(deviceAdrress, this.getUserInfo().getEmail());
    }

    public void isUsableEmail( String email ){
        WeakHashMap<String , String> parameters = new WeakHashMap<>();
        parameters.put(UserInfo.EMAIL_KEY, email);
        this.mHttpManager.sendServer(parameters, this.IS_USABLE_EMAIL_FILE_NAME, USABLE_EMAIL_RESPONSE);
    }

    @Override
    public void modifyUserInfo(UserInfo userInfo) {
        this.mDBManager.modifyUserInfo(userInfo);
    }

    public void deleteUserInfo(  ){
        this.mDBManager.deleteUserInfo();
    }

    @Override
    public void sendPasswordEmail(String email) {
        String fileName = "api/" + SEND_PASSWORD_FILE_NAME + "?email=" + email;
        this.mHttpManager.sendServer(fileName, SEND_PASSWORD_RESPONSE, HttpPost.METHOD_NAME);
    }

    public void checkVersion(String version) {
        String fileName = "api/" + VERSION_CHECK_FILE_NAME + "?version=" + version ;
        this.mHttpManager.sendServer(fileName, VERSION_CHECK_RESPONSE , HttpGet.METHOD_NAME );
    }

    @Override
    public void onHttpResponse(String data, int responseCode, int resultCode) {

        if( responseCode == LOGIN_RESPONSE_CODE ){
            if( resultCode == HttpRequest.SUCCESS){
                try {
                    JSONObject jsonObject = new JSONObject( data );
                    String result = jsonObject.getString( CommonKey.RESPONSE_VALUE_KEY );

                    if( result.equals( CommonKey.SUCCESS ) ){

                        //네트워크 버전
                        UserInfo userInfo = UserInfo.parseJSONObject( jsonObject );
                        userInfo.setEmail( this.mEmail );
                        userInfo.setPassword(this.mPassword);

                        if( userInfo.getDeviceAddress() == null || userInfo.getDeviceAddress().equals("") || userInfo.getDeviceAddress().equals("null"))
                        {
                            this.setSettedUserInfo(false);
                            this.mDBManager.setUserAccount( userInfo );
                        }else{
                            this.setSettedUserInfo(true);
                            this.mDBManager.setUserInfo( userInfo );
                        }

                        this.mListener.onSuccess(responseCode, userInfo);

                    }else if(result.equals(CommonKey.FAIL)){
                        this.mListener.onFail( responseCode );
                    }
                } catch (JSONException e) {
                    this.mListener.onErrorOccured( responseCode , e.getMessage() );
                }
            }else if( resultCode == HttpRequest.FAIL ){
                this.mListener.onFail(responseCode);
            }
        }else if( responseCode == JOIN_MEMBER_RESPONSE ) {
            if (resultCode == HttpRequest.SUCCESS) {

                try {
                    JSONObject jsonObject = new JSONObject(data);
                    String result = jsonObject.getString(CommonKey.RESPONSE_VALUE_KEY);

                    if (result.equals(CommonKey.SUCCESS)) {

                        String imageUrl = jsonObject.getString("url");
                        if( imageUrl != null) {
                            if(!imageUrl.equals("")) {
                                this.mUserInfo.setAccountPhoto( imageUrl );
                            }
                        }
                        this.mDBManager.setUserInfo( this.mUserInfo );
                        this.mListener.onSuccess(responseCode);


                    } else if (result.equals(CommonKey.FAIL)) {
                        this.mListener.onFail(responseCode);
                    }

                } catch (JSONException e) {
                    this.mListener.onErrorOccured(responseCode, e.getMessage());
                }
            } else if (resultCode == HttpRequest.FAIL) {
                this.mListener.onFail(responseCode);
            }
        } else if( responseCode == SET_ACCOUNT_RESPONSE ) {
            if (resultCode == HttpRequest.SUCCESS) {

                try {
                    JSONObject jsonObject = new JSONObject(data);
                    String result = jsonObject.getString(CommonKey.RESPONSE_VALUE_KEY);

                    if (result.equals(CommonKey.SUCCESS)) {

                        String imageUrl = jsonObject.getString("url");
                        if( imageUrl != null) {
                            if(!imageUrl.equals("")) {
                                this.mUserInfo.setAccountPhoto(imageUrl);
                            }
                        }

                        this.mDBManager.setUserAccount(this.mUserInfo);
                        this.mListener.onSuccess(responseCode);

                    } else if (result.equals(CommonKey.FAIL)) {
                        this.mListener.onFail(responseCode);
                    }

                } catch (JSONException e) {
                    this.mListener.onErrorOccured(responseCode, e.getMessage());
                }
            } else if (resultCode == HttpRequest.FAIL) {
                this.mListener.onFail(responseCode);
            }
        } else if( responseCode == USABLE_EMAIL_RESPONSE ){
            if( resultCode == HttpRequest.SUCCESS ){
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    String result = jsonObject.getString(CommonKey.RESPONSE_VALUE_KEY);

                    if (result.equals(CommonKey.SUCCESS))
                    {
                        this.mListener.onSuccess(responseCode);

                    } else if (result.equals(CommonKey.FAIL)) {

                        this.mListener.onFail(responseCode);

                    }

                } catch (JSONException e) {
                    this.mListener.onErrorOccured(responseCode, e.getMessage());
                }
            }else if( resultCode == HttpRequest.FAIL ){
                this.mListener.onFail( responseCode );
            }
        }else if( responseCode == CHANGE_USERINFO_RESPONSE ){
            if( resultCode == HttpRequest.SUCCESS ){
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    String result = jsonObject.getString( CommonKey.RESPONSE_VALUE_KEY );

                    if (result.equals(CommonKey.SUCCESS))
                    {
                        this.mListener.onSuccess( responseCode );

                    } else if (result.equals(CommonKey.FAIL)) {

                        this.mListener.onFail(responseCode);
                    }

                } catch (JSONException e) {
                    this.mListener.onErrorOccured(responseCode, e.getMessage());
                }
            }else if( resultCode == HttpRequest.FAIL ){
                this.mListener.onFail( responseCode );
            }
        }else if( responseCode == FIND_ACCOUNT_RESPONSE ){
            if( resultCode == HttpRequest.SUCCESS ){

                data = data.replaceAll("\"" , "");

                if( data == null || data.equals("") || data.isEmpty() ){
                    this.mListener.onFail(responseCode);
                }else{
                    UserInfo userInfo = new UserInfo();
                    userInfo.setName(data);
                    this.mListener.onSuccess(responseCode , userInfo);
                }

            }else if(resultCode == HttpRequest.FAIL){
                this.mListener.onFail(responseCode);
            }
        }else if( responseCode == SEND_PASSWORD_RESPONSE ){
            if( resultCode == HttpRequest.SUCCESS ){

                if(data == null){
                    this.mListener.onFail(responseCode);
                }
                else if( data.equals( CommonKey.TRUE ) ){
                    this.mListener.onSuccess( responseCode );
                }else if( data.equals(CommonKey.FALSE) ){
                    this.mListener.onFail(responseCode);
                }
            }else if(resultCode == HttpRequest.FAIL){
                this.mListener.onFail(responseCode);
            }
        }else if(responseCode == VERSION_CHECK_RESPONSE){
            if( resultCode == HttpRequest.SUCCESS ){
                if(data == null){
                    this.mListener.onFail(responseCode);
                }
                else if( data.equals( CommonKey.TRUE ) ){
                    this.mListener.onSuccess( responseCode , true );
                }else if( data.equals(CommonKey.FALSE) ){
                    this.mListener.onSuccess(responseCode , false);
                }
            }else if(resultCode == HttpRequest.FAIL){
                this.mListener.onFail(responseCode);
            }
        }
    }

    private void setSettedUserInfo(boolean settedUserInfo){
        SharedPreferences pref = this.mContext.getSharedPreferences( "fitmateservice" , this.mContext.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean( LoginPresenter.SET_USERINFO_KEY , settedUserInfo);
        editor.commit();
    }
}
