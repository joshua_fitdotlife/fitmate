package com.fitdotlife.fitmate.model;

/**
 * Created by Joshua on 2015-02-12.
 */
public interface ServiceResultListener {
    public void onCalculateCompleted();
    public void onSyncEnd();
    public void onBatteryStatus(int batteryRatio);
    public void onSyncProgress( int receiveBytes , int totalBytes );
}
