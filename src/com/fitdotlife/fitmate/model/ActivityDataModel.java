package com.fitdotlife.fitmate.model;

import android.content.Context;
import android.util.Log;

import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseAnalyzer;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.StrengthInputType;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.UserInfoForAnalyzer;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.WearingLocation;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.http.HttpManager;
import com.fitdotlife.fitmate_lib.http.HttpRequest;
import com.fitdotlife.fitmate_lib.http.HttpRequestCallback;
import com.fitdotlife.fitmate_lib.key.CommonKey;
import com.fitdotlife.fitmate_lib.object.DayActivity;
import com.fitdotlife.fitmate_lib.object.ExerciseProgram;
import com.fitdotlife.fitmate_lib.object.ScoreClass;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.object.WeekActivity;
import com.fitdotlife.fitmate_lib.util.Utils;

import org.apache.http.client.methods.HttpGet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Joshua on 2015-01-29.
 */
public class ActivityDataModel implements HttpRequestCallback {

    private final String TAG = "fitmate";

    public static final int UPLOAD_ACTIVITYLIST_RESPONSE_CODE = 0;
    public static final int GET_DAYACTIVITY_RESPONSE_CODE = 1;
    public static final int GET_WEEKACTIVITY_RESPONSE_CODE = 2;
    public static final int GET_MONTHACTIVITY_RESPONSE_CODE = 3;
    public static final int GET_DAYACTIVITY_RESPONSE_CODE_Mergy = 4;

    //tick
    private final String UPLOAD_ACTIVITYLIST_FILE_NAME = "SyncUpload/postupLoadActivityData_v_2";
    private final String GET_DAYACTIVITY_FILE_NAME = "WeekInfoWithDaysList/getWeekActivityWithDaysList_v_2";
    private final String GET_WEEKACTIVITY_FILE_NAME = "WeekInfoWithDaysList/getWeekActivityWithDaysList_v_2";
    private final String GET_MONTHACTIVITY_FILE_NAME = "WeekInfoWithDaysList/getWeekActivityWithDaysList_v_2";

    //private ServiceClient mServiceClient = null;
    private HttpManager mHttpManager = null;

    private Context mContext = null;
    private ServiceResultListener mListener = null;
    private ActivityDataListener mActivityDataListener = null;
    private FitmateDBManager mDBManager = null;

    private List<DayActivity> mUploadDayActivityList = null;
    private List<WeekActivity> mUploadWeekActivityList = null;


    public ActivityDataModel(Context context ,  ActivityDataListener activityDataListener )
    {
        this.mContext = context;
        //this.mServiceClient = new ServiceClient(this.mContext , this);
        this.mDBManager =  new FitmateDBManager( this.mContext );
        this.mActivityDataListener = activityDataListener;
        this.mHttpManager = new HttpManager(this.mContext , this);
    }

    public ActivityDataModel(Context context)
    {
        this.mContext = context;
        this.mDBManager =  new FitmateDBManager( this.mContext );
        this.mHttpManager = new HttpManager(this.mContext , this);
    }

    public DayActivity getDayActivity( String activityDate ){
        return this.mDBManager.getDayActivity(activityDate);
    }

    public WeekActivity getWeekActivity( String activityDate ){
        return this.mDBManager.getWeekActivity(activityDate);
    }

    public List<WeekActivity> getWeekActivityList( String startDate , String endDate ){
        return this.mDBManager.getWeekActivityList(startDate, endDate);
    }

//    public void requestCalculateActivity() throws RemoteException {
//        this.mServiceClient.sendServiceMessage(ServiceClient.MSG_CALCULATE_ACTIVITY);
//    }

    //public void open() {
    //    this.mServiceClient.open();
    //}

//    public void start(){
//        this.mServiceClient.start();
//    }

    //public void close(){ this.mServiceClient.close(); }

    public List<DayActivity> getNotUploadDayActivity(){

        return this.mDBManager.getNotUploadDayActivity();
    }

    public List<WeekActivity> getNotUploadWeekActivity(){

        return this.mDBManager.getNotUploadWeekActivity();
    }

    public List<DayActivity> getDayActivityList(String weekStartDate, String weekEndDate) {
        return this.mDBManager.getDayActivityList(weekStartDate, weekEndDate);
    }

    public void uploadActivityList( List<DayActivity> uploadDayActivityList , List<WeekActivity> uploadWeekActivityList ){
        JSONObject jsonUploadObject = new JSONObject();
        ExerciseProgram usedProgram = this.mDBManager.getAppliedProgram();

        if( uploadWeekActivityList != null ) {
            JSONArray jsonArrayWeekActivity = new JSONArray();
            for (int i = 0; i < uploadWeekActivityList.size(); i++) {

                WeekActivity weekActivity = uploadWeekActivityList.get(i);
                jsonArrayWeekActivity.put(weekActivity.toJSONObject( usedProgram.getId()) );
            }

            try {
                jsonUploadObject.put("weekItemList", jsonArrayWeekActivity);
            } catch (JSONException e) {
            }
        }

        if( uploadDayActivityList != null ) {
            JSONArray jsonArrayDayActivity = new JSONArray();
            for (int i = 0; i < uploadDayActivityList.size(); i++) {

                DayActivity dayActivity = uploadDayActivityList.get(i);
                jsonArrayDayActivity.put( dayActivity.toJSONObject( usedProgram.getId()) );
            }

            try {
                jsonUploadObject.put("dayItemList", jsonArrayDayActivity);
            } catch (JSONException e) {
            }
        }

        UserInfo userInfo = this.mDBManager.getUserInfo();

        //tick
        //String fileName = "api/" + this.UPLOAD_ACTIVITYLIST_FILE_NAME + "?email=" + userInfo.getEmail()";
        //yyyy-MM-dd
        //String fileName = "api/" + this.UPLOAD_ACTIVITYLIST_FILE_NAME + "?email=" + userInfo.getEmail()+"&isTick=false";
        String fileName = "api/" + this.UPLOAD_ACTIVITYLIST_FILE_NAME + "/" + userInfo.getEmail()+"/";
        this.mHttpManager.sendServer( jsonUploadObject.toString(), fileName, UPLOAD_ACTIVITYLIST_RESPONSE_CODE );

        this.mUploadWeekActivityList = uploadWeekActivityList;
        this.mUploadDayActivityList = uploadDayActivityList;
    }

//    public void uploadDayActivityList(  ) {
//
//        UserInfo userInfo = this.mDBManager.getUserInfo();
//        String fileName = "api/" + UPLOAD_DAYACTIVITYLIST_FILE_NAME + "?email=" + userInfo.getEmail();
//        this.mHttpManager.sendServer( jsonArrayDayActivity , fileName , UPLOAD_DAYACTIVITYLIST_RESPONSE_CODE );
//        this.mUploadDayActivityList = uploadDayActivityList;
//    }
//
//    public void uploadWeekActivityList() {
//
//        UserInfo userInfo = this.mDBManager.getUserInfo();
//        String fileName = "api/" + UPLOAD_WEEKACTIVITYLIST_FILE_NAME + "?email=" + userInfo.getEmail();
//        this.mHttpManager.sendServer(jsonArrayWeekActivity, fileName, UPLOAD_WEEKACTIVITYLIST_RESPONSE_CODE);
//        this.mUploadWeekActivityList = uploadWeekActivityList;
//    }

    public void getDayActivityFromServer( String activityDate ) {

//        UserInfo userInfo = this.mDBManager.getUserInfo();
//        String email = userInfo.getEmail();
//        String password = userInfo.getPassword();
//
//        WeakHashMap<String, String> parameters = new WeakHashMap<String , String>();
//        parameters.put("email" , email);
//        parameters.put("date" , activityDate + "");
//        parameters.put( "pw" , password );
//        //TODO 수정해야함.
//        parameters.put("exprogramid", "3");
//
//        this.mHttpManager.sendServer( parameters , this.GET_DAYACTIVITY_FILE_NAME , GET_DAYACTIVITY_RESPONSE_CODE  );

        UserInfo userInfo = this.mDBManager.getUserInfo();
        String email = userInfo.getEmail();

        String fileName = "api/" + GET_DAYACTIVITY_FILE_NAME + "/" + email + "/?miliseconds=" + activityDate;
        this.mHttpManager.sendServer( fileName , GET_DAYACTIVITY_RESPONSE_CODE , HttpGet.METHOD_NAME );
    }

    public void getDayActivityFromServer_Mergy( String activityDate ) {

        UserInfo userInfo = this.mDBManager.getUserInfo();
        String email = userInfo.getEmail();

        String fileName = "api/" + GET_DAYACTIVITY_FILE_NAME + "/" + email + "/?miliseconds=" + activityDate;
        this.mHttpManager.sendServer( fileName , GET_DAYACTIVITY_RESPONSE_CODE_Mergy , HttpGet.METHOD_NAME );
    }

    public void getWeekActivityFromServer( String weekStartDate ) {
        UserInfo userInfo = this.mDBManager.getUserInfo();
        String email = userInfo.getEmail();

        //tick
        //String fileName = "api/" + GET_WEEKACTIVITY_FILE_NAME + "?email=" + email + "&miliseconds=" + weekStartDate;
        //this.mHttpManager.sendServer( fileName , GET_WEEKACTIVITY_RESPONSE_CODE , HttpGet.METHOD_NAME  );

        //yyyy-MM-dd
        //TimeInfo timeInfo= new TimeInfo(weekStartDate);
        String fileName = "api/" + GET_WEEKACTIVITY_FILE_NAME + "/" + email + "/?miliseconds=" + weekStartDate;
        this.mHttpManager.sendServer( fileName , GET_WEEKACTIVITY_RESPONSE_CODE , HttpGet.METHOD_NAME  );
    }

    public void getMonthActivityListFromServer( List<String> noWeekList ) {
        UserInfo userInfo = this.mDBManager.getUserInfo();
        String email = userInfo.getEmail();

        JSONArray jsonWeekList = new JSONArray();
        for( int i = 0 ; i < noWeekList.size() ;i++  ) {
            jsonWeekList.put( noWeekList.get(i) );
        }

        String fileName = "api/" + GET_MONTHACTIVITY_FILE_NAME + "/?email=" + email +"&isTick=false";
        this.mHttpManager.sendServer(jsonWeekList.toString() , fileName , GET_MONTHACTIVITY_RESPONSE_CODE );
    }

    public void deleteAllDayActivity(){
        this.mDBManager.deleteAllDayActivity();
    }

    public void deleteAllWeekActivity(){
        this.mDBManager.deleteAllWeekActivity();
    }

    public void setNeedMergeToday() {
        Calendar todayCalendar = Calendar.getInstance();
        String todayDate = todayCalendar.get(Calendar.YEAR) + "-" + String.format("%02d", todayCalendar.get(Calendar.MONTH) + 1) + "-" + String.format("%02d", todayCalendar.get(Calendar.DAY_OF_MONTH));

        this.mDBManager.setNeedMergeDate(todayDate);

        DayActivity dayActivity = this.getDayActivity(todayDate);

    }

    @Override
    public void onHttpResponse(String data, int responseCode, int resultCode) {
       if( responseCode == UPLOAD_ACTIVITYLIST_RESPONSE_CODE ) {
           if (resultCode == HttpRequest.SUCCESS) {

               Log.i("fitmate", "DayActivity " + data);

               if (data.equals(CommonKey.TRUE)) {

                   if( this.mUploadDayActivityList != null ) {
                       this.mDBManager.checkUploadDayActivityList(this.mUploadDayActivityList);
                   }

                   if(this.mUploadWeekActivityList != null) {
                       this.mDBManager.checkUploadWeekActivityList(this.mUploadWeekActivityList);
                   }

                   this.mActivityDataListener.onUploadedData( true );
               }
           }
           else if( resultCode == HttpRequest.FAIL ){
               this.mActivityDataListener.onUploadedData( false );
           }
       }else if( responseCode == GET_DAYACTIVITY_RESPONSE_CODE || responseCode== GET_DAYACTIVITY_RESPONSE_CODE_Mergy ){

             if( resultCode == HttpRequest.SUCCESS ){
                 try {
                     JSONObject jsonObject = new JSONObject(data);
                     String result = jsonObject.getString( CommonKey.RESPONSE_VALUE_KEY );

                     if( result.equals( CommonKey.SUCCESS ) ){

                         WeekActivity weekActivity = WeekActivity.toWeekActivity(jsonObject);
                         this.mDBManager.setWeekActivity( weekActivity, FitmateDBManager.UPLOADED);

                         JSONArray jsonDayArray = jsonObject.getJSONArray("daysList");

                         List<DayActivity> dayActivityList = new ArrayList<DayActivity>();

                        //날짜 저장
                         for( int i = 0 ; i < jsonDayArray.length() ;i++ ) {
                             JSONObject dayJsonObject = jsonDayArray.getJSONObject(i);
                             DayActivity dayActivity = DayActivity.toDayActivity(dayJsonObject);
                             dayActivityList.add(dayActivity);
                         }

                         List<Integer>CalorieList = new ArrayList<Integer>();
                         for(int i=0;i<dayActivityList.size();i++){
                             CalorieList.add((dayActivityList.get(i).getCalorieByActivity()));
                         }

                         ExerciseProgram  exerciseProgram = this.mDBManager.getUserExerciProgram(weekActivity.getExerciseProgramID());
                         UserInfo userInfo = mDBManager.getUserInfo();
                         UserInfoForAnalyzer userInfoForAnalyzer = com.fitdotlife.fitmate_lib.service.util.Utils.getUserInfoForAnalyzer(userInfo);

                         com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseProgram program = Utils.ConvertExerciseProgram(exerciseProgram);
                         ExerciseAnalyzer analyzer = new ExerciseAnalyzer(program, userInfoForAnalyzer, WearingLocation.WRIST, 10);
                         StrengthInputType strengthType = program.getStrengthInputType();

                         List<ScoreClass> scoreClassList = null;
                         if( strengthType.equals( StrengthInputType.CALORIE ) || strengthType.equals(StrengthInputType.VS_BMR) ) {
                             scoreClassList = Utils.CalculateDayScores_Calorie(CalorieList, analyzer);
                         }else if( strengthType.equals(StrengthInputType.CALORIE_SUM) ){
                             scoreClassList = Utils.CalculateDayScores_CalorieSUM(CalorieList, analyzer);
                         }else{
                             scoreClassList = Utils.CalculateDayScores_Strength(dayActivityList, analyzer);
                         }

                         for(int i=0;i<dayActivityList.size();i++)
                         {
                             DayActivity act = dayActivityList.get(i);
                             ScoreClass score = scoreClassList.get(i);
                             act.setPredayScore(score.preDayAchieve);
                             act.setTodayScore(score.todayGetPoint);
                             act.setExtraScore(score.todayPossibleMorePoint);
                             act.setAchieveMax(score.isAchieveMax);
                             act.setPossibleMaxScore(score.possibleMaxScore);
                             act.setPossibleTimes(score.possibleTimes);
                             act.setPossibleValue(score.possibleValue);
                             Log.d("recalcul_point", String.format("pre:%d, today:%d, extra:%d ", score.preDayAchieve, score.todayGetPoint, score.todayPossibleMorePoint));
                             this.mDBManager.setDayActivity(act);
                         }
                     }

                 } catch (JSONException e) {
                     Log.d(TAG , e.getMessage());
                 }
             }

             this.mActivityDataListener.onDayActivityReceived();

        }else if( responseCode == GET_WEEKACTIVITY_RESPONSE_CODE ){

            if( resultCode == HttpRequest.SUCCESS ){
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    String result = jsonObject.getString( CommonKey.RESPONSE_VALUE_KEY );

                    if( result.equals( CommonKey.SUCCESS ) )
                    {

                        WeekActivity weekActivity = WeekActivity.toWeekActivity(jsonObject);
                        this.mDBManager.setWeekActivity( weekActivity, FitmateDBManager.UPLOADED);

                        JSONArray jsonDayArray = jsonObject.getJSONArray("daysList");

                        List<DayActivity> dayActivityList = new ArrayList<DayActivity>();
                        for( int i = 0 ; i < jsonDayArray.length() ;i++ ){
                            JSONObject dayJsonObject = jsonDayArray.getJSONObject(i);
                            DayActivity dayActivity = DayActivity.toDayActivity( dayJsonObject );
                            dayActivityList.add(dayActivity);
                        }

                        List<Integer>CalorieList = new ArrayList<Integer>();
                        for(int i=0;i<dayActivityList.size();i++){
                            CalorieList.add((dayActivityList.get(i).getCalorieByActivity()));
                        }

                        ExerciseProgram  exerciseProgram = this.mDBManager.getUserExerciProgram(weekActivity.getExerciseProgramID());
                        UserInfo userInfo = mDBManager.getUserInfo();

                        UserInfoForAnalyzer userInfoForAnalyzer = com.fitdotlife.fitmate_lib.service.util.Utils.getUserInfoForAnalyzer(userInfo);

                        com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseProgram program = Utils.ConvertExerciseProgram(exerciseProgram);
                        ExerciseAnalyzer analyzer = new ExerciseAnalyzer(program, userInfoForAnalyzer, WearingLocation.WRIST, 10);
                        StrengthInputType strengthType = program.getStrengthInputType();

                        List<ScoreClass> scoreClassList = null;
                        if( strengthType.equals( StrengthInputType.CALORIE ) || strengthType.equals(StrengthInputType.VS_BMR) ) {
                            scoreClassList = Utils.CalculateDayScores_Calorie(CalorieList, analyzer);
                        }else if( strengthType.equals(StrengthInputType.CALORIE_SUM) ){
                            scoreClassList = Utils.CalculateDayScores_CalorieSUM(CalorieList, analyzer);
                        }else{
                            scoreClassList = Utils.CalculateDayScores_Strength(dayActivityList, analyzer);
                        }

                        for(int i=0;i<dayActivityList.size();i++)
                        {
                            DayActivity act = dayActivityList.get(i);
                            ScoreClass score= scoreClassList.get(i);
                            act.setPredayScore(score.preDayAchieve);
                            act.setTodayScore(score.todayGetPoint);
                            act.setExtraScore(score.todayPossibleMorePoint);
                            act.setAchieveMax(score.isAchieveMax);
                            act.setPossibleMaxScore(score.possibleMaxScore);
                            act.setPossibleTimes(score.possibleTimes);
                            act.setPossibleValue(score.possibleValue);
                            Log.d("recalcul_point", String.format("pre:%d, today:%d, extra:%d ", score.preDayAchieve, score.todayGetPoint, score.todayPossibleMorePoint));
                            this.mDBManager.setDayActivity(act);
                        }


                    }
                } catch (JSONException e) {
                    Log.d(TAG , e.getMessage());
                }
            }
             this.mActivityDataListener.onWeekActivityReceived(  );
        }else if( responseCode == GET_MONTHACTIVITY_RESPONSE_CODE ){

            if( resultCode == HttpRequest.SUCCESS ){
                try {

                    JSONArray jsonArray = new JSONArray( data );

                    for( int k = 0 ; k < jsonArray.length() ;k++ ) {

                        JSONObject jsonObject = jsonArray.getJSONObject(k);
                        String result = jsonObject.getString(CommonKey.RESPONSE_VALUE_KEY);

                        if (result.equals(CommonKey.SUCCESS)) {

                            WeekActivity weekActivity = WeekActivity.toWeekActivity(jsonObject);
                            this.mDBManager.setWeekActivity(weekActivity, FitmateDBManager.UPLOADED);

                            JSONArray jsonDayArray = jsonObject.getJSONArray("daysList");

                            List<DayActivity> dayActivityList = new ArrayList<DayActivity>();

                            for (int i = 0; i < jsonDayArray.length(); i++) {
                                JSONObject dayJsonObject = jsonDayArray.getJSONObject(i);
                                DayActivity dayActivity = DayActivity.toDayActivity(dayJsonObject);

                                dayActivityList.add(dayActivity);
                            }

                            List<Integer>CalorieList = new ArrayList<Integer>();
                            for(int i=0;i<dayActivityList.size();i++){
                                CalorieList.add((dayActivityList.get(i).getCalorieByActivity()));
                            }

                            ExerciseProgram  exerciseProgram = this.mDBManager.getUserExerciProgram( weekActivity.getExerciseProgramID() );
                            UserInfo userInfo = mDBManager.getUserInfo();

                            UserInfoForAnalyzer userInfoForAnalyzer = com.fitdotlife.fitmate_lib.service.util.Utils.getUserInfoForAnalyzer(userInfo);
                            com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseProgram program = Utils.ConvertExerciseProgram(exerciseProgram);
                            ExerciseAnalyzer analyzer = new ExerciseAnalyzer(program, userInfoForAnalyzer, WearingLocation.WRIST, 10);
                            StrengthInputType strengthType = program.getStrengthInputType();

                            List<ScoreClass> scoreClassList = null;
                            if( strengthType.equals( StrengthInputType.CALORIE ) || strengthType.equals(StrengthInputType.VS_BMR) ) {
                                scoreClassList = Utils.CalculateDayScores_Calorie(CalorieList, analyzer);
                            }else if( strengthType.equals(StrengthInputType.CALORIE_SUM) ){
                                scoreClassList = Utils.CalculateDayScores_CalorieSUM(CalorieList, analyzer);
                            }else{
                                scoreClassList = Utils.CalculateDayScores_Strength(dayActivityList, analyzer);
                            }

                            for(int i=0;i<dayActivityList.size();i++){
                                DayActivity act = dayActivityList.get(i);
                                ScoreClass score= scoreClassList.get(i);
                                act.setPredayScore(score.preDayAchieve);
                                act.setTodayScore(score.todayGetPoint);
                                act.setExtraScore(score.todayPossibleMorePoint);
                                act.setAchieveMax(score.isAchieveMax);
                                act.setPossibleMaxScore(score.possibleMaxScore);
                                act.setPossibleTimes(score.possibleTimes);
                                act.setPossibleValue(score.possibleValue);
                                Log.d("recalcul_point", String.format("pre:%d, today:%d, extra:%d ", score.preDayAchieve, score.todayGetPoint, score.todayPossibleMorePoint));
                                this.mDBManager.setDayActivity(act);
                            }
                        }
                    }




                } catch (JSONException e) {
                    Log.d(TAG , e.getMessage());
                }
            }
             this.mActivityDataListener.onMonthActivityReceived(  );
        }
    }
}
