package com.fitdotlife.fitmate.model;

/**
 * Created by Joshua on 2015-01-29.
 */
public interface ActivityDataListener {

    void onDayActivityReceived( );
    void onWeekActivityReceived(  );
    void onMonthActivityReceived(  );
    void onUploadedData( boolean isUpload );

}