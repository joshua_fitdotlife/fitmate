package com.fitdotlife;

import com.fitdotlife.fitmate_lib.http.NetworkClass;

/**
 * Created by fitlife.soondong on 2015-09-04.
 */
public class Preprocess {
    /**
     * 테스트 버전일때는 true, 배포할때는 반드시 false로 수정한다.
     */
    public static final boolean IsTest =false;

    /**
     * 테스트 버전인 경우 변경되어야 하는 사항들 처리
     */
    public static void ChangeToTestVersion(){
        if(IsTest==true){
            final String testUrl= "http://testfitmateapi.azurewebsites.net/";
            NetworkClass.baseURL =testUrl;
            final  String imageBaseURL = "https://fitmeter.blob.core.windows.net/testuserprofileimage/";
            NetworkClass.imageBaseURL= imageBaseURL;
        }
    }
}
