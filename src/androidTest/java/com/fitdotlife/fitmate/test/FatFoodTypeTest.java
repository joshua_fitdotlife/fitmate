package com.fitdotlife.fitmate.test;

import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.SmallTest;
import static org.junit.Assert.assertTrue;

import com.fitdotlife.fitmate_lib.key.FatFoodType;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Created by Joshua on 2016-03-22.
 */
@RunWith(AndroidJUnit4.class)
@SmallTest
public class FatFoodTypeTest
{
    @Test
    public void selectFatFoodTypeTest()
    {
        FatFoodType fatFoodType = FatFoodType.selectFatFoodType(39);
        assertTrue( fatFoodType.ordinal() <= 3 );
    }
}
