package org.apache.log4j;

/**
 * Created by wooseok on 2015-05-12.
 */
public class Log
{

    static Logger l = null;

    public static void setLogger(Logger ll)
    {
        l = ll;
    }

    public Log(Logger l)
    {
        setLogger(l);
    }

    public static void MakeLog(String name)
    {
        setLogger(LogManager.getLogger(name));
    }

    public static Log getLogger(Class clazz)
    {
        Logger l = LogManager.getLogger(clazz.getName());
        Log ll = new Log(l);

        return ll;
    }

    public static int e(String tag, String msg)
    {
        if(l != null) {
            l.error(CurrentText(tag, msg));
        }
        return 0;
    }

    public static int d(String tag, String msg)
    {
        if(l != null) {
            l.debug(CurrentText(tag, msg));
        }

        return 0;
    }

    public static String CurrentText(String tag, String msg)
    {
        StackTraceElement[] ste = Thread.currentThread().getStackTrace();
        StackTraceElement cur = ste[4];
        String className =cur.getClassName();
        String[] name = className.split("\\.");
        String Name = name[name.length - 1];
        return String.format("%s/%s[%d]\t %s\t %s",Name ,cur.getMethodName(),cur.getLineNumber(), tag, msg);
    }


    public static int i(String tag, String msg) {
        if(l != null) {
            l.info(CurrentText(tag, msg));
        }
        return 0;
    }

    public static int t(String tag, String msg)
    {
        if(l != null) {
            l.trace(CurrentText(tag, msg));
        }
        return 0;
    }

    public static int v(String tag, String msg)
    {
        if(l != null) {
            l.debug(CurrentText(tag, msg));
        }
        return 0;
    }

    public static int w(String tag, String msg)
    {
        if(l != null) {
            l.warn(CurrentText(tag, msg));
        }
        return 0;
    }
}