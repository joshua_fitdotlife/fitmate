package com.fitdotlife.fitmate_lib.iview;

import android.content.Intent;

import com.fitdotlife.fitmate_lib.object.DayActivity;
import com.fitdotlife.fitmate_lib.object.ExerciseProgram;
import com.fitdotlife.fitmate_lib.object.MonthActivity;
import com.fitdotlife.fitmate_lib.object.WeekActivity;

/**
 * Created by Joshua on 2015-01-29.
 */
public interface IMainView {
    void displayDayActivity(DayActivity dayActivity,  int weekScore, int year, int month, int day, int dayOfWeek, ExerciseProgram appliedProgram);
    void updateDayActivity( DayActivity dayActivity , int weekScore , ExerciseProgram appliedProgram );
    void displayWeekActivity( WeekActivity weekActivity, int[] arrExerciseTime , int[] arrCalorie , String[] arrDayDate  ,int year, int month, int day, int weekNumber, ExerciseProgram appliedProgram);
    void updateWeekActivity(WeekActivity weekActivity, int[] arrExerciseTime, int[] arrCalorie, ExerciseProgram appliedProgram);
    void displayMonthActivity(MonthActivity monthActivity , int year, int month, int day, ExerciseProgram appliedProgram);
    void updateMonthActivity(MonthActivity monthActivity, ExerciseProgram appliedProgram);
    void showResult(String message);
    void moveOtherActivity(Intent intent);

}
