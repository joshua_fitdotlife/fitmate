package com.fitdotlife.fitmate_lib.iview;

/**
 * Created by Joshua on 2015-04-02.
 */
public interface IMyProfileView {
    void showDeviceChangeActivity();
    void showWearingLocationActivity();
    void setShowing(boolean showing);
    void dismissProgress();
    void setProgressText( String progressText );
}
