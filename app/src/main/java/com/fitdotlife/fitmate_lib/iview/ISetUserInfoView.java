package com.fitdotlife.fitmate_lib.iview;

/**
 * Created by Joshua on 2015-01-29.
 */
public interface ISetUserInfoView {
    void moveHomeActivity();
    void showResult( String message );
    void showPopup(int code , String message);
    void finishActivity();
    void startProgressDialog(String title, String message);
    void stopProgressDialog();
    void setDuplicateResult( boolean duplicateResult );
}
