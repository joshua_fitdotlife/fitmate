package com.fitdotlife.fitmate_lib.iview;

public interface ISetExerciseProgramView 
{
	public void showResult( String message );
	public void finishActivity();
}
