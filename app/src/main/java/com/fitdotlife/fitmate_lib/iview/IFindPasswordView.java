package com.fitdotlife.fitmate_lib.iview;

import com.fitdotlife.fitmate_lib.object.UserInfo;

public interface IFindPasswordView
{
	void navigateToSearchDialog( UserInfo userInfo);
	void showResult(String message);
	void dismisDialog(  );

	void startProgressDialog();
	void stopProgressDialog();
}
