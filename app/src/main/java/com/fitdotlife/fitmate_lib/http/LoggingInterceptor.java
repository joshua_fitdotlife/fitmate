package com.fitdotlife.fitmate_lib.http;

import org.apache.log4j.Log;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

/**
 * Created by Joshua on 2015-10-23.
 */
public class LoggingInterceptor implements ClientHttpRequestInterceptor{

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException
    {

        ClientHttpRequestFactory requestFactory = new BufferingClientHttpRequestFactory( new SimpleClientHttpRequestFactory() );
        ClientHttpRequest clientHttpRequest = requestFactory.createRequest( request.getURI() , request.getMethod() );

        if( body != null ){
            Log.d("fitmate", "body 설정");

            OutputStream os = clientHttpRequest.getBody();
            os.write(body , 0, body.length);
            os.flush();
    }

        logRequest(clientHttpRequest);
        ClientHttpResponse response = clientHttpRequest.execute();
        logResponse(response);
        return response;
    }

    private void logRequest(HttpRequest request) {
        Log.d( "fitmate" , "HTTP REQUEST URI : " + request.getURI().toString()  );
    }

    private void logResponse(ClientHttpResponse response) {

        String data = null;
        StringBuilder sb = new StringBuilder();
        try {

            BufferedReader buf = new BufferedReader(new InputStreamReader(response.getBody(), "UTF-8"));

            int character;
            while ((character = buf.read()) != -1) {
                sb.append((char) character);
                String.valueOf(character);
            }

        }catch(IOException e){
            e.getMessage();
        }

        data = sb.toString();
        if(data != null) {
            Log.d("fitmate", "HTTP RESPONSE : " + data);
        }
    }
}
