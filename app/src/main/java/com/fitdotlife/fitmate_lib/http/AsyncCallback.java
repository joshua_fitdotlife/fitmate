package com.fitdotlife.fitmate_lib.http;

/**
 * Created by fitlife.soondong on 2015-03-09.
 */
public interface AsyncCallback<T> {
    public void onResult(T result);




    public void exceptionOccured(Exception e);




    public void cancelled();
}
