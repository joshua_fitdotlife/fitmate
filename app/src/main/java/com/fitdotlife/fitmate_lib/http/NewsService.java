package com.fitdotlife.fitmate_lib.http;

import com.fitdotlife.fitmate_lib.object.News;
import com.fitdotlife.fitmate_lib.object.NewsResult;

import org.androidannotations.rest.spring.annotations.Get;
import org.androidannotations.rest.spring.annotations.Path;
import org.androidannotations.rest.spring.annotations.Rest;
import org.androidannotations.rest.spring.api.RestClientErrorHandling;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import java.util.List;

/**
 *
 * Created by Joshua on 2015-07-28.
 *
 */
@Rest( converters = {MappingJackson2HttpMessageConverter.class})
public interface NewsService extends RestClientErrorHandling {

    @Get("/news/MyNewsList/{email}/")
    List<News> getMyNewsList(@Path String email);

    @Get("/news/FriendNewsList/{email}/")
    List<News> getFriendNewsList(@Path String email);

    void setRootUrl(String rootUrl);

   @Get("/News/news/{email}/?loadingTimes={loadingTimes}")
    NewsResult getNewsList( @Path String email , @Path int loadingTimes );
    
}