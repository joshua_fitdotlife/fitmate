package com.fitdotlife.fitmate_lib.http;

import android.os.Handler;

import java.util.WeakHashMap;

public class FitLifeHttpClientThread extends Thread
{
	private String mDataUri;
	private Handler mHandler;
	private WeakHashMap<String, String> mParameters = null;
	private String mMethod;
	
	public FitLifeHttpClientThread( String dataUri , String method , WeakHashMap<String , String> parameters , Handler handler )
	{
		this.mDataUri = dataUri;
		this.mHandler = handler;
		this.mParameters = parameters;
		this.mMethod = method;
	}
	
	public FitLifeHttpClientThread( String dataUri , String method , Handler handler)
	{
		this.mDataUri = dataUri;
		this.mHandler = handler;
		this.mMethod = method;
	}
	
	@Override
	public void run()
	{
		FitLifeHttpClient client = new FitLifeHttpClient();
		String data = client.getDataFromService(mDataUri, mMethod, mParameters);
		this.mHandler.obtainMessage( HttpRequest.RECEIVE , data ).sendToTarget();

        //this.mHandler.obtainMessage( HttpRequest.RECEIVE , "{ responsevalue:SUCCESS}" ).sendToTarget();

	}
	
}
