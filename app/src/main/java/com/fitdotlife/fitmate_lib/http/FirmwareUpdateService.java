package com.fitdotlife.fitmate_lib.http;

import com.fitdotlife.fitmate_lib.object.DayActivity_Rest;
import com.fitdotlife.fitmate_lib.object.FirmwareVersionInfo;

import org.androidannotations.rest.spring.annotations.Get;
import org.androidannotations.rest.spring.annotations.Path;
import org.androidannotations.rest.spring.annotations.Rest;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

/**
 * Created by Joshua on 2016-06-30.
 */
@Rest( converters = {MappingJackson2HttpMessageConverter.class})
public interface FirmwareUpdateService {

    @Get("/FirmwareUpdate/getLastVersion?isDebugVersion={isDebugVersion}&modelnumer={modelnumber}")
    FirmwareVersionInfo getLastVersion(@Path boolean isDebugVersion, @Path String modelnumber);

    void setRootUrl(String rootUrl);
}