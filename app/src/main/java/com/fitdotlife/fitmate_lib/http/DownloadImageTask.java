package com.fitdotlife.fitmate_lib.http;

import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.URL;

public class DownloadImageTask extends AsyncTask<String, Void, Drawable>
{

	private String uri;
	private final WeakReference<ImageView> imageViewReference;
	
	public DownloadImageTask( ImageView imageView )
	{
		imageViewReference = new WeakReference<ImageView>( imageView );
	}
	
	@Override
	protected Drawable doInBackground(String... params )
	{
		uri = params[0];
		Drawable drawable = this.ImageOperations(uri);
		return drawable;
	}
	
	@Override
	protected void onPostExecute( Drawable drawable )
	{
		if( isCancelled() )
		{
			drawable = null;
		}
		
		if( this.imageViewReference != null )
		{
			ImageView imageView = this.imageViewReference.get();
			imageView.setImageDrawable(drawable);
		}
	}
	
	private Drawable ImageOperations( String url )
	{
		try {
			InputStream is = (InputStream) this.fetch(url);
			Drawable d = Drawable.createFromStream(is, "src");
			return d;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	private Object fetch( String address ) throws IOException
	{
		URL url = new URL(address);
		Object content = url.getContent();
		return content;
	}
}
