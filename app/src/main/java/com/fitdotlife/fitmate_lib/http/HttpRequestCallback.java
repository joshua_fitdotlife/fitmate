package com.fitdotlife.fitmate_lib.http;

public interface HttpRequestCallback
{
	public void onHttpResponse(String data, int responseCode, int resultCode);
}
