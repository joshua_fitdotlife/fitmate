package com.fitdotlife.fitmate_lib.http;

import com.fitdotlife.fitmate_lib.object.DayActivity_Rest;
import com.fitdotlife.fitmate_lib.object.FriendDayActivity;
import com.fitdotlife.fitmate_lib.object.FriendWeekActivity;
import com.fitdotlife.fitmate_lib.object.WeekActivityWithDayActivityList;
import com.fitdotlife.fitmate_lib.object.WeekActivity_Rest;

import org.androidannotations.rest.spring.annotations.Get;
import org.androidannotations.rest.spring.annotations.Path;
import org.androidannotations.rest.spring.annotations.Post;
import org.androidannotations.rest.spring.annotations.Rest;
import org.androidannotations.rest.spring.api.RestClientErrorHandling;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import java.util.List;

/**
 * Created by Joshua on 2015-08-25.
 */
@Rest( converters = {MappingJackson2HttpMessageConverter.class})
public interface ActivityService extends RestClientErrorHandling {

    @Get("/activity/GetFriendDayActivity/{email}/?friendEmail={friendEmail}&activityDate={activityDate}")
    DayActivity_Rest getDayActivity(@Path String email , @Path String friendEmail , @Path String activityDate );

    @Get("/activity/GetFriendWeekActivity/{email}/?friendEmail={friendEmail}&activityDate={activityDate}")
    WeekActivity_Rest getWeekActivity(@Path String email , @Path String friendEmail , @Path String activityDate );

    @Get("/activity/GetFriendDayActivityList/{email}/?friendEmail={friendEmail}&startDate={startDate}&endDate={endDate}")
    List<DayActivity_Rest> getDayActivityList(@Path String email ,@Path  String friendEmail , @Path String startDate , @Path String endDate );

    @Get("/activity/GetFriendMonthActivity/{email}/?friendEmail={friendEmail}&startDate={startDate}&endDate={endDate}")
    List<WeekActivity_Rest> getMonthActivity(@Path String email , @Path String friendEmail , @Path String startDate , @Path String endDate );

    @Get("/WeekInfoWithDaysList/getWeekActivityWithDaysList_v_2/{email}/?miliseconds={miliseconds}")
    WeekActivityWithDayActivityList getWeekActivityWithDayActivityList(@Path String email , @Path String miliseconds );

    @Post("/FriendHomeData/DayAchieveListOfWeek?useremail={useremail}&friendemail={friendemail}&kindOfData={kindOfData}&weekStartDate={weekStartDate}&containDateRange={containDateRange}")
    FriendWeekActivity getFriendWeekActivity(@Path String useremail , @Path String friendemail , @Path int kindOfData ,@Path String weekStartDate , @Path boolean containDateRange );

    @Post("/FriendHomeData/DayDataOfFriend?useremail={useremail}&friendemail={friendemail}&date={date}&containDateRange={containDateRange}")
    FriendDayActivity getFriendDayActivity(@Path String useremail , @Path String friendemail , @Path String date , @Path boolean containDateRange );

    void setRootUrl(String rootUrl);
}
