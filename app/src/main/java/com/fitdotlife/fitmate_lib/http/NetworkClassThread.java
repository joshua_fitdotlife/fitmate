package com.fitdotlife.fitmate_lib.http;

import android.content.Context;
import android.os.Handler;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;

import java.io.UnsupportedEncodingException;

/**
 * Created by Joshua on 2015-05-08.
 */
public class NetworkClassThread extends Thread{

    private String mUrl = null;
    private String mJSONString = null;
    private Handler mResponseHandler = null;
    private String mMethod = null;

    public NetworkClassThread( Context context ,  String url , String jsonString , int responseCode , HttpRequestCallback callback ){
        this.mUrl = url;
        this.mJSONString = jsonString;
        this.mResponseHandler = new ResponseHandler( context , responseCode , callback );
    }

    public NetworkClassThread(Context context, String url, int responseCode, String method , HttpRequestCallback callback) {
        this.mUrl = url;
        this.mResponseHandler = new ResponseHandler( context , responseCode , callback );
        this.mMethod = method;
    }

    public void run(){
        NetworkClass networkClass = new NetworkClass();
        String data = null;
        try {
            if( this.mJSONString != null ) {
                data = networkClass.postStringFromUrl(mUrl, this.mJSONString);
            }else if( this.mJSONString == null ){
                if( mMethod.equals( HttpPost.METHOD_NAME ) ){
                    data = networkClass.postStringFromUrl(mUrl);
                }else if( mMethod.equals( HttpGet.METHOD_NAME ) ) {
                    data = networkClass.getStringFromUrl(mUrl);
                }
            }

        } catch (UnsupportedEncodingException e) {

        }

        this.mResponseHandler.obtainMessage( HttpRequest.RECEIVE , data ).sendToTarget();
    }
}