package com.fitdotlife.fitmate_lib.http;

import android.util.Log;

import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseProgram;
import com.fitdotlife.fitmate_lib.object.DayActivity;
import com.fitdotlife.fitmate_lib.object.LogInfo;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.object.WeekActivity;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.TimeUnit;

import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

/**
 * Created by Joshua on 2016-05-04.
 */
public class HttpApi {

    private static final String TAG = "HttpApi";

    private static Request.Builder getRequestBuilder(String url) {
//        Headers.Builder headerBuilder = new Headers.Builder();
        Request.Builder builder = new Request.Builder();
//        builder.headers(headerBuilder.build());
        builder.url(url);
        return builder;
    }

    public static void sendLog (LogInfo logInfo ,HttpResponse response)
    {
        try {
            String url = "http://fitlog.azurewebsites.net/api/log";

            Log.d(TAG, "url : " + url);
            RequestBody body = RequestBody.create(MediaType.parse("application/json"), logInfo.toJsonObject().toString() );
            Request.Builder request = getRequestBuilder(url);
            request.post(body);

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.build().newCall(request.build()).enqueue(response);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void getUserInfo( String id , String pwd , HttpResponse response ){
        try {
            String url = NetworkClass.baseURL + "GetUserInfo.aspx";
            String qry = String.format("?email=%s&pw=%s", id, pwd);
            url = url + qry;

            Request.Builder request = new Request.Builder();
            request.header("Authorization", "Client-ID " + ":DDD");
            request.url(url);

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.connectTimeout(10, TimeUnit.SECONDS);
            builder.writeTimeout(10, TimeUnit.SECONDS);
            builder.readTimeout(10, TimeUnit.SECONDS);
            builder.build().newCall(request.build()).enqueue(response);

        }catch( Exception e){
            e.printStackTrace();
        }
    }

    public static void changePassword( String email , String currentPassword , String newPassword , HttpResponse response ) throws IOException
    {
        String url = NetworkClass.baseURL + "ChangePassword.aspx?";
        url = url + String.format("email=%s&pw=%s&newpw=%s", email, currentPassword , newPassword);

        JSONObject passwordObject = new JSONObject();
        try {
            passwordObject.put( "email" , email );
            passwordObject.put("pw" , currentPassword);
            passwordObject.put("newpw" , newPassword);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d(TAG , "url : " + url );
        Log.d(TAG, "password : " + passwordObject.toString());

        RequestBody body = RequestBody.create(MediaType.parse("application/json"), passwordObject.toString() );
        Request.Builder request = getRequestBuilder(url);
        request.post(body);

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(10, TimeUnit.SECONDS);
        builder.writeTimeout(10, TimeUnit.SECONDS);
        builder.readTimeout(10, TimeUnit.SECONDS);
        builder.build().newCall(request.build()).enqueue(response);
    }

    public static void setUserAccount(  UserInfo userInfo , int programid , String fileName , HttpResponse response  )
    {
        try {
            String url = NetworkClass.baseURL + fileName;

            WeakHashMap<String, String> parameters = userInfo.toAccountParameters( programid );
            parameters.put("isTick", "false");

            StringBuilder sb = new StringBuilder();
            if (parameters != null) {

                List<NameValuePair> params = new ArrayList<NameValuePair>();
                Iterator<String> parametersIter = parameters.keySet().iterator();

                while (parametersIter.hasNext()) {
                    String key = parametersIter.next();
                    String val = parameters.get(key);

                    sb.append(String.format("%s=%s&", key, val));
                }
            }

            url = url + "?" + sb.toString();

            RequestBody body = body = RequestBody.create(null, "");
            Request.Builder request = new Request.Builder();
            request.header("Authorization", "Client-ID " + ":DDD");
            request.url(url);
            request.post(body);

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.connectTimeout(10, TimeUnit.SECONDS);
            builder.writeTimeout(10, TimeUnit.SECONDS);
            builder.readTimeout(10, TimeUnit.SECONDS);
            builder.build().newCall(request.build()).enqueue(response);

        }catch(Exception e){
            e.printStackTrace();
        }

    }
    public static void setUserInfo( UserInfo userInfo, int programid , String fileName ,HttpResponse response)
    {

        try {
            String url = NetworkClass.baseURL + fileName;

            WeakHashMap<String, String> parameters = userInfo.toAccountParameters(programid);
            parameters.put("isTick", "false");

            StringBuilder sb = new StringBuilder();
            if (parameters != null) {

                List<NameValuePair> params = new ArrayList<NameValuePair>();
                Iterator<String> parametersIter = parameters.keySet().iterator();

                while (parametersIter.hasNext()) {
                    String key = parametersIter.next();
                    String val = parameters.get(key);

                    sb.append(String.format("%s=%s&", key, val));
                }
            }

            url = url + "?" + sb.toString();

            RequestBody body = null;
            if (userInfo.getAccountPhoto() != null && !userInfo.getAccountPhoto().equals("")) {

                String sss = userInfo.getAccountPhoto();
                if (sss.startsWith("file://")) {
                    sss = (String) sss.subSequence(7, sss.length());
                }
                File file = new File(sss);
                body = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart("Files", file.getName(), RequestBody.create(MediaType.parse("image/jpg"), file))
                        .build();

            } else {
                body = RequestBody.create(null, "");
            }

            Request.Builder request = new Request.Builder();
            request.header("Authorization", "Client-ID " + ":DDD");
            request.url(url);
            request.post(body);

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.connectTimeout(10, TimeUnit.SECONDS);
            builder.writeTimeout(10, TimeUnit.SECONDS);
            builder.readTimeout(10, TimeUnit.SECONDS);
            builder.build().newCall(request.build()).enqueue(response);

        }catch(Exception e){
            e.printStackTrace();
        }

    }

    public static void chageUserInfo( UserInfo userInfo , HttpResponse response )
    {

        try {
            String url = NetworkClass.baseURL + "/ChangeUserInfo.aspx?";
            Map<String, Object> params = new HashMap<String, Object>();
            url += String.format("%s=%s&%s=%s", "email", userInfo.getEmail() , "pw", userInfo.getPassword() );

            RequestBody body = null;
            if ( userInfo.getAccountPhoto() != null && ! userInfo.getAccountPhoto().equals(""))
            {
                String sss = userInfo.getAccountPhoto();
                if (sss.startsWith("file://")) {
                    sss = (String) sss.subSequence(7, sss.length());
                }
                File file = new File(sss);
                if( file.exists() )
                {
                    body = new MultipartBody.Builder()
                            .setType(MultipartBody.FORM)
                            .addFormDataPart("Files", file.getName(), RequestBody.create(MediaType.parse("image/jpg"), file))
                            .build();
                }else{
                    body = RequestBody.create(null, "");
                }
            } else {
                body = RequestBody.create(null, "");
            }

            Request.Builder request = new Request.Builder();
            request.header("Authorization", "Client-ID " + ":DDD");
            request.url(url);
            request.post(body);

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.connectTimeout(10, TimeUnit.SECONDS);
            builder.writeTimeout(10, TimeUnit.SECONDS);
            builder.readTimeout(10, TimeUnit.SECONDS);
            builder.build().newCall(request.build()).enqueue(response);

        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public static void chageUserInfo( String email , String deviceAddress , int wearingLocation  ,HttpResponse response )
    {
        try {
            String url = NetworkClass.baseURL + "ChangeUserInfo.aspx?";
            url = url + UserInfo.EMAIL_KEY + "=" + email + "&" + UserInfo.BTADDRESS_KEY + "=" + deviceAddress + "&" + UserInfo.WEARING_LOCATION_KEY + "=" + wearingLocation + "&" + "isTick=false" ;

            Log.e( TAG , url );

            RequestBody body = RequestBody.create(null, "");
            Request.Builder request = new Request.Builder();
            request.header("Authorization", "Client-ID " + ":DDD");
            request.url(url);
            request.post(body);

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.connectTimeout(10, TimeUnit.SECONDS);
            builder.writeTimeout(10, TimeUnit.SECONDS);
            builder.readTimeout(10, TimeUnit.SECONDS);
            builder.build().newCall(request.build()).enqueue(response);

        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public static void uploadData( String uploadDataString , String userEmail , HttpResponse response )
    {
        try {
            String url = NetworkClass.baseURL + "api/SyncUpload/postupLoadActivityData_v_2/" + userEmail +"/";

            RequestBody body = RequestBody.create( MediaType.parse("application/json; charset=utf-8") , uploadDataString );
            Request.Builder request = new Request.Builder();
            request.header("Authorization", "Client-ID " + ":DDD");
            request.url(url);
            request.post(body);

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.connectTimeout(10, TimeUnit.SECONDS);
            builder.writeTimeout(10, TimeUnit.SECONDS);
            builder.readTimeout(10, TimeUnit.SECONDS);
            builder.build().newCall(request.build()).enqueue(response);

        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public static void getExerciseProgram( int programID , String language , HttpResponse response ){
        try {
            String url = NetworkClass.baseURL + "api/ExerciseProgram/getExerciseProgram?programID="+ programID +"&language=" + language;

            Request.Builder request = new Request.Builder();
            request.header("Authorization", "Client-ID " + ":DDD");
            request.url(url);

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.connectTimeout(10, TimeUnit.SECONDS);
            builder.writeTimeout(10, TimeUnit.SECONDS);
            builder.readTimeout(10, TimeUnit.SECONDS);
            builder.build().newCall(request.build()).enqueue(response);

        }catch(Exception e){
            e.printStackTrace();
        }
    }


}
