package com.fitdotlife.fitmate_lib.http;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;

import org.apache.log4j.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Created by Joshua on 2016-05-04.
 */
public abstract class HttpResponse implements Callback {
    private final String TAG = "HttpResponse";

    private Handler handler = new Handler(Looper.getMainLooper()){
        @Override
        public void handleMessage(Message msg) {
            onResponse(msg.what, (String) msg.obj);
        }
    };

    @Override
    public void onFailure(final Call call, final IOException e) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                handler.sendMessage(handler.obtainMessage(0, e.getMessage()));

                Log.d(TAG, call.request().url().toString() + "\n => onFailure() : " + e.getMessage());
                e.printStackTrace();

            }
        });
    }

    private String resStr = "{}";

    @Override
    public void onResponse(Call call, final Response response) throws IOException {

        if( response == null ){return;}

        Log.d(TAG, "onResponse : " + response.request().url().toString());

        try {
            resStr = response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(TextUtils.isEmpty(resStr)) {
            resStr = response.message();
        }

        Log.d(TAG, response.request().url().toString() + "\n => " + resStr);

        handler.post(new Runnable()
        {
            @Override
            public void run() {
                handler.sendMessage(handler.obtainMessage(response.code() , resStr));
            }
        });
    }

    abstract public void onResponse(int resultCode, String response);

}
