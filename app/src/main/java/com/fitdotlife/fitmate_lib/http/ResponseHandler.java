package com.fitdotlife.fitmate_lib.http;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

/**
 * Created by Joshua on 2015-05-08.
 */
public class ResponseHandler extends Handler
{
    private int mResponseCode;
    private HttpRequestCallback mCallback;
    private Context mContext;

    public ResponseHandler( Context context , int responseCode , HttpRequestCallback callback )
    {
        this.mCallback = callback;
        this.mResponseCode = responseCode;
        this.mContext = context;
    }

    @Override
    public void handleMessage( Message msg )
    {
        super.handleMessage(msg);
        if( msg.what == HttpRequest.RECEIVE )
        {
            String data = (String) msg.obj;
            int resultCode = HttpRequest.SUCCESS;

            if( data == null )
            {
                resultCode = HttpRequest.FAIL;
            }

            this.mCallback.onHttpResponse(data, mResponseCode, resultCode);
        }
    }

}
