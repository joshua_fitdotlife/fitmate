package com.fitdotlife.fitmate_lib.http;

import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

/**
 * Created by fitlife.soondong on 2015-02-27.
 */
public class NetworkClass
{

    //public final static String baseURL="http://localhost:64624/";
    //public final static String baseURL="http://192.168.0.21:19907/";
    //public final static String baseURL="http://125.209.192.99/fitmateSNU/";

//    public static String baseURL = "http://testfitmateapi.azurewebsites.net/";
//    public static String imageBaseURL = "https://fitmeter.blob.core.windows.net/testuserprofileimage/";
    public static String baseURL = "https://fitwapi.azurewebsites.net/";
    public static String imageBaseURL = "https://fitmeter.blob.core.windows.net/userprofileimage/";
    public static String firmwareURL = "https://fitmeter.blob.core.windows.net/firmware/";
    private final static String api_Incentive="api/IncentiveInfo";


    public static String getIncentiveInfo(String email, int numberOfDate) throws UnsupportedEncodingException{
        return getStringFromUrl_static(getApi_IncentiveURL_static(email, numberOfDate));
    }

    private static String getApi_IncentiveURL_static(String email, int numberOfDate){
        String result=baseURL+api_Incentive+"?email="+email+"&numberOfDate="+numberOfDate;

        return result;
    }

    // getInputStreamFromUrl : 주어진 URL 에 대한 입력 스트림(InputStream)을 얻는다.
    private InputStream getInputStreamFromUrl(String url) {
        InputStream contentStream = null;
        try {
            // HttpClient 를 사용해서 주어진 URL에 대한 입력 스트림을 얻는다.
            HttpParams params = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(params, 10000);
            HttpConnectionParams.setSoTimeout(params, 20000);

            HttpClient httpclient = new DefaultHttpClient(params);

            HttpResponse response = httpclient.execute(new HttpGet(url));
            contentStream = response.getEntity().getContent();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return contentStream;
    } // getInputStreamFromUrl

    private static InputStream getInputStreamFromUrl_static(String url) {
        InputStream contentStream = null;
        try {
            // HttpClient 를 사용해서 주어진 URL에 대한 입력 스트림을 얻는다.
            HttpParams params = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(params, 10000);
            HttpConnectionParams.setSoTimeout(params, 20000);

            HttpClient httpclient = new DefaultHttpClient( params );
            Log.d("connectionTimeLog", "start");
            HttpResponse response = httpclient.execute(new HttpGet(url));
            Log.d("connectionTimeLog", "end");
            contentStream = response.getEntity().getContent();
        } catch (Exception e) {
            Log.d("connectionTimeLog", "exception");
            e.printStackTrace();
        }
        return contentStream;
    } // getInputStreamFromUrl

    private InputStream PostInputStreamFromUrl(String url, JSONObject jsonObject) {
        InputStream contentStream = null;

        try {
            // HttpClient 를 사용해서 주어진 URL에 대한 입력 스트림을 얻는다.

            HttpParams params = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(params, 10000);
            HttpConnectionParams.setSoTimeout(params, 20000);

            HttpClient httpclient = new DefaultHttpClient(params);

            HttpPost httppost = new HttpPost(url);
            httppost.addHeader("Accept", "application/json");
            httppost.addHeader("Content-type", "application/json");

            httppost.setEntity(new ByteArrayEntity(jsonObject.toString().getBytes(("UTF8"))));

            /*StringEntity se = new StringEntity(jsonObject.toString() );*/
         /*   StringEntity se = new StringEntity( jsonObject.toString() );*/

       /*     httppost.setEntity(se);*/
            HttpResponse response = httpclient.execute(httppost);
            contentStream = response.getEntity().getContent();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return contentStream;
    } // getInputStreamFromUrl

    private InputStream PostInputStreamFromUrl( String url, String jsonString ) {
        InputStream contentStream = null;
        try {
            // HttpClient 를 사용해서 주어진 URL에 대한 입력 스트림을 얻는다.

            HttpParams params = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(params, 10000);
            HttpConnectionParams.setSoTimeout(params, 20000);

            HttpClient httpclient = new DefaultHttpClient( params );

            HttpPost httppost = new HttpPost(url);
            httppost.addHeader("Content-type", "application/json");

            Log.i("NetworkClass", "httppost url:"+url);
            Log.i("NetworkClass", "httppost params:" + jsonString);
            StringEntity entity = new StringEntity( jsonString ,"UTF-8");
            entity.setContentType("application/json");

            httppost.setEntity(entity);

            /*StringEntity se = new StringEntity(jsonObject.toString() );*/
         /*   StringEntity se = new StringEntity( jsonObject.toString() );*/

       /*     httppost.setEntity(se);*/
            HttpResponse response = httpclient.execute(httppost);


            contentStream = response.getEntity().getContent();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return contentStream;
    } // getInputStreamFromUrl

    private InputStream PostInputStreamFromUrl(String url) {
        InputStream contentStream = null;
        try {
            // HttpClient 를 사용해서 주어진 URL에 대한 입력 스트림을 얻는다.
            HttpParams params = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(params, 10000);
            HttpConnectionParams.setSoTimeout(params, 20000);

            HttpClient httpclient = new DefaultHttpClient( params );

            HttpPost httppost = new HttpPost(url);

            HttpResponse response = httpclient.execute(httppost);
            contentStream = response.getEntity().getContent();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return contentStream;
    } // getInputStreamFromUrl
    private InputStream deleteInputStreamFromUrl(String url) {
        InputStream contentStream = null;
        try {
            // HttpClient 를 사용해서 주어진 URL에 대한 입력 스트림을 얻는다.
            HttpClient httpclient = new DefaultHttpClient();

            HttpResponse response = httpclient.execute(new HttpDelete(url));
            contentStream = response.getEntity().getContent();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return contentStream;
    } // getInputStreamFromUrl

    private InputStream putInputStreamFromUrl(String url, JSONObject jsonObject) {
        InputStream contentStream = null;
        try {
            // HttpClient 를 사용해서 주어진 URL에 대한 입력 스트림을 얻는다.
            HttpParams params = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(params, 20000);
            HttpConnectionParams.setSoTimeout(params, 20000);

            HttpClient httpclient = new DefaultHttpClient( params );

            HttpPut httpPut = new HttpPut(url);

            httpPut.addHeader("Accept", "application/json");
            httpPut.addHeader("Content-type", "application/json");
            httpPut.setEntity(new ByteArrayEntity(jsonObject.toString().getBytes(("UTF8"))));
           /* StringEntity se = new StringEntity(jsonObject.toString());
            httpPut.setEntity(se );*/
            HttpResponse response = httpclient.execute(httpPut);
            contentStream = response.getEntity().getContent();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return contentStream;
    } // getInputStreamFromUrl
    public String putStringFromUrl(String url, JSONObject jsonObject)  {

        // 입력스트림을 "UTF-8" 를 사용해서 읽은 후, 라인 단위로 데이터를 읽을 수 있는 BufferedReader 를 생성한다.
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(putInputStreamFromUrl(url,jsonObject), "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        // 읽은 데이터를 저장한 StringBuffer 를 생성한다.
        StringBuffer sb = new StringBuffer();

        try {
            // 라인 단위로 읽은 데이터를 임시 저장한 문자열 변수 line
            String line = null;

            // 라인 단위로 데이터를 읽어서 StringBuffer 에 저장한다.
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sb.toString();
    } // postStringFromUrl

    public String deleteStringFromUrl(String url)  {

        // 입력스트림을 "UTF-8" 를 사용해서 읽은 후, 라인 단위로 데이터를 읽을 수 있는 BufferedReader 를 생성한다.

        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(deleteInputStreamFromUrl(url), "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        // 읽은 데이터를 저장한 StringBuffer 를 생성한다.
        StringBuffer sb = new StringBuffer();

        try {
            // 라인 단위로 읽은 데이터를 임시 저장한 문자열 변수 line
            String line = null;

            // 라인 단위로 데이터를 읽어서 StringBuffer 에 저장한다.
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        } catch (Exception e) {
            //e.printStackTrace();
        }
        return sb.toString();
    } // postStringFromUrl

    public static String getStringFromUrl_static(String url) throws UnsupportedEncodingException {

        // 입력스트림을 "UTF-8" 를 사용해서 읽은 후, 라인 단위로 데이터를 읽을 수 있는 BufferedReader 를 생성한다.
        BufferedReader br = new BufferedReader(new InputStreamReader(getInputStreamFromUrl_static(url), "UTF-8"));


        // 읽은 데이터를 저장한 StringBuffer 를 생성한다.
        StringBuffer sb = new StringBuffer();


        try {
            // 라인 단위로 읽은 데이터를 임시 저장한 문자열 변수 line
            String line = null;

            // 라인 단위로 데이터를 읽어서 StringBuffer 에 저장한다.
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        int length= sb.length();
        return sb.toString();
    } // postStringFromUrl
    public String getStringFromUrl(String url) throws UnsupportedEncodingException {

        StringBuffer sb = null;
        try {
            // 입력스트림을 "UTF-8" 를 사용해서 읽은 후, 라인 단위로 데이터를 읽을 수 있는 BufferedReader 를 생성한다.
            BufferedReader br = new BufferedReader(new InputStreamReader(getInputStreamFromUrl(url), "UTF-8"));

            // 읽은 데이터를 저장한 StringBuffer 를 생성한다.
            sb = new StringBuffer();


            // 라인 단위로 읽은 데이터를 임시 저장한 문자열 변수 line
            String line = null;

            // 라인 단위로 데이터를 읽어서 StringBuffer 에 저장한다.
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }  catch(NullPointerException npe){
            npe.printStackTrace();
            return null;
        }
        return sb.toString();
    } // postStringFromUrl
    // postStringFromUrl : 주어진 URL 페이지를 문자열로 얻는다.
    public String postStringFromUrl(String url, JSONObject jsonObject) {

        // 입력스트림을 "UTF-8" 를 사용해서 읽은 후, 라인 단위로 데이터를 읽을 수 있는 BufferedReader 를 생성한다.
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(PostInputStreamFromUrl(url, jsonObject), "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        // 읽은 데이터를 저장한 StringBuffer 를 생성한다.
        StringBuffer sb = new StringBuffer();

        try {
            // 라인 단위로 읽은 데이터를 임시 저장한 문자열 변수 line
            String line = null;

            // 라인 단위로 데이터를 읽어서 StringBuffer 에 저장한다.
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sb.toString();
    } // postStringFromUrl
    public String postStringFromUrl(String url, String jsonString) throws UnsupportedEncodingException {

        StringBuffer sb = null;
        // 입력스트림을 "UTF-8" 를 사용해서 읽은 후, 라인 단위로 데이터를 읽을 수 있는 BufferedReader 를 생성한다.
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader( PostInputStreamFromUrl(url, jsonString), "UTF-8"));

            // 읽은 데이터를 저장한 StringBuffer 를 생성한다.
            sb = new StringBuffer();

            // 라인 단위로 읽은 데이터를 임시 저장한 문자열 변수 line
            String line = null;

            // 라인 단위로 데이터를 읽어서 StringBuffer 에 저장한다.
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        } catch (Exception e) {
            return null;
        }

        return sb.toString();
    } // postStringFromUrl
    public String postStringFromUrl(String url) throws UnsupportedEncodingException {

        // 입력스트림을 "UTF-8" 를 사용해서 읽은 후, 라인 단위로 데이터를 읽을 수 있는 BufferedReader 를 생성한다.
        BufferedReader br = new BufferedReader(new InputStreamReader(PostInputStreamFromUrl(url), "UTF-8"));

        // 읽은 데이터를 저장한 StringBuffer 를 생성한다.
        StringBuffer sb = new StringBuffer();

        try {
            // 라인 단위로 읽은 데이터를 임시 저장한 문자열 변수 line
            String line = null;

            // 라인 단위로 데이터를 읽어서 StringBuffer 에 저장한다.
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sb.toString();
    } // postStringFromUrl
    /*public static String getJsonText() {

        // 내부적으로 문자열 편집이 가능한 StringBuffer 생성자
        StringBuffer sb = new StringBuffer();

        try {
            String line = postStringFromUrl("http://125.209.192.99/iCareD/api/MealStatus/1");

            // 원격에서 읽어온 데이터로 JSON 객체 생성
            //JSONObject object = new JSONObject(line);

            // "kkt_list" 배열로 구성 되어있으므로 JSON 배열생성
            JSONArray Array = new JSONArray(line);

            for (int i = 0; i < Array.length(); i++) {
                // bodylist 배열안에 내부 JSON 이므로 JSON 내부 객체 생성
                JSONObject insideObject = Array.getJSONObject(i);

                // StringBuffer 메소드 ( append : StringBuffer 인스턴스에 뒤에 덧붙인다. )
                // JSONObject 메소드 ( get.String(), getInt(), getBoolean() .. 등 : 객체로부터 데이터의 타입에 따라 원하는 데이터를 읽는다. )
                String statusName= insideObject.getString("StatusName");
                if(statusName!=null){ m_Adapter.add(statusName);}

            } // for
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sb.toString();
    } // getJsonText*/


}
