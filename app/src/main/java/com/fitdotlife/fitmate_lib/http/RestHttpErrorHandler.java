package com.fitdotlife.fitmate_lib.http;

import org.androidannotations.annotations.EBean;
import org.androidannotations.rest.spring.api.RestErrorHandler;
import org.apache.log4j.Log;
import org.springframework.core.NestedRuntimeException;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;

/**
 * Created by Joshua on 2015-11-30.
 */
@EBean
public class RestHttpErrorHandler implements RestErrorHandler {

    @Override
    public void onRestClientExceptionThrown(NestedRuntimeException e) {
        Log.e("fitmate", e.getMessage() );
    }
}
