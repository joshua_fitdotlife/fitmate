package com.fitdotlife.fitmate_lib.http;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.log4j.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.WeakHashMap;

public class FitLifeHttpClient 
{
	private String TAG = "fitmate";
	private final int TIMEOUT = 10000;
	
	public String getDataFromService( String uri , String method , WeakHashMap<String , String> parameters )
	{
		String data = null;
		try {

			HttpUriRequest uriRequest = null;
			if( method.equals( HttpPost.METHOD_NAME ) )
			{
				uriRequest = this.getHttpPost(uri, parameters);
			}
			else if( method.equals(HttpGet.METHOD_NAME) )
			{
				uriRequest = this.getHttpGet(uri, parameters);
			}

			HttpParams httpParams = new BasicHttpParams();
			HttpConnectionParams.setConnectionTimeout(httpParams, TIMEOUT);
			HttpConnectionParams.setSoTimeout(httpParams, TIMEOUT);

			HttpClient client = new DefaultHttpClient(httpParams);

			HttpResponse response = client.execute(uriRequest);
			InputStream ips = response.getEntity().getContent();
			BufferedReader buf = new BufferedReader(new InputStreamReader( ips , "UTF-8" ));
			
			StringBuilder sb = new StringBuilder();
			
			int character;
			while( ( character = buf.read() ) != -1 )
			{
				sb.append((char)character);
				String.valueOf(character);
			}
			
			ips.close();
			data = sb.toString();
			
		} catch (ClientProtocolException e) {
			Log.e( "fitmate" ,  e.getMessage() );
			data = null;
        } catch (IOException e) {
			Log.e( "fitmate" ,  e.getMessage() );
			data = null;
		}
		
		return data;
	}
	
	private HttpUriRequest getHttpPost( String uri , WeakHashMap<String , String> parameters ) throws UnsupportedEncodingException
	{
		HttpPost post = new HttpPost( uri );
		
		if( parameters != null )
		{
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			Iterator<String> parametersIter = parameters.keySet().iterator();
			
			while( parametersIter.hasNext() )
			{
				String key = parametersIter.next();
				params.add( new BasicNameValuePair( key , parameters.get(key) ));
			}

			UrlEncodedFormEntity ent = new UrlEncodedFormEntity( params , HTTP.UTF_8 );
			post.setEntity(ent);
		}
		
		return post;
	}
	
	private HttpUriRequest getHttpGet( String uri , WeakHashMap<String , String> parameters )
	{
		HttpGet get;
		
		if(parameters != null)
		{
			StringBuilder sb = new StringBuilder();
			sb.append(uri);
			sb.append( "?" );
			
			for( String item : parameters.keySet() )
			{
				sb.append( item + "=" + parameters.get(item) + "&" );
			}
			
			get = new HttpGet(sb.toString());
			
		}
		else 
		{
			get = new HttpGet(uri);
		}
		
		return get;
	} 
	
}
