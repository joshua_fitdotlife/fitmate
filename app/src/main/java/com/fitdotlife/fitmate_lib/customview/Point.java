package com.fitdotlife.fitmate_lib.customview;

public class Point 
{
	protected float x ,y;
	protected float dx, dy;
	
	
	public Point( float x , float y )
	{
		this.x = x;
		this.y = y;
	}
	
	@Override
	public String toString() {
		return x + ", " + y;
	}
}
