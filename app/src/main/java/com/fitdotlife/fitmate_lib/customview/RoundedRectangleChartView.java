package com.fitdotlife.fitmate_lib.customview;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.util.TypedValue;
import android.view.View;

import com.fitdotlife.fitmate.R;

public class RoundedRectangleChartView extends View {

	private Context mContext = null;
	
	private int mTopBlank = 0;
	private int mBottomBlank = 0;
	private int mLeftBlank = 0;
	private int mRightBlank = 0;
	
	private int defaultSpace = 0;
	private int extraBalnk = 0;
	
	private int heightSize = 0;
	private int widthSize = 0;
	
	private Path barPath = null;
	
	private Paint leftRectangelPaint = null;
	private Paint rightRectangelPaint = null;
    private Paint textPaint = null;
    private Paint valueTextPaint = null;
	
	private int capRadius = 0;

    private int textSize = 0;
    private int valueTextSize = 0;
    private int imageHeightSize = 0;
    private int imageWidthSize = 0;

    private int mExerciseTime = 0;
    private double mAverageMET = 0;

    private String mLeftUpText = null;
    private String mLeftDownText = null;
    private String mRightUpText = null;
    private String mRightDownText = null;

    private float mTextGapHeight=0;

	public RoundedRectangleChartView(Context context) {
		super(context);
		
		this.mContext = context;
		this.init();
	}

	private void init()
	{
        this.valueTextSize = this.getResources().getDimensionPixelSize(R.dimen.day_oval_chart_value_text_size);
        this.textSize = this.getResources().getDimensionPixelSize(R.dimen.day_oval_chart_text_size);
        this.imageHeightSize= this.getResources().getDimensionPixelSize( R.dimen.day_oval_chart_image_height_size );
        this.imageWidthSize = this.getResources().getDimensionPixelSize(R.dimen.day_oval_chart_image_width_size);

		this.barPath = new Path();
		
		this.leftRectangelPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		this.leftRectangelPaint.setColor( this.mContext.getResources().getColor(R.color.fitmate_orange ) );
		
		this.rightRectangelPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		this.rightRectangelPaint.setColor( this.mContext.getResources().getColor(R.color.fitmate_green ) );

        this.textPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.textPaint.setTextSize(  this.textSize );
        this.textPaint.setColor(Color.WHITE);

        this.valueTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.valueTextPaint.setTextSize(this.valueTextSize);
        this.valueTextPaint.setColor(Color.WHITE);

        this.mTextGapHeight= TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 6, this.getResources().getDisplayMetrics());
	}
	
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) 
	{
		int heightMode = MeasureSpec.getMode(heightMeasureSpec);
		switch( heightMode )
		{
		case MeasureSpec.UNSPECIFIED:
			heightSize = heightMeasureSpec;
			break;
		case MeasureSpec.AT_MOST:
			heightSize = 200;
			break;
		case MeasureSpec.EXACTLY:
			heightSize = MeasureSpec.getSize(heightMeasureSpec);
			break;
		}
		
		int widthMode = MeasureSpec.getMode(widthMeasureSpec);
		switch( widthMode )
		{
		case MeasureSpec.UNSPECIFIED:
			widthSize = widthMeasureSpec;
			break;
		case MeasureSpec.AT_MOST:
			widthSize = 300;
			break;
		case MeasureSpec.EXACTLY:
			widthSize = MeasureSpec.getSize(widthMeasureSpec);
			break;
		}
		
		this.setMeasuredDimension(widthSize, heightSize);
	}
	
	@Override
	protected void onDraw(Canvas canvas)
	{
		capRadius = this.getHeight() / 2;
		
		this.defaultSpace = ( this.getMeasuredWidth() - ( capRadius * 4 ) ) / 11;
		this.extraBalnk = ( this.getMeasuredWidth() - ( capRadius * 4 ) ) % 11;
		this.mLeftBlank = this.defaultSpace;
		
		drawLeftRoundedRectangle( canvas , capRadius );
		drawRightRoundedRectangle( canvas , capRadius );
		drawLeftContent( canvas );
		drawRightContent( canvas );
	}
	
	private void drawLeftContent(Canvas canvas)
	{

		float left = this.mLeftBlank;
		float top = 0;
		float right = this.mLeftBlank + ( this.defaultSpace * 4 ) + ( capRadius * 2 );
		float bottom = this.getMeasuredHeight();

		int contentWidth = ( this.defaultSpace * 4 ) + ( capRadius * 2 );
		int contentHeight = this.getMeasuredHeight();

		//int widthSpace = contentWidth / 30;

		float textsize = textPaint.getTextSize();
		float gap = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 2.33f, this.getResources().getDisplayMetrics());
		Bitmap src = BitmapFactory.decodeResource(this.mContext.getResources(), R.drawable.icon_time);
        Bitmap resizeBitmap = Bitmap.createScaledBitmap(src, this.imageHeightSize, this.imageHeightSize, false);
		float bitmapLeft=  TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 12.67f, this.getResources().getDisplayMetrics());
		float bitmapRight=  TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10f, this.getResources().getDisplayMetrics());

		float y = contentHeight/2;
	//	int top  =  (this.getMeasuredHeight()  - resizeBitmap.getHeight())/2;
		canvas.drawBitmap(resizeBitmap, this.mLeftBlank + bitmapLeft, y - this.imageHeightSize, this.leftRectangelPaint);
		canvas.drawText(this.mLeftDownText, this.mLeftBlank + resizeBitmap.getWidth() + bitmapLeft +bitmapRight , y, textPaint );

		y-=(gap+textsize);

		canvas.drawText(this.mLeftUpText, this.mLeftBlank + bitmapLeft + resizeBitmap.getWidth() + bitmapRight, y, textPaint);

		String valueString= String.valueOf(this.mExerciseTime + " " + this.getResources().getString(R.string.common_minute));
		int valueTextWidth = this.getTextWidth(valueString, this.valueTextPaint);
		int valueTextHeight = this.getTextHeight(valueString , this.valueTextPaint);

		y =0.75f*(top+bottom);


		float x=(left+right)/2 - valueTextWidth/2;

        canvas.drawText(valueString , x , y+valueTextHeight/2 , this.valueTextPaint );
	}

	private void drawRightContent(Canvas canvas)
	{
		/*int contentWidth = ( this.defaultSpace * 4 ) + ( capRadius * 2 );
		int contentHeight = this.getMeasuredHeight();
		
		int widthSpace = contentWidth / 30;
		float y = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 15, this.getResources().getDisplayMetrics());
		
		Bitmap src = BitmapFactory.decodeResource(this.mContext.getResources(), R.drawable.icon_met);
        Bitmap resizeBitmap = Bitmap.createScaledBitmap( src , this.imageWidthSize , this.imageHeightSize , false );
		int top  = ( this.getMeasuredHeight() / 2 ) - resizeBitmap.getHeight();
		if( top < 0 ) top = 0;
		canvas.drawBitmap(resizeBitmap, this.mLeftBlank + ( this.defaultSpace * 4 )  + ( capRadius * 2 ) + this.defaultSpace + this.extraBalnk + ( widthSpace * 2 ) , top , this.leftRectangelPaint );
		
		canvas.drawText(this.mRightUpText, this.mLeftBlank + (this.defaultSpace * 4) + (capRadius * 2) + this.defaultSpace + this.extraBalnk + widthSpace + resizeBitmap.getWidth() + (widthSpace * 2), y, textPaint);
		y+=TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 11, this.getResources().getDisplayMetrics());
		y+=TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 2.33f, this.getResources().getDisplayMetrics());

		canvas.drawText( this.mRightDownText , this.mLeftBlank + ( this.defaultSpace * 4 )  + ( capRadius * 2 ) + this.defaultSpace + this.extraBalnk+ widthSpace + resizeBitmap.getWidth() + ( widthSpace * 2 ) , y+mTextGapHeight , textPaint );
		y+=TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 24.67f, this.getResources().getDisplayMetrics());

        int valueTextWidth = this.getTextWidth(String.valueOf(this.mAverageMET) , this.valueTextPaint);

        canvas.drawText(String.valueOf(this.mAverageMET) , this.mLeftBlank + ( this.defaultSpace * 7 ) + ( capRadius * 3 ) - ( valueTextWidth / 2 ) , y+mTextGapHeight , this.valueTextPaint );*/

		float left = this.mLeftBlank + ( this.defaultSpace * 4 )  + ( capRadius * 2 ) + this.defaultSpace + this.extraBalnk;
		float top = 0;
		float right = this.mLeftBlank + ( this.defaultSpace * 4 * 2 )  + ( capRadius * 2 * 2 ) + this.defaultSpace + this.extraBalnk;
		float bottom = this.getMeasuredHeight();

		//float left =this.getMeasuredWidth()/2;
		int contentWidth = ( this.defaultSpace * 4 ) + ( capRadius * 2 );
		int contentHeight = this.getMeasuredHeight();

		//int widthSpace = contentWidth / 30;

		float textsize = textPaint.getTextSize();
		float gap = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 2.33f, this.getResources().getDisplayMetrics());
		Bitmap src = BitmapFactory.decodeResource(this.mContext.getResources(), R.drawable.icon_met);
		Bitmap resizeBitmap = Bitmap.createScaledBitmap(src, this.imageWidthSize, this.imageHeightSize, false);
		float bitmapLeft=  TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 12.67f, this.getResources().getDisplayMetrics());
		float bitmapRight=  TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10f, this.getResources().getDisplayMetrics());

		float y = contentHeight/2;
		//	int top  =  (this.getMeasuredHeight()  - resizeBitmap.getHeight())/2;
		canvas.drawBitmap(resizeBitmap, bitmapLeft+left, y - this.imageHeightSize, this.leftRectangelPaint);
		canvas.drawText(this.mRightDownText, left+ resizeBitmap.getWidth() + bitmapLeft +bitmapRight , y, textPaint );

		y-=(gap+textsize);

		canvas.drawText(this.mRightUpText, left + bitmapLeft + resizeBitmap.getWidth() + bitmapRight, y, textPaint);

		String valueString= String.valueOf(this.mAverageMET);
		int valueTextWidth = this.getTextWidth(valueString, this.valueTextPaint);
		int valueTextHeight = this.getTextHeight(valueString , this.valueTextPaint);

		y =0.75f*(top+bottom);
		float x=(left+right)/2 - valueTextWidth/2;

		canvas.drawText(valueString , x , y+valueTextHeight/2 , this.valueTextPaint );
	}

	private void drawLeftRoundedRectangle(Canvas canvas, int capRadius )
	{
		float left = this.mLeftBlank;
		float top = 0;
		float right = this.mLeftBlank + ( this.defaultSpace * 4 ) + ( capRadius * 2 );
		float bottom = this.getMeasuredHeight();
		
		this.drawRoundedRectangle( canvas ,capRadius ,  left , top , right , bottom , this.leftRectangelPaint );
	}

	private void drawRightRoundedRectangle(Canvas canvas, int capRadius )
	{
		float left = this.mLeftBlank + ( this.defaultSpace * 4 )  + ( capRadius * 2 ) + this.defaultSpace + this.extraBalnk;
		float top = 0;
		float right = this.mLeftBlank + ( this.defaultSpace * 4 * 2 )  + ( capRadius * 2 * 2 ) + this.defaultSpace + this.extraBalnk;
		float bottom = this.getMeasuredHeight();
		
		this.drawRoundedRectangle(canvas , capRadius ,  left , top , right , bottom , this.rightRectangelPaint );
	}
	
	private void drawRoundedRectangle( Canvas canvas , int capRadius , float left , float top , float right , float bottom , Paint paint )
	{
		barPath.addRect( left + capRadius  , top , right - capRadius , top + bottom , Path.Direction.CW );
		barPath.addCircle( left + capRadius , top + capRadius  , capRadius , Path.Direction.CW );
		barPath.addCircle( right - capRadius , top + capRadius  , capRadius , Path.Direction.CW );
		
		barPath.close();
		
		canvas.drawPath( barPath , paint);
		
		barPath.reset();
	}

    public void setExerciseTime(int exerciseTime)
    {
        this.mExerciseTime = exerciseTime;
    }

    public void setAverageMET( double averageMET )
    {
        this.mAverageMET = averageMET;
    }

    public void setLeftText( String upText , String downText ){
        this.mLeftUpText = upText;
        this.mLeftDownText = downText;
    }

    public void setRightText( String upText , String downText ){
        this.mRightUpText = upText;
        this.mRightDownText = downText;
    }

    private int getTextWidth(String text, Paint paint)
    {
        Rect bounds = new Rect();
        paint.getTextBounds(text, 0, text.length(), bounds);
        return bounds.width();
    }

    private int getTextHeight(String text, Paint paint)
    {
        Rect bounds = new Rect();
        paint.getTextBounds(text, 0, text.length(), bounds);
        return bounds.height();
    }

}
