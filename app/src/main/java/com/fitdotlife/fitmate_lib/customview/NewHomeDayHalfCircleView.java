package com.fitdotlife.fitmate_lib.customview;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fitdotlife.fitmate.R;
import com.fitdotlife.fitmate.log.Log;
import com.fitdotlife.fitmate.newhome.NewHomeProgramClickListener;
import com.fitdotlife.fitmate_lib.key.RangeType;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import okhttp3.internal.io.RealConnection;

/**
 * Created by Joshua on 2015-12-17.
 */
@SuppressWarnings("ResourceType")
@EViewGroup(R.layout.child_newhome_day_halfcircle)
public class NewHomeDayHalfCircleView extends RelativeLayout implements ValueAnimator.AnimatorUpdateListener {

    private String TAG = NewHomeDayHalfCircleView.class.getSimpleName();

    private float mValue = 0;
    private NewHomeProgramClickListener mListener = null;
    private int mExerciseProgramId = 0;
    private Context mContext = null;

    private float mRangeFrom = 0;
    private float mRangeTo = 0;
    private String mValueUnitText = null;
    private int mDuration = 0;
    private float preValue = 0;

    private RangeType mRangeType = null;

    @ViewById(R.id.tv_child_newhome_day_halfcircle_achieve_value)
    TextView tvAchieveValue;

    @ViewById(R.id.tv_child_newhome_day_halfcircle_achieve_value_unit)
    TextView tvAchieveValueUnit;

    @ViewById(R.id.tv_child_newhome_day_halfcircle_value)
    TextView tvValue;

    @ViewById(R.id.tv_child_newhome_day_halfcircle_value_unit)
    TextView tvValueUnit;

    @ViewById(R.id.tv_child_newhome_day_halfcircle_category)
    TextView tvCategory;

    @ViewById(R.id.dhcv_child_newhome_day_halfcircle)
    DayHalfCircleView dhcvDay;

    @ViewById(R.id.rl_child_newhome_day_halfcircle_content)
    RelativeLayout rlContent;

    @ViewById(R.id.rl_child_newhome_day_halfcircle)
    RelativeLayout rlDayHalfCircle;

    @ViewById(R.id.ll_child_newhome_day_halfcircle_content)
    LinearLayout llHalfCircleContent;

    public NewHomeDayHalfCircleView(Context context) {

        super(context);
        this.mContext = context;

    }

    public void setCategoryText( String categoryText ){
        tvCategory.setText(categoryText);
    }

    public void setRange( float rangeFrom , float rangeTo  ){
        this.mRangeFrom = rangeFrom;
        this.mRangeTo = rangeTo;

        if( mRangeFrom == mRangeTo ){
            mRangeType = RangeType.ONE_RANGE;
        }else{
            mRangeType = RangeType.TWO_RANGE;
        }
    }

    public void setAchieveValueText( String achieveValueText ){
        this.tvAchieveValueUnit.setText( achieveValueText );
    }

    public void setStrengthTypeUnitText(String valueUnitText){
        this.mValueUnitText = valueUnitText;
        this.tvValueUnit.setText( valueUnitText );
    }

    public void setValue( float value )
    {
        mValue = value;
    }

    public void display()
    {

        int[] attrs = new int[]{R.attr.newhomeDayCirleRangeEmpty , R.attr.newhomeAchieveEmpty , R.attr.newhomeAchieveUnder ,R.attr.newhomeAchieve , R.attr.newhomeAchieveOver };
        TypedArray a = mContext.getTheme().obtainStyledAttributes(attrs);

        int dayCirclRangeEmptyColor = a.getColor(0, 0);
        int achieveEmptyColor = a.getColor(1, 0);
        int achieveUnderColor = a.getColor(2, 0);
        int achieveColor = a.getColor(3,0);
        int achieveOverColor = a.getColor(4,0);
        a.recycle();

        int valueColor = 0;
        int achieveValue = 0;

        tvValue.setText( (int)mValue + "" );

        int defaultAchieveFontSize = 63;
        int overAchieveFontSize = 38;
        //값에 색깔 게산
        if(mValue > 0)
        {

            if( mRangeType.equals( RangeType.ONE_RANGE ) )
            {
                //언더일 때
                if (this.mValue < mRangeTo) {

                    achieveValue = (int) ((mValue / mRangeTo) * 100);
                    valueColor = achieveUnderColor;
                    this.tvAchieveValue.setTextSize(TypedValue.COMPLEX_UNIT_DIP, defaultAchieveFontSize);
                    this.tvAchieveValue.setText(String.format("%d", (int) achieveValue));
                    this.tvAchieveValueUnit.setVisibility(View.VISIBLE);
                    this.tvAchieveValue.setTextColor(achieveColor);
                    this.tvAchieveValueUnit.setTextColor(achieveColor);

                    RelativeLayout.LayoutParams halfCircleValueParams = (LayoutParams) llHalfCircleContent.getLayoutParams();
                    halfCircleValueParams.removeRule(RelativeLayout.CENTER_VERTICAL);
                    llHalfCircleContent.setLayoutParams(halfCircleValueParams);

                    LinearLayout.LayoutParams tvCategoryParams = (LinearLayout.LayoutParams) tvCategory.getLayoutParams();
                    tvCategoryParams.setMargins( 0, (int)( TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP , 2.5f , getResources().getDisplayMetrics() ) ) , 0, 0 );
                    tvCategory.setLayoutParams( tvCategoryParams );

                } else {

                    achieveValue = 100;
                    valueColor = achieveColor;
                    this.tvAchieveValue.setTextSize(TypedValue.COMPLEX_UNIT_DIP, defaultAchieveFontSize);
                    this.tvAchieveValue.setText(String.format("%d", (int) achieveValue));
                    this.tvAchieveValueUnit.setVisibility(View.VISIBLE);
                    this.tvAchieveValue.setTextColor(achieveColor);
                    this.tvAchieveValueUnit.setTextColor(achieveColor);

                    RelativeLayout.LayoutParams halfCircleValueParams = (LayoutParams) llHalfCircleContent.getLayoutParams();
                    halfCircleValueParams.removeRule(RelativeLayout.CENTER_VERTICAL);
                    llHalfCircleContent.setLayoutParams(halfCircleValueParams);

                    LinearLayout.LayoutParams tvCategoryParams = (LinearLayout.LayoutParams) tvCategory.getLayoutParams();
                    tvCategoryParams.setMargins( 0, (int)( TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP , 2.5f , getResources().getDisplayMetrics() ) ) , 0, 0 );
                    tvCategory.setLayoutParams( tvCategoryParams );
                }

            }else{

                //언더일 때
                if (this.mValue < mRangeFrom)
                {
                    achieveValue = (int) ((mValue / mRangeFrom) * 100);
                    valueColor = achieveUnderColor;
                    this.tvAchieveValue.setTextSize(TypedValue.COMPLEX_UNIT_DIP, defaultAchieveFontSize);
                    this.tvAchieveValue.setText(String.format("%d", (int) achieveValue));
                    this.tvAchieveValueUnit.setVisibility(View.VISIBLE);
                    this.tvAchieveValue.setTextColor(achieveColor);
                    this.tvAchieveValueUnit.setTextColor(achieveColor);

                    RelativeLayout.LayoutParams halfCircleValueParams = (LayoutParams) llHalfCircleContent.getLayoutParams();
                    halfCircleValueParams.removeRule(RelativeLayout.CENTER_VERTICAL);
                    llHalfCircleContent.setLayoutParams(halfCircleValueParams);

                    LinearLayout.LayoutParams tvCategoryParams = (LinearLayout.LayoutParams) tvCategory.getLayoutParams();
                    tvCategoryParams.setMargins( 0, (int)( TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP , 2.5f , getResources().getDisplayMetrics() ) ) , 0, 0 );
                    tvCategory.setLayoutParams( tvCategoryParams );

                } else {

                    //달성일 때
                    if (this.mValue <= mRangeTo) {
                        achieveValue = 100;
                        valueColor = achieveColor;
                        this.tvAchieveValue.setTextSize(TypedValue.COMPLEX_UNIT_DIP, defaultAchieveFontSize);
                        this.tvAchieveValue.setText(String.format("%d", (int) achieveValue));
                        this.tvAchieveValueUnit.setVisibility(View.VISIBLE);
                        this.tvAchieveValue.setTextColor(achieveColor);
                        this.tvAchieveValueUnit.setTextColor(achieveColor);

                        RelativeLayout.LayoutParams halfCircleValueParams = (LayoutParams) llHalfCircleContent.getLayoutParams();
                        halfCircleValueParams.removeRule(RelativeLayout.CENTER_VERTICAL);
                        llHalfCircleContent.setLayoutParams(halfCircleValueParams);

                        LinearLayout.LayoutParams tvCategoryParams = (LinearLayout.LayoutParams) tvCategory.getLayoutParams();
                        tvCategoryParams.setMargins( 0, (int)( TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP , 2.5f , getResources().getDisplayMetrics() ) ) , 0, 0 );
                        tvCategory.setLayoutParams( tvCategoryParams );

                    } else { //초과일 때
                        valueColor = achieveOverColor;
                        this.tvAchieveValue.setTextSize(TypedValue.COMPLEX_UNIT_DIP, overAchieveFontSize);
                        this.tvAchieveValue.setText(this.getResources().getString(R.string.newhome_day_overwork));
                        this.tvAchieveValue.setTextColor(achieveOverColor);
                        this.tvAchieveValueUnit.setVisibility(View.GONE);

                        RelativeLayout.LayoutParams halfCircleValueParams = (LayoutParams) llHalfCircleContent.getLayoutParams();
                        halfCircleValueParams.addRule(RelativeLayout.CENTER_VERTICAL);
                        llHalfCircleContent.setLayoutParams(halfCircleValueParams);

                        LinearLayout.LayoutParams tvCategoryParams = (LinearLayout.LayoutParams) tvCategory.getLayoutParams();
                        tvCategoryParams.setMargins( 0, (int)( TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP , 10 , getResources().getDisplayMetrics() ) ) , 0, 0 );
                        tvCategory.setLayoutParams( tvCategoryParams );
                    }
                }
            }
        }else{
            valueColor = dayCirclRangeEmptyColor;
            this.tvAchieveValue.setTextSize(TypedValue.COMPLEX_UNIT_DIP, defaultAchieveFontSize);
            this.tvAchieveValue.setText(String.format("%d", (int) achieveValue));
            this.tvAchieveValueUnit.setVisibility(View.VISIBLE);
            this.tvAchieveValue.setTextColor(achieveColor);
            this.tvAchieveValueUnit.setTextColor(achieveColor);

            RelativeLayout.LayoutParams halfCircleValueParams = (LayoutParams) llHalfCircleContent.getLayoutParams();
            halfCircleValueParams.removeRule(RelativeLayout.CENTER_VERTICAL);
            llHalfCircleContent.setLayoutParams(halfCircleValueParams);

            LinearLayout.LayoutParams tvCategoryParams = (LinearLayout.LayoutParams) tvCategory.getLayoutParams();
            tvCategoryParams.setMargins( 0, (int)( TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP , 2.5f , getResources().getDisplayMetrics() ) ) , 0, 0 );
            tvCategory.setLayoutParams( tvCategoryParams );
        }

        dhcvDay.setCircleThickness(8.5f);
        dhcvDay.setEmptyCircleColor(achieveEmptyColor);
        dhcvDay.setRange(mRangeFrom, mRangeTo, mValueUnitText);

        this.tvValue.setTextColor(valueColor);
        this.tvValueUnit.setTextColor(valueColor);

        dhcvDay.setValueColor(valueColor);
        dhcvDay.setValue(mValue);
        dhcvDay.invalidate();
    }

    public void changeMargin()
    {
        RelativeLayout.LayoutParams dayHalfParams = (LayoutParams) rlDayHalfCircle.getLayoutParams();
        int topMargin = (int) dhcvDay.getCalculateTopMargin();
        int bottomMargin = (int) dhcvDay.getCalculateBottomMargin();
        dayHalfParams.setMargins(0, topMargin, 0, bottomMargin);
        rlDayHalfCircle.setLayoutParams( dayHalfParams );
    }

    @Override
    public void onAnimationUpdate(ValueAnimator animation) {
        float value = (float) animation.getAnimatedValue();
        mValue = value;
        display();
    }

    public void startAnimation( int duration , boolean startZero )
    {
        mDuration = duration;

        if( mValue < 10 ) {
            mDuration = (int) ( mValue * 100 );
        }

        startAnimation(startZero);
    }

    public void startAnimation(boolean startZero)
    {
        float startValue = 0;

        if( !startZero ){
            startValue = preValue;
        }

        float animateValue = mValue;
        preValue = mValue;

        ValueAnimator animator = ValueAnimator.ofFloat( startValue , animateValue );
        animator.addUpdateListener(this);
        animator.setDuration(mDuration );
        animator.start();
    }

    public void setCategoryIntroListener( View.OnClickListener clickListener , int categoryIndex ){

        rlContent.setTag( categoryIndex );
        rlContent.setOnClickListener(clickListener);
    }

}
