package com.fitdotlife.fitmate_lib.customview;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;

import com.fitdotlife.fitmate.R;

/**
 * Created by Joshua on 2016-05-02.
 */
public class FitmeterDialog {

    private Context mContext = null;
    private Dialog mDialog = null;
    private LinearLayout llContent = null;
    private LinearLayout llButton = null;

    public FitmeterDialog( Context context)
    {
        this.mContext = context;
        this.mDialog = new Dialog( mContext );

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        View exitView = inflater.inflate(R.layout.dialog_default, null);
        llContent = (LinearLayout) exitView.findViewById(R.id.ll_dialog_default_content);
        llButton = (LinearLayout) exitView.findViewById( R.id.ll_dialog_default_button );

        mDialog.getWindow().setBackgroundDrawableResource( R.color.dialog_background );
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        mDialog.setContentView(exitView);
    }

    public void setContentView( View contentView )
    {

        if( contentView == null ){

            llContent.setVisibility( View.GONE );

        }else{
            llContent.addView( contentView );
        }

    }

    public void setButtonView( View buttonView ){

        if( buttonView == null ){
            llButton.setVisibility(View.GONE);
        }else{
            llButton.addView( buttonView );
        }
    }

    public void setCancelable( boolean cancelable ){
        mDialog.setCancelable( cancelable );
    }

    public void show(){
        mDialog.show();
    }

    public void close(){
        mDialog.cancel();
    }

    public boolean isShowing(){
        return mDialog.isShowing();
    }

    public void setOnCancelListener( DialogInterface.OnCancelListener cancelListener  ){
        mDialog.setOnCancelListener( cancelListener );
    }


}