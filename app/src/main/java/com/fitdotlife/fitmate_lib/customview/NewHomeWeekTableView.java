package com.fitdotlife.fitmate_lib.customview;

import android.content.Context;
import android.content.res.TypedArray;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fitdotlife.fitmate.R;
import com.fitdotlife.fitmate.newhome.NewHomeWeekView;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

/**
 * Created by Joshua on 2015-12-18.
 */
@EViewGroup(R.layout.child_newhome_week_table)
public class NewHomeWeekTableView extends LinearLayout implements NewHomeWeekView{

    private double[] mValues = null;
    private long[] mTimeMillis = null;
    private Context mContext = null;
    private View.OnClickListener mClickListener = null;
    private int mSelectedDayIndex = -1;
    private int mTotalValue = 0;


    @ViewById(R.id.tv_child_newhome_week_table_weekrange)
    TextView tvWeekRange;

    @ViewById(R.id.ll_child_newhome_week_table)
    LinearLayout llWeekTable;

    public NewHomeWeekTableView(Context context) {
        super(context);
        this.mContext = context;
    }

    public void setWeekRange( String weekRange ){
        tvWeekRange.setText(weekRange);
    }

    public void setValues( double[] values , int selectDayIndex ){
        this.mValues = values;
        this.mSelectedDayIndex = selectDayIndex;
    }

    public void setValues( double[] values ){
        this.mValues = values;
    }

    public void setListener( View.OnClickListener listener ){
        this.mClickListener = listener;
    }

    public void updateView( int childIndex , double value )
    {
        if( value < 0 ){
            value = 0;
        }

        mTotalValue += ( (int)value - (int)mValues[childIndex] );

        mValues[ childIndex ] = value;
        NewHomeWeekTableCellView cellView = (NewHomeWeekTableCellView) llWeekTable.getChildAt(childIndex);
        cellView.setScore(value, String.format(getResources().getString(R.string.newhome_week_table_date_format), childIndex + 1), null);

        NewHomeWeekTableCellView totalCellView = (NewHomeWeekTableCellView) llWeekTable.getChildAt( llWeekTable.getChildCount() - 1 );

        int[] attrs = new int[] { R.attr.newhomeDayValue };
        TypedArray ta = mContext.getTheme().obtainStyledAttributes(attrs);
        int totalColor = ta.getColor(0, 0);
        ta.recycle();

        totalCellView.setScore(String.format("%d", mTotalValue), getResources().getString(R.string.newhome_week_table_total), totalColor, totalColor);
    }

    public void updateView( int[] childList , double[] valueList ){

        for( int i = 0 ; i < childList.length ;i++ )
        {
            double value = 0;
            if( valueList[i] > 0 ){
                value = valueList[i];
            }

            mTotalValue += ( (int)value - (int)mValues[i] );

            mValues[ i ] = value;
            NewHomeWeekTableCellView cellView = (NewHomeWeekTableCellView) llWeekTable.getChildAt(i);
            cellView.setScore(value, String.format(getResources().getString(R.string.newhome_week_table_date_format), i + 1), null);

        }

        NewHomeWeekTableCellView totalCellView = (NewHomeWeekTableCellView) llWeekTable.getChildAt( llWeekTable.getChildCount() - 1 );

        int[] attrs = new int[] { R.attr.newhomeDayValue };
        TypedArray ta = mContext.getTheme().obtainStyledAttributes(attrs);
        int totalColor = ta.getColor(0, 0);
        ta.recycle();

        totalCellView.setScore(String.format("%d", mTotalValue), getResources().getString(R.string.newhome_week_table_total), totalColor, totalColor);
    }


    @Override
    public void removeSelectDay() {

        if( mSelectedDayIndex > -1 ){

            NewHomeWeekTableCellView cellView = (NewHomeWeekTableCellView)  llWeekTable.getChildAt(mSelectedDayIndex);
            cellView.setSelectedDay( false );
            mSelectedDayIndex = -1;

        }
    }

    public void refresh(){

        mTotalValue = 0;

        for( int i = 0 ; i < mValues.length;i++ )
        {

            NewHomeWeekTableCellView cellView = (NewHomeWeekTableCellView) llWeekTable.getChildAt(i);

            cellView.setScore(mValues[i], String.format(getResources().getString(R.string.newhome_week_table_date_format), i + 1), mClickListener);

            if( mValues[i] >= 0 ) {
                mTotalValue += (int) mValues[i];
            }
        }

        NewHomeWeekTableCellView totalCellView = (NewHomeWeekTableCellView) llWeekTable.getChildAt( mValues.length );

        int[] attrs = new int[] { R.attr.newhomeDayValue };
        TypedArray ta = mContext.getTheme().obtainStyledAttributes(attrs);

        int totalColor = ta.getColor(0 , 0);
        ta.recycle();

        totalCellView.setScore(String.format( "%d", mTotalValue ), getResources().getString(R.string.newhome_week_table_total ) , totalColor , totalColor );

    }

    @Override
    public void setSelectDay(int childIndex) {

        NewHomeWeekTableCellView cellView = (NewHomeWeekTableCellView)  llWeekTable.getChildAt(childIndex);
        cellView.setSelectedDay( true );
        mSelectedDayIndex = childIndex;
    }

    public void display(){

        for( int i = 0 ; i < mValues.length;i++ )
        {

            NewHomeWeekTableCellView cellView = NewHomeWeekTableCellView_.build(this.mContext);

            cellView.setScore(mValues[i], String.format(getResources().getString(R.string.newhome_week_table_date_format), i + 1) , mClickListener);

            cellView.setTag( R.string.newhome_weekview_time_key , mTimeMillis[i]);
            cellView.setTag( R.string.newhome_weekview_cellindex_key , i );

            if( mSelectedDayIndex == i ) {
                cellView.setSelectedDay( true );
            }

            llWeekTable.addView(cellView , i);
            if( mValues[i] >= 0 ) {
                mTotalValue += (int) mValues[i];
            }
        }

        NewHomeWeekTableCellView totalCellView = NewHomeWeekTableCellView_.build(this.mContext);

        int[] attrs = new int[] { R.attr.newhomeDayValue };
        TypedArray ta = mContext.getTheme().obtainStyledAttributes(attrs);

        int totalColor = ta.getColor(0 , 0);
        ta.recycle();

        totalCellView.setScore(String.format( "%d", mTotalValue ), getResources().getString(R.string.newhome_week_table_total ) , totalColor , totalColor );
        llWeekTable.addView( totalCellView );
    }

    public void setDays(long[] timeMillis) {
        this.mTimeMillis = timeMillis;
    }
}
