package com.fitdotlife.fitmate_lib.customview;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import com.fitdotlife.fitmate.R;

/**
 * Created by Joshua on 2016-02-02.
 */
public class BatteryPopupCircleView extends View{
    Paint mFillPaint = null;
    Paint mBatteryValuePaint = null;
    Paint mXfermodePaint = null;

    private int heightSize = 0;
    private int widthSize = 0;

    private int mBatteryValue = 20;

    public BatteryPopupCircleView(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.mFillPaint = new Paint( Paint.ANTI_ALIAS_FLAG );
        this.mFillPaint.setColor( this.getResources().getColor(R.color.newhome_battery_popup_fill) );

        this.mBatteryValuePaint = new Paint( Paint.ANTI_ALIAS_FLAG );
        this.mBatteryValuePaint.setColor( this.getResources().getColor(R.color.newhome_battery_popup_value));

        this.mXfermodePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.mXfermodePaint.setXfermode( new PorterDuffXfermode(PorterDuff.Mode.SRC_ATOP ) );
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        int heightMode = View.MeasureSpec.getMode(heightMeasureSpec);
        switch( heightMode )
        {
            case View.MeasureSpec.UNSPECIFIED:
                heightSize = heightMeasureSpec;
                break;
            case View.MeasureSpec.AT_MOST:
                heightSize = 200;
                break;
            case View.MeasureSpec.EXACTLY:
                heightSize = View.MeasureSpec.getSize(heightMeasureSpec);
                break;
        }

        int widthMode = View.MeasureSpec.getMode(widthMeasureSpec);
        switch( widthMode )
        {
            case View.MeasureSpec.UNSPECIFIED:
                widthSize = widthMeasureSpec;
                break;
            case View.MeasureSpec.AT_MOST:
                widthSize = 300;
                break;
            case View.MeasureSpec.EXACTLY:
                widthSize = View.MeasureSpec.getSize(widthMeasureSpec);
                break;
        }
        this.setMeasuredDimension(widthSize, heightSize);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        Bitmap bm = Bitmap.createBitmap( this.getMeasuredWidth() , this.getMeasuredHeight()  , Bitmap.Config.ARGB_8888 );
        bm.setDensity(this.getResources().getDisplayMetrics().densityDpi);
        Canvas c = new Canvas(bm);

        //DST
        RectF oval = new RectF( 0 , 0 , this.getMeasuredWidth() , this.getMeasuredHeight() );
        c.drawArc(oval, 0, 360, true, mFillPaint);

        c.saveLayer(oval , mXfermodePaint , Canvas.ALL_SAVE_FLAG);

        //SRC
        float batteryOvalHeight = this.getMeasuredHeight() - ( ( this.getMeasuredHeight()  * mBatteryValue ) / 100 );
        RectF batteryOval = new RectF( 0 , batteryOvalHeight , this.getMeasuredWidth() , this.getMeasuredHeight() );
        c.drawRect(batteryOval, mBatteryValuePaint);
        c.restore();

        Paint bitmapPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        canvas.drawBitmap(bm , 0, 0 , bitmapPaint );
    }

    public void setBatteryValue(  int batteryValue  ){
        this.mBatteryValue = batteryValue;
    }

}
