package com.fitdotlife.fitmate_lib.customview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;

/**
 * Created by Joshua on 2015-04-16.
 */
public class StripeView extends View {

    private String TAG = "fitmate - " + StripeView.class.getSimpleName();

    //private static final float STRIPE_WIDTH_PERCT = .05f;
    private static final float STRIPE_WIDTH_PERCT = 1;
    private static final float STRIPE_OFFSET_PERCT = 1;
    private Path stripespath = new Path();
    private Paint stripePaint = new Paint();
    private Paint textPaint = new Paint();
    private static final long REFRESH_RATE = 33;
    private static final long ANIMATION_SPEED = 5;
    private String mText = null;
    private float mTextSize = 0;

    private final Runnable invalidateRunnable = new Runnable(){
        public void run(){
            invalidate();
        }
    };

    public StripeView(Context context) {
        super(context);
    }

    public StripeView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public StripeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    //init()
    {

        this.mTextSize = TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_SP , 11.33f , this.getResources().getDisplayMetrics() );

        stripePaint.setColor( 0xFF5B872A );
        stripePaint.setAntiAlias(true);

        textPaint.setTextSize( this.mTextSize );
        textPaint.setColor( Color.WHITE );
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawColor( 0xFF63932E );
        drawStripes(canvas, 0, getHeight(), 0, getWidth(), stripePaint);
        postDelayed( invalidateRunnable , REFRESH_RATE );
    }

    private void drawStripes( Canvas canvas, float topy, float bottomy, float startx, float endx, Paint p)
    {
        if(bottomy - topy < 1){
            return;
        }

        stripespath.reset();

        float width = (bottomy - topy) * STRIPE_WIDTH_PERCT;
        float offset = (bottomy - topy) * STRIPE_OFFSET_PERCT;
        //for(float xPos = startx - offset; xPos <= endx; xPos += offset + width){
        for( float xPos = startx - (System.currentTimeMillis()/ANIMATION_SPEED) % (long)(offset + width); xPos <= endx; xPos += offset + width){
            stripespath.moveTo(xPos, topy);
            stripespath.lineTo(xPos + width, topy);
            stripespath.lineTo(xPos + offset + width, bottomy);
            stripespath.lineTo(xPos + offset, bottomy);
            stripespath.lineTo(xPos, topy);
        }
        canvas.drawPath(stripespath, p);

        drawText(canvas);
    }

    private void drawText( Canvas canvas ){

        int textHeight = this.getTextHeight( this.mText , this.textPaint );
        int textWidth = this.getTextWidth(this.mText , this.textPaint);

        float textX = ( this.getMeasuredWidth() / 2 ) - ( textWidth / 2 );
        float textY = ( this.getMeasuredHeight() / 2 ) + (textHeight / 2);

        canvas.drawText( this.mText , textX , textY , this.textPaint );
    }

    public void setText( String text ){

        //Log.i(TAG , "StripeView " + text);
        this.mText = text;
    }

    private int getTextHeight(String text, Paint paint)
    {
        Rect bounds = new Rect();
        paint.getTextBounds(text, 0, text.length(), bounds);
        return bounds.height();
    }

    private int getTextWidth(String text, Paint paint)
    {
        Rect bounds = new Rect();
        paint.getTextBounds(text, 0, text.length(), bounds);
        return bounds.width();
    }

}
