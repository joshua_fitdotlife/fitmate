package com.fitdotlife.fitmate_lib.customview;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;


public class HomeWeekScoreChartView extends View implements AnimatorUpdateListener {

    private Context mContext = null;
	private int mValue;
	private Resources resource;

    private Paint mSeriesBackgroundPaint = null;
    private Paint mPredaySeriesPaint = null;
    private Paint mTodaySeriaPaint = null;

    private int mPredayScore = 0;
    private int mTodayScore = 0;
    private int mAniTotalScore = 0;
    protected int textSize = 0;
    private String mValueUnit = "";

    private float mTextBlank = 0;
    private int mPreviousValue = 0;

    public HomeWeekScoreChartView(Context context)
	{
		super(context);
		
		this.mContext = context;
		this.init();
	}

	private void init()
	{
        this.mTextBlank = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP , 2 , this.getResources().getDisplayMetrics());

        this.mPredaySeriesPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.mPredaySeriesPaint.setColor( 0xFFF58F7E );

        this.mTodaySeriaPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.mTodaySeriaPaint.setColor( 0xFFE42707 );

        this.mSeriesBackgroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.mSeriesBackgroundPaint.setColor( 0xFFE9E9E9 );
	}

    public HomeWeekScoreChartView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.init();
    }

    @Override
	protected void onMeasure(int widthMeasureSpec , int heightMeasureSpec)
	{
		int heightMode = MeasureSpec.getMode(heightMeasureSpec);
		int heightSize = 0;
		
		switch( heightMode )
		{
		case MeasureSpec.UNSPECIFIED:
			heightSize = heightMeasureSpec;
			break;
		case MeasureSpec.AT_MOST:
			heightSize = 80;
			break;
		case MeasureSpec.EXACTLY:
			heightSize = MeasureSpec.getSize(heightMeasureSpec);
			break;
		}
		
		int widthMode = MeasureSpec.getMode(widthMeasureSpec);
		int widthSize = 0;
		switch( widthMode )
		{
		case MeasureSpec.UNSPECIFIED:
			widthSize = widthMeasureSpec;
			break;
		case MeasureSpec.AT_MOST:
			widthSize = 200;
			break;
		case MeasureSpec.EXACTLY:
			widthSize = MeasureSpec.getSize(widthMeasureSpec);
			break;
		}
		
		this.setMeasuredDimension(widthSize, heightSize);
	}
	
	protected void onDraw(Canvas canvas)
	{
        this.drawBackgroundSeries(canvas);
        this.drawSeries(canvas);
	}

    private void drawBackgroundSeries(Canvas canvas) {

        int barHeight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP , 24, getResources().getDisplayMetrics());

        Path barPath = new Path();
        barPath.addRect( ( barHeight / 2 )  , 0 , this.getMeasuredWidth() - ( barHeight / 2 ) , barHeight , Path.Direction.CW );
        barPath.addCircle( ( barHeight / 2 ) , ( barHeight / 2 )  , barHeight / 2 , Path.Direction.CW );
        barPath.addCircle( this.getMeasuredWidth() - ( barHeight / 2 ) , barHeight / 2  , barHeight / 2 , Path.Direction.CW );
        barPath.close();

        canvas.drawPath(barPath, this.mSeriesBackgroundPaint);

        float indicatorTextSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP , 9 , this.getResources().getDisplayMetrics());
        Paint indicatorTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        indicatorTextPaint.setTextSize( indicatorTextSize );

        int startIndicatorTextWidth = this.getTextWidth("0" + this.mValueUnit, indicatorTextPaint);
        int startIndicatorTextHeight = this.getTextHeight("0" + this.mValueUnit , indicatorTextPaint);
        float startIndicatorTextX = 0;
        float startIndicatorTextY = barHeight + startIndicatorTextHeight + TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP , 5.33f , this.getResources().getDisplayMetrics());
        float endIndicatorTextWidth = indicatorTextPaint.measureText("100" + this.mValueUnit);
        int endIndicatorTextHeight = this.getTextHeight("100" + this.mValueUnit , indicatorTextPaint);
        float endIndicatorTextX = this.getMeasuredWidth() - endIndicatorTextWidth;
        float endIndicatorTextY = barHeight + endIndicatorTextHeight + TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP , 5.33f , this.getResources().getDisplayMetrics());;
        canvas.drawText("0" + this.mValueUnit , startIndicatorTextX , startIndicatorTextY , indicatorTextPaint);
        canvas.drawText("100" + this.mValueUnit , endIndicatorTextX , endIndicatorTextY  , indicatorTextPaint);
    }

    private void drawSeries( Canvas canvas )
	{
		if( mPredayScore == 0 & mTodayScore == 0 ) return;

        float barHeight = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP , 24, getResources().getDisplayMetrics());

        float totalBarWidth = ( this.mAniTotalScore * ( this.getMeasuredWidth() - barHeight) ) / 100;
        float predayBarWidth = (this.mAniTotalScore * ( this.getMeasuredWidth() - barHeight) ) / 100;

        if( mPredayScore > 0 ) {

            if ( mAniTotalScore >= mPredayScore) {  //
                predayBarWidth = ( this.mPredayScore * (this.getMeasuredWidth() - barHeight)) / 100;
            }

            this.drawFirstSeries( canvas, barHeight, predayBarWidth, this.mAniTotalScore, this.mValueUnit, this.mPredaySeriesPaint );

            if  (mAniTotalScore > mPredayScore) {
                float todayBarWidth = ( ( this.mAniTotalScore - this.mPredayScore ) * (this.getMeasuredWidth() - barHeight)) / 100;
                this.drawSecondSeries( canvas, barHeight, predayBarWidth, todayBarWidth, totalBarWidth, this.mAniTotalScore - this.mPredayScore , this.mValueUnit);
            }

        }else{

            this.drawFirstSeries(canvas, barHeight, predayBarWidth, this.mAniTotalScore, this.mValueUnit, this.mTodaySeriaPaint);

        }

        //this.drawTotalText( canvas , totalBarWidth , barHeight , this.mAniTotalScore , this.mValueUnit);
        this.drawTotalText( canvas , ( this.getMeasuredWidth() / 2 ) , barHeight , this.mAniTotalScore , this.mValueUnit);
	}

    private void drawTotalText( Canvas canvas , float totalBarWidth , float barHeight , int value , String valueUnit ){
        Paint totalScoreTextPaint = new Paint();
        totalScoreTextPaint.setColor(0xFFCB1F02);
        totalScoreTextPaint.setTextSize(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 14.5f, getResources().getDisplayMetrics()));
        totalScoreTextPaint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
        int totalScoreTextWidth = this.getTextWidth( value + valueUnit , totalScoreTextPaint  );
        int totalScoreTextheight = this.getTextHeight( value + valueUnit , totalScoreTextPaint );

        canvas.drawText( value + valueUnit , totalBarWidth - ( totalScoreTextWidth / 2 ) ,  barHeight + totalScoreTextheight + TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP , 5.33f , this.getResources().getDisplayMetrics())  , totalScoreTextPaint  );
    }

    private void drawFirstSeries( Canvas canvas , float barHeight , float predayBarWidth , int value , String valueUnit , Paint paint ){
        Path predayPath = new Path();

        //predayPath.addCircle( (barHeight / 2), (barHeight / 2), ( barHeight / 2 ) , Path.Direction.CW );
        //predayPath.addArc( 0 , 0, barHeight ,barHeight , 90 , 180 );
        //predayPath.arcTo( 0 , 0, barHeight ,barHeight , 90 , 180 , false);

        RectF circleRect = new RectF(0 , 0, barHeight ,barHeight);
        predayPath.addArc(circleRect , 90 ,180);
        predayPath.addRect( (barHeight / 2), 0, (barHeight / 2) + predayBarWidth, barHeight, Path.Direction.CW);
        if( predayBarWidth == ( this.getMeasuredWidth() - barHeight ) ){
            predayPath.addCircle( this.getMeasuredWidth() - ( barHeight / 2 ) , (barHeight / 2) , ( barHeight / 2 ) , Path.Direction.CW );
        }
        predayPath.close();
        canvas.drawPath( predayPath , paint );

        Paint predayScoreTextPaint = new Paint();
        predayScoreTextPaint.setColor(Color.WHITE);
        predayScoreTextPaint.setTextSize(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 9, getResources().getDisplayMetrics()));
        predayScoreTextPaint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
        int predayScoreTextWidth = this.getTextWidth( value + this.mValueUnit , predayScoreTextPaint  );
        int predayScoreTextHeight = this.getTextHeight( value + this.mValueUnit , predayScoreTextPaint );

        if( mPredayScore > 0 ) {
            if (value >= mPredayScore) {
                value = mPredayScore;
            }
        }

        if( predayScoreTextWidth < predayBarWidth ) {
            canvas.drawText(value + this.mValueUnit, ((((barHeight / 2) + predayBarWidth) / 2) - (predayScoreTextWidth / 2)), (barHeight / 2) + (predayScoreTextHeight / 2), predayScoreTextPaint);
        }else{
            canvas.drawText(value + "" , ( barHeight / 2 ) - this.mTextBlank , (barHeight / 2) + (predayScoreTextHeight / 2), predayScoreTextPaint);
        }
    }

    private void drawSecondSeries( Canvas canvas , float barHeight , float predayBarWidth , float todayBarWidth , float totalBarWidth , int value , String valueUnit ){

        Path todayPath = new Path();
        todayPath.addRect( (barHeight / 2) + predayBarWidth, 0, ( barHeight / 2 ) + totalBarWidth , barHeight, Path.Direction.CW);
        if( totalBarWidth == ( this.getMeasuredWidth() - barHeight ) ){
            RectF circleRect = new RectF( this.getMeasuredWidth() - barHeight , 0, this.getMeasuredWidth() ,barHeight);
            todayPath.addArc(circleRect , 270 ,180);

            //todayPath.addCircle( this.getMeasuredWidth() - ( barHeight / 2 ) , (barHeight / 2) , ( barHeight / 2 ) , Path.Direction.CW );
        }
        todayPath.close();
        canvas.drawPath(todayPath, this.mTodaySeriaPaint);

        Paint todayScoreTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        todayScoreTextPaint.setTextSize(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 9, getResources().getDisplayMetrics()));
        todayScoreTextPaint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
        int todayScoreTextWidth = this.getTextWidth(value + valueUnit, todayScoreTextPaint);
        int todayScoreTextHeight = this.getTextHeight(value + valueUnit, todayScoreTextPaint);

        if( todayScoreTextWidth < todayBarWidth ) {
            todayScoreTextPaint.setColor(Color.WHITE);
            canvas.drawText( value + valueUnit, ( predayBarWidth + ( barHeight / 2 ) + (todayBarWidth / 2 )) - (todayScoreTextWidth / 2) , (barHeight / 2) + (todayScoreTextHeight / 2) , todayScoreTextPaint);
        }else{
            if( totalBarWidth == ( this.getMeasuredWidth() - barHeight ) ){
                todayScoreTextPaint.setColor(Color.WHITE);
                canvas.drawText(value + "", (barHeight / 2) + predayBarWidth + todayBarWidth + this.mTextBlank , (barHeight / 2) + (todayScoreTextHeight / 2) , todayScoreTextPaint);
            }else{
                todayScoreTextPaint.setColor(Color.BLACK);
                canvas.drawText(value + valueUnit, (barHeight / 2) + predayBarWidth + todayBarWidth + this.mTextBlank , (barHeight / 2) + (todayScoreTextHeight / 2) , todayScoreTextPaint);
            }
        }
    }

	public void startAnimation( int duration )
	{
        ValueAnimator animator = ValueAnimator.ofInt( this.mPreviousValue , this.mPredayScore + this.mTodayScore );
        animator.addUpdateListener(this);
        animator.setDuration(duration);
        animator.start();

        this.mPreviousValue = this.mPredayScore + this.mTodayScore;
	}

	@Override
	public void onAnimationUpdate(ValueAnimator animation)
	{	
		this.mAniTotalScore = (Integer) animation.getAnimatedValue();
		this.invalidate();
	}

    public void setScore( int predayScore , int todayScore ){
        this.mPredayScore = predayScore;
        this.mTodayScore = todayScore;
    }

	private int getTextWidth(String text, Paint paint)
	{
		Rect bounds = new Rect();
		paint.getTextBounds(text, 0, text.length(), bounds);
		return bounds.width();
	}
	
	private int getTextHeight(String text, Paint paint)
	{
		Rect bounds = new Rect();
		paint.getTextBounds(text, 0, text.length(), bounds);
		return bounds.height();
	}

    public void setValueUnit(String valueUnit) {
        this.mValueUnit = valueUnit;
    }
}
