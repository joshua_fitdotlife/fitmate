package com.fitdotlife.fitmate_lib.customview;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.util.TypedValue;
import android.view.View;

import com.fitdotlife.fitmate.R;

public class DayCalorieBarChartView extends View implements AnimatorUpdateListener {

	private final int DEFAULT_MAX_CALORIE = 1000;

	private Paint mSeriesPaint;
	private Paint mTextPaint;
	private float mAniBarValue = 0;
	private int mValue = 0;

	private int mMaxValue = 500;
	private float mMaxValueTextWidth = 0;
	private float mMaxValueWidth = 0;
	private float mSeriesTextBlank = 0;

	protected float mLeftBlank = 0;
	protected float mRightBlank = 0;
	protected int mTopBlank = 0;
	protected int mBottomBlank = 0;
	protected float mTextSize = 0;
	private float initBarWidth;
	private Context mContext = null;
	private float calorieLabelWidth=0;
	private float calorieLabelHeight=0;
	private float mYTitleBlank = 0;

	private Bitmap calorieBitmap;
	private Bitmap[] runmanBitmap;


	private int imageWidthSize;
	private int imageHeightSize;
	private float imageTopmargin;
	private float imageLeftmargin;
	private float imageRightmargin;

	private int mPreValue = 0;

	public DayCalorieBarChartView(Context context)
	{
		super(context);
		mContext= context;
		this.init();
	}

	private void init()
	{
		imageTopmargin = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 6, this.getResources().getDisplayMetrics());
		imageLeftmargin = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 9, this.getResources().getDisplayMetrics());

		imageWidthSize= (int)TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP, 20 , this.getResources().getDisplayMetrics() );
		imageHeightSize= (int)TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP, 22 , this.getResources().getDisplayMetrics() );


		calorieBitmap = BitmapFactory.decodeResource(this.mContext.getResources(), R.drawable.icon_calory);

		calorieBitmap = Bitmap.createScaledBitmap(calorieBitmap, this.imageWidthSize, this.imageHeightSize, false);
		//	calorieBitmap = Bitmap.createScaledBitmap(src, imageWidthSize, imageHeightSize, false);


		runmanBitmap =new Bitmap[2];
		runmanBitmap[0] = BitmapFactory.decodeResource(this.mContext.getResources(), R.drawable.icon_runman_ani1);
		runmanBitmap[0] = Bitmap.createScaledBitmap(runmanBitmap[0], this.imageWidthSize, this.imageHeightSize, false);
		runmanBitmap[1] = BitmapFactory.decodeResource(this.mContext.getResources(), R.drawable.icon_runman_ani2);
		runmanBitmap[1] = Bitmap.createScaledBitmap(runmanBitmap[1], this.imageWidthSize, this.imageHeightSize, false);


		this.initBarWidth = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 36, this.getResources().getDisplayMetrics());	// 76-40=36 왼쪽 시작이 40이라서 뻇음
		this.mTextSize = TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP , 14 , this.getResources().getDisplayMetrics() );
		this.mLeftBlank = TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP, 40 , this.getResources().getDisplayMetrics() );
		this.mRightBlank = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, this.getResources().getDisplayMetrics());
		this.mYTitleBlank = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 7, this.getResources().getDisplayMetrics());

		this.mSeriesPaint = new Paint();
		this.mSeriesPaint.setColor(getResources().getColor(R.color.fitmate_green));
		this.mSeriesPaint.setStyle(Paint.Style.STROKE);
		//this.mSeriesPaint.setStrokeJoin( Paint.Join.ROUND );
		this.mSeriesPaint.setStrokeCap(Paint.Cap.ROUND);
		this.mSeriesPaint.setAntiAlias(true);

		this.mTextPaint = new Paint();
		this.mTextPaint.setColor(0xFF7db933);
		this.mTextPaint.setTextSize(this.mTextSize);

		//�ִ� 5���ڱ��� ǥ�������ϵ���
		this.calorieLabelWidth = getTextWidth("99999 Calorie", mTextPaint);
		calorieLabelHeight = getTextHeight("99999 Calorie", mTextPaint);
		this.mMaxValueTextWidth= calorieLabelWidth;
		this.mSeriesTextBlank = 70;
	}

	@Override
	protected void onMeasure(int widthMeasureSpec , int heightMeasureSpec)
	{
		int heightMode = MeasureSpec.getMode(heightMeasureSpec);
		int heightSize = 0;

		switch( heightMode )
		{
			case MeasureSpec.UNSPECIFIED:
				heightSize = heightMeasureSpec;
				break;
			case MeasureSpec.AT_MOST:
				heightSize = 80;
				break;
			case MeasureSpec.EXACTLY:
				heightSize = MeasureSpec.getSize(heightMeasureSpec);
				break;
		}

		int widthMode = MeasureSpec.getMode(widthMeasureSpec);
		int widthSize = 0;
		switch( widthMode )
		{
			case MeasureSpec.UNSPECIFIED:
				widthSize = widthMeasureSpec;
				break;
			case MeasureSpec.AT_MOST:
				widthSize = 200;
				break;
			case MeasureSpec.EXACTLY:
				widthSize = MeasureSpec.getSize(widthMeasureSpec);
				break;
		}

		this.setMeasuredDimension(widthSize, heightSize);
	}

	protected void onDraw(Canvas canvas)
	{
		this.drawSeries(canvas);
	}

	private void drawSeries( Canvas canvas )
	{
		float width = this.getMeasuredHeight();
		this.mSeriesPaint.setStrokeWidth( width );
		this.mMaxValueWidth = this.getMeasuredWidth() - this.mLeftBlank - this.mRightBlank - this.mSeriesTextBlank - this.mMaxValueTextWidth-initBarWidth;
		float center_y = ( this.getMeasuredHeight() / 2 );


		float barWidth =initBarWidth+ (mMaxValueWidth * mAniBarValue ) / mMaxValue;

		Path barPath = new Path();
		barPath.moveTo(this.mLeftBlank, center_y);
		barPath.lineTo(this.mLeftBlank + barWidth, center_y);
		//barPath.close();

		canvas.drawPath(barPath , mSeriesPaint);

		canvas.drawBitmap(calorieBitmap, this.mLeftBlank - 25, imageTopmargin, null);

		canvas.drawBitmap(runmanBitmap[ ((int)mAniBarValue)%2], this.mLeftBlank + barWidth-imageWidthSize+25,  imageTopmargin, null );

		String valueText = (int)this.mAniBarValue + " " + this.getResources().getString(R.string.common_calorie);
		float valueTextHeight = calorieLabelHeight;
		float textX = this.mLeftBlank + barWidth + this.mSeriesTextBlank;
		/*if( mAniBarValue == 0 ) {
			textX = this.mLeftBlank + barWidth;
		}*/
		float textY = center_y + ( valueTextHeight / 2);

		canvas.drawText( valueText , textX ,  textY , this.mTextPaint);

	}

	public void startAnimation( int duration )
	{
		ValueAnimator animator = ValueAnimator.ofFloat( mPreValue , this.mValue);
		animator.addUpdateListener(this);
		animator.setDuration(duration);
		animator.start();

		this.mPreValue = mValue;
	}

	@Override
	public void onAnimationUpdate(ValueAnimator animation)
	{
		this.mAniBarValue = (float) animation.getAnimatedValue();
		this.invalidate();
	}

	public void setValue( int value )
	{



		this.mValue = value;
	//	this.mValue= 1500;


		if(this.mValue > this.DEFAULT_MAX_CALORIE){
			this.mMaxValue = mValue;
		}else{
			this.mMaxValue= this.DEFAULT_MAX_CALORIE;
		}


	//	String maxValueText = this.mMaxValue + " " +  this.getResources().getString(R.string.txt_calorie);
		//this.mMaxValueTextWidth = this.getTextWidth( "99999" , this.mTextPaint );
	}

	private int getTextWidth(String text, Paint paint)
	{
		Rect bounds = new Rect();
		paint.getTextBounds(text, 0, text.length(), bounds);
		return bounds.width();
	}

	private int getTextHeight(String text, Paint paint)
	{
		Rect bounds = new Rect();
		paint.getTextBounds(text, 0, text.length(), bounds);
		return bounds.height();
	}
}
