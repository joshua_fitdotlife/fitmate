package com.fitdotlife.fitmate_lib.customview;

import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.NinePatch;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;

import com.fitdotlife.fitmate.R;
import org.apache.log4j.Log;

/**
 * Created by Joshua on 2016-03-24.
 */
public class NewHomeMETChartView extends View {

    public enum ChartType{ MET , CALORIE };

    private ChartType mChartType;
    private enum StrengthType{  None , LowStrength , MediumStrength , HighStrength  };
    private int xTextListNumber = 5;

    public static float mediumStrengthRange = 3.0f;
    public static float highStrengthRange = 6.0f;

    private int heightSize = 0;
    private int widthSize = 0;

    private float[] mValues = null;
    private Point[] mPoints = null;
    private int mTouchIndex = 0;

    private float mMaxValue = 0;
    private int mMaxMET = 0;

    private int mHighStrengthRatio = 0;
    private int mMediumStrengthRatio = 3;
    private int mLowStrengthRatio = 2;

    private float mHighStrengthHeight = 0;
    private float mMediumStrengthHeight = 0;
    private float mLowStrengthHeight = 0;

    private float mHighStrengthBottom = 0;
    private float mMediumStrengthBottom = 0;
    private float mLowStrengthBottom = 0;

    private String[] mIntensityCharacterList = null;

    private Context mContext = null;
    private float metAreaWidth = 0;
    private float metSeperatorHeight = 0;

    private float xAxisTextAreaHeight = 0;
    private float xAxisTextSize = 0;

    private float mMetValueLineWidth = 0;

    public static int minutesperPoint=5;

    private int mBackgroundAlpha = 0;
    private int mTextAlpha = 0;

    private MetChartTouchListener  mMetChartTouchListener = null;

    //왼쪽 카테고리 구분의 색깔
    //고강도 색깔.
    public static int highStrengthColor = 0xFFE56244;
    //고강도 색깔.
    public static int mediumStrengthColor = 0xFFED917C;
    //고강도 색깔.
    public static int lowStrengthColor = 0xFFF0D298;

    //강도 구분 텍스트 색깔
    private int metDivisionTextColor = 0x8082868F;

    //xAxisText의 색깔
    private int xAxisTextColor = 0xFF333333;

    private Bitmap strengthTick =  null;


    public interface MetChartTouchListener{

        void onTouch( float x , float y , float met , int metIndex , float chartStartX , float chartWidth );

    }

    public NewHomeMETChartView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        mContext = context;

        metAreaWidth = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 7.5f, this.getResources().getDisplayMetrics());
        metSeperatorHeight = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 0.07f, this.getResources().getDisplayMetrics());

        mMetValueLineWidth = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1, this.getResources().getDisplayMetrics());

        xAxisTextAreaHeight = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 26.33f, this.getResources().getDisplayMetrics());
        xAxisTextSize = TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP , 11 , this.getResources().getDisplayMetrics() );

    }

    public void setMetChartTouchListener( MetChartTouchListener listener ){

        this.mMetChartTouchListener = listener;

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        switch( heightMode )
        {
            case MeasureSpec.UNSPECIFIED:
                heightSize = heightMeasureSpec;
                break;
            case MeasureSpec.AT_MOST:
                heightSize = 200;
                break;
            case MeasureSpec.EXACTLY:
                heightSize = MeasureSpec.getSize(heightMeasureSpec);
                break;
        }

        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        switch( widthMode )
        {
            case MeasureSpec.UNSPECIFIED:
                widthSize = widthMeasureSpec;
                break;
            case MeasureSpec.AT_MOST:
                widthSize = 300;
                break;
            case MeasureSpec.EXACTLY:
                widthSize = MeasureSpec.getSize(widthMeasureSpec);
                break;
        }

        this.setMeasuredDimension(widthSize, heightSize);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if( mMaxValue > highStrengthRange  )


        if( mMaxValue < 8  )
        {
            mHighStrengthRatio = 2;
            mMaxMET = 8;

        }else{
            //1Met부터 시작하기 때문에
            mMaxMET = (int) ( mMaxValue + 1 );
            mHighStrengthRatio = ( mMaxMET - 1 ) - mMediumStrengthRatio - mLowStrengthRatio;
        }

        if( mChartType.equals( ChartType.CALORIE ) ){
            drawDivisionStyle( canvas );
        }else{
            drawTargetStyle( canvas );
        }
    }

    private void drawDivisionStyle( Canvas canvas )
    {
        if( mMaxValue < 8  )
        {
            mHighStrengthRatio = 2;
            mMaxMET = 8;

        }else{
            //1Met부터 시작하기 때문에
            mMaxMET = (int) ( mMaxValue + 1 );
            mHighStrengthRatio = ( mMaxMET - 1 ) - mMediumStrengthRatio - mLowStrengthRatio;
        }

        mediumStrengthRange = 3.0f;
        highStrengthRange = 6.0f;

        //왼쪽 카테고리 구분의 색깔
        //고강도 색깔.
        highStrengthColor = 0xFFE56244;
        //고강도 색깔.
        mediumStrengthColor = 0xFFED917C;
        //고강도 색깔.lowStrengthColor = 0xFFF0D298;

        //강도 구분 텍스트 색깔
        int metDivisionTextColor = 0x8082868F;

        //xAxisText의 색깔
        int xAxisTextColor = 0xFF333333;



        Bitmap popupMetBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.popup_met);
        float metDivisionAreaHeight = this.getMeasuredHeight() - xAxisTextAreaHeight - ( metSeperatorHeight * 2) - popupMetBitmap.getHeight();

        canvas.drawColor(Color.WHITE);
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        canvas.drawBitmap(popupMetBitmap, 0, 0, paint);

        mHighStrengthHeight = ( metDivisionAreaHeight * mHighStrengthRatio ) / ( mMaxMET - 1);
        mMediumStrengthHeight = ( metDivisionAreaHeight * mMediumStrengthRatio ) / ( mMaxMET - 1);
        mLowStrengthHeight = ( metDivisionAreaHeight * mLowStrengthRatio ) / ( mMaxMET - 1);

        mHighStrengthBottom = popupMetBitmap.getHeight() + mHighStrengthHeight;
        mMediumStrengthBottom = mHighStrengthBottom + metSeperatorHeight + mMediumStrengthHeight;
        mLowStrengthBottom = mMediumStrengthBottom + metSeperatorHeight + mLowStrengthHeight;

        drawMetDivisionArea(canvas, popupMetBitmap.getHeight());
        drawMetDivisionText(canvas , popupMetBitmap.getHeight() );
        drawMetDivisionLine(canvas);
        if( mValues != null ) {
            drawMetValueLine(canvas , popupMetBitmap.getHeight() );
        }
    }

    private void drawTargetStyle( Canvas canvas )
    {
        highStrengthColor = 0x4CE56244;
        mediumStrengthColor = 0xFFE56244;;
        lowStrengthColor = 0x4CE56244;;

        if( highStrengthRange < 99 )
        {
            if (mMaxValue < highStrengthRange) //
            {
                mMaxValue = (int) highStrengthRange;
            }
        }

        if( mMaxValue < 8  )
        {
            mMaxMET = 8;
        }else{
            //1Met부터 시작하기 때문에
            mMaxMET = (int) ( mMaxValue + 1 );
        }

        Bitmap halfpopupBitmap = BitmapFactory.decodeResource( getResources() , R.drawable.halfpopup_top );
        drawMetRange(canvas, halfpopupBitmap);

        if( mValues != null )
        {
            drawMetValueLine(canvas, halfpopupBitmap.getHeight());
        }
    }

    private void drawMetRange(Canvas canvas , Bitmap halfPopupBitmap)
    {
        int initialTop = halfPopupBitmap.getHeight();

        float chartHeight = this.getMeasuredHeight() - xAxisTextAreaHeight - initialTop;
        float chartWidth = this.getMeasuredWidth();

        float fromStrengthY = initialTop + chartHeight - (( mediumStrengthRange - 1) * chartHeight / (mMaxMET - 1));
        float toStrengthY = 0;
        if( highStrengthRange >= 99 )
        {
            toStrengthY = initialTop;
        }else{
            toStrengthY = initialTop + chartHeight - ((highStrengthRange - 1) * chartHeight / (mMaxMET - 1));
        }

        float metLineWidth = TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP , 0.5f , getResources().getDisplayMetrics() );
        Paint strengthPaint = new Paint( Paint.ANTI_ALIAS_FLAG );
        strengthPaint.setColor(0x80E56244);
        strengthPaint.setStyle(Paint.Style.STROKE);
        strengthPaint.setStrokeWidth(metLineWidth);

        Paint bitmapPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        Bitmap pattern = BitmapFactory.decodeResource(getResources(), R.drawable.met_popup_pattern);
        canvas.drawBitmap(pattern, 0, 0, bitmapPaint);
        pattern.recycle();

        //매스크를 한다.
        Bitmap maskBitmap = Bitmap.createBitmap( (int)chartWidth , getMeasuredHeight() - initialTop  , Bitmap.Config.ARGB_8888);
        maskBitmap.setDensity(getResources().getDisplayMetrics().densityDpi);
        Canvas maskCanvas = new Canvas(maskBitmap);

        maskCanvas.drawColor(Color.WHITE);
        Paint xferModPaint = new Paint( Paint.ANTI_ALIAS_FLAG );
        xferModPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));

        maskCanvas.drawRect(0, toStrengthY - initialTop, chartWidth, fromStrengthY - initialTop, xferModPaint);
        canvas.drawBitmap(maskBitmap, 0, initialTop, bitmapPaint);

        if(highStrengthRange < 99)
        {
            NinePatch sc = new NinePatch(halfPopupBitmap, halfPopupBitmap.getNinePatchChunk(), "");
            sc.draw(canvas, new Rect(0, 0, (int) chartWidth, initialTop), bitmapPaint);
            halfPopupBitmap.recycle();
        }

        //애니메이션 구간.
        Paint helpBackgroundPaint = new Paint( Paint.ANTI_ALIAS_FLAG );
        helpBackgroundPaint.setColor(0xEB8169);
        helpBackgroundPaint.setAlpha(mBackgroundAlpha);
        canvas.drawRect(0, toStrengthY, chartWidth, fromStrengthY, helpBackgroundPaint);

        Paint helpTextPaint = new Paint( Paint.ANTI_ALIAS_FLAG );
        helpTextPaint.setTextSize(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 19, getResources().getDisplayMetrics()));
        helpTextPaint.setColor(Color.WHITE);
        helpTextPaint.setAlpha(mBackgroundAlpha);
        String helpText = getResources().getString(R.string.met_graph_target_range);
        Rect helpTextBounds = getTextBounds(helpText, helpTextPaint);

        float textY = toStrengthY + ( (fromStrengthY - toStrengthY) / 2 ) + (helpTextBounds.bottom / 2);
        canvas.drawText(helpText, (chartWidth / 2) - (helpTextBounds.right / 2), textY, helpTextPaint);

        if(highStrengthRange >= 99){

            Paint bitmapAnimationPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
            bitmapAnimationPaint.setAlpha(mBackgroundAlpha);

            Bitmap heipTextBitmap = BitmapFactory.decodeResource( getResources() , R.drawable.halfpopup_top_red );
            NinePatch sc = new NinePatch(heipTextBitmap, heipTextBitmap.getNinePatchChunk(), "");
            sc.draw(canvas, new Rect(0, 0, (int) chartWidth, initialTop), bitmapAnimationPaint);
            heipTextBitmap.recycle();
        }

        canvas.drawLine(0, fromStrengthY, chartWidth, fromStrengthY, strengthPaint);
        Bitmap fromStrengthTick = BitmapFactory.decodeResource( getResources() , R.drawable.met_tick );
        canvas.drawBitmap(fromStrengthTick, 0, fromStrengthY - ( fromStrengthTick.getHeight() / 2), bitmapPaint);
        fromStrengthTick.recycle();

        if(highStrengthRange < 99) {
            canvas.drawLine(0, toStrengthY, chartWidth, toStrengthY, strengthPaint);
            Bitmap toStrengthTick = BitmapFactory.decodeResource( getResources() , R.drawable.met_tick );
            canvas.drawBitmap(toStrengthTick, 0, toStrengthY - (toStrengthTick.getHeight() / 2), bitmapPaint);
            toStrengthTick.recycle();

        }

        Paint metTextPaint = new Paint( Paint.ANTI_ALIAS_FLAG );
        metTextPaint.setTextSize(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 14, getResources().getDisplayMetrics()));
        metTextPaint.setColor(Color.WHITE);

        String fromStrengthString = String.valueOf( mediumStrengthRange );

        Rect bounds = new Rect();
        String toStrengthString = String.valueOf( highStrengthRange );
        metTextPaint.getTextBounds(fromStrengthString, 0, fromStrengthString.length(), bounds);
        bounds.offset(0, -bounds.top);

        float fromStrengthHeight = getTextBounds( fromStrengthString , metTextPaint ).bottom;
        float toStrengthHeight = getTextBounds( toStrengthString , metTextPaint ).bottom;

        canvas.drawText( fromStrengthString, 10,  fromStrengthY + ( fromStrengthHeight / 2 )  , metTextPaint);

        if(highStrengthRange < 99)
        {
            canvas.drawText(toStrengthString, 10, toStrengthY + (toStrengthHeight / 2), metTextPaint);
        }

    }

    private void drawMetValueLine( Canvas canvas , int initialTop ){

        float chartLeftBlankWidth = TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP , 13 , this.getResources().getDisplayMetrics() );
        float chartRightBlankWidth = TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP , 13 , this.getResources().getDisplayMetrics() );

        float chartStartX = metAreaWidth + chartLeftBlankWidth;
        float chartWidth = this.getMeasuredWidth() - metAreaWidth - chartLeftBlankWidth - chartRightBlankWidth;
        float chartHeight = this.getMeasuredHeight() - xAxisTextAreaHeight - initialTop;
        float xLocationGap = chartWidth / ( mValues.length - 1 );

        float mediumStrengthY = initialTop + chartHeight - (( mediumStrengthRange - 1) * chartHeight / (mMaxMET - 1));
        float highStrengthY = initialTop + chartHeight - (( highStrengthRange - 1) * chartHeight / (mMaxMET - 1));

        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(mMetValueLineWidth);

        Path path = new Path();
        boolean first = true;

        for( int i = 0 ; i < mValues.length ; i++ )
        {
            //x,y를 계산한다. 1MET부터 시작하므로 값에서도 1을 빼고 최대MET값에서 1을 빼서 Y 값을 계산한다.
            float x = chartStartX + ( xLocationGap * ( i + 1) );

            float y = 0;

            if( mValues[i] < 1  ){
                //값이 1보다 작으면 0의 값으로 세팅한다.
                y = initialTop + chartHeight;
            }else {
                y = initialTop + chartHeight - ((mValues[i] - 1) * chartHeight / (mMaxMET - 1));
            }

            Point currentPoint = new Point( x , y );
            mPoints[ i ] = currentPoint;

            if(first){

                //라인 그리기 시작할 때나 다시 그리기 시작할 때나
                //값이 없으면 그리지 않는다.
                if( mValues[i] < 1 ){
                    continue;
                }

                //라인 그리기 시작할 때나 다시 그리기 시작할 때나
                //값이 있으면 시작 위치로 이동한다.
                first = false;

            }else{

                if( mValues[ i - 1 ] < 1 ){
                    continue;
                }

                Point previousPoint = mPoints[ i - 1 ];

                //각도를 구한다.
                double angle = getAngle( previousPoint.x , previousPoint.y , currentPoint.x , currentPoint.y );

                if(mValues[i-1] < mediumStrengthRange ){ //저강도

                    if( mValues[i] < mediumStrengthRange ){

                        //저강도에서 저강도를 움직이는 그냥 그리면 됨.
                        drawMetLine( canvas , path , previousPoint.x , previousPoint.y , currentPoint.x , currentPoint.y , lowStrengthColor , paint );

                    }else if( mValues[i] < highStrengthRange ){
                        //저강도 구간을 그린다.
                        float mediumX = previousPoint.x +  getX(angle, mediumStrengthY - previousPoint.y );
                        drawMetLine(canvas, path, previousPoint.x, previousPoint.y, mediumX, mediumStrengthY, lowStrengthColor, paint);

                        //중강도 구간을 그린다.
                        drawMetLine(canvas, path, mediumX, mediumStrengthY, currentPoint.x, currentPoint.y, mediumStrengthColor, paint);

                    }else{

                        //저강도 구간을 그린다.
                        float mediumX = previousPoint.x + getX( angle, mediumStrengthY - previousPoint.y);
                        drawMetLine(canvas, path, previousPoint.x, previousPoint.y, mediumX, mediumStrengthY, lowStrengthColor, paint);

                        //중강도 구간을 그린다.
                        float highX = mediumX + getX( angle , mediumStrengthY - highStrengthY );
                        drawMetLine(canvas, path, mediumX, mediumStrengthY, highX, highStrengthY, mediumStrengthColor, paint);

                        //고강도 구간을 그린다.
                        drawMetLine(canvas, path, highX, highStrengthY, currentPoint.x, currentPoint.y, highStrengthColor, paint);

                    }
                }else if( mValues[i-1] < highStrengthRange ){ //중강도

                    if( mValues[i] < mediumStrengthRange ){ //중강도에서 저강도로

                        //중강도
                        float mediumX = previousPoint.x + getX( angle, previousPoint.y - mediumStrengthY);
                        drawMetLine(canvas, path, previousPoint.x, previousPoint.y, mediumX, mediumStrengthY, mediumStrengthColor, paint);

                        //저강도
                        drawMetLine(canvas, path , mediumX, mediumStrengthY, currentPoint.x, currentPoint.y, lowStrengthColor, paint);


                    }else if( mValues[i] < highStrengthRange ){ // 중강도에서 중강도로
                        //같은 구간이니 그냥 그림.
                        drawMetLine(canvas, path, previousPoint.x, previousPoint.y, currentPoint.x, currentPoint.y, mediumStrengthColor, paint );

                    }else{ //중강도에서 고강도로
                        //중강도 구간을 그린다.
                        float highX = previousPoint.x + getX( angle , highStrengthY - previousPoint.y );
                        drawMetLine(canvas, path, previousPoint.x, previousPoint.y, highX, highStrengthY, mediumStrengthColor, paint);

                        //고강도 구간을 그린다.
                        drawMetLine(canvas, path, highX, highStrengthY, currentPoint.x, currentPoint.y, highStrengthColor, paint);

                    }

                }else{ //고강도

                    if( mValues[i] < mediumStrengthRange ) // 고강도에서 저강도로
                    {
                        //고강도
                        float highX = previousPoint.x + getX( angle , previousPoint.y - highStrengthY );
                        drawMetLine(canvas, path  , previousPoint.x, previousPoint.y, highX, highStrengthY, highStrengthColor, paint);

                        //중강도
                        float mediumX = highX + getX( angle , highStrengthY - mediumStrengthY );
                        drawMetLine(canvas, path, highX, highStrengthY, mediumX, mediumStrengthY, mediumStrengthColor, paint);

                        //저강도
                        drawMetLine(canvas, path, mediumX, mediumStrengthY, currentPoint.x, currentPoint.y, lowStrengthColor, paint);

                    }else if( mValues[i] < highStrengthRange ){ //고강도에서 중강도로

                        //고강도
                        float highX = previousPoint.x + getX( angle , previousPoint.y - highStrengthY );
                        drawMetLine(canvas, path, previousPoint.x, previousPoint.y, highX, highStrengthY, highStrengthColor, paint);

                        //중강도
                        drawMetLine(canvas, path, highX, highStrengthY, currentPoint.x, currentPoint.y, mediumStrengthColor, paint);

                    }else{ //고강도에서 고강도로.

                        drawMetLine(canvas, path, previousPoint.x, previousPoint.y, currentPoint.x, currentPoint.y, highStrengthColor, paint );
                    }
                }


                //값이 없으면 모아놨던 Path를 그리고 다시 시작하도록 first를 true로 설정한다.
                if( mValues[ i ] < 1 )
                {
                    first = true;
                }

            }
        }

        //XText List 그리기
        Paint xTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        xTextPaint.setTextSize(xAxisTextSize);
        xTextPaint.setColor(xAxisTextColor);

        float xTextWidth = ( chartWidth / ( xTextListNumber - 1 ) );
        int textHour = 0;
        int hourGap = 24 / ( xTextListNumber - 1 );
        for( int i = 0 ; i < xTextListNumber ; i++ )
        {

            String xText = String.format("%02d", textHour + ( i * hourGap  ) ) + ":00";

            Rect bounds = new Rect();
            xTextPaint.getTextBounds( xText , 0 , xText.length() , bounds );
            bounds.offset( 0, -bounds.top );
            float textHeight = bounds.bottom;

            float textWidth = (float) Math.ceil( xTextPaint.measureText( xText ) );
            float textX = chartStartX + ( (xTextWidth * i ) - ( textWidth / 2 ) );
            float textY = initialTop + chartHeight + ( xAxisTextAreaHeight / 2 ) + ( textHeight / 2 );
            canvas.drawText( xText , textX , textY, xTextPaint );
        }

    }

    private void drawMetDivisionLine( Canvas canvas )
    {
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(0xFFFF0000);
        paint.setStyle(Paint.Style.STROKE);
        paint.setPathEffect(new DashPathEffect(new float[]{ 10, 40, }, 0));
        paint.setStrokeWidth(metSeperatorHeight);

        // 위 구분선을 그린다.
        float upperLineY = mHighStrengthBottom + ( metSeperatorHeight / 2 );
        canvas.drawLine(metAreaWidth, upperLineY, getMeasuredWidth(), upperLineY, paint);

        //아래 구분선을 그린다.
        float lowerlineY = mMediumStrengthBottom + ( metSeperatorHeight / 2);
        canvas.drawLine(metAreaWidth, lowerlineY, getMeasuredWidth(), lowerlineY, paint);

    }

    //고강도 중강도 저강도 텍스트를 그린다.
    private void drawMetDivisionText( Canvas canvas , int initialTop )
    {
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(metDivisionTextColor);
        paint.setTextSize(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 15, this.getResources().getDisplayMetrics()));

        float textX = metAreaWidth + TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP , 13 , this.getResources().getDisplayMetrics());
        float textHeight = paint.descent() + paint.ascent();

        //고강도 텍스트를 그린다.
        canvas.drawText(mIntensityCharacterList[0], textX, initialTop + (mHighStrengthHeight / 2) - (textHeight / 2), paint);

        //중강도 텍스트를 그린다.
        canvas.drawText(mIntensityCharacterList[1], textX, mMediumStrengthBottom - (mMediumStrengthHeight / 2) - (textHeight / 2), paint);

        //저강도 텍스트를 그린다.
        canvas.drawText(mIntensityCharacterList[2], textX, mLowStrengthBottom - (mLowStrengthHeight / 2) - (textHeight / 2), paint);

    }

    //고강도 중강도 저강도 구역을 그린다.
    private void drawMetDivisionArea(Canvas canvas , int initialTop)
    {
        Paint paint = new Paint( Paint.ANTI_ALIAS_FLAG );
        paint.setStyle(Paint.Style.FILL);

        //고강도 구역을 그린다.
        paint.setColor(highStrengthColor);
        canvas.drawRect(0, initialTop, metAreaWidth, mHighStrengthBottom, paint);

        //구분 구역을 그린다.
        paint.setColor(Color.WHITE );
        float upperSeperatorBottom = mHighStrengthBottom + metSeperatorHeight;
        canvas.drawRect(0 , mHighStrengthBottom , metAreaWidth , upperSeperatorBottom , paint );

        //중강도 구역을 그린다.
        paint.setColor(mediumStrengthColor);
        canvas.drawRect(0, upperSeperatorBottom, metAreaWidth, mMediumStrengthBottom, paint);

        //구분 구역을 그린다.
        paint.setColor(Color.WHITE);
        float lowerSeperatorBottom = mMediumStrengthBottom + metSeperatorHeight;
        canvas.drawRect(0, mMediumStrengthBottom, metAreaWidth, lowerSeperatorBottom, paint);

        //저강도 구역을 그린다.
        paint.setColor(lowStrengthColor);
        canvas.drawRect(0, lowerSeperatorBottom, metAreaWidth, mLowStrengthBottom, paint);
    }

    public void setIntensityCharacterList( String[] intensityCharacterList  ){

        this.mIntensityCharacterList = intensityCharacterList;

    }

    public void setValues( float[] values ){

        if( values == null ){
            mValues = null;
            return;
        }

        int dataSavingInterval = 86400 / values.length;
        int countPerPoint = 60 * minutesperPoint / dataSavingInterval;
        float tempsum =0;
        int count_over1=0;
        mValues= new  float[values.length/countPerPoint];

        int lastindex=0;

        float max=1.0f;
        for(int i=0;i<values.length;i+=countPerPoint){
            count_over1=0;
            tempsum=0;
            for(int count=0;count<countPerPoint;count++){
                if(values[i+count]>=1){
                    tempsum+= values[i+count];
                    count_over1++;
                    lastindex = i+count;
                }
            }
            if(count_over1>0){

                mValues[i/countPerPoint]= tempsum/count_over1;

                if(max< mValues[i/countPerPoint]){
                    max= mValues[i/countPerPoint];
                }
            }
        }

        this.mPoints = new Point[ mValues.length ];

        for( int i=0 ; i < mValues.length ; i++ ){

            if( mMaxValue < mValues[i] ){
                mMaxValue = mValues[i];
            }
        }



    }

    private double getAngle( float x1, float y1 , float x2 , float y2 ){

        float dx = x2 - x1;
        float dy = Math.abs( y2 - y1 );

        double rad= Math.atan2( dx, dy );
        //90도를 빼야 맞는 각도가 나온다. ㅠㅜㅠㅜ
        double degree = 90 - ( Math.toDegrees( rad ) );
        return degree;
    }

    private float getX( double angle , float y )
    {
        double radians = Math.toRadians(angle);
        float x = (float) (Math.abs(y) / Math.tan(radians ) );

        return x;
    }

    private float getCenter( float src , float dst ){
        return src + ( ( dst - src ) / 2);
    }

    private void drawMetLine(  Canvas canvas , Path path , float fromX , float fromY , float toX , float toY , int color , Paint paint )
    {
        paint.setColor(color);
        path.moveTo(fromX, fromY);
        path.quadTo(getCenter(fromX, toX), getCenter(fromY, toY), toX, toY);
        canvas.drawPath(path, paint);
        path.reset();
    }

    private Rect getTextBounds( String text, Paint paint) {
        Rect bounds = new Rect();
        paint.getTextBounds(text, 0, text.length(), bounds);
        bounds.offset(0, -bounds.top);
        return bounds;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {

        if( mValues == null )
        {
            return true;
        }

        float touchX = event.getX();
        float chartLeftBlankWidth = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 13, this.getResources().getDisplayMetrics());
        float chartRightBlankWidth = TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP , 13 , this.getResources().getDisplayMetrics() );

        float chartStartX = metAreaWidth + chartLeftBlankWidth;
        float chartWidth = this.getMeasuredWidth() - metAreaWidth - chartLeftBlankWidth - chartRightBlankWidth;
        float chartHeight = this.getMeasuredHeight() - xAxisTextAreaHeight;

        if( touchX < chartStartX || touchX > chartStartX + chartWidth ){
            return true;
        }

        for( int index = 0 ; index < mPoints.length ;index++ ){

            float pointX = mPoints[index].x;
            if( pointX > touchX )
            {

                if( mValues[ index ] >= 1 )
                {
                    mMetChartTouchListener.onTouch(  mPoints[index].x ,  mPoints[index].y , mValues[ index ] , index , chartStartX , chartWidth );
                }

                mTouchIndex = index;
                break;
            }
        }

        return true;
    }

    public void startAnimation()
    {
        ValueAnimator incBackground = ValueAnimator.ofInt(0, 179);
        incBackground.addUpdateListener(new HelpTextBackgroundAnimtaionListener());
        incBackground.setDuration(500);
        ValueAnimator decBackground = ValueAnimator.ofInt(179, 0);
        decBackground.addUpdateListener(new HelpTextBackgroundAnimtaionListener());
        decBackground.setDuration(500);
        AnimatorSet aniBackgroundSet = new AnimatorSet();
        aniBackgroundSet.playSequentially(incBackground, decBackground);
        aniBackgroundSet.start();

//        ValueAnimator incText = ValueAnimator.ofInt( 0, 255 );
//        incText.addUpdateListener(new HelpTextBackgroundAnimtaionListener());
//        incText.setDuration(500);
//        ValueAnimator decText = ValueAnimator.ofInt( 255 , 0 );
//        decText.addUpdateListener( new HelpTextBackgroundAnimtaionListener() );
//        decText.setDuration(500);
//        AnimatorSet aniTextSet = new AnimatorSet();
//        aniTextSet.playSequentially(incText , decText);
//        aniTextSet.start();
    }


    private class HelpTextBackgroundAnimtaionListener implements  ValueAnimator.AnimatorUpdateListener{

        @Override
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            mBackgroundAlpha = (Integer) valueAnimator.getAnimatedValue();
            invalidate();
        }
    }

    private class HelpTextAnimtaionListener implements  ValueAnimator.AnimatorUpdateListener{

        @Override
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            mTextAlpha = (Integer) valueAnimator.getAnimatedValue();
            invalidate();
        }
    }

    public void setMetRange( float fromStrength , float toStrength )
    {
        mediumStrengthRange = fromStrength;
        highStrengthRange = toStrength;
    }

    public void setChartType( ChartType chartType ){
        this.mChartType = chartType;
    }

}