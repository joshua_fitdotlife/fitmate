package com.fitdotlife.fitmate_lib.customview;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.TypedValue;
import android.view.View;

import com.fitdotlife.fitmate.R;

import java.util.ArrayList;
import java.util.List;

public class Day24HourCalorieChartView extends View implements AnimatorUpdateListener
{
    private Context mContext = null;
    private double[] labelValueArray = new double[]{1,2,3,4,5,10,15,20,25,30,40,50,75,100,150,200,250,300,500,1000,1500,2000,2500,5000,10000};

    private int mTopBlank = 10;
    private int mBottomBlank = 10;
    private int mLeftBlank = 10;
    private int mRightBlank = 10;
    private float mLabelBlank=4.67f;
    private float mYTitleTextBlank = 0;

    private int mTopTextBlank = 10;
    private int mBottomTextBlank = 10;
    private int mLeftTextBlank = 10;
    private int mRightTextBlank = 10;

    private int mSeriesBlank = 0;
    private int mSeriesTextBlank = 0;

    private int heightSize = 0;
    private int widthSize = 0;

    private String mYAxisTitle = "kcal";
    private String mXAxisTitle = "시";
    private String[] mXAxisTextList = new String[] {"0", "1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24"};
    private int[] mValues = null;
    private int[] mColors = null;

    private int[] mHashes = null;
    private int[] mBarHeights = null;

    private Paint mAxisPaint = null;
    private Paint mSeriesPaint = null;
    private Paint mTextPaint = null;
    private Paint mxAxisLabelTextPaint = null;
    private Paint mRangePaint = null;

    //칼로리 차트 최대값의 최소값을 100으로 고정
    private int mMaxValue = 100;

    private int mAxisTextSize = 4;
    private int mxAxisLabelSize=4;
    private int mBarTextSize = 0;

    private int mLowRange = 0;
    private int mHighRange = 0;

    private int[] mPreValues = null;

    private List<Animator> mListAnimator = null;

    public Day24HourCalorieChartView(Context context) {
        super(context);
        this.mContext = context;

        this.init();
    }

    private void init()
    {

        this.mLeftBlank = this.getResources().getDimensionPixelSize(R.dimen.week_exercisetime_bar_chart_left_blank);
        this.mRightBlank = this.getResources().getDimensionPixelSize(R.dimen.week_exercisetime_bar_chart_right_blank);
        this.mTopBlank = this.getResources().getDimensionPixelSize(R.dimen.week_exercisetime_bar_chart_top_blank);

        this.mBottomBlank = this.getResources().getDimensionPixelSize(R.dimen.week_exercisetime_bar_chart_bottom_blank);

        this.mLabelBlank =this.getResources().getDimensionPixelSize(R.dimen.chartAxisLabelBlank);
        this.mLeftTextBlank =this.getResources().getDimensionPixelSize(R.dimen.week_exercisetime_bar_chart_text_left_blank);
        this.mRightTextBlank =this.getResources().getDimensionPixelSize(R.dimen.day_intensity_bar_chart_text_right_blank);;
        this.mTopTextBlank = this.getResources().getDimensionPixelSize(R.dimen.week_exercisetime_bar_chart_text_top_blank);
        this.mBottomTextBlank = this.getResources().getDimensionPixelSize(R.dimen.week_exercisetime_bar_chart_text_bottom_blank);
        this.mYTitleTextBlank = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 7, this.getResources().getDisplayMetrics());

        this.mAxisTextSize = this.getResources().getDimensionPixelSize(R.dimen.week_exercisetime_bar_chart_axis_text_size);
        this.mxAxisLabelSize = this.getResources().getDimensionPixelSize(R.dimen.week_exercisetime_bar_chart_xAxis_text_size);
        this.mBarTextSize = this.getResources().getDimensionPixelSize(R.dimen.week_exercisetime_bar_chart_bar_text_size);

        this.mSeriesBlank = this.getResources().getDimensionPixelSize(R.dimen.week_exercisetime_bar_chart_series_blank);
        this.mSeriesTextBlank = this.getResources().getDimensionPixelSize(R.dimen.week_exercisetime_bar_chart_series_text_blank);

        this.mAxisPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.mAxisPaint.setColor( 0xFF939393 );

        this.mSeriesPaint = new Paint();
        this.mSeriesPaint.setTextSize( this.mBarTextSize );

        this.mTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.mTextPaint.setTextSize(this.mAxisTextSize);
        this.mTextPaint.setColor(0xFF666666);

        this.mxAxisLabelTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.mxAxisLabelTextPaint.setTextSize(this.mxAxisLabelSize);
        this.mxAxisLabelTextPaint.setColor(0xFF666666);


        this.mRangePaint = new Paint();
        this.mRangePaint.setColor(0xFFd5f5fa);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        switch( heightMode )
        {
            case MeasureSpec.UNSPECIFIED:
                heightSize = heightMeasureSpec;
                break;
            case MeasureSpec.AT_MOST:
                heightSize = 200;
                break;
            case MeasureSpec.EXACTLY:
                heightSize = MeasureSpec.getSize(heightMeasureSpec);
                break;
        }

        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        switch( widthMode )
        {
            case MeasureSpec.UNSPECIFIED:
                widthSize = widthMeasureSpec;
                break;
            case MeasureSpec.AT_MOST:
                widthSize = 300;
                break;
            case MeasureSpec.EXACTLY:
                widthSize = MeasureSpec.getSize(widthMeasureSpec);
                break;
        }
        this.setMeasuredDimension(widthSize, heightSize);
    }

    @Override
    protected void onDraw( Canvas canvas ) {
        int viewHeight = this.getMeasuredHeight();
        int viewWidth = this.getMeasuredWidth();
        int chartMaxValue = this.mMaxValue + this.mSeriesBlank;



        //XAxis Title 크기 계산
        float xAxisTitleHeight = this.getTextHeight(this.mXAxisTitle, this.mTextPaint);
        float xAxisTitleWidth = this.getTextWidth(this.mXAxisTitle, this.mTextPaint);

        //YAxisTitle 크기 계산
        float yAxisTitleHeight = this.getTextHeight(this.mYAxisTitle, this.mTextPaint);
        float yAxisTitleWidth = this.getTextWidth(this.mYAxisTitle, this.mTextPaint);
        float gap = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, this.getResources().getDisplayMetrics());

        if( yAxisTitleWidth > mLeftTextBlank ){
            mLeftTextBlank = (int)yAxisTitleWidth;
        }

        //차트 크기 계산
        float chartHeight = viewHeight - this.mTopBlank - this.mBottomBlank - mBottomTextBlank - mTopTextBlank - yAxisTitleHeight - xAxisTitleHeight-gap;
        float chartWidth = viewWidth - this.mLeftBlank - this.mRightBlank -mLeftTextBlank - mRightTextBlank ;

        //바의 크기와  바 간격 계산
        float tempBarSpace = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP , 8 , this.getResources().getDisplayMetrics());

        float barWidth = tempBarSpace ;
        float barBlank = gap;

        //차트 외곽선 그리기
        float axisLeft = this.mLeftBlank +  mLeftTextBlank;
        float axisTop = mTopBlank + yAxisTitleHeight + this.mYTitleTextBlank;
        float axisRight = axisLeft + chartWidth;
        float axisBottom = yAxisTitleHeight + mTopBlank + chartHeight + mYTitleTextBlank;

        canvas.drawLine(axisLeft , axisTop , axisLeft , axisBottom , this.mAxisPaint  );
        canvas.drawLine(axisLeft , axisBottom , axisRight , axisBottom , this.mAxisPaint );

        int yTextHeight = this.getTextHeight( String.valueOf(24) , this.mTextPaint );
        float xAxisLevelY= mTopBlank + this.mTopTextBlank + this.mBottomTextBlank + yAxisTitleHeight + chartHeight + yTextHeight;

        //Y축 타이틀과 x축 타이틀 그리기

        canvas.drawText( this.mYAxisTitle , axisLeft- ( this.getTextWidth(this.mYAxisTitle, this.mTextPaint) / 2) , axisTop - mYTitleTextBlank , this.mTextPaint );
        canvas.drawText( this.mXAxisTitle , axisRight - xAxisTitleWidth,  xAxisLevelY+ gap+ this.mTextPaint.getTextSize() ,  this.mTextPaint );

        //Y축 문자들 그리기
        double div = this.getDiv(chartMaxValue);

        int yTextListNumber = (int) (chartMaxValue / div);
        for( int i = 0 ; i < yTextListNumber ; i++ )
        {
            int yTextValue = (int) (( i + 1 ) * div);

            float y = this.mTopBlank + this.mYTitleTextBlank + yAxisTitleHeight + ( yTextHeight / 2 ) + ( chartHeight - ( chartHeight * yTextValue / chartMaxValue) );
            canvas.drawText(   String.valueOf(yTextValue) , mLeftBlank+ mLeftTextBlank-mLabelBlank-this.getTextWidth(String.valueOf(yTextValue), this.mTextPaint) ,  y , this.mTextPaint );
        }

        float drawingWidth = chartWidth- barBlank-barBlank;
        float xgap= drawingWidth/this.mValues.length;

        float startX= this.mLeftBlank + this.mLeftTextBlank +barBlank;

        float halfOfBarWidth= barWidth/2;


        //x축 라벨 그리기
        for( int i = 0 ; i < this.mXAxisTextList.length ; i++ )
        {
            float xTextWidth = this.getTextWidth( this.mXAxisTextList[i] , this.mTextPaint);

            canvas.drawText(this.mXAxisTextList[i], startX - (xTextWidth / 2 )  , xAxisLevelY , this.mxAxisLabelTextPaint );

            //틱마커 그리기


            //      canvas.drawLine(startX, axisBottom, startX, axisBottom+mLabelBlank,  this.mAxisPaint );
            startX += xgap;
        }
        startX= this.mLeftBlank + this.mLeftTextBlank +barBlank;
        //계열 그리기
        int color = this.getResources().getColor(R.color.fitmate_green);
        for( int i = 0 ; i < this.mValues.length ; i++ )
        {
            //  float xTextWidth = this.getTextWidth( this.mXAxisTextList[i] , this.mTextPaint);

            // if( i%3==2)  canvas.drawText(this.mXAxisTextList[i], startX +xgap/2 - (xTextWidth / 2 )  , xAxisLevelY , this.mTextPaint );
            //if(i<23) canvas.drawText(this.mXAxisTextList[i], startX +xgap - (xTextWidth / 2 )  , xAxisLevelY , this.mxAxisLabelTextPaint );
            //    canvas.drawText(this.mXAxisTextList[i], startX - (xTextWidth / 2 )  , xAxisLevelY , this.mxAxisLabelTextPaint );
           /* int color = this.getResources().getColor(R.color.fitmate_yellow);
            if( mBarHeights[i] > this.mLowRange ){  color = this.getResources().getColor(R.color.fitmate_green); }
            if( mLowRange!=mHighRange && mBarHeights[i] > this.mHighRange ){ color = this.getResources().getColor(R.color.fitmate_orange); }*/

            this.mSeriesPaint.setColor( color );

            float left =startX +  xgap/2 -halfOfBarWidth;
            float right =left+ barWidth;
            float top = ( mTopBlank + this.mYTitleTextBlank + yAxisTitleHeight + chartHeight ) -  ( chartHeight * this.mBarHeights[i] / chartMaxValue);
            float bottom = mTopBlank + this.mYTitleTextBlank + yAxisTitleHeight + chartHeight;

            canvas.drawRect(left, top, right, bottom, this.mSeriesPaint );
            startX += xgap;

            /*int valueTextHeight = this.getTextHeight(String.valueOf(this.mBarHeights[i]) , this.mSeriesPaint);
            int valueTextWidth = this.getTextWidth( String.valueOf(this.mBarHeights[i]) , this.mSeriesPaint );
            if( this.mBarHeights[i] > 0 ) {
                canvas.drawText(String.valueOf(this.mBarHeights[i]), right - (barWidth / 2) - ( valueTextWidth / 2), top + this.mSeriesTextBlank - valueTextHeight, this.mSeriesPaint);
            }*/
        }

    }

    public void setYAxisTitle( String yAxisTitle )
    {
        this.mYAxisTitle = yAxisTitle;
    }


    public void setXAxisTitle( String xAxisTitle )
    {
        this.mXAxisTitle = xAxisTitle;
    }

    public void setXAxisTextList( String[] xAxisTextList )
    {
        this.mXAxisTextList = xAxisTextList;
    }

    public void setValues( int[] values )
    {
        this.mValues = values;

        for( int i = 0 ; i < this.mValues.length ; i++ )
        {
            if( this.mMaxValue < mValues[i] )
            {
                this.mMaxValue = mValues[i];
            }
        }
    }

    public void setColors( int[] colors )
    {
        this.mColors = colors;
    }

    private float getTextWidth(String text, Paint paint)
    {
        Rect bounds = new Rect();
        paint.getTextBounds(text, 0, text.length(), bounds);
        return bounds.width();
    }

    private int getTextHeight(String text, Paint paint)
    {
        Rect bounds = new Rect();
        paint.getTextBounds(text, 0, text.length(), bounds);
        return bounds.height();
    }

    public void startAnimation( int duration )
    {
        //List<Animator> listValueAnimator = new ArrayList<Animator>();
        this.mListAnimator = new ArrayList<Animator>();
        //this.mHashes = new int[ this.mValues.length ];
        this.mBarHeights = new int[ this.mValues.length ];


        for( int i = 0 ; i < this.mValues.length ; i++ )
        {

            int preValue = 0;
            if( mPreValues != null ){
                preValue = mPreValues[i];
            }

            ValueAnimator animator = ValueAnimator.ofInt( preValue, this.mValues[i]);
            //this.mHashes[i] = animator.hashCode();
            animator.addUpdateListener( this );

            //istValueAnimator.add(animator);
            this.mListAnimator.add(animator);
        }

        AnimatorSet animatorSet = new AnimatorSet();
        //animatorSet.playTogether( listValueAnimator );
        animatorSet.playTogether(this.mListAnimator);
        animatorSet.setDuration(duration);
        animatorSet.start();

        this.mPreValues = this.mValues;
    }

    @Override
    public void onAnimationUpdate(ValueAnimator animation)
    {
        //int hash = animation.hashCode();
        int index = this.arraySearch( animation );
        if( index != -1 ) {

            this.mBarHeights[index] = (Integer) animation.getAnimatedValue();
            this.invalidate();

        }
    }

    private int arraySearch( Animator srcAnimator )
    {
        int result = -1;
        for( int i = 0 ; i < this.mListAnimator.size() ; i++ )
        {
            Animator animator = this.mListAnimator.get(i);
            if( animator.equals( srcAnimator ) )
            {
                result = i;
                break;
            }
        }
        return result;
    }

    public void setRange( int lowRange , int highRange )
    {
        this.mLowRange = lowRange;
        this.mHighRange = highRange;
    }

    private double getDiv( double maxValue )
    {
        double div = 1;
        boolean found = false;

        for( int i = 0 ; i < this.labelValueArray.length ;i++ )
        {
            if( labelValueArray[i] * 4 > maxValue ){
                div = labelValueArray[i];
                found = true;
                break;
            }
            if (labelValueArray[i] * 5 > maxValue)
            {
                div = labelValueArray[i];
                found = true;
                break;
            }
            if (labelValueArray[i] * 6 > maxValue)
            {
                div = labelValueArray[i];
                found = true;
                break;
            }
        }


        if (!found)
        {
            int u = 1;
            for (int k = 0; ; k++)
            {
                u = u * 10;
                if (u > maxValue)
                {
                    div = u / 10/5;
                    break;
                }
            }
        }
        return div;
    }

}
