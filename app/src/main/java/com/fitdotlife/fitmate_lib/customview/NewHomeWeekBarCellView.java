package com.fitdotlife.fitmate_lib.customview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fitdotlife.fitmate.NewHomeActivity;
import com.fitdotlife.fitmate.R;
import com.fitdotlife.fitmate.newhome.WeekPagerAdapter;
import com.fitdotlife.fitmate_lib.util.Utils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

/**
 * Created by Joshua on 2015-12-18.
 */
@SuppressWarnings("ResourceType")
@EViewGroup(R.layout.child_newhome_week_bar_cell)
public class NewHomeWeekBarCellView extends LinearLayout{

    private Context mContext = null;

    @ViewById(R.id.ll_newhome_week_bar_cell)
    LinearLayout llWeekBar;

    @ViewById(R.id.tv_newhome_week_bar_cell_text)
    TextView tvWeekBarText;

    @ViewById(R.id.view_newhome_week_bar_cell_text_blank)
    View viewBatTextBlank;

    @ViewById( R.id.img_newhome_week_bar )
    ImageView imgWeekBar;

    @ViewById(R.id.tv_newhome_week_bar_cell_weekday)
    TextView tvWeekDay;

    @ViewById( R.id.img_newhome_week_bar_cell_daysel)
    ImageView imgDaySel;

    @ViewById( R.id.tv_newhome_week_bar_cell_day)
    TextView tvDay;

    public NewHomeWeekBarCellView(Context context)
    {
        super(context);
        this.mContext = context;

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT , ViewGroup.LayoutParams.WRAP_CONTENT , 1 );
        layoutParams.gravity = Gravity.CENTER_HORIZONTAL;
        this.setLayoutParams( layoutParams );
    }

    public void setWeekDay( String weekday ){
        this.tvWeekDay.setText(weekday);
    }

    public void setSelectedDay( boolean isSelected )
    {
        if(isSelected){
            tvDay.setTextColor( this.getResources().getColor(R.color.newhome_week_date_sel) );
            imgDaySel.setVisibility( View.VISIBLE );
        }else{
            tvDay.setTextColor( this.getResources().getColor(R.color.newhome_week_date) );
            imgDaySel.setVisibility( View.INVISIBLE );
        }
    }

    public void setWeekBatTextColor( int color ){
        tvWeekBarText.setTextColor(color);
    }

    public void setAchieveImage( Drawable achieveImage){
        imgWeekBar.setBackground(achieveImage);
    }

    public void setDay(String day){
        this.tvDay.setText( day );
    }

    public void setValue( float value , float maxValue ){

        float defaultBarHeight = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8.33f, this.getResources().getDisplayMetrics());

        float barMaxheight = llWeekBar.getLayoutParams().height - tvWeekBarText.getLayoutParams().height - viewBatTextBlank.getLayoutParams().height - defaultBarHeight ;
        float barHeight = ( barMaxheight * value ) / maxValue;

        imgWeekBar.getLayoutParams().height = (int) (barHeight + defaultBarHeight);
        tvWeekBarText.setText( String.format("%d" , (int)value) );
    }

    public void setValue(double value, double maxValue, int pointNumber, OnClickListener clickListener) {

        if( value >= 0 ) {
            float defaultBarHeight = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8.33f, this.getResources().getDisplayMetrics());

            float barMaxheight = llWeekBar.getLayoutParams().height - tvWeekBarText.getLayoutParams().height - viewBatTextBlank.getLayoutParams().height - defaultBarHeight;

            double barHeight = 0;
            if(maxValue > 0){
                barHeight = (barMaxheight * value) / maxValue;
            }

            imgWeekBar.getLayoutParams().height = (int) (barHeight + defaultBarHeight);

            if( clickListener != null ) {
            this.setOnClickListener(clickListener);
            }

            String valueText = null;
            if (pointNumber > 0) {

                valueText = Utils.get3DigitString( value );

            } else {

                valueText = String.format("%d", (int) value);
                
            }

            tvWeekBarText.setText(valueText);
        }else{

            if(value == WeekPagerAdapter.TODAY_AFTER_NODATA)
            {
                imgWeekBar.setVisibility( View.INVISIBLE);
                tvWeekBarText.setVisibility( View.INVISIBLE );
            }else{

                int[] attrs = new int[] { R.attr.newhomeWeekBarEmpty , R.attr.newhomeAchieveEmpty };
                TypedArray ta = mContext.getTheme().obtainStyledAttributes(attrs);

                Drawable emptyDrawable = ta.getDrawable(0);
                int emptyColor = ta.getColor(1,0);

                imgWeekBar.setBackground(emptyDrawable);
                tvWeekBarText.setText("0");
                tvWeekBarText.setTextColor( emptyColor );
                if(clickListener != null) {
                this.setOnClickListener(clickListener);
            }
            }

        }
    }
}
