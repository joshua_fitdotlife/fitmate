package com.fitdotlife.fitmate_lib.customview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;

import org.apache.log4j.Log;

/**
 * Created by Joshua on 2016-03-28.
 */
public class NewHomeMetChartInfoView  extends View {

    private Context mContext = null;

    private int heightSize = 0;
    private int widthSize = 0;

    private float mCircleCenterX = 0;
    private float mCircleCenterY = 0;

    private float mMetValue = 0;
    private int mMetIndex = 0;

    private float mChartStartX = 0;
    private float mChartWidth = 0;

    private float innerCircleRadius = 0;
    private float outerCircleRadius = 0;

    private float mainTextSize = 0;
    private int mainTextColor = 0xFF333333;
    private float subTextSize = 0;
    private int subTextColor = 0xFF9E9E9E;

    public NewHomeMetChartInfoView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;

        innerCircleRadius = TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP , 5 , this.getResources().getDisplayMetrics() );
        outerCircleRadius = TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP , 8 , this.getResources().getDisplayMetrics() );

        mainTextSize = TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP , 14 , this.getResources().getDisplayMetrics() );
        subTextSize = TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP , 8 , this.getResources().getDisplayMetrics() );
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        switch( heightMode )
        {
            case MeasureSpec.UNSPECIFIED:
                heightSize = heightMeasureSpec;
                break;
            case MeasureSpec.AT_MOST:
                heightSize = 200;
                break;
            case MeasureSpec.EXACTLY:
                heightSize = MeasureSpec.getSize(heightMeasureSpec);
                break;
        }

        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        switch( widthMode )
        {
            case MeasureSpec.UNSPECIFIED:
                widthSize = widthMeasureSpec;
                break;
            case MeasureSpec.AT_MOST:
                widthSize = 300;
                break;
            case MeasureSpec.EXACTLY:
                widthSize = MeasureSpec.getSize(widthMeasureSpec);
                break;
        }

        this.setMeasuredDimension(widthSize, heightSize);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        //원을 그린다.
        Paint circlePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        int outerCircleColor = 0;
        if( mMetValue < 1  ){

            return;

        }else if( mMetValue < NewHomeMETChartView.mediumStrengthRange ){
            outerCircleColor = NewHomeMETChartView.lowStrengthColor;
        } else if( mMetValue < NewHomeMETChartView.highStrengthRange ){
            outerCircleColor = NewHomeMETChartView.mediumStrengthColor;
        }else{
            outerCircleColor = NewHomeMETChartView.highStrengthColor;
        }

        circlePaint.setColor( outerCircleColor );
        canvas.drawCircle( mCircleCenterX, mCircleCenterY , outerCircleRadius , circlePaint );

        circlePaint.setColor(Color.WHITE);
        canvas.drawCircle(mCircleCenterX, mCircleCenterY, innerCircleRadius, circlePaint);

        //MET 값을 그린다.
        float topMargin = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP , 22.33f , this.getResources().getDisplayMetrics() );
        Paint metTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        metTextPaint.setTextSize(mainTextSize);
        metTextPaint.setColor(mainTextColor);
        String metText = String.format( "%.2f" , mMetValue ) + " MET";
        float metTextWidth = metTextPaint.measureText(metText);

        float metTextHeight = getTextHeight(metText, metTextPaint);
        canvas.drawText(metText, mChartStartX + (mChartWidth / 2) - (metTextWidth / 2), topMargin + metTextHeight, metTextPaint);

        float textMargin = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP , 6.33f , this.getResources().getDisplayMetrics() );
        //시간을 표시한다.
        Paint timeTextPaint = new Paint( Paint.ANTI_ALIAS_FLAG );
        timeTextPaint.setTextSize(subTextSize);
        timeTextPaint.setColor(subTextColor);

        int hour = ( (mMetIndex + 1 ) * NewHomeMETChartView.minutesperPoint ) / 60;
        int minute = ( ( mMetIndex + 1 ) * NewHomeMETChartView.minutesperPoint ) % 60;

        String timeText = String.format( "%02d" , hour) + ":" + String.format("%02d" , minute);

        float timeTextWidth = timeTextPaint.measureText( timeText );
        float timeTextHeight = getTextHeight( timeText , timeTextPaint );
        canvas.drawText( timeText , mChartStartX + ( mChartWidth / 2 ) - ( timeTextWidth / 2 ) , topMargin + metTextHeight + textMargin + timeTextHeight , timeTextPaint );

    }


    public void setMETInfo(float x, float y, float met, int metIndex , float chartStartX , float chartWidth )
    {
        mCircleCenterX = x;
        mCircleCenterY = y;
        mMetValue = met;
        mMetIndex = metIndex;
        mChartStartX = chartStartX;
        mChartWidth = chartWidth;
    }

    public float getTextHeight( String text , Paint textPaint ){
        Rect bounds = new Rect();
        textPaint.getTextBounds( text , 0, text.length(), bounds);
        bounds.offset( 0, -bounds.top );
        return bounds.bottom;
    }
}
