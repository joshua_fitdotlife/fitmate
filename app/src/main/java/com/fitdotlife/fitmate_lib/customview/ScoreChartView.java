package com.fitdotlife.fitmate_lib.customview;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.view.View;

public class ScoreChartView extends View implements AnimatorUpdateListener
{
	private Context mContext = null;
	private int mValueAngle = 0;
	private Paint valueTextPaint = null;
	
	/**
	 * 생성자
	 * @param context
	 */
	public ScoreChartView(Context context) {
		super(context);
		this.mContext = context;
		this.valueTextPaint = new Paint();
		this.valueTextPaint.setTextSize( 50 );
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec , int heightMeasureSpec)
	{
		int heightMode = MeasureSpec.getMode(heightMeasureSpec);
		int heightSize = 0;
		
		switch( heightMode )
		{
			case MeasureSpec.UNSPECIFIED:
				heightSize = heightMeasureSpec;
				break;
			case MeasureSpec.AT_MOST:
				heightSize = 200;
				break;
			case MeasureSpec.EXACTLY:
				heightSize = MeasureSpec.getSize(heightMeasureSpec);
				break;
		}
		
		int widthMode = MeasureSpec.getMode(widthMeasureSpec);
		int widthSize = 0;
		switch( widthMode )
		{
			case MeasureSpec.UNSPECIFIED:
				widthSize = widthMeasureSpec;
				break;
			case MeasureSpec.AT_MOST:
				widthSize = 300;
				break;
			case MeasureSpec.EXACTLY:
				widthSize = MeasureSpec.getSize(widthMeasureSpec);
				break;
		}
		
		this.setMeasuredDimension( widthSize, heightSize );
	}
	
	@Override
	protected void onDraw(Canvas canvas)
	{   
		this.drawRangeArc( canvas );
		this.drawArcValue( canvas );
		this.drawArrow( canvas );
	}
	
	private void drawRangeArc(Canvas canvas)
	{
		float center_x , center_y;
	    center_x = this.getMeasuredWidth() / 2;
	    center_y = this.getMeasuredHeight() / 2;
	    
	    int radius = 150;
	    
	    //일
	    RectF oval = new RectF( center_x - radius,	center_y - radius, center_x + radius, center_y + radius );
	    Paint paint = new Paint();
	    
	    paint.setColor( Color.YELLOW );
	    canvas.drawArc( oval, 180, 60, true,  paint );
	    paint.setColor( Color.GREEN );
	    canvas.drawArc(oval, 240, 60 , true , paint);
	    paint.setColor( Color.RED );
	    canvas.drawArc(oval, 300 , 60 , true , paint);
	    
	    //주
	    radius = 100;
	    oval = new RectF( center_x - radius,	center_y - radius, center_x + radius, center_y + radius );
	    paint.setColor( Color.WHITE);
	    canvas.drawArc(oval, 180 , 180 , true , paint);
	}
	
	private void drawArrow(Canvas canvas)
	{
		float center_x , center_y;
	    center_x = this.getMeasuredWidth() / 2;
	    center_y = this.getMeasuredHeight() / 2;
		
		Paint paint = new Paint();
		paint.setColor( Color.BLACK );
	    
		canvas.save();
		
		canvas.rotate( this.mValueAngle - 180 , center_x  , center_y );
		
		Path path = new Path();
		path.moveTo( center_x , center_y - 10 );
		path.lineTo( center_x - 50  , center_y - 10 );
		path.lineTo( center_x - 50  , center_y - 30 );
		
		path.lineTo( center_x - 100  , center_y );
		
		path.lineTo( center_x - 50  , center_y + 30 );
		path.lineTo( center_x - 50  , center_y + 10 );
		path.lineTo( center_x  , center_y + 10);
		path.close();
		
		canvas.drawPath( path , paint );
		
		canvas.restore();
	}
	
	private void drawArcValue( Canvas canvas )
	{
		Paint paint = new Paint();
		paint.setColor( Color.BLACK );
		paint.setTextSize( 30 );
		
		int radius = 5;
		
		float center_x , center_y;
	    center_x = this.getMeasuredWidth() / 2;
	    center_y = this.getMeasuredHeight() / 2;
	    
	    double radian = Math.toRadians(this.mValueAngle);
	    
	    float cx = (float) (center_x + ( 160 * Math.cos(radian) ));
	    float cy = (float) (center_y + ( 160 * Math.sin(radian) ));
	    
		canvas.drawCircle( cx , cy , radius , paint );
		canvas.save();
		
		int textWidth = this.getTextWidth( this.mValueAngle + "" , paint );
		int textHeight = this.getTextHeight( this.mValueAngle + "" , paint );
		
		float dx = (float) (center_x + ( ( 165 + ( textWidth / 2 ) ) * Math.cos(radian) ));
	    float dy = (float) (center_y + ( ( 165 + ( textWidth / 2 ) ) * Math.sin(radian) ));
		
	    canvas.rotate( this.mValueAngle + 90 , dx  , dy );
		canvas.drawText( this.mValueAngle + "" , dx - ( textWidth / 2)  , dy + ( textHeight / 2 ) , paint);
	    canvas.restore();    
	}
	
	public void startAnimation( int duration )
	{
		ObjectAnimator animValueText = ObjectAnimator.ofInt(this, "ValueAngle", 180, 320);
		animValueText.addUpdateListener(this);
		animValueText.setDuration( duration );
		animValueText.start();
	}

	@Override
	public void onAnimationUpdate(ValueAnimator animation)
	{
		this.invalidate();
	}
	
	private void setValueAngle( int valueAngle )
	{
		this.mValueAngle = valueAngle;
	}
	
	private int getValueAngle()
	{
		return this.mValueAngle;
	}
	
	
	private int getTextWidth(String text, Paint paint)
	{
		Rect bounds = new Rect();
		paint.getTextBounds(text, 0, text.length(), bounds);
		return bounds.width();
	}
	
	private int getTextHeight(String text, Paint paint)
	{
		Rect bounds = new Rect();
		paint.getTextBounds(text, 0, text.length(), bounds);
		return bounds.height();
	}
	
}
