package com.fitdotlife.fitmate_lib.customview;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.view.View;

import com.fitdotlife.fitmate.R;

import java.util.ArrayList;
import java.util.List;

public class PieChartView extends View implements AnimatorUpdateListener
{
	private Context mContext = null;
	
	private int[] mValues = null;
	private int[] mColors = null;
	private double mValueSum = 0;
	
	private int heightSize = 0;
	private int widthSize = 0;
	
	private Path arcPath = null;
	private Paint arcPaint = null;
    private Paint legendPaint = null;
    private Paint pieTextPaint = null;
	
	private int[] mHashes = null;
	private int[] mAnimationValues = null;

    private int mTopBlank = 0;
    private int mBottomBlank = 0;
    private int mLegendBlank = 0;

    private int mLegendTextSize = 0;
    private int mPieTextSize = 0;
    private int mPieTextBlank = 0;

	public PieChartView(Context context) {
		super(context);
		
		this.mContext = context;
		this.init();
	}
	
	private void init()
	{
        this.mTopBlank = this.getResources().getDimensionPixelSize(R.dimen.week_exercisetime_pie_chart_top_blank);
        this.mBottomBlank = this.getResources().getDimensionPixelSize(R.dimen.week_exercisetime_pie_chart_bottom_blank);
        this.mLegendBlank = this.getResources().getDimensionPixelSize(R.dimen.week_exercisetime_pie_chart_legend_blank);
        this.mLegendTextSize = this.getResources().getDimensionPixelSize(R.dimen.week_exercisetime_pie_chart_legend_text_size);
        this.mPieTextBlank = this.getResources().getDimensionPixelSize(R.dimen.week_exercisetime_pie_chart_pie_text_blank);
        this.mPieTextSize = this.getResources().getDimensionPixelSize(R.dimen.week_exercisetime_pie_chart_pie_text_size);
		this.arcPath = new Path();
		this.arcPaint = new Paint();

        this.legendPaint = new Paint();
        this.legendPaint.setTextSize(this.mLegendTextSize);
        this.legendPaint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));

        this.pieTextPaint = new Paint();
        this.pieTextPaint.setTextSize( this.mPieTextSize );
        this.pieTextPaint.setColor(Color.BLACK);
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) 
	{
		int heightMode = MeasureSpec.getMode(heightMeasureSpec);
		switch( heightMode )
		{
		case MeasureSpec.UNSPECIFIED:
			heightSize = heightMeasureSpec;
			break;
		case MeasureSpec.AT_MOST:
			heightSize = 200;
			break;
		case MeasureSpec.EXACTLY:
			heightSize = MeasureSpec.getSize(heightMeasureSpec);
			break;
		}
		
		int widthMode = MeasureSpec.getMode(widthMeasureSpec);
		switch( widthMode )
		{
		case MeasureSpec.UNSPECIFIED:
			widthSize = widthMeasureSpec;
			break;
		case MeasureSpec.AT_MOST:
			widthSize = 300;
			break;
		case MeasureSpec.EXACTLY:
			widthSize = MeasureSpec.getSize(widthMeasureSpec);
			break;
		}
		
		this.setMeasuredDimension(widthSize, heightSize);
	}
	
	@Override
	protected void onDraw( Canvas canvas )
	{
		int preAngle = 0;

        float radius = ( this.getMeasuredHeight() - this.mTopBlank - this.mBottomBlank ) / 2;

		for( int i = 0 ; i < this.mValues.length ; i++ )
		{
			int color = this.mColors[i];
            int startAngle = preAngle;
			long sweepAngle = Math.round( ( this.mAnimationValues[ i ] * 360 ) / this.mValueSum );
            //long sweepAngle = Math.round( ( this.mValues[i] * 360 ) / this.mValueSum );
			this.drawArc(canvas, color, startAngle, sweepAngle , radius );
            this.drawPieText(canvas , this.mAnimationValues[i] ,color ,  startAngle , sweepAngle , radius );
            //this.drawPieText(canvas , this.mValues[i] ,color ,  startAngle , sweepAngle , radius );
			preAngle += sweepAngle;
		}

        this.drawLegend(canvas , radius);
	}

    private void drawPieText(Canvas canvas, int mAnimationValue, int color ,  int startAngle, long sweepAngle , float radius)
    {
        float center_x = this.getMeasuredWidth() / 2;
        float center_y = this.mTopBlank + radius;

        long pieTextAngle = startAngle + ( sweepAngle / 2 );

        double radian = Math.toRadians( pieTextAngle );

        float cx = (float) (center_x + ( (radius + this.mPieTextBlank) * Math.cos( radian ) ));
        float cy = (float) (center_y + ( (radius + this.mPieTextBlank) * Math.sin( radian ) ));

        this.pieTextPaint.setColor(color);

        Point convertPoint = this.convertPieTextPosition( mAnimationValue+"" , cx , cy , pieTextAngle );

        canvas.drawText( mAnimationValue + "" , convertPoint.x , convertPoint.y , this.pieTextPaint);
    }

    private void drawLegend(Canvas canvas , float radius)
    {

        float center_x = this.getMeasuredWidth() / 2;

        int legendTextHeight = this.getTextHeight("High" , this.legendPaint);

        float legendX = center_x + radius + this.mLegendBlank;
        float legendY = this.getMeasuredHeight() - this.mBottomBlank;
        this.legendPaint.setColor(this.mColors[2]);
        canvas.drawText("High" , legendX , legendY , this.legendPaint );

        legendY = this.getMeasuredHeight() - this.mBottomBlank - ( legendTextHeight * 2 );
        this.legendPaint.setColor(this.mColors[1]);
        canvas.drawText("Medium" , legendX , legendY , this.legendPaint);

        legendY = this.getMeasuredHeight() - this.mBottomBlank - ( legendTextHeight * 4);
        this.legendPaint.setColor(this.mColors[0]);
        canvas.drawText("Low" , legendX , legendY , this.legendPaint);
    }

    private void drawArc(Canvas canvas, int color, int startAngle, long sweepAngle, float radius)
	{
		//파이 그리기
		float center_x = this.getMeasuredWidth() / 2;
		
		RectF oval = new RectF( center_x - radius , this.mTopBlank  , center_x + radius , this.getMeasuredHeight() - this.mBottomBlank);
		this.arcPaint.setColor(color);

		canvas.drawArc( oval, startAngle, sweepAngle, true, this.arcPaint );

	}

	public void setValues( int[] values )
	{
		this.mValues = values;
		
		for( int i = 0 ; i < this.mValues.length ;i++ )
		{
			this.mValueSum += this.mValues[ i ];
		}
	}
	
	public void setColors( int[] colors )
	{
		this.mColors = colors;
	}
	
	public void startAnimation( int duration )
	{
		List<Animator> listValueAnimator = new ArrayList<Animator>();
		this.mHashes = new int[ this.mValues.length ];
		this.mAnimationValues = new int[ this.mValues.length ];
		
		for( int i = 0 ; i < this.mValues.length ; i++ )
		{
			ValueAnimator animator = ValueAnimator.ofInt(0, this.mValues[i]);
			this.mHashes[i] = animator.hashCode();
			animator.addUpdateListener(this);
			
			listValueAnimator.add(animator);
		}
		
		AnimatorSet animatorSet = new AnimatorSet();
		animatorSet.playTogether( listValueAnimator );
		animatorSet.setDuration(duration);
		animatorSet.start();
	}
	
	
	@Override
	public void onAnimationUpdate(ValueAnimator animation)
	{
		int hash = animation.hashCode();
		int index = this.arraySearch( hash );
		
		this.mAnimationValues[ index ] = (Integer) animation.getAnimatedValue();
		
		this.invalidate();	
	}
	
	private int arraySearch( int src )
	{
		int result = -1;
		for( int i = 0 ; i < this.mHashes.length ; i++ )
		{
			if( this.mHashes[i] == src )
			{
				result = i;
				break;
			}
		}
		return result;
	}
	
	private int getTextWidth(String text, Paint paint)
	{
		Rect bounds = new Rect();
		paint.getTextBounds(text, 0, text.length(), bounds);
		return bounds.width();
	}
	
	private int getTextHeight(String text, Paint paint)
	{
		Rect bounds = new Rect();
		paint.getTextBounds(text, 0, text.length(), bounds);
		return bounds.height();
	}

    private Point convertPieTextPosition( String text , float srcX , float srcY , long degree )
    {
        int textHeight = this.getTextHeight(text , this.pieTextPaint);
        int textWidth = this.getTextWidth(text , this.pieTextPaint);

        float addX = 0;
        float addY = 0;

        if( degree > 0 && degree < 90 )
        {
            addY = textHeight;
        }
        else if( degree == 90)
        {
            addX = -(textWidth / 2);
            addY = textHeight;
        }
        else if(degree > 90 && degree < 180)
        {
            addX = -textWidth;
            addY = textHeight;
        }
        else if( degree == 180 )
        {
            addX = -textWidth;
            addY = textHeight / 2;
        }
        else if( degree > 180 && degree < 270 )
        {
            addX = -textWidth;
        }
        else if( degree == 270 )
        {
            addX = -(textWidth / 2);
        }
        else if( degree > 270 && degree < 360 )
        {
            addX = 0;
            addY = 0;
        }

        return new Point( srcX + addX ,srcY + addY );
    }


}
