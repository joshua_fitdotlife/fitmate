package com.fitdotlife.fitmate_lib.customview;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.fitdotlife.fitmate.R;
import com.fitdotlife.fitmate.newhome.CategoryType;
import com.fitdotlife.fitmate.newhome.NewHomeWeekView;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

/**
 * Created by Joshua on 2015-12-18.
 */
@SuppressWarnings("ResourceType")
@EViewGroup( R.layout.child_newhome_week_bar )
public class NewHomeWeekBarView extends RelativeLayout implements ValueAnimator.AnimatorUpdateListener, NewHomeWeekView {

    private Context mContext = null;

    private CategoryType mCategoryType = null;
    private int[] mHashes = null;
    private int[] mBarHeights = null;
    private double[] mValues = null;
    private long[] mTimeMillis = null;

    private double maxValue = 0;
    private String[] mDayStrings = null;

    private int mPointNumber = 0;

    private View.OnClickListener mClickListener = null;

    private float mRangeFrom = 0;
    private float mRangeTo = 0;

    private int mSelectedDayIndex = -1;

    @ViewById( R.id.ll_child_newhome_week_bar )
    LinearLayout llWeekBar;

    public NewHomeWeekBarView(Context context) {
        super(context);
        mContext = context;
    }

//    public void addView( View cellView  ){
//        llWeekBar.addView(cellView);
//    }

    public void startAnimation( int duration )
    {
//        List<Animator> listValueAnimator = new ArrayList<Animator>();
//        this.mHashes = new int[ this.mValues.length ];
//        this.mBarHeights = new int[ this.mValues.length ];
//
//        for( int i = 0 ; i < this.mValues.length ; i++ )
//        {
//
//            ValueAnimator animator = ValueAnimator.ofFloat( 0, this.mValues[i] );
//            this.mHashes[i] = animator.hashCode();
//            animator.addUpdateListener( this );
//
//            listValueAnimator.add(animator);
//        }
//
//        AnimatorSet animatorSet = new AnimatorSet();
//        animatorSet.playTogether( listValueAnimator );
//        animatorSet.setDuration(duration);
//        animatorSet.start();
    }

    @Override
    public void onAnimationUpdate(ValueAnimator animation)
    {
//        int hash = animation.hashCode();
//        int index = this.arraySearch( hash );
//        if( index != -1 ) {
//
//            float animationValue = (Float) animation.getAnimatedValue();
//            NewHomeWeekBarCellView cellView = (NewHomeWeekBarCellView) this.llWeekBar.getChildAt( index );
//            cellView.setValue( animationValue , maxValue );
//            this.invalidate();
//
//        }
    }

    private int arraySearch( int src )
    {
        int result = -1;

        for( int i = 0 ; i < this.mHashes.length ; i++ )
        {
            if( this.mHashes[i] == src )
            {
                result = i;
                break;
            }
        }

        return result;
    }

    public void setListener( View.OnClickListener listener ){
        this.mClickListener = listener;
    }

    public void setValues( double[] values , CategoryType categoryType )
    {
        this.mCategoryType = categoryType;
        this.maxValue = 0;
        this.mValues = values;
        this.mPointNumber = CategoryType.getPointNumber( mCategoryType );

        int defaultValue = CategoryType.getDefaultValue( mCategoryType );

        for( int i = 0 ; i < mValues.length ;i++ ){

            if( maxValue < mValues[i] ){
                maxValue = mValues[i];
    }
        }

        if( maxValue < defaultValue )
        {
            maxValue = defaultValue;
        }

    }

    public void setValues( double[] values , CategoryType categoryType , float strengthFrom , float strengthTo ){
        this.mRangeFrom = strengthFrom;
        this.mRangeTo = strengthTo;
        this.setValues( values , categoryType );
    }

    public void setValues(double[] values, String[] dayStrings, int selectDayIndex, CategoryType categoryType){
        this.mCategoryType = categoryType;
        this.setGraphData(values , dayStrings , selectDayIndex , CategoryType.getPointNumber( categoryType ) , CategoryType.getDefaultValue( categoryType ) );
    }

    public void setValues(double[] values, String[] dayStrings, int selectDayIndex, CategoryType categoryType , float rangeFrom, float rangeTo) {
        this.mCategoryType = categoryType;
        this.mRangeFrom = rangeFrom;
        this.mRangeTo = rangeTo;
        this.setGraphData(values , dayStrings , selectDayIndex , CategoryType.getPointNumber( categoryType ) , CategoryType.getDefaultValue( categoryType ) );
    }

    private void setGraphData( double[] values , String[] dayStrings , int selectDayIndex , int pointNumber , int defaultValue ){
        this.mValues = values;
        this.mDayStrings = dayStrings;
        this.mSelectedDayIndex = selectDayIndex;
        this.mPointNumber = pointNumber;

        for( int i = 0 ; i < mValues.length ;i++ ){

            if( maxValue < mValues[i] ){
                maxValue = mValues[i];
            }
        }

        if( maxValue < defaultValue )
        {
            maxValue = defaultValue;
        }
    }

    public void updateView( int childIndex , double value )
    {
        if( maxValue < value  )
        {
            maxValue = value;
            mValues[ childIndex ] = value;

            for( int i = 0 ; i < 7 ;i++ )
            {
                NewHomeWeekBarCellView cellView = (NewHomeWeekBarCellView) llWeekBar.getChildAt( i );
                updateCellValue( cellView , mValues[i] );
            }

        }else{

            NewHomeWeekBarCellView cellView = (NewHomeWeekBarCellView) llWeekBar.getChildAt( childIndex );
            updateCellValue( cellView , value );
        }
    }

    public void updateView( int[] childList , double[] valueList )
    {
        for( int i = 0 ; i < childList.length ;i++ ){

            int childIndex = childList[ i ];
            mValues[ childIndex ] = valueList[i];

            if( maxValue < mValues[childIndex] ){
                maxValue = mValues[childIndex];
            }
        }

        for( int i = 0 ; i < 7 ;i++ )
        {
            NewHomeWeekBarCellView cellView = (NewHomeWeekBarCellView) llWeekBar.getChildAt( i );
            updateCellValue( cellView , mValues[i] );
        }
    }


    @Override
    public void removeSelectDay() {

        if( mSelectedDayIndex > -1 ){

            NewHomeWeekBarCellView cellView = (NewHomeWeekBarCellView) llWeekBar.getChildAt( mSelectedDayIndex );
            cellView.setSelectedDay( false );
            mSelectedDayIndex = -1;

        }

    }

    @Override
    public void setSelectDay(int childIndex) {

        NewHomeWeekBarCellView cellView = (NewHomeWeekBarCellView) llWeekBar.getChildAt( childIndex );
        cellView.setSelectedDay( true );
        mSelectedDayIndex = childIndex;

    }

    private void updateCellValue( NewHomeWeekBarCellView cellView , double value )
    {

        int[] attrs = new int[] { R.attr.newhomeWeekBarEmpty , R.attr.newhomeWeekBarUnder , R.attr.newhomeWeekBarAchieve , R.attr.newhomeWeekBarOver ,
            R.attr.newhomeAchieveEmpty , R.attr.newhomeAchieveUnder , R.attr.newhomeAchieve , R.attr.newhomeAchieveOver};
        TypedArray ta = mContext.getTheme().obtainStyledAttributes(attrs);

        Drawable emptyDrawable = ta.getDrawable(0);
        Drawable underDrawable = ta.getDrawable(1);
        Drawable achieveDrawable = ta.getDrawable(2);
        Drawable overDrawable = ta.getDrawable(3);
        int emptyColor = ta.getColor(4,0);
        int underColor = ta.getColor(5,0);
        int achieveColor = ta.getColor(6,0);
        int overColor = ta.getColor(7,0);

        ta.recycle();

        if( mCategoryType.equals( CategoryType.DAILY_ACHIEVE ) ){

            if( mRangeFrom == mRangeTo )
            {
                if( value < mRangeTo ){
                    cellView.setAchieveImage( underDrawable );
                    cellView.setWeekBatTextColor( underColor );
                }else{
                    cellView.setAchieveImage( achieveDrawable );
                    cellView.setWeekBatTextColor( achieveColor );
                }

            }else {

                if( value < mRangeFrom )
                {
                    cellView.setAchieveImage( underDrawable );
                    cellView.setWeekBatTextColor( underColor );
                }else if( value <= mRangeTo ){
                    cellView.setWeekBatTextColor( achieveColor );
                    cellView.setAchieveImage( achieveDrawable );
                }else{
                    cellView.setWeekBatTextColor( overColor );
                    cellView.setAchieveImage( overDrawable );
                }
            }

        }else{

            cellView.setAchieveImage( achieveDrawable );
            cellView.setWeekBatTextColor( achieveColor );

        }

        cellView.setValue( value , maxValue , mPointNumber , mClickListener );
    }


    public void display()
    {

        for( int i = 0 ; i < 7 ;i++ )
        {
            NewHomeWeekBarCellView cellView = NewHomeWeekBarCellView_.build( mContext );
            cellView.setDay(mDayStrings[i]);
            cellView.setWeekDay(getResources().getStringArray(R.array.week_char)[i]);
            updateCellValue( cellView , mValues[i] );

            cellView.setTag( R.string.newhome_weekview_time_key , mTimeMillis[i] );
            cellView.setTag( R.string.newhome_weekview_cellindex_key , i );

            if( mSelectedDayIndex == i ) {
                cellView.setSelectedDay( true );
            }

            llWeekBar.addView(cellView , i);
        }
    }

    public void refresh(){
        for( int i = 0 ; i < 7 ;i++ )
        {
            NewHomeWeekBarCellView cellView = (NewHomeWeekBarCellView) llWeekBar.getChildAt( i );
            updateCellValue(cellView, mValues[i]);
        }
    }

    public void setDays(long[] timeMillis) {
        mTimeMillis = timeMillis;
    }
}
