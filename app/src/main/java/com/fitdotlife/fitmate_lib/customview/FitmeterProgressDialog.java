package com.fitdotlife.fitmate_lib.customview;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.fitdotlife.fitmate.R;

/**
 * Created by Joshua on 2016-08-24.
 */
public class FitmeterProgressDialog {

    private Context mContext = null;
    private Dialog mDialog = null;
    private ImageView imgDialog = null;
    private TextView tvMessage = null;

    public FitmeterProgressDialog(Context context ){
        this.mContext = context;
        this.mDialog = new Dialog( mContext , R.style.Theme_DialogFullScreen  );

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        View exitView = inflater.inflate(R.layout.dialog_fitmeter_progress, null);
        imgDialog = (ImageView) exitView.findViewById(R.id.img_dialog_fitmeter_progress);
        tvMessage = (TextView) exitView.findViewById( R.id.tv_dialog_fitmeter_progress_message );

        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        //mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(0xD9000000));
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setContentView(exitView);

        imgDialog.startAnimation(AnimationUtils.loadAnimation( mContext , R.anim.dialog_fitmeter_progress));
    }

    public void setMessage(String message){
        tvMessage.setText( message );
    }

    public void setCancelable( boolean cancelable ){
        mDialog.setCancelable( cancelable );
    }

    public void show(){
        mDialog.show();
    }

    public void dismiss(){
        mDialog.dismiss();
    }

    public boolean isShowing(){
        return mDialog.isShowing();
    }

}
