package com.fitdotlife.fitmate_lib.customview;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.widget.DrawerLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fitdotlife.fitmate.ActivityManager;
import com.fitdotlife.fitmate.ConfigureLog4J;
import com.fitdotlife.fitmate.ExerciseProgramHomeActivity;
import com.fitdotlife.fitmate.FriendActivity_;
import com.fitdotlife.fitmate.HelpActivity;
import com.fitdotlife.fitmate.HelpListActivity;
import com.fitdotlife.fitmate.MainActivity;
import com.fitdotlife.fitmate.MainAsActivity_;
import com.fitdotlife.fitmate.MyApplication;
import com.fitdotlife.fitmate.MyProfileActivity;
import com.fitdotlife.fitmate.NewHomeActivity;
import com.fitdotlife.fitmate.NewHomeActivity_;
import com.fitdotlife.fitmate.NewHomeAsActivity_;
import com.fitdotlife.fitmate.NewHomeTutorialActivity_;
import com.fitdotlife.fitmate.NewsActivity_;
import com.fitdotlife.fitmate.R;
import com.fitdotlife.fitmate.ServiceAgreementActivity;
import com.fitdotlife.fitmate.SettingActivity_;
import org.apache.log4j.Log;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.http.LogService;
import com.fitdotlife.fitmate_lib.http.NetworkClass;
import com.fitdotlife.fitmate_lib.key.GenderType;
import com.fitdotlife.fitmate_lib.object.LogInfo;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.presenter.LoginPresenter;
import com.fitdotlife.fitmate_lib.util.Utils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.rest.spring.annotations.RestService;
import org.springframework.web.client.RestClientException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Joshua on 2015-08-17.
 */
@EViewGroup(R.layout.child_drawer_layout)
public class DrawerView extends LinearLayout
{
    private String mTag = null;

    public static final int REQUEST_PROFILE_IMAGE = 55;
    public static final int REQUEST_SETTING = 56;

    private Context mContext = null;
    private FitmateDBManager mDBManager = null;
    private Activity mParentActivity = null;
    private FitmeterProgressDialog mProgressDialog;
    private FitmeterDialog mDialog = null;

    private DrawerLayout mDrawerLayout = null;

    @ViewById(R.id.img_child_drawer_layout_profile)
    ImageView imgProfile;

    @ViewById(R.id.tv_child_drawer_layout_name)
    TextView tvName;

    @ViewById(R.id.tv_child_drawer_layout_email)
    TextView tvEmail;

    @RestService
    LogService logServiceClient;

    public DrawerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
    }

    @AfterViews
    void init()
    {

        mDBManager = new FitmateDBManager(this.mContext);
        if( !this.isInEditMode() ) {
            if (mDBManager != null)
            {

                UserInfo userInfo = mDBManager.getUserInfo();

                tvEmail.setText(userInfo.getEmail());
                tvName.setText(userInfo.getName());

                refreshProfileImage();
            }
        }
    }

    public void setDrawerLayout( DrawerLayout drawerLayout)
    {
        mDrawerLayout = drawerLayout;
    }


    @Click(R.id.ll_child_drawer_layout_home)
    void homeClick()
    {
        String tag = (String) this.getTag();
        if( tag.equals(mParentActivity.getResources().getString(R.string.gnb_home)) ) {
            mDrawerLayout.closeDrawer(this);
            return;
        }

        if(this.getSettedUserInfo()) {
            this.moveActivity(NewHomeActivity_.class);
            Intent intent = new Intent( mContext , NewHomeActivity_.class );
            intent.putExtra( NewHomeActivity.HOME_LAUNCH_KEY , NewHomeActivity.LaunchMode.FROM_OTHERMENU.ordinal() );
            mParentActivity.startActivity( intent );
        }else{
            this.moveActivity(NewHomeAsActivity_.class);
        }

        if( !this.getHomeNotView() ){
            this.showTutorialActivity();
        }

        ActivityManager.getInstance().finishAllActivity();
    }

//    @Click(R.id.ll_child_drawer_layout_activity)
//    void activityClick()
//    {
//        String tag = (String) this.getTag();
//
//        if( tag.equals(mParentActivity.getResources().getString(R.string.gnb_activity)) ) {
//            mDrawerLayout.closeDrawer(this);
//            return;
//
//        }
//
//        if(this.getSettedUserInfo()) {
//            this.moveActivity(MainActivity.class);
//        }else {
//            this.moveActivity(MainAsActivity_.class);
//        }
//
//        if( !this.getActivityNotView() ){
//            this.showHelpActivity(HelpActivity.ACTIVITY_HELP_MODE);
//        }
//
//        ActivityManager.getInstance().finishAllActivity();
//
//        mDrawerLayout.closeDrawer(this);
//    }

    @Click(R.id.ll_child_drawer_layout_exercise_program)
    void exerciseProgramClick()
    {
        String tag = (String) this.getTag();

        if( tag.equals(mParentActivity.getResources().getString(R.string.gnb_exerciseprogram)) ) {
            mDrawerLayout.closeDrawer(this);
            return;
        }

        this.moveActivity(ExerciseProgramHomeActivity.class);

        //if( !this.getExerciseNotView() ){
        //    this.showHelpActivity( HelpActivity.EXERCISE_HELP_MODE );
        //}

        ActivityManager.getInstance().finishAllActivity();
        mDrawerLayout.closeDrawer(this);
    }

    @Click(R.id.ll_child_drawer_layout_friend)
    void friendClick(){
        String tag = (String) this.getTag();
        if( tag.equals(mParentActivity.getResources().getString(R.string.gnb_friend)) ) {
            mDrawerLayout.closeDrawer(this);
            return;
        }
        this.moveActivity( FriendActivity_.class );

        ActivityManager.getInstance().finishAllActivity();
        mDrawerLayout.closeDrawer(this);
    }

    @Click(R.id.ll_child_drawer_layout_news)
    void newsClick(){

        String tag = (String) this.getTag();

        if( tag.equals(mParentActivity.getResources().getString(R.string.gnb_news)) ) {
            mDrawerLayout.closeDrawer(this);
            return;
        }
        this.moveActivity( NewsActivity_.class );

        ActivityManager.getInstance().finishAllActivity();
        mDrawerLayout.closeDrawer(this);
    }

    @Click(R.id.ll_child_drawer_layout_setting)
     void settingClick(){
        Intent settingIntent = new Intent( mContext, SettingActivity_.class );
        mParentActivity.startActivityForResult(settingIntent, REQUEST_SETTING);
    }

    @Click(R.id.ll_child_drawer_layout_service_agreement)
    void serviceAgreementClick(){
        Intent serviecAgreementIntent = new Intent(mContext, ServiceAgreementActivity.class);
        mContext.startActivity(serviecAgreementIntent);
    }

    @Click(R.id.ll_child_drawer_layout_report)
    void reportClick(){

        if(!Utils.isOnline(mContext)){
            Toast.makeText(mContext, mContext.getString(R.string.common_connect_network), Toast.LENGTH_SHORT).show();
            return;
        }

        mDialog = new FitmeterDialog( mContext);
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        View contentView = inflater.inflate( R.layout.dialog_report_error , null );

        final EditText etContent = (EditText) contentView.findViewById(R.id.etx_dialog_search_password_search_content);
        mDialog.setContentView(contentView);

        View buttonView = inflater.inflate( R.layout.dialog_button_two , null );
        Button btnLeft = (Button) buttonView.findViewById(R.id.btn_dialog_button_two_left);
        btnLeft.setText(mContext.getString(R.string.common_cancel));
        btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.close();
            }
        });

        Button btnRight = (Button) buttonView.findViewById(R.id.btn_dialog_button_two_right);
        btnRight.setText( mContext.getString( R.string.help_send_error_report ) );

        btnRight.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                showProgressDialog();

                String version = "0.0.0";
                PackageInfo pInfo = null;
                try {
                    pInfo = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0);
                    version = pInfo.versionName;
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }

                final File file = new File(ConfigureLog4J.FilePath);
                if (!file.exists() || !file.canRead()) {
                    Toast.makeText(mContext, "Attachment Error", Toast.LENGTH_SHORT).show();
                    //finish();
                    return;
                }

                FileInputStream fr = null;
                try {
                    fr = new FileInputStream(file);
                } catch (FileNotFoundException e) {
                    Toast.makeText(mContext, "Attachment Error", Toast.LENGTH_SHORT).show();
                    return;
                }

                StringBuffer sb = new StringBuffer();
                try {

                    BufferedReader br = new BufferedReader(new InputStreamReader(fr));
                    int character;
                    while( ( character = br.read() ) != -1 )
                    {
                        sb.append((char) character);
                    }

                    br.close();

                }catch( IOException e){

                }

                final LogInfo logInfo = new LogInfo();
                logInfo.setAppversion(version);
                logInfo.setOsversion(System.getProperty("os.version"));
                logInfo.setContent(etContent.getText().toString());
                logInfo.setAttachment(sb.toString());
                logInfo.setModel(Build.MODEL);
                logInfo.setSdkversion(android.os.Build.VERSION.SDK_INT + "");
                logInfo.setUsername(mDBManager.getUserInfo().getName());
                logInfo.setEmail(mDBManager.getUserInfo().getEmail());
                logInfo.setTagnum(MyApplication.TAG_NUM);

                sendLog(logInfo);
            }
        });
        mDialog.setButtonView(buttonView);
        mDialog.show();

        //startActivity(Intent.createChooser(intent, "Send Email"));
        return;
    }

    @Background
    void sendLog( LogInfo logInfo ){
        try {
            logServiceClient.sendLog(logInfo);
            showMessage(mContext.getString(R.string.help_send_report_success), Toast.LENGTH_SHORT);
        }catch(RestClientException e){
            showMessage(mContext.getString(R.string.help_send_report_fail), Toast.LENGTH_SHORT);
        }

        closeDialog();
        closeProgressDialog();
    }

    @UiThread
    void closeDialog(){
        mDialog.close();
    }

    @UiThread
    void showMessage( String message , int duration) {
        Toast.makeText( mContext , message , duration ).show();
    }


    @Click(R.id.ll_child_drawer_layout_help)
    void helpClick(){
        Intent helpIntent = new Intent(mContext , HelpListActivity.class);
        mContext.startActivity(helpIntent);
    }

    @Click(R.id.ll_child_drawer_layout_userinfo)
    void userInfoClick(){
        Intent myProfileIntent = new Intent(mContext, MyProfileActivity.class);
        //mContext.startActivity(myProfileIntent);
        mParentActivity.startActivityForResult(myProfileIntent, REQUEST_PROFILE_IMAGE);
    }


    public void refreshProfileImage(){
        UserInfo userInfo = mDBManager.getUserInfo();

        String imageName = userInfo.getAccountPhoto();
        if( imageName != null )
        {
            String imgUri = null;
            if (imageName.startsWith("file://")) {
                imgUri = imageName;
            } else {
                imgUri = NetworkClass.imageBaseURL + imageName;
            }

            DisplayImageOptions options = new DisplayImageOptions.Builder()
                    .showImageForEmptyUri(R.drawable.profile_img1)
                    .showImageOnFail(R.drawable.profile_img1)
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .displayer(new RoundedBitmapDisplayer(300))
                    .build();

            ImageLoader.getInstance().displayImage(imgUri, imgProfile, options);
            imgProfile.invalidate();
        }
    }

    public void setParentActivity(Activity activity){
        this.mParentActivity = activity;
    }


    void showProgressDialog(){
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        this.mProgressDialog =  new FitmeterProgressDialog( mContext );
        mProgressDialog.setMessage( mContext.getString(R.string.common_wait) );

        mProgressDialog.show();
    }

    void closeProgressDialog(){
        if ( mProgressDialog!=null )
        {
            mProgressDialog.dismiss();
        }
    }

    private void moveActivity(Class<?> cls)
    {
        Intent intent = new Intent( this.mParentActivity , cls );
        this.mParentActivity.getWindow().setWindowAnimations(0);
        this.mParentActivity.startActivity(intent);
    }

    private void showHelpActivity( int mode )
    {
        Intent intent = new Intent( this.mParentActivity , HelpActivity.class );
        intent.putExtra( HelpActivity.MODE_KEY, mode );
        intent.putExtra(HelpActivity.VISIBLE_KEY, true);
        this.mParentActivity.startActivity(intent);
    }

    private boolean getSettedUserInfo()
    {
        SharedPreferences pref =  this.mParentActivity.getSharedPreferences("fitmateservice", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        return pref.getBoolean( LoginPresenter.SET_USERINFO_KEY , false);
    }

    private boolean getHomeNotView(){
        return getNotView(HelpActivity.HOME_NOT_VIEW_KEY);
    }

    private boolean getActivityNotView(){
        return getNotView(HelpActivity.ACTIVITY_NOT_VIEW_KEY);
    }

    private boolean getExerciseNotView(){
        return getNotView( HelpActivity.EXERCISE_NOT_VIEW_KEY );
    }

    private boolean getNotView(String key){
        SharedPreferences pref = mParentActivity.getSharedPreferences( "fitmateservice" , Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        return pref.getBoolean(key, false);
    }

    private void showTutorialActivity(){
        Intent intent = new Intent( this.mParentActivity , NewHomeTutorialActivity_.class );
        this.mParentActivity.startActivity(intent);
    }


}
