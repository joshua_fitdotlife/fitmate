package com.fitdotlife.fitmate_lib.customview;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fitdotlife.fitmate.R;
import com.fitdotlife.fitmate_lib.http.NetworkClass;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

/**
 * Created by Joshua on 2015-07-31.
 */
@EViewGroup(R.layout.child_activity_friend_profile)
public class FriendProfileCommonView extends LinearLayout{

    @ViewById(R.id.img_child_activity_friend_profile)
    ImageView imgProfile;

    @ViewById(R.id.txt_child_activity_friend_profile_name)
    TextView txtName;

    public FriendProfileCommonView(Context context ,  AttributeSet attrs) {
        super(context , attrs);
    }

    public void setName( String name ){
        txtName.setText(name);
    }

    public void setProfileImage(String imageName){

        int cornerRadius  = imgProfile.getLayoutParams().width / 2;

        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.profile_img1)
                .showImageOnFail(R.drawable.profile_img1)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .displayer(new RoundedBitmapDisplayer( cornerRadius ))
                .build();

        ImageLoader.getInstance().displayImage(NetworkClass.imageBaseURL + imageName, imgProfile, options);
    }
}
