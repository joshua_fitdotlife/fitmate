package com.fitdotlife.fitmate_lib.customview;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.View;

import com.fitdotlife.fitmate.R;

import java.util.ArrayList;
import java.util.List;

public class MultipleBarChartView_copy extends View implements AnimatorUpdateListener
{
	private Context mContext = null;

	private int mTopBlank = 10;
	private int mBottomBlank = 10;
	private int mLeftBlank = 10;
	private int mRightBlank = 10;

    private int mTopTextBlank = 10;
    private int mBottomTextBlank = 10;
    private int mLeftTextBlank = 10;
    private int mRightTextBlank = 10;

    private int mSeriesBlank = 0;
    private int mSeriesTextBlank = 0;

    private int heightSize = 0;
	private int widthSize = 0;

	private String mYAxisTitle = "";
	private String mXAxisTitle = "";
	private String[] mXAxisTextList = null;
	private int[] mValues = null;
	private int[] mColors = null;

	private int[] mHashes = null;
	private int[] mBarHeights = null;

	private Paint mAxisPaint = null;
	private Paint mSeriesPaint = null;
	private Paint mTextPaint = null;
    private Paint mRangePaint = null;

	private int mMaxValue = 0;

	private int mAxisTextSize = 7;
    private int mBarTextSize = 0;

    private int mLowRange = 0;
    private int mHighRange = 0;

	public MultipleBarChartView_copy(Context context) {
		super(context);
		this.mContext = context;
		
		this.init();
	}
	
	private void init()
	{
		this.mLeftBlank = this.getResources().getDimensionPixelSize(R.dimen.week_exercisetime_bar_chart_left_blank);
        this.mRightBlank = this.getResources().getDimensionPixelSize(R.dimen.week_exercisetime_bar_chart_right_blank);
        this.mTopBlank = this.getResources().getDimensionPixelSize(R.dimen.week_exercisetime_bar_chart_top_blank);
        this.mBottomBlank = this.getResources().getDimensionPixelSize(R.dimen.week_exercisetime_bar_chart_bottom_blank);

        this.mLeftTextBlank = this.getResources().getDimensionPixelSize(R.dimen.week_exercisetime_bar_chart_text_left_blank);
        this.mRightTextBlank = this.getResources().getDimensionPixelSize(R.dimen.week_exercisetime_bar_chart_text_right_blank);
        this.mTopTextBlank = this.getResources().getDimensionPixelSize(R.dimen.week_exercisetime_bar_chart_text_top_blank);
        this.mBottomTextBlank = this.getResources().getDimensionPixelSize(R.dimen.week_exercisetime_bar_chart_text_bottom_blank);

        this.mAxisTextSize = this.getResources().getDimensionPixelSize(R.dimen.week_exercisetime_bar_chart_axis_text_size);
        this.mBarTextSize = this.getResources().getDimensionPixelSize(R.dimen.week_exercisetime_bar_chart_bar_text_size);

        this.mSeriesBlank = this.getResources().getDimensionPixelSize(R.dimen.week_exercisetime_bar_chart_series_blank);
        this.mSeriesTextBlank = this.getResources().getDimensionPixelSize(R.dimen.week_exercisetime_bar_chart_series_text_blank);

        this.mAxisPaint = new Paint();

		this.mSeriesPaint = new Paint();
        this.mSeriesPaint.setTextSize( this.mBarTextSize );

        this.mTextPaint = new Paint();
        this.mTextPaint.setTextSize(this.mAxisTextSize);
        this.mTextPaint.setColor(0xFF666666);

        this.mRangePaint = new Paint();
        this.mRangePaint.setColor(0xFFd5f5fa);
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) 
	{
		int heightMode = MeasureSpec.getMode(heightMeasureSpec);
		switch( heightMode )
		{
		case MeasureSpec.UNSPECIFIED:
			heightSize = heightMeasureSpec;
			break;
		case MeasureSpec.AT_MOST:
			heightSize = 200;
			break;
		case MeasureSpec.EXACTLY:
			heightSize = MeasureSpec.getSize(heightMeasureSpec);
			break;
		}
		
		int widthMode = MeasureSpec.getMode(widthMeasureSpec);
		switch( widthMode )
		{
		case MeasureSpec.UNSPECIFIED:
			widthSize = widthMeasureSpec;
			break;
		case MeasureSpec.AT_MOST:
			widthSize = 300;
			break;
		case MeasureSpec.EXACTLY:
			widthSize = MeasureSpec.getSize(widthMeasureSpec);
			break;
		}
		
		this.setMeasuredDimension(widthSize, heightSize);
	}
	
	@Override
	protected void onDraw( Canvas canvas )
	{
		int viewHeight = this.getMeasuredHeight();
		int viewWidth = this.getMeasuredWidth();
        int chartMaxValue = this.mMaxValue + this.mSeriesBlank;

		//XAxis Title 크기 계산
		int xAxisTitleHeight = this.getTextHeight( this.mXAxisTitle  , this.mTextPaint );
		int xAxisTitleWidth = this.getTextWidth( this.mXAxisTitle , this.mTextPaint );
		
		//YAxisTitle 크기 계산
		int yAxisTitleHeight = this.getTextHeight( this.mYAxisTitle  , this.mTextPaint );
		int yAxisTitleWidth = this.getTextWidth( this.mYAxisTitle , this.mTextPaint );
		
		//차트 크기 계산
		int chartHeight = viewHeight - this.mTopBlank - this.mBottomBlank - mBottomTextBlank - mTopTextBlank - yAxisTitleHeight - xAxisTitleHeight;
		int chartWidth = viewWidth - this.mLeftBlank - this.mRightBlank - - mLeftTextBlank - mRightTextBlank - yAxisTitleWidth - xAxisTitleWidth;
		
		//바의 크기와  바 간격 계산
		float tempBarSpace = chartWidth / ( ( this.mValues.length * 2 ) + ( this.mValues.length + 1 ) );
		float barWidth = tempBarSpace * 2;
		float barBlank = tempBarSpace;

        //범위 그리기
        int rangeLeft  = this.mLeftBlank + yAxisTitleWidth + mLeftTextBlank;
        int rangeRight = mLeftBlank + mLeftTextBlank + yAxisTitleWidth + chartWidth;
        int rangeTop = ( mTopBlank + this.mTopTextBlank + yAxisTitleHeight + chartHeight ) -  ( chartHeight * this.mHighRange/ chartMaxValue);
        int rangeBottom = ( mTopBlank + this.mTopTextBlank + yAxisTitleHeight + chartHeight ) -  ( chartHeight * this.mLowRange / chartMaxValue);
        canvas.drawRect(rangeLeft , rangeTop , rangeRight , rangeBottom , this.mRangePaint );

		//차트 외곽선 그리기
        int axisLeft = rangeLeft;
        int axisTop = yAxisTitleHeight + mTopBlank + mTopTextBlank;
        int axisRight = rangeRight;
        int axisBottom = yAxisTitleHeight + mTopBlank + chartHeight + mTopTextBlank;

        canvas.drawLine(axisLeft , axisTop , axisLeft , axisBottom , this.mAxisPaint  );
        canvas.drawLine(axisLeft , axisBottom , axisRight , axisBottom , this.mAxisPaint );
		
		//Y축 타이틀과 x축 타이틀 그리기
		canvas.drawText( this.mYAxisTitle , mLeftBlank , mTopBlank + yAxisTitleHeight , this.mTextPaint );
		canvas.drawText( this.mXAxisTitle , mLeftBlank + yAxisTitleWidth + chartWidth,  mTopBlank + yAxisTitleHeight + chartHeight + xAxisTitleHeight ,  this.mTextPaint );
		
		//Y축 문자들 그리기
		int yTextListNumber = chartMaxValue / 10;
		for( int i = 0 ; i < yTextListNumber ; i++ )
		{
			int yTextValue = ( i + 1 ) * 10;
			int yTextHeight = this.getTextHeight( String.valueOf(yTextValue) , this.mTextPaint );
			float y = this.mTopBlank + this.mTopTextBlank + yAxisTitleHeight + ( yTextHeight / 2 ) + ( chartHeight - ( chartHeight * yTextValue / chartMaxValue) );
			canvas.drawText(   String.valueOf(yTextValue) , mLeftBlank ,  y , this.mTextPaint );
		}

		//계열 그리기
		for( int i = 0 ; i < this.mValues.length ; i++ )
		{
			int xTextHeight = this.getTextHeight( this.mXAxisTextList[i] , this.mTextPaint );
			int xTextWidth = this.getTextWidth( this.mXAxisTextList[i] , this.mTextPaint);
			canvas.drawText(this.mXAxisTextList[i], this.mLeftBlank + this.mLeftTextBlank + yAxisTitleWidth + ( ( i + 1 ) * barBlank ) + ( i * barWidth ) + ( barWidth / 2 ) - (xTextWidth / 2 )  , mTopBlank + this.mTopTextBlank + this.mBottomTextBlank + yAxisTitleHeight + chartHeight + xTextHeight , this.mTextPaint );

            int color = this.getResources().getColor(R.color.fitmate_yellow);
            if( mBarHeights[i] > this.mLowRange ){  color = this.getResources().getColor(R.color.fitmate_green); }
            if( mBarHeights[i] > this.mHighRange ){ color = this.getResources().getColor(R.color.fitmate_orange); }

            this.mSeriesPaint.setColor( color );

			float left = this.mLeftBlank + this.mLeftTextBlank + yAxisTitleWidth + ( ( i + 1 ) * barBlank ) + ( i * barWidth ) ;
			float right = this.mLeftBlank + this.mLeftTextBlank + yAxisTitleWidth + ( ( i + 1 ) * barBlank ) + ( ( i + 1 ) * barWidth );
			float top = ( mTopBlank + this.mTopTextBlank + yAxisTitleHeight + chartHeight ) -  ( chartHeight * this.mBarHeights[i] / chartMaxValue);
			float bottom = mTopBlank + this.mTopTextBlank + yAxisTitleHeight + chartHeight;

			canvas.drawRect( left ,  top   ,  right ,  bottom  , this.mSeriesPaint );

            int valueTextHeight = this.getTextHeight(String.valueOf(this.mBarHeights[i]) , this.mSeriesPaint);
			canvas.drawText( String.valueOf(this.mBarHeights[i])  , right - ( barWidth /2) - ( xTextWidth / 2 )  , top + this.mSeriesTextBlank - valueTextHeight, this.mSeriesPaint);
		}
	}
	
	public void setYAxisTitle( String yAxisTitle )
	{
		this.mYAxisTitle = yAxisTitle;
	}
	
	
	public void setXAxisTitle( String xAxisTitle )
	{
		this.mXAxisTitle = xAxisTitle;
	}
	
	public void setXAxisTextList( String[] xAxisTextList )
	{
		this.mXAxisTextList = xAxisTextList;
	}

	public void setValues( int[] values )
	{
		this.mValues = values;
		
		for( int i = 0 ; i < this.mValues.length ; i++ )
		{
			if( this.mMaxValue < mValues[i] )
			{
				this.mMaxValue = mValues[i];
			} 
		}
	}
	
	public void setColors( int[] colors )
	{
		this.mColors = colors;
	}
	
	private int getTextWidth(String text, Paint paint)
	{
		Rect bounds = new Rect();
		paint.getTextBounds(text, 0, text.length(), bounds);
		return bounds.width();
	}
	
	private int getTextHeight(String text, Paint paint)
	{
		Rect bounds = new Rect();
		paint.getTextBounds(text, 0, text.length(), bounds);
		return bounds.height();
	}
	
	public void startAnimation( int duration )
	{
		List<Animator> listValueAnimator = new ArrayList<Animator>();
		this.mHashes = new int[ this.mValues.length ];
		this.mBarHeights = new int[ this.mValues.length ];
		
		for( int i = 0 ; i < this.mValues.length ; i++ )
		{
			ValueAnimator animator = ValueAnimator.ofInt(0, this.mValues[i]);
			this.mHashes[i] = animator.hashCode();
			animator.addUpdateListener( this );
			
			listValueAnimator.add(animator);
		}
		
		AnimatorSet animatorSet = new AnimatorSet();
		animatorSet.playTogether( listValueAnimator );
		animatorSet.setDuration(duration);
		animatorSet.start();
	}

	@Override
	public void onAnimationUpdate(ValueAnimator animation)
	{
		int hash = animation.hashCode();
		int index = this.arraySearch( hash );
		
		this.mBarHeights[ index ] = (Integer) animation.getAnimatedValue();
		
		this.invalidate();	
	}
	
	private int arraySearch( int src )
	{
		int result = -1;
		for( int i = 0 ; i < this.mHashes.length ; i++ )
		{
			if( this.mHashes[i] == src )
			{
				result = i;
				break;
			}
		}
		return result;
	}

    public void setRange( int lowRange , int highRange )
    {
        this.mLowRange = lowRange;
        this.mHighRange = highRange;
    }
}
