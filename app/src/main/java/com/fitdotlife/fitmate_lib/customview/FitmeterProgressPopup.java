package com.fitdotlife.fitmate_lib.customview;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.fitdotlife.fitmate.R;

/**
 * Created by Joshua on 2016-08-24.
 */
public class FitmeterProgressPopup {

    private Context mContext = null;
    private PopupWindow mPopupWindow = null;
    private ImageView imgProgress = null;
    private TextView tvMessage = null;
    private View progressView = null;

    public FitmeterProgressPopup(Context context){

        this.mContext = context;

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        progressView = inflater.inflate(R.layout.dialog_fitmeter_progress, null);
        imgProgress = (ImageView) progressView.findViewById(R.id.img_dialog_fitmeter_progress);
        tvMessage = (TextView) progressView.findViewById( R.id.tv_dialog_fitmeter_progress_message );

        imgProgress.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.dialog_fitmeter_progress));

    }

    public void setMessageText(String messageText){
        tvMessage.setText(messageText);
    }

    public void dismiss(){
        mPopupWindow.dismiss();
    }

    public void show(){
        mPopupWindow = new PopupWindow( progressView , ViewGroup.LayoutParams.MATCH_PARENT , ViewGroup.LayoutParams.MATCH_PARENT);
        mPopupWindow.setFocusable(true);
        mPopupWindow.showAtLocation( progressView, Gravity.CENTER, 0, 0);
    }

    public boolean isShowing(){
        return mPopupWindow.isShowing();
    }


}
