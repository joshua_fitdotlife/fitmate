package com.fitdotlife.fitmate_lib.customview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.widget.RelativeLayout;

import org.apache.log4j.Log;

/**
 * Created by Joshua on 2015-12-17.
 */
public class DayCircleView extends View
{

    private float mValue = 0;
    private Paint mEmptyCirclePaint = null;
    private Paint mValueCirClePaint = null;

    private float mCircleThickness = 0;
    private float mCircleThickness_DP =7.5f;

    private float mCalculateTopMargin = 0;
    private float mCalculateBottomMargin = 0;

    public DayCircleView(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.mEmptyCirclePaint = new Paint( Paint.ANTI_ALIAS_FLAG );
        this.mEmptyCirclePaint.setStyle(Paint.Style.STROKE);

        this.mValueCirClePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.mValueCirClePaint.setStyle(Paint.Style.STROKE);
        this.mValueCirClePaint.setStrokeCap(Paint.Cap.ROUND);

        this.setWillNotDraw(false);
    }

    @Override
    protected void onDraw(Canvas canvas) {

        final float DefaultGraphThickniss= mCircleThickness_DP;
        final float DefaultMargin = 32.33f;
        final float DefaultCircleHeight= 215.33f;

        final float DefaultSize =  DefaultMargin+DefaultCircleHeight;
        final float DefaultRadius = (DefaultCircleHeight)/2;
        float dividerForConvertPixelToDP=getResources().getDisplayMetrics().density;
        float viewHeightDP =  getMeasuredHeight()/dividerForConvertPixelToDP;
        float viewWidthDP = getMeasuredWidth()/dividerForConvertPixelToDP;

        float viewWhatDP= viewHeightDP;
        if(viewHeightDP>viewWidthDP){
            viewWhatDP= viewWidthDP;
            com.fitdotlife.fitmate.log.Log.e("DayHalfCircleView", "viewWhat : viewWidth");
        }else{
            com.fitdotlife.fitmate.log.Log.e("DayHalfCircleView", "viewWhat : viewHeight");
        }

        float ratio = viewWhatDP/DefaultSize;

        //두께도 바꾸기
        // mCircleThickness =ratio*DefaultGraphThickniss*dividerForConvertPixelToDP;
        // 안바꾸려면
        mCircleThickness = DefaultGraphThickniss * dividerForConvertPixelToDP ;

        this.mEmptyCirclePaint.setStrokeWidth(mCircleThickness);
        this.mValueCirClePaint.setStrokeWidth(mCircleThickness);

        float radius = 0;
        float center_x = viewWidthDP * dividerForConvertPixelToDP / 2;
        float center_y = 0;
        float calculateMargin=0;
        calculateMargin = DefaultMargin *ratio * dividerForConvertPixelToDP;

        radius= DefaultRadius * (1+ (ratio-1)/2) * dividerForConvertPixelToDP;

        float radiusDP = radius/dividerForConvertPixelToDP;
        center_y = radius + calculateMargin - ( mCircleThickness /2 );

        mCalculateTopMargin = center_y - radius;
        mCalculateBottomMargin = getMeasuredHeight() - ( center_y + radius );

        canvas.drawCircle(center_x, center_y, radius, mEmptyCirclePaint);

        float sweepAngle = ( 360 * mValue ) / 100;
        if( sweepAngle < 360 ) {
            this.drawRingArc(canvas, center_x, center_y, radius, radius, 270, sweepAngle , mValueCirClePaint);
        }else{
            canvas.drawCircle( center_x , center_y , radius , mValueCirClePaint );
        }
        super.onDraw(canvas);
    }

    private void drawRingArc( Canvas canvas, float x , float y ,float xThickness , float yThickness ,  float startDegree ,  float endDegree , Paint paint )
    {
        RectF oval = new RectF( x - xThickness ,y - yThickness , x + xThickness , y + yThickness );

        Path path = new Path();
        path.arcTo(oval, startDegree, endDegree);
        path.arcTo(oval, startDegree, endDegree, true);
        canvas.drawPath(path, paint);
    }

    public void setValue(float value){
        mValue = value;
    }

    public void setCircleThickness( float thickness ){
        mCircleThickness_DP = thickness;
    }

    public void setEmptyCircleColor( int emptyCircleColor ){
        this.mEmptyCirclePaint.setColor( emptyCircleColor );
    }

    public void setValueCircleColor( int valueCircleColor ){
        this.mValueCirClePaint.setColor( valueCircleColor );
    }

    public float getCalculateTopMargin(){
        return mCalculateTopMargin;
    }

    public float getCalculateBottomMargin(){
        return mCalculateBottomMargin;
    }

}
