package com.fitdotlife.fitmate_lib.customview;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;

import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.StrengthInputType;
import com.fitdotlife.fitmate.R;

public class ArcChartView extends View implements AnimatorUpdateListener
{

//    private final String ANI_WEEK_VALUE = "AniWeekValue";
//    private final String ANI_DAY_VALUE = "AniDayValue";
    private Context mContext;

	private Paint emptyPaint;
	private Paint valueTextPaint;
	
	private Paint mWeekPaint;
	private Paint mWeekEmptyPaint;

	private Paint mTodayPaint;
	private Paint mTodayEmptyPaint;

    private Paint mMainTextPaint;
    private Paint mSubTextPaint;
    private Paint mArcTextPaint;

	private int mWeekValue;
	private int mTodayValue;
	private float mAniWeekValue;
	private float mAniTodayValue;
	
	private int mDuration;
	private int mBackgroundColor;
	
	private int[] mArrTodayArcAchievementColor = new int[]{ 0xFFEEEE49 , 0xFF90EE90 , 0xFFF572A0  };
	private int[] mArrTodayArcBackgroundColor = new int[]{ 0xFFFEFAE6 , 0xFFE8FBE8 , 0xFFFCE9F0  };
	
	private int mTodayThickness = 0;
	private int mWeekThickness = 0;

    private int mLeftBlank = 0;
    private int mRightBlank = 0;
    private int mTopBlank = 0;
    private int mBootomBlank = 0;

    private int mMainTextSize = 0;
    private int mSubTextSize = 0;
    private int mArcTextSize = 0;

    private int innerTextGap = 0;

	private int heightSize = 0;
	private int widthSize = 0;

    private int mTodayFrom = 0;
    private int mTodayTo = 0;

    private int mRangeMax = 0;
    private String mWeekUnit = null;
    private String mTodayUnit = null;

    private ValueAnimator mAnimWeekArc = null;
    private ValueAnimator mAnimTodayArc = null;

	private float mPreTodayValue = 0;
	private float mPreWeekValue = 0;

    private StrengthInputType strengthInputType = StrengthInputType.MET;

	private Bitmap devideLine;

	private String todayArcText="";



    /**
	 * ?�성??
	 * @param context
	 */
	public ArcChartView(Context context )
	{
		super(context);
		this.mContext = context;
		
		this.init();
	}
	
	/**
	 * ?�성??
	 * @param context
	 * @param attrs
	 */
	public ArcChartView(Context context , AttributeSet attrs)
	{
		super(context , attrs);
		this.mContext = context;
		
		this.init();
	}
	
	
	private void init()
	{
		this.mRightBlank = this.getResources().getDimensionPixelSize(R.dimen.day_score_arc_chart_right_blank);
        this.mLeftBlank = this.getResources().getDimensionPixelSize(R.dimen.day_score_arc_chart_left_blank);
        this.mTopBlank = this.getResources().getDimensionPixelSize(R.dimen.day_score_arc_chart_top_blank);
        this.mBootomBlank = this.getResources().getDimensionPixelSize(R.dimen.day_score_arc_chart_bottom_blank);

        this.mMainTextSize = this.getResources().getDimensionPixelSize(R.dimen.day_score_arc_chart_maintext_size);
        this.mSubTextSize = this.getResources().getDimensionPixelSize(R.dimen.day_score_arc_chart_subtext_size);
        this.mArcTextSize = this.getResources().getDimensionPixelSize(R.dimen.day_score_arc_chart_arc_text_size);
        this.mTodayThickness = this.getResources().getDimensionPixelSize(R.dimen.day_score_arc_chart_thickness);
        this.mWeekThickness = this.getResources().getDimensionPixelSize(R.dimen.day_score_arc_chart_thickness);

        this.innerTextGap = this.getResources().getDimensionPixelSize(R.dimen.day_score_arc_chart_innertext_gap);

        this.emptyPaint = new Paint();
		this.emptyPaint.setColor( Color.WHITE );

        this.mMainTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.mMainTextPaint.setTextSize( this.mMainTextSize );

        this.mSubTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.mSubTextPaint.setTextSize( this.mSubTextSize);
        this.mSubTextPaint.setColor(0xFF666666);

        this.mArcTextPaint =  new Paint(Paint.ANTI_ALIAS_FLAG);
        this.mArcTextPaint.setTextSize(this.mArcTextSize);
        this.mArcTextPaint.setColor(Color.WHITE);

		this.mWeekPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		this.mWeekPaint.setColor( 0xFFFF5033 );
		this.mWeekPaint.setTextSize(27);
		this.mWeekPaint.setStyle(Paint.Style.STROKE);
		this.mWeekPaint.setStrokeCap(Cap.ROUND);
		this.mWeekPaint.setStrokeWidth(this.mWeekThickness);
		
		this.mWeekEmptyPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		this.mWeekEmptyPaint.setColor( 0xFF5E5E5E );
		this.mWeekEmptyPaint.setStyle(Paint.Style.STROKE);
		this.mWeekEmptyPaint.setStrokeCap(Cap.ROUND);
		this.mWeekEmptyPaint.setStrokeWidth(this.mWeekThickness);

		this.mTodayPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		this.mTodayPaint.setColor( 0xFF7DB933 );
		this.mTodayPaint.setTextSize(27);
		this.mTodayPaint.setStyle(Paint.Style.STROKE);
		this.mTodayPaint.setStrokeCap(Cap.ROUND);
		this.mTodayPaint.setStrokeWidth(this.mTodayThickness);

		this.mTodayEmptyPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		this.mTodayEmptyPaint.setColor( 0xFFAFAFAF );
		this.mTodayEmptyPaint.setStyle(Paint.Style.STROKE);
		this.mTodayEmptyPaint.setStrokeCap(Cap.ROUND);
		this.mTodayEmptyPaint.setStrokeWidth(this.mTodayThickness);

		this.valueTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		this.valueTextPaint.setColor( Color.BLACK );
		this.valueTextPaint.setTextSize(27);

		devideLine = BitmapFactory.decodeResource(this.mContext.getResources(), R.drawable.home_graph_line2);
		//Bitmap resizeBitmap = Bitmap.createScaledBitmap(src, this.imageHeightSize, this.imageHeightSize, false);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec , int heightMeasureSpec)
	{
		int heightMode = MeasureSpec.getMode(heightMeasureSpec);
		switch( heightMode )
		{
		case MeasureSpec.UNSPECIFIED:
			heightSize = heightMeasureSpec;
			break;
		case MeasureSpec.AT_MOST:
			heightSize = 200;
			break;
		case MeasureSpec.EXACTLY:
			heightSize = MeasureSpec.getSize(heightMeasureSpec);
			break;
		}
		
		int widthMode = MeasureSpec.getMode(widthMeasureSpec);
		switch( widthMode )
		{
		case MeasureSpec.UNSPECIFIED:
			widthSize = widthMeasureSpec;
			break;
		case MeasureSpec.AT_MOST:
			widthSize = 300;
			break;
		case MeasureSpec.EXACTLY:
			widthSize = MeasureSpec.getSize(widthMeasureSpec);
			break;
		}
		
		this.setMeasuredDimension(widthSize, heightSize);
	}
	
	protected void onDraw(Canvas canvas)
	{
		canvas.drawColor( this.mBackgroundColor );
		
		this.drawWeekArcForAni(canvas);
		this.drawTodayArcForAni(canvas);
		//this.drawCenterCircle(canvas);
		this.drawValueTextForAni(canvas);

	}

	/**
	 * ����� �ִ� ���� �׸���.
	 * @param canvas
	 */
	private void drawValueTextForAni( Canvas canvas )
	{
        //주간 달성 점수
		String weekValueText = Math.round( mAniWeekValue) + "";
        String weekHeaderText = this.getResources().getString(R.string.activity_week_score);
        String weekTailText = this.mWeekUnit;

        // 500칼로리
		String todayValueText = Math.round( mAniTodayValue ) + "";
        String todayHeaderText = "";
        String todayTailText =  this.mTodayUnit;

        String todaySubText = this.mTodayFrom + " ~ " +  this.mTodayTo + " " + this.mTodayUnit;
        switch (strengthInputType){
            case CALORIE:

				if(mIsToday)
					todaySubText = this.mContext.getString(R.string.activity_day_scorechart_calorie_consume_today);
				else
					todaySubText = this.mContext.getString(R.string.activity_day_scorechart_caorie_consume_during_today);
				break;
            case CALORIE_SUM:
                todaySubText = this.mContext.getString(R.string.activity_day_scorechart_calorie_consume_today);
                break;
			case VS_BMR:
				if(mIsToday)
					todaySubText = this.mContext.getString(R.string.activity_day_scorechart_calorie_consume_today);
				else
					todaySubText = this.mContext.getString(R.string.activity_day_scorechart_caorie_consume_during_today);
				break;
            default:
                todaySubText = this.mContext.getString(R.string.activity_day_scorechart_optimum_exercise_time);
                break;

        }

        int blankWidth = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 10, getResources().getDisplayMetrics());
        //int innerTextBlank =  (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5 , getResources().getDisplayMetrics());

        int weekHeaderTextWidth  = this.getTextWidth(weekHeaderText , this.mSubTextPaint);
        int weekTailTextWidth = this.getTextWidth(weekTailText , this.mSubTextPaint);
		int weekValueTextWidth = this.getTextWidth(weekValueText, this.mMainTextPaint );

        int todayHeaderTextWidth  = this.getTextWidth(todayHeaderText , this.mMainTextPaint);
        int todayTailTextWidth = this.getTextWidth(todayTailText , this.mMainTextPaint);
		int todayValueTextWidth = this.getTextWidth( todayValueText, this.mMainTextPaint );

        int weekTextWidth = weekHeaderTextWidth + blankWidth +weekValueTextWidth + blankWidth +  weekTailTextWidth;
        int weekTextHeight = this.getTextHeight( weekHeaderText + weekValueText + weekTailText , this.mMainTextPaint);
        int todayTextWidth = todayHeaderTextWidth + blankWidth + todayValueTextWidth + blankWidth + todayTailTextWidth;
        int todayTextHeight = this.getTextHeight(todayHeaderText + todayValueText + todayTailText , this.mMainTextPaint);

        //오늘 소비한 칼로리
        int todaySubTextWidth = this.getTextWidth(todaySubText , this.mSubTextPaint);
        int todaySubTextHeight = this.getTextHeight(todaySubText , this.mSubTextPaint);

        float center_x = this.getMeasuredWidth() / 2;

        //주간 달성 점수 15점
	    float y = this.getMeasuredHeight() - this.mBootomBlank - this.innerTextGap;

        float x = center_x - ( weekTextWidth / 2 );
        this.mMainTextPaint.setColor(0xFF666666);
        canvas.drawText(weekHeaderText, x, y, this.mSubTextPaint);

        x += ( weekHeaderTextWidth + blankWidth );
        this.mMainTextPaint.setColor(this.getResources().getColor(R.color.fitmate_orange));
        canvas.drawText(weekValueText, x, y, this.mMainTextPaint);

        x += ( weekValueTextWidth + blankWidth );
        this.mMainTextPaint.setColor(0xFF666666);
        canvas.drawText(weekTailText, x, y, this.mSubTextPaint);


		y -= ( weekTextHeight + innerTextGap);
		canvas.drawBitmap(devideLine, center_x - devideLine.getWidth()/2, y, null);

		y-=(innerTextGap+devideLine.getHeight()); ;
        //500 칼로리
     //   y -= ( weekTextHeight + innerTextGap);
        x = center_x - ( todayTextWidth / 2 );
        this.mMainTextPaint.setColor( 0xFF666666 );
        canvas.drawText(todayHeaderText , x , y , this.mMainTextPaint );
        x += ( todayHeaderTextWidth + blankWidth );
        this.mMainTextPaint.setColor( this.getResources().getColor(R.color.fitmate_green) );
        canvas.drawText(todayValueText , x , y , this.mMainTextPaint );
        x += ( todayValueTextWidth + blankWidth );
        this.mMainTextPaint.setColor( 0xFF666666 );
        canvas.drawText(todayTailText , x , y , this.mMainTextPaint);


        y -=  ( innerTextGap + todayTextHeight );
        x = center_x - ( todaySubTextWidth / 2 );
        canvas.drawText(todaySubText , x ,y ,this.mSubTextPaint);




	}
	
	/**
	 * 
	 * @param canvas
	 */
	private void drawWeekArcForAni(Canvas canvas)
	{
        int xThickness = ( this.getMeasuredWidth() - this.mRightBlank - this.mLeftBlank ) / 2;
        int yThickness = this.getMeasuredHeight() - this.mTopBlank - this.mBootomBlank - ( this.mWeekThickness / 2 );

		float weekAngle =  ((( 180 * mAniWeekValue ) / 100 ) + 180);
    	
		Path arcPath = drawRingArc( canvas , xThickness , yThickness , 180 , 180 , this.mWeekEmptyPaint );
		drawRingArc( canvas , xThickness , yThickness , 180 , weekAngle - 180 , this.mWeekPaint );

        //타원의 원주의 근사치를 구한다.
        int arcLength = (int)( Math.PI*( ( 5*( xThickness + yThickness )) / 4  - ( xThickness*yThickness ) / (xThickness + yThickness) ));
        String weekArcText = this.getResources().getString(R.string.activity_week_score);
        int weekArcTextWidth = this.getTextWidth(weekArcText , this.mArcTextPaint);
        int weekArcTextHeight = this.getTextHeight(weekArcText , this.mArcTextPaint);
        canvas.drawTextOnPath(weekArcText , arcPath , ( arcLength / 4 ) - ( weekArcTextWidth / 2 ) , ( this.mWeekThickness / 2 ) - ( weekArcTextHeight / 2 ) , this.mArcTextPaint);
	}
	
	/**
	 * 
	 * @param canvas
	 */
	private void drawTodayArcForAni(Canvas canvas)
	{
        int xThickness = ( this.getMeasuredWidth() - this.mRightBlank - this.mLeftBlank - ( this.mWeekThickness * 2 ) ) / 2;
        int yThickness = this.getMeasuredHeight() - this.mTopBlank - this.mBootomBlank - this.mWeekThickness - ( this.mTodayThickness / 2 );

		float todayAngle = (( 180 * mAniTodayValue ) / this.mRangeMax ) + 180 ;

		if( todayAngle > 360 ){
			todayAngle = 360;
		}
    	
		Path arcPath = drawRingArc( canvas , xThickness , yThickness , 180 , 180 , this.mTodayEmptyPaint);
		drawRingArc( canvas , xThickness , yThickness ,  180 , todayAngle - 180 , this.mTodayPaint );

        //타원의 원주의 근사치를 구한다.
        int arcLength = (int)( Math.PI*( ( 5*( xThickness + yThickness )) / 4  - ( xThickness*yThickness ) / (xThickness + yThickness) ));




        int todayArcTextWidth = this.getTextWidth(todayArcText , this.mArcTextPaint);
        int todayArcTextHeight = this.getTextHeight(todayArcText , this.mArcTextPaint);
        canvas.drawTextOnPath(todayArcText , arcPath , ( arcLength / 4 ) - ( todayArcTextWidth / 2 ) , ( this.mTodayThickness / 2 ) - ( todayArcTextHeight / 2 ) , this.mArcTextPaint);

	}
	
//	/**
//	 * ��ũ�� �ΰ��� �� �� �׷����� �ִϸ��̼����� �׸���.
//	 * @param canvas
//	 * @param radius
//	 * @param mAniValue
//	 * @param totalValue
//	 * @param partValue
//	 * @param totalPaint
//	 * @param partPaint
//	 */
//	private void drawArcForAni(Canvas canvas , int radius , int mAniValue , int totalValue , int partValue , Paint totalPaint , Paint partPaint )
//	{
//
//	    if( mAniValue  > ( totalValue - partValue) )
//	    {
//	    	int preTodayAngle = ((360 * ( totalValue - partValue )) / 100 ) + 90;
//	    	int todayAngle = ((360 * mAniValue)  / 100 ) + 90;
//
//	    	drawRingArc( canvas , radius , 90 , preTodayAngle , totalPaint );
//	    	drawRingArc( canvas , radius , preTodayAngle , todayAngle - preTodayAngle , partPaint );
//	    	drawRingArc(  canvas , radius , todayAngle , 450 - todayAngle  , this.emptyPaint );
//	    }
//	    else
//	    {
//	    	int todayAngle = ((360 * mAniValue ) / 100 ) + 90 ;
//
//	    	drawRingArc( canvas , radius , 90 , todayAngle , totalPaint );
//	    	drawRingArc(  canvas , radius , todayAngle , 450 - todayAngle  , this.emptyPaint );
//	    }
//	}
	
	private Path drawRingArc( Canvas canvas, int xThickness , int yThickness ,  float startDegree ,  float endDegree , Paint paint )
	{
		float x , y;
	    
		x = this.getMeasuredWidth() / 2;
		y = this.getMeasuredHeight() - this.mBootomBlank;
		
	    RectF oval = new RectF( x - xThickness ,y - yThickness , x + xThickness , y + yThickness );
	    
	    Path path = new Path();
	    path.moveTo( x - xThickness , y );
	    path.arcTo(oval, startDegree, endDegree);
	    
	    canvas.drawPath(path, paint);
        return path;
	}

    public void setTodayRange( int todayFrom , int todayTo ){
        this.mTodayFrom = todayFrom;
        this.mTodayTo = todayTo;
    }

	boolean mIsToday = false;

	public void setIsToday(boolean val)
	{
		mIsToday = val;
	}

	public void setWeekValue( int weekValue )
	{
		this.mWeekValue = weekValue;
	}
	
	public void setTodayValue( int todayValue )
	{
		this.mTodayValue = todayValue;
	}
	
//	public void setBackGroundColor( int color )
//	{
//		this.mBackgroundColor = color;
//	}
//
//	public void setTodayArcBackgroundColor( int index , int color )
//	{
//		this.mArrTodayArcBackgroundColor[ index ] = color;
//	}
//
//	public void setTodayArcAchievementColor( int index , int color )
//	{
//		this.mArrTodayArcAchievementColor[ index ] = color;
//	}
//
//
//	public void setWeekArcAchievemnetColor( int color )
//	{
//		this.mWeekPaint.setColor(color);
//	}
//
//	public void setWeekArcBackgroundColor( int color )
//	{
//		this.mWeekEmptyPaint.setColor( color );
//	}
//
//	public void setTodayThickness( int thickness )
//	{
//		this.mTodayThickness = thickness;
//	}
//
//	public void setWeekThickness( int thickness )
//	{
//		this.mWeekThickness = thickness;
//	}
	
	public void startAnimation( int duration )
	{
		this.mDuration = duration;

		mAnimWeekArc = ValueAnimator.ofFloat( this.mPreWeekValue , this.mWeekValue );
		mAnimWeekArc.addUpdateListener(this);
		mAnimWeekArc.setDuration( duration );
		mAnimWeekArc.start();

		mAnimTodayArc = ValueAnimator.ofFloat( this.mPreTodayValue , this.mTodayValue);
		mAnimTodayArc.addUpdateListener(this);
		mAnimTodayArc.setDuration( duration );
		mAnimTodayArc.start();

		this.mPreTodayValue =  this.mTodayValue;
		this.mPreWeekValue = this.mWeekValue;
		
	}

    public void setRangMax( int rangeMax ){ this.mRangeMax = rangeMax; }

    public void setTodayUnit( String todayUnit, StrengthInputType type ){
        this.mTodayUnit = todayUnit;
        this.strengthInputType = type;



		if(this.strengthInputType == StrengthInputType.CALORIE || this.strengthInputType == StrengthInputType.CALORIE_SUM || this.strengthInputType == StrengthInputType.VS_BMR){
			todayArcText = this.getResources().getString(R.string.activity_day_scorechart_caorie_consume_during_today);
		}
		else{
			todayArcText = this.getResources().getString(R.string.activity_day_scorechart_exercise_time_during_today);
		}

    }

    public void setWeekUnit( String weekUnit  ) { this.mWeekUnit = weekUnit; }
	// ############
	
	private int getTextWidth(String text, Paint paint)
	{
		Rect bounds = new Rect();
		paint.getTextBounds(text, 0, text.length(), bounds);
		return bounds.width();
	}
	
	private int getTextHeight(String text, Paint paint)
	{
		Rect bounds = new Rect();
		paint.getTextBounds(text, 0, text.length(), bounds);
		return bounds.height();
	}

	@Override
	public void onAnimationUpdate(ValueAnimator animator )
	{
        if( animator.equals( this.mAnimTodayArc ) ){
            this.mAniTodayValue = (float) animator.getAnimatedValue();
        }
        else if( animator.equals( this.mAnimWeekArc ) ){
            this.mAniWeekValue = (float) animator.getAnimatedValue();
        }
        this.invalidate();

	}
}
