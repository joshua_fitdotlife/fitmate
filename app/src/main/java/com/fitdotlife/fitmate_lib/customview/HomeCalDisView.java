package com.fitdotlife.fitmate_lib.customview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.LinearLayout;

import com.fitdotlife.fitmate.R;

/**
 * Created by Joshua on 2015-03-25.
 */
public class HomeCalDisView extends LinearLayout{

    private Drawable mBackgroundDrawable = null;
    private float backgroundDrawableWidth = 0;

    public HomeCalDisView(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.obtainStyledAttributes(attrs , R.styleable.HomeImageTextView );
        Drawable aBackgroundDrawable = a.getDrawable(R.styleable.HomeImageTextView_src);
        if( aBackgroundDrawable != null ) { mBackgroundDrawable = aBackgroundDrawable;}

        backgroundDrawableWidth = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 44.33f, this.getResources().getDisplayMetrics());
    }

    @Override
    protected void onDraw(Canvas canvas) {

        Rect drawableArea = new Rect();

        //이미지를 그린다.
        if( mBackgroundDrawable != null ) {
            drawableArea.left = (int) (this.getMeasuredWidth() - backgroundDrawableWidth - TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, this.getResources().getDisplayMetrics()));
            drawableArea.right = (int) backgroundDrawableWidth + drawableArea.left;
            drawableArea.top = 0;
            drawableArea.bottom = this.getMeasuredHeight();

            mBackgroundDrawable.setBounds(drawableArea);
            mBackgroundDrawable.draw(canvas);
        }
        super.onDraw(canvas);
    }

    public void setImage(Drawable backgroundDrawable)
    {
        this.mBackgroundDrawable =  backgroundDrawable;
        this.invalidate();
    }

}
