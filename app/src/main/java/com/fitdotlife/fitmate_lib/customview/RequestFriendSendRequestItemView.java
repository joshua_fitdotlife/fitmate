package com.fitdotlife.fitmate_lib.customview;

import android.content.Context;
import android.content.Intent;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fitdotlife.fitmate.FriendActivity;

import com.fitdotlife.fitmate.FriendProfileAddActivity_;
import com.fitdotlife.fitmate.R;
import com.fitdotlife.fitmate.RefreshListener;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.http.FriendService;
import com.fitdotlife.fitmate_lib.http.NetworkClass;
import com.fitdotlife.fitmate_lib.key.FriendStatusType;
import com.fitdotlife.fitmate_lib.object.UserFriend;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.rest.spring.annotations.RestService;

import java.util.List;

/**
 * Created by Joshua on 2015-08-03.
 */
@EViewGroup(R.layout.child_activity_friend_requestfriend_listitem_send_request)
public class RequestFriendSendRequestItemView extends LinearLayout {

    private Context mContext;
    private UserFriend mUserFriend;
    private List<UserFriend> mSendList;
    private int mId = 0;
    private RefreshListener mRefreshListener = null;
    private UserInfo mUserInfo = null;

    @RestService
    FriendService friendServiceClient;

    @ViewById(R.id.tv_child_activity_friend_requestfriend_listitem_send_request_content)
    TextView tvContent;

    @ViewById(R.id.child_activity_friend_requestfriend_send_request_profile)
    ImageView imgProfile;

    public RequestFriendSendRequestItemView(Context context) {
        super(context);
        mContext = context;
    }

    public void setUserFriend( UserFriend userFriend , List<UserFriend> sendList , int id , String email ){
        friendServiceClient.setRootUrl(NetworkClass.baseURL + "/api");

        this.mUserFriend = userFriend;
        this.mSendList = sendList;
        this.mId = id;
        this.mUserInfo = new FitmateDBManager(this.mContext).getUserInfo();

        String content = String.format( mContext.getString(R.string.friend_requestfriend_requesting_friend) , userFriend.getName() );
        tvContent.setText( content );

        if( !(mUserFriend.getProfileImagePath() == null ) ) {
            DisplayImageOptions options = new DisplayImageOptions.Builder()
                    .showImageForEmptyUri(R.drawable.profile_img1)
                    .showImageOnFail(R.drawable.profile_img1)
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .displayer(new RoundedBitmapDisplayer(200))
                    .build();

            ImageLoader.getInstance().displayImage(NetworkClass.imageBaseURL + mUserFriend.getProfileImagePath(), imgProfile, options);
        }
    }

    public void setRefreshListener( RefreshListener listener ){
        this.mRefreshListener = listener;
    }

    @UiThread
    void refreshList(  FriendStatusType statusType ){
        UserFriend userFriend = this.mSendList.get(this.mId);
        userFriend.setRequestStatus( statusType.getValue() );

        this.mRefreshListener.refreshList(FriendActivity.FriendModeType.REQUESTFRIEND );
    }

    @Background
    void changeStatus(  FriendStatusType statusType  ){
        boolean result = friendServiceClient.changeStatus( mUserInfo.getEmail(), mUserFriend.getUserFriendsID(), statusType.getValue());

        if( result ){
            this.refreshList( statusType );
        }
    }

    @Click(R.id.btn_child_activity_friend_requestfriend_listitem_cancle)
    void cancleClick(){
        this.changeStatus(FriendStatusType.CANCLE);
    }

    @Click(R.id.ll_child_activity_friend_requestfriend_listitem_send_request)
    void intentClick(){
        FriendProfileAddActivity_.intent(mContext).extra("UserFriend" ,  mUserFriend ).startForResult( FriendActivity.FRIEND_REQUEST_ADD );
    }
}