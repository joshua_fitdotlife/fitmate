package com.fitdotlife.fitmate_lib.service.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class FitmateServiceDBOpenHelper extends SQLiteOpenHelper {

    private static FitmateServiceDBOpenHelper mInstance = null;

    private static final String DB_NAME 			              = "FitmateService.db";
    public static final String DB_TABLE_RAWDATA                 = "TRawData";

    private static final int DB_VERSION 				= 1;

    public static synchronized FitmateServiceDBOpenHelper getInstance( Context context ){
        if( mInstance == null ){
            mInstance = new FitmateServiceDBOpenHelper(context.getApplicationContext());
        }

        return mInstance;
    }

    private FitmateServiceDBOpenHelper( Context context )
    {
        super(context, DB_NAME , null , DB_VERSION );
    }

    @Override
    public void onCreate(SQLiteDatabase db )
    {
        db.execSQL( "CREATE TABLE " + DB_TABLE_RAWDATA + "(" +
                "fileindex INTEGER, " +
                "fileoffset INTEGER, " +
                "filedata BLOB );");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {

    }
}
