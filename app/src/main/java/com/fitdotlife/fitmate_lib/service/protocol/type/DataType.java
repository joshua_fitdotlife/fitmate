package com.fitdotlife.fitmate_lib.service.protocol.type;

/**
 * @author Joshua
 *
 */
public class DataType {
	public static final int ACCELEROSENSOR_RAW 			= 0x00;
	public static final int ACCELEROSENSOR_FILTERED  		= 0x01;
	public static final int ACTIVITY								= 0x02;
	public static final int CALORIE								= 0x03;
	public static final int MET									= 0x04;
	public static final int ILLUMINANCE 						= 0x05;
	public static final int ALTITUDE 								= 0x06;
	
	public static final int RESERVED_1 							= 0x07;
	public static final int RESERVED_2 							= 0x08;
	public static final int RESERVED_3 							= 0x09;
	public static final int RESERVED_4 							= 0x0A;
	public static final int RESERVED_5 							= 0x0B;
	public static final int RESERVED_6 							= 0x0C;
	public static final int RESERVED_7 							= 0x0D;
	public static final int RESERVED_8 							= 0x0E;
	public static final int RESERVED_9 							= 0x0F;
}
