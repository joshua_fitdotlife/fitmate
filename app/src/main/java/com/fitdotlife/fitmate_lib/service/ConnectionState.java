package com.fitdotlife.fitmate_lib.service;

/**
 * Created by Joshua on 2016-05-13.
 */
public enum ConnectionState {
    NONE , SCAN_SUCCESS , SCAN_FAIL , CONNECTION_SUCESS , CONNECTION_FAIL , CLOSED
}
