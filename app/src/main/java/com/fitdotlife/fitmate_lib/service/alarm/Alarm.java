package com.fitdotlife.fitmate_lib.service.alarm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.fitdotlife.fitmate_lib.object.UserInfo;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by wooseok on 2015-06-03.
 */
public class Alarm {


    // 설정 일시
    private GregorianCalendar mCalendar;
    // 알람 메니저
    private AlarmManager mManager;

    Context mContext = null;

    public Alarm(Context ctx)
    {
        mContext = ctx;

        //알람 매니저를 취득
        mManager = (AlarmManager)mContext.getSystemService(Context.ALARM_SERVICE);
    }


    public void setAlarmFromString(String s)
    {
        Calendar c = UserInfo.getCalendarFromString(s);
        GregorianCalendar gc = new GregorianCalendar();
        gc.set(Calendar.HOUR_OF_DAY, c.get(Calendar.HOUR_OF_DAY));
        gc.set(Calendar.MINUTE, c.get(Calendar.MINUTE));

        //setAlarm(gc);
    }


    //알람의 설정
    public void setAlarm(GregorianCalendar mCalendar) {

        this.mCalendar = mCalendar;

        mManager.cancel(pendingIntent()); // cancel any existing alarms
        GregorianCalendar CurCalendar = new GregorianCalendar();


        if(false) {
            mCalendar = new GregorianCalendar();
            mCalendar.add(Calendar.SECOND, 10);

            mManager.setInexactRepeating(AlarmManager.RTC_WAKEUP,
                    mCalendar.getTimeInMillis(),
                    AlarmManager.INTERVAL_FIFTEEN_MINUTES / 15, pendingIntent());
            Log.i("HelloAlarmActivity", "SET ALARM TOMORROW");
        }

        if(true) {
            long IntervalAlarm = AlarmManager.INTERVAL_DAY;
            //long IntervalAlarm = AlarmManager.INTERVAL_FIFTEEN_MINUTES / 3;

            if (
                    mCalendar.get(Calendar.HOUR_OF_DAY) >= CurCalendar.get(Calendar.HOUR_OF_DAY) &&
                            mCalendar.get(Calendar.MINUTE) > CurCalendar.get(Calendar.MINUTE)
                    ) {
                // 아직 지나지 않은 시간
                mManager.setRepeating(AlarmManager.RTC_WAKEUP,
                        mCalendar.getTimeInMillis(),
                        IntervalAlarm, pendingIntent());
                Log.i("HelloAlarmActivity", "SET ALARM TODAY");
            } else {
                // 이미 지난 시간
                mManager.setRepeating(AlarmManager.RTC_WAKEUP,
                        mCalendar.getTimeInMillis() + AlarmManager.INTERVAL_DAY,
                        IntervalAlarm, pendingIntent());
                Log.i("HelloAlarmActivity", "SET ALARM TOMORROW");
            }
        }
        Log.i("HelloAlarmActivity", "SET ALARM" + mCalendar.getTime().toString());

        //finish();
    }

    //알람의 해제
    public void resetAlarm() {
        mManager.cancel(pendingIntent());
        Log.i("HelloAlarmActivity", "CLEAR ALARM" + mCalendar.getTime().toString());
    }


    //알람의 설정 시각에 발생하는 인텐트 작성
    private PendingIntent pendingIntent() {
        if(mCalendar == null)
        {
            mCalendar = new GregorianCalendar();
        }

        Intent i = new Intent(mContext.getApplicationContext(), NotiActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pi = PendingIntent.getActivity(mContext, 0, i, 0);

        Log.i("HelloAlarmActivity", "Pending Indent" + mCalendar.getTime().toString());
        return pi;
    }

}
