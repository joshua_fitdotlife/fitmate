package com.fitdotlife.fitmate_lib.service.key;

public enum WearingPositionType 
{
	 WAIST(0),
	 WRIST(1),
	 ANKLE(2),
	 HUMERUS(3),
	 POCKET(4);
	 
	 private int mValue;
	 
	 WearingPositionType( int value )
	 {
		 this.mValue = value;
	 }
	 
	 public int getValue()
	{
		return this.mValue;
	}
	
	public static WearingPositionType getWearingPositionType( int value )
	{
		WearingPositionType wearingPositionType = null;
		
		switch( value )
		{
		case 0:
			wearingPositionType = WearingPositionType.WAIST;
			break;
		case 1:
			wearingPositionType = WearingPositionType.WRIST;
			break;
		case 2:
			wearingPositionType = WearingPositionType.ANKLE;
			break;
		case 3:
			wearingPositionType = WearingPositionType.HUMERUS;
			break;
		case 4:
			wearingPositionType = WearingPositionType.POCKET;
			break;
		}
		
		return wearingPositionType;
	}
	 
	 
}
