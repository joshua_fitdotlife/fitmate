package com.fitdotlife.fitmate_lib.service.protocol.object;

public class ExtendHeader {
	
	private final byte CORRECTION_VALUE_ID = (byte)0xF3;
	private final byte USERINFO_ID = (byte)0xF2;
	
	private byte[] arrCorrectionValues; 
	private byte[] arrUserInfo;
	
	/**
	 * 생성자
	 * @param arrExtendHeader
	 */
	public ExtendHeader(byte[] arrExtendHeader )
	{
		int index = 0;
		
		for( ; index < arrExtendHeader.length; )
		{
			/**
			 * 보정 값 설정정보는 식별자(0xF3), 길이, 값 목록(key-value 2byte)으로 구성되어 있다.
			 * 사용자 정보는 식별자(0xF2) , 길이, 사용자 정보로 구성되어 있다.
			 * */
			
			//보정값인지 사용자정보인지 판단하기 위하여 첫바이트를 읽는다.
			int dataType = arrExtendHeader[ index ];
			index++;
			
			switch( dataType )
			{
			case CORRECTION_VALUE_ID:
				int correctionValueLength = arrExtendHeader[ index ] & 0xFF;
				
				this.arrCorrectionValues  = new byte[ correctionValueLength ];
				System.arraycopy(arrExtendHeader, index, arrCorrectionValues, 0, correctionValueLength);
				index += correctionValueLength;
				
				break;
			case USERINFO_ID:
				int userInfoLength = arrExtendHeader[ index ] & 0xFF;
				
				this.arrUserInfo = new byte[ userInfoLength ];
				System.arraycopy(arrExtendHeader, index, arrUserInfo, 0, userInfoLength);
				index += userInfoLength;
				
				break;
			}
		}
	}
	
	/**
	 * 보정값 설정 정보를 반환한다.
	 * @return
	 */
	public byte[] CorrectionValues()
	{
		return this.arrCorrectionValues;
	}
	
	/**
	 * 사용자 정보를 반환한다.
	 * @return
	 */
	public UserInfo getUserInfo()
	{
		return new UserInfo(arrUserInfo);
	}
}
