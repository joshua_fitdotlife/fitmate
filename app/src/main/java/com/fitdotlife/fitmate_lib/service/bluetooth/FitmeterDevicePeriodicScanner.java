package com.fitdotlife.fitmate_lib.service.bluetooth;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import org.apache.log4j.Log;

/**
 * Created by Joshua on 2015-02-27.
 */
public class FitmeterDevicePeriodicScanner implements BluetoothAdapter.LeScanCallback {

    private final String TAG = "fitmateservice - " + FitmeterDevicePeriodicScanner.class.getSimpleName();

    private final String ACTION_ALARM = "com.fitdotlife.fitmateservice_beta.ALARM";
    //private final int SCAN_INTERVAL = 30000;
    private final int SCAN_INTERVAL = 10000;
    private final int SCAN_TIME = 2000;

    private BluetoothAdapter mBluetoothAdapter = null;
    private Context mContext = null;

    private PendingIntent mSender = null;
    private AlarmManager mAlarmManager = null;

    private boolean mScanning = false;
    private boolean mAlarmRegistered = false;

    private Runnable scanTimeout = null;
    private Handler mHandler = new Handler();

    private String mDeviceAddress = "NOT SETTED";
    private FitmemterScanListener mListener = null;

    private boolean mSearched = false;

    private BroadcastReceiver mAlarmReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            startScan();
        }
    };

    public interface FitmemterScanListener {
        public void onFitmeterFound(BluetoothDevice device);
    }

    public FitmeterDevicePeriodicScanner(Context context, BluetoothAdapter bluetoothAdapter, FitmemterScanListener listener){
        this.mContext = context;
        this.mBluetoothAdapter = bluetoothAdapter;
        this.mListener = listener;

        this.mAlarmManager = (AlarmManager)this.mContext.getSystemService( Context.ALARM_SERVICE );
    }

    boolean isNeedRequest = false;

    public void RequestScan()
    {
        isNeedRequest = true;
    }

    public synchronized void startSearchProgress( String deviceAddress, final long delay )
    {
        if( mSearched ){ return; }

        Log.i(TAG , "검색을 시작합니다.");

        try{
            throw new Exception();
        } catch (Exception e) {
            e.printStackTrace();
        }

        this.mDeviceAddress = deviceAddress;

        this.startAlarm( delay );
        this.mSearched = true;

    }

    public void stopSearchProgress()
    {
        Log.i(TAG , "검색을 종료합니다.");
        if( this.mScanning ){
            stopScan();
        }

        if( mAlarmRegistered ){
            this.stopAlarm();
        }

        this.mSearched = false;
    }

    private void startScan(){
        Log.i(TAG, "기기 스캔을 시작합니다. 주소 : " + this.mDeviceAddress );

        try {
            //throw new Exception();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        this.mScanning = true;
        this.addScanningTimeout();
        this.mBluetoothAdapter.startLeScan( this );
    }


    private void startConnect(){
        Log.i(TAG, "기기 연결을 시작합니다. 주소 : " + this.mDeviceAddress );

        try {
            //throw new Exception();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        this.mScanning = true;
        this.addConnectingTimeout();
        BluetoothDevice mDevice = this.mBluetoothAdapter.getRemoteDevice(this.mDeviceAddress);

    }

    private void addConnectingTimeout() {

        this.scanTimeout = new Runnable() {

            @Override
            public void run() {
                if( mBluetoothAdapter == null ) return;
                stopScan();
            }
        };

        this.mHandler.postDelayed(this.scanTimeout, this.SCAN_TIME);
    }

    private void addScanningTimeout() {

        this.scanTimeout = new Runnable() {

            @Override
            public void run() {
                if( mBluetoothAdapter == null ) return;
                stopScan();
            }
        };

        this.mHandler.postDelayed(this.scanTimeout , this.SCAN_TIME);
    }

    private void stopScan(){
        Log.i(TAG , "기기 스캔을 종료합니다.");
        this.mScanning = false;
        this.mBluetoothAdapter.stopLeScan( this);
    }

    @Override
    public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {
        String deviceAddress = device.getAddress();
        if( deviceAddress != null )
        {
            if( deviceAddress.equals( this.mDeviceAddress ) )
            {
                Log.i(TAG, "휘트미터 기기를 찾았습니다. 주소 : " + this.mDeviceAddress);
                this.mHandler.removeCallbacks(scanTimeout);
                this.stopScan();
                this.stopSearchProgress();

                final BluetoothDevice bd = device;
                final FitmemterScanListener fs = mListener;

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            // scan 하고 약간의 시간을 줘서 정리할 시간을??
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        fs.onFitmeterFound( bd );
                    }
                }).start();

            }
        }
    }

    private void startAlarm( long delay )
    {
        Log.d( TAG, "알람을 시작합니다.");
        IntentFilter filter = new IntentFilter();
        filter.addAction( this.ACTION_ALARM );
        this.mContext.registerReceiver(this.mAlarmReceiver , filter);

        Intent intent = new Intent( ACTION_ALARM );
        this.mSender = PendingIntent.getBroadcast(this.mContext, 0, intent, 0);
        this.mAlarmManager.setRepeating(AlarmManager.RTC_WAKEUP , System.currentTimeMillis() + delay , this.SCAN_INTERVAL , this.mSender);
        this.mAlarmRegistered = true;
    }

    private void stopAlarm()
    {
        Log.d( TAG, "알람을 종료합니다.");
        this.mAlarmManager.cancel( this.mSender );
        this.mContext.unregisterReceiver( this.mAlarmReceiver );
        this.mAlarmRegistered = false;
    }

}


