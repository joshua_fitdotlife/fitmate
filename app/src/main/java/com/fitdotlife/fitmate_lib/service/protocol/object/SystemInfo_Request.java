package com.fitdotlife.fitmate_lib.service.protocol.object;

public class SystemInfo_Request {
	
	public static final int LENGTH = 20;
	private byte[] arrSystemInfo;
	
	public SystemInfo_Request()
	{
		arrSystemInfo = new byte[LENGTH];
	}
	
	
	public SystemInfo_Request(byte[] systemInfo)
	{
		this.arrSystemInfo = systemInfo;
	}
	
	public byte[] getBytes()
	{
		return this.arrSystemInfo;
	}
	
	/**
	 * 가속도 범위를 설정한다.
	 * @param range
	 */
	public void setAccRange(int range)
	{
		this.arrSystemInfo[0] = (byte)range;
	}
	
	/**
	 * 가속도 범위를 반환한다.
	 * @return
	 */
	public byte getAccRange()
	{
		return this.arrSystemInfo[0];
	}
	
	/**
	 * LED 알림을 설정한다.
	 * @param notice
	 */
	public void setLEDNotice(int notice)
	{
		this.arrSystemInfo[1] = (byte) notice;
	}
	/**
	 * LED 알림을 반환한다.
	 * @return
	 */
	public byte getLEDNotice()
	{
		return this.arrSystemInfo[1];
	}
	
	/**
	 * 순간기록을 설정한다.
	 * @param saveTime
	 */
	public void setTimeMark(int saveTime)
	{
		this.arrSystemInfo[2] = (byte) saveTime;
	}
	/**
	 * 순간기록을 반환한다.
	 * @return
	 */
	public byte getTImeMark()
	{
		return this.arrSystemInfo[2];
	}
	
	/**
	 * 착용위치를 설정한다.
	 * @param position
	 */
	public void setWearingPosition(int position)
	{
		this.arrSystemInfo[3] = (byte)position;
	}
	/**
	 * 착용위치를 반환한다.
	 * @return
	 */
	public byte getWearingPosition()
	{
		return this.arrSystemInfo[3];
	}
	
	/**
	 * 저장간격의 바이트배열을 반환한다.
	 * @return
	 */
	public byte[] getSavingIntervalBytes()
	{
		byte[] arrSavingInterval = new byte[ 16 ];
		System.arraycopy(arrSystemInfo, 4, arrSavingInterval, 0, arrSavingInterval.length);
		
		return arrSavingInterval;
	}
	
	
	/**
	 * 3축 가속센서(Raw) 저장간격을 설정한다.
	 * @param savingInterval
	 */
	public void setRawSavingInterval( int savingInterval )
	{
		this.arrSystemInfo[4] = (byte) savingInterval;
	}
	/**
	 * 3축 가속센서(Raw) 저장간격을 반환한다.
	 * @return
	 */
	public byte getRawSavingInterval()
	{
		return this.arrSystemInfo[4];
	}
	
	/**
	 * 3축 가속센서(Filtered) 저장간격을 설정한다.
	 * @param savingInterval
	 */
	public void setFilteredSavingInterval( int savingInterval )
	{
		this.arrSystemInfo[5] = (byte)savingInterval;
	}
	/**
	 * 3축 가속센서(Filtered) 저장간격을 반환한다.
	 * @return
	 */
	public byte getFilteredSavingInterval( )
	{
		return this.arrSystemInfo[5];
	}
	
	
	/**
	 * 활동량 저장간격을 설정한다.
	 * @param savingInterval
	 */
	public void setActivitySavingInterval( int savingInterval )
	{
		this.arrSystemInfo[6] = (byte)savingInterval;
	}
	/**
	 * 활동량 저장간격을 반환한다.
	 * @return
	 */
	public byte getActivitySavingInterval( )
	{
		return this.arrSystemInfo[6];
	}
	
	/**
	 * 칼로리 저장간격을 설정한다.
	 * @param savingInterval
	 */
	public void setCaloriesSavingInterval( int savingInterval )
	{
		this.arrSystemInfo[7] = (byte)savingInterval;
	}
	/**
	 * 칼로리 저장간격을 반환한다.
	 * @return
	 */
	public byte getCaloriesSavingInterval( )
	{
		return this.arrSystemInfo[7];
	}
	
	/**
	 * MET 저장간격을 설정한다.
	 * @param savingInterval
	 */
	public void setMETSavingInterval( int savingInterval )
	{
		this.arrSystemInfo[8] = (byte)savingInterval;
	}
	/**
	 * MET 저장간격을 반환한다.
	 * @return
	 */
	public byte getMETSavingInterval( )
	{
		return this.arrSystemInfo[8];
	}
	
	/**
	 * 조도 저장간격을 설정한다.
	 * @param savingInterval
	 */
	public void setIlluminanceSavingInterval( int savingInterval )
	{
		this.arrSystemInfo[9] = (byte)savingInterval;
	}
	/**
	 * 조도 저장간격을 반환한다.
	 * @return
	 */
	public byte getIlluminanceSavingInterval( )
	{
		return this.arrSystemInfo[9];
	}
	
	/**
	 * 고도 저장간격을 설정한다.
	 * @param savingInterval
	 */
	public void setAtmSavingInterval( int savingInterval )
	{
		this.arrSystemInfo[10] = (byte)savingInterval;
	}
	/**
	 * 고도 저장간격을 반환한다.
	 * @return
	 */
	public byte getAtmSavingInterval( )
	{
		return this.arrSystemInfo[10];
	}
	
	/**
	 * Reserved 1 저장간격을 설정한다.
	 * @param savingInterval
	 */
	public void setReserved1SavingInterval( int savingInterval )
	{
		this.arrSystemInfo[11] = (byte)savingInterval;
	}
	/**
	 * Reserved 1 저장간격을 반환한다.
	 * @return
	 */
	public byte getReserved1SavingInterval( )
	{
		return this.arrSystemInfo[11];
	}
	
	/**
	 * Reserved 2 저장간격을 설정한다.
	 * @param savingInterval
	 */
	public void setReserved2SavingInterval( int savingInterval )
	{
		this.arrSystemInfo[12] = (byte)savingInterval;
	}
	/**
	 * Reserved 2 저장간격을 반환한다.
	 * @return
	 */
	public byte getReserved2SavingInterval( )
	{
		return this.arrSystemInfo[12];
	}
	
	/**
	 * Reserved 3 저장간격을 설정한다.
	 * @param savingInterval
	 */
	public void setReserved3SavingInterval( int savingInterval )
	{
		this.arrSystemInfo[13] = (byte)savingInterval;
	}
	/**
	 * Reserved 3 저장간격을 반환한다.
	 * @return
	 */
	public byte getReserved3SavingInterval( )
	{
		return this.arrSystemInfo[13];
	}
	
	/**
	 * Reserved 4 저장간격을 설정한다.
	 * @param savingInterval
	 */
	public void setReserved4SavingInterval( int savingInterval )
	{
		this.arrSystemInfo[14] = (byte)savingInterval;
	}
	/**
	 * Reserved 4 저장간격을 반환한다.
	 * @return
	 */
	public byte getReserved4SavingInterval( )
	{
		return this.arrSystemInfo[14];
	}
	
	/**
	 * Reserved 5 저장간격을 설정한다.
	 * @param savingInterval
	 */
	public void setReserved5SavingInterval( int savingInterval )
	{
		this.arrSystemInfo[15] = (byte)savingInterval;
	}
	/**
	 * Reserved 5 저장간격을 반환한다.
	 * @return
	 */
	public byte getReserved5SavingInterval( )
	{
		return this.arrSystemInfo[15];
	}
	
	/**
	 * Reserved 6 저장간격을 설정한다.
	 * @param savingInterval
	 */
	public void setReserved6SavingInterval( int savingInterval )
	{
		this.arrSystemInfo[16] = (byte)savingInterval;
	}
	/**
	 * Reserved 6 저장간격을 반환한다.
	 * @return
	 */
	public byte getReserved6SavingInterval( )
	{
		return this.arrSystemInfo[16];
	}
	
	/**
	 * Reserved 7 저장간격을 설정한다.
	 * @param savingInterval
	 */
	public void setReserved7SavingInterval( int savingInterval )
	{
		this.arrSystemInfo[17] = (byte)savingInterval;
	}
	/**
	 * Reserved 7 저장간격을 반환한다.
	 * @return
	 */
	public byte getReserved7SavingInterval( )
	{
		return this.arrSystemInfo[17];
	}
	
	/**
	 * Reserved 8 저장간격을 설정한다.
	 * @param savingInterval
	 */
	public void setReserved8SavingInterval( int savingInterval )
	{
		this.arrSystemInfo[18] = (byte)savingInterval;
	}
	/**
	 * Reserved 8 저장간격을 반환한다.
	 * @return
	 */
	public byte getReserved8SavingInterval( )
	{
		return this.arrSystemInfo[18];
	}
	
	/**
	 * Reserved 9 저장간격을 설정한다.
	 * @param savingInterval
	 */
	public void setReserved9SavingInterval( int savingInterval )
	{
		this.arrSystemInfo[19] = (byte)savingInterval;
	}
	/**
	 * Reserved 9 저장간격을 반환한다.
	 * @return
	 */
	public byte getReserved9SavingInterval( )
	{
		return this.arrSystemInfo[19];
	}
	
}
