package com.fitdotlife.fitmate_lib.service.protocol;

import com.fitdotlife.fitmate_lib.service.protocol.type.CodeConstants;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class Decoder 
{
	private static final String TAG = "fitmate";

    public static byte[] decoding( byte[] arrSrc )
	{
		byte[]  resultBytes = null;
		ByteArrayOutputStream result = new ByteArrayOutputStream();
		ByteArrayInputStream source = new ByteArrayInputStream(arrSrc);
        
        byte b = (byte)source.read();
		
		while (b != CodeConstants.PC_COMM_START)
        {
            b = (byte)source.read();
		}

		b = (byte)source.read();


		while (b != CodeConstants.PC_COMM_END)
		{
		    if (b != CodeConstants.PC_COMM_EXCEPTION)
		    {
		        result.write(b);
		    }
		    else
		    {
		        byte c = (byte)source.read();

		        if (c == CodeConstants.PC_COMM_EXP_START)
		        {
		            result.write(CodeConstants.PC_COMM_START);
		        }
		        else if (c == CodeConstants.PC_COMM_EXP_END)
		        {
		            result.write(CodeConstants.PC_COMM_END);
		        }
		        else if (c == CodeConstants.PC_COMM_EXP_EXP)
		        {
		            result.write(CodeConstants.PC_COMM_EXCEPTION);
		        }
		        else
		        {
		            result.reset();
		            return result.toByteArray();
		        }
		    }
		    
		    b = (byte)source.read();
		}

		resultBytes = result.toByteArray();
		
		try {
			result.close();
		} catch (IOException e) { }
		
        return resultBytes;
	}
}
