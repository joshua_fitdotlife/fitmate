package com.fitdotlife.fitmate_lib.service.protocol.object;

/**
 * 제품 종류
 *  제품종류(4byte) = 제품대분류(1) + 제품중분류(1) + 제품하분류(1) + 버전(1)
 * @author Joshua
 *
 */
public class ProductType 
{
	public static final int Length = 4;
	private byte[] arrProductType;
	
	/**
	 * 생성자
	 */
	public ProductType()
	{
		this.arrProductType = new byte[Length];
	}
	
	/**
	 * 생성자
	 * @param productType
	 */
	public ProductType(byte[] productType)
	{
		this.arrProductType = productType;
	}
	
	/**
	 * 바이트 배열을 반환한다.
	 * @return
	 */
	public byte[] getBytes()
	{
		return this.arrProductType;
	}
	
	/**
	 * 카테고리를 반환한다.
	 * @return
	 */
	public byte getCategory()
	{
		return this.arrProductType[0];
	}
	
	/**
	 * 카테고리를 설정한다.
	 * @param category
	 */
	public void setCategory(int category)
	{
		this.arrProductType[0] = (byte) category;
	}
	
	/**
	 * Division을 반환한다.
	 * @return
	 */
	public byte getDivision()
	{
		return this.arrProductType[1];
	}
	
	/**
	 * Division을 설정한다.
	 * @param division
	 */
	public void setDivision(int division)
	{
		this.arrProductType[1] = (byte) division;
	}
	
	/**
	 * 섹션을 반환한다.
	 * @return
	 */
	public byte getSection()
	{
		return this.arrProductType[2];
	}
	
	/**
	 * 섹션을 설정한다.
	 * @param section
	 */
	public void setSection(int section)
	{
		this.arrProductType[2] = (byte) section;
	}
	
	/**
	 * 버전을 반환한다.
	 * @return
	 */
	public byte getVersion()
	{
		return this.arrProductType[3];
	}
	
	/**
	 * 버전을 설정한다.
	 * @param version
	 */
	public void setVersion(int version)
	{
		this.arrProductType[3] = (byte) version;
	}
	
}