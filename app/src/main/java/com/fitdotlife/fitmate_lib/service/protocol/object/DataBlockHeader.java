package com.fitdotlife.fitmate_lib.service.protocol.object;

public class DataBlockHeader {
	
	public static final int Length = 8;
	private byte[] arrDataBlockHeader;
	
	
	public DataBlockHeader(byte[] sensedDataHeader)
	{
		this.arrDataBlockHeader = sensedDataHeader;
	}
	
	public byte[] getBytes()
	{
		return this.arrDataBlockHeader;
	}
	
	/**
	 * Start of Text 1
	 * @return
	 */
	public byte getSTX1()
	{
		return this.arrDataBlockHeader[0];
	}
	
	/**
	 * Start of Text 2
	 * @return
	 */
	public byte getSTX2()
	{
		return this.arrDataBlockHeader[1];
	}
	
	public String getFLDF()
	{
		byte[] arrFLDF = new byte[]{ this.arrDataBlockHeader[2],this.arrDataBlockHeader[3],this.arrDataBlockHeader[4],this.arrDataBlockHeader[5] };
		return String.valueOf(arrFLDF);
	}
	
	public int getMajorVersion()
	{
		return this.arrDataBlockHeader[6];
	}
	
	public int getMinorVersion()
	{
		return this.arrDataBlockHeader[7];
	}
}

