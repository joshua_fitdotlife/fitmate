package com.fitdotlife.fitmate_lib.service.activity;

import org.apache.log4j.Log;
import com.fitdotlife.fitmate_lib.service.bluetooth.BluetoothRawData;
import com.fitdotlife.fitmate_lib.service.protocol.object.ActivityData;
import com.fitdotlife.fitmate_lib.service.protocol.object.AxisData;
import com.fitdotlife.fitmate_lib.service.protocol.object.DataBlockHeader;
import com.fitdotlife.fitmate_lib.service.protocol.object.ExtendHeader;
import com.fitdotlife.fitmate_lib.service.protocol.object.IntervalData;
import com.fitdotlife.fitmate_lib.service.protocol.object.SystemInfo_Response;
import com.fitdotlife.fitmate_lib.service.protocol.object.TimeInfo;
import com.fitdotlife.fitmate_lib.service.protocol.type.AcceleroRangeType;
import com.fitdotlife.fitmate_lib.service.protocol.type.DataType;
import com.fitdotlife.fitmate_lib.service.protocol.type.IntervalType;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ActivityParser
{
	private final byte PERIODIC = 0x00;
	private final int NON_PERIODIC = 0x80;

    private List<ActivityData> activityDataList = new ArrayList<ActivityData>();
    private ActivityData currentActivityData = null;

	private int mIndex = 0;
	private int mCount = 0;
	private int mGravity = 0;
	//private TimeInfo mStartTime = null;
    //private TimeInfo addTime = null;
    //private double mSaveInterval = 0;

	public void parse( byte[] dataBlock , int fileIndex ) throws DataParseException, HeaderParseException {

        this.mIndex = 0;
        this.mCount = 0;
        SystemInfo_Response systemInfo;
        TimeInfo startTime = new TimeInfo();

        try {

            byte startTimeHeader;
            ExtendHeader extendHeader;

            //헤더, 시스템 설정 정보 , 기록시작시각헤더, 기록시작시간을 읽지 않는다.
            mIndex += DataBlockHeader.Length;

            byte[] systemInfoBytes = new byte[SystemInfo_Response.LENGTH];
            System.arraycopy(dataBlock, mIndex, systemInfoBytes, 0, SystemInfo_Response.LENGTH);
            systemInfo = new SystemInfo_Response(systemInfoBytes);
            this.mGravity = this.getGravity(systemInfo.getSystemInfo().getAccRange());

            //기록 시작 시각 헤더
            mIndex += SystemInfo_Response.LENGTH;
            startTimeHeader = dataBlock[mIndex];

            //기록 시작 시간 정보
            mIndex += 1;
            byte[] startTimeBytes = new byte[TimeInfo.LENGTH];
            System.arraycopy(dataBlock, mIndex, startTimeBytes, 0, TimeInfo.LENGTH);


            startTime.setYear(2010 + startTimeBytes[0]);
            startTime.setMonth(startTimeBytes[1]);
            startTime.setDay(startTimeBytes[2]);
            startTime.setDayOfTheWeek(startTimeBytes[3]);
            startTime.setHour(startTimeBytes[4]);
            startTime.setMinute(startTimeBytes[5]);
            startTime.setSecond(startTimeBytes[6]);
            //addTime = mStartTime.copyCurrentValue();

            mIndex += TimeInfo.LENGTH;

            //확장헤더 길이
            byte[] arrExtendHeaderLength = new byte[2];
            System.arraycopy(dataBlock, mIndex, arrExtendHeaderLength, 0, 2);
            int extendHeaderLength = this.converToInt16(arrExtendHeaderLength);

            //확장헤더
            mIndex += 2;
            byte[] arrExtendHeader = new byte[extendHeaderLength];
            System.arraycopy(dataBlock, mIndex, arrExtendHeader, 0, extendHeaderLength);
            extendHeader = new ExtendHeader(arrExtendHeader);

            mIndex += extendHeaderLength;

        }catch( IndexOutOfBoundsException e ){
            throw new HeaderParseException( e.getMessage() );
        }

        this.currentActivityData = new ActivityData();
        activityDataList.add( currentActivityData );
        currentActivityData.setStartTime(startTime);
        this.currentActivityData.setRawFileIndex( fileIndex );
		this.currentActivityData.setSystemInfoResponse(systemInfo);

		//시스템 정보에 설정되어 있는 데이터들의 주기 바이트 배열(16바이트)을 가져온다.
		byte[] arrSavingInterval = systemInfo.getSystemInfo().getSavingIntervalBytes();
		
		double minFrequency = 60;
		List<IntervalData> intervalDataList = new ArrayList<IntervalData>();
		
		//주기바이트 배열의 각각을 확인하여 주기가 설정되어 있는 데이터만 가져오고 , 설정되어 있는 주기중 최소 값을 저장한다.
		for( int type = 0 ; type < arrSavingInterval.length ; type++ )
		{
			int intervalType = arrSavingInterval[ type ];
			double frequency = this.getFrequency( intervalType );
			if( frequency != -1 )
			{
				intervalDataList.add( new IntervalData( (byte)type , frequency ) );
				
				if( frequency < minFrequency )
				{
					minFrequency = frequency;
				}
			}
		}
		
		//저장간격을 저장한다.
        double saveInterval = minFrequency;

        //첫번째 활동량 데이터를 만든다.
        currentActivityData.setSaveInterval( saveInterval );

		// 주기적 데이터를 읽을 때마다 1씩 증가한다. 
		// samplingNumber와 함께 사용하여 각 데이터의 샘플링을 하게 한다.
		for( ; mIndex < dataBlock.length; )
		{
			//첫번째 바이트를 읽어서 비주기 데이터( MSB 비트 1 ) 인지 주기 데이터(MSB 비트 0)인지 패딩( 0xFF )인지 확인한다.
			//패딩 바이트 먼저 확인한다.
			if( dataBlock[ mIndex ] == 0xFF )
			{
				mIndex++;
				continue;
			}
			//주기 데이터, 비주기 데이터 여부를 확인한다.
			if( ( dataBlock[ mIndex ] & 0x80) == this.PERIODIC )
			{
				//*** 우선 순위에 따라 intervalDataList에 저장했기 때문에 우선순위 연산이 따로 필요치 않다.
				Iterator<IntervalData> dataListIter = intervalDataList.iterator();
				
				while( dataListIter.hasNext() )
				{ 
					// count를 IntervalData의 SamplingNumber로 나눠서 나머지가 없다면  ( count % samplingNumber ) == 0  
					//그 해당하는 데이터를 계산하도록 한다.
					IntervalData intervalData = dataListIter.next();
					
					if( ( mCount % ( intervalData.getFrequency() / minFrequency ) == 0 ))
					{
						this.periodicDataParsing( intervalData.getDataType() , dataBlock );
					}
				}
				mCount++;
			}
			else if( (dataBlock[ mIndex] & 0x80 ) == this.NON_PERIODIC ) // 비주기, 현재 비주기 데이터는 순간기록 시점 데이터만 존재함.
			{
				this.parseMarkTimeData( dataBlock );
			}
		}
	}
	
	/**
	 * 주기 데이터를 파싱한다.
	 * @param dataType
	 * @param srcArray
	 * @throws DataParseException
	 */
	private void periodicDataParsing( int dataType, byte[] srcArray ) throws DataParseException
	{
		switch( dataType )
		{
		case DataType.ACCELEROSENSOR_RAW:
			this.parseAxisData(srcArray);
			break;
		case DataType.ACCELEROSENSOR_FILTERED:
			this.parseAxisData(srcArray);
			break;
		case DataType.ACTIVITY:
			int activity = this.parseActCalData( srcArray );
            this.currentActivityData.setSVM( activity );

            //this.addTime.addSecond( (int)this.mSaveInterval );
			break;
		case DataType.CALORIE:
			this.parseActCalData(srcArray);
			break;
		case DataType.MET:
			this.parseMETLuxData(srcArray);
			break;
		case DataType.ILLUMINANCE:
			this.parseMETLuxData(srcArray);
			break;
		case DataType.ALTITUDE:
			this.parseAltitudeData(srcArray);
			break;
		case DataType.RESERVED_1:
			break;
		case DataType.RESERVED_2:
			break;
		case DataType.RESERVED_3:
			break;
		case DataType.RESERVED_4:
			break;
		case DataType.RESERVED_5:
			break;
		case DataType.RESERVED_6:
			break;
		case DataType.RESERVED_7:
			break;
		case DataType.RESERVED_8:
			break;
		case DataType.RESERVED_9:
			break;
		}
	}

	/**
	 * 3축 값이 나오는 데이들을 파싱한다.
	 * RawData , FilterData
	 * @param srcArray
	 * @return
	 */
	private AxisData parseAxisData( byte[] srcArray)
	{
		AxisData axisData = null;
		try {

			byte[] arrAxisData = new byte[AxisData.LENGTH];
			System.arraycopy(srcArray, mIndex, arrAxisData, 0, AxisData.LENGTH);
			mIndex += AxisData.LENGTH;
			axisData = new AxisData(arrAxisData);

		}catch( IndexOutOfBoundsException ibe ){
			Log.e( "fitmate" , "3축 데이터를 파싱하는 중에 에러가 발생하였습니다." );
		}

		return axisData;
	}

	/**
	 * 활동량과 칼로리 데이터들을 파싱한다.
	 * @param srcArray
	 * @return
	 * @throws DataParseException
	 */
	private int parseActCalData( byte[] srcArray ) throws DataParseException
	{
		int act = 0;
		try {
			int k = (srcArray[mIndex] & 0x60);
			k = k >> 5;

			act = srcArray[mIndex] & 0x1F;
			mIndex++;

			for (int u = 0; u < k; u++) {
				try {
					act = (act << 8) + (srcArray[mIndex] & 0xFF);
					mIndex++;
				} catch (ArrayIndexOutOfBoundsException e) {
					throw new DataParseException("데이터를 분석하는 중에 오류가 발생했습니다." + e.getMessage());
				}
			}

			if (act < 0) {
				act = 0;
			}

			act = this.devideBy32ForInt(this.getTransValueForInt(act, this.mGravity));
		}catch(IndexOutOfBoundsException ibe){
			Log.e("fitmate" , "활동량과 칼로리 데이터들을 파싱하는 중에 오류가 발생하였습니다.");
		}
		
		return act;
	}
	
	/**
	 * MET 와 조도 데이터를 파싱한다.
	 * @param srcArray
	 * @return
	 */
	private int parseMETLuxData( byte[] srcArray )
	{
		int value = ( srcArray[ mIndex++ ] & 0x7F ) << 8;
		value += (srcArray[ mIndex++] & 0xFF);
		return value;
	}
	
	/**
	 * 기압 데이터를 파싱한다.
	 * @param srcArray
	 * @return
	 */
	private int parseAltitudeData( byte[] srcArray )
	{
		int alt = 0;

		try {
			alt = (srcArray[mIndex++] & 0x7F) << 8;
			alt += (srcArray[mIndex++] & 0xFF);
			alt += (srcArray[mIndex++] & 0xFF);
		}catch(IndexOutOfBoundsException ioe){
			Log.e("fitmate" , "altitude 파싱하는 중에 오류가 발생하였습니다.");
		}

		return alt;
	}
	
	/**
	 * 순간기록 시점 데이터를 파싱한다.
	 * @param srcArray
	 * @return
	 */
	private void parseMarkTimeData( byte[] srcArray )
	{
		//##### 순간기록 시점 데이터 일 때
		
		//byte[] arrMarkTime = new byte[ TimeInfo.LENGTH ];
		//System.arraycopy(srcArray, ++mIndex, arrMarkTime, 0, TimeInfo.LENGTH);
		mIndex  += TimeInfo.LENGTH;
		//TODO 사용할려면 고쳐야 함.
        //TimeInfo markTimeData = null;
	}
	
	private double getFrequency(int intervalType) {
		
		 double result = -1;
		switch( intervalType )
		{
		case IntervalType.SECOND_1:
			result = 1;
			break;
		case IntervalType.SECOND_5:
			result = 5;
			break;
		case IntervalType.SECOND_10:
			result = 10;
			break;
		case IntervalType.SECOND_30:
			result = 30;
			break;
		case IntervalType.SECOND_60:
			result = 60;
			break;
		case IntervalType.HERTZ_2:
			result = 1/2;
			break;
		case IntervalType.HERTZ_4:
			result = 1/4;
			break;
		case IntervalType.HERTZ_8:
			result = 1/8;
			break;
		case IntervalType.HERTZ_16:
			result = 1/16;
			break;
		case IntervalType.HERTZ_32:
			result = 1/32;
			break;
		case IntervalType.HERTZ_64:
			result = 1/64;
			break;
		}
		return result;
	}

	private int converToInt16( byte[] source )
	{
		int intValue = 0;
		intValue = ( ( source[1] & 0xFF ) <<8 ) + ( source[0] & 0xFF );
		return  intValue;
	}
	
	private int devideBy32ForInt(int k)
    {
        boolean negative = false;
        if (k < 0)
        {
            negative = true;
            k = -k;
        }
        k += 16;

        int r = k >> 5;
        if (negative)
        {
            r = -r;
        }
        return (int)r;
    }
	
    private int getTransValueForInt(int k, int gravity)
    {
        return (int) Math.round(k * 980.0 / 512 * gravity);
    }
    
    private int getGravity( int accRangeType )
    {
    	int gravity = 8;
    	switch( accRangeType )
    	{
    	case AcceleroRangeType.GRAVITY_2:
    		gravity = 2;
    	break;
    	case AcceleroRangeType.GRAVITY_4:
    		gravity = 4;
    	break;
    	case AcceleroRangeType.GRAVITY_8:
    		gravity = 8;
    	break;
    	case AcceleroRangeType.GRAVITY_16:
    		gravity = 16;
    	break;
    	}
    	
    	return gravity;
    }

    public List<ActivityData> getActivityData(){
		List<ActivityData> result = new ArrayList<ActivityData>();
		for( ActivityData temp : activityDataList ){
			if( temp.getSVMList().size() > 0 ){
				result.add(temp);
			}
		}
        return result;
    }
}