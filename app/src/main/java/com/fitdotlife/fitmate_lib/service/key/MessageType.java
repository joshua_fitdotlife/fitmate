package com.fitdotlife.fitmate_lib.service.key;

/**
 * Created by Joshua on 2015-02-09.
 */
public class MessageType {

    public static final int MSG_REGISTER_CLIENT = 0;
    public static final int MSG_UNREGISTER_CLIENT = 1;
    public static final int MSG_SET_USERINFO = 2;
    public static final int MSG_GET_DAY_ACTIVITY = 3;
    public static final int MSG_GET_WEEK_ACTIVITY = 4;
    public static final int MSG_GET_MONTH_ACTIVITY = 5;
    public static final int MSG_CALCULATE_ACTIVITY = 6;
    public static final int MSG_START_SERVICE = 7;
    public static final int MSG_APPLY_PROGRAM = 8;
    public static final int MSG_DATA_SYNC = 9;
    public static final int MSG_DELETE_ALLFILES =10;
    public static final int MSG_BATTERYRATE = 11;
    public static final int MSG_STOP_SERVICE = 12;
    public static final int MSG_RESTART_SERVICE = 13;
    public static final int MSG_RESET_GET_DATA = 14;
    public static final int MSG_CONNECTION_STATE = 15;
}
