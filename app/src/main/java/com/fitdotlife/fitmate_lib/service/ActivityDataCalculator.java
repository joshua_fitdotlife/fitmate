package com.fitdotlife.fitmate_lib.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v4.app.NotificationCompat;

import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ContinuousCheckPolicy;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ContinuousExerciseInfo;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseAnalyzer;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseProgram;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.FatAndCarbonhydrateConsumtion;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.StrengthInputType;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.UserInfoForAnalyzer;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.WearingLocation;
import com.fitdotlife.fitmate.CommonFunction;
import com.fitdotlife.fitmate.LoginActivity;
import com.fitdotlife.fitmate.R;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.object.DayActivity;
import com.fitdotlife.fitmate_lib.object.ScoreClass;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.object.WeekActivity;
import com.fitdotlife.fitmate_lib.service.activity.ActivityParser;
import com.fitdotlife.fitmate_lib.service.activity.DataParseException;
import com.fitdotlife.fitmate_lib.service.activity.HeaderParseException;
import com.fitdotlife.fitmate_lib.service.bluetooth.BluetoothRawData;
import com.fitdotlife.fitmate_lib.service.database.FitmateServiceDBManager;
import com.fitdotlife.fitmate_lib.service.key.MessageType;
import com.fitdotlife.fitmate_lib.service.protocol.object.ActivityData;
import com.fitdotlife.fitmate_lib.service.util.Utils;
import com.fitdotlife.fitmate_lib.util.DateUtils;

import org.androidannotations.annotations.EBean;
import org.apache.log4j.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Joshua on 2015-03-16.
 */
@EBean
public class ActivityDataCalculator extends  Thread {

    //private final String TAG = "fitmateservice - ActivityDataCalculator" + ActivityDataCalculator.class.getSimpleName();
    public static enum DayAchieveType{ DAY_ACHIEVE_BELOW_50, DAY_ACHIEVE_OVER_50, DAY_ACHIEVE_OVER_80, DAY_ACHIEVE_100  };
    public static final String DAY_ACHIEVE_KEY = "DayAchieve";
    public static final String WEEK_ACHIEVE_KEY = "WeekAchieve";

    private final String TAG = "ActivityDataCalculator";

    private FitmateDBManager mDBManager = null;
    private FitmateServiceDBManager mServiceDBManager = null;

    private boolean mCalculated = false;
    private boolean mNotifyCalculated = false;

    private Messenger mResponseMessenger = null;
    private Context mContext = null;
    private CalculateCompleteListener mListener;

    private DayActivity lastActivityData = null;


    public interface CalculateCompleteListener{
        public void onCalculateCompleted();
    }

    public ActivityDataCalculator(Context context){
        this.mContext = context;
    }

    public void setFitmateServiceDBManager( FitmateServiceDBManager dbServiceManager ){
        this.mServiceDBManager = dbServiceManager;
    }

    public void setFitmateDBManager( FitmateDBManager dbManager ){
        this.mDBManager = dbManager;
    }

    public void setCalculateCompleteListener( CalculateCompleteListener listener ){
        this.mListener = listener;
    }

    public void setNotifyCalculateComplete(  boolean notifyCalculated ){
        this.mNotifyCalculated = notifyCalculated;
    }

    public void setResponseMessenger( Messenger responseMessenger ){
        this.mResponseMessenger = responseMessenger;
    }

    private boolean isCalorieType(StrengthInputType type){
        if(type == StrengthInputType.CALORIE || type== StrengthInputType.CALORIE_SUM || type==StrengthInputType.VS_BMR){
            return true;
        }else{
            return false;
        }
    }

    public void run(){

        ContinuousCheckPolicy policy = ContinuousCheckPolicy.Loosely_SumOverXExercise;
        //현재 적용된 운동 프로그램을 가져옵니다.
        ExerciseProgram appliedExerciseProgram = this.mDBManager.getAppliedExerciseProgram();
        //사용자 정보를 가져옵니다.
        UserInfo userInfo = this.mDBManager.getUserInfo();
        //사용자 정보로부터 Analyzer 정보를 가져온다.
        UserInfoForAnalyzer userInfoForAnalyzer = Utils.getUserInfoForAnalyzer(userInfo);


        List<BluetoothRawData>  rawDataList = this.mServiceDBManager.getAllFileData();
        Log.d(TAG, "RawDataList 크기 : " + rawDataList.size());

        if( rawDataList.isEmpty() ){
            Log.d(TAG , "데이터가 없습니다...");
            quitDataCalculator(null);
            return;
        }

        ActivityParser activityParser = new ActivityParser();
        for( int i = 0 ; i < rawDataList.size() ;i++ )
        {
            BluetoothRawData rawData = null;
            try {
                rawData = rawDataList.get(i);
                activityParser.parse( rawData.getFileData() , rawData.getFileIndex() );

            } catch (DataParseException dpe) {
                Log.e( TAG ,"데이터 파일 분석중에 오류가 발생했습니다. Index = " + rawData.getFileIndex() );
                Log.e( TAG , "오류 내용 : " + dpe.getMessage() );
                continue;
            } catch (HeaderParseException e) {
                Log.e( TAG ,"데이터 파일 분석중에 헤더에서 오류가 발생했습니다. Index = " + rawData.getFileIndex() );
                Log.e( TAG ,"오류 내용 : " + e.getMessage() );
                this.mServiceDBManager.deleteFile(rawData.getFileIndex());
                continue;
            }
        }

        List<ActivityData> activityDataList = activityParser.getActivityData();
        if( activityDataList.size() == 0 ) {
            this.quitDataCalculator(null);
            return;
        }

        List<DayActivity> dayActivityList = new ArrayList<DayActivity>();

        //rawdataList의 데이터들을 날짜별 데이터로 만든다.
        for( int index = 0 ; index < activityDataList.size() ;index++ )
        {
            try
            {
                ActivityData activityData = activityDataList.get(index);

                int wearingPosition = activityData.getSystemInfoResponse().getSystemInfo().getWearingPosition();
                //Log.i( TAG , "착용 위치 : " + wearingPosition );

                WearingLocation location = WearingLocation.WAIST;
                if( wearingPosition > 1 ) {
                    location = WearingLocation.values()[activityData.getSystemInfoResponse().getSystemInfo().getWearingPosition() - 1];
                }

                ExerciseAnalyzer analyzer = new ExerciseAnalyzer( appliedExerciseProgram, userInfoForAnalyzer, location , (int) activityData.getSaveInterval());
                List<Integer> svmList = activityData.getSVMList();

                int calorieByActivity = 0;
                float[] metArray = new float[(24 * 60 * 60) / (int) activityData.getSaveInterval()];

                DayActivity dayActivity = new DayActivity();
                float[] rawMetArray = new float[svmList.size()];
                dayActivity.addFileIndex(activityData.getRawFileIndex());

                //칼로리와 MET를 계산한다.
                int metArrayIndex = ((activityData.getStartTime().getHour() * 60 * 60) + (activityData.getStartTime().getMinute() * 60) + activityData.getStartTime().getSecond()) / (int) activityData.getSaveInterval();
                for (int i = 0; i < svmList.size(); i++)
                {
                    int calorie = analyzer.Calcuate_Calorie(svmList.get(i));
                    calorieByActivity += calorie;
                    float met = analyzer.Calculate_METbyCaloire(calorie);
                    rawMetArray[i] = met;
                    if(  ( metArrayIndex + i ) < metArray.length  ) {
                        metArray[metArrayIndex + i] = met;
                    }
                }

                //저중고 강도 운동시간을 계산한다.
                int[] arrHMLTime = analyzer.MinutesforEachStrength(rawMetArray);

                dayActivity.setRawMetArray(rawMetArray);
                dayActivity.setStartTime(activityData.getStartTime());
                dayActivity.setCalorieByActivity((int) Math.round(calorieByActivity / 1000.0));
                dayActivity.setHmlStrengthTime(arrHMLTime);
                dayActivity.setMetArray(metArray);
                dayActivity.setDataSaveInterval((int) activityData.getSaveInterval());

                //평균 MET를 계산한다.
                dayActivity.setAverageMET(analyzer.CalculateAverageMET(rawMetArray));

                //AhcieveOfTarget를 계산한다.
                StrengthInputType strengthInputType= appliedExerciseProgram.getStrengthInputType();
                int todayDayOfAchieve = 0;

                if(isCalorieType(strengthInputType)){
                    todayDayOfAchieve= dayActivity.getCalorieByActivity();
                }else{
                    //AchieveOfTarget 를 계산한다.
                    List<ContinuousExerciseInfo> todayExerciseInfoList = analyzer.getContinuousExercise(policy, rawMetArray, dayActivity.getStartTime().getMilliSecond() , 10, 60, 60, 600);

                    if (todayExerciseInfoList.size() > 0) {
                        ContinuousExerciseInfo todayExerciseInfo = todayExerciseInfoList.get(0);
                        todayDayOfAchieve = Math.round((todayExerciseInfo.getTimeSpan()) / (60 * 1000));
                    }
                }

                //지방, 탄수화물, 이동거리를 계산한다.
                List<Float> metList = new ArrayList<>();
                for( float f : metArray ){
                    metList.add( f );
                }

                FatAndCarbonhydrateConsumtion fatBurn = analyzer.getFatandCarbonhydrate( metList , (int) activityData.getSaveInterval() );
                float fat = fatBurn.getFatBurned();
                float carbon = fatBurn.getCarbonhydrateBurned();
                double distance = ExerciseAnalyzer.getDistanceFromMETArray_over2MET( metList , (int) activityData.getSaveInterval() );

                dayActivity.setFat( fat );
                dayActivity.setCarbohydrate( carbon );
                dayActivity.setDistance( distance );

                dayActivity.setAchieveOfTarget(todayDayOfAchieve);

                //이전데이터와 겹치는지 확인한다.
                if (dayActivityList.size() > 0) {

                    //최근에 날짜목록에 저장된 데이터를 가져온다. 날짜목록에 있는 데이터의 날짜와 현재의 날짜가 겹치는 확인한다.
                    //저장간격이 같은지 확인한다.
                    //날짜도 같고 저장간격도 같다면 데이터를 합친다.
                    DayActivity preDayActivity = dayActivityList.get(dayActivityList.size() - 1);

                    if (dayActivity.getActivityDate().equals(preDayActivity.getActivityDate())) { //날짜가 겹치는지 확인한다.
                        preDayActivity.add(dayActivity);
                        continue;
                    }
                }

                dayActivityList.add( dayActivity );
            }catch( Exception e ){
                Log.e(TAG , e.getMessage() );
                this.quitDataCalculator( null );
                return;
            }

        }//END FOR


        //마지막 날짜의 시작 파일 인덱스를 저장한다.
        Log.i(TAG, "Day Activity 목록의 크기 : " + dayActivityList.size() );

        //####
        // 일 데이터를 DB에 삽입한다.
        // 삽입한 일 데이터에 해당하는 주는 다시 계산한다.
        //####
        ArrayList<String> weeksNeedUpdate= new ArrayList<>();
        String weekStartDate = null;
        JSONArray jsonWeekArray = new JSONArray();

        for(int i=0;i<dayActivityList.size();i++)
        {
            DayActivity day=  mDBManager.getDayActivity( dayActivityList.get(i).getActivityDate() );
            if(day==null){
                mDBManager.setDayActivity(dayActivityList.get(i));
            }else{
                WearingLocation location =WearingLocation.WAIST;
                ExerciseAnalyzer analyzer = new ExerciseAnalyzer(appliedExerciseProgram, userInfoForAnalyzer, location, (int) day.getDataSaveInterval());
                dayActivityList.set(i,Utils.mergyDayActivity( day , dayActivityList.get(i), analyzer) );
                mDBManager.updateDayActivity( dayActivityList.get(i) );
            }

            Calendar weekCalendar = Calendar.getInstance();
            weekCalendar.setTimeInMillis( dayActivityList.get(i).getStartTime().getMilliSecond() );

            int dayOfWeek = weekCalendar.get(Calendar.DAY_OF_WEEK);
            weekCalendar.add(Calendar.DATE, -(dayOfWeek - 1));
            weekStartDate = this.getDateString( weekCalendar );

            if( !weeksNeedUpdate.contains( weekStartDate ) )
            {
                weeksNeedUpdate.add(( weekStartDate ));
            }

            JSONObject dayObject = new JSONObject();
            try {
                dayObject.put("daydate" , dayActivityList.get(i).getActivityDate() );
                dayObject.put("dayindex" , dayOfWeek);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            boolean isContain = false;
            for( int j = 0 ; j < jsonWeekArray.length() ; j++ )
            {

                try {

                    JSONObject jsonWeekObject = jsonWeekArray.getJSONObject(j);
                    if(weekStartDate.equals( jsonWeekObject.getString("weekdate") )){

                        isContain = true;
                        JSONArray jsonDayArray = jsonWeekObject.getJSONArray("daylist");
                        jsonDayArray.put( dayObject );
                        break;

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            if( !isContain ){

                try {
                    JSONObject jsonWeekObject = new JSONObject();
                    JSONArray jsonDayArray = new JSONArray();
                    jsonDayArray.put(dayObject);
                    jsonWeekObject.put("weekdate", weekStartDate );
                    jsonWeekObject.put("daylist", jsonDayArray);
                    jsonWeekArray.put(jsonWeekObject);
                }catch(JSONException e){ }
            }

        }//End for

        for(int i=0;i<weeksNeedUpdate.size();i++){
            recalculateweek(DateUtils.getDateFromString_yyyy_MM_dd(weeksNeedUpdate.get(i)));
        }

        //오늘 목표 로컬 푸쉬를 확인한다.
        checkLocalPush();

        Bundle recalculateWeekListBundle = new Bundle();
        recalculateWeekListBundle.putString("recalculateweeklist", jsonWeekArray.toString());

        quitDataCalculator( recalculateWeekListBundle );
    }


    private void checkLocalPush()
    {
        Date todayDate = new Date();
        String strTodayDate = DateUtils.getDateString_yyyy_MM_dd(todayDate);
        DayActivity todayActivity = mDBManager.getDayActivity( strTodayDate );

        //오늘 날짜 데이터가 없으면
        if( todayActivity == null ) return;

        //주간 목표 확인
        Calendar weekCalendar = Calendar.getInstance();
        int dayOfWeek = weekCalendar.get(Calendar.DAY_OF_WEEK);
        weekCalendar.add(Calendar.DATE, -(dayOfWeek - 1));
        String weekStartDate = DateUtils.getDateString(weekCalendar);
        WeekActivity weekActivity = mDBManager.getWeekActivity(weekStartDate);

        if( weekActivity.getScore() >= 100 )
        {

            //주간 목표 달성 푸쉬를 보냈는지 확인한다.
            if( !isSendWeekAchievePush( weekStartDate ) ){
                showNotification( "Fitmate" , mContext.getString( R.string.noti_week_achieve_success) , mContext.getString( R.string.noti_week_achieve_success) , 522 );
                setSendWeekAchievePush( weekStartDate , true );
            }
            //주간 목표를 달성했으면 오늘 운동 목표에 대한 푸쉬를 보내지 않는다.
            return;
        }

        boolean isCaloridType = false;
        float goalValue = 0;
        //운동 프로그램을 가져온다.
        com.fitdotlife.fitmate_lib.object.ExerciseProgram program = mDBManager.getAppliedProgram();
        //운동프로그램 종류를 확인한다.
        StrengthInputType strengthInputType = StrengthInputType.values()[ program.getCategory() ];

        if( strengthInputType== StrengthInputType.CALORIE|| strengthInputType == StrengthInputType.CALORIE_SUM || strengthInputType == StrengthInputType.VS_BMR){
            isCaloridType = true;

            if( strengthInputType.equals( StrengthInputType.CALORIE) ){
                goalValue = program.getStrengthFrom();
            }else if(strengthInputType.equals( StrengthInputType.CALORIE_SUM )){

                goalValue = program.getStrengthFrom() / 7;

            }else{

                UserInfoForAnalyzer userInfoForAnalyzer = Utils.getUserInfoForAnalyzer( mDBManager.getUserInfo() );
                goalValue =  ( userInfoForAnalyzer.getBMR() * program.getStrengthFrom() ) * 100;

            }

        }else{

            goalValue = program.getMinuteFrom();

        }

        int achieveOfTarget = todayActivity.getAchieveOfTarget();
        int achievePercent = (int) (( achieveOfTarget / goalValue ) * 100);

        DayAchieveType dayAchieveType = DayAchieveType.values()[getDayAchieve(strTodayDate) ];
        if(achievePercent < 50) //오늘 목표대비 50% 미만일 때 암것도 안한다.
        {
        }
        else if( achievePercent < 80 ) //오늘 목표대비 50% 이상 ~ 80% 미만일 때
        {
            if( !dayAchieveType.equals( DayAchieveType.DAY_ACHIEVE_OVER_50) )
            {
                String achieveMessage = String.format( mContext.getString(R.string.noti_day_achieve_over_50) , achievePercent );
                int possibleValue = todayActivity.getPossibleValue();
                if( isCaloridType ){
                    achieveMessage +=  " " + String.format( mContext.getString( R.string.noti_day_achieve_need_calorie ) , possibleValue );
                }else{
                    achieveMessage +=  " " + String.format( mContext.getString( R.string.noti_day_achieve_need_intensity ) , possibleValue );
                }

                showNotification("Fitmate", achieveMessage, achieveMessage , 523);
                setDayAchieve(strTodayDate, DayAchieveType.DAY_ACHIEVE_OVER_50.ordinal());
            }

        }else if( achievePercent < 100 ){ //오늘 목표대비 80% 이상 ~ 100% 미만일 때

            if( !dayAchieveType.equals( DayAchieveType.DAY_ACHIEVE_OVER_80) ) {

                String achieveMessage = String.format( mContext.getString(R.string.noti_day_achieve_over_50) , achievePercent);
                int possibleValue = todayActivity.getPossibleValue();

                if( isCaloridType ){
                    achieveMessage +=  " " + String.format( mContext.getString( R.string.noti_day_achieve_need_calorie ) , possibleValue );
                }else{
                    achieveMessage +=  " " + String.format( mContext.getString( R.string.noti_day_achieve_need_intensity ) , possibleValue );
                }

                showNotification("Fitmate", achieveMessage, achieveMessage , 524);
                setDayAchieve(strTodayDate, DayAchieveType.DAY_ACHIEVE_OVER_80.ordinal());
            }

        }else{ //오늘 목표대비 100% 달성했을 때

            if( !dayAchieveType.equals( DayAchieveType.DAY_ACHIEVE_100 ) )
            {
                showNotification("Fitmate", mContext.getString(R.string.noti_day_achieve_success), mContext.getString(R.string.noti_day_achieve_success) , 525);
                setDayAchieve( strTodayDate , DayAchieveType.DAY_ACHIEVE_100.ordinal() );
            }
        }
    }

    private boolean recalculateweek( Date weekStartDate )
    {
        FitmateDBManager dbManager = new FitmateDBManager(mContext);
        UserInfo userTable =dbManager.getUserInfo();
        WeekActivity weekTable= dbManager.getWeekActivity(DateUtils.getDateString_yyyy_MM_dd(weekStartDate));

        Date weekEndDate = new Date(weekStartDate.getTime() + 86400*6*1000);
        List<DayActivity> dayActivitySet = dbManager.getDayActivityList(DateUtils.getDateString_yyyy_MM_dd(weekStartDate), DateUtils.getDateString_yyyy_MM_dd(weekEndDate));

        DayActivity lastDayActivityTable= dayActivitySet.get(dayActivitySet.size() - 1);

        int exerciseProgramId= dbManager.getAppliedProgram().getId();

        // 주간 데이터가 없을 때 확인한다.
        boolean needInsert= true;
        if(weekTable==null)
        {
            needInsert= true;
            weekTable = new WeekActivity();
            weekTable.setActivityDate(DateUtils.getDateString_yyyy_MM_dd(weekStartDate));
            weekTable.setExerciseProgramID(exerciseProgramId);

        }else{

            exerciseProgramId = weekTable.getExerciseProgramID();
            needInsert= false;
        }

        //   weekTable = lastDayActivityTable.datasavinginterval;
        com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseProgram exTable =dbManager.getExerciseProgramForAnalyzer(exerciseProgramId);

        UserInfoForAnalyzer userInfoForAnalyzer = com.fitdotlife.fitmate_lib.service.util.Utils.getUserInfoForAnalyzer(userTable);

        com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseProgram program =dbManager.getExerciseProgramForAnalyzer(exerciseProgramId);

        WearingLocation location = WearingLocation.WAIST;

        ExerciseAnalyzer analyzer = new ExerciseAnalyzer(exTable, userInfoForAnalyzer, location , (int) lastDayActivityTable.getDataSaveInterval());

        List<Integer> arrayforScore = new ArrayList<Integer>();

        boolean calorieType=false;

        if(program.getStrengthInputType()== StrengthInputType.CALORIE|| program.getStrengthInputType()== StrengthInputType.CALORIE_SUM ||program.getStrengthInputType()== StrengthInputType.VS_BMR){
            calorieType=true;
        }
        Date tempDate=weekStartDate;

        int str_h=0;
        int str_m=0;
        int str_l=0;
        int str_ul=0;
        int calorie=0;
        float metSm=0;
        int nullIndex=0;

        List<Integer> nullIndexArray= new ArrayList<Integer>();
        for(DayActivity table: dayActivitySet){

            Date date = DateUtils.getDateFromString_yyyy_MM_dd(table.getActivityDate());

            while( tempDate.compareTo(date) <0){
                arrayforScore.add(0);
                tempDate=new Date(tempDate.getTime()+86400000);
                nullIndexArray.add(nullIndex);
                nullIndex++;
            }
            if(calorieType){
                table.setAchieveOfTarget(table.getCalorieByActivity());
            }else{
                List<ContinuousExerciseInfo> continuousEx = analyzer.getContinuousExercise(ContinuousCheckPolicy.Loosely_SumOverXExercise, table.getMetArray(),date.getTime(), 1, 60,60, 600 );
                if(continuousEx==null || continuousEx.size()==0){
                    table.setAchieveOfTarget(0);
                }
                else{
                    if(analyzer.getUserInfo().getContinuousCheckPolicy() == ContinuousCheckPolicy.Loosely_SumOverXExercise){
                        int seconds = 0;
                        for (ContinuousExerciseInfo info : continuousEx) {
                            seconds+= info.getTimeSpan()/1000;
                        }
                        table.setAchieveOfTarget((int)Math.round(seconds / 60.0));
                    }
                    else{
                        int minutes =(int) Math.round( continuousEx.get(0).getTimeSpan()/(60.0*1000));
                        table.setAchieveOfTarget(minutes);
                    }
                }
            }

            if(table.getProgramID()!= exerciseProgramId)
            {
                table.setProgramID(exerciseProgramId);
            }
            arrayforScore.add(table.getAchieveOfTarget());
            calorie += table.getCalorieByActivity();
            metSm += table.getAverageMET();
            str_h+= table.getStrengthHigh();
            str_m+= table.getStrengthMedium();
            str_l+= table.getStrengthLow();
            str_ul+= table.getStrengthUnderLow();
            tempDate=new Date(tempDate.getTime() +86400*1000);

            nullIndex++;
        }



        if(dayActivitySet.size()>0){

            str_h = str_h/dayActivitySet.size();

            str_m = str_m/dayActivitySet.size();

            str_l = str_l/dayActivitySet.size();

            str_ul = str_ul/dayActivitySet.size();

            calorie =calorie/dayActivitySet.size();

            metSm = metSm/dayActivitySet.size();

        }

        weekTable.setAverageCalorie(calorie);

        weekTable.setAverageStrengthHigh(str_h);
        weekTable.setAverageStrengthMedium(str_m);
        weekTable.setAverageStrengthLow(str_l);
        weekTable.setAverageStrengthUnderLow(str_ul);

        weekTable.setAverageMET(metSm);

        weekTable.setActivityDate(DateUtils.getDateString_yyyy_MM_dd(weekStartDate));

        List<ScoreClass> scoreArray = new ArrayList<ScoreClass>();
        if(calorieType){
            scoreArray=CommonFunction.CalculateDayScores_Calories(arrayforScore,analyzer);

        }
        else{
            scoreArray=CommonFunction.CalculateDayScores_Minutes(arrayforScore ,analyzer);
        }
        int nth=0;

        for(int i=0;i<scoreArray.size() ;i++)
        {
            if(nth<nullIndexArray.size()){

                if( nullIndexArray.get(nth)== i){

                    nth++;
                    continue;
                }

            }



            ScoreClass score= scoreArray.get(i);

            DayActivity act= dayActivitySet.get(i-nth);

            act.setPredayScore(score.preDayAchieve);
            act.setTodayScore(score.todayGetPoint);
            act.setExtraScore(score.todayPossibleMorePoint);
            act.setAchieveMax(score.isAchieveMax);
            act.setPossibleMaxScore(score.possibleMaxScore);
            act.setPossibleTimes(score.possibleTimes);
            act.setPossibleValue(score.possibleValue);
            ///notuploaded==0
            act.setUploadState(0);

            dbManager.updateDayActivity( act );
        }

        ScoreClass score= scoreArray.get(scoreArray.size()-1);

        weekTable.setScore(score.weekAchieve);

        if(needInsert){
            dbManager.setWeekActivity(weekTable , FitmateDBManager.NOT_UPLOADED );
        }else{
            dbManager.updateWeekActivity(weekTable);
        }

        return true;
    }

    private void quitDataCalculator(Bundle recalculateWeekListBundle)
    {
        this.mListener.onCalculateCompleted();
        if( mNotifyCalculated )
        {
            this.sendMessage( recalculateWeekListBundle );
        }
    }

    private String getDateString( Calendar calendar )
    {
        return calendar.get(Calendar.YEAR) + "-" +String.format("%02d", calendar.get(Calendar.MONTH) + 1) + "-" + String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH));
    }

    private void sendMessage( Bundle recalculateWeekListBundle ){
        Message msg = Message.obtain(null, MessageType.MSG_CALCULATE_ACTIVITY);

        if (recalculateWeekListBundle != null ){
            msg.setData(recalculateWeekListBundle);
        }

        try {

            if( this.mResponseMessenger != null )
            {
                this.mResponseMessenger.send(msg);
            }

        } catch (RemoteException e) {

        }
    }

    private void showNotification( String title , String content , String notification , int id )
    {
        NotificationManager nm = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0, new Intent(mContext, LoginActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mCompatBuilder = new NotificationCompat.Builder(mContext);
        mCompatBuilder.setSmallIcon(R.drawable.fitlife_ico);
        mCompatBuilder.setTicker(notification);
        mCompatBuilder.setWhen(System.currentTimeMillis());
        mCompatBuilder.setContentTitle(title);
        mCompatBuilder.setContentText(content);
        mCompatBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(content));
        mCompatBuilder.setVisibility(Notification.VISIBILITY_PUBLIC);
        mCompatBuilder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);
        mCompatBuilder.setContentIntent(pendingIntent);
        mCompatBuilder.setAutoCancel(true);

        nm.notify(222, mCompatBuilder.build());
    }

    private boolean isSendWeekAchievePush( String weekStartDate  ){
        SharedPreferences pref = mContext.getSharedPreferences("fitmate", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        return pref.getBoolean(WEEK_ACHIEVE_KEY + "_" + weekStartDate, false);
    }

    private void setSendWeekAchievePush( String weekStartDate , boolean isSetWeekAchieve   ){
        SharedPreferences pref = mContext.getSharedPreferences("fitmate", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(WEEK_ACHIEVE_KEY + "_" + weekStartDate, isSetWeekAchieve);
        editor.commit();
    }

    private int getDayAchieve( String dayDate  ){
        SharedPreferences pref = mContext.getSharedPreferences("fitmate", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        return pref.getInt(DAY_ACHIEVE_KEY + "_" + dayDate, 0 );
    }

    private void setDayAchieve( String dayDate , int dayAchieve   ){
        SharedPreferences pref = mContext.getSharedPreferences("fitmate", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(DAY_ACHIEVE_KEY + "_" + dayDate, dayAchieve );
        editor.commit();
    }

}

//    private void calculate(){
//        //newsServiceClient.setRootUrl(NetworkClass.baseURL + "/api");
//
//        ContinuousCheckPolicy policy = ContinuousCheckPolicy.Loosely_SumOverXExercise;
//        ExerciseProgram appliedExerciseProgram = this.mDBManager.getAppliedExerciseProgram();
//
//        UserInfo userInfo = this.mDBManager.getUserInfo();
//        UserInfoForAnalyzer userInfoForAnalyzer = new UserInfoForAnalyzer(NewHomeUtils.calculateAge( userInfo.getBirthDate() ) , userInfo.getGender().getBoolean() , (int)userInfo.getHeight() , (int)userInfo.getWeight() , policy , 10 , USERVO2MAXLEVEL.Fair );
//
//        List<BluetoothRawData>  rawDataList = this.mServiceDBManager.getAllFileData();
//        Log.d(TAG, "RawDataList 크기 : " + rawDataList.size());
//
//        if( rawDataList.isEmpty() ){
//            Log.d(TAG , "데이터가 없습니다...");
//            quitDataCalculator();
//            return;
//        }
//
//        ActivityParser activityParser = new ActivityParser();
//        for( int i = 0 ; i < rawDataList.size() ;i++ ){
//            BluetoothRawData rawData = null;
//            try {
//                rawData = rawDataList.get(i);
//                activityParser.parse( rawData );
//
//            } catch (DataParseException dpe) {
//                Log.e( TAG ,"데이터 파일 분석중에 오류가 발생했습니다. Index = " + rawData.getFileIndex() );
//                Log.e( TAG , "오류 내용 : " + dpe.getMessage() );
//                continue;
//            } catch (HeaderParseException e) {
//                Log.e( TAG ,"데이터 파일 분석중에 헤더에서 오류가 발생했습니다. Index = " + rawData.getFileIndex() );
//                Log.e( TAG ,"오류 내용 : " + e.getMessage() );
//                this.mServiceDBManager.deleteFile(rawData.getFileIndex());
//                continue;
//            }
//        }
//
//        List<ActivityData> activityDataList = activityParser.getActivityData();
//        Log.d(TAG, "Activity Data 목록의 크기 : " + activityDataList.size());
//        if( activityDataList.size() == 0 ) {
//            this.quitDataCalculator();
//            return;
//        }
//
//        List<DayActivity> dayActivityList = new ArrayList<DayActivity>();
//
//        //날짜별로 데이터를 만든다.
//        //날짜에 따른 칼로리, MET , 저중고 운동시간, 평균 MET , 운동시간을 계산한다.
//        //계산되어진 활동량 정보를 날짜목록에 저장한다.
//        for( int index = 0 ; index < activityDataList.size() ;index++ )
//        {
//            try {
//                ActivityData activityData = activityDataList.get(index);
//
//                WearingLocation location = WearingLocation.values()[ activityData.getSystemInfoResponse().getSystemInfo().getWearingPosition() - 1];
//
//                ExerciseAnalyzer analyzer = new ExerciseAnalyzer(appliedExerciseProgram, userInfoForAnalyzer, location , (int) activityData.getSaveInterval());
//                List<Integer> svmList = activityData.getSVMList();
//
//                int calorieByActivity = 0;
//                float[] metArray = new float[(24 * 60 * 60) / (int) activityData.getSaveInterval()];
//
//                DayActivity dayActivity = new DayActivity();
//                float[] rawMetArray = new float[svmList.size()];
//                dayActivity.addFileIndex(activityData.getRawFileIndex());
//
//                //칼로리와 MET를 계산한다.
//                int metArrayIndex = ((activityData.getStartTime().getHour() * 60 * 60) + (activityData.getStartTime().getMinute() * 60) + activityData.getStartTime().getSecond()) / (int) activityData.getSaveInterval();
//                for (int i = 0; i < svmList.size(); i++)
//                {
//                    int calorie = analyzer.Calcuate_Calorie(svmList.get(i));
//                    calorieByActivity += calorie;
//                    float met = analyzer.Calculate_METbyCaloire(calorie);
//                    rawMetArray[i] = met;
//                    if(  ( metArrayIndex + i ) < metArray.length  ) {
//                        metArray[metArrayIndex + i] = met;
//                    }
//                }
//
//                //저중고 강도 운동시간을 계산한다.
//                int[] arrHMLTime = analyzer.MinutesforEachStrength(rawMetArray);
//                dayActivity.setRawMetArray(rawMetArray);
//                dayActivity.setStartTime(activityData.getStartTime());
//                dayActivity.setCalorieByActivity((int) Math.round(calorieByActivity / 1000.0));
//                dayActivity.setHmlStrengthTime(arrHMLTime);
//                dayActivity.setMetArray(metArray);
//                dayActivity.setDataSaveInterval((int) activityData.getSaveInterval());
//
//                //평균 MET를 계산한다.
//                dayActivity.setAverageMET(analyzer.CalculateAverageMET(rawMetArray));
//
//
//                StrengthInputType strengthInputType= appliedExerciseProgram.getStrengthInputType();
//                int todayDayOfAchieve = 0;
//
//                if(isCalorieType(strengthInputType)){
//                    todayDayOfAchieve= dayActivity.getCalorieByActivity();
//                }else{
//                    //AchieveOfTarget 를 계산한다.
//                    List<ContinuousExerciseInfo> todayExerciseInfoList = analyzer.getContinuousExercise(policy, rawMetArray, dayActivity.getStartTime().getMilliSecond() , 10, 60, 60, 600);
//
//                    if (todayExerciseInfoList.size() > 0) {
//                        ContinuousExerciseInfo todayExerciseInfo = todayExerciseInfoList.get(0);
//                        todayDayOfAchieve = Math.round((todayExerciseInfo.getTimeSpan()) / (60 * 1000));
//                    }
//
//                }
//
//                dayActivity.setAchieveOfTarget(todayDayOfAchieve);
//                //이전데이터와 겹치는지 확인한다.
//                if (dayActivityList.size() > 0) {
//
//                    //최근에 날짜목록에 저장된 데이터를 가져온다. 날짜목록에 있는 데이터의 날짜와 현재의 날짜가 겹치는 확인한다.
//                    //저장간격이 같은지 확인한다.
//                    //날짜도 같고 저장간격도 같다면 데이터를 합친다.
//                    DayActivity preDayActivity = dayActivityList.get(dayActivityList.size() - 1);
//
//                    if (dayActivity.getActivityDate().equals(preDayActivity.getActivityDate())) { //날짜가 겹치는지 확인한다.
//                        //if( lastCalculateFileIndex ==  dayActivity.getLastDayActivity().getLastFileIndex() ){
//                        //    preDayActivity.overwrite( dayActivity , this.getPreviousCalorie() , new int[]{ this.getPreviousStrengthUnderLow() , this.getPreviousStrengthLow() , this.getPreviousStrengthMedium() , this.getPreviousStrengthHigh() } );
//                        //    continue;
//                        //}else {
//                        preDayActivity.add(dayActivity);
//                        continue;
//                        //}
//                    }
//                }
//
//                dayActivityList.add(dayActivity);
//            }catch( Exception e ){
//                Log.e(TAG , e.getMessage() );
//                this.quitDataCalculator();
//                return;
//            }
//
//        }//END FOR
//
//
//        //마지막 날짜의 시작 파일 인덱스를 저장한다.
//        Log.d(TAG, "Day Activity 목록의 크기 : " + dayActivityList.size());
//
//        //####
//        // 위에서 일별 활동량 목록을 계산을 다 했다.
//        // 주 활동량을 계산 해야 하는데 먼저 각 날짜가 어느 주에 속하는지 알아야 한다.
//        // 각 날짜는 52주 중에 한 주에 해당할 것이다. 각 날짜의 주 숫자를 알아내어서 주활동량을 계산하게 된다.
//        // 그리고, 위의 일 활동량 목록의 첫날이 주중 요일이라면 주의 처음부터 데이터가 필요하다.
//        //예를 들면 일 활동량 목록이 첫날이 화요일 이라면 주의 처음부터 화요일까지 데이터가 필요하다.
//        //그래서 아래의 코드는 일 활동량 목록의 첫날이 무슨 요일인지 알아내고 데이터베이스로부터 이 날짜가 속한 주 활동량을 가져와서 먼저 계산하도록 한다.
//        //####
//
//        //일 활동량 목록에서 첫날이 속한 요일의 시작일 요일의 종료일 가져온다.
//        int preDataSaveInterval = 0;
//        Calendar weekCalendar = Calendar.getInstance();
//        weekCalendar.setTimeInMillis(dayActivityList.get(0).getStartTime().getMilliSecond());
//
//        int dayOfWeek = weekCalendar.get( Calendar.DAY_OF_WEEK );
//        weekCalendar.add(Calendar.DATE, -(dayOfWeek - 1));
//        String weekStartDate = this.getDateString( weekCalendar );
//        Log.d(TAG, "주 시작일 : " + weekStartDate );
//
//        weekCalendar.add(Calendar.DATE, 6);
//        String weekEndDate = this.getDateString( weekCalendar );
//        Log.d(TAG, "주 종료일 : " + weekEndDate );
//
//        int weekHighTimeSum = 0;
//        int weekMediumTimeSum = 0;
//        int weekLowTimeSum = 0;
//        int weekUnderLowTimeSum = 0;
//        int weekCalorieSum = 0;
//        float weekMETSum = 0;
//        int predayScore = 0;
//
//        List<Integer> calorieList = new ArrayList<Integer>();
//        List<Integer> scoreCalorieList = new ArrayList<Integer>();
//        List<List<Integer>> weekContinuousExerciseList = new ArrayList<List<Integer>>();
//        List<Integer> weekExerciseList = new ArrayList<Integer>();
//
//        List<DayActivity> weekActivityList = this.mDBManager.getDayActivityList( weekStartDate , weekEndDate );
//        Log.d(TAG, "데이터베이스로부터 가져온 첫번째 날짜가 해당하는 주의 일 활동량 목록 크기 : " + weekActivityList.size() );
//
//        if( weekActivityList.size() > 0 ) {
//
//            DayActivity fitstDayActivity = dayActivityList.get(0);
//
//            String firstDateofIncomming=fitstDayActivity.getActivityDate();
//            while(weekActivityList.size()>0){
//                if(weekActivityList.get(weekActivityList.size()-1).getActivityDate().compareTo(firstDateofIncomming)>=0)
//                {
//                    weekActivityList.remove(weekActivityList.size()-1);
//                }else{
//                    break;
//                }
//            }
//
//            for (int i = 0; i < weekActivityList.size(); i++) {
//
//                DayActivity preDayActivity = weekActivityList.get(i);
//
//                //휘트미터로부터 읽어온 일별 데이터 중 DB로부터 읽어온 첫번째 데이터의 날짜와 같은 데이터가 데이터베이스에 있을 수 있다.
//                //온종일 데이터를 저장하지 않을 수 있기때문에
//                //여기서 날짜가 겹치는지 확인한다.
//                if (fitstDayActivity.getActivityDate().equals( preDayActivity.getActivityDate() )) {
//                    Log.d(TAG, "데이터베이스로부터 가져온 일 활동량과 현재 목록의 일 활동량이 겹침. : 인덱스" + i );
//                    break;
//                }
//
//                weekHighTimeSum += preDayActivity.getStrengthHigh();
//                weekMediumTimeSum += preDayActivity.getStrengthMedium();
//                weekLowTimeSum += preDayActivity.getStrengthMedium();
//                weekUnderLowTimeSum += preDayActivity.getStrengthUnderLow();
//                weekCalorieSum += preDayActivity.getCalorieByActivity();
//                weekMETSum += preDayActivity.getAverageMET();
//                calorieList.add(preDayActivity.getCalorieByActivity());
//                scoreCalorieList.add(preDayActivity.getCalorieByActivity());
//
//                List<Integer> achieveOfTageList =  new ArrayList<Integer>();
//                achieveOfTageList.add( preDayActivity.getAchieveOfTarget() );
//                weekContinuousExerciseList.add( achieveOfTageList );
//                weekExerciseList.add( preDayActivity.getAchieveOfTarget() );
//
//                //predayScore = preDayActivity.getPredayScore() + preDayActivity.getTodayScore();
//            } //End For
//        } // End If
//
//        int preWeekNumber = weekCalendar.get( Calendar.WEEK_OF_YEAR );
//        Log.d(TAG, "주의 숫자 : " + preWeekNumber );
//        //그 날짜의 주간 첫날과 일 목록의 첫날까지의 일 활동량을 디비에서 가져온다.
//        //일을 업데이트한다.
//
//
//        boolean ismergied=false;
//        for( int i = 0 ; i < dayActivityList.size() ;i++ ) {
//
//            //#### Mergy
//            DayActivity dayActivity = dayActivityList.get(i);
//            //mergy 조건... DB에서 읽은 ACTIVITY값의 upload필드가 mergy이고 같은 날자일때 머지 필요
//
//            DayActivity dbActivity = mDBManager.getNeedMergyDayActivity();
//            if (dbActivity != null) {
//                String a = dbActivity.getActivityDate();
//                String b = dayActivity.getActivityDate();
//                int calorie = dayActivity.getCalorieByActivity();
//                if (dbActivity.getActivityDate().equals(dayActivity.getActivityDate())) {
//                    WearingLocation location = WearingLocation.values()[userInfo.getWearAt() - 1];
//                    ExerciseAnalyzer analyzer = new ExerciseAnalyzer(appliedExerciseProgram, userInfoForAnalyzer, location, (int) dbActivity.getDataSaveInterval());
//                    dayActivityList.set(i, NewHomeUtils.mergyDayActivity(dayActivity, dbActivity, analyzer));
//                    ismergied = true;
//                }
//            }
//
//            weekCalendar.setTimeInMillis(dayActivity.getStartTime().getMilliSecond());
//            int weekNumber = weekCalendar.get(Calendar.WEEK_OF_YEAR);
//
//            Log.d(TAG, "### 주의 숫자 : " + weekNumber + "");
//            Log.d(TAG, "### 날짜 : " + dayActivity.getActivityDate());
//            Log.d(TAG, "### 시작시간 : " + dayActivity.getStartTime().getTimeString());
//
//            //다음주로 넘어가거나 마지막 데이터 일 때 지금까지 모아놓은 데이터로 주 활동량을 계산한다.
//            if (weekNumber > preWeekNumber) {
//
//                ExerciseAnalyzer exerciseAnalyzer = new ExerciseAnalyzer(appliedExerciseProgram, userInfoForAnalyzer, WearingLocation.WRIST, dayActivity.getDataSaveInterval());
//
//                //점수를 계산한다.
//                int weekAchieve = 0;
//                if (appliedExerciseProgram.getStrengthInputType().equals(StrengthInputType.CALORIE)) {
//                    weekAchieve = exerciseAnalyzer.CalculateWeekAchievement_Calorie(scoreCalorieList);
//                } else if (appliedExerciseProgram.getStrengthInputType().equals(StrengthInputType.VS_BMR)) {
//                    weekAchieve = exerciseAnalyzer.CalculateWeekAchievement_VS_BMR(scoreCalorieList);
//                } else if (appliedExerciseProgram.getStrengthInputType().equals(StrengthInputType.CALORIE_SUM)) {
//                    weekAchieve = exerciseAnalyzer.CalculateWeekAchievement_CalorieSum(weekCalorieSum);
//                } else {
//                    weekAchieve = exerciseAnalyzer.CalcuateWeekAchieveByMinutesList(policy, weekContinuousExerciseList);
//                }
//
//                int size = weekActivityList.size();
//                int averageWeekHighTime = weekHighTimeSum / size;
//                int averageWeekMediumTime = weekMediumTimeSum / size;
//                int averageWeekLowTime = weekLowTimeSum / size;
//                int averageWeekUnderLowTime = weekUnderLowTimeSum / size;
//                int averageWeekCalorie = weekCalorieSum / size;
//                float averageWeekAverageMet = weekMETSum / size;
//
//                Log.d(TAG, " 평균 고강도 시간 : " + averageWeekHighTime);
//                Log.d(TAG, " 평균 중간도 시간 : " + averageWeekMediumTime);
//                Log.d(TAG, " 평균 저강도 시간 : " + averageWeekLowTime);
//                Log.d(TAG, " 평균 저강도 미만 시간 : " + averageWeekUnderLowTime);
//                Log.d(TAG, " 평균 칼로리 : " + averageWeekCalorie);
//                Log.d(TAG, " 평균 MET : " + averageWeekAverageMet);
//                Log.d(TAG, " 점수 : " + weekAchieve);
//
//                this.mDBManager.updateWeekActivity(weekStartDate, (averageWeekCalorie), weekAchieve, averageWeekHighTime, averageWeekMediumTime, averageWeekLowTime, averageWeekUnderLowTime, averageWeekAverageMet, this.mDBManager.getAppliedProgram().getId());
//
//                //주 시작일을 가져온다.
//                weekCalendar.setTimeInMillis(dayActivity.getStartTime().getMilliSecond());
//                dayOfWeek = weekCalendar.get(Calendar.DAY_OF_WEEK);
//                weekCalendar.add(Calendar.DATE, -(dayOfWeek - 1));
//                weekStartDate = this.getDateString(weekCalendar);
//                Log.d(TAG, "주 시작일 : " + weekStartDate);
//
//                //초기화
//                weekMediumTimeSum = 0;
//                weekHighTimeSum = 0;
//                weekLowTimeSum = 0;
//                weekUnderLowTimeSum = 0;
//                weekCalorieSum = 0;
//                weekMETSum = 0;
//                predayScore = 0;
//                scoreCalorieList.clear();
//                calorieList.clear();
//                weekActivityList.clear();
//                weekContinuousExerciseList.clear();
//                weekExerciseList.clear();
//            } // End if
//
//            ExerciseAnalyzer exerciseAnalyzer = new ExerciseAnalyzer(appliedExerciseProgram, userInfoForAnalyzer, WearingLocation.WRIST, dayActivity.getDataSaveInterval());
//
//            //주간 고강도 시간의 합
//            weekHighTimeSum += dayActivity.getStrengthHigh();
//            //주간 중강도 시간의 합
//            weekMediumTimeSum += dayActivity.getStrengthMedium();
//            //주간 저강도 시간의 합
//            weekLowTimeSum += dayActivity.getStrengthMedium();
//            //주간 초저강도 시간의 합
//            weekUnderLowTimeSum += dayActivity.getStrengthUnderLow();
//
//            weekMETSum += dayActivity.getAverageMET();
//
//            //강도의 종류가 CalorieSum용 계산할 때 사용함.
//            int bmr = exerciseAnalyzer.getUserInfo().getBMR();
//            int extraWeekCalorieSum = 0;
//            int preWeekCalorieSum = weekCalorieSum;
//
//            if (appliedExerciseProgram.getStrengthInputType().equals(StrengthInputType.CALORIE)) {
//                extraWeekCalorieSum = (int) (weekCalorieSum + appliedExerciseProgram.getStrength_From());
//            } else if (appliedExerciseProgram.getStrengthInputType().equals(StrengthInputType.VS_BMR)) {
//                extraWeekCalorieSum = (int) (weekCalorieSum + (bmr * (appliedExerciseProgram.getStrength_From() / 100)));
//            }
//            weekCalorieSum += dayActivity.getCalorieByActivity();
//
//            List<Integer> preCalorieList = new ArrayList<Integer>();
//            preCalorieList.addAll(calorieList);
//
//            //강도의 종류가 Calorie용 계산할 때 사용함.
//
//            List<Integer> extraScoreList = new ArrayList<Integer>();
//
//            Calendar c = Calendar.getInstance();
//            int day_of_week = c.get(Calendar.DAY_OF_WEEK);
//
//            //이미 지나간 빈 날자 만큼 0으로 채운다. 이번주에 추가로 획득할 수 있는 최대 점수 계산에서 반드시 필요
//            if (scoreCalorieList.size() < day_of_week) {
//                int addNum = dayOfWeek - scoreCalorieList.size() - 1;
//                for (int daysNum = 0; daysNum < addNum; daysNum++) {
//                    scoreCalorieList.add(0, 0);
//                    List<Integer> tempList = new ArrayList<>();
//                    tempList.add(0);
//                    weekContinuousExerciseList.add(0, tempList);
//                    weekExerciseList.add(0, 0);
//                }
//            }
//            calorieList.add(dayActivity.getCalorieByActivity());
//
//
//            for (int j = 0; j < scoreCalorieList.size(); j++) {
//                extraScoreList.add(scoreCalorieList.get(j));
//            }
//
//            scoreCalorieList.add(dayActivity.getCalorieByActivity());
//            if (appliedExerciseProgram.getStrengthInputType().equals(StrengthInputType.CALORIE)) {
//                extraScoreList.add((int) appliedExerciseProgram.getStrength_From());
//            } else if (appliedExerciseProgram.getStrengthInputType().equals(StrengthInputType.VS_BMR)) {
//                extraScoreList.add((int) (bmr * (appliedExerciseProgram.getStrength_From() / 100)));
//            }
//
//            List<List<Integer>> preWeekContinuousExerciseList = new ArrayList<List<Integer>>();
//            preWeekContinuousExerciseList.addAll(weekContinuousExerciseList);
//            //강도의 종류가 칼로리와 칼로리합, vs_bmr이 아닐때.
//            List<List<Integer>> extraWeekContinuousExerciseList = new ArrayList<List<Integer>>();
//            for (int k = 0; k < weekContinuousExerciseList.size(); k++) {
//                extraWeekContinuousExerciseList.add(weekContinuousExerciseList.get(k));
//            }
//
//            List<Integer> extraAchieveOfTageList = new ArrayList<Integer>();
//            extraAchieveOfTageList.add(appliedExerciseProgram.getMinutes_From());
//            extraWeekContinuousExerciseList.add(extraAchieveOfTageList);
//
//            List<Integer> achieveOfTageList = new ArrayList<Integer>();
//            achieveOfTageList.add(dayActivity.getAchieveOfTarget());
//            weekContinuousExerciseList.add(achieveOfTageList);
//            weekExerciseList.add(dayActivity.getAchieveOfTarget());
//
//            //점수를 계산한다.
//            int weekAchieve = 0;
//            int todayAchieve = 0;
//            boolean isAchieveMax = false;
//            int possibleMaxScore = 0;
//            int possibleTimes = 0;
//            int possibleValue = 0;
//
//            if (appliedExerciseProgram.getStrengthInputType().equals(StrengthInputType.CALORIE)) {
//                predayScore = exerciseAnalyzer.CalculateWeekAchievement_Calorie(preCalorieList);
//                weekAchieve = exerciseAnalyzer.CalculateWeekAchievement_Calorie(scoreCalorieList);
//                todayAchieve = exerciseAnalyzer.CalculateWeekAchievement_Calorie(extraScoreList);
//                MoreExerciseForMaxScore_StrengthType moreStrengthType = exerciseAnalyzer.getNeedMoreExercise_Calorie(scoreCalorieList);
//
//                isAchieveMax = moreStrengthType.isAchieveMax();
//                possibleMaxScore = moreStrengthType.getPossibleMaxScore();
//                possibleTimes = moreStrengthType.getDays();
//                possibleValue = moreStrengthType.getRangeFrom();
//
//            } else if (appliedExerciseProgram.getStrengthInputType().equals(StrengthInputType.VS_BMR)) {
//                predayScore = exerciseAnalyzer.CalculateWeekAchievement_VS_BMR(preCalorieList);
//                weekAchieve = exerciseAnalyzer.CalculateWeekAchievement_VS_BMR(scoreCalorieList);
//                todayAchieve = exerciseAnalyzer.CalculateWeekAchievement_VS_BMR(extraScoreList);
//                MoreExerciseForMaxScore_StrengthType moreStrengthType = exerciseAnalyzer.getNeedMoreExercise_VS_BMR(scoreCalorieList);
//
//                isAchieveMax = moreStrengthType.isAchieveMax();
//                possibleMaxScore = moreStrengthType.getPossibleMaxScore();
//                possibleTimes = moreStrengthType.getDays();
//                possibleValue = moreStrengthType.getRangeFrom();
//            } else if (appliedExerciseProgram.getStrengthInputType().equals(StrengthInputType.CALORIE_SUM)) {
//                predayScore = exerciseAnalyzer.CalculateWeekAchievement_CalorieSum(preWeekCalorieSum);
//                weekAchieve = exerciseAnalyzer.CalculateWeekAchievement_CalorieSum(weekCalorieSum);
//                todayAchieve = exerciseAnalyzer.CalculateWeekAchievement_CalorieSum(extraWeekCalorieSum);
//                MoreExeriseForMaxSocre_CalorieSum moreCalorieSum = exerciseAnalyzer.getNeedMoreExercise(weekCalorieSum);
//
//                isAchieveMax = moreCalorieSum.isAchieveMax();
//                possibleMaxScore = moreCalorieSum.getPossibleMaxScore();
//                possibleTimes = 0;
//                possibleValue = moreCalorieSum.getNeedMoreCalorie();
//
//            } else {
//                predayScore = exerciseAnalyzer.CalcuateWeekAchieveByMinutesList(policy, preWeekContinuousExerciseList);
//                weekAchieve = exerciseAnalyzer.CalcuateWeekAchieveByMinutesList(policy, weekContinuousExerciseList);
//                todayAchieve = exerciseAnalyzer.CalcuateWeekAchieveByMinutesList(policy, extraWeekContinuousExerciseList);
//
//                MoreExerciseForMaxScore_StrengthType moreStrengthType = exerciseAnalyzer.getNeedMoreExercise(ContinuousCheckPolicy.Loosely_SumOverXExercise, weekExerciseList);
//
//                isAchieveMax = moreStrengthType.isAchieveMax();
//                possibleMaxScore = moreStrengthType.getPossibleMaxScore();
//                possibleTimes = moreStrengthType.getDays();
//                possibleValue = moreStrengthType.getRangeFrom();
//            }
//
//            int todayScore = weekAchieve - predayScore;
//            int extraScore = todayAchieve - weekAchieve;
//
//            if (ismergied) {
//                this.mDBManager.updateDayActivity_Mergy(
//                        dayActivity.getActivityDate(),
//                        (dayActivity.getCalorieByActivity()),
//                        dayActivity.getStrengthUnderLow(),
//                        dayActivity.getStrengthLow(),
//                        dayActivity.getStrengthMedium(),
//                        dayActivity.getStrengthHigh(),
//                        dayActivity.getMetArray(),
//                        dayActivity.getAchieveOfTarget(),
//                        dayActivity.getAverageMET(),
//                        dayActivity.getStartTime().getTimeString(),
//                        dayActivity.getDataSaveInterval(),
//                        predayScore,
//                        todayScore,
//                        extraScore,
//                        isAchieveMax,
//                        possibleMaxScore,
//                        possibleTimes,
//                        possibleValue,
//                        policy
//                );
//            } else {
//                this.mDBManager.updateDayActivity(
//                        dayActivity.getActivityDate(),
//                        (dayActivity.getCalorieByActivity()),
//                        dayActivity.getStrengthUnderLow(),
//                        dayActivity.getStrengthLow(),
//                        dayActivity.getStrengthMedium(),
//                        dayActivity.getStrengthHigh(),
//                        dayActivity.getMetArray(),
//                        dayActivity.getAchieveOfTarget(),
//                        dayActivity.getAverageMET(),
//                        dayActivity.getStartTime().getTimeString(),
//                        dayActivity.getDataSaveInterval(),
//                        predayScore,
//                        todayScore,
//                        extraScore,
//                        isAchieveMax,
//                        possibleMaxScore,
//                        possibleTimes,
//                        possibleValue,
//                        policy
//                );
//            }
//        }
//    }
