package com.fitdotlife.fitmate_lib.service.protocol;

public class FileOpenException extends Exception
{
	
	private static final long serialVersionUID = 1L;
	private String message;
	
	public FileOpenException( String message )
	{
		this.message = message;
	}

	@Override
	public String getMessage() {

		return this.message;
	}
	
	
}
