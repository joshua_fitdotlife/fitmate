package com.fitdotlife.fitmate_lib.service.protocol;

import android.content.Context;
import android.util.Log;

import com.fitdotlife.fitmate_lib.service.protocol.object.SerialCode;
import com.fitdotlife.fitmate_lib.service.protocol.object.SystemInfo_Request;
import com.fitdotlife.fitmate_lib.service.protocol.object.SystemInfo_Response;
import com.fitdotlife.fitmate_lib.service.protocol.object.TimeInfo;
import com.fitdotlife.fitmate_lib.service.protocol.object.UserInfo;
import com.fitdotlife.fitmate_lib.service.protocol.type.HeaderType;
import com.fitdotlife.fitmate_lib.service.protocol.type.VersionType;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Date;

/**
 * @author Joshua
 *
 */
public class ProtocolManager
{
	private final String TAG = "fitmateservice - " + ProtocolManager.class.getSimpleName();
	private final int RECEIVE_LENGTH = 64;
	private CommunicationInterface communicationInterface;
	private DataReceiver dataReceiver;
	
	private Context mContext;
	
	public ProtocolManager(Context context, CommunicationInterface communicationInterface)
	{
		this.mContext = context;
		this.communicationInterface = communicationInterface;
		this.dataReceiver = new DataReceiver(communicationInterface);
	}
	
	public boolean setSystemInfo( SystemInfo_Request systemInfo ) throws FileReadException
	{
		byte[] arrSystemInfo = new byte[ SystemInfo_Request.LENGTH + 1 ];
		int position = 0;
		arrSystemInfo[ position ] = HeaderType.REQ_SET_SYSTEMINFO;
		position += 1;
		System.arraycopy(systemInfo.getBytes(), 0, arrSystemInfo, position, SystemInfo_Request.LENGTH);
		sendData( arrSystemInfo );
		
		return readResponse(HeaderType.RES_SUCCESS);
	}
	
	public SystemInfo_Response getSystemInfo() throws FileReadException {
		SystemInfo_Response systemInfo = null;

		sendOnlyHeader(HeaderType.REQ_GET_SYSTEMINFO);
		
		byte[] responseData = getResponseBytes(HeaderType.RES_SYSTEMINFO);
		
		if(responseData != null)
		{
			systemInfo =  new SystemInfo_Response(responseData);
		}
		
		return systemInfo;
	}
	
	public boolean setCurrentTime( ) throws FileReadException {
		TimeInfo timeInfo = new TimeInfo();
		timeInfo.setTime(new Date());
		return this.setCurrentTime(timeInfo);
	}

	
	public TimeInfo getCurrentTime() throws FileReadException {
		TimeInfo timeInfo = null;
		
		sendOnlyHeader(HeaderType.REQ_GET_TIMEINFO);
		
		byte[] responseData = getResponseBytes( HeaderType.RES_TIMEINFO );
		
		if(responseData != null)
		{
			timeInfo =  new TimeInfo(responseData);
		}
		
		return timeInfo;
	}
	
	public int getBatteryInfo() throws FileReadException {
		int batteryInfo = -1;

		sendOnlyHeader( HeaderType.REQ_GET_BATTERY );
		
		byte[] responseData = getResponseBytes(HeaderType.RES_BATTERY );

		if(responseData != null)
		{
			batteryInfo =  getBatteryChargeStatus(responseData[0]+ responseData[1]*0.01f);
		}
		
		return batteryInfo;
	}
	private int getBatteryChargeStatus(float voltage)
	{
		float fullCharge = 4.2f;
		float percent1= 3.4f;


		if(voltage>=fullCharge) {
			return 100;
		} else if(voltage<=3.4) {
			return 1;
		}else {

			float per_diff = 100 / (fullCharge - percent1);
			int remain = (int) Math.round(per_diff * (voltage - percent1));

			if (remain == 0) return 1;
			return remain;
		}
	}


	
//	public void setSerialCode( SerialCode serialCode )
//	{
//		this.commonProtocolManager.setSerialCode(serialCode);
//	} 
	
//	public ByteBuffer[] getAllDataFile() throws FileReadException, FileOpenException, FileCloseException
//	{
//		int numberOfFile = this.getNumberOfFile();
//		Log.i("GTele", numberOfFile + "" );
//		
//		ByteBuffer[] arrDataFile = new ByteBuffer[ numberOfFile ];
//		
//		// 파일 Index는 0부터 시작한다.
//		for( int index = 0 ; index < numberOfFile ; index++  )
//		{
//			ByteBuffer dataFile = this.getDataFile( index );
//			arrDataFile[ index ] = dataFile;
//		}
//		return arrDataFile;
//	}
//	
//	public ByteBuffer getDataFile(int index) throws FileReadException, FileOpenException, FileCloseException
//	{
//		int fileSize = this.getFileInfo(index);
//		Log.i("GTele",index + " , " + fileSize);
//		return this.getDataFile(index, fileSize);
//	}
	
	/**
	 * Fitmeter 기기를 포맷한다.
	 * @return
	 */
	private boolean format() throws FileReadException {
		this.sendOnlyHeader( HeaderType.REQ_FORMAT );
		return this.readResponse(HeaderType.RES_FORMAT_STATUS);
	}


//	public ByteBuffer getDataFile( int fileIndex , int fileSize ) throws FileReadException, FileOpenException, FileCloseException
//	{
//		//파일을 연다.
//		this.openFile( fileIndex );
//
//		ByteBuffer bos = ByteBuffer.allocate( fileSize );
//
//		//파일 가져온다.
//		this.commonProtocolManager.sendOnlyHeader( HeaderType.REQ_FILE_READ );
//		byte[] arrReadData = this.commonProtocolManager.readDataResponseBytes( HeaderType.RES_FILE_READ , fileSize );
//		if( arrReadData[2]  == -1 ) throw new FileReadException(" 수신된 바이트 배열의 첫번째 바이트가 실패(-1) 를 반환하였습니다. [ File Index = " + fileIndex + "]" );
//
//		bos.put( arrReadData );
//
//		//파일을 닫는다.
//		this.closeFile();
//
//		return bos;
//	}


//	public byte[] getDataFile( int fileIndex , int fileSize , int index ) throws FileReadException, FileOpenException, FileCloseException
//	{
//		//파일을 연다.
//		this.openFile( fileIndex );
//
//		//파일 가져온다.
//		//this.commonProtocolManager.sendOnlyHeader( HeaderType.REQ_FILE_READ );
//		byte[] arrLength = NewHomeUtils.IntToByteArray(index);
//		byte[] arrSendData = new byte[ ]{ HeaderType.REQ_FILE_READ, arrLength[0] , arrLength[1] , arrLength[2] , arrLength[3] };
//		this.sendData( arrSendData );
//
//		byte[] arrReadData = this.readDataResponseBytes( HeaderType.RES_FILE_READ , fileSize );
//
//		//파일을 닫는다.
//		this.closeFile();
//
////		if( arrReadData.length < fileSize )
////		{
////			try {
////				this.communicationInterface.close();
////			} catch (IOException e) { }
////		}
//
//		return arrReadData;
//	}

    public byte[] getDataFile( int fileIndex , int fileSize , int fileOffset ) throws FileReadException, FileOpenException, FileCloseException
    {

        //파일을 연다.
        this.openFile( fileIndex );

        ByteBuffer bos = ByteBuffer.allocate( fileSize - fileOffset );

        while( fileOffset < fileSize ) {

            int readLength = fileSize - fileOffset;
            readLength = Math.min( readLength, 0x200 );

            byte[] arrOffset = ProtocolUtils.IntToByteArray(fileOffset);
            byte[] arrLength = ProtocolUtils.shortToByteArray(readLength);

            byte[] arrSendData = new byte[]{HeaderType.REQ_GET_MASS_DATA, arrOffset[0], arrOffset[1], arrOffset[2], arrOffset[3], arrLength[0], arrLength[1]};
            this.sendData(arrSendData);

            byte[] arrReadData = this.readDataResponseBytes( readLength + 2 );
            bos.put(arrReadData);

            fileOffset += readLength;
        }

        //파일을 닫는다.
        this.closeFile();

        return bos.array();
    }

    public byte[] readCurrentFileData( int fileIndex , int fileOffset , int readLength ) throws FileReadException {

        byte[] arrFileIndex = ProtocolUtils.IntToByteArray( fileIndex );
        byte[] arrOffset = ProtocolUtils.IntToByteArray(fileOffset);
        byte[] arrLength = ProtocolUtils.shortToByteArray(readLength);

        byte[] arrSendData = new byte[]{ HeaderType.REQ_FILE_READ, arrFileIndex[0], arrFileIndex[1] , arrFileIndex[2] , arrFileIndex[3] , arrOffset[0], arrOffset[1], arrOffset[2], arrOffset[3], arrLength[0], arrLength[1]};
        this.sendData(arrSendData);

        return this.readDataResponseBytes(readLength);
    }


    private byte[] readDataResponseBytes(int readLength) throws FileReadException {
        byte[] responseData;

        try {

            responseData = dataReceiver.readData(readLength);

        } catch (IOException e) {
            return null;
        }

        if(responseData == null)
        {
            return null;
        }

        return responseData;
    }


    /**
	 * 파일의 갯수를 요청한다.
	 * @return
	 */
	public int[] getNumberOfFile() throws FileReadException {
        this.sendOnlyHeader( HeaderType.REQ_NUMBER_OF_FILE );

		byte[] arrNumberOfFile = this.getResponseBytes( HeaderType.RES_NUMBER_OF_FILE );

        int startIndex = ProtocolUtils.ByteArrayToInt(new byte[]{ arrNumberOfFile[0] ,arrNumberOfFile[1] ,arrNumberOfFile[2] ,arrNumberOfFile[3] });
        int endIndex = ProtocolUtils.ByteArrayToInt( new byte[]{ arrNumberOfFile[4] ,arrNumberOfFile[5] ,arrNumberOfFile[6] ,arrNumberOfFile[7] });

		return new int[]{ startIndex , endIndex };
	}

	public int getFileInfo( int index ) throws FileReadException {
        Log.d(TAG, "파일 정보 송신");
        byte[] arrByteIndex = ProtocolUtils.IntToByteArray(index);
        byte[] arrFileInfo = this.makeMessage(HeaderType.REQ_FILE_INFO, arrByteIndex);
		this.sendData(arrFileInfo);

        Log.d(TAG , "파일 정보 수신");
		byte[] arrFileSize = this.getResponseBytes(HeaderType.RES_FILE_INFO);
		return ProtocolUtils.ByteArrayToInt(arrFileSize);
	}

	public boolean openFile( int index ) throws FileOpenException, FileReadException {
        byte[] arrByteIndex = ProtocolUtils.shortToByteArray(index);
        byte[] arrOpenFile = this.makeMessage(HeaderType.REQ_FILE_OPEN , arrByteIndex);

		this.sendData(arrOpenFile);
		byte[] arrResponseData = this.getResponseBytes( HeaderType.RES_FILE_OPEN );


		if( arrResponseData[0]  == 0 )
		{
			return true;
		}
		else if( arrResponseData[0]  == -1 )
		{
			throw new FileOpenException("파일을 여는데 실패하였습니다.. 코드 = " + arrResponseData[0]);
		}
		else
		{
			throw new FileOpenException("반환코드가 잘못되었습니다. 코드 = " + arrResponseData[0]);
		}
	}

	/**
	 * 휘트미터 기기의 파일을 닫습니다.
	 *
	 * @return
	 */
	public boolean closeFile( ) throws FileCloseException, FileReadException {
		this.sendOnlyHeader(HeaderType.REQ_FILE_CLOSE);
		byte[] arrResponseData = this.getResponseBytes( HeaderType.RES_FILE_CLOSE );

		if( arrResponseData[0]  == 0 )
		{
			return true;
		}
		else if( arrResponseData[0]  == -1 )
		{
			throw new FileCloseException("파일을 닫는데 실패하였습니다.. 코드 = " + arrResponseData[0]);
		}
		else
		{
			throw new FileCloseException("반환코드가 잘 못되었습니다. 코드 = " + arrResponseData[0]);
		}
	}

	public boolean removeAllData() throws FileReadException {
		byte[] arrDeleteRequest = new byte[]{ HeaderType.REQ_DELETE_ALL_FILE};
		this.sendData(arrDeleteRequest);

		return this.readResponse( HeaderType.RES_SUCCESS );
	}


	public boolean setAcceleroCorrectionValue()
	{
		return true;
		//byte[] arrAcceleroCorrection =  new byte[]{HeaderType.REQ_SET_ACCELERO_CORRECTION , AcceleroRangeType.GRAVITY_2 };
		//this.sendData(arrAcceleroCorrection);
		//return this.readResponse(HeaderType.RES_SUCCESS);
	}

	public boolean setIlluminanceCorrectionValue(byte correctionValue1, byte correctionValue2) throws FileReadException {
		byte[] arrIlluminanceCorrectionCorrection = new byte[]{HeaderType.REQ_SET_ILLUMINACE_CORRECTION , correctionValue1, correctionValue2};
		sendData(arrIlluminanceCorrectionCorrection);
		return readResponse(HeaderType.RES_SUCCESS);
	}

	public boolean setUserInfo( UserInfo userInfo ) throws FileReadException {
		sendData( this.makeMessage(HeaderType.REQ_SET_USERINFO, userInfo.getBytes() ) );
		return readResponse(HeaderType.RES_SUCCESS);
	}

	public UserInfo getUserInfo() throws FileReadException {
		UserInfo userInfo = null;

		sendOnlyHeader(HeaderType.REQ_GET_USERINFO);

		byte[] responseData = getResponseBytes( HeaderType.RES_USERINFO );

		if(responseData != null)
		{
			userInfo =   new UserInfo(responseData);
		}

		return userInfo;
	}

	public void onLED() throws FileReadException {
		sendOnlyHeader( HeaderType.REQ_ON_LED);

		readResponse(HeaderType.RES_SUCCESS);
	}

	public void offLED() throws FileReadException {

		sendOnlyHeader( HeaderType.REQ_OFF_LED);
		readResponse(HeaderType.RES_SUCCESS);
	}


	public boolean startBTDataSync() throws FileReadException {
		sendOnlyHeader(HeaderType.REQ_START_DATASYNC);

		return readResponse(HeaderType.RES_SUCCESS);
	}

	public boolean endBTDataSync() throws FileReadException {
		sendOnlyHeader(HeaderType.REQ_END_DATASYNC);

		return readResponse(HeaderType.RES_SUCCESS);
	}

	public int dataFormat() throws FileReadException
	{
		int formatStatus = -1;

		sendOnlyHeader( HeaderType.REQ_FORMAT );

		byte[] responseData = getResponseBytes( HeaderType.RES_FORMAT_STATUS  );

		if(responseData != null)
		{
			formatStatus =  responseData[0];
		}

		return formatStatus;
	}

	public boolean deleteAllFile() throws FileReadException {
		sendOnlyHeader( HeaderType.REQ_DELETE_ALL_FILE );

		return readResponse( HeaderType.RES_SUCCESS );
	}

	public int byteArrayToInt(byte[] source)
	{
		int result = 0;

		for(int i = source.length - 1 ; i > -1 ; i--)
		{
			 result = ( result << ( i * 8 ) ) & source[ i ];
		}

		return result;
	}

	/**
	 * 펌웨어 버전을 확인한다.
	 * @return
	 */
	public VersionType checkVersion() throws FileReadException
	{
		VersionType vType = VersionType.Version_2;
		this.sendOnlyHeader(HeaderType.REQ_GET_SYSTEMINFO);

		byte[] responseData = this.getResponseBytes(HeaderType.RES_SYSTEMINFO);

		if(responseData != null)
		{
			if( responseData.length == 22 )
			{
				vType = VersionType.Version_1;
			}
		}
		else
		{
			vType = VersionType.Version_no;
		}

		return vType;
	}

	/**
	 * 현재 시간을 설정한다.
	 * @return
	 */
	public boolean setCurrentTime( TimeInfo timeInfo ) throws FileReadException {
		this.sendData( this.makeMessage( HeaderType.REQ_SET_TIMEINFO , timeInfo.getBytes()) );
		return this.readResponse(HeaderType.RES_SUCCESS);
	}

	/**
	 * 싱크를 확인한다.
	 * @return
	 */
	public boolean checkSync() throws FileReadException
	{
		this.sendOnlyHeader( HeaderType.SYNC );

		return this.readResponse( HeaderType.RES_SUCCESS );
	}

	/**
	 * 시리얼 코드를 설정한다.
	 * @param serialCode
	 * @retur
	 */
	public boolean setSerialCode( SerialCode serialCode ) throws FileReadException {
		this.sendData( serialCode.getBytes() );
		return this.readResponse(HeaderType.RES_SUCCESS);
	}
	
	/**
	 * 헤더와 바디를 합친다.
	 * @param header
	 * @param body
	 * @return
	 */
	public byte[] makeMessage( byte header , byte[] body )
	{
		byte[] message = new byte[ body.length + 1];
		message[0] = header;
		System.arraycopy(body, 0, message, 1, body.length);
		
		return message;
	}
	
	//##########################################//
	//############### 수신 전송 함수 ###############//
	//##########################################//
	
	/**
	 * 하나의 헤더만 전송한다.
	 * @param header
	 */
	public void sendOnlyHeader(byte header)
	{
		this.sendData(new byte[]{header});
	}

	/**
	 * 바이트 배열을 전송한다.
	 * @param arrData
	 * @return
	 */
	public boolean sendData(byte[] arrData)
	{
		byte[] sendBytes = Encoder.encoding(arrData);
		
		try {
			communicationInterface.send( sendBytes );
		} catch (IOException e) {
			return false; 
		}
		
		return true;
	}
	
	public boolean readResponse(byte responseHeader ) throws FileReadException {
		byte[] responseData;
		try {
			responseData = dataReceiver.read(  );
		} catch (IOException e) {
			return false;
		}
	
		if(responseData == null)
		{
			return false;
		}
		
		if(responseData[0] != responseHeader)
		{
			return false;
		}
		
		return true;
	} 
	
	public byte[] getResponseBytes( byte responseHeader ) throws FileReadException {
		//헤더를 떼어 낸다.
		byte[] arrResponseWithHeader = readResponseBytes( responseHeader );

        if( arrResponseWithHeader == null ){
            throw new FileReadException( "받은 데이터가 NULL입니다." );
        }

		byte[] arrResponseData = new byte[ arrResponseWithHeader.length-1 ];
		System.arraycopy(arrResponseWithHeader, 1, arrResponseData, 0, arrResponseData.length);
		
		return arrResponseData;
	}
	
	
	
//	public byte[] readResponseBytes( int readLength ) throws FileReadException
//	{
//		//리턴되는 메소드에서 판단할 수 있도록 헤더를 떼어내지 않는다.
//		return readResponseBytes( (byte) 100 , readLength );
//	}
	
	public byte[] readDataResponseBytes( byte responseHeader , int length ) throws FileReadException
	{
		byte[] responseData = null;
		
		try
		{
			responseData = dataReceiver.readData( length );
			
		}catch( IOException e )
		{
			return null;
		}
		
		return responseData;
	}
	
	public byte[] readResponseBytes(byte responseHeader ) throws FileReadException {
		byte[] responseData;
		
		try {
			
			responseData = dataReceiver.read( );
			
		} catch (IOException e) {
            throw new FileReadException( e.getMessage());
		}
	
		if(responseData == null)
		{
            throw new FileReadException( "Response Data 가 없습니다." );
		}
		
		if(responseHeader !=  100 )
		{
			if(responseData[0] != responseHeader)
			{
                throw new FileReadException( "헤더가 다릅니다." );
			}
		}
		
		return responseData;
	}
}
