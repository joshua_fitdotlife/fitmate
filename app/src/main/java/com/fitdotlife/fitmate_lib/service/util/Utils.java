package com.fitdotlife.fitmate_lib.service.util;

import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ContinuousCheckPolicy;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseAnalyzer;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.FatAndCarbonhydrateConsumtion;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.USERVO2MAXLEVEL;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.UserInfoForAnalyzer;
import com.fitdotlife.fitmate_lib.object.DayActivity;
import com.fitdotlife.fitmate_lib.object.UserInfo;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Utils 
{
	private static float[] METArrayToOtherSavingInterval(float[] sourceMETArray, int mul)
	{
		float[] resultMETArray = new float[sourceMETArray.length/mul];
		float metsum=0;
		float counterOver1MET=0;

		int countIndex=0;


		for(int i=0;i<sourceMETArray.length;i++)
		{
			countIndex++;
			if(sourceMETArray[i]>=0){
				counterOver1MET++;
				metsum+=sourceMETArray[i];
			}
			if(countIndex== mul){
				resultMETArray[i/mul]= metsum/counterOver1MET;
				counterOver1MET=0;
				metsum=0;
				countIndex=0;
			}
		}
		return resultMETArray;
	}

	/**
	 * 두 일일 데이터를 합쳐서 리턴한다. master가 우선이 되는 데이터, slaver가 우선순위가 떨어지는 데이터이다. 우선순위가 높은 데이터를 최대한 유지한다. master로 합쳐서 리턴한다.
	 * @param inDB 로컬에서 만들어진 데이터
	 * @param fromDevice 서버에서 만들어진 데이터
	 * @return
	 */
	public static DayActivity mergyDayActivity(DayActivity inDB, DayActivity fromDevice, ExerciseAnalyzer analyzer){

		int datasavinginterval= inDB.getDataSaveInterval();
		if(datasavinginterval < fromDevice.getDataSaveInterval()) datasavinginterval = fromDevice.getDataSaveInterval();

		float[] inDBmastermetArry = inDB.getMetArray();
		float[] fromDevicemetArray = fromDevice.getMetArray();
		if(inDB.getDataSaveInterval()!= datasavinginterval){
			inDBmastermetArry = METArrayToOtherSavingInterval(inDB.getMetArray(), datasavinginterval/inDB.getDataSaveInterval());
		}
		if(fromDevice.getDataSaveInterval()!=datasavinginterval){
			fromDevicemetArray= METArrayToOtherSavingInterval(fromDevice.getMetArray(), datasavinginterval/fromDevice.getDataSaveInterval());
		}

		int calsum=0;

		//
		int devicefristIndex=0;

		for(int i=0;i<fromDevicemetArray.length;i++){
			if(fromDevicemetArray[i]>=1){
				devicefristIndex=i;
				break;
			}
		}
		for(int i=0;i<devicefristIndex;i++){
			fromDevicemetArray[i] = inDBmastermetArry[i];
			calsum += analyzer.Calculate_CalorieFromMET(fromDevicemetArray[i]);
		}
		calsum += 1000*fromDevice.getCalorieByActivity();

		int[] strength= analyzer.MinutesforEachStrength(fromDevicemetArray);
		inDB.setAverageMET(analyzer.CalculateAverageMET(fromDevicemetArray));
		inDB.setHmlStrengthTime(strength);
		inDB.setCalorieByActivity((int) (Math.round(calsum / 1000.0)));
	//	master.setProgramID(master.getProgramID());
		inDB.setStartTime(fromDevice.getStartTime());
		inDB.setDataSaveInterval(datasavinginterval);
		inDB.setMetArray(fromDevicemetArray);

		List<Float> metList= new ArrayList<>();
		for(float f:fromDevicemetArray){
			metList.add(f);
		}

		//지방과 탄수화물 거리를 계산한다.
		FatAndCarbonhydrateConsumtion fatAndCarbo = analyzer.getFatandCarbonhydrate( metList , datasavinginterval );
		float fat = fatAndCarbo.getFatBurned();
		float carbohydrate = fatAndCarbo.getCarbonhydrateBurned();
		double distance = ExerciseAnalyzer.getDistanceFromMETArray_over2MET(metList, datasavinginterval);

		inDB.setFat(fat);
		inDB.setCarbohydrate(carbohydrate);
		inDB.setDistance( distance );

		return inDB;
	}

	public static byte[] IntToByteArray(final int source)
	{
		ByteBuffer buffer = ByteBuffer.allocate(Integer.SIZE / 8);
		buffer.putInt(source);
		buffer.order(ByteOrder.BIG_ENDIAN );
		return buffer.array();
	}
	
	public static int ByteArrayToInt(byte[] source)
	{
		final int size = Integer.SIZE / 8;
		ByteBuffer buffer = ByteBuffer.allocate(size);
		final byte[] newBytes = new byte[size];
		
		for(int i = 0 ; i < size ; i++)
		{
			if(i + source.length < size)
			{
				newBytes[i] = (byte)0x00;
			}
			else
			{
				newBytes[i] = source[i + source.length -size];
			}
		}
		
		buffer = ByteBuffer.wrap(newBytes);
		buffer.order(ByteOrder.BIG_ENDIAN);
		return buffer.getInt();
	}
	
	public static String getDateString(Date date)
	{	
		return (date.getYear() + 1900 ) + "-" + String.format("%02d", date.getMonth() + 1)+ "-" + String.format("%02d", date.getDate()) ;
	}
	
	public static int dateCompare( String srcDate , String compareDate )
	{
		String sss = srcDate.substring( 8, 10 );
		int srcDay = Integer.parseInt(srcDate.substring(8, 10)) ;
		int compareDay = Integer.parseInt(compareDate.substring(8, 10));
		
		return compareDay - srcDay;
	}
	
	public static Date getDate( Date currentDate )
	{
		Date cDate = null;
		SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat( "yyyy-MM-dd", Locale.getDefault() );
		try {
			cDate =  mSimpleDateFormat.parse( mSimpleDateFormat.format ( currentDate ));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return cDate;
	}
	
	public static String getDateTimeString( Date date)
	{	
		return date.getYear() + "-" + String.format("%02d", date.getMonth() + 1) + "-" + String.format("%02d", date.getDate()) + " " + String.format("%02d", date.getHours()) + ":" + String.format("%02d", date.getMinutes()) + ":" + String.format("%02d", date.getSeconds()) +".000";
	}
	
	public static String getCurrentDate()
	{
		return getDateString( new Date() );
	}
	
	public static Date getDate(String dateString)
	{
		Date date = null;
		SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat( "yyyyMMdd", Locale.getDefault() );
		try 
		{
			date =  mSimpleDateFormat.parse(dateString);
		} 
		catch (ParseException e) {}
		
		return date;
	}

	/**
	 *  kg을 pound로 변환한다.
	 *
	 *  @param kg si단위계인 kg단위(float)
	 *
	 *  @return pound(float)
	 */
	public static float  kg_to_lb( float kg){

	float result = kg/0.45359237f;

	return result;
}

/**
 *  cm를 ft.(피트)단위로 변환한다.
 *
 *  @param cm cm(float)
 *
 *  @return ft.(float)
 */

public static float  cm_to_ft( float cm) {

	float result = cm * 0.0328f;
	return result;
}

/**
 *  pound를 kg으로 변환하여 리턴한다.
 *
 *  @param lb 파운드(float)
 *
 *  @return kg(float)
 */
public static float lb_to_kg(float lb){
	float result = lb*0.45359237f;

	return result;
}

/**
 *  ft.(피트)를 cm단위로 변환하여 리턴한다.
 *
 *  @param ft 길이단위 피트(float)
 *
 *  @return ft.(float)
 */
public static float ft_to_cm(float  ft) {
	float result = ft / 0.0328f;
	return result;
}
	public static UserInfoForAnalyzer getUserInfoForAnalyzer( UserInfo userInfo ){
		if(userInfo.isSI()){
			return new UserInfoForAnalyzer(Utils.calculateAge( userInfo.getBirthDate() ) , userInfo.getGender().getBoolean() , (int)userInfo.getHeight() , (int)userInfo.getWeight() , ContinuousCheckPolicy.Loosely_SumOverXExercise , 10 , USERVO2MAXLEVEL.Fair );
		}
		else{
			int weight = (int)(Utils.lb_to_kg(userInfo.getWeight()));
			int height = (int)(Utils.ft_to_cm(userInfo.getHeight()));
			return new UserInfoForAnalyzer(Utils.calculateAge( userInfo.getBirthDate() ) , userInfo.getGender().getBoolean() ,height , weight , ContinuousCheckPolicy.Loosely_SumOverXExercise , 10 , USERVO2MAXLEVEL.Fair );
		}
	}

	public static UserInfoForAnalyzer getUserInfoForAnalyzer( String birthdate , boolean gender , float userHeight , float userWeight , boolean isSi)
	{
		if(isSi){
			return new UserInfoForAnalyzer(Utils.calculateAge( birthdate ) , gender , (int)userHeight , (int)userWeight , ContinuousCheckPolicy.Loosely_SumOverXExercise , 10 , USERVO2MAXLEVEL.Fair );
		}
		else{
			int weight = (int)(Utils.lb_to_kg( userWeight ));
			int height = (int)(Utils.ft_to_cm( userHeight ));
			return new UserInfoForAnalyzer(Utils.calculateAge( birthdate ) , gender ,height , weight , ContinuousCheckPolicy.Loosely_SumOverXExercise , 10 , USERVO2MAXLEVEL.Fair );
		}
	}

	
	public static Date getDateTime( String dateString )
	{
		Date cDate = null;
		SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss", Locale.getDefault() );
		try {
			cDate =  mSimpleDateFormat.parse( dateString );
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return cDate;
	}

	public static int calculateAge( long birthDate )
	{
		Date todayDate= new Date();

		Calendar birthCalendar = Calendar.getInstance();
		Calendar todayCalendar = Calendar.getInstance();

		birthCalendar.setTimeInMillis(birthDate);
		todayCalendar.setTime(todayDate);

		int birthYear = birthCalendar.get(Calendar.YEAR);
		int birthMonth = birthCalendar.get(Calendar.MONTH);
		int birthDay = birthCalendar.get(Calendar.DATE);
		int todayYear = todayCalendar.get(Calendar.YEAR);
		int age = 0;

		int diff = todayYear - birthYear;
		if ( birthMonth > todayCalendar.get( Calendar.MONTH ) ||
				( birthMonth == todayCalendar.get(Calendar.MONTH) && birthDay > todayCalendar.get(Calendar.DATE))) {
			diff--;
		}
		return diff;
	}

	/**
	 * yyyy-mm-dd의 문자열로 나이를 계산한다.
	 * @param birthDate
	 * @return
	 */
	public static int calculateAge( String birthDate )
	{
		Date todayDate= new Date();
		Calendar todayCalendar = Calendar.getInstance();
		todayCalendar.setTime(todayDate);

		int birthYear = Integer.parseInt(birthDate.substring(0, 4));
		int birthMonth = Integer.parseInt(birthDate.substring(5, 7));
		int birthDay = Integer.parseInt(birthDate.substring(8, 10));

		int diff = todayCalendar.get(Calendar.YEAR) - birthYear;
		if ( birthMonth > todayCalendar.get( Calendar.MONTH ) ||
				( birthMonth == todayCalendar.get(Calendar.MONTH) && birthDay > todayCalendar.get(Calendar.DATE))) {
			diff--;
		}
		return diff;

	}
}

