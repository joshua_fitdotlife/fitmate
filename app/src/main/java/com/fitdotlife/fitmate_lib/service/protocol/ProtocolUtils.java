package com.fitdotlife.fitmate_lib.service.protocol;

import com.fitdotlife.fitmate_lib.service.protocol.type.IntervalType;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class ProtocolUtils {

    public static int calculateBatteryRatio(byte natural, byte fraction ){
       float volt =  natural + fraction * 0.01f;
        return calculateBatteryRatio(volt);
    }

    public static int calculateBatteryRatio(float voltage){
        if (voltage >= 5.0) return 100;

        int[] level = new int[] { 99, 91, 79, 65, 44, 15, 8, 5, 4, 3, 2, 1 };

        float asc = 0.1f;
        float cutOff = 3.0f;
        float max = 4.1f;


        if (voltage > max) return level[0];
        if (voltage <= cutOff) return 1;

        float fl = max;
        for (int i = 1; i < level.length; i++)
        {
            fl = max - i * asc;

            if (voltage > fl)
            {
                return (int) (Math.round(level[i] + (level[i - 1] - level[i]) * (voltage - fl) / asc));
            }
        }
        return 1;
    }

	public static double getIntervalValue(int interval)
    {
        double result = 0;
        switch (interval)
        {
            case IntervalType.SECOND_1:
                result = 1;
                break;
            case IntervalType.SECOND_5:
                result = 5;
                break;
            case IntervalType.SECOND_10:
                result = 10;
                break;
            case IntervalType.SECOND_30:
                result = 30;
                break;
            case IntervalType.SECOND_60:
                result = 60;
                break;
            case IntervalType.HERTZ_32:
                result = 1 / 32.0; ;
                break;
            default:
			try {
				throw new Exception("unsupported input");
			} catch (Exception e) { }
        }
        return result;
    }
	
	public static int convertToInt( byte[] source )
	{
		int intValue = 0;
		intValue = ( ( source[1] & 0xFF ) <<8 ) + ( source[0] & 0xFF ); 
		return  intValue;
	}
	
	public static byte[] shortToByteArray( int src ){
        byte[] arrByte = new byte[2];

        arrByte[1] = (byte)(src & 0xff);
        arrByte[0] = (byte)((src >> 8) & 0xff);

        return arrByte;
    }

    public static byte[] IntToByteArray(final int source)
    {
        ByteBuffer buffer = ByteBuffer.allocate(Integer.SIZE / 8);
        buffer.putInt(source);
        buffer.order(ByteOrder.BIG_ENDIAN );
        return buffer.array();
    }

    public static int ByteArrayToInt(byte[] source)
    {
        final int size = Integer.SIZE / 8;
        ByteBuffer buffer = ByteBuffer.allocate(size);
        final byte[] newBytes = new byte[size];

        for(int i = 0 ; i < size ; i++)
        {
            if(i + source.length < size)
            {
                newBytes[i] = (byte)0x00;
            }
            else
            {
                newBytes[i] = source[i + source.length -size];
            }
        }

        buffer = ByteBuffer.wrap(newBytes);
        buffer.order(ByteOrder.BIG_ENDIAN);
        return buffer.getInt();
    }

    public static int ByteArrayToInt(byte[] source , ByteOrder byteOrder)
    {
        final int size = Integer.SIZE / 8;
        ByteBuffer buffer = ByteBuffer.allocate(size);
        final byte[] newBytes = new byte[size];

        for(int i = 0 ; i < size ; i++)
        {
            if(i + source.length < size)
            {
                newBytes[i] = (byte)0x00;
            }
            else
            {
                newBytes[i] = source[i + source.length -size];
            }
        }

        buffer = ByteBuffer.wrap(newBytes);
        buffer.order(byteOrder);
        return buffer.getInt();
    }


}
