package com.fitdotlife.fitmate_lib.service.protocol.object;

public class IntervalData 
{
	private byte dataType;
	private double frequency;
	
	/**
	 * 생성자
	 * @param dataType
	 * @param frequency
	 */
	public IntervalData( byte dataType , double frequency )
	{
		this.dataType = dataType;
		this.frequency = frequency;
	}

	public byte getDataType() {
		return dataType;
	}

	public double getFrequency() {
		return frequency;
	}
	
	public void setFrequency(  double frequency ) {
		this.frequency = frequency;
	}
}
