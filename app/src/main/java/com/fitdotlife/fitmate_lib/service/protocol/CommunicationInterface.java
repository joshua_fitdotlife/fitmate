package com.fitdotlife.fitmate_lib.service.protocol;

import java.io.IOException;

public interface CommunicationInterface 
{
    public void send(byte[] message) throws IOException;
	public void send(byte message) throws IOException;
	public byte[] read(int length) throws IOException, FileReadException;
	public byte[] read() throws IOException, FileReadException;
	public byte[] readData(int length) throws IOException, FileReadException;
	public void close() throws IOException;

}
