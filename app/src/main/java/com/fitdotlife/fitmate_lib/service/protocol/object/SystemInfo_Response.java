package com.fitdotlife.fitmate_lib.service.protocol.object;

public class SystemInfo_Response {
	
	public static final int LENGTH = 33;
	private byte[] arrSystemInfo;
	
	/**
	 * 생성자
	 */
	public SystemInfo_Response()
	{
		arrSystemInfo = new byte[LENGTH];
	}
	
	/**
	 * 생성자
	 * @param systemInfo
	 */
	public SystemInfo_Response( byte[] systemInfo )
	{
		this.arrSystemInfo = systemInfo;
	}
	
	/**
	 * 바이트 배열을 반환한다.
	 * @return
	 */
	public byte[] getBytes()
	{
		return this.arrSystemInfo;
	}
	
	/**
	 * 주 버전을 설정한다.
	 */
	public void setMajorVersion( int majorVersion )
	{
		this.arrSystemInfo[0] = (byte)majorVersion;
	}
	/**
	 * 주 버전을 반환한다.
	 * @return
	 */
	public int getMajorVersion()
	{
		return this.arrSystemInfo[0];
	}
	
	/**
	 * 부 버전을 설정한다.
	 */
	public void setMinorVersion( int minorVersion )
	{
		this.arrSystemInfo[1] = (byte)minorVersion;
	}
	/**
	 * 부 버전을 반환한다.
	 * @return
	 */
	public int getMinorVersion()
	{
		return this.arrSystemInfo[1];
	}
	
	/**
	 * 데이터 존재 여부를 설정한다.
	 * @param hasData
	 */
	public void setHasData( int hasData )
	{
		this.arrSystemInfo[2] = (byte)hasData;
	}
	/**
	 * 데이터 존재 여부를 반환한다.
	 * @return
	 */
	public int getHasData()
	{
		return this.arrSystemInfo[2];
	}
	
	/**
	 * 제품 시리얼 코드를 설정한다.
	 * @param serialCode
	 */
	public void setSerialCode( SerialCode serialCode )
	{
		byte[] arrSerialCode = serialCode.getBytes();
		
		for(int i = 0 ; i < arrSerialCode.length ; i++)
		{
			this.arrSystemInfo[ i + 3 ] = arrSerialCode[ i ];
		}
	}
	/**
	 * 제품 시리얼 코드를 반환한다.
	 * @return
	 */
	public SerialCode getSerialCode()
	{
		byte[] arrSerialCode = new byte[SerialCode.Length];
		
		for(int i = 0 ; i < SerialCode.Length ; i++)
		{
			arrSerialCode[ i ] = this.arrSystemInfo[ i + 3 ];
		}
		
		return new SerialCode(arrSerialCode);
	}
	
	/**
	 * 시스템 정보를 설정한다.
	 */
	public void setSystemInfo( SystemInfo_Request systemInfo )
	{
		byte[] arrSystemInfo = systemInfo.getBytes();
		
		for(int i = 0 ; i < arrSystemInfo.length ; i++)
		{
			this.arrSystemInfo[ i + 13 ] = arrSystemInfo[ i ];
		}
	}
	/**
	 * 시스템 정보를 반환한다.
	 * @return
	 */
	public SystemInfo_Request getSystemInfo()
	{
		byte[] arrSystemInfo = new byte[ SystemInfo_Request.LENGTH ];
		
		for(int i = 0 ; i < arrSystemInfo.length - 1; i++)
		{
			arrSystemInfo[ i ] = this.arrSystemInfo[ i + 13 ];
		}
		
		return new SystemInfo_Request( arrSystemInfo );
	}
}
