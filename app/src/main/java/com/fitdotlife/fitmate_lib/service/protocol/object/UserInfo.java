package com.fitdotlife.fitmate_lib.service.protocol.object;


import com.fitdotlife.fitmate_lib.service.util.Utils;

/**
 * 사용자 정보
 * @author Joshua
 *
 */
public class UserInfo 
{	
	private final int defaultSize = 9; 
	//식별자
	private int identifier;
	private int weight;
	private int height;
	private int age;
	private int gender;
	//칼로리 알고리즘
	private byte calorieAlgorithm;
	private String name;
	
	/**
	 * 생성자
	 */
	public UserInfo(){}
	
	/**
	 * 생성자
	 */
	public UserInfo(byte[] arrUserInfo)
	{
		this.identifier 			= Utils.ByteArrayToInt(new byte[]{arrUserInfo[0], arrUserInfo[1], arrUserInfo[2], arrUserInfo[3]});
		this.weight 				= arrUserInfo[4];
		this.height 				= arrUserInfo[5] & 0xFF;
		this.age 					= arrUserInfo[6];
		this.gender 				= arrUserInfo[7];
		this.calorieAlgorithm 	= arrUserInfo[8];
		
		byte[] arrName = new byte[arrUserInfo.length - defaultSize];
		for(int i = 0 ; i < arrName.length; i++)
		{
			arrName[i] = arrUserInfo[i + defaultSize];
		}
		
		this.name = new String(arrName);
	}
	
	/**
	 * 바이트 배열을 반환한다.
	 * @return 바이트 	배열
	 */
	public byte[] getBytes()
	{

		byte[] arrName = name.getBytes(    );
		
		byte[] arrUserInfo = new byte[defaultSize + arrName.length];
		

		byte[] arrIdentifier = Utils.IntToByteArray(this.identifier);
		for(int i = 0 ; i < arrIdentifier.length ; i++)
		{
			arrUserInfo[i] = arrIdentifier[i]; 
		}
		
		arrUserInfo[4] = (byte) this.weight;
		arrUserInfo[5] = (byte) this.height;
		arrUserInfo[6] = (byte) this.age;
		arrUserInfo[7] = (byte) this.gender;
		arrUserInfo[8] = this.calorieAlgorithm;
		
		for(int j = 0 ; j < arrName.length ; j++)
		{
			arrUserInfo[j + 9] = arrName[ j ];
		}
		
		return arrUserInfo;
	}
	
	/**
	 * 식별자를 반환한다.
	 * @return
	 */
	public int getIdentifier() {
		
		return identifier;
	}
	
	/**
	 * 식별자를 설정한다.
	 * @param identifier
	 */
	public void setIdentifier(int identifier) {
		this.identifier = identifier;
	}

	/**
	 * 체중을 반환한다.
	 * @return
	 */
	public int getWeight() {
		return weight;
	}

	/**
	 *  체중을 설정한다.
	 * @param weight
	 */
	public void setWeight(int weight) {
		this.weight = weight;
	}

	/**
	 * 신장을 반환한다.
	 * @return
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * 신장을 설정한다.
	 * @param height
	 */
	public void setHeight(int height) {
		this.height = height;
	}

	/**
	 * 나이를 반환한다.
	 * @return
	 */
	public int getAge() {
		return age;
	}

	/**
	 * 나이를 설정한다.
	 * @param age
	 */
	public void setAge(int age) {
		this.age = age;
	}

	/**
	 * 성별을 반환한다.
	 * @return
	 */
	public int getGender() {
		return gender;
	}

	/**
	 * 성별을 설정한다.
	 * @param gender
	 */
	public void setGender(int gender) 
	{
		this.gender = gender;
	}

	/**
	 * 칼로리 알고리즘을 반환한다.
	 * @return
	 */
	public byte getCalorieAlgorithm() {
		return calorieAlgorithm;
	}

	/**
	 * 칼로리 알고리즘을 설정한다.
	 * @param calorieAlgorithm
	 */
	public void setCalorieAlgorithm(byte calorieAlgorithm) {
		this.calorieAlgorithm = calorieAlgorithm;
	}

	/**
	 * 이름을 반환한다.
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * 이름을 설정한다.
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	
	
}
