package com.fitdotlife.fitmate_lib.service.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.fitdotlife.fitmate_lib.service.bluetooth.BluetoothRawData;

import java.util.ArrayList;
import java.util.List;

public class FitmateServiceDBManager
{
	private String TAG = "fitmateservice";
	private SQLiteDatabase mDB = null;
	private FitmateServiceDBOpenHelper mDBOpenHelper = null;
	
	private Context mContext = null;
	
	public FitmateServiceDBManager(Context context)
	{
		this.mContext = context;
	}
	
	/**
	 * 데이터베이스를 오픈한다.
	 */
	private void open()
	{
		if( mDB == null )
		{
            this.mDBOpenHelper = FitmateServiceDBOpenHelper.getInstance(this.mContext);
			this.mDB = this.mDBOpenHelper.getWritableDatabase();
		}
	}
	
	/**
	 * 데이터베이스를 닫는다.
	 */
	public void close()
	{
		if( this.mDBOpenHelper != null )
		{
			this.mDBOpenHelper.close();
		}
	}
	
	/**
	 * 데이터베이스에 데이터를 삽입한다.
	 * @param tableName
	 * @param addRowValue
	 * @return
	 */
	private long insert( String tableName , ContentValues addRowValue )
	{
		if( mDB == null )
		{
			this.open();
		}
		
		long result = mDB.insert( tableName , null , addRowValue );
		
		return result;
	}
	
	/**
	 * 데이터 베이스에 데이터를 갱신한다.
	 * @param tableName
	 * @param updateRowValue
	 * @param whereClause
	 * @param whereArgs
	 * @return
	 */
	private long update( String tableName , ContentValues updateRowValue , String whereClause , String[] whereArgs  )
	{
		if( mDB ==null )
		{
			this.open();
		}
		
		long result = mDB.update(tableName, updateRowValue, whereClause, whereArgs);
		return result;
	}
	
	/**
	 * 데이터베이스에서 데이터를 가져온다.
	 * @param dbName
	 * @param columns
	 * @param selection
	 * @return
	 */
	private Cursor query( String dbName , String[] columns , String selection )
	{
		if( mDB == null )
		{
			open();
		}
		
		Cursor c = null;
		
		c = mDB.query(	dbName , 
				columns , 
				selection , 
				null , 
				null , 
				null , 
				null );
		
		return c;
	}

    /**
     * 데이터베이스에서 데이터를 가져온다.
     * @param dbName
     * @param columns
     * @param selection
     * @return
     */
    private Cursor query( String dbName , String[] columns , String selection , String[] selectionArgs , String groupBy , String having , String orderBy)
    {
        if( mDB == null )
        {
            open();
        }

        Cursor c = null;

        c = mDB.query(	dbName ,
                columns ,
                selection ,
                selectionArgs ,
                groupBy ,
                having ,
                orderBy );

        return c;
    }

	/**
	 * 데이터베이스에서 데이터를 삭제한다.
	 * @param tableName
	 * @param whereClause
	 * @param whereArgs
	 */
	private int delete( String tableName , String whereClause , String[] whereArgs )
	{
		if( this.mDB == null )
		{
			this.open();
		}
		
		int result = mDB.delete(tableName , whereClause , whereArgs);
        return result;
	}

    public synchronized boolean updateFileData( int fileIndex , int fileDataOffset , byte[] addFileData ){

        Cursor c = null;
        long result = 0;
        c = this.query( FitmateServiceDBOpenHelper.DB_TABLE_RAWDATA , new String[]{"*"} , "fileindex=" + fileIndex );

        if( c.getCount() > 0 ){

            c.moveToFirst();

//            byte[] previousData = c.getBlob( c.getColumnIndex("filedata") );
//            byte[] totalBytes = new byte[ previousData.length + addFileData.length ];
//
//            System.arraycopy(previousData , 0 , totalBytes , 0, previousData.length);
//            System.arraycopy(addFileData , 0 , totalBytes , previousData.length , addFileData.length);

            ContentValues updateValues = new ContentValues();
            updateValues.put( "filedata" , addFileData );
            updateValues.put( "fileoffset" , fileDataOffset);
            result = this.update(FitmateServiceDBOpenHelper.DB_TABLE_RAWDATA , updateValues , "fileindex=" + fileIndex , null);

        }else{
            ContentValues addValues = new ContentValues();
            addValues.put("fileindex", fileIndex);
            addValues.put("fileoffset", fileDataOffset);
            addValues.put("filedata", addFileData);

            result = this.insert(FitmateServiceDBOpenHelper.DB_TABLE_RAWDATA, addValues);
        }

        c.close();

        if( result > 0 ){
            return true;
        }
        return false;
    }

    public synchronized List<BluetoothRawData> getDataFileList( int startIndex , int endIndex ) {

        Cursor c = null;
        long result = 0;
        c = this.query( FitmateServiceDBOpenHelper.DB_TABLE_RAWDATA , new String[]{"*"} , "fileindex >=" + startIndex + " AND fileindex <= " + endIndex );

        List<BluetoothRawData> rawDataList = new ArrayList<BluetoothRawData>();
        if( c.getCount() > 0 ) {

            while (c.moveToNext()) {

                BluetoothRawData rawData = new BluetoothRawData();
                rawData.setFileIndex(c.getInt(c.getColumnIndex("fileindex")));
                //rawData.setFileOffset(c.getInt(c.getColumnIndex("fileoffset")));
                rawData.setFileData(c.getBlob(c.getColumnIndex("filedata")));

                rawDataList.add(rawData);
            }
        }
        c.close();

        return rawDataList;
    }

    public synchronized void deleteFileList( List<Integer> fileIndexList ){
        for( int i = 0  ; i < fileIndexList.size();i++ ) {
            int fileIndex = fileIndexList.get(i);
            this.delete(FitmateServiceDBOpenHelper.DB_TABLE_RAWDATA, "fileindex=" + fileIndex , null);
        }
    }

    //모든 파일 삭제, 기기 변경시 기존 파일들이 있다면 다 지워야 한다.
    public synchronized void deleteAllFiles(){
        this.delete(FitmateServiceDBOpenHelper.DB_TABLE_RAWDATA, null , null);
    }

    public synchronized boolean deleteFile( int fileIndex ){

        int result = this.delete( FitmateServiceDBOpenHelper.DB_TABLE_RAWDATA , "fileindex=" + fileIndex , null );
        if( result > 0 ) return  true;
        return false;

    }

    public synchronized BluetoothRawData getLastFileData(){
        Cursor c = null;
        long result = 0;
        c = this.query( FitmateServiceDBOpenHelper.DB_TABLE_RAWDATA , new String[]{"*"} , null, null , null , null , "fileindex DESC" );

        BluetoothRawData data = null;
        if( c!= null && c.getCount() > 0 && c.moveToFirst() ) {

            data = new BluetoothRawData();
            data.setFileIndex( c.getInt(c.getColumnIndex("fileindex")) );
            //data.setFileOffset( c.getInt(c.getColumnIndex("fileoffset")) );
            data.setFileData( c.getBlob( c.getColumnIndex("filedata") ) );

        }
        c.close();

        return data;
    }

    public synchronized List<BluetoothRawData> getAllFileData() {

        Cursor c = null;
        long result = 0;
        c = this.query( FitmateServiceDBOpenHelper.DB_TABLE_RAWDATA , new String[]{"*"} , null );

        List<BluetoothRawData> rawDataList = new ArrayList<BluetoothRawData>();
        if( c.getCount() > 0 ) {

            while (c.moveToNext()) {

                BluetoothRawData rawData = new BluetoothRawData();
                rawData.setFileIndex(c.getInt(c.getColumnIndex("fileindex")));
                //rawData.setFileOffset(c.getInt(c.getColumnIndex("fileoffset")));
                rawData.setFileData(c.getBlob(c.getColumnIndex("filedata")));

                rawDataList.add(rawData);
            }
        }
        c.close();

        return rawDataList;
    }

    public synchronized BluetoothRawData getDataFile( int fileIndex ){
        Cursor c = null;
        long result = 0;
        c = this.query( FitmateServiceDBOpenHelper.DB_TABLE_RAWDATA , new String[]{"*"} , "fileindex=" + fileIndex );

        BluetoothRawData data = null;
        if( c.getCount() > 0 ) {

            c.moveToFirst();

            data = new BluetoothRawData();
            data.setFileIndex( c.getInt(c.getColumnIndex("fileindex")) );
            //data.setFileOffset( c.getInt(c.getColumnIndex("fileoffset")) );
            data.setFileData( c.getBlob( c.getColumnIndex("filedata") ) );
        }
        c.close();

        return data;
    }

}
