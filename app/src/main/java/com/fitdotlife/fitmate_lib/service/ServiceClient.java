package com.fitdotlife.fitmate_lib.service;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;

import com.fitdotlife.fitmate_lib.service.alarm.AlarmService;
import com.fitdotlife.fitmate_lib.service.key.MessageType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Joshua on 2015-02-23.
 */
public class ServiceClient implements ServiceConnection {

//    public static final int MSG_REGISTER_CLIENT = 0;
//    public static final int MSG_UNREGISTER_CLIENT = 1;
//    public static final int MSG_SET_USERINFO = 2;
//    public static final int MSG_GET_DAY_ACTIVITY = 3;
//    public static final int MSG_GET_WEEK_ACTIVITY = 4;
//    public static final int MSG_GET_MONTH_ACTIVITY = 5;
//    public static final int MSG_CALCULATE_ACTIVITY = 6;
//    public static final int MSG_START_SERVICE = 7;
//    public static final int MSG_APPLY_EXERCISEPROGRAM = 8;
//    public static final int MSG_DATA_SYNC = 9;
//    public static final int MSG_DELETE_ALLFILES =10;
//    public static final int MSG_BATTERYRATE = 11;
//    public static final int MSG_STOP_SERVICE = 12;
//    public static final int MSG_RESTART_SERVICE = 13;
//    public static final int MSG_RESET_GET_DATA = 14;

    private final String TAG = "fitmate - " + ServiceClient.class.getSimpleName();
    private final String SERVICE_ACTION_NAME = "com.fitdotlife.fitmate.service_beta.FITMETER";
    private final String SERVICE_PACKAGE_NAME = "com.fitdotlife.fitmate";

    private final String CLIENT_NUMBER_KEY = "clientnumber";

    private final String SUCCESS = "SUCCESS";
    private final String FAIL = "FAIL";
    private final String RESULT_KEY = "result";

    private Context mContext = null;
    private Messenger mResponseMessenger = null;
    private Messenger mService = null;
    private int mClientNumber = 0;
    private ServiceRequestCallback mCallback = null;

    private List<Integer> messageCodeList = new ArrayList<Integer>();

    private Handler inComingHandler = new Handler()
    {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch( msg.what )
            {
                case MessageType.MSG_REGISTER_CLIENT:
                    Bundle responseRegisterClientBundle = msg.getData();
                    mClientNumber = responseRegisterClientBundle.getInt( CLIENT_NUMBER_KEY );

                    //쌓인 메시지를 전송한다.
                    for( int i = 0 ; i < messageCodeList.size() ;i++ ){
                        try {
                            sendServiceMessage(messageCodeList.get(i));
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }

                    messageCodeList.clear();
                    break;
                case MessageType.MSG_CALCULATE_ACTIVITY:
                    mCallback.onServiceResultReceived(msg.what , msg.getData());
                    break;
                case MessageType.MSG_DATA_SYNC:
                    mCallback.onServiceResultReceived(msg.what , msg.getData());
                    break;
                case MessageType.MSG_BATTERYRATE:
                    mCallback.onServiceResultReceived(MessageType.MSG_BATTERYRATE, msg.getData());
                    break;
                case MessageType.MSG_STOP_SERVICE:
                    mCallback.onServiceResultReceived(msg.what, null);
                    break;
                case MessageType.MSG_CONNECTION_STATE:
                    mCallback.onServiceResultReceived( msg.what , msg.getData() );
                    break;
            }
        }
    };

    public ServiceClient(Context context, ServiceRequestCallback callback){
        this.mContext = context;
        this.mCallback = callback;
    }



    private void startService(){
        //Intent intent = new Intent( this.SERVICE_ACTION_NAME);
        //intent.setPackage(this.SERVICE_PACKAGE_NAME);
        //this.mContext.startService(intent);

        Intent serviceIntent = new Intent(this.mContext , FitmateService.class);
        this.mContext.startService(serviceIntent);

        Intent alarmIntent = new Intent( this.mContext , AlarmService.class );
        this.mContext.startService(alarmIntent);
    }

    private void stopService(){
        Intent serviceIntent = new Intent( this.mContext , FitmateService.class );
        this.mContext.stopService( serviceIntent );
    }

    private void bindService(  )
    {
        this.mResponseMessenger = new Messenger(this.inComingHandler);

        //Intent intent = new Intent( this.SERVICE_ACTION_NAME);
        //intent.setPackage(this.SERVICE_PACKAGE_NAME);
        //this.mContext.bindService( intent , this , Context.BIND_AUTO_CREATE );

        Intent serviceIntent = new Intent(this.mContext , FitmateService.class);
        this.mContext.bindService( serviceIntent , this , Context.BIND_AUTO_CREATE );


        Intent alarmIntent = new Intent( this.mContext , AlarmService.class );
        this.mContext.bindService(alarmIntent, this, Context.BIND_AUTO_CREATE);

    }

    private void unBindService( ){
        if( this.mService != null )
        {
            Message msg = Message.obtain(null , MessageType.MSG_UNREGISTER_CLIENT);
            msg.replyTo = this.mResponseMessenger;
            Bundle unBindBundle = new Bundle();
            unBindBundle.putInt(this.CLIENT_NUMBER_KEY ,  this.mClientNumber);
            msg.setData(unBindBundle);

            try {
                mService.send(msg);
            } catch (RemoteException e) {
                //TODO 에러처리
            }
        }

        this.mContext.unbindService( this );

    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        this.mService = new Messenger(service);

        Message msg = Message.obtain(null , MessageType.MSG_REGISTER_CLIENT);
        msg.replyTo = this.mResponseMessenger;

        try {

            this.mService.send(msg);

        } catch (RemoteException e) {
            //TODO 에러처리
        }


    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        this.mService = null;
    }

//    public void sendServiceMessage( int messageCode , Bundle messageBundle  ) throws RemoteException {
//
//        Message message = Message.obtain(null , messageCode );
//        messageBundle.putInt(this.CLIENT_NUMBER_KEY , this.mClientNumber);
//        message.setData(messageBundle);
//        if( this.mService != null ) {
//            this.mService.send(message);
//        }else{
//            this.messageList.add(message);
//        }
//    }

    public void sendServiceMessage(int messageCode) throws RemoteException {
        Message message = Message.obtain(null , messageCode );
        Bundle messageBundle = new Bundle();
        messageBundle.putInt(this.CLIENT_NUMBER_KEY , this.mClientNumber);
        message.setData(messageBundle);
        if( this.mService != null ) {
            this.mService.send(message);
        }else{
            this.messageCodeList.add( messageCode );
        }
    }

    public void close() {
        this.unBindService();
    }
    public void open(){ this.bindService(); }
    public void start(){ this.startService(); }
    public void stop() { this.stopService(); }
}
