package com.fitdotlife.fitmate_lib.service.protocol;

public class FileReadException extends Exception
{
	private static final long serialVersionUID = 1L;
	
	private String message;
	
	public FileReadException( String message )
	{
		this.message = message;
	}
	
	@Override
	public String getMessage()
	{
		return this.message;
	}
	
}
