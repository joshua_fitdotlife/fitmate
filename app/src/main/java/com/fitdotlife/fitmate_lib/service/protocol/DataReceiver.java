package com.fitdotlife.fitmate_lib.service.protocol;

import java.io.IOException;


public class DataReceiver {
	
	private final int RECEIVE_LENGTH = 1024;
	private CommunicationInterface communicationInterface;
	

	public DataReceiver(CommunicationInterface communicationInterface)
	{
		this.communicationInterface = communicationInterface;
	}

	public byte[] read() throws IOException, FileReadException {
		byte[] result = null;
		byte[] readBytes = communicationInterface.read(  );

		byte[] arrDecodedData = Decoder.decoding(readBytes);

		if( checkSum(arrDecodedData) )
		{
			result = new byte[arrDecodedData.length -1];
			try {
                System.arraycopy(arrDecodedData, 0, result, 0, result.length);
            }catch( IndexOutOfBoundsException e ){
                throw new FileReadException( "read() 함수에서 checksum 후 배열을 복사하는데에서 에러가 발생하였습니다. -" + e.getMessage() );
            }
		}
		
		return result;
    }
	
	private boolean checkSum(byte[] source)
	{
		boolean result = false;

		byte checksum = 0;
		
		for(int i = 0 ; i < source.length - 1 ; i++)
		{
			checksum += ( source[i] & 0xFF );
		}
		
		if( source[source.length-1] == checksum )
			result = true;
		
		return result;
	}

	public byte[] readData( int length ) throws IOException, FileReadException
	{
		byte[] result = null;
		result = communicationInterface.readData( length );
		return result;
	}
}
