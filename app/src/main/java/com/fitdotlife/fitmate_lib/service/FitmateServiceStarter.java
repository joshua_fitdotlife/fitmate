package com.fitdotlife.fitmate_lib.service;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.os.Vibrator;
import android.support.v4.content.PermissionChecker;
import android.util.Log;

import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.service.bluetooth.FitmeterDeviceDriver;
import com.fitdotlife.fitmate_lib.service.bluetooth.FitmeterDeviceManager;
import com.fitdotlife.fitmate_lib.service.database.FitmateServiceDBManager;
import com.fitdotlife.fitmate_lib.service.key.MessageType;
import com.fitdotlife.fitmate_lib.service.key.SyncType;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;

/**
 * Created by Joshua on 2015-01-28.
 */
public class FitmateServiceStarter implements FitmeterDeviceManager.FitmeterDeviceStateCallback ,  ActivityDataCalculator.CalculateCompleteListener
{
    private final String TAG = "fitmateservice - " + FitmateServiceStarter.class.getSimpleName();
    private FitmateServiceStarter fss = null;
    private boolean isSleep = false;

    public interface MessageCallback{

        void onSyncInfo(SyncType syncType, int recceiveBytes, int totalBytes);
        void onBatteryStatus(int batteryRatio);
        void onConnectionStatus( ConnectionState ConnectionState );
    }

    private MessageCallback mMessageCallback;
    private Context mContext = null;
    private BluetoothManager mBluetoothManager = null;
    private BluetoothAdapter mBluetoothAdapter = null;
    private FitmateDBManager mDBManager = null;

    private FitmateServiceDBManager mServiceDBManager = null;

    private boolean isRunThread = false;
    private boolean isRun = true;

    private Thread mDataGetThread = null;
    private FitmeterDeviceManager mFitmeterDeviceManager = null;
    private FitmeterDeviceDriver mFitmeterDeviceDriver = null;

    private BroadcastReceiver mReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            String action = intent.getAction();
            if( BluetoothAdapter.ACTION_STATE_CHANGED.equals( action ) )
            {
                final int state = intent.getIntExtra( BluetoothAdapter.EXTRA_STATE , BluetoothAdapter.ERROR);
                switch( state )
                {
                    case BluetoothAdapter.STATE_TURNING_OFF:
                        org.apache.log4j.Log.d(TAG, "블루투스가 꺼집니다.");
                        try{
                            isRun = false;
                            isRunThread = false;
                            mFitmeterDeviceDriver.close();
                        }
                        catch (Exception ex)
                        {
                            org.apache.log4j.Log.d(TAG, "블루투스 데이터 스레드 에러 발생. " + ex.getMessage());
                        }

                        break;
                    case BluetoothAdapter.STATE_ON:
                        org.apache.log4j.Log.d(TAG, "블루투스가 켜집니다.");

                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        if(!isRunThread)
                            start();

                        break;
                }
            }
        }
    };

    public FitmateServiceStarter( final Context context  , FitmateDBManager dbManager , InComingMessageManager inComingMessageManager ) {
        fss = this;
        this.mContext = context;
        this.mDBManager = dbManager;
        this.mServiceDBManager = new FitmateServiceDBManager(this.mContext);
        this.mMessageCallback = inComingMessageManager;

        this.mBluetoothManager = (BluetoothManager) this.mContext.getSystemService(Context.BLUETOOTH_SERVICE);
        this.mBluetoothAdapter = this.mBluetoothManager.getAdapter();

        //블루투스가 꺼질때와 켜질때를 알수 있는 리시버를 등록한다.
        IntentFilter bluetoothFilter = new IntentFilter();
        bluetoothFilter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        this.mContext.registerReceiver(this.mReceiver, bluetoothFilter);

        start();
    }


//    public boolean stopThread()
//    {
//        Log.d("CONTROLSERVICE", "STOP Thread start");
//        isRun = false;
//        for (int i = 0; i < 100; i++)
//        {
//
//            if(!isRunThread) {
//                Log.d("CONTROLSERVICE", "STOP Thread Completed");
//                break;
//            }
//            try {
//                if(mDataGetThread != null)
//                    mDataGetThread.interrupt();
//                Thread.sleep(100);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
//        mFitmeterDeviceDriver = null;
//        Log.d("CONTROLSERVICE", "STOP Thread end");
//
//        return isRunThread;
//    }

    public boolean stopThread()
    {
        isRun = false;
        return true;
    }


    void start()
        {
        Log.d(TAG , "isRunThread : " + isRunThread);

        //쓰레드가 실행되어 있다면 시작할 수 없다.
        if( isRunThread ) return;

        //블루투스가 켜져 있지 않다면 시작할 수 없다.
        if( !isBtEnabled() ) {
            return;
        }

        final UserInfo userInfo = mDBManager.getUserInfo();
        Log.e(TAG, "FitmateServiceStarter start : " + userInfo.getDeviceAddress() );
        //사용자의 Fitmeter 기기 주소가 없으면 시작할 수 없다.
        if( userInfo.getDeviceAddress() == null ) {
            return;
        }

        mFitmeterDeviceManager = new FitmeterDeviceManager( mContext , mBluetoothAdapter , fss, mServiceDBManager);

        mDataGetThread = new Thread(new Runnable()
        {

            @Override
            public void run()
            {
                isRunThread = true;
                isRun = true;
                boolean isDataTransfer = false;

                int scan_fail_cnt = 0;
                int dataReceivedFailCnt = 0;
                Log.i("ConThread", "Thread Run Start");

                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                while( mBluetoothAdapter.isEnabled() && isRun )
                {
                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M )
                    {
                        if( PermissionChecker.checkSelfPermission( mContext , Manifest.permission.ACCESS_FINE_LOCATION ) == PackageManager.PERMISSION_DENIED ){
                            try {
                                Thread.sleep(10000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                            continue;
                        }
                    }

                    isSleep = false;
                    boolean isError133 = false;

                    try {
                        Log.i("ConThread", "RunConnection");

                        String deviceAddress = userInfo.getDeviceAddress();
                        if(mFitmeterDeviceDriver == null)
                        {
                            mFitmeterDeviceDriver = new FitmeterDeviceDriver( mContext , mBluetoothAdapter , deviceAddress );
                        }

                        Log.d(TAG, "RunConnection");

                        Log.i("ConThread", "startScan");
                        if(mFitmeterDeviceDriver.startScan())
                        {

                            Thread.sleep(100);
                            Log.i("ConThread", "Start Connection");
                            mFitmeterDeviceDriver.initialize();
                        }
                        else
                        {
                            Log.i("ConThread", "Scan Fail");
                            mMessageCallback.onConnectionStatus(ConnectionState.SCAN_FAIL);
                            if(scan_fail_cnt > 1)
                            {
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        //NetworkClass nc = new NetworkClass();
                                        //nc.PostInputStreamFromUrl("https://api.hipchat.com/v2/room/dev_fitmatesnu/notification?auth_token=uWBYEXiP4lZvCftjVJ5SVPsdk7KEkEM8E7Hy4zKD", new J);
                                        //NetworkClassThread networkClassThread = new NetworkClassThread( mContext , "https://api.hipchat.com/v2/room/dev_fitmatesnu/notification?auth_token=uWBYEXiP4lZvCftjVJ5SVPsdk7KEkEM8E7Hy4zKD", jsonArray , responseCode , this.mCallback );
                                        //networkClassThread.start();
                                        Vibrator vibe = (Vibrator) mContext.getSystemService(Context.VIBRATOR_SERVICE);
                                        //vibe.vibrate(500);

                                        // todo 테스트용 진동 코드

                                        Log.e("ConThread", "스캔 다수 실패로 진동!!");

                                    }
                                }).start();
                            }
                            else {
                                scan_fail_cnt++;
                            }

                            continue;
                        }

                        Log.i("ConThread", "Connection Check");
                        // todo 10을 50으로 변경해야됨. 테스트용
                        for(int i = 0; i < 500; i++)
                        {
                            if(mFitmeterDeviceDriver.isConnected)
                            {
                                Log.i(TAG, "연결 성공");
                                scan_fail_cnt = 0;
                                break;
                            }

                            if(!mFitmeterDeviceDriver.isDisonnected)
                            {
                                Log.i(TAG, "연결 실패");
                                break;
                            }

                            if(mFitmeterDeviceDriver.isFailed)
                            {
                                Log.i(TAG, "연결 isFailed");
                                isError133 = true;

                                //mFitmeterDeviceDriver.disconnect();
                                //mFitmeterDeviceDriver.close();

                                Thread.sleep(5000);

                                // todo 여기 걸리면 삼성폰은 계속 대기함. 아마 커넥션을 강제로 끊는게 먹지 않는듯.
                                // 133 에러날때도 마찬가지임

                                throw new Exception("커넥션 중 133 에러 발생");

                                //break;
                            }

                            Thread.sleep(100);
                        }

                        if(mFitmeterDeviceDriver.isConnected)
                        {
                            Log.i("ConThread", "Connected");
                            Log.i("ConThread", "Start Discovery");
                            mFitmeterDeviceDriver.startServicesDiscovery();
                            for(int i = 0; i < 30; i++)
                            {
                                if(mFitmeterDeviceDriver.isDiscovered)
                                {
                                    break;
                                }

                                Thread.sleep(100);
                            }
                        }
                        else
                        {
                            Log.i("ConThread", "Not Connected");

                            // todo 133과 비슷한 상황임
                            isError133 = true;

                            mFitmeterDeviceDriver.OnlyClose();
                            //mFitmeterDeviceDriver.close();
                            mFitmeterDeviceDriver.disconnect();
                            Thread.sleep(5000);

                            List<BluetoothDevice> mList = mBluetoothManager.getConnectedDevices(BluetoothProfile.GATT);
                            for (BluetoothDevice bd : mList) {
                                Log.i(TAG, String.format("연결 장치 체크 : %s[%s] Connected", bd.getName(), bd.getAddress()));
                            }

                            mList = mBluetoothManager.getConnectedDevices(BluetoothProfile.GATT_SERVER);
                            for (BluetoothDevice bd : mList) {
                                Log.i(TAG, String.format("연결 서버 체크 : %s[%s] Connected", bd.getName(), bd.getAddress()));
                            }
                            continue;
                        }

                        if(mFitmeterDeviceDriver.isDiscovered)
                        {
                            Log.i("ConThread", "Start Descriptor Write");
                            mFitmeterDeviceDriver.setTXNotification();
                            for(int i = 0; i < 30; i++)
                            {
                                if(mFitmeterDeviceDriver.isSetTxNotification)
                                {
                                    Log.i("ConThread", "Descriptor Write Completed");
                                    break;
                                }

                                try {
                                    Thread.sleep(100);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                        else
                        {
                            Log.i("ConThread", "Not Discorverd");
                            continue;
                        }

                        for(int i = 0; i < 30; i++)
                        {
                            if (mFitmeterDeviceDriver.isSetTxNotification )
                            {
                                mMessageCallback.onConnectionStatus( ConnectionState.CONNECTION_SUCESS );
                                break;
                            }

                            Thread.sleep(100);
                        }

                        if(!mFitmeterDeviceDriver.isSetTxNotification)
                        {
                            Log.i("ConThread", "Not isConnectedComplted");
                            continue;
                        }

                        Log.i("ConThread", "Start GetData");
                        isDataTransfer = true;
                        if(!mFitmeterDeviceManager.getData(mFitmeterDeviceDriver))
                        {
                            throw new Exception("데이터 전송 중 실패. 빠른 재연결 필요!!");

                        }
                        isDataTransfer = false;
                        Log.i("ConThread", "End GetData");
                    }
                    catch(Exception ex)
                    {
                        StringWriter errors = new StringWriter();
                        ex.printStackTrace(new PrintWriter(errors));

                        org.apache.log4j.Log.d("ConThread", "throw>" + ex.getMessage() + "\n" + errors.toString());
                    }

                    finally {
                        try {
                            Log.i("ConThread", "Finally Call");

                            if(!isError133)
                            {
                                if( mFitmeterDeviceDriver != null )
                                {
                                    mFitmeterDeviceDriver.disconnect();
                                }

                                Thread.sleep(100);
                            }

                            if(mFitmeterDeviceDriver != null)
                            {
                                mFitmeterDeviceDriver.close();
                            }

                            Log.i("ConThread", "Device disconnet complete");

                            if(isDataTransfer && dataReceivedFailCnt <= 3)
                            {
                                dataReceivedFailCnt++;
                                Thread.sleep(1000);
                            }
                            else
                            {
                                dataReceivedFailCnt = 0;
                                isSleep = true;
                                Thread.sleep(10000);

                                // 혹시 슬립을 20초를 줘야 블투 장비를 정리하나??? 테스트 해봐야지
                            }
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                    }
                }
            }
        });

        mDataGetThread.start();
    }

    private boolean isBtEnabled()
    {

        final BluetoothManager manager = (BluetoothManager) mContext.getSystemService( Context.BLUETOOTH_SERVICE );
        if( manager == null ) return false;

        final BluetoothAdapter adapter = manager.getAdapter();
        if( adapter == null ) return false;

        return adapter.isEnabled();
    }

    public void requestCalculate( Messenger responseMessenger){

        if( true ) {
            ActivityDataCalculator dataCalculator = ActivityDataCalculator_.getInstance_(mContext);
            dataCalculator.setFitmateDBManager(this.mDBManager);
            dataCalculator.setCalculateCompleteListener( this );
            dataCalculator.setFitmateServiceDBManager( this.mServiceDBManager );
            dataCalculator.setNotifyCalculateComplete(true);
            dataCalculator.setResponseMessenger( responseMessenger );
            dataCalculator.start();
        }else{
            Message msg = Message.obtain(null, MessageType.MSG_CALCULATE_ACTIVITY);

            try {

                if( responseMessenger != null ) {
                    responseMessenger.send(msg);
                }

            } catch (RemoteException e) {
            }
        }
    }

    public void close()
    {
        this.mContext.unregisterReceiver(this.mReceiver);
        this.stopThread();
        mDataGetThread = null;
    }

    public void resetGetData(){
        if( isSleep ) {
            if( mDataGetThread != null ) {
                try {
                    if(mDataGetThread != null)
                        mDataGetThread.interrupt();
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    return;
                }
            }

            start();
        }
    }


    @Override
    public void onSyncInfo( final SyncType syncType , final int recceiveBytes, final int totalBytes ) {

        mMessageCallback.onSyncInfo(syncType, recceiveBytes, totalBytes);

    }

    @Override
    public void onBatteryStatus(int batteryRatio)
    {
        mMessageCallback.onBatteryStatus(batteryRatio);
    }

    @Override
    public void onCalculateCompleted() {

    }

}
