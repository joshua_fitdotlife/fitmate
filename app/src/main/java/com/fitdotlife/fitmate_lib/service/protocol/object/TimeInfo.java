package com.fitdotlife.fitmate_lib.service.protocol.object;

import com.fitdotlife.fitmate_lib.service.activity.HeaderParseException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class TimeInfo {
	
	public static final int LENGTH = 7;
	private byte[] arrTimeInfo;
	private short[] arrLastDay = new short[]{ 31 , 28 , 31 , 30 , 31 , 30 , 31 , 31 , 30 , 31 , 30 , 31 };
	private int currentLastMonthDay = 0;
    private NextDayListener mListener = null;
    private long milliSecond;

    public TimeInfo()
	{
		arrTimeInfo = new byte[LENGTH]; 
	}
	
	public TimeInfo(TimeInfo timeInfo)
	{
		arrTimeInfo = new byte[LENGTH];
		
		this.setYear( timeInfo.getYear() );
		this.setMonth( timeInfo.getMonth() );
		this.setDay( timeInfo.getDay() );
		this.setHour( timeInfo.getHour() );
		this.setMinute( timeInfo.getMinute() );
		this.setSecond( timeInfo.getSecond() );
		
		//윤년 구하기
		this.checkLeapYear();
		
		//현재 월의 마지막 날짜 구하기
		currentLastMonthDay = this.arrLastDay[ this.getMonth() - 1 ];
	}

	public TimeInfo( String timeString )
	{
		arrTimeInfo = new byte[LENGTH];
		
		this.setYear(Integer.parseInt(timeString.substring(0, 4)));
		this.setMonth(Integer.parseInt(timeString.substring(5, 7)));
		this.setDay(Integer.parseInt(timeString.substring(8, 10)));

        if( timeString.length() > 10 ) {

            this.setHour(Integer.parseInt(timeString.substring(11, 13)));
            this.setMinute(Integer.parseInt(timeString.substring(14, 16)));
            this.setSecond(Integer.parseInt(timeString.substring(17, 19)));

        }
		
		//윤년 구하기
		this.checkLeapYear();
		
		//현재 월의 마지막 날짜 구하기
		currentLastMonthDay = this.arrLastDay[ this.getMonth() - 1 ];
		
	}

    public TimeInfo( long time ){

		arrTimeInfo = new byte[LENGTH];

		Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis( time );

        this.setSecond(calendar.get(Calendar.SECOND));
        this.setMinute(calendar.get(Calendar.MINUTE));
        this.setHour(calendar.get(Calendar.HOUR_OF_DAY));
        this.setDay( calendar.get(Calendar.DAY_OF_MONTH ));
        this.setMonth(calendar.get(Calendar.MONTH) + 1);
        this.setYear(calendar.get(Calendar.YEAR));
    }

	public TimeInfo(byte[] time)
	{
        this.arrTimeInfo = time;
		
		//윤년 구하기
		this.checkLeapYear();
		
		//현재 월의 마지막 날짜 구하기
		currentLastMonthDay = this.arrLastDay[ this.getMonth() - 1 ];
	}
	
	public byte[] getBytes()
	{
		return this.arrTimeInfo;
	}
	
	public void setYear(int year)
	{
		this.arrTimeInfo[0] = (byte)(year - 2010);
		
		//윤년 구하기
		this.checkLeapYear();
	}
	
	public int getYear()
	{
		return this.arrTimeInfo[0] + 2010;
	}
	
	public void setMonth(int month)
	{
        if( month < 1 || month > 12 ){
               new HeaderParseException("설정된 월 값이 이상합니다. - " + month );
        }

		this.arrTimeInfo[1] = (byte) month;
		
		//현재 월의 마지막 날짜 구하기
		currentLastMonthDay = this.arrLastDay[ this.getMonth() - 1 ];
	}
	
	public int getMonth()
	{
		return this.arrTimeInfo[1];
	}
	
	
	public void setDay(int day)
	{
        if( day < 1 || day > currentLastMonthDay ){
            new HeaderParseException("설정된 일 값이 이상합니다. - " + day );
        }

		this.arrTimeInfo[2] = (byte)day;
	}
	
	
	public int getDay()
	{
		return this.arrTimeInfo[2];
	}
	
	public void setDayOfTheWeek(int weekday)
	{
		this.arrTimeInfo[3] = (byte) weekday;
	}
	
	public int getDayOfTheWeek()
	{
		return this.arrTimeInfo[3];
	}
	
	public void setHour(int hour)
	{
		this.arrTimeInfo[4] = (byte) hour;
	}
	
	public int getHour()
	{
		return this.arrTimeInfo[4];
	}
	
	public void setMinute(int minute)
	{
		this.arrTimeInfo[5] = (byte) minute;
	}
	
	public int getMinute()
	{
		return this.arrTimeInfo[5];
	}
	
	public void setSecond(int second)
	{
		this.arrTimeInfo[6] = (byte) second;
	}
	
	public int getSecond()
	{
		return this.arrTimeInfo[6];
	}
	
	public Date getTime()
	{
		Calendar cal = Calendar.getInstance();
		cal.set(this.getYear()  , this.getMonth() - 1  , this.getDay() , this.getHour() , this.getMinute() , this.getSecond());
		return cal.getTime();
	}
	
	public void setTime( Date date )
	{
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		
		this.setYear(cal.get( Calendar.YEAR ));
		this.setMonth( cal.get( Calendar.MONTH ) + 1 );
		this.setDay( cal.get(Calendar.DAY_OF_MONTH) );
		this.setDayOfTheWeek( cal.get( Calendar.DAY_OF_WEEK ) );
		this.setHour( cal.get( Calendar.HOUR_OF_DAY ) );
		this.setMinute( cal.get( Calendar.MINUTE ) );
		this.setSecond(cal.get(Calendar.SECOND));
	}

	public String getTimeString()
	{
		return this.getYear() + "-" + String.format("%02d", this.getMonth()) + "-" + String.format("%02d", this.getDay()) + " " + String.format("%02d", this.getHour()) + ":" + String.format("%02d", this.getMinute()) + ":" + String.format("%02d", this.getSecond());
	}
	
	public String getDateString()
	{
		return this.getYear() + "-" + String.format("%02d", this.getMonth()) + "-" + String.format("%02d", this.getDay());
	}
	
	private void checkLeapYear()
    {
        int currentYear = this.getYear();

        if(( currentYear % 4 == 0 && currentYear % 100 != 0 ) || currentYear % 400 == 0)
        {
            this.arrLastDay[ 1 ] = 29;
        }
    }
	
	public void addSecond( int addSecond  )
	{
		//초 더하기
		int second = this.getSecond() + addSecond;
		int addMin = second / 60;
		
		if( addMin  > 0 ) {
            this.setSecond(second % 60);
            this.addMin(addMin);
        }else{
			this.setSecond( second );
		}
	}
	
	private void addMin( int addMin )
	{
		//분 더하기
		int minute = this.getMinute() + addMin;
		int addHour = minute / 60;
		
		if( addHour  > 0 )
		{
			this.setMinute(minute % 60);
			this.addHour(addHour);
		}
		else
		{
			this.setMinute(minute);
		}
	}
	
	private void addHour( int addHour )
	{
		int hour = this.getHour() + addHour ;
		int addDay = hour / 24 ; 
		
		if( addDay  > 0 )
		{
			this.setHour(hour % 24);
			this.addDay(addDay);
            if( this.mListener != null ) { this.mListener.onNextDay(); }
		}
		else
		{
			this.setHour(hour);
		}
		
	}

	private void addDay(int addDay) 
	{
		//일 더하기
		int day = this.getDay() + addDay;
		int addMonth = day / ( this.currentLastMonthDay + 1 ) ;
		
		if( addMonth > 0 )
		{
			this.setDay((day + 1) % (this.currentLastMonthDay + 1));
			this.addMonth(addMonth);
		}
		else
		{
			this.setDay(day);
		}
	}

	private void addMonth(int addMonth) 
	{
		int month = this.getMonth() + addMonth;
		int addYear = month / 13 ;
		
		if( addYear > 0 )
		{
			this.setMonth( ( month + 1 ) % 13 );
			//월의 마지막날 구하기
			this.currentLastMonthDay = this.arrLastDay[ this.getMonth() - 1 ];
			this.addYear(addYear);
		}
		else
		{
			this.setMonth(month);
		}
	}

	private void addYear(int addYear) 
	{
		int year = this.getYear() + addYear;
		this.setYear(year);
		this.checkLeapYear();
	}

	public boolean before(TimeInfo afterTimeInfo ) 
	{
		long currentTimeLong = 0;
		long afterTimeLong = 0;
		currentTimeLong = ( this.getYear() << 5 ) + ( this.getMonth() << 4 ) +( this.getDay() << 3 ) +( this.getHour() << 2 ) +( this.getMinute() << 1 ) + this.getSecond();
		afterTimeLong = ( afterTimeInfo.getYear() << 5 ) + ( afterTimeInfo.getMonth()  << 4 ) +( afterTimeInfo.getDay() << 3 ) +( afterTimeInfo.getHour() << 2 ) +( afterTimeInfo.getMinute() << 1 ) + afterTimeInfo.getSecond();
		
		if( afterTimeLong > currentTimeLong ) return true;
		return false;
	}

    public void setNextDayListener( NextDayListener listener ){
        this.mListener = listener;
    }

    public TimeInfo copyCurrentValue(){
        return new TimeInfo( this.arrTimeInfo );
    }


    public long getMilliSecond() {
        Calendar calendar = Calendar.getInstance(Locale.KOREA );
        calendar.set( this.getYear() , this.getMonth() - 1 , this.getDay() , this.getHour() , this.getMinute() , this.getSecond() );


		 return calendar.getTimeInMillis();
    }

	public long getLocalMillisecond(){
		String strDate = this.getTimeString();
		Date localDate = null;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

		try {
			localDate = simpleDateFormat.parse(strDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return localDate.getTime();
	}

}
