package com.fitdotlife.fitmate_lib.imodel;

import com.fitdotlife.fitmate_lib.object.UserInfo;

/**
 * Created by Joshua on 2015-03-18.
 */
public interface IUserInfoModel{

    void setUserInfo(UserInfo userInfo);
    boolean localLogin(String email, String pwd);
    void serverLogin(String email, String pwd);
    UserInfo getUserInfo( );
    void updateDeviceAddress(String deviceAdrress);
    void modifyUserInfo(UserInfo userInfo);
    void isUsableEmail(String email);
    void deleteUserInfo();
    void sendPasswordEmail(String email);
}
