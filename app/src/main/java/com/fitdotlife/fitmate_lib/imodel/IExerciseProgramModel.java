package com.fitdotlife.fitmate_lib.imodel;

import com.fitdotlife.fitmate_lib.object.ExerciseProgram;

import java.util.List;

/**
 * Created by Joshua on 2015-03-18.
 */
public interface IExerciseProgramModel extends IModel{
    void getUserProgramList(String email, String password);

    List<ExerciseProgram> getUserProgramListByName(String serachWord);

    void getProgramList();

    void getProgramListByName(String exerciseProgramName);

    void getProgramListByOwner(String owner);

    void getExerciseProgram(int id);

    void createUserExerciseProgram(ExerciseProgram program);

    void getAppliedProgram(String email, String password);

    void applyExerciseProgram(String email, String password, ExerciseProgram program);

    void downloadProgram(String email, String password, int id);
}
