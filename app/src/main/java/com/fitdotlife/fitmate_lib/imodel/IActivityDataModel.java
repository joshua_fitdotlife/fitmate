package com.fitdotlife.fitmate_lib.imodel;

import android.os.RemoteException;

/**
 * Created by Joshua on 2015-03-18.
 */
public interface IActivityDataModel extends IModel{
    void requestDayActivity(String email, String password, String activityDate, int appliedExerciseProgramID);

    void requestWeekActivity(String email, String password, String activityDate, int appliedExerciseProgramID);

    void requestMonthActivity(String email, String password, String activityDate, int appliedExerciseProgramID);

    void requestCalculateActivity() throws RemoteException;
}
