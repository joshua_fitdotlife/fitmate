package com.fitdotlife.fitmate_lib.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabaseLockedException;
import android.util.Log;

/**
 * Created by Joshua on 2016-04-28.
 */
public class LogDBManager {
    private final int LOGGING_MAX_LENGTH = 1 * 1024 * 1024; //1MB

    private String TAG = "fitmate";
    private SQLiteDatabase mDB = null;
    private LogDBOpenHelper mDBOpenHelper = null;
    private Context mContext = null;

    private String logBuffer = "";

    public LogDBManager(Context context)
    {
        this.mContext = context;
    }

    /**
     * 데이터베이스를 오픈한다.
     */
    private void open()
    {
        if( mDB == null )
        {
            this.mDBOpenHelper = LogDBOpenHelper.getHelper( this.mContext );
            try {
                this.mDB = this.mDBOpenHelper.getWritableDatabase();
            }catch( SQLiteDatabaseLockedException sqldle ){ }
        }
    }

    /**
     * 데이터베이스를 닫는다.
     */
    public void close()
    {
        if( this.mDBOpenHelper != null )
        {
            this.mDBOpenHelper.close();
        }
    }

    /**
     * 데이터 베이스에 데이터를 갱신한다.
     * @param tableName
     * @param updateRowValue
     * @param whereClause
     * @param whereArgs
     * @return
     */
    private long update( String tableName , ContentValues updateRowValue , String whereClause , String[] whereArgs )
    {
        if( mDB ==null )
        {
            this.open();
        }

        long result = 0;
        if( mDB != null ) {
            result = mDB.update(tableName, updateRowValue, whereClause, whereArgs);
        }

        return result;
    }

    private Cursor query( String dbName , String[] columns , String selection )
    {
        if( mDB == null )
        {
            open();
        }


        Cursor c = null;
        if( mDB != null ) {

            c = mDB.query(dbName,
                    columns,
                    selection,
                    null,
                    null,
                    null,
                    null);
        }

        return c;
    }

    public synchronized void writeLog( String logMessage )
    {

        logBuffer += logMessage;

        Cursor c = null;
        c = this.query(  LogDBOpenHelper.DB_TABLE_LOG , new String[]{"*"} , null);

        if( c != null ) {
            if (c.getCount() > 0) {
                c.moveToFirst();

                logBuffer = logBuffer.replace("\\n", "$");

                String log = c.getString( c.getColumnIndex("log") );
                StringBuffer sb = new StringBuffer(log);
                sb.append(logBuffer);
                    //sb.append( ++logNumber + "\n" );
                if (sb.length() > (LOGGING_MAX_LENGTH))
                {
                    int overLength = LOGGING_MAX_LENGTH - sb.length();

                    while (true) {

                        int lineLocation = 0;
                        lineLocation = sb.indexOf("\n", lineLocation + 1);

                        if (overLength <= (lineLocation + 1)) {
                            sb.delete(0, lineLocation);
                            break;
                        }
                    }
                }

                ContentValues values = new ContentValues();
                values.put("log", sb.toString());

                sb.setLength(0);
                try {
                    this.update(LogDBOpenHelper.DB_TABLE_LOG, values, null, null);
                    logBuffer = "";
                } catch (SQLiteDatabaseLockedException e) {
                    c.close();
                }

            }

            c.close();
        }
    }

    public synchronized String readLog()
    {
        String logMessage = null;
        Cursor c = this.query( LogDBOpenHelper.DB_TABLE_LOG , new String[]{"*"} , null);

        if( c != null )
        {
            if (c.getCount() > 0) {
                c.moveToFirst();
                logMessage = c.getString( c.getColumnIndex("log") );
                logMessage = logMessage.replace("$", "\\n");
            }

            c.close();
        }

        return logMessage;
    }

}
