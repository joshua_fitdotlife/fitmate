package com.fitdotlife.fitmate_lib.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Joshua on 2016-04-28.
 */
public class LogDBOpenHelper extends SQLiteOpenHelper {

    private static final String DB_NAME 			                         = "Log.db";
    public static final String DB_TABLE_LOG                                  ="TLog";

    private static final int DB_VERSION 				= 1;

    private Context mContext = null;
    private static LogDBOpenHelper instance = null;

    public static synchronized LogDBOpenHelper getHelper( Context context ){
        if( instance == null ){
            instance = new LogDBOpenHelper( context );
        }

        return instance;
    }

    private LogDBOpenHelper(Context context)
    {
        super(context, DB_NAME , null , DB_VERSION );
        this.mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        createLogTable( db );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    private void createLogTable( SQLiteDatabase db ){
        db.execSQL( "CREATE TABLE " + DB_TABLE_LOG + "(" +
                "id INTEGER , " +
                "log TEXT);");

        db.execSQL("INSERT INTO " + DB_TABLE_LOG +
                "( id , log ) " +
                "VALUES(0 , '');");
    }

}
