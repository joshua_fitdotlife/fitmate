package com.fitdotlife.fitmate_lib.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ContinuousCheckPolicy;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.StrengthInputType;
import com.fitdotlife.fitmate_lib.key.GenderType;
import com.fitdotlife.fitmate_lib.object.DayAchieve;
import com.fitdotlife.fitmate_lib.object.DayActivity;
import com.fitdotlife.fitmate_lib.object.ExerciseProgram;
import com.fitdotlife.fitmate_lib.object.FriendWeekActivity;
import com.fitdotlife.fitmate_lib.object.ResponseDayActivity;
import com.fitdotlife.fitmate_lib.object.ResponseMonthActivity;
import com.fitdotlife.fitmate_lib.object.ResponseWeekActivity;
import com.fitdotlife.fitmate_lib.object.TargetInfo;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.object.UserNotiSetting;
import com.fitdotlife.fitmate_lib.object.WeekActivity;
import com.fitdotlife.fitmate_lib.service.protocol.object.TimeInfo;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class FitmateDBManager
{

    public static final int UPLOADED = 1;
    public static final int NOT_UPLOADED = 0;
    private final int NEED_MERGY = 2;

    private final int LOGGING_MAX_LENGTH = 1 * 1024 * 1024; //1MB

	private String TAG = "fitmate";
    private SQLiteDatabase mDB = null;
	private FitmateDBOpenHelper mDBOpenHelper = null;
	private Context mContext = null;
	
	public FitmateDBManager(Context context)
	{
		this.mContext = context;
	}
	
	/**
	 * 데이터베이스를 오픈한다.
	 */
	private void open()
	{
		if( mDB == null )
		{
			this.mDBOpenHelper = FitmateDBOpenHelper.getHelper( this.mContext );
			this.mDB = this.mDBOpenHelper.getWritableDatabase();
		}
	}
	
	/**
	 * 데이터베이스를 닫는다.
	 */
	public void close()
	{
		if( this.mDBOpenHelper != null )
		{
			this.mDBOpenHelper.close();
		}
	}
	
	/**
	 * 데이터베이스에 데이터를 삽입한다.
	 * @param tableName
	 * @param addRowValue
	 * @return
	 */
	private long insert( String tableName , ContentValues addRowValue )
	{
		if( mDB == null )
		{
			this.open();
		}
		
		long result = mDB.insert(tableName, null, addRowValue);
		
		return result;
	}

	/**
	 * 데이터 베이스에 데이터를 갱신한다.
	 * @param tableName
	 * @param updateRowValue
	 * @param whereClause
	 * @param whereArgs
	 * @return
	 */
	private long update( String tableName , ContentValues updateRowValue , String whereClause , String[] whereArgs  )
	{
		if( mDB ==null )
		{
			this.open();
		}

		long result = mDB.update(tableName, updateRowValue, whereClause, whereArgs);
		return result;
	}
	
	/**
	 * 데이터베이스에서 데이터를 가져온다.
	 * @param dbName
	 * @param columns
	 * @param selection
	 * @return
	 */
	private Cursor query( String dbName , String[] columns , String selection , String[] selectionArgs , String groupBy , String having , String orderBy)
	{
		if( mDB == null )
		{
			open();
		}
		
		Cursor c = null;
		
		c = mDB.query(	dbName , 
				columns , 
				selection ,
                selectionArgs ,
                groupBy ,
                having ,
                orderBy );
		
		return c;
	}

    private Cursor query( String dbName , String[] columns , String selection )
    {
        if( mDB == null )
        {
            open();
        }

        Cursor c = null;

        c = mDB.query(	dbName ,
                columns ,
                selection ,
                null ,
                null ,
                null ,
                null );

        return c;
    }



	/**
	 * 데이터베이스에서 데이터를 삭제한다.
	 * @param tableName
	 * @param whereClause
	 * @param whereArgs
	 */
	private int delete( String tableName , String whereClause , String[] whereArgs )
	{
		if( this.mDB == null )
		{
			this.open();
		}
		
		return mDB.delete(tableName, whereClause, whereArgs);
	}

    public synchronized List<ExerciseProgram> getExerciseProgramList()
    {
        Cursor c = null;

        c = this.query(FitmateDBOpenHelper.DB_TABLE_PROGRAM , new String[]{"*"}, null);

        List<ExerciseProgram> programList = new ArrayList<ExerciseProgram>();
        if( c.getCount() > 0 )
        {
            while( c.moveToNext() )
            {
                ExerciseProgram program = new ExerciseProgram();
                program.setId( c.getInt(c.getColumnIndex("id")) );
                program.setName( c.getString(c.getColumnIndex("name")) );
                program.setCategory(c.getInt(c.getColumnIndex("category")));
                program.setMinuteFrom(c.getInt(c.getColumnIndex("minutefrom")));
                program.setMinuteTo(c.getInt(c.getColumnIndex("minuteto")));
                program.setStrengthFrom(c.getFloat(c.getColumnIndex("strengthfrom")));
                program.setStrengthTo(c.getFloat(c.getColumnIndex("strengthto")));
                program.setTimesFrom(c.getInt(c.getColumnIndex("timesfromperweek")));
                program.setTimesTo(c.getInt(c.getColumnIndex("timestoperweek")));
                program.setInfo(c.getString(c.getColumnIndex("info")));
                program.setContinuousExercise(c.getInt(c.getColumnIndex("checkcontinuousexercise")));
                programList.add(program);
            }
        }

        c.close();

        return programList;
    }

    public synchronized List<ExerciseProgram> getExerciseProgramList( String searchWord )
    {
        Cursor c = null;

        c = this.query(FitmateDBOpenHelper.DB_TABLE_PROGRAM , new String[]{"*"}, "name='" + searchWord + "'");

        List<ExerciseProgram> programList = new ArrayList<ExerciseProgram>();
        if( c.getCount() > 0 )
        {
            while( c.moveToNext() )
            {
                ExerciseProgram program = new ExerciseProgram();
                program.setId(c.getInt(c.getColumnIndex("id")));
                program.setName(c.getString(c.getColumnIndex("name")));
                program.setCategory(c.getInt(c.getColumnIndex("category")));
                program.setMinuteFrom(c.getInt(c.getColumnIndex("minutefrom")));
                program.setMinuteTo(c.getInt(c.getColumnIndex("minuteto")));
                program.setStrengthFrom(c.getFloat(c.getColumnIndex("strengthfrom")));
                program.setStrengthTo(c.getFloat(c.getColumnIndex("strengthto")));
                program.setTimesFrom(c.getInt(c.getColumnIndex("timesfromperweek")));
                program.setTimesTo(c.getInt(c.getColumnIndex("timestoperweek")));
                program.setInfo(c.getString(c.getColumnIndex("info")));
                program.setContinuousExercise(c.getInt(c.getColumnIndex("checkcontinuousexercise")));
                boolean defaultprogram = (c.getInt(c.getColumnIndex("defaultprogram")) != 0 );
                program.setDefaultProgram(defaultprogram);

                programList.add(program);
            }
        }

        c.close();

        return programList;
    }


    public synchronized List<ExerciseProgram> getUserExerciseProgramList()
    {
        Cursor c = null;

        c = this.query(FitmateDBOpenHelper.DB_TABLE_USER_PROGRAM , new String[]{"*"}, null , null, null , null , "diseasecategory ASC");

        List<ExerciseProgram> programList = new ArrayList<ExerciseProgram>();

        if( c.getCount() > 0 )
        {
            int index = 1;
            while( c.moveToNext() )
            {
                ExerciseProgram program = new ExerciseProgram();
                program.setId( c.getInt(c.getColumnIndex("id")));
                program.setName( c.getString(c.getColumnIndex("name")));
                program.setCategory(c.getInt(c.getColumnIndex("category")));
                program.setMinuteFrom(c.getInt(c.getColumnIndex("minutefrom")));
                program.setMinuteTo(c.getInt(c.getColumnIndex("minuteto")));
                program.setStrengthFrom(c.getFloat(c.getColumnIndex("strengthfrom")));
                program.setStrengthTo(c.getFloat(c.getColumnIndex("strengthto")));
                program.setTimesFrom(c.getInt(c.getColumnIndex("timesfromperweek")));
                program.setTimesTo(c.getInt(c.getColumnIndex("timestoperweek")));
                program.setInfo(c.getString(c.getColumnIndex("info")));
                program.setContinuousExercise(c.getInt(c.getColumnIndex("checkcontinuousexercise")));
                boolean isApplied = (c.getInt(c.getColumnIndex("isapplied")) != 0 );
                program.setApplied(isApplied);
                program.setDiseaseCategory(c.getInt(c.getColumnIndex("diseasecategory")));
                boolean defaultprogram = (c.getInt(c.getColumnIndex("defaultprogram")) != 0 );
                program.setDefaultProgram(defaultprogram);

//                if( isApplied ){
//                    programList.add( 0 , program );
//                }else{
//                    programList.add( index ,program );
//                    index++;
//                }

                programList.add(program);
            }
        }

        c.close();

        return programList;
    }

    public synchronized List<ExerciseProgram> getUserExerciseProgramList(String searchWord)
    {
        Cursor c = null;

        c = this.query(FitmateDBOpenHelper.DB_TABLE_USER_PROGRAM, new String[]{"*"} , null);

        List<ExerciseProgram> programList = new ArrayList<ExerciseProgram>();
        if( c.getCount() > 0 )
        {
            while( c.moveToNext() )
            {
                int resId =  mContext.getResources().getIdentifier( c.getString(c.getColumnIndex("name")) , "string", mContext.getPackageName() );
                String programName = mContext.getString( resId );

                if( programName.contains( searchWord ) )
                {
                    ExerciseProgram program = new ExerciseProgram();
                    program.setId(c.getInt(c.getColumnIndex("id")));
                    program.setName(c.getString(c.getColumnIndex("name")));
                    program.setCategory(c.getInt(c.getColumnIndex("category")));
                    program.setMinuteFrom(c.getInt(c.getColumnIndex("minutefrom")));
                    program.setMinuteTo(c.getInt(c.getColumnIndex("minuteto")));
                    program.setStrengthFrom(c.getFloat(c.getColumnIndex("strengthfrom")));
                    program.setStrengthTo(c.getFloat(c.getColumnIndex("strengthto")));
                    program.setTimesFrom(c.getInt(c.getColumnIndex("timesfromperweek")));
                    program.setTimesTo(c.getInt(c.getColumnIndex("timestoperweek")));
                    program.setInfo(c.getString(c.getColumnIndex("info")));
                    program.setContinuousExercise(c.getInt(c.getColumnIndex("checkcontinuousexercise")));
                    boolean isApplied = (c.getInt(c.getColumnIndex("isapplied")) != 0);
                    program.setApplied(isApplied);
                    program.setDiseaseCategory(c.getInt(c.getColumnIndex("diseasecategory")));
                    program.setDefaultProgram( (c.getInt(c.getColumnIndex("defaultprogram")) != 0) );
                    programList.add(program);
                }
            }//end while
        }

        c.close();

        return programList;
    }



    public synchronized ExerciseProgram getUserExerciProgram( int id )
    {

        Cursor c = null;

        c = query(FitmateDBOpenHelper.DB_TABLE_USER_PROGRAM, new String[]{"*"}, "id=" + id);

        ExerciseProgram program = null;

        if( c.getCount() > 0 )
        {
            c.moveToFirst();

            program = new ExerciseProgram();
            program.setId(c.getInt(c.getColumnIndex("id")));
            program.setName(c.getString(c.getColumnIndex("name")));
            program.setCategory(c.getInt(c.getColumnIndex("category")));
            program.setMinuteFrom(c.getInt(c.getColumnIndex("minutefrom")));
            program.setMinuteTo(c.getInt(c.getColumnIndex("minuteto")));
            program.setStrengthFrom(c.getFloat(c.getColumnIndex("strengthfrom")));
            program.setStrengthTo(c.getFloat(c.getColumnIndex("strengthto")));
            program.setTimesFrom(c.getInt(c.getColumnIndex("timesfromperweek")));
            program.setTimesTo(c.getInt(c.getColumnIndex("timestoperweek")));
            program.setInfo(c.getString(c.getColumnIndex("info")));
            program.setContinuousExercise(c.getInt(c.getColumnIndex("checkcontinuousexercise")));
            boolean isApplied = (c.getInt(c.getColumnIndex("isapplied")) != 0 );
            program.setApplied(isApplied);
            boolean defaultprogram = (c.getInt(c.getColumnIndex("defaultprogram")) != 0 );
            program.setDefaultProgram(defaultprogram);

        }

        c.close();

        return program;

    }

	
	public synchronized boolean setUserProgram( ExerciseProgram program )
    {

        ContentValues updateValues  = new ContentValues();
        updateValues.put("isapplied",0);
        this.update(FitmateDBOpenHelper.DB_TABLE_USER_PROGRAM , updateValues , null , null );

        boolean result =false;

        ContentValues addRowValue = new ContentValues();
        addRowValue.put( "name", program.getName() );
        addRowValue.put( "category" , program.getCategory() );
        addRowValue.put( "strengthfrom", program.getStrengthFrom() );
        addRowValue.put( "strengthto", program.getStrengthTo() );
        addRowValue.put( "timesfromperweek", program.getTimesFrom() );
        addRowValue.put( "timestoperweek", program.getTimesTo() );
        addRowValue.put( "minutefrom", program.getMinuteFrom() );
        addRowValue.put( "minuteto", program.getMinuteTo() );
        addRowValue.put( "checkcontinuousexercise", program.getContinuousExercise() );
        addRowValue.put( "info", program.getInfo() );
        addRowValue.put( "recommendnumber", program.getRecommendNumber() );
        addRowValue.put( "isapplied" , program.isApplied()? 1: 0 );

        long rowID = insert( FitmateDBOpenHelper.DB_TABLE_USER_PROGRAM , addRowValue );

        if( rowID > 0 ) result = true;

        return result;
    }

    public synchronized ExerciseProgram getAppliedProgram() {
        Cursor c = null;

        c = query(FitmateDBOpenHelper.DB_TABLE_USER_PROGRAM , new String[]{"*"} , "isapplied=1");

        ExerciseProgram program = null;

        if( c.getCount() > 0 )
        {
            c.moveToFirst();

            program = new ExerciseProgram();
            program.setId( c.getInt(c.getColumnIndex("id")) );
            program.setName( c.getString(c.getColumnIndex("name")) );
            program.setCategory(c.getInt(c.getColumnIndex("category")));
            program.setMinuteFrom(c.getInt(c.getColumnIndex("minutefrom")));
            program.setMinuteTo(c.getInt(c.getColumnIndex("minuteto")));
            program.setStrengthFrom(c.getFloat(c.getColumnIndex("strengthfrom")));
            program.setStrengthTo(c.getFloat(c.getColumnIndex("strengthto")));
            program.setTimesFrom(c.getInt(c.getColumnIndex("timesfromperweek")));
            program.setTimesTo(c.getInt(c.getColumnIndex("timestoperweek")));
            program.setInfo(c.getString(c.getColumnIndex("info")));
            program.setContinuousExercise(c.getInt(c.getColumnIndex("checkcontinuousexercise")));
            boolean defaultprogram = (c.getInt(c.getColumnIndex("defaultprogram")) != 0 );
            program.setDefaultProgram( defaultprogram );

        }

        c.close();

        return program;
    }

    public synchronized boolean modifyExerciseProgram(ExerciseProgram program) {
        boolean result =false;

        ContentValues updateValues = new ContentValues();
        updateValues.put("name", program.getName());
        updateValues.put("category", program.getCategory());
        updateValues.put("strengthfrom", program.getStrengthFrom());
        updateValues.put("strengthto", program.getStrengthTo());
        updateValues.put("timesfromperweek", program.getTimesFrom());
        updateValues.put("timestoperweek", program.getTimesTo());
        updateValues.put("minutefrom", program.getMinuteFrom());
        updateValues.put("minuteto", program.getMinuteTo());
        updateValues.put("checkcontinuousexercise", program.getContinuousExercise());
        updateValues.put("info", program.getInfo());
        updateValues.put("recommendnumber", program.getRecommendNumber());
        updateValues.put("isapplied", program.isApplied() ? 1 : 0);

        long rowID = this.update(FitmateDBOpenHelper.DB_TABLE_USER_PROGRAM , updateValues , "id=" + program.getId() , null );

        if( rowID > 0 ) result = true;

        return result;
    }

    public synchronized boolean modifyUserInfo(UserInfo userInfo) {
        boolean result =false;

        ContentValues updateValues = new ContentValues();
        updateValues.put("name", userInfo.getName());
        updateValues.put("email", userInfo.getEmail());
        updateValues.put("birthdate", userInfo.getBirthDate());
        updateValues.put("height", userInfo.getHeight());
        updateValues.put("weight", userInfo.getWeight());
        updateValues.put("gender", userInfo.getGender().getValue());
        updateValues.put("deviceaddress", userInfo.getDeviceAddress());
        updateValues.put("intro", userInfo.getIntro());
        updateValues.put("wearat", userInfo.getWearAt());
        updateValues.put( "profileimg", userInfo.getAccountPhoto());
        updateValues.put( "issi", (userInfo.isSI() ? 1 : 0 ) );

        long rowID = this.update(FitmateDBOpenHelper.DB_TABLE_USERINFO , updateValues , null , null );

        if( rowID > 0 ) result = true;

        return result;
    }

    public synchronized UserInfo getUserInfo(  ){

        Cursor c = null;

        c = query(FitmateDBOpenHelper.DB_TABLE_USERINFO , new String[]{"*"} , null );

        UserInfo userInfo = null;

        if( c.getCount() > 0 )
        {
            c.moveToNext();
            userInfo= new UserInfo();

            userInfo.setName(c.getString(c.getColumnIndex("name")));
            userInfo.setEmail(c.getString(c.getColumnIndex("email")));
            userInfo.setPassword(c.getString(c.getColumnIndex("password")));
            userInfo.setBirthDate(c.getLong(c.getColumnIndex("birthdate")));
            userInfo.setHeight(c.getFloat(c.getColumnIndex("height")));
            userInfo.setWeight(c.getFloat(c.getColumnIndex("weight")));
            userInfo.setGender( GenderType.getGenderType(c.getInt(c.getColumnIndex("gender"))) );
            userInfo.setIsSI((c.getInt(c.getColumnIndex("issi")) != 0));
            userInfo.setMaxMet(c.getDouble(c.getColumnIndex("maxmet")));
            userInfo.setIsVO2Max(c.getInt(c.getColumnIndex("isvo2max")) != 0);
            userInfo.setIntro(c.getString(c.getColumnIndex("intro")));
            userInfo.setDeviceAddress(c.getString(c.getColumnIndex("deviceaddress")));
            userInfo.setWearAt(c.getInt(c.getColumnIndex("wearat")));
            userInfo.setIncentiveUser(c.getInt(c.getColumnIndex("isincentiveuser")) != 0);
            userInfo.setLastDataDate(c.getLong(c.getColumnIndex("lastdatadate")));
            userInfo.setAccountPhoto( c.getString(c.getColumnIndex( "profileimg" )) );
        }

        c.close();

        return userInfo;
    }

    public synchronized boolean setLastConnectedTime(String email, Date time , String btaddress)
    {
        boolean result = false;
        ContentValues addRowValue = new ContentValues();

        addRowValue.put( "email" , email );
        addRowValue.put( "connectedtime" , time.getTime() );
        addRowValue.put( "deviceaddress", btaddress );

        Cursor c = null;

        c = query(FitmateDBOpenHelper.DB_TABLE_DEVICE_RECENT_CONNECTIONINFO , new String[]{"*"} , "email='"+email+"'" );
        long rowID = 0;
        if( c.getCount() > 0 ) {
            rowID = update( FitmateDBOpenHelper.DB_TABLE_DEVICE_RECENT_CONNECTIONINFO , addRowValue , "email='" + email + "'" , null );
        }else{
            addRowValue.put( "batteryratio", 0 );
            rowID = insert(FitmateDBOpenHelper.DB_TABLE_DEVICE_RECENT_CONNECTIONINFO, addRowValue);
        }
        if( rowID > 0 ) result = true;

        c.close();

        return result;
    }

    public synchronized boolean setLastBatteryRatio(String email, int battery, String btAddress){

        boolean result = false;
        ContentValues addRowValue = new ContentValues();

        addRowValue.put( "batteryratio", battery );

        Cursor c = null;

        c = query(FitmateDBOpenHelper.DB_TABLE_DEVICE_RECENT_CONNECTIONINFO , new String[]{"*"} , "email='"+email+"'" );
        long rowID = 0;
        if( c.getCount() > 0 ) {
            rowID = update( FitmateDBOpenHelper.DB_TABLE_DEVICE_RECENT_CONNECTIONINFO , addRowValue , "email='" + email + "'" , null );
        }else{
            addRowValue.put( "email" , email );
            addRowValue.put( "connectedtime" , Calendar.getInstance().getTimeInMillis() );
            addRowValue.put( "deviceaddress", btAddress );
            rowID = insert(FitmateDBOpenHelper.DB_TABLE_DEVICE_RECENT_CONNECTIONINFO, addRowValue);
        }
        if( rowID > 0 ) result = true;

        c.close();

        return result;
    }

    public synchronized long getLastConnectedTime( String email ){
        Cursor c = null;

        c = query(FitmateDBOpenHelper.DB_TABLE_DEVICE_RECENT_CONNECTIONINFO , new String[]{"*"} , "email='"+email+"'" );
        long connectedTime = -1;

        if( c.getCount() > 0 ) {
            c.moveToFirst();
            connectedTime = c.getLong(c.getColumnIndex("connectedtime"));
        }

        c.close();

        return connectedTime;
    }

    public synchronized void deleteRecentConnectionInfo()
    {
        delete(FitmateDBOpenHelper.DB_TABLE_DEVICE_RECENT_CONNECTIONINFO , null , null);
    }

    public synchronized int getLastBatteryRatio( String email ){
        Cursor c = null;

        c = query(FitmateDBOpenHelper.DB_TABLE_DEVICE_RECENT_CONNECTIONINFO , new String[]{"*"} , "email='"+email+"'" );
        int batteryRatio =-1;

        if( c.getCount() > 0 ) {
            c.moveToFirst();
            batteryRatio = c.getInt(c.getColumnIndex("batteryratio"));
        }

        c.close();

        return batteryRatio;
    }

    public synchronized boolean setUserInfo( UserInfo userInfo )
    {
        Cursor c = null;

        c = query(FitmateDBOpenHelper.DB_TABLE_USERINFO , new String[]{"*"} , null );

        boolean result = false;

        ContentValues addRowValue = new ContentValues();
        addRowValue.put( "name", userInfo.getName());
        addRowValue.put( "email" , userInfo.getEmail() );
        addRowValue.put( "password" , userInfo.getPassword() );
        addRowValue.put( "birthdate" ,userInfo.getBirthDate());
        addRowValue.put( "height", userInfo.getHeight() );
        addRowValue.put( "weight", userInfo.getWeight() );
        addRowValue.put( "gender" , userInfo.getGender().getValue() );
        addRowValue.put( "issi" , (userInfo.isSI() ? 1 : 0 )  );
        addRowValue.put( "maxmet" , userInfo.getMaxMet() );
        addRowValue.put( "isvo2max" , ( userInfo.isVO2Max() ? 1 : 0 ) );
        addRowValue.put( "deviceaddress", userInfo.getDeviceAddress() );
        addRowValue.put( "intro" , userInfo.getIntro());
        addRowValue.put( "wearat" , userInfo.getWearAt() );
        addRowValue.put("isincentiveuser" , (userInfo.getIncentiveUser() ? 1 : 0 ) );
        addRowValue.put("lastdatadate" , userInfo.getLastDataDate() );
        addRowValue.put("profileimg" , userInfo.getAccountPhoto());

        long rowID = 0;
        if( c.getCount() > 0 ) {
            rowID = update( FitmateDBOpenHelper.DB_TABLE_USERINFO , addRowValue , "email='" + userInfo.getEmail() + "'" , null );
        }else{
            rowID = insert(FitmateDBOpenHelper.DB_TABLE_USERINFO, addRowValue);
        }

        if( rowID > 0 ) result = true;

        c.close();

        return result;
    }

    public synchronized boolean setUserAccount( UserInfo userInfo )
    {
        Cursor c = null;

        c = query(FitmateDBOpenHelper.DB_TABLE_USERINFO , new String[]{"*"} , null );

        boolean result = false;

        ContentValues addRowValue = new ContentValues();
        addRowValue.put( "name", userInfo.getName());
        addRowValue.put( "email" , userInfo.getEmail() );
        addRowValue.put( "password" , userInfo.getPassword() );
        addRowValue.put( "intro" , userInfo.getIntro());
        addRowValue.put("lastdatadate" , userInfo.getLastDataDate() );
        addRowValue.put("profileimg" , userInfo.getAccountPhoto());

        long rowID = 0;
        if( c.getCount() > 0 ) {
            rowID = update( FitmateDBOpenHelper.DB_TABLE_USERINFO , addRowValue , "email='" + userInfo.getEmail() + "'" , null );
        }else{
            rowID = insert( FitmateDBOpenHelper.DB_TABLE_USERINFO, addRowValue);
        }

        if( rowID > 0 ) result = true;

        c.close();

        return result;
    }


    public synchronized boolean setAppliedExerciseProgram( ExerciseProgram program)
    {
        Cursor c = null;

        c = this.query(FitmateDBOpenHelper.DB_TABLE_USER_PROGRAM, new String[]{"*"}, "id=" + program.getId());

        ContentValues updateValues  = new ContentValues();
        boolean result =false;
        long rowID = 0;

        ContentValues values = new ContentValues();
        values.put("name", program.getName() );
        values.put("category", program.getCategory() );
        values.put("strengthfrom", program.getStrengthFrom());
        values.put("strengthto", program.getStrengthTo() );
        values.put("timesfromperweek", program.getTimesFrom() );
        values.put("timestoperweek", program.getTimesTo() );
        values.put("minutefrom", program.getMinuteFrom() );
        values.put("minuteto", program.getMinuteTo() );
        values.put("checkcontinuousexercise", program.getContinuousExercise());
        values.put("info", program.getInfo());
        values.put("recommendnumber", program.getRecommendNumber() );
        values.put("isapplied", 1);

        ContentValues resetValues = new ContentValues();
        resetValues.put( "isapplied" , 0 );
        this.update(FitmateDBOpenHelper.DB_TABLE_USER_PROGRAM, resetValues, null, null);
        rowID = this.update( FitmateDBOpenHelper.DB_TABLE_USER_PROGRAM , values , "id=" + program.getId()  , null);

        if(rowID > 0){ result = true; }

        c.close();

        return result;
    }

    public synchronized boolean setAppliedExerciseProgram( int exerciseProgramID )
    {

        boolean result = false;
        long rowID = 0;

        ContentValues resetValues = new ContentValues();
        resetValues.put( "isapplied" , 0 );
        this.update(FitmateDBOpenHelper.DB_TABLE_USER_PROGRAM , resetValues , null, null );

        ContentValues values = new ContentValues();
        values.put("isapplied", 1);
        rowID = this.update( FitmateDBOpenHelper.DB_TABLE_USER_PROGRAM , values , "id=" + exerciseProgramID  , null);

        if(rowID > 0){ result = true; }

        return result;
    }

    public synchronized com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseProgram getExerciseProgramForAnalyzer( int programid )
    {
        Cursor c = null;

        c = query(FitmateDBOpenHelper.DB_TABLE_USER_PROGRAM, new String[]{"*"}, "id="+programid);

        com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseProgram program = null;

        if( c.getCount() > 0 )
        {
            c.moveToFirst();

            program = new com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseProgram(
                    c.getString(c.getColumnIndex("name")) ,
                    StrengthInputType.getStrengthType(c.getInt(c.getColumnIndex("category"))),
                    c.getFloat(c.getColumnIndex("strengthfrom")) ,
                    c.getFloat(c.getColumnIndex("strengthto")) ,
                    c.getInt(c.getColumnIndex("timesfromperweek")) ,
                    c.getInt(c.getColumnIndex("timestoperweek")) ,
                    c.getInt(c.getColumnIndex("minutefrom")) ,
                    c.getInt(c.getColumnIndex("minuteto")) ,
                    ( c.getInt(c.getColumnIndex("checkcontinuousexercise")) != 0) ,
                    false,1,1 );
        }

        c.close();

        return program;
    }


    public synchronized com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseProgram getAppliedExerciseProgram(  )
    {
        Cursor c = null;

        c = query(FitmateDBOpenHelper.DB_TABLE_USER_PROGRAM , new String[]{"*"} , "isapplied=1");

        com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseProgram program = null;

        if( c.getCount() > 0 )
        {
            c.moveToFirst();

            program = new com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseProgram(
                    c.getString(c.getColumnIndex("name")) ,
                    StrengthInputType.getStrengthType(c.getInt(c.getColumnIndex("category"))),
                    c.getFloat(c.getColumnIndex("strengthfrom")) ,
                    c.getFloat(c.getColumnIndex("strengthto")) ,
                    c.getInt(c.getColumnIndex("timesfromperweek")) ,
                    c.getInt(c.getColumnIndex("timestoperweek")) ,
                    c.getInt(c.getColumnIndex("minutefrom")) ,
                    c.getInt(c.getColumnIndex("minuteto")) ,
                    ( c.getInt(c.getColumnIndex("checkcontinuousexercise")) != 0) ,
                    false,1,1 );
        }

        c.close();

        return program;
    }



    public synchronized DayActivity getDayActivity(String activityDate) {

        Cursor c = null;

        c = query(FitmateDBOpenHelper.DB_TABLE_DAY_ACTIVITY , new String[]{"*"} , "activitydate='" + activityDate + "'" );

        DayActivity dayActivity = null;

        if( c.getCount() > 0 )
        {
            c.moveToFirst();

            TimeInfo startTime = new TimeInfo( c.getString( c.getColumnIndex("starttime") ) );
            int[] arrHMLTime = new int[4];
            arrHMLTime[0] = c.getInt( c.getColumnIndex("strength_underlow"));
            arrHMLTime[1] = c.getInt( c.getColumnIndex("strength_low"));
            arrHMLTime[2] = c.getInt( c.getColumnIndex("strength_medium"));
            arrHMLTime[3] = c.getInt( c.getColumnIndex("strength_high"));

            dayActivity = new DayActivity( );
            dayActivity.setStartTime(startTime);
            dayActivity.setCalorieByActivity(c.getInt(c.getColumnIndex("caloriebyactivity")));
            dayActivity.setHmlStrengthTime(arrHMLTime);
            dayActivity.setDataSaveInterval(c.getInt(c.getColumnIndex("datasavinginterval")));
            dayActivity.setAchieveOfTarget(c.getInt(c.getColumnIndex("achieveoftarget")));
            dayActivity.setAverageMET( c.getFloat(c.getColumnIndex("averagemet")));

            byte[] byteMetArray = c.getBlob(c.getColumnIndex("metarray"));
            float[] metArray = null;
            if( byteMetArray != null ){
                metArray = DayActivity.convertByteArrayToFloatArray(byteMetArray);
            }
            dayActivity.setTodayScore(c.getInt(c.getColumnIndex("todayscore")));
            dayActivity.setPredayScore(c.getInt(c.getColumnIndex("predayscore")));
            dayActivity.setExtraScore(c.getInt(c.getColumnIndex("extrascore")));
            dayActivity.setAchieveMax((c.getInt(c.getColumnIndex("achievemax")) != 0));
            dayActivity.setPossibleMaxScore(c.getInt(c.getColumnIndex("possiblemaxscore")));
            dayActivity.setPossibleTimes(c.getInt(c.getColumnIndex("possibletimes")));
            dayActivity.setPossibleValue(c.getInt(c.getColumnIndex("possiblevalue")));
            dayActivity.setMetArray(metArray);
            dayActivity.setUploadState(c.getInt(c.getColumnIndex("uploaded")));
            dayActivity.setFat(c.getFloat(c.getColumnIndex("fat")));
            dayActivity.setCarbohydrate( c.getFloat( c.getColumnIndex("carbohydrate") ));
            dayActivity.setDistance( c.getDouble( c.getColumnIndex( "distance" ) ) );
        }

        c.close();

        return dayActivity;
    }

    public synchronized WeekActivity getWeekActivity(String activityDate) {
        Cursor c = null;

        c = this.query(FitmateDBOpenHelper.DB_TABLE_WEEK_ACTIVITY,  new String[]{"*"} , "weekstartdate='" + activityDate + "'");

        WeekActivity weekActivity = null;

        if( c.getCount() > 0 )
        {
            c.moveToFirst();

            weekActivity = new WeekActivity();
            weekActivity.setActivityDate( c.getString(c.getColumnIndex("weekstartdate")) );
            weekActivity.setAverageCalorie(c.getInt(c.getColumnIndex("avecalorie")));
            weekActivity.setAverageMET(c.getFloat(c.getColumnIndex("avemet")));
            weekActivity.setAverageStrengthHigh(c.getInt(c.getColumnIndex("avestrength_high")));
            weekActivity.setAverageStrengthLow(c.getInt(c.getColumnIndex("avestrength_low")));
            weekActivity.setAverageStrengthMedium(c.getInt(c.getColumnIndex("avestrength_medium")));
            weekActivity.setAverageStrengthUnderLow(c.getInt(c.getColumnIndex("avestrength_underlow")));
            weekActivity.setScore(c.getInt(c.getColumnIndex("score")));
            weekActivity.setExerciseProgramID(c.getInt(c.getColumnIndex("exerciseprogramid")));
        }

        c.close();

        return weekActivity;

    }

    public synchronized List<WeekActivity> getWeekActivityList(String monthWeekStartDate, String monthWeekLastDate) {
        Cursor c = null;

        c = this.query( FitmateDBOpenHelper.DB_TABLE_WEEK_ACTIVITY ,  new String[]{"*"} , "weekstartdate >='" + monthWeekStartDate + "' AND weekstartdate <= '" + monthWeekLastDate + "'" , null, null, null, "weekstartdate ASC");

        List<WeekActivity> weekActivityList = null;

        if( c.getCount() > 0 )
        {
            weekActivityList = new ArrayList<WeekActivity>();
            while( c.moveToNext() ) {

                WeekActivity weekActivity = new WeekActivity();
                weekActivity.setActivityDate( c.getString( c.getColumnIndex("weekstartdate") ) );
                weekActivity.setAverageCalorie(c.getInt(c.getColumnIndex("avecalorie")));
                weekActivity.setAverageMET(c.getFloat(c.getColumnIndex("avemet")));
                weekActivity.setAverageStrengthHigh(c.getInt(c.getColumnIndex("avestrength_high")));
                weekActivity.setAverageStrengthLow(c.getInt(c.getColumnIndex("avestrength_low")));
                weekActivity.setAverageStrengthMedium(c.getInt(c.getColumnIndex("avestrength_medium")));
                weekActivity.setAverageStrengthUnderLow(c.getInt(c.getColumnIndex("avestrength_underlow")));
                weekActivity.setScore(c.getInt(c.getColumnIndex("score")));

                weekActivityList.add(weekActivity);
            }
        }

        c.close();

        return weekActivityList;
    }

//    public MonthActivity getMonthActivity(String monthActivityDate) {
//        Cursor c = null;
//
//        c = this.query(FitmateDBOpenHelper.DB_TABLE_MONTH_ACTIVITY, new String[]{"*"}, "monthstartdate='" + monthActivityDate + "'");
//
//        MonthActivity monthActivity = null;
//
//        if( c.getCount() > 0 )
//        {
//            c.moveToFirst();
//
//            monthActivity = new MonthActivity();
//            monthActivity.setActivityDate(monthActivityDate);
//            monthActivity.setAverageCalorie(c.getInt(c.getColumnIndex("avecalorie")));
//            monthActivity.setAverageMET(c.getInt(c.getColumnIndex("avemet")));
//            monthActivity.setAverageStrengthHigh(c.getInt(c.getColumnIndex("avestrength_high")));
//            monthActivity.setAverageStrengthLow(c.getInt(c.getColumnIndex("avestrength_medium")));
//            monthActivity.setAverageStrengthMedium(c.getInt(c.getColumnIndex("avestrength_medium")));
//            monthActivity.setAverageStrengthUnderLow( c.getInt(c.getColumnIndex("avestrength_underlow")));
//        }
//
//        c.close();
//
//        return monthActivity;
//    }

    public synchronized ResponseDayActivity getResponseDayActivity(String dayActivityDate) {

        ResponseDayActivity responseDayActivity = null;
        DayActivity dayActivity = this.getDayActivity(dayActivityDate);

        if( dayActivity != null )
        {
            responseDayActivity = new ResponseDayActivity();
            responseDayActivity.setActivityDate( dayActivityDate );
            responseDayActivity.setCalorie( dayActivity.getCalorieByActivity() );
            responseDayActivity.setStrengthHigh( dayActivity.getStrengthHigh() );
            responseDayActivity.setStrengthMedium( dayActivity.getStrengthMedium() );
            responseDayActivity.setStrengthLow( dayActivity.getStrengthLow() );
            responseDayActivity.setStrengthUnderLow( dayActivity.getStrengthUnderLow());
            //responseDayActivity.setMetArray( dayActivity.getMetByteArray() );
            responseDayActivity.setAchieveOfTarget( dayActivity.getAchieveOfTarget() );

            //주의 점수를 가져온다.
            String weekStartDate = this.getWeekStartDate(dayActivityDate);
            WeekActivity weekActivity = this.getWeekActivity( weekStartDate );
            if( weekActivity != null ) {
                responseDayActivity.setWeekScore(weekActivity.getScore());
            }
        }

        return responseDayActivity;
    }

    public synchronized ResponseWeekActivity getResponseWeekActivity(String weekStartDate)
    {
        ResponseWeekActivity responseWeekActivity = null;
        WeekActivity weekActivity = this.getWeekActivity(weekStartDate);

        if( weekActivity != null )
        {
            responseWeekActivity = new ResponseWeekActivity();
            responseWeekActivity.setActivityDate( weekStartDate );
            responseWeekActivity.setScore(weekActivity.getScore());
            responseWeekActivity.setAverageStrengthHigh(weekActivity.getAverageStrengthHigh());
            responseWeekActivity.setAverageStrengthMedium(weekActivity.getAverageStrengthMedium());
            responseWeekActivity.setAverageStrengthLow(weekActivity.getAverageStrengthLow());

            int[] arrExerciseTime = new int[7];
            Calendar weekStartCalendar = this.getWeekCalendar(weekStartDate);

            for (int index = 0 ; index < 7 ; index++ )
            {
                String dayActivityDate = this.getDateString(weekStartCalendar);
                DayActivity dayActivity = this.getDayActivity(dayActivityDate);
                if( dayActivity != null ){
                    arrExerciseTime[ index ] = dayActivity.getAchieveOfTarget();
                }

                weekStartCalendar.add(Calendar.DATE , 1);
            }

            responseWeekActivity.setExerciseTimeArray( arrExerciseTime);
        }

        return responseWeekActivity;
    }

    public synchronized ResponseMonthActivity getResponseMonthActivity( String monthActivityDate ) {
        ResponseMonthActivity responseMonthActivity = null;

        String[] splitedMonthDate = monthActivityDate.split("-");
        int year = Integer.parseInt( splitedMonthDate[0] );
        int month = Integer.parseInt( splitedMonthDate[1] );
        int day = Integer.parseInt( splitedMonthDate[2] );

        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month - 1 , day);

        //해당월의 마지막 날짜를 구하여서 몇주까지 있는지 확인한다.
        int lastMonthDay = calendar.getActualMaximum( Calendar.DATE );
        calendar.set( year , month - 1 , lastMonthDay );
        int lastWeekNumber = calendar.get( Calendar.WEEK_OF_MONTH );
        int lastDayofWeek = calendar.get( Calendar.DAY_OF_WEEK );
        String strLastDay = this.getDateString( calendar );

        int[] calorieList = new int[ lastWeekNumber ];
        int[] scoreList = new int[ lastWeekNumber ];
        int monthAchieve = 0;

        //해당월의 첫날을 구하여서 첫날이 속한 주의 첫날을 가져온다.
        calendar.set(year , month -1 , 1);
        int firstDayOfWeek = calendar.get( Calendar.DAY_OF_WEEK );

        calendar.add(Calendar.DATE , -( firstDayOfWeek - 1 ));
        String strMonthWeekStartDay = this.getDateString( calendar );

        //해당월의 주의 모든 값을 가져온다.
        List<WeekActivity> weekActivityList = this.getWeekActivityList( strMonthWeekStartDay , strLastDay );
        if( weekActivityList.size() == 0 )  return null;


        for( int i = 0 ; i < lastWeekNumber ;i++ ){

            String strCurrentWeekDate = this.getDateString( calendar );

            Iterator<WeekActivity> weekActivityIterator = weekActivityList.iterator();
            WeekActivity currentWeekActivity = null;
            while( weekActivityIterator.hasNext() ){

                WeekActivity weekActivity = weekActivityIterator.next();
                if( weekActivity.getActivityDate().equals( strCurrentWeekDate ) ){
                    currentWeekActivity = weekActivity;
                }

            }

            if( currentWeekActivity != null ){
                if( i == 0 ){
                    scoreList[i] = (( 7 - firstDayOfWeek) / 7) * currentWeekActivity.getScore();
                    calorieList[i] = (( 7 - firstDayOfWeek) / 7) * currentWeekActivity.getAverageCalorie();
                    monthAchieve += (( 7 - firstDayOfWeek) / 7) * currentWeekActivity.getScore();
                } else if( i == lastWeekNumber - 1 ){
                    scoreList[i] = (( lastDayofWeek + 1 ) / 7) * currentWeekActivity.getScore();
                    calorieList[i] = (( lastDayofWeek + 1 ) / 7) * currentWeekActivity.getAverageCalorie();
                    monthAchieve += (( lastDayofWeek + 1 ) / 7) * currentWeekActivity.getScore();
                } else{
                    scoreList[i] = currentWeekActivity.getScore();
                    calorieList[i] = currentWeekActivity.getAverageCalorie();
                    monthAchieve += currentWeekActivity.getScore();
                }
            }

            calendar.add(Calendar.DATE , 7);
        }

        monthAchieve /= lastMonthDay;

        responseMonthActivity = new ResponseMonthActivity();
        responseMonthActivity.setActivityDate( monthActivityDate );
        responseMonthActivity.setScoreArray(scoreList);
        responseMonthActivity.setCalorieArray(calorieList);
        responseMonthActivity.setAverageScore(monthAchieve);

        return responseMonthActivity;
    }

//    public ResponseMonthActivity getResponseMonthActivity( String monthActivityDate ) {
//        ResponseMonthActivity responseMonthActivity = null;
//
//        int MONTH_DATA_NUMBER = 6;
//
//        String[] splitedMonthDate = monthActivityDate.split("-");
//        int year = Integer.parseInt( splitedMonthDate[0] );
//        int month = Integer.parseInt( splitedMonthDate[1] );
//
//        Calendar calendar = Calendar.getInstance();
//        calendar.set(year, month , 1);
//        calendar.add(Calendar.MONTH, - MONTH_DATA_NUMBER);
//
//        int[] calorieList = new int[ MONTH_DATA_NUMBER ];
//        int[] scoreList = new int[ MONTH_DATA_NUMBER ];
//        String[] monthList = new String[MONTH_DATA_NUMBER];
//        int averageScore = 0;
//        int dataNumber = 0;
//
//        for( int i = 0 ; i < MONTH_DATA_NUMBER ;i++ )
//        {
//            MonthActivity monthActivity = this.getMonthActivity( this.getDateString_yyyy_MM_dd(calendar) );
//            if( monthActivity != null )
//            {
//                dataNumber ++;
//                calorieList[i] = monthActivity.getAverageCalorie();
//                scoreList[i] = monthActivity.getScore();
//                averageScore += monthActivity.getScore();
//            }
//
//            monthList[i] = this.getMonthChar( calendar.get(Calendar.MONTH)  );
//            calendar.add( Calendar.MONTH, 1 );
//        }
//
//        if( dataNumber > 0 )
//        {
//            averageScore /= dataNumber;
//            responseMonthActivity = new ResponseMonthActivity();
//            responseMonthActivity.setActivityDate( monthActivityDate );
//            responseMonthActivity.setActivityDate(monthActivityDate);
//            responseMonthActivity.setScoreArray(scoreList);
//            responseMonthActivity.setCalorieArray(calorieList);
//            responseMonthActivity.setAverageScore(averageScore);
//            responseMonthActivity.setMonthArray(monthList);
//        }
//
//        return responseMonthActivity;
//    }

    public synchronized List<DayActivity> getDayActivityList(String startDate , String endDate )
    {
        Cursor c = null;

        c = this.query(FitmateDBOpenHelper.DB_TABLE_DAY_ACTIVITY , new String[]{"*"}, "activitydate >= '" + startDate + "' AND activitydate <= '" + endDate + "'" , null, null , null , "activitydate ASC");

        List<DayActivity> dayActivityList = new ArrayList<DayActivity>();
        if( c.getCount() > 0 )
        {
            while( c.moveToNext() )
            {
                TimeInfo startTime = new TimeInfo( c.getString( c.getColumnIndex("starttime") ) );
                int[] arrHMLTime = new int[4];
                arrHMLTime[0] = c.getInt( c.getColumnIndex("strength_underlow"));
                arrHMLTime[1] = c.getInt( c.getColumnIndex("strength_low"));
                arrHMLTime[2] = c.getInt( c.getColumnIndex("strength_medium"));
                arrHMLTime[3] = c.getInt( c.getColumnIndex("strength_high"));

                //METArray를 계산한다. TODO 수정해야함.
                byte[] byteMetArray = c.getBlob(c.getColumnIndex("metarray"));
                float[] metArray = null;
                if( byteMetArray != null ){
                    metArray = DayActivity.convertByteArrayToFloatArray(byteMetArray);
                }

                DayActivity dayActivity = new DayActivity( );
                dayActivity.setStartTime(startTime);
                dayActivity.setCalorieByActivity(c.getInt(c.getColumnIndex("caloriebyactivity")));
                dayActivity.setHmlStrengthTime(arrHMLTime);
                dayActivity.setMetArray(metArray);
                dayActivity.setDataSaveInterval(c.getInt(c.getColumnIndex("datasavinginterval")));
                dayActivity.setAchieveOfTarget(c.getInt(c.getColumnIndex("achieveoftarget")));
                dayActivity.setAverageMET(c.getFloat(c.getColumnIndex("averagemet")));
                dayActivity.setTodayScore(c.getInt(c.getColumnIndex("todayscore")));
                dayActivity.setPredayScore(c.getInt(c.getColumnIndex("predayscore")));
                dayActivity.setExtraScore(c.getInt(c.getColumnIndex("extrascore")));
                dayActivity.setAchieveMax((c.getInt(c.getColumnIndex("achievemax")) != 0));
                dayActivity.setPossibleMaxScore(c.getInt(c.getColumnIndex("possiblemaxscore")));
                dayActivity.setPossibleTimes(c.getInt(c.getColumnIndex("possibletimes")));
                dayActivity.setPossibleValue(c.getInt(c.getColumnIndex("possiblevalue")));
                dayActivity.setFat(c.getFloat(c.getColumnIndex("fat")));
                dayActivity.setCarbohydrate(c.getFloat(c.getColumnIndex("carbohydrate")));
                dayActivity.setDistance( c.getDouble( c.getColumnIndex( "distance" ) ) );
                dayActivityList.add(dayActivity);
            }
        }

        c.close();

        return dayActivityList;

    }


//    public synchronized void updateDayActivity(DayActivity dayActivity){
//        updateDayActivity(dayActivity.getActivityDate(), dayActivity.getCalorieByActivity(), dayActivity.getStrengthUnderLow(), dayActivity.getStrengthLow(), dayActivity.getStrengthMedium(), dayActivity.getStrengthHigh(), dayActivity.getMetArray(), dayActivity.getAchieveOfTarget(), dayActivity.getAverageMET(), dayActivity.getStartTime().getTimeString(), dayActivity.getDataSaveInterval(), dayActivity.getPredayScore(), dayActivity.getTodayScore(), dayActivity.getExtraScore(), dayActivity.isAchieveMax(), dayActivity.getPossibleMaxScore(), dayActivity.getPossibleTimes(), dayActivity.getPossibleValue(), ContinuousCheckPolicy.Loosely_SumOverXExercise);
//    }


    public synchronized void updateDayActivity( DayActivity dayActivity )
    {
        Cursor c = null;

        c = query(FitmateDBOpenHelper.DB_TABLE_DAY_ACTIVITY, new String[]{"*"}, "activitydate='" + dayActivity.getActivityDate() + "'");

        ContentValues values = new ContentValues();
        values.put("caloriebyactivity", dayActivity.getCalorieByActivity());
        values.put("strength_high", dayActivity.getStrengthHigh());
        values.put("strength_medium", dayActivity.getStrengthMedium() );
        values.put("strength_low", dayActivity.getStrengthLow());
        values.put("strength_underlow", dayActivity.getStrengthUnderLow() );
        values.put("achieveoftarget", dayActivity.getAchieveOfTarget());
        values.put("fat" , dayActivity.getFat());
        values.put("carbohydrate" , dayActivity.getCarbohydrate() );
        values.put("distance" , dayActivity.getDistance() );
        values.put("metarray", DayActivity.convertFloatArrayToByteArray(dayActivity.getMetArray()));
        values.put("averagemet", dayActivity.getAverageMET());
        values.put("starttime", dayActivity.getStartTime().getTimeString() );
        values.put("predayscore", dayActivity.getPredayScore() );
        values.put("todayscore", dayActivity.getTodayScore() );
        values.put("extrascore" , dayActivity.getExtraScore());
        values.put("datasavinginterval", dayActivity.getDataSaveInterval());
        values.put( "achievemax" , dayActivity.isAchieveMax() ? 1 : 0 );
        values.put( "possiblemaxscore" , dayActivity.getPossibleMaxScore() );
        values.put("possibletimes", dayActivity.getPossibleTimes());
        values.put("possiblevalue", dayActivity.getPossibleValue());
        values.put("fat",dayActivity.getFat());
        values.put("carbohydrate" , dayActivity.getCarbohydrate() );
        values.put( "distance" , dayActivity.getDistance() );
        values.put("uploaded", NOT_UPLOADED);

        Log.d(TAG , "UPDATE DAY ACTIVITY");
        Log.d(TAG , "ACTIVITYDATE = " + dayActivity.getActivityDate() );
        Log.d(TAG , "CALORIE = " + dayActivity.getCalorieByActivity() );

        long result = 0;
        if( c.getCount() > 0 ) {
            result = this.update(FitmateDBOpenHelper.DB_TABLE_DAY_ACTIVITY, values, "activitydate='" + dayActivity.getActivityDate() + "'", null);
        }else{
            values.put("activitydate" , dayActivity.getActivityDate() );
            result = this.insert(FitmateDBOpenHelper.DB_TABLE_DAY_ACTIVITY , values);
        }

        c.close();
    }

    public synchronized void updateDayActivity_Mergy( String activityDate, int calorie, int underLowTime, int lowTime, int mediumTime, int highTime, float[] addMetArray, int achieveOfTarget, float averageMet , String startTime  ,int dataSaveInterval , int predayScore , int todayScore , int extraScore , boolean isAchieveMax , int possibleMaxScore , int possibleTimes , int possibleValue , ContinuousCheckPolicy policy)
    {
        Cursor c = null;

        c = query(FitmateDBOpenHelper.DB_TABLE_DAY_ACTIVITY, new String[]{"*"}, "activitydate='" + activityDate + "'");

        ContentValues values = new ContentValues();
        values.put("caloriebyactivity", calorie);
        values.put("strength_high", highTime);
        values.put("strength_medium", mediumTime);
        values.put("strength_low", lowTime);
        values.put("strength_underlow", underLowTime);
        values.put("achieveoftarget", achieveOfTarget);
        values.put("metarray", DayActivity.convertFloatArrayToByteArray(addMetArray));
        values.put("averagemet", averageMet);
        values.put("starttime", startTime);
        values.put("predayscore", predayScore );
        values.put("todayscore", todayScore );
        values.put("extrascore" , extraScore);
        values.put("datasavinginterval", dataSaveInterval);
        values.put( "achievemax" , isAchieveMax ? 1 : 0 );
        values.put( "possiblemaxscore" , possibleMaxScore );
        values.put("possibletimes" , possibleTimes);
        values.put("possiblevalue" , possibleValue);
        values.put("uploaded" , NEED_MERGY);

        long result = 0;
        if( c.getCount() > 0 ) {
            result = this.update(FitmateDBOpenHelper.DB_TABLE_DAY_ACTIVITY, values, "activitydate='" + activityDate + "'", null);
        }else{
            values.put("activitydate" , activityDate);
            result = this.insert(FitmateDBOpenHelper.DB_TABLE_DAY_ACTIVITY , values);
        }

        Log.d(TAG , "UPDATE DAY ACTIVITY");
        Log.d(TAG , "RESULT = " + result);
        Log.d(TAG , "ACTIVITYDATE = " + activityDate );
        Log.d(TAG , "CALORIE = " + calorie );
        Log.d(TAG , "STERENGTH_HIGH = " + highTime);
        Log.d(TAG , "STERENGTH_MEDIUM = " + mediumTime);
        Log.d(TAG , "STERENGTH_LOW = " + lowTime);
        Log.d(TAG , "STERENGTH_UNDERLOW = " + underLowTime);
        Log.d(TAG , "ACHIEVEOFTARGET = " + achieveOfTarget);
        Log.d(TAG , "AVERAGEMET = " + averageMet);
        Log.d(TAG , "STARTTIME = " + startTime);
        Log.d(TAG , "PREDAYSCORE = " + predayScore);
        Log.d(TAG , "TODAYSCORE = " + todayScore);
        Log.d(TAG , "ISACHIEVEMAX = " + isAchieveMax);
        Log.d(TAG , "POSSIBLEMAXSCORE = " + possibleMaxScore);
        Log.d(TAG , "POSSIBLETIMES = " + possibleTimes);
        Log.d(TAG , "POSSIBLEVALUE = " + possibleValue);
        Log.d(TAG , "DATASAVINGINTERVAL = " + dataSaveInterval );

        c.close();
    }

    public synchronized  void updateWeekActivity(WeekActivity weekActivity){
        updateWeekActivity(weekActivity.getActivityDate(), weekActivity.getAverageCalorie(), weekActivity.getScore(), weekActivity.getAverageStrengthHigh(), weekActivity.getAverageStrengthMedium(), weekActivity.getAverageStrengthLow(), weekActivity.getAverageStrengthUnderLow(), weekActivity.getAverageMET(), weekActivity.getExerciseProgramID());
    }
    public synchronized void updateWeekActivity(String weekStartDate, int addCalorie, int addAchieve, int addAverageHighTime, int addAverageMediumTime, int addAverageLowTime, int addAverageUnderLowTime , float addAverageMet , int appliedExerciseProgramID) {

        Cursor c = null;
        c = this.query(FitmateDBOpenHelper.DB_TABLE_WEEK_ACTIVITY,  new String[]{"*"} , "weekstartdate='" + weekStartDate + "'");

        ContentValues values = new ContentValues();
        values.put("avecalorie", addCalorie);
        values.put("avestrength_high", addAverageHighTime);
        values.put("avestrength_medium", addAverageMediumTime);
        values.put("avestrength_low", addAverageLowTime);
        values.put("avestrength_underlow", addAverageUnderLowTime);
        values.put("score" ,  addAchieve);
        values.put("avemet" , addAverageMet);
        values.put("uploaded" , NOT_UPLOADED);
        values.put("exerciseprogramid", appliedExerciseProgramID);

        long result = 0;
        if( c.getCount() > 0 ) {
            result = this.update(FitmateDBOpenHelper.DB_TABLE_WEEK_ACTIVITY, values, "weekstartdate='" + weekStartDate + "'", null);
        }else{
            values.put("weekstartdate" , weekStartDate);
            result = this.insert(FitmateDBOpenHelper.DB_TABLE_WEEK_ACTIVITY , values);
        }

        c.close();

    }

    public synchronized void updateDeviceAddress(String deviceAdrress, String email) {

        ContentValues values = new ContentValues();
        values.put("deviceaddress", deviceAdrress);

        long rowID = this.update( FitmateDBOpenHelper.DB_TABLE_USERINFO , values , "email='" + email + "'" , null);

    }


    private void addMetArray( float[] metArray , float[] addMetArray )
    {
//        for( int i = 0 ; i < ( metArray.length / 2) ;i++ )
//        {
//            metArray[ 2 * i + 1 ] += addMetArray[2 * i + 1];
//            metArray[ 2 * i ] += addMetArray[2 * i];
//
//            int tempMetInt = (byte)Math.floor(metArray[ 2 * 1 + 1 ]);
//            metArray[ 2 * i + 1 ] = (byte) ((byte) Math.round( ( tempMetInt - metArray[ 2 * i +1 ] ) * 100 ) & 0xFF);
//            metArray[ 2 * i ] += tempMetInt;
//        }

        for( int i = 0 ; i < metArray.length ;i++ ){
            metArray[i] += addMetArray[i];
        }
    }


    private Calendar getWeekCalendar( String activityDate )
    {
        String[] splitedActivityDate = activityDate.split("-");

        Calendar calendar = Calendar.getInstance();
        int year = Integer.parseInt( splitedActivityDate[0] );
        int month = Integer.parseInt( splitedActivityDate[1] );
        int day = Integer.parseInt(splitedActivityDate[2]);

        calendar.set(year, month - 1, day);

        return calendar;
    }
    private String getWeekStartDate( String activityDate )
    {
        String[] splitedActivityDate = activityDate.split("-");

        Calendar calendar = Calendar.getInstance();
        int year = Integer.parseInt(splitedActivityDate[0]);
        int month = Integer.parseInt( splitedActivityDate[1] );
        int day = Integer.parseInt( splitedActivityDate[2] );

        calendar.set( year , month -1 , day );

        int weekNumber = calendar.get(Calendar.DAY_OF_WEEK);
        calendar.add(Calendar.DATE, -(weekNumber - 1));

        return this.getDateString( calendar );
    }

    private String getWeekEndDate( String activityDate )
    {
        String[] splitedActivityDate = activityDate.split("-");

        Calendar calendar = Calendar.getInstance();
        int year = Integer.parseInt(splitedActivityDate[0]);
        int month = Integer.parseInt( splitedActivityDate[1] );
        int day = Integer.parseInt( splitedActivityDate[2] );

        calendar.set( year , month -1 , day );

        int weekNumber = calendar.get(Calendar.DAY_OF_WEEK);
        calendar.add(Calendar.DATE, (7 - weekNumber));

        return this.getDateString( calendar );
    }

    private String getDateString( Calendar calendar )
    {
        return calendar.get(Calendar.YEAR) + "-" + this.addZero(calendar.get(Calendar.MONTH) + 1) + "-" + this.addZero( calendar.get( Calendar.DAY_OF_MONTH ) );
    }

    private String addZero( int calNumber )
    {
        String strNumber = String.valueOf(calNumber);
        if( strNumber.length() < 2 ) strNumber = "0" + strNumber;
        return strNumber;
    }

    private String getMonthChar( int month )
    {
        String monthString = null;
        switch( month )
        {
            case Calendar.JANUARY:
                monthString = "Jan";
                break;
            case Calendar.FEBRUARY:
                monthString = "Feb";
                break;
            case Calendar.MARCH:
                monthString = "Mar";
                break;
            case Calendar.APRIL:
                monthString = "Apr";
                break;
            case Calendar.MAY:
                monthString = "May";
                break;
            case Calendar.JUNE:
                monthString = "Jun";
                break;
            case Calendar.JULY:
                monthString = "Jul";
                break;
            case Calendar.AUGUST:
                monthString = "Aug";
                break;
            case Calendar.SEPTEMBER:
                monthString = "Sep";
                break;
            case Calendar.OCTOBER:
                monthString = "Oct";
                break;
            case Calendar.NOVEMBER:
                monthString = "Nov";
                break;
            case Calendar.DECEMBER:
                monthString = "Dec";
                break;
        }

        return monthString;
    }

    public synchronized List<WeekActivity> getNotUploadWeekActivity() {

        Cursor c = null;

        c = this.query( FitmateDBOpenHelper.DB_TABLE_WEEK_ACTIVITY ,  new String[]{"*"} , "uploaded=" + NOT_UPLOADED );

        List<WeekActivity> weekActivityList = null;

        if( c.getCount() > 0 )
        {
            weekActivityList = new ArrayList<WeekActivity>();

            while( c.moveToNext() ) {

                WeekActivity weekActivity = new WeekActivity();
                weekActivity.setActivityDate( c.getString( c.getColumnIndex("weekstartdate") ) );
                weekActivity.setAverageCalorie(c.getInt(c.getColumnIndex("avecalorie")));
                weekActivity.setAverageMET(c.getFloat(c.getColumnIndex("avemet")));
                weekActivity.setAverageStrengthHigh(c.getInt(c.getColumnIndex("avestrength_high")));
                weekActivity.setAverageStrengthLow(c.getInt(c.getColumnIndex("avestrength_low")));
                weekActivity.setAverageStrengthMedium(c.getInt(c.getColumnIndex("avestrength_medium")));
                weekActivity.setAverageStrengthUnderLow(c.getInt(c.getColumnIndex("avestrength_underlow")));
                weekActivity.setScore( c.getInt(c.getColumnIndex("score")) );
                weekActivity.setExerciseProgramID( c.getInt(c.getColumnIndex("exerciseprogramid")) );

                weekActivityList.add(weekActivity);
            }
        }

        c.close();

        return weekActivityList;
    }
    public synchronized DayActivity getNeedMergyDayActivity() {

        List<DayActivity> dayActivityList = this.getNeedMergeDayActivityList();

        if(dayActivityList!=null && dayActivityList.size() > 0)        return dayActivityList.get(0);
        else return null;
    }

    private List<DayActivity> getNeedMergeDayActivityList(){
        Cursor c = null;

        c = this.query(FitmateDBOpenHelper.DB_TABLE_DAY_ACTIVITY , new String[]{"*"}, "uploaded=" + NEED_MERGY );

        List<DayActivity> dayActivityList = new ArrayList<DayActivity>();

        if( c.getCount() > 0 )
        {
            while( c.moveToNext() )
            {
                TimeInfo startTime = new TimeInfo( c.getString( c.getColumnIndex("starttime") ) );
                int[] arrHMLTime = new int[4];
                arrHMLTime[0] = c.getInt( c.getColumnIndex("strength_underlow"));
                arrHMLTime[1] = c.getInt( c.getColumnIndex("strength_low"));
                arrHMLTime[2] = c.getInt( c.getColumnIndex("strength_medium"));
                arrHMLTime[3] = c.getInt( c.getColumnIndex("strength_high"));

                //METArray를 계산한다. TODO 수정해야함.
                byte[] byteMetArray = c.getBlob(c.getColumnIndex("metarray"));
                float[] metArray = null;
                if( byteMetArray != null ){
                    metArray = DayActivity.convertByteArrayToFloatArray(byteMetArray);
                }

                DayActivity dayActivity = new DayActivity( );
                dayActivity.setStartTime(startTime);
                dayActivity.setCalorieByActivity( c.getInt(c.getColumnIndex("caloriebyactivity")) );
                dayActivity.setHmlStrengthTime( arrHMLTime );
                dayActivity.setMetArray( metArray );
                dayActivity.setDataSaveInterval( c.getInt(c.getColumnIndex("datasavinginterval")) );
                dayActivity.setAchieveOfTarget(c.getInt(c.getColumnIndex("achieveoftarget")));
                dayActivity.setAverageMET( c.getFloat(c.getColumnIndex("averagemet")));
                dayActivity.setTodayScore( c.getInt( c.getColumnIndex("todayscore") ) );
                dayActivity.setPredayScore(c.getInt(c.getColumnIndex("predayscore")));
                dayActivity.setExtraScore( c.getInt(c.getColumnIndex("extrascore")) );
                dayActivity.setAchieveMax( ( c.getInt(c.getColumnIndex("achievemax")) != 0 ) );
                dayActivity.setPossibleMaxScore( c.getInt(c.getColumnIndex("possiblemaxscore")) );
                dayActivity.setPossibleTimes( c.getInt(c.getColumnIndex("possibletimes")) );
                dayActivity.setPossibleValue(c.getInt(c.getColumnIndex("possiblevalue")));
                dayActivityList.add(dayActivity);
            }

        }

        c.close();

        return dayActivityList;
    }


    public synchronized List<DayActivity> getNotUploadDayActivity() {

        Cursor c = null;

        c = this.query(FitmateDBOpenHelper.DB_TABLE_DAY_ACTIVITY , new String[]{"*"}, "uploaded !=" + UPLOADED );

        List<DayActivity> dayActivityList = null;

        if( c.getCount() > 0 )
        {

            dayActivityList = new ArrayList<DayActivity>();

            while( c.moveToNext() )
            {
                TimeInfo startTime = new TimeInfo( c.getString( c.getColumnIndex("starttime") ) );
                int[] arrHMLTime = new int[4];
                arrHMLTime[0] = c.getInt( c.getColumnIndex("strength_underlow"));
                arrHMLTime[1] = c.getInt( c.getColumnIndex("strength_low"));
                arrHMLTime[2] = c.getInt( c.getColumnIndex("strength_medium"));
                arrHMLTime[3] = c.getInt( c.getColumnIndex("strength_high"));

                //METArray를 계산한다. TODO 수정해야함.
                byte[] byteMetArray = c.getBlob(c.getColumnIndex("metarray"));
                float[] metArray = null;
                if( byteMetArray != null ){
                    metArray = DayActivity.convertByteArrayToFloatArray(byteMetArray);
                }

                DayActivity dayActivity = new DayActivity( );
                dayActivity.setStartTime(startTime);
                dayActivity.setCalorieByActivity( c.getInt(c.getColumnIndex("caloriebyactivity")) );
                dayActivity.setHmlStrengthTime( arrHMLTime );
                dayActivity.setMetArray( metArray );
                dayActivity.setDataSaveInterval( c.getInt(c.getColumnIndex("datasavinginterval")) );
                dayActivity.setAchieveOfTarget(c.getInt(c.getColumnIndex("achieveoftarget")));
                dayActivity.setAverageMET( c.getFloat(c.getColumnIndex("averagemet")));
                dayActivity.setTodayScore( c.getInt( c.getColumnIndex("todayscore") ) );
                dayActivity.setPredayScore(c.getInt(c.getColumnIndex("predayscore")));
                dayActivity.setExtraScore( c.getInt(c.getColumnIndex("extrascore")) );
                dayActivity.setAchieveMax( ( c.getInt(c.getColumnIndex("achievemax")) != 0 ) );
                dayActivity.setPossibleMaxScore( c.getInt(c.getColumnIndex("possiblemaxscore")) );
                dayActivity.setPossibleTimes( c.getInt(c.getColumnIndex("possibletimes")) );
                dayActivity.setPossibleValue(c.getInt(c.getColumnIndex("possiblevalue")));
                dayActivity.setFat( c.getFloat( c.getColumnIndex("fat") ) );
                dayActivity.setCarbohydrate( c.getFloat( c.getColumnIndex("carbohydrate") ) );
                dayActivity.setDistance( c.getDouble( c.getColumnIndex( "distance" ) ) );
                dayActivityList.add(dayActivity);
            }
        }

        c.close();

        return dayActivityList;
    }

    public synchronized void checkUploadDayActivityList(List<DayActivity> uploadDayActivityList) {

        //머지가 필요한 일 액티비티를 가져와서
        //머지가 필요한 날짜가 오늘이라면
        boolean todayMerge = false;
        Calendar todayCalendar = Calendar.getInstance();
        String todayDate = todayCalendar.get(Calendar.YEAR) + "-" + String.format("%02d", todayCalendar.get(Calendar.MONTH) + 1) + "-" + String.format("%02d", todayCalendar.get(Calendar.DAY_OF_MONTH));

        DayActivity needMergeActivity = this.getNeedMergyDayActivity();
        String needMergeDayDate = null;
        if (needMergeActivity == null) {
            todayMerge = false;
        } else {
            needMergeDayDate = needMergeActivity.getActivityDate();

            if (todayDate.equals(needMergeDayDate)) {
                todayMerge = true;
            }else{
                todayMerge = false;
            }
        }

        for( int i = 0 ; i < uploadDayActivityList.size() ;i++ ){

            DayActivity dayActivity = uploadDayActivityList.get(i);

            if( todayMerge && needMergeDayDate.equals( dayActivity.getActivityDate() ) ){ continue; }

            ContentValues updateValues = new ContentValues();
            updateValues.put("uploaded", UPLOADED);
            long id = this.update( FitmateDBOpenHelper.DB_TABLE_DAY_ACTIVITY , updateValues ,"activitydate='" + dayActivity.getActivityDate() + "'" , null  );
        }
    }

    public synchronized void checkUploadWeekActivityList(List<WeekActivity> uploadWeekActivityList) {
        for( int i = 0 ; i < uploadWeekActivityList.size() ;i++ ){

            WeekActivity weekActivity = uploadWeekActivityList.get(i);
            ContentValues updateValues = new ContentValues();
            updateValues.put("uploaded", UPLOADED);
            long id = this.update( FitmateDBOpenHelper.DB_TABLE_WEEK_ACTIVITY , updateValues ,"weekstartdate='" + weekActivity.getActivityDate() + "'" , null  );
        }
    }

    /*public synchronized boolean setDayActivityForMergy( String activityDate, int calorie, int datasavinginterval, int programid, float[] metarray) {
        Cursor c = null;


        c = query(FitmateDBOpenHelper.DB_TABLE_DAY_ACTIVITY_MERGY, new String[]{"*"}, "activitydate='" + activityDate + "'");

        ContentValues values = new ContentValues();
        values.put("caloriebyactivity", calorie );
        values.put("metarray", DayActivity.convertFloatArrayToByteArray(metarray) );
        values.put("exerciseprogramid", programid );
        values.put("datasavinginterval", datasavinginterval);

        long result = 0;
        if( c.getCount() > 0 ) {
            result = this.update(FitmateDBOpenHelper.DB_TABLE_DAY_ACTIVITY_MERGY, values, "activitydate='" + activityDate + "'", null);
        }else{
            values.put("activityDate", activityDate);
            result = this.insert(FitmateDBOpenHelper.DB_TABLE_DAY_ACTIVITY_MERGY , values);
        }

        if(result<=0) return false;
        return  true;


    }

    public synchronized DayActivity getDayActivityForMergy( String activityDate) {




        Cursor c = null;

        c = query(FitmateDBOpenHelper.DB_TABLE_DAY_ACTIVITY_MERGY , new String[]{"*"} , "activitydate='" + activityDate + "'" );

        DayActivity dayActivity = null;

        if( c.getCount() > 0 )
        {
            c.moveToFirst();

            TimeInfo date = new TimeInfo( c.getString( c.getColumnIndex("activityDate") ) );


            dayActivity = new DayActivity( );
            dayActivity.setStartTime(date);
            dayActivity.setCalorieByActivity(c.getInt(c.getColumnIndex("caloriebyactivity")));
            dayActivity.setDataSaveInterval(c.getInt(c.getColumnIndex("datasavinginterval")));
            dayActivity.setProgramID(c.getInt(c.getColumnIndex("exerciseprogramid")));
            byte[] byteMetArray = c.getBlob(c.getColumnIndex("metarray"));
            float[] metArray = null;
            if( byteMetArray != null ){
                metArray = DayActivity.convertByteArrayToFloatArray(byteMetArray);
            }
            dayActivity.setMetArray(metArray);
        }

        c.close();

        return dayActivity;
    }*/

    public synchronized void setDayActivity( DayActivity dayActivity ) {

        Cursor c = null;

        c = query(FitmateDBOpenHelper.DB_TABLE_DAY_ACTIVITY, new String[]{"*"}, "activitydate='" + dayActivity.getActivityDate() + "'");

        ContentValues values = new ContentValues();
        values.put("caloriebyactivity", dayActivity.getCalorieByActivity() );
        values.put("strength_high", dayActivity.getStrengthHigh() );
        values.put("strength_medium", dayActivity.getStrengthMedium() );
        values.put("strength_low", dayActivity.getStrengthLow());
        values.put("strength_underlow", dayActivity.getStrengthUnderLow() );
        values.put("achieveoftarget", dayActivity.getAchieveOfTarget());
        values.put("fat" , dayActivity.getFat());
        values.put("carbohydrate" , dayActivity.getCarbohydrate() );
        values.put("distance", dayActivity.getDistance() );
        values.put("metarray", DayActivity.convertFloatArrayToByteArray(dayActivity.getMetArray()) );
        values.put("averagemet", dayActivity.getAverageMET());
        values.put("starttime", dayActivity.getStartTime().getDateString());
        values.put("predayscore", dayActivity.getPredayScore() );
        values.put("todayscore", dayActivity.getTodayScore() );
        values.put("extrascore" , dayActivity.getExtraScore());
        values.put("datasavinginterval", dayActivity.getDataSaveInterval());
        values.put( "achievemax" , dayActivity.isAchieveMax() ? 1 : 0 );
        values.put( "possiblemaxscore" , dayActivity.getPossibleMaxScore() );
        values.put("possibletimes" , dayActivity.getPossibleTimes());
        values.put("possiblevalue" , dayActivity.getPossibleValue());
        values.put("fat",dayActivity.getFat());
        values.put("carbohydrate" , dayActivity.getCarbohydrate());
        values.put( "distance" , dayActivity.getDistance() );
        values.put("uploaded" ,NOT_UPLOADED );

        long result = 0;
        if( c.getCount() > 0 ) {
           // result = this.update(FitmateDBOpenHelper.DB_TABLE_DAY_ACTIVITY, values, "activitydate='" + dayActivity.getActivityDate() + "'", null);
        }else{
            values.put("activitydate" , dayActivity.getActivityDate());
            result = this.insert(FitmateDBOpenHelper.DB_TABLE_DAY_ACTIVITY , values);
        }

        c.close();
    }

    public synchronized void setFatAndCarbonAndDistance( String activityDate , double fat , double carbo , double distance ) {
        ContentValues values = new ContentValues();
        values.put("fat",fat);
        values.put("carbohydrate" , carbo);
        values.put( "distance" , distance );
        values.put( "uploaded", NOT_UPLOADED );
        this.update(FitmateDBOpenHelper.DB_TABLE_DAY_ACTIVITY, values, "activitydate='" + activityDate + "'", null);
    }


    public synchronized void setDayActivity_MERGY( DayActivity dayActivity )
    {

        Cursor c = null;

        c = query(FitmateDBOpenHelper.DB_TABLE_DAY_ACTIVITY, new String[]{"*"}, "activitydate='" + dayActivity.getActivityDate() + "'");

        ContentValues values = new ContentValues();
        values.put("caloriebyactivity", dayActivity.getCalorieByActivity() );
        values.put("strength_high", dayActivity.getStrengthHigh() );
        values.put("strength_medium", dayActivity.getStrengthMedium() );
        values.put("strength_low", dayActivity.getStrengthLow());
        values.put("strength_underlow", dayActivity.getStrengthUnderLow() );
        values.put("achieveoftarget", dayActivity.getAchieveOfTarget());
        values.put("metarray", DayActivity.convertFloatArrayToByteArray(dayActivity.getMetArray()) );
        values.put("averagemet", dayActivity.getAverageMET());
        values.put("starttime", dayActivity.getStartTime().getDateString());
        values.put("predayscore", dayActivity.getPredayScore() );
        values.put("todayscore", dayActivity.getTodayScore() );
        values.put("extrascore" , dayActivity.getExtraScore());
        values.put("datasavinginterval", dayActivity.getDataSaveInterval());
        values.put( "achievemax" , dayActivity.isAchieveMax() ? 1 : 0 );
        values.put( "possiblemaxscore" , dayActivity.getPossibleMaxScore() );
        values.put("possibletimes" , dayActivity.getPossibleTimes());
        values.put("possiblevalue" , dayActivity.getPossibleValue());
        values.put("uploaded" , NEED_MERGY);

        long result = 0;
        if( c.getCount() > 0 ) {
            //result = this.update(FitmateDBOpenHelper.DB_TABLE_DAY_ACTIVITY, values, "activitydate='" + dayActivity.getActivityDate() + "'", null);
        }else{
            values.put("activitydate" , dayActivity.getActivityDate());
            result = this.insert(FitmateDBOpenHelper.DB_TABLE_DAY_ACTIVITY , values);
        }

        c.close();
    }

    public synchronized void setWeekActivity(WeekActivity weekActivity, int uploadState) {

        Cursor c = null;
        c = this.query(FitmateDBOpenHelper.DB_TABLE_WEEK_ACTIVITY, new String[]{"*"}, "weekstartdate='" + weekActivity.getActivityDate() + "'");

        ContentValues values = new ContentValues();
        values.put("avecalorie", weekActivity.getAverageCalorie());
        values.put("avestrength_high", weekActivity.getAverageStrengthHigh() );
        values.put("avestrength_medium", weekActivity.getAverageStrengthMedium() );
        values.put("avestrength_low", weekActivity.getAverageStrengthLow() );
        values.put("avestrength_underlow", weekActivity.getAverageStrengthUnderLow() );
        values.put("score" ,  weekActivity.getScore() );
        values.put("avemet" , weekActivity.getAverageMET() );
        values.put("uploaded" , uploadState );
        values.put("exerciseprogramid" , weekActivity.getExerciseProgramID());

        long result = 0;
        if( c.getCount() > 0 ) {
            //result = this.update(FitmateDBOpenHelper.DB_TABLE_WEEK_ACTIVITY, values, "weekstartdate='" + weekActivity.getActivityDate() + "'", null);
        }else{
            values.put("weekstartdate" , weekActivity.getActivityDate());
            result = this.insert(FitmateDBOpenHelper.DB_TABLE_WEEK_ACTIVITY , values);
        }

        c.close();
    }

    public void deleteAllDayActivity() {
        this.delete( FitmateDBOpenHelper.DB_TABLE_DAY_ACTIVITY , null , null  );
    }

    public void deleteAllWeekActivity(){
        this.delete( FitmateDBOpenHelper.DB_TABLE_WEEK_ACTIVITY , null , null  );
    }

    public void deleteUserInfo(){
        this.delete(FitmateDBOpenHelper.DB_TABLE_USERINFO , null , null);
    }

    public boolean setPassword( String email, String password ){

        boolean result = false;
        ContentValues addRowValue = new ContentValues();
        addRowValue.put( "password" , password );

        long rowID = update( FitmateDBOpenHelper.DB_TABLE_USERINFO , addRowValue , "email='" + email + "'" , null );

        if( rowID > 0){ result = true; }

        return result;
    }

    public void setNeedMergeDate(String todayDate) {

        ContentValues updateValues = new ContentValues();
        updateValues.put( "uploaded" , NEED_MERGY );
        this.update(FitmateDBOpenHelper.DB_TABLE_DAY_ACTIVITY, updateValues, "activitydate='" + todayDate + "'", null);


    }

    public void setNotUploadData(){
        List<DayActivity> dayActivityList = this.getNeedMergeDayActivityList();

        for( int i = 0 ; i < dayActivityList.size() ;i++ ){

            DayActivity dayActivity = dayActivityList.get(i);
            ContentValues updateValues = new ContentValues();
            updateValues.put("uploaded", NOT_UPLOADED );
            long id = this.update( FitmateDBOpenHelper.DB_TABLE_DAY_ACTIVITY , updateValues ,"activitydate='" + dayActivity.getActivityDate() + "'" , null  );

        }
    }

    public void setExerciseProgramIDonWeekActivity(String weekStartDate , int exerciseProgramID)
    {
        ContentValues values = new ContentValues();
        values.put("exerciseprogramid" , exerciseProgramID );
        long result = this.update(FitmateDBOpenHelper.DB_TABLE_WEEK_ACTIVITY, values, "weekstartdate='" + weekStartDate + "'", null);
    }


    public synchronized void setUserNotiSetting( UserNotiSetting userNotiSetting ) {

        Cursor c = null;

        c = query(FitmateDBOpenHelper.DB_TABLE_USERNOTI_SETTING, new String[]{"*"}, null);

        ContentValues values = new ContentValues();
        values.put("mytodayactivitynews", userNotiSetting.isMytodayactivitynews() ? 1 : 0 );
        values.put("myweekactivitynews", userNotiSetting.isMyweekactivitynews() ? 1 : 0 );
        values.put("batteryusagenoti", userNotiSetting.isBatteryusagenoti() ? 1 : 0);
        values.put("fitmeternotusednoti", userNotiSetting.isFitmeternotusednoti() ? 1 : 0 );
        values.put("fitmeterwearnoti", userNotiSetting.isFitmeterwearnoti() ? 1 : 0);
        values.put("alarmtime", userNotiSetting.getAlarmtime() );
        values.put("friendnews", userNotiSetting.isFriendnews() ? 1 : 0);
        values.put("friendtodayactivitynews", userNotiSetting.isFriendtodayactivitynews() ? 1 : 0);
        values.put("friendweekactivitynews", userNotiSetting.isFriendweekactivitynews() ? 1 : 0);

        long result = 0;
        if( c.getCount() > 0 ) {
            result = this.update(FitmateDBOpenHelper.DB_TABLE_USERNOTI_SETTING, values, null, null);
        }else{
            result = this.insert(FitmateDBOpenHelper.DB_TABLE_USERNOTI_SETTING , values);
        }

        c.close();
    }


    public synchronized UserNotiSetting getUserNotiSetting( ) {

        Cursor c = null;

        c = this.query(FitmateDBOpenHelper.DB_TABLE_USERNOTI_SETTING, new String[]{"*"}, null);

        UserNotiSetting userNotiSetting = null;

        if( c.getCount() > 0 )
        {

            userNotiSetting = new UserNotiSetting();
            c.moveToFirst();

            userNotiSetting.setMytodayactivitynews((c.getInt(c.getColumnIndex("mytodayactivitynews")) != 0));
            userNotiSetting.setMyweekactivitynews((c.getInt(c.getColumnIndex("myweekactivitynews")) != 0));
            userNotiSetting.setBatteryusagenoti((c.getInt(c.getColumnIndex("batteryusagenoti")) != 0));
            userNotiSetting.setFitmeternotusednoti((c.getInt(c.getColumnIndex("fitmeternotusednoti")) != 0));
            userNotiSetting.setFitmeterwearnoti((c.getInt(c.getColumnIndex("fitmeterwearnoti")) != 0));
            userNotiSetting.setAlarmtime(c.getString(c.getColumnIndex("alarmtime")));
            userNotiSetting.setFriendnews((c.getInt(c.getColumnIndex("friendnews")) != 0));
            userNotiSetting.setFriendtodayactivitynews((c.getInt(c.getColumnIndex("friendtodayactivitynews")) != 0));
            userNotiSetting.setFriendweekactivitynews((c.getInt(c.getColumnIndex("friendweekactivitynews")) != 0 ) );
        }

        c.close();

        return userNotiSetting;
    }


    public synchronized void setFriendWeekActivity( String weekStartDate , FriendWeekActivity friendWeekActivity) {

        ContentValues values = new ContentValues();
        values.put("weekstartdate", weekStartDate );
        values.put("kindofdata", friendWeekActivity.getKindofdata() );
        String dayAchieves = "";
        String days = "";

        for ( int i = 0 ; i < friendWeekActivity.getListofdayachieve().size() ;i++ )
        {
            if( i == friendWeekActivity.getListofdayachieve().size() -1 ) {
                days += friendWeekActivity.getListofdayachieve().get(i).getDate();
                dayAchieves += friendWeekActivity.getListofdayachieve().get(i).getAchieve();
            }else{
                days += friendWeekActivity.getListofdayachieve().get(i).getDate() + "|";
                dayAchieves += friendWeekActivity.getListofdayachieve().get(i).getAchieve() + "|";
            }
        }

        values.put("listofdayachieve", dayAchieves );
        values.put("listofday",days);

        if( friendWeekActivity.getTargetrangeinfo() != null ) {
            values.put("rangefrom", friendWeekActivity.getTargetrangeinfo().getRangeFrom());
            values.put("rangeto", friendWeekActivity.getTargetrangeinfo().getRangeTo());
            values.put("rangeunittype", friendWeekActivity.getTargetrangeinfo().getRangeUnitType());
        }

        long result = 0;
        result = this.insert(FitmateDBOpenHelper.DB_TABLE_FRIEND_WEEK_ACTIVITY , values);
    }

    public synchronized  boolean isExistFriendWeekActivity( String weekStartDate , int kindOfData )
    {
        boolean result = false;
        Cursor c = null;
        c = this.query(FitmateDBOpenHelper.DB_TABLE_FRIEND_WEEK_ACTIVITY,  new String[]{"*"} , "weekstartdate='" + weekStartDate + "' and kindofdata=" + kindOfData);

        if( c.getCount() > 0 ){
            result = true;
        }

        c.close();
        return result;
    }

    public synchronized  void deleteFriendData()
    {
        int deleteCount = this.delete(FitmateDBOpenHelper.DB_TABLE_FRIEND_WEEK_ACTIVITY, null, null);
        Log.i("fitmate" , "삭제 갯수 : " + deleteCount);

    }

    public synchronized FriendWeekActivity getFriendWeekActivity(String activityDate , int kindOfData) {
        Cursor c = null;

        c = this.query(FitmateDBOpenHelper.DB_TABLE_FRIEND_WEEK_ACTIVITY, new String[]{"*"}, "weekstartdate='" + activityDate + "' and kindofdata=" + kindOfData);

        FriendWeekActivity weekActivity = null;

        if( c.getCount() > 0 )
        {
            c.moveToFirst();

            weekActivity = new FriendWeekActivity();
            weekActivity.setKindofdata(c.getInt(c.getColumnIndex("kindofdata")));
            List<DayAchieve> listDayOfAchieve = new ArrayList<DayAchieve>();
            String[] dayAchieveString = c.getString( c.getColumnIndex( "listofdayachieve" ) ).split("\\|");
            String[] dayString = c.getString(c.getColumnIndex("listofday")).split("\\|");

            for( int i = 0 ; i < dayAchieveString.length ;i++ ){
                DayAchieve dayAchieve = new DayAchieve();
                dayAchieve.setDate(dayString[i]);
                dayAchieve.setAchieve(dayAchieveString[i]);
                listDayOfAchieve.add(dayAchieve);
            }
            weekActivity.setListofdayachieve( listDayOfAchieve );

            TargetInfo targetInfo = new TargetInfo();
            targetInfo.setRangeFrom( c.getInt( c.getColumnIndex("rangefrom")));
            targetInfo.setRangeTo(c.getInt(c.getColumnIndex("rangeto")));
            targetInfo.setRangeUnitType(c.getInt(c.getColumnIndex("rangeunittype")));
            weekActivity.setTargetrangeinfo(targetInfo);
        }

        c.close();

        return weekActivity;

    }

}

//    public voidDayActivity( String mStartTime, int addCalorie, int addUnderLowTime, int addLowTime, int addMediumTime, int addHighTime, float[] addMetArray, int addAchieveOfTarget, int dataSaveInterval , float addAverageMet ,  ContinuousCheckPolicy policy)
//    {
//        Cursor c = null;
//        c = this.query( FitmateServiceDBOpenHelper.DB_TABLE_DAY_ACTIVITY , new String[]{"*"} , "activitydate='" + mStartTime + "'" );
//
//        Log.d(TAG, "CURSOR COUNT =" + c.getCount());
//        Log.d(TAG, "ACTIVITY DATE=" + mStartTime );
//
//        if( c.getCount() > 0 )
//        {
//            c.moveToFirst();
//
//            int calorie = c.getInt(c.getColumnIndex("caloriebyactivity"));
//            int underLowTime = c.getInt(c.getColumnIndex("strength_underlow"));
//            int lowTime = c.getInt(c.getColumnIndex("strength_low"));
//            int mediumTime = c.getInt(c.getColumnIndex("strength_medium"));
//            int highTime = c.getInt(c.getColumnIndex("strength_high"));
//            int averageMet = c.getInt( c.getColumnIndex("averagemet") );
//
//            int achieveOfTarget = c.getInt(c.getColumnIndex("achieveoftarget"));
//            byte[] preByteMetArray = c.getBlob(c.getColumnIndex("metarray"));
//
//            ContentValues updateValues = new ContentValues();
//            updateValues.put("caloriebyactivity",calorie + addCalorie);
//            updateValues.put("strength_high", highTime + addHighTime);
//            updateValues.put("strength_medium", mediumTime + addMediumTime);
//            updateValues.put("strength_low", lowTime + addLowTime);
//            updateValues.put("strength_underlow", underLowTime + addUnderLowTime);
//            updateValues.put( "averagemet" , ((averageMet + addAverageMet ) / 2) );
//            updateValues.put("starttime" , mStartTime);
//
//            if( policy.equals(ContinuousCheckPolicy.Strictly) )
//            {
//                if( achieveOfTarget < addAchieveOfTarget ) { achieveOfTarget = addAchieveOfTarget; }
//            }
//            else if( policy.equals(ContinuousCheckPolicy.Loosely_SumOverXExercise))
//            {
//                achieveOfTarget += addAchieveOfTarget;
//            }
//            updateValues.put( "achieveoftarget" , achieveOfTarget );
//
//            //metarray를 합친다.
//
//            float[] preFloatMetArray = DayActivity.convertByteArrayToFloatArray( preByteMetArray );
//            this.addMetArray( preFloatMetArray , addMetArray );
//            byte[] addedByteMetArray = DayActivity.convertFloatArrayToByteArray( preFloatMetArray );
//            updateValues.put("metarray" , addedByteMetArray );
//
//            this.update(FitmateServiceDBOpenHelper.DB_TABLE_DAY_ACTIVITY , updateValues , "activitydate='" + mStartTime + "'" , null);
//
//            Log.d(TAG , "PREVIOUS UNDERLOWTIME=" + underLowTime);
//            Log.d(TAG , "CURRENT UNDERLOWTIME=" + addUnderLowTime);
//            Log.d(TAG , "UPDATE UNDERLOWTIME =" + (addUnderLowTime + underLowTime));
//
//        }
//        else
//        {
//            ContentValues addValues = new ContentValues();
//            addValues.put("activitydate" , mStartTime);
//            addValues.put("caloriebyactivity", addCalorie);
//            addValues.put("strength_high", addHighTime);
//            addValues.put("strength_medium", addMediumTime);
//            addValues.put("strength_low", addLowTime);
//            addValues.put("strength_underlow", addUnderLowTime);
//            addValues.put("achieveoftarget", addAchieveOfTarget );
//            addValues.put("metarray", DayActivity.convertFloatArrayToByteArray( addMetArray ));
//            addValues.put("averagemet" , addAverageMet);
//            addValues.put("starttime" , mStartTime);
//            addValues.put("datasavinginterval" , dataSaveInterval);
//
//            long result = this.insert(FitmateServiceDBOpenHelper.DB_TABLE_DAY_ACTIVITY, addValues);
//            Log.d(TAG , "INSERT RESULT = " + result);
//            Log.d(TAG , "INSERT UNDERLOWTIME =" + addCalorie );
//        }
//
//        c.close();
//
//    }
//public void updateWeekActivity(String weekStartDate, int addCalorie, int addAchieve, int addAverageHighTime, int addAverageMediumTime, int addAverageLowTime, int addAverageUnderLowTime , float addAverageMet) {
//
//    Cursor c = null;
//    c = this.query( FitmateServiceDBOpenHelper.DB_TABLE_WEEK_ACTIVITY , new String[]{"*"} , "weekstartdate='" + weekStartDate + "'");
//
//    ContentValues values = new ContentValues();
//    values.put("avecalorie", addCalorie);
//    values.put("avestrength_high", addAverageHighTime);
//    values.put("avestrength_medium", addAverageMediumTime);
//    values.put("avestrength_low", addAverageLowTime);
//    values.put("avestrength_underlow", addAverageUnderLowTime);
//    values.put("score" ,  addAchieve);
//    values.put("avemet" , addAverageMet);
//
//    if( c.getCount() > 0 )
//    {
//        this.update(FitmateServiceDBOpenHelper.DB_TABLE_WEEK_ACTIVITY , values , "weekstartdate='" + weekStartDate + "'" , null);
//    }
//    else
//    {
//        values.put("weekstartdate" , weekStartDate );
//        this.insert(FitmateServiceDBOpenHelper.DB_TABLE_WEEK_ACTIVITY, values);
//    }
//}
