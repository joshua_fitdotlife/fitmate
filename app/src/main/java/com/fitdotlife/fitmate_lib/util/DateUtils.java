package com.fitdotlife.fitmate_lib.util;

/**
 * Created by fitlife.soondong on 2015-06-03.
 */
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateUtils {

    public static Date getDateFromString_yyyy_MM_dd(String dateString)
    {
        Date date = null;
        SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat( "yyyy-MM-dd", Locale.getDefault() );
        try
        {
            date =  mSimpleDateFormat.parse(dateString);
        }
        catch (ParseException e) {}

        return date;
    }
    public static Date getDateFromString_yyyyMMdd(String dateString)
    {
        Date date = null;
        SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat( "yyyyMMdd", Locale.getDefault() );
        try
        {
            date =  mSimpleDateFormat.parse(dateString);
        }
        catch (ParseException e) {}

        return date;
    }
    public static String getDateString_yyyy_MM_dd(Date date){
        String dateString = null;
        SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat( "yyyy-MM-dd", Locale.getDefault() );
        dateString =  mSimpleDateFormat.format(date);
        return dateString;
    }
    public static String getDateString_yyyyMMdd(Date date){
        String dateString = null;
        SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat( "yyyyMMdd", Locale.getDefault() );
        dateString =  mSimpleDateFormat.format(date);
        return dateString;
    }

    /**
     * <p>Checks if two dates are on the same day ignoring time.</p>
     * @param date1  the first date, not altered, not null
     * @param date2  the second date, not altered, not null
     * @return true if they represent the same day
     * @throws IllegalArgumentException if either date is <code>null</code>
     */
    public static boolean isSameDay(Date date1, Date date2) {
        if (date1 == null || date2 == null) {
            throw new IllegalArgumentException("The dates must not be null");
        }
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);
        return isSameDay(cal1, cal2);
    }

    /**
     * <p>Checks if two calendars represent the same day ignoring time.</p>
     * @param cal1  the first calendar, not altered, not null
     * @param cal2  the second calendar, not altered, not null
     * @return true if they represent the same day
     * @throws IllegalArgumentException if either calendar is <code>null</code>
     */
    public static boolean isSameDay(Calendar cal1, Calendar cal2) {
        if (cal1 == null || cal2 == null) {
            throw new IllegalArgumentException("The dates must not be null");
        }
        return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) &&
                cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR));
    }

    /**
     * <p>Checks if a date is today.</p>
     * @param date the date, not altered, not null.
     * @return true if the date is today.
     * @throws IllegalArgumentException if the date is <code>null</code>
     */
    public static boolean isToday(Date date) {
        return isSameDay(date, Calendar.getInstance().getTime());
    }

    /**
     * <p>Checks if a calendar date is today.</p>
     * @param cal  the calendar, not altered, not null
     * @return true if cal date is today
     * @throws IllegalArgumentException if the calendar is <code>null</code>
     */
    public static boolean isToday(Calendar cal) {
        return isSameDay(cal, Calendar.getInstance());
    }

    /**
     * 현재와 동일한 주인지 확인
     * @param cal 확인하려는 날자
     * @return 같은 주면 true, 아니면 false
     */
    public static boolean isThisWeek(Calendar cal)
    {
        return isSameWeeks(cal, Calendar.getInstance());

    }

    public static boolean isThisWeek( Date weekStartDate)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(weekStartDate);
        return isSameWeeks(cal, Calendar.getInstance());
    }

    /**
     * 현재와 같은 달인지 확인
     * @param cal
     * @return
     */
    public static boolean isThisMonth(Calendar cal){
        return isSameMonth(cal, Calendar.getInstance());
    }


    private static boolean isSameMonth(Calendar cal1, Calendar cal2){
        if (cal1 == null || cal2 == null) {
            throw new IllegalArgumentException("The dates must not be null");
        }
        return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) &&
                cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH));
    }


    public static boolean isSameWeeks(Calendar cal1, Calendar cal2) {
        if (cal1 == null || cal2 == null) {
            throw new IllegalArgumentException("The dates must not be null");
        }
        return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) &&
                cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                cal1.get(Calendar.WEEK_OF_YEAR) == cal2.get(Calendar.WEEK_OF_YEAR));
    }

    /**
     * <p>Checks if the first date is before the second date ignoring time.</p>
     * @param date1 the first date, not altered, not null
     * @param date2 the second date, not altered, not null
     * @return true if the first date day is before the second date day.
     * @throws IllegalArgumentException if the date is <code>null</code>
     */
    public static boolean isBeforeDay(Date date1, Date date2) {
        if (date1 == null || date2 == null) {
            throw new IllegalArgumentException("The dates must not be null");
        }
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);
        return isBeforeDay(cal1, cal2);
    }

    /**
     * <p>Checks if the first calendar date is before the second calendar date ignoring time.</p>
     * @param cal1 the first calendar, not altered, not null.
     * @param cal2 the second calendar, not altered, not null.
     * @return true if cal1 date is before cal2 date ignoring time.
     * @throws IllegalArgumentException if either of the calendars are <code>null</code>
     */
    public static boolean isBeforeDay(Calendar cal1, Calendar cal2) {
        if (cal1 == null || cal2 == null) {
            throw new IllegalArgumentException("The dates must not be null");
        }
        if (cal1.get(Calendar.ERA) < cal2.get(Calendar.ERA)) return true;
        if (cal1.get(Calendar.ERA) > cal2.get(Calendar.ERA)) return false;
        if (cal1.get(Calendar.YEAR) < cal2.get(Calendar.YEAR)) return true;
        if (cal1.get(Calendar.YEAR) > cal2.get(Calendar.YEAR)) return false;
        return cal1.get(Calendar.DAY_OF_YEAR) < cal2.get(Calendar.DAY_OF_YEAR);
    }

    /**
     * <p>Checks if the first date is after the second date ignoring time.</p>
     * @param date1 the first date, not altered, not null
     * @param date2 the second date, not altered, not null
     * @return true if the first date day is after the second date day.
     * @throws IllegalArgumentException if the date is <code>null</code>
     */
    public static boolean isAfterDay(Date date1, Date date2) {
        if (date1 == null || date2 == null) {
            throw new IllegalArgumentException("The dates must not be null");
        }
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);
        return isAfterDay(cal1, cal2);
    }

    /**
     * <p>Checks if the first calendar date is after the second calendar date ignoring time.</p>
     * @param cal1 the first calendar, not altered, not null.
     * @param cal2 the second calendar, not altered, not null.
     * @return true if cal1 date is after cal2 date ignoring time.
     * @throws IllegalArgumentException if either of the calendars are <code>null</code>
     */
    public static boolean isAfterDay(Calendar cal1, Calendar cal2) {
        if (cal1 == null || cal2 == null) {
            throw new IllegalArgumentException("The dates must not be null");
        }
        if (cal1.get(Calendar.ERA) < cal2.get(Calendar.ERA)) return false;
        if (cal1.get(Calendar.ERA) > cal2.get(Calendar.ERA)) return true;
        if (cal1.get(Calendar.YEAR) < cal2.get(Calendar.YEAR)) return false;
        if (cal1.get(Calendar.YEAR) > cal2.get(Calendar.YEAR)) return true;
        if( cal1.get(Calendar.DAY_OF_YEAR) <= cal2.get(Calendar.DAY_OF_YEAR) ) return false;
        if( cal1.get(Calendar.DAY_OF_YEAR) > cal2.get(Calendar.DAY_OF_YEAR) ) return true;

        return false;
    }

    /**
     * <p>Checks if a date is after today and within a number of days in the future.</p>
     * @param date the date to check, not altered, not null.
     * @param days the number of days.
     * @return true if the date day is after today and within days in the future .
     * @throws IllegalArgumentException if the date is <code>null</code>
     */
    public static boolean isWithinDaysFuture(Date date, int days) {
        if (date == null) {
            throw new IllegalArgumentException("The date must not be null");
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return isWithinDaysFuture(cal, days);
    }

    /**
     * <p>Checks if a calendar date is after today and within a number of days in the future.</p>
     * @param cal the calendar, not altered, not null
     * @param days the number of days.
     * @return true if the calendar date day is after today and within days in the future .
     * @throws IllegalArgumentException if the calendar is <code>null</code>
     */
    public static boolean isWithinDaysFuture(Calendar cal, int days) {
        if (cal == null) {
            throw new IllegalArgumentException("The date must not be null");
        }
        Calendar today = Calendar.getInstance();
        Calendar future = Calendar.getInstance();
        future.add(Calendar.DAY_OF_YEAR, days);
        return (isAfterDay(cal, today) && ! isAfterDay(cal, future));
    }

    /** Returns the given date with the time set to the start of the day. */
    public static Date getStart(Date date) {
        return clearTime(date);
    }

    /** Returns the given date with the time values cleared. */
    public static Date clearTime(Date date) {
        if (date == null) {
            return null;
        }
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return c.getTime();
    }

    /** Determines whether or not a date has any time values (hour, minute,
     * seconds or millisecondsReturns the given date with the time values cleared. */

    /**
     * Determines whether or not a date has any time values.
     * @param date The date.
     * @return true iff the date is not null and any of the date's hour, minute,
     * seconds or millisecond values are greater than zero.
     */
    public static boolean hasTime(Date date) {
        if (date == null) {
            return false;
        }
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        if (c.get(Calendar.HOUR_OF_DAY) > 0) {
            return true;
        }
        if (c.get(Calendar.MINUTE) > 0) {
            return true;
        }
        if (c.get(Calendar.SECOND) > 0) {
            return true;
        }
        if (c.get(Calendar.MILLISECOND) > 0) {
            return true;
        }
        return false;
    }

    /** Returns the given date with time set to the end of the day */
    public static Date getEnd(Date date) {
        if (date == null) {
            return null;
        }
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.set(Calendar.HOUR_OF_DAY, 23);
        c.set(Calendar.MINUTE, 59);
        c.set(Calendar.SECOND, 59);
        c.set(Calendar.MILLISECOND, 999);
        return c.getTime();
    }

    /**
     * Returns the maximum of two dates. A null date is treated as being less
     * than any non-null date.
     */
    public static Date max(Date d1, Date d2) {
        if (d1 == null && d2 == null) return null;
        if (d1 == null) return d2;
        if (d2 == null) return d1;
        return (d1.after(d2)) ? d1 : d2;
    }

    /**
     * Returns the minimum of two dates. A null date is treated as being greater
     * than any non-null date.
     */
    public static Date min(Date d1, Date d2) {
        if (d1 == null && d2 == null) return null;
        if (d1 == null) return d2;
        if (d2 == null) return d1;
        return (d1.before(d2)) ? d1 : d2;
    }

    /** The maximum date possible. */
    public static Date MAX_DATE = new Date(Long.MAX_VALUE);

    public static long getUTCTimeMilliseconds()
    {
        TimeZone utc = TimeZone.getTimeZone("UTC");
        Calendar calendar = Calendar.getInstance(utc);
        return calendar.getTimeInMillis();
    }

    public static String getLocalTimeString( long longutcTime ){
        String localTime = "";

        TimeZone zone = TimeZone.getDefault();
        int offset = zone.getOffset(longutcTime);
        long longLocalTime = longutcTime + offset;

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        Date dateLocalTime = new Date();
        dateLocalTime.setTime(longLocalTime);

        localTime = dateFormat.format(dateLocalTime);

        return localTime;
    }

    public static Date getLocalTime( long longutcTime ){

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(longutcTime);

        return calendar.getTime();
    }



    public static String convertUTCTicktoDateString( long utcTick ){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        TimeZone utcTimeZone = TimeZone.getTimeZone("UTC");
        dateFormat.setTimeZone(utcTimeZone);
        Date utcDate = new Date();
        utcDate.setTime(utcTick);
        return dateFormat.format(utcDate);
    }

    public static String convertUTCTickToDateString( long utcTick , String pattern ){
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
        TimeZone utcTimeZone = TimeZone.getTimeZone("UTC");
        dateFormat.setTimeZone(utcTimeZone);
        Date utcDate = new Date();
        utcDate.setTime( utcTick );
        return dateFormat.format( utcDate );
    }


    public static long convertDateStringtoUTCTick( String dateString ){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date dateUtcTime = null;
        try {
            dateUtcTime = dateFormat.parse(dateString);
        } catch (ParseException e) {  }
        return dateUtcTime.getTime();
    }

    public static long convertDateStringtoUTCTick( String dateString , String pattern ){
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date dateUtcTime = null;
        try {
            dateUtcTime = dateFormat.parse(dateString);
        } catch (ParseException e) {  }
        return dateUtcTime.getTime();
    }


    public static String getDateStringForPattern(String dateString, String pattern)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date dateTime = null;
        try {
            dateTime = dateFormat.parse(dateString);
        } catch (ParseException e) {  }

        dateFormat  = new SimpleDateFormat( pattern );
        return dateFormat.format(dateTime);
    }

    public static String getDateStringForPattern( Date srcDate , String pattern)
    {
        String dateString = null;
        SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat( pattern, Locale.getDefault() );
        dateString =  mSimpleDateFormat.format(srcDate);
        return dateString;
    }

    public static long diffOfDate( Calendar endCalendar ) throws Exception
    {

        Calendar beginCalendar = Calendar.getInstance();
        beginCalendar.set(2014,11,28);

        long diff = endCalendar.getTimeInMillis() - beginCalendar.getTimeInMillis();
        long diffDays = diff / (24 * 60 * 60 * 1000);

        return diffDays;
    }

    public static String toWeekString(String weekString, Locale currentLocale){
        String resultString="";
        String language= currentLocale.getLanguage();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date weekDate = null;
        try {
            weekDate = dateFormat.parse(weekString);
        } catch (ParseException e) {  }


        Calendar weekCalendar = Calendar.getInstance();
        weekCalendar.setTime(weekDate);
        int weeknum = weekCalendar.get(Calendar.WEEK_OF_MONTH);

        if(language.equals("ko")){

            SimpleDateFormat format3 =new SimpleDateFormat("MMM");
            resultString +="" + format3.format(weekCalendar.getTime())+" ";
            if(weeknum==1){
                resultString +="첫째주";
            }else if (weeknum == 2) {

                resultString +="둘째주";
            }else if(weeknum==3){
                resultString +="셋째주";
            }else if(weeknum==4){
                resultString +="넷째주";
            }else if(weeknum==5){
                resultString +="다섯째주";
            }
            else if(weeknum==6){
                resultString +="여섯째주";
            }
        }else{
            if(weeknum==1){
                resultString +="The 1st week";
            }else if (weeknum == 2) {

                resultString +="The 2nd week";
            }else if(weeknum==3){
                resultString +="The 3rd week";
            }else {
                resultString +="The "+weeknum +"th week";
            }
            SimpleDateFormat format3 =new SimpleDateFormat("MMM yyyy");
            resultString +=" " + format3.format(weekCalendar.getTime());
        }

        return resultString;
    }

    public static String getDateString( Calendar calendar )
    {
        return calendar.get(Calendar.YEAR) + "-" + String.format("%02d",  calendar.get( Calendar.MONTH ) + 1 ) + "-" + String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH) );
    }


}
