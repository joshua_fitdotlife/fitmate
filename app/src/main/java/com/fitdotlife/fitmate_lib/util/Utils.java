package com.fitdotlife.fitmate_lib.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ContinuousCheckPolicy;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseAnalyzer;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.MoreExerciseForMaxScore_StrengthType;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.MoreExeriseForMaxSocre_CalorieSum;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.StrengthInputType;
import com.fitdotlife.fitmate.NewHomeActivity;
import com.fitdotlife.fitmate.newhome.CategoryType;
import com.fitdotlife.fitmate.newhome.NewHomeConstant;
import com.fitdotlife.fitmate_lib.object.DayActivity;
import com.fitdotlife.fitmate_lib.object.ExerciseProgram;
import com.fitdotlife.fitmate_lib.object.ScoreClass;

import org.apache.log4j.Log;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import java.util.List;
import java.util.Locale;

/**
 * Created by Joshua on 2015-03-27.
 */
public class Utils {



    public static com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseProgram ConvertExerciseProgram(ExerciseProgram program){
        boolean iscontinued= false;
        if(program.getContinuousExercise()==1) iscontinued=true;
        com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseProgram result = new com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseProgram(program.getName(),toStrengthInputType(program.getCategory()), program.getStrengthFrom(), program.getStrengthTo(), program.getTimesFrom(), program.getTimesTo(),program.getMinuteFrom(), program.getMinuteTo(), iscontinued, false, 1,1);
        return result;
    }

    public static StrengthInputType toStrengthInputType(int type){
        return StrengthInputType.values()[type];
    }

    public static List<ScoreClass> CalculateDayScores_Calorie( List<Integer> targets, ExerciseAnalyzer exerciseAnalyzer ){
        List<ScoreClass> result = new ArrayList<ScoreClass>();

        List<Integer>toYesterDay= new ArrayList<Integer>();

        com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseProgram appliedExerciseProgram = exerciseAnalyzer.getProgram();

        int weekCalorieSum=0;
        int weekAchieveToYesterday = 0;
        int todayGetPoint = 0;
        int toDayPossibleMaxScore = 0;
        int weekAchieveToToday = 0;
        MoreExerciseForMaxScore_StrengthType moreStrengthType = null;
        for(int i=0;i<targets.size();i++)
        {
            ScoreClass temp = new ScoreClass();
            //어제까지 점수
            if(exerciseAnalyzer.getProgram().getStrengthInputType().equals(StrengthInputType.CALORIE) ) {
                weekAchieveToYesterday = exerciseAnalyzer.CalculateWeekAchievement_Calorie(toYesterDay);

                //오늘 점수
                toYesterDay.add(targets.get(i));
                weekAchieveToToday = exerciseAnalyzer.CalculateWeekAchievement_Calorie(toYesterDay);
                moreStrengthType = exerciseAnalyzer.getNeedMoreExercise_Calorie(toYesterDay);

                todayGetPoint = weekAchieveToToday - weekAchieveToYesterday;
                toYesterDay.remove(targets.get(i));

                //오늘 얻을 수 있는 최대 점수
                toYesterDay.add((int)appliedExerciseProgram.getStrength_From());
                toDayPossibleMaxScore=exerciseAnalyzer.CalculateWeekAchievement_Calorie(toYesterDay);

            } else if(exerciseAnalyzer.getProgram().getStrengthInputType().equals(StrengthInputType.VS_BMR) ) {
                weekAchieveToYesterday = exerciseAnalyzer.CalculateWeekAchievement_VS_BMR(toYesterDay);

                //오늘 점수
                toYesterDay.add(targets.get(i));
                weekAchieveToToday = exerciseAnalyzer.CalculateWeekAchievement_VS_BMR(toYesterDay);
                moreStrengthType = exerciseAnalyzer.getNeedMoreExercise_VS_BMR(toYesterDay);

                todayGetPoint = weekAchieveToToday - weekAchieveToYesterday;
                toYesterDay.remove(targets.get(i));

                //오늘 얻을 수 있는 최대 점수
                int bmr = exerciseAnalyzer.getUserInfo().getBMR();
                toYesterDay.add((int) (bmr * (appliedExerciseProgram.getStrength_From() / 100)));
                toDayPossibleMaxScore = exerciseAnalyzer.CalculateWeekAchievement_VS_BMR( toYesterDay );
            }

            temp.todayPossibleMorePoint = toDayPossibleMaxScore-weekAchieveToToday;
            temp.isAchieveMax = moreStrengthType.isAchieveMax();
            temp.possibleMaxScore= moreStrengthType.getPossibleMaxScore();
            temp.todayGetPoint = todayGetPoint;
            temp.weekAchieve = weekAchieveToToday;
            temp.preDayAchieve = weekAchieveToYesterday;
            temp.possibleTimes = moreStrengthType.getDays();
            temp.possibleValue = moreStrengthType.getRangeFrom();

            toYesterDay.remove(i);

            toYesterDay.add(targets.get(i));

            result.add(temp);
        }

        return result;
    }

    public static List<ScoreClass> CalculateDayScores_CalorieSUM( List<Integer> targets, ExerciseAnalyzer exerciseAnalyzer ){
        List<ScoreClass> result = new ArrayList<ScoreClass>();
        List<Integer>toYesterDay= new ArrayList<Integer>();

        com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseProgram appliedExerciseProgram = exerciseAnalyzer.getProgram();

        int weekCalorieSum=0;
        int weekAchieveToYesterday = 0;
        int todayGetPoint = 0;
        int toDayPossibleMaxScore = 0;
        int weekAchieveToToday = 0;
        MoreExeriseForMaxSocre_CalorieSum moreStrengthType = null;
        for(int i=0;i<targets.size();i++)
        {
            ScoreClass temp = new ScoreClass();
            //어제까지 점수

            weekAchieveToYesterday = exerciseAnalyzer.CalculateWeekAchievement_CalorieSum(weekCalorieSum);
            //오늘 점수
            weekCalorieSum += targets.get(i);
            weekAchieveToToday = exerciseAnalyzer.CalculateWeekAchievement_CalorieSum(weekCalorieSum);
            moreStrengthType = exerciseAnalyzer.getNeedMoreExercise(weekCalorieSum);

            todayGetPoint = weekAchieveToToday - weekAchieveToYesterday;
            weekCalorieSum -= targets.get(i);

            //오늘 얻을 수 있는 최대 점수
            //toYesterDay.add((int)appliedExerciseProgram.getStrength_From());
            weekCalorieSum += appliedExerciseProgram.getStrength_From();
            toDayPossibleMaxScore=exerciseAnalyzer.CalculateWeekAchievement_CalorieSum(weekCalorieSum);


            temp.todayPossibleMorePoint = toDayPossibleMaxScore-weekAchieveToToday;
            temp.isAchieveMax = moreStrengthType.isAchieveMax();
            temp.possibleMaxScore= moreStrengthType.getPossibleMaxScore();
            temp.todayGetPoint = todayGetPoint;
            temp.weekAchieve = weekAchieveToToday;
            temp.preDayAchieve = weekAchieveToYesterday;
            temp.possibleTimes = 0;
            temp.possibleValue = moreStrengthType.getNeedMoreCalorie();

            toYesterDay.remove(i);

            toYesterDay.add(targets.get(i));

            result.add(temp);
        }

        return result;
    }

    public static List<ScoreClass> CalculateDayScores_Strength( List<DayActivity> dayActivityList , ExerciseAnalyzer exerciseAnalyzer){

        List<ScoreClass> result = new ArrayList<ScoreClass>();
        List<List<Integer>>toYesterDay= new ArrayList<List<Integer>>();
        List<Integer> weekExerciseList = new ArrayList<Integer>();

        com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseProgram appliedExerciseProgram = exerciseAnalyzer.getProgram();

        for( int i = 0 ; i < dayActivityList.size() ;i++ ){
            ScoreClass temp = new ScoreClass();
            //어제까지 점수
            int weekAchieveToYesterday = exerciseAnalyzer.CalcuateWeekAchieveByMinutesList(ContinuousCheckPolicy.Strictly, toYesterDay);

            //오늘 점수
            DayActivity dayActivity = dayActivityList.get(i);
            List<Integer> achieveOfTageList =  new ArrayList<Integer>();
            achieveOfTageList.add( dayActivity.getAchieveOfTarget() );
            toYesterDay.add(achieveOfTageList);
            weekExerciseList.add( dayActivity.getAchieveOfTarget() );

            int weekAchieveToToday = exerciseAnalyzer.CalcuateWeekAchieveByMinutesList(ContinuousCheckPolicy.Loosely_SumOverXExercise, toYesterDay);
            MoreExerciseForMaxScore_StrengthType moreStrengthType = exerciseAnalyzer.getNeedMoreExercise( ContinuousCheckPolicy.Loosely_SumOverXExercise , weekExerciseList );
            int todayGetPoint = weekAchieveToToday - weekAchieveToYesterday;
            toYesterDay.remove( achieveOfTageList );

            //오늘 얻을 수 있는 최대 점수

            //toYesterDay.add( (int)appliedExerciseProgram.getStrength_From() );
            List<Integer> extraAchieveOfTageList = new ArrayList<Integer>();
            extraAchieveOfTageList.add(appliedExerciseProgram.getMinutes_From());
            toYesterDay.add(extraAchieveOfTageList);
            int toDayPossibleMaxScore=exerciseAnalyzer.CalcuateWeekAchieveByMinutesList(ContinuousCheckPolicy.Loosely_SumOverXExercise, toYesterDay);

            temp.todayPossibleMorePoint = toDayPossibleMaxScore-weekAchieveToToday;
            temp.isAchieveMax = moreStrengthType.isAchieveMax();
            temp.possibleMaxScore= moreStrengthType.getPossibleMaxScore();
            temp.todayGetPoint = todayGetPoint;
            temp.weekAchieve = weekAchieveToToday;
            temp.preDayAchieve = weekAchieveToYesterday;
            temp.possibleTimes = moreStrengthType.getDays();
            temp.possibleValue = moreStrengthType.getRangeFrom();

            toYesterDay.remove( i );

            toYesterDay.add(achieveOfTageList);
            result.add(temp);
        }

        return result;
    }

    public static String encodePassword( String password ){
        String md5 = encodeMD5( password );
        String sha1 = encodeSHA1(md5);

        return sha1.substring( 0,32 );
    }

    public static int getSNUTargetCalorie(float weight){
        return (int)Math.round( (49.6 + 20) * weight /  7);
    }

    private static String encodeMD5( String str ){
        String MD5 = "";
        try{
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(str.getBytes());
            byte byteData[] = md.digest();
            StringBuffer sb = new StringBuffer();
            for(int i = 0 ; i < byteData.length ; i++){
                sb.append(Integer.toString((byteData[i]&0xff) + 0x100, 16).substring(1));
            }
            MD5 = sb.toString();

        }catch(NoSuchAlgorithmException e){
            e.printStackTrace();
            MD5 = null;
        }

        return MD5;
    }

    private static String encodeSHA1( String str ){
        String SHA = "";
        try{
            MessageDigest sh = MessageDigest.getInstance("SHA-1");
            sh.update(str.getBytes());
            byte byteData[] = sh.digest();
            StringBuffer sb = new StringBuffer();
            for(int i = 0 ; i < byteData.length ; i++){
                sb.append(Integer.toString((byteData[i]&0xff) + 0x100, 16).substring(1));
            }
            SHA = sb.toString();

        }catch(NoSuchAlgorithmException e){
            e.printStackTrace();
            SHA = null;
        }
        return SHA;
    }

    public static boolean  isOnline( Context context ){
        try {
            ConnectivityManager conMan = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo.State wifi = conMan.getNetworkInfo(1).getState(); // wifi
            if (wifi == NetworkInfo.State.CONNECTED || wifi == NetworkInfo.State.CONNECTING) {
                return true;
            }

            NetworkInfo.State mobile = conMan.getNetworkInfo(0).getState(); // mobile ConnectivityManager.TYPE_MOBILE
            if (mobile == NetworkInfo.State.CONNECTED || mobile == NetworkInfo.State.CONNECTING) {
                return true;
            }

        } catch (NullPointerException e) {
            return false;
        }

        return false;
    }

    public static String getStrengthUnit( StrengthInputType strengthInputType){
        String strengthUnit = null;

        switch( strengthInputType ){
            case VO2MAX:
            case VO2R:
                strengthUnit = "%";
                break;
            case STRENGTH:
                strengthUnit = "";
                break;
            case MET:
                strengthUnit = "MET";
                break;
            case RPE:
                strengthUnit = "";
                break;
            case CALORIE:
                strengthUnit = "kcal";
                break;
            case CALORIE_SUM:
                strengthUnit = "kcal";
                break;
            case VS_BMR:
                strengthUnit ="%";
                break;
        }

        return strengthUnit;


    }

    public static String toWeekString(int weeknum,Locale currentLocale){
        String resultString="";
        String language= currentLocale.getLanguage();
        if(language.equals("ko")){

            if(weeknum==1){
                resultString +="첫째주";
            }else if (weeknum == 2) {
                resultString +="둘째주";
            }else if(weeknum==3){
                resultString +="셋째주";
            }else if(weeknum==4){
                resultString +="넷째주";
            }else if(weeknum==5){
                resultString +="다섯째주";
            }else if(weeknum==6){
                resultString +="여섯째주";
            }

        }else {
            if (weeknum == 1) {
                resultString += "1st";
            } else if (weeknum == 2) {

                resultString += "2nd";
            } else if (weeknum == 3) {
                resultString += "3rd";
            } else {
                resultString += weeknum + "th";
            }
        }
        return resultString;
    }



    public static String toWeekString(int weeknum, Calendar calendar, Locale currentLocale){
        String resultString="";
        String language= currentLocale.getLanguage();
        if(language.equals("ko")){

            SimpleDateFormat format3 =new SimpleDateFormat("yyyy'년' MMM");
            resultString +="" + format3.format(calendar.getTime())+" ";
            if(weeknum==1){
                resultString +="첫째주";
            }else if (weeknum == 2) {

                resultString +="둘째주";
            }else if(weeknum==3){
                resultString +="셋째주";
            }else if(weeknum==4){
                resultString +="넷째주";
            }else if(weeknum==5){
                resultString +="다섯째주";
            }
            else if(weeknum==6){
                resultString +="여섯째주";
            }
        }else{
            if(weeknum==1){
                resultString +="The 1st week";
            }else if (weeknum == 2) {

                resultString +="The 2nd week";
            }else if(weeknum==3){
                resultString +="The 3rd week";
            }else {
               resultString +="The "+weeknum +"th week";
            }
            SimpleDateFormat format3 =new SimpleDateFormat("MMM yyyy");
            resultString +=" " + format3.format(calendar.getTime());
        }

        return resultString;
    }

    public static String toMonthString(Calendar calendar, Locale currentLocale){
        String resultString="";
        String language= currentLocale.getLanguage();
        if(language.equals("ko")){

            SimpleDateFormat format3 =new SimpleDateFormat("yyyy'년' MMM");
            resultString +="" + format3.format(calendar.getTime())+" ";


        }else{

            SimpleDateFormat format3 =new SimpleDateFormat("MMM yyyy");
            resultString +=" " + format3.format(calendar.getTime());
        }

        return resultString;
    }

    public static String convertToString( boolean boolValue ){
        if( boolValue ){
            return "true";
        }else {
            return "false";
        }
    }

    public static boolean convertToBoolean( String strBool ){
        if( strBool.equals("true") ){
            return true;
        }
        else{
            return false;
        }
    }

    public static CategoryType[] readTabViewOrder( Context context ){
        SharedPreferences prefs = context.getSharedPreferences("fitmate", Context.MODE_PRIVATE);
        String strViewOrder  = prefs.getString(NewHomeActivity.VIEWORDER_KEY  , null);
        CategoryType[] categoryViewOrder = null;

        if( strViewOrder != null ) {

            String[] arrViewOrder = strViewOrder.split(",");
            categoryViewOrder = new CategoryType[ arrViewOrder.length ];
            for (int i = 0; i < arrViewOrder.length; i++) {
                categoryViewOrder[i] =  CategoryType.values()[ Integer.parseInt(arrViewOrder[i]) ];
            }

        }else{

            categoryViewOrder = NewHomeConstant.DefaultViewOrder;
        }

        return categoryViewOrder;
    }

    public static int readSelectedCategoryIndex( Context context ){
        SharedPreferences pref = context.getSharedPreferences("fitmate", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        int selectedCategoryIndex = pref.getInt(NewHomeActivity.SELECTEDCATEGORYINDEX_KEY, 0);
        return selectedCategoryIndex;
    }

    //값을 소수점으로 나타내는 경우
    //세자리에 맞춰주는 함수이다.
    //값의 정수의 자릿수가 하나이면 소수점을 2자리로
    //값의 정수의 자릿수가 두개이먄 소수점을 1자리고
    //값의 정수의 자릿수가 3개 이상이면 정수로 표현한다.
    public static String get3DigitString( double value )
    {
        String valueText = null;
        String intValueString = String.valueOf((int) value);
        if( intValueString.length() < 2 ){ //한자리 일때
            valueText = String.format( "%.2f" , value);

        }else if(intValueString.length() < 3) { //두자리일 때
            valueText = String.format( "%.1f" , value );

        }else{ //세자리 이상일 때
            valueText = String.format("%d" , (int)value);
        }
        return valueText;
    }

    public static double convertGramToOunce( double gram ){
        double ounce = 0;
        ounce = gram * 0.035274;
        return ounce;
    }

    public static double convertKMToMile( double km ){
        double mile = 0;

        mile = km * 0.621371;

        return mile;
    }

    public static float parseFloat( String valueText ) throws ParseException
    {
        NumberFormat format = NumberFormat.getInstance( Locale.getDefault() );
        Number number = format.parse( valueText );
        return number.floatValue();
    }

    public static double parseDouble( String valueText ) throws ParseException
    {
        NumberFormat format = NumberFormat.getInstance( Locale.getDefault() );
        Number number = format.parse( valueText );
        return number.doubleValue();
    }


}
