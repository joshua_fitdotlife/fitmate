package com.fitdotlife.fitmate_lib.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.fitdotlife.fitmate.R;
import com.fitdotlife.fitmate_lib.customview.FitmeterDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by wooseok on 2015-06-16.
 */
public class ImageUtil extends Activity
{

    private final static String TAG = ImageUtil.class.getSimpleName();

    public static String FIXRATE_KEY = "fixrate";
    public static final String PICTURE_URI = "PICURI";

    private static final int PICK_FROM_CAMERA = 0;
    private static final int PICK_FROM_ALBUM = 1;
    private static final int CROP_FROM_CAMERA = 2;

    private Handler mHandler = new Handler();
    private ProgressDialog mDialog = null;
    private Uri mImageCaptureUri;

    private boolean mDialogClose = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startDialog();
            }
        }, 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        Log.d(TAG, "onActivityResult");
        if(resultCode==0)
        {
            finish();
            return;
        }

        if(resultCode != RESULT_OK){
            return;
        }

        switch (requestCode) {
            case PICK_FROM_ALBUM:
            {
                Log.d(TAG , "PICK_FROM_ALBUM");
                //이후의 처리가 카메라와 같으므로 break없이 진행한다.
                //실제 코드에서는 좀더 합리적인 방법을 선택하시기 바랍니다.
                mImageCaptureUri = data.getData();
                File origimalFile = getImageFile(mImageCaptureUri);

                mImageCaptureUri = createSaceCropFile();
                File copy_file = new File(mImageCaptureUri.getPath());

                //SD카드에 저장된 파일을 이미지 Crop을 위해 복사한다.
                copyFile( origimalFile , copy_file );
            }

            case PICK_FROM_CAMERA:
            {
                Log.d(TAG, "PICK_FROM_CAMERA");
                //이미지를 가져온 이후의 리사이즈할 이미지크기를 결정합니다.
                //이후에 이미지 크롭 어플리케이션을 호출하게 됩니다.
                Intent intent = new Intent("com.android.camera.action.CROP");
                intent.setDataAndType(mImageCaptureUri, "image/*");

                //crop한 이미지를 저장할 path
                intent.putExtra( "output" , mImageCaptureUri  );
//                if( mFixRate )
//                {
//                    intent.putExtra("aspectX", 2);
//                    intent.putExtra("aspectY", 1);
//                    intent.putExtra("scale", true);
//                }

                //Return Data를 사용하면 번들 용량 제한으로 크기가 큰 이미지를 넘겨줄수 없다.
                startActivityForResult(intent, CROP_FROM_CAMERA);

                break;
            }
            case CROP_FROM_CAMERA:
            {
                //Crop된 이미지를 넘겨 받습니다.
                Log.w(TAG , "mImageCaptureUri = " + mImageCaptureUri);

                //String full_path = mImageCaptureUri.getPath();
                //이미지의 파일의 크기를 알아본다.

                //resizeFile( full_path );

                Intent intent = new Intent();
                intent.putExtra(PICTURE_URI, mImageCaptureUri);
                setResult(Activity.RESULT_OK, intent);
                finish();


                //String photo_path = full_path.substring(4 , full_path.length());
                //Log.w(TAG , "비트맵 Image path = " + photo_path);
                //Bitmap photo = BitmapFactory.decodeFile( photo_path );



            }
       }
    }

    private void startDialog() {

        final FitmeterDialog imageDialog = new FitmeterDialog( this );
        //LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LayoutInflater inflater = LayoutInflater.from( this );

        View contentView = inflater.inflate(R.layout.dialog_default_content, null);
        TextView tvTitle = (TextView) contentView.findViewById(R.id.tv_dialog_default_title);
        tvTitle.setText(this.getString(R.string.myprofile_howto_select_photo));

        TextView tvContent = (TextView) contentView.findViewById(R.id.tv_dialog_default_content);
        tvContent.setText(this.getString(R.string.myprofile_select_photo));
        imageDialog.setContentView(contentView);

        View buttonView = inflater.inflate(R.layout.dialog_button_two , null);
        Button btnLeft = (Button) buttonView.findViewById(R.id.btn_dialog_button_two_left);
        btnLeft.setText(this.getString(R.string.myprofile_gallery));
        btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                doTakeAlbumAction();
                mDialogClose = true;
                imageDialog.close();
            }
        });

        Button btnRight = (Button) buttonView.findViewById(R.id.btn_dialog_button_two_right);
        btnRight.setText(this.getString(R.string.myprofile_camera));
        btnRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doTakePhotoAction();
                mDialogClose = true;
                imageDialog.close();
            }
        });

        imageDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                if (!mDialogClose) {
                    finish();
                }
            }
        });

        imageDialog.setButtonView(buttonView);
        imageDialog.setCancelable(true);
        imageDialog.show();

    }

    /**
     * 카메라 호출하기
     */
    private void doTakePhotoAction()
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //crop된 이미지를 저장할 파일의 경로를 생성.
        mImageCaptureUri = createSaceCropFile();
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
        this.startActivityForResult(intent, PICK_FROM_CAMERA);
    }

    private void doTakeAlbumAction(){

        //앨범 호출
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        this.startActivityForResult(intent, PICK_FROM_ALBUM);
    }

    private Uri createSaceCropFile()
    {
        Uri uri;
        String url = ".tmp_" + String.valueOf( System.currentTimeMillis() ) + ".jpg";
        uri = Uri.fromFile( new File( Environment.getExternalStorageDirectory() , url ) );
        return uri;
    }

    /**
     * 선택된 uri의 사진 path를 가져온다.
     * @param uri
     * @return
     */
    private File getImageFile(Uri uri)
    {
        String[] projecton = {MediaStore.Images.Media.DATA};
        if(uri == null){
            uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        }

        Cursor mCursor = getContentResolver().query(uri , projecton , null , null , MediaStore.Images.Media.DATE_MODIFIED + " desc");
        if(mCursor == null || mCursor.getCount() < 1){
            return null;
        }

        int column_index = mCursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        mCursor.moveToFirst();

        String path = mCursor.getString( column_index );

        if(mCursor != null){
            mCursor.close();
            mCursor = null;
        }

        return new File(path);
    }

    /**
     * 파일 복사
     * @param srcFile
     * @param destFile
     * @return
     */
    private boolean copyFile(File srcFile , File destFile){
        boolean result = false;
        try {
            InputStream in = new FileInputStream(srcFile);
            try{
                result = copyToFile( in , destFile );
            }finally {
                in.close();
            }
        }catch(IOException e){
            result = false;
        }
        return result;
    }

    private boolean copyToFile( InputStream inputStream , File destFile ){
        try {
            OutputStream out = new FileOutputStream(destFile);
            try {
                byte[] buffer = new byte[4096];
                int bytesRead;
                while ((bytesRead = inputStream.read(buffer)) > 0) {
                    out.write(buffer, 0, bytesRead);
                }
            }finally {
                out.close();
            }
            return true;
        }catch(IOException e){
            return true;
        }
    }

    private void resizeFile( String pathImage )
    {
        try {
            int wWidth = 800;
            int wHeight = 800;

            InputStream in = new FileInputStream(pathImage);

            Bitmap srcBitmap = BitmapFactory.decodeFile(pathImage);
            float srcWidth = srcBitmap.getWidth();
            float srcHeight = srcBitmap.getHeight();

            if (srcWidth >= srcHeight) {
                if (srcWidth > wWidth) {
                    float mWidth = (float) (srcWidth / 100);
                    float fScale = (float) (wWidth / mWidth);
                    srcWidth *= (fScale / 100);
                    srcHeight *= (fScale / 100);
                }
            } else {
                if (srcHeight > wHeight) {
                    float mHeight = (float) (srcHeight / 100);
                    float fScale = (float) (wHeight / mHeight);
                    srcWidth *= (fScale / 100);
                    srcHeight *= (fScale / 100);
                }
            }

            Bitmap resizedBitmap = Bitmap.createScaledBitmap(srcBitmap, (int)srcWidth , (int)srcHeight , true);

            try {
                FileOutputStream out = new FileOutputStream(pathImage);
                resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);
            } catch (Exception e) {
                Log.e("Image", e.getMessage(), e);
            }finally {

            }

        }catch (IOException e)
        {
            Log.e("Image", e.getMessage(), e);
        }
    }
}
