package com.fitdotlife.fitmate_lib.object;

/**
 * Created by Joshua on 2015-10-12.
 */
public class UserFriendsNewsSetting {

    private NewsUser friendInfo;
    private boolean sendMyNews;
    private boolean receiveFriendsNews;

    public NewsUser getFriendInfo() {
        return friendInfo;
    }

    public void setFriendInfo(NewsUser friendInfo) {
        this.friendInfo = friendInfo;
    }

    public boolean isSendMyNews() {
        return sendMyNews;
    }

    public void setSendMyNews(boolean sendMyNews) {
        this.sendMyNews = sendMyNews;
    }

    public boolean isReceiveFriendsNews() {
        return receiveFriendsNews;
    }

    public void setReceiveFriendsNews(boolean receiveFriendsNews) {
        this.receiveFriendsNews = receiveFriendsNews;
    }
}
