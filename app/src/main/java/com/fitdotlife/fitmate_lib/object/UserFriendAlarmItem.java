package com.fitdotlife.fitmate_lib.object;

/**
 * Created by Joshua on 2015-08-06.
 */
public class UserFriendAlarmItem {

    private int requestvalue = 0;
    private int id = 0;
    private boolean checkedvalue = false;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRequestvalue() {
        return requestvalue;
    }

    public void setRequestvalue(int requestvalue) {
        this.requestvalue = requestvalue;
    }

    public boolean isCheckedvalue() {
        return checkedvalue;
    }

    public void setCheckedvalue(boolean checkedvalue) {
        this.checkedvalue = checkedvalue;
    }
}
