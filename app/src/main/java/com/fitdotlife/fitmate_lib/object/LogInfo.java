package com.fitdotlife.fitmate_lib.object;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Joshua on 2015-09-17.
 */
public class LogInfo {
    private String product = "fitmate";
    private String devicetype = "android";
    private String appversion;   // 앱 버전
    private String osversion;    // OS 버젼
    private String content;      // 피드백 내용
    private String attachment;   // 파일 로그(String 값으로.. 첨부했던 파일 자체를 그냥 read해서 보냄)
    private String model;         // 핸드폰 모델(s5, g3)
    private String sdkversion;   // SDK 버전 (
    private String username;     // 사용자 이름
    private String email;

    private String tagnum;

    public String getAppversion() {
        return appversion;
    }

    public void setAppversion(String appversion) {
        this.appversion = appversion;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDevicetype() {
        return devicetype;
    }

    public void setDevicetype(String devicetype) {
        this.devicetype = devicetype;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getOsversion() {
        return osversion;
    }

    public void setOsversion(String osversion) {
        this.osversion = osversion;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getSdkversion() {
        return sdkversion;
    }

    public void setSdkversion(String sdkversion) {
        this.sdkversion = sdkversion;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTagnum() {
        return tagnum;
    }

    public void setTagnum(String tagnum) {
        this.tagnum = tagnum;
    }

    public JSONObject toJsonObject()
    {
        JSONObject logObject = new JSONObject();

        try {
            logObject.put( "product" , product );
            logObject.put( "devicetype" , devicetype );
            logObject.put( "appversion" , appversion );
            logObject.put( "osversion" , osversion );
            logObject.put( "content" , content );
            logObject.put( "attachment" , attachment );
            logObject.put( "model" , model );
            logObject.put( "sdkversion" , sdkversion );
            logObject.put( "username" , username );
            logObject.put( "email" , email );
            logObject.put( "tagnum" , tagnum );
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return logObject;
    }
}
