package com.fitdotlife.fitmate_lib.object;

/**
 * Created by Joshua on 2015-08-06.
 */
public class UserFriendCheckedItem {

    private int requestvalue = 0;
    private int id = 0;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRequestvalue() {
        return requestvalue;
    }

    public void setRequestvalue(int requestvalue) {
        this.requestvalue = requestvalue;
    }
}
