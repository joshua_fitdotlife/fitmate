package com.fitdotlife.fitmate_lib.object;

/**
 * Created by Joshua on 2016-01-11.
 */
public class FriendDayActivity {

    private String nodata;
    private DatesRangeInfo daterange;
    private String exprogramid;
    private String calorie;
    private int calorierank;
    private String averagemet;
    private int averagemetrank;
    private String date;
    private String strengthhigh;
    private String strengthmedium;
    private String strengthlow;
    private String strengthunder;
    private int strengthoverlightrank;
    private String achieve;
    private String weekscore;
    private String distance;
    private int distancerank;
    private String fat;
    private int fatrank;
    private String carbohydrate;
    private TargetInfo targetrangeinfo;

    public String getAchieve() {
        return achieve;
    }

    public void setAchieve(String achieve) {
        if(achieve == null){
            this.achieve = "0";
            return;
        }
        this.achieve = achieve;
    }

    public String getAveragemet() {
        return averagemet;
    }

    public void setAveragemet(String averagemet) {
        if( averagemet == null )
        {
            this.averagemet = "0";
            return;
        }

        this.averagemet = averagemet;
    }

    public int getAveragemetrank() {
        return averagemetrank;
    }

    public void setAveragemetrank(int averagemetrank) {
        this.averagemetrank = averagemetrank;
    }

    public String getCalorie() {
        return calorie;
    }

    public void setCalorie(String calorie) {

        if(calorie == null){
            this.calorie = "0";
            return;
        }
        this.calorie = calorie;
    }

    public int getCalorierank() {
        return calorierank;
    }

    public void setCalorierank(int calorierank) {
        this.calorierank = calorierank;
    }

    public String getCarbohydrate() {
        return carbohydrate;
    }

    public void setCarbohydrate(String carbohydrate) {

        if( carbohydrate == null ){
            this.carbohydrate = "0";
            return;
        }

        this.carbohydrate = carbohydrate;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public DatesRangeInfo getDaterange() {
        return daterange;
    }

    public void setDaterange(DatesRangeInfo daterange) {
        this.daterange = daterange;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {

        if(distance == null){
            this.distance = "0";
            return;
        }

        this.distance = distance;
    }

    public int getDistancerank() {
        return distancerank;
    }

    public void setDistancerank(int distancerank) {
        this.distancerank = distancerank;
    }

    public String getExprogramid() {
        return exprogramid;
    }

    public void setExprogramid(String exprogramid) {

        if(exprogramid == null){
            this.exprogramid = "0";
            return;
        }

        this.exprogramid = exprogramid;
    }

    public String getFat() {
        return fat;
    }

    public void setFat(String fat) {

        if(fat == null){
            this.fat = "0";
            return;
        }
        this.fat = fat;
    }

    public int getFatrank() {
        return fatrank;
    }

    public void setFatrank(int fatrank) {
        this.fatrank = fatrank;
    }

    public String getNodata() {
        return nodata;
    }

    public void setNodata(String nodata) {
        this.nodata = nodata;
    }

    public String getStrengthhigh() {
        return strengthhigh;
    }

    public void setStrengthhigh(String strengthhigh) {

        if(strengthhigh == null){
            this.strengthhigh = "0";
            return;
        }

        this.strengthhigh = strengthhigh;
    }

    public String getStrengthlow() {
        return strengthlow;
    }

    public void setStrengthlow(String strengthlow) {

        if( strengthlow == null ){
            this.strengthlow = "0";
            return;
        }

        this.strengthlow = strengthlow;
    }

    public String getStrengthmedium() {
        return strengthmedium;
    }

    public void setStrengthmedium(String strengthmedium) {

        if( strengthmedium == null ){
            this.strengthmedium = "0";
            return;
        }

        this.strengthmedium = strengthmedium;
    }

    public int getStrengthoverlightrank() {
        return strengthoverlightrank;
    }

    public void setStrengthoverlightrank(int strengthoverlightrank) {
        this.strengthoverlightrank = strengthoverlightrank;
    }

    public String getStrengthunder() {
        return strengthunder;
    }

    public void setStrengthunder(String strengthunder) {

        if( strengthunder == null ){
            this.strengthunder = "0";
            return;
        }

        this.strengthunder = strengthunder;
    }

    public TargetInfo getTargetrangeinfo() {
        return targetrangeinfo;
    }

    public void setTargetrangeinfo(TargetInfo targetrangeinfo) {
        this.targetrangeinfo = targetrangeinfo;
    }

    public String getWeekscore() {
        return weekscore;
    }

    public void setWeekscore(String weekscore) {

        if( weekscore == null ){
            this.weekscore = "0";
            return;
        }

        this.weekscore = weekscore;
    }

}
