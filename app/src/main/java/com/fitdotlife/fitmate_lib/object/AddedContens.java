package com.fitdotlife.fitmate_lib.object;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

/**
 * Created by Joshua on 2015-09-25.
 */
@JsonTypeInfo(use = Id.NAME ,
                include = JsonTypeInfo.As.PROPERTY,
                property = "classType")

@JsonSubTypes({
    @Type(value=NewsDayAchieve.class , name = "NEWS_Achieve_Day" ),
    @Type(value=NewsWeekAchieve.class , name = "NEWS_Achieve_Week") ,
    @Type(value=NewsFriend.class , name = "NEWS_FRIEND"),
    @Type(value=NewsProgramChange.class , name = "NEWS_CHANGE_EXERCISEPROGRAM")
})

public abstract class AddedContens {

}
