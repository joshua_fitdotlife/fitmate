package com.fitdotlife.fitmate_lib.object;

import java.util.List;

/**
 * Created by Joshua on 2015-08-03.
 */
public class FriendRequestList {

    private List<UserFriend> receiveList;
    private List<UserFriend> sendList;

    public List<UserFriend> getSendList() {
        return sendList;
    }

    public void setSendList(List<UserFriend> sendList) {
        this.sendList = sendList;
    }

    public List<UserFriend> getReceiveList() {
        return receiveList;
    }

    public void setReceiveList(List<UserFriend> receiveList) {
        this.receiveList = receiveList;
    }
}
