package com.fitdotlife.fitmate_lib.object;

/**
 * Created by Joshua on 2015-02-02.
 */
public class MonthActivity {

    public static final String MONTH_ACTIVITY_ACTIVITY_DATE_KEY = "activitydate";
    public static final String MONTH_ACTIVITY_SCORE_LIST_KEY = "scorelist";
    public static final String MONTH_ACTIVITY_CALORIE_LIST_KEY = "calorielist";
    public static final String MONTH_ACTIVITY_AVERAGE_SCORE_KEY = "averagescore";
    public static final String MONTH_ACTIVITY_MONTH_LIST_KEY = "monthlist";

    private int[] scoreList = null;
    private int[] calorieList = null;
    private int averageScore = 0;

    public int getAverageScore() {
        return averageScore;
    }

    public void setAverageScore(int averageScore) {
        this.averageScore = averageScore;
    }

    public int[] getCalorieList() {
        return calorieList;
    }

    public void setCalorieList(int[] calorieList) {
        this.calorieList = calorieList;
    }

    public int[] getScoreList() {
        return scoreList;
    }

    public void setScoreList(int[] scoreList) {
        this.scoreList = scoreList;
    }
}
