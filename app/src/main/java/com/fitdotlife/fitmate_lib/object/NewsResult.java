package com.fitdotlife.fitmate_lib.object;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Joshua on 2015-09-25.
 */
public class NewsResult {

    private List<News> newsList;
    private String responsevalue = "";
    private String comment = "";

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public List<News> getNewsList() {
        return newsList;
    }

    public void setNewsList(List<News> newsList) {
        this.newsList = newsList;
    }

    public String getResponsevalue() {
        return responsevalue;
    }

    public void setResponsevalue(String responsevalue) {
        this.responsevalue = responsevalue;
    }
}
