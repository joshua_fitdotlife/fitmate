package com.fitdotlife.fitmate_lib.object;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Joshua on 2015-09-01.
 */
public class DayActivity_Rest {

    private String date;
    private float averageMet;
    private int calorieByActivity;
    private int strengthHigh;
    private int strengthMedium;
    private int strengthLow;
    private int strengthUnderLow;
    private int achieveOfTarget;
    private String metArray;
    private int exerciseProgramID;
    private int todayScore;
    private int predayScore;
    private int extraScore;
    private boolean achieveMax;
    private int possibleMaxscore;
    private int possibleTimes;
    private int possibleValue;
    private int dataSavingInterval;
    private float fat;
    private float carbohydrate;
    private double distance;

    @JsonProperty("interval")
    public int getDataSavingInterval() {
        return dataSavingInterval;
    }
    @JsonProperty("interval")
    public void setDataSavingInterval(int dataSavingInterval) {
        this.dataSavingInterval = dataSavingInterval;
    }
    @JsonProperty("date")
    public String getDate() {
        return date;
    }
    @JsonProperty("date")
    public void setDate(String date) {
        this.date = date;
    }
    @JsonProperty("calorie")
    public int getCalorieByActivity() {
        return calorieByActivity;
    }
    @JsonProperty("calorie")
    public void setCalorieByActivity(int calorieByActivity) {
        this.calorieByActivity = calorieByActivity;
    }
    @JsonProperty("averagemet")
    public float getAverageMet() {
        return averageMet;
    }
    @JsonProperty("averagemet")
    public void setAverageMet(float averageMet) {
        this.averageMet = averageMet;
    }
    @JsonProperty("strengthhigh")
    public int getStrengthHigh() {
        return strengthHigh;
    }
    @JsonProperty("strengthhigh")
    public void setStrengthHigh(int strengthHigh) {
        this.strengthHigh = strengthHigh;
    }
    @JsonProperty("strengthmedium")
    public int getStrengthMedium() {
        return strengthMedium;
    }
    @JsonProperty("strengthmedium")
    public void setStrengthMedium(int strengthMedium) {
        this.strengthMedium = strengthMedium;
    }
    @JsonProperty("strengthlow")
    public int getStrengthLow() {
        return strengthLow;
    }
    @JsonProperty("strengthlow")
    public void setStrengthLow(int strengthLow) {
        this.strengthLow = strengthLow;
    }
    @JsonProperty("strengthunder")
    public int getStrengthUnderLow() {
        return strengthUnderLow;
    }
    @JsonProperty("strengthunder")
    public void setStrengthUnderLow(int strengthUnderLow) {
        this.strengthUnderLow = strengthUnderLow;
    }
    @JsonProperty("achieve")
    public int getAchieveOfTarget() {
        return achieveOfTarget;
    }
    @JsonProperty("achieve")
    public void setAchieveOfTarget(int achieveOfTarget) {
        this.achieveOfTarget = achieveOfTarget;
    }
    @JsonProperty("metdataarray")
    public String getMetArray() {
        return metArray;
    }
    @JsonProperty("metdataarray")
    public void setMetArray(String metArray) {
        this.metArray = metArray;
    }
    @JsonProperty("exprogramid")
    public int getExerciseProgramID() {
        return exerciseProgramID;
    }
    @JsonProperty("exprogramid")
    public void setExerciseProgramID(int exerciseProgramID) {
        this.exerciseProgramID = exerciseProgramID;
    }
    @JsonProperty("todayscore")
    public int getTodayScore() {
        return todayScore;
    }
    @JsonProperty("todayscore")
    public void setTodayScore(int todayScore) {
        this.todayScore = todayScore;
    }
    @JsonProperty("predayscore")
    public int getPredayScore() {
        return predayScore;
    }
    @JsonProperty("predayscore")
    public void setPredayScore(int predayScore) {
        this.predayScore = predayScore;
    }
    @JsonProperty("extrascore")
    public int getExtraScore() {
        return extraScore;
    }
    @JsonProperty("extrascore")
    public void setExtraScore(int extraScore) {
        this.extraScore = extraScore;
    }
    @JsonProperty("possiblemaxscore")
    public int getPossibleMaxscore() {
        return possibleMaxscore;
    }
    @JsonProperty("possiblemaxscore")
    public void setPossibleMaxscore(int possibleMaxscore) {
        this.possibleMaxscore = possibleMaxscore;
    }
    @JsonProperty("possibletimes")
    public int getPossibleTimes() {
        return possibleTimes;
    }
    @JsonProperty("possibletimes")
    public void setPossibleTimes(int possibleTimes) {
        this.possibleTimes = possibleTimes;
    }
    @JsonProperty("possiblevalue")
    public int getPossibleValue() {
        return possibleValue;
    }
    @JsonProperty("possiblevalue")
    public void setPossibleValue(int possibleValue) {
        this.possibleValue = possibleValue;
    }
    @JsonProperty("achievemax")
    public boolean isAchieveMax() {
        return achieveMax;
    }
    @JsonProperty("achievemax")
    public void setAchieveMax(boolean achieveMax) {
        this.achieveMax = achieveMax;
    }

    @JsonProperty("carbohydrate")
    public float getCarbohydrate() { return carbohydrate;}
    @JsonProperty("carbohydrate")
    public void setCarbohydrate(float carbohydrate) { this.carbohydrate = carbohydrate; }
    @JsonProperty("distance")
    public double getDistance() { return distance;}
    @JsonProperty("distance")
    public void setDistance(double distance) { this.distance = distance;}
    @JsonProperty("fat")
    public float getFat() { return fat; }
    @JsonProperty("fat")
    public void setFat(float fat) { this.fat = fat;}

    public static byte[] convertFloatArrayToByteArray( float[] arrFloat ){
        byte[] arrTempByte = new byte[ arrFloat.length * 2 ];

        for( int i = 0 ; i < arrFloat.length ;i++ ){
            arrTempByte[2 * i] = (byte)((byte)( Math.floor( arrFloat[i] )) & 0xFF);
            arrTempByte[2 * i + 1 ] = (byte) ((byte) Math.round( ( arrFloat[i] - arrTempByte[ 2 * i ] ) * 100 ) & 0xFF);
        }

        return arrTempByte;
    }

    public static float[] convertByteArrayToFloatArray( byte[] arrByte ){
        float[] metFloatArray = new float[ arrByte.length / 2 ];

        for( int i = 0 ; i < metFloatArray.length ;i++ ){

            float met = (float) ( Math.round( arrByte[2 * i ] * 100d  +  arrByte[ 2 * i + 1] ) / 100d );
            metFloatArray[i] = met;
        }

        return metFloatArray;
    }

}
