package com.fitdotlife.fitmate_lib.object;

import com.fitdotlife.fitmate_lib.service.protocol.object.TimeInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Joshua on 2015-02-02.
 */
public class WeekActivity {

    public static final String WEEK_ACTIVITY_DATE_KEY = "date";
    public static final String WEEK_ACTIVITY_SCORE_KEY = "achieve";
    public static final String WEEK_ACTIVITY_EXERCISE_TIME_LIST_KEY = "exercisetimelist";
    public static final String WEEK_ACTIVITY_AVERAGE_STRENGTH_HIGH_KEY = "strengthhigh";
    public static final String WEEK_ACTIVITY_AVERAGE_STRENGTH_MEDIUM_KEY = "strengthmedium";
    public static final String WEEK_ACTIVITY_AVERAGE_STRENGTH_LOW_KEY = "strengthlow";
    public static final String WEEK_ACTIVITY_AVERAGE_STRENGTH_UNDER_LOW_KEY = "strengthunder";
    public static final String WEEK_ACTIVITY_AVERAGE_MET_KEY = "averagemet";
    public static final String WEEK_ACTIVITY_CALORIE_KEY = "calorie";

    private String activityDate = null;
    private int score = 0;
    private int averageCalorie = 0;
    private float averageMET = 0;
    private int averageStrengthHigh = 0;
    private int averageStrengthMedium = 0;
    private int averageStrengthLow = 0;
    private int averageStrengthUnderLow = 0;

    private int exerciseProgramID = 0;

    public int getAverageCalorie() {
        return averageCalorie;
    }

    public void setAverageCalorie(int averageCalorie) {
        this.averageCalorie = averageCalorie;
    }

    public float getAverageMET() {
        return averageMET;
    }

    public void setAverageMET(float averageMET) {
        this.averageMET = averageMET;
    }

    public int getAverageStrengthHigh() {
        return averageStrengthHigh;
    }

    public void setAverageStrengthHigh(int averageStrengthHigh) {
        this.averageStrengthHigh = averageStrengthHigh;
    }

    public int getAverageStrengthMedium() {
        return averageStrengthMedium;
    }

    public void setAverageStrengthMedium(int averageStrengthMedium) {
        this.averageStrengthMedium = averageStrengthMedium;
    }

    public int getAverageStrengthLow() {
        return averageStrengthLow;
    }

    public void setAverageStrengthLow(int averageStrengthLow) {
        this.averageStrengthLow = averageStrengthLow;
    }

    public int getAverageStrengthUnderLow() {
        return averageStrengthUnderLow;
    }

    public void setAverageStrengthUnderLow(int averageStrengthUnderLow) {
        this.averageStrengthUnderLow = averageStrengthUnderLow;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getActivityDate() {
        return activityDate;
    }

    public void setActivityDate(String activityDate) {
        this.activityDate = activityDate;
    }

    public int getExerciseProgramID() {
        return exerciseProgramID;
    }

    public void setExerciseProgramID(int exerciseProgramID) {
        this.exerciseProgramID = exerciseProgramID;
    }

    public JSONObject toJSONObject( ) {

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put( ExerciseProgram.ID_KEY , this.getExerciseProgramID()  );
            jsonObject.put(WEEK_ACTIVITY_CALORIE_KEY, this.getAverageCalorie() );
            jsonObject.put(WEEK_ACTIVITY_AVERAGE_MET_KEY, this.getAverageMET());
            jsonObject.put(WEEK_ACTIVITY_AVERAGE_STRENGTH_HIGH_KEY, this.getAverageStrengthHigh());
            jsonObject.put(WEEK_ACTIVITY_AVERAGE_STRENGTH_MEDIUM_KEY, this.getAverageStrengthMedium());
            jsonObject.put(WEEK_ACTIVITY_AVERAGE_STRENGTH_LOW_KEY, this.getAverageStrengthLow());
            jsonObject.put(WEEK_ACTIVITY_AVERAGE_STRENGTH_UNDER_LOW_KEY, this.getAverageStrengthUnderLow());
            jsonObject.put(WEEK_ACTIVITY_SCORE_KEY, this.getScore());
            //tick
           // jsonObject.put(WEEK_ACTIVITY_DATE_KEY, this.getTimeMilliSecond(this.getActivityDate()));
            //yyyy-MM-dd
            jsonObject.put(WEEK_ACTIVITY_DATE_KEY, this.getActivityDate());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    public static WeekActivity toWeekActivity( JSONObject jsonObject ){
        WeekActivity weekActivity = null;

        try {
            weekActivity = new WeekActivity();
            //tick
            //weekActivity.setActivityDate(new TimeInfo(jsonObject.getLong("date")).getDateString_yyyy_MM_dd());
            //yyyy-MM-dd
            weekActivity.setActivityDate(  jsonObject.getString("date"));
            weekActivity.setAverageCalorie( jsonObject.getInt("calorie"));
            weekActivity.setAverageMET((float) jsonObject.getDouble( "averagemet" ));
            weekActivity.setAverageStrengthHigh( jsonObject.getInt("strengthhigh") );
            weekActivity.setAverageStrengthLow( jsonObject.getInt( "strengthlow" ));
            weekActivity.setAverageStrengthMedium( jsonObject.getInt("strengthmedium") );
            weekActivity.setAverageStrengthUnderLow( jsonObject.getInt("strengthunder"));
            weekActivity.setScore( jsonObject.getInt("achieve") );
            weekActivity.setExerciseProgramID( jsonObject.getInt("exprogramid") );
        }catch( JSONException e ){

        }

        return weekActivity;
    }

    private long getTimeMilliSecond( String strDate ){

        DateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date tempDate  = null;
        try {
            tempDate = sdFormat.parse( strDate );
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime( tempDate );

        return calendar.getTimeInMillis();
    }


}
