package com.fitdotlife.fitmate_lib.object;

import android.os.Bundle;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.WeakHashMap;

/**
 * Created by Joshua on 2015-02-10.
 */
public class ExerciseProgram {

    public static final String ID_KEY = "exprogramid";
    public static final String NAME_KEY = "name";
    public static final String CATEGORY_KEY = "category";
    public static final String STRENGTH_FROM_KEY = "strengthfrom";
    public static final String STRENGTH_TO_KEY = "strengthto";
    public static final String MINUTE_FROM_KEY = "minutefrom";
    public static final String MINUTE_TO_KEY = "minuteto";
    public static final String TIMES_FROM_KEY = "timesfrom";
    public static final String TIMES_TO_KEY = "timesto";
    public static final String CONTINUOUS_EXERCISE_KEY = "continuousexercise";
    public static final String OWNER_KEY = "owner";
    public static final String INFO_KEY = "info";
    public static final String RECOMMEND_NUMBER_KEY = "recommendnumber";
    public static final String PUBLISH_TYPE_KEY = "publishtype";

    public static final String NUMBER_OF_PROGRAMS_KEY = "numberofprograms";
    public static final String CONTAIN_KEY = "contain";
    public static final String PROGRAMLIST_KEY = "programlist";

    public static final String ID_API_KEY = "ID";
    public static final String NAME_API_KEY = "Name";
    public static final String STRENGTHTYPE_API_KEY = "StrengthInputType";
    public static final String STRENGTHFROM_API_KEY = "StrengthFrom";
    public static final String STRENGTHTO_API_KEY = "StrengthTo";
    public static final String TIMESFROMWEEEK_API_KEY = "TimesFromPerWeek";
    public static final String TIMESTOWEEK_API_KEY = "TimesToPerWeek";
    public static final String MINUTESFROM_API_KEY = "MinutesFrom";
    public static final String MINUTESTO_API_KEY = "MinutesTo";
    public static final String CONTINUOUSEX_API_KEY = "CheckContinuousEx";
    public static final String TIMESFROMDAY_API_KEY = "TimesPerADayFrom";
    public static final String TIMESTODAY_API_KEY = "TimesPerADayTo";
    public static final String INTRO_API_KEY = "Intro";

    private int id;
    private String name;
    private int category;
    private int minuteFrom;
    private int minuteTo;
    private float strengthFrom;
    private float strengthTo;
    private int timesFrom;
    private int timesTo;
    private int recommendNumber;
    private String info;
    private int continuousExercise;
    private String owner;
    private int publishType;
    private int diseaseCategory;

    private boolean defaultProgram;

    private boolean applied = false;

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMinuteFrom() {
        return minuteFrom;
    }

    public void setMinuteFrom(int minuteFrom) {
        this.minuteFrom = minuteFrom;
    }

    public int getMinuteTo() {
        return minuteTo;
    }

    public void setMinuteTo(int minuteTo) {
        this.minuteTo = minuteTo;
    }

    public float getStrengthFrom() {
        return strengthFrom;
    }
    public float getStrengthTo( ) {

        return strengthTo;
    }
    public boolean getDefaultProgram() {
        return defaultProgram;
    }

    public void setDefaultProgram( boolean defaultProgram) {
        this.defaultProgram = defaultProgram;
    }


    public void setStrengthFrom(float strengthFrom) {
        this.strengthFrom = strengthFrom;
    }

    public float getStrengthFrom(float weight) {
        return (49.6f + 20) * weight /  7;
    }
    public float getStrengthTo(float weight) {
        return (49.6f + 20) * weight /  7;
    }

    public void setStrengthTo(float strengthTo) {
        this.strengthTo = strengthTo;
    }

    public int getTimesFrom() {
        return timesFrom;
    }

    public void setTimesFrom(int timesFrom) {
        this.timesFrom = timesFrom;
    }

    public int getTimesTo() {
        return timesTo;
    }

    public void setTimesTo(int timesTo) {
        this.timesTo = timesTo;
    }

    public int getRecommendNumber() {
        return recommendNumber;
    }

    public void setRecommendNumber(int recommendNumber) {
        this.recommendNumber = recommendNumber;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public int getContinuousExercise() {
        return continuousExercise;
    }

    public void setContinuousExercise(int continuousExercise) {
        this.continuousExercise = continuousExercise;
    }

    public boolean isApplied() {
        return applied;
    }

    public void setApplied(boolean applied) {
        this.applied = applied;
    }

    public int getDiseaseCategory() {
        return diseaseCategory;
    }

    public void setDiseaseCategory(int diseaseCategory) {
        this.diseaseCategory = diseaseCategory;
    }

    public Bundle toBundle(){

        Bundle exerciseProgramBundle = new Bundle();
        exerciseProgramBundle.putString(this.NAME_KEY , this.name);
        exerciseProgramBundle.putInt(this.CATEGORY_KEY, this.category);
        exerciseProgramBundle.putFloat(this.STRENGTH_FROM_KEY , this.strengthFrom);
        exerciseProgramBundle.putFloat(this.STRENGTH_TO_KEY , this.strengthTo);
        exerciseProgramBundle.putInt(this.TIMES_FROM_KEY, this.timesFrom);
        exerciseProgramBundle.putInt(this.TIMES_TO_KEY , this.timesTo);
        exerciseProgramBundle.putInt(this.MINUTE_FROM_KEY , this.minuteFrom);
        exerciseProgramBundle.putInt(this.MINUTE_TO_KEY , this.minuteTo);
        exerciseProgramBundle.putInt(this.CONTINUOUS_EXERCISE_KEY , this.continuousExercise);

        return exerciseProgramBundle;
    }


    public WeakHashMap<String, String> toParameters() {
        WeakHashMap<String , String> parameters = new WeakHashMap<String, String>();

        return parameters;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public int getPublishType() {
        return publishType;
    }

    public void setPublishType(int publishType) {
        this.publishType = publishType;
    }

    public static ExerciseProgram getExerciseProgram( String programJSONString ) throws JSONException {

        JSONObject programObject = new JSONObject( programJSONString );
        ExerciseProgram program = new ExerciseProgram();
        program.setId(programObject.getInt(ID_API_KEY));
        program.setName(programObject.getString(NAME_API_KEY));
        program.setCategory(programObject.getInt(STRENGTHTYPE_API_KEY));
        program.setStrengthFrom(Float.parseFloat(programObject.getString(STRENGTHFROM_API_KEY)));
        program.setStrengthTo(Float.parseFloat(programObject.getString(STRENGTHTO_API_KEY)));
        program.setTimesFrom(programObject.getInt(TIMESFROMWEEEK_API_KEY));
        program.setTimesTo(programObject.getInt(TIMESTOWEEK_API_KEY));
        program.setMinuteFrom(programObject.getInt(MINUTESFROM_API_KEY));
        program.setMinuteTo(programObject.getInt(MINUTESTO_API_KEY));
        program.setContinuousExercise( programObject.getBoolean(CONTINUOUSEX_API_KEY) ? 1 : 0  );
        program.setInfo(  programObject.getString( INTRO_API_KEY ) );
        program.setDefaultProgram( false );
        return program;
    }


}
