package com.fitdotlife.fitmate_lib.object;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Joshua on 2015-03-19.
 */
public class UserFriend implements Parcelable {

    private int requestType = 0;

    private String jaeum = null;
    private String email = null;
    private String name = null;
    private String birthDate = null;
    private boolean sex = false;
    private String profileImagePath = null;
    private String introYourSelf = null;
    private int exerciseProgramID = 0;
    private boolean si = true;
    private float height = 0;
    private float weight = 0;
    private int wearingLocation = 0;

    private int ID = 0;
    private int userID = 0;
    private int friendID = 0;
    private int userFriendsID = 0;
    private String agreeDate = null;
    private int delFlag = 0;
    private int requestStatus = 0;
    private boolean userConfirm = false;
    private boolean friendConfirm = false;
    private boolean isCheckedRelationUser = false;
    private boolean isCheckedRelationFriend = false;
    private boolean isCheckedRequestUser = false;
    private boolean isCheckedRequestFriend = false;
    private boolean userAlarm = false;
    private boolean friendAlarm = false;

    public String getAgreeDate() {
        return agreeDate;
    }
    @JsonProperty("AgreeDate")
    public void setAgreeDate(String agreeDate) {
        this.agreeDate = agreeDate;
    }

    public String getBirthDate() {
        return birthDate;
    }
    @JsonProperty("BirthDate")
    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public int getDelFlag() {
        return delFlag;
    }
    @JsonProperty("DelFlag")
    public void setDelFlag(int delFlag) {
        this.delFlag = delFlag;
    }

    public String getEmail() {
        return email;
    }
    @JsonProperty("Email")
    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isFriendConfirm() {
        return friendConfirm;
    }
    @JsonProperty("FriendConfirm")
    public void setFriendConfirm(boolean friendConfirm) {
        this.friendConfirm = friendConfirm;
    }

    public int getFriendID() {
        return friendID;
    }
    @JsonProperty("FriendID")
    public void setFriendID(int friendID) {
        this.friendID = friendID;
    }

    public int getID() {
        return ID;
    }

    @JsonProperty("ID")
    public void setID(int ID) {
        this.ID = ID;
    }

    public String getIntroYourSelf() {
        return introYourSelf;
    }
    @JsonProperty("IntroYourSelf")
    public void setIntroYourSelf(String introYourSelf) {
        this.introYourSelf = introYourSelf;
    }

    public boolean isCheckedRelationFriend() {
        return isCheckedRelationFriend;
    }

    @JsonProperty("IsCheckedRelationFriend")
    public void setIsCheckedRelationFriend(boolean isCheckedRelationFriend) {
        this.isCheckedRelationFriend = isCheckedRelationFriend;
    }

    public boolean isCheckedRelationUser() {
        return isCheckedRelationUser;
    }

    @JsonProperty("IsCheckedRelationUser")
    public void setIsCheckedRelationUser(boolean isCheckedRelationUser) {
        this.isCheckedRelationUser = isCheckedRelationUser;
    }

    public boolean isCheckedRequestFriend() {
        return isCheckedRequestFriend;
    }

    @JsonProperty("IsCheckedRequestFriend")
    public void setIsCheckedRequestFriend(boolean isCheckedRequestFriend) {
        this.isCheckedRequestFriend = isCheckedRequestFriend;
    }

    public boolean isCheckedRequestUser() {
        return isCheckedRequestUser;
    }

    @JsonProperty("IsCheckedRequestUser")
    public void setIsCheckedRequestUser(boolean isCheckedRequestUser) {
        this.isCheckedRequestUser = isCheckedRequestUser;
    }

    public String getJaeum() {
        return jaeum;
    }

    public boolean isSi() {
        return si;
    }

    @JsonProperty("IsSI")
    public void setSi(boolean si) {
        this.si = si;
    }

    @JsonProperty("Jaeum")
    public void setJaeum(String jaeum) {
        this.jaeum = jaeum;
    }

    public String getName() {
        return name;
    }

    @JsonProperty("Name")
    public void setName(String name) {
        this.name = name;
    }

    public String getProfileImagePath() {
        return profileImagePath;
    }

    @JsonProperty("ProfileImagePath")
    public void setProfileImagePath(String profileImagePath) {
        this.profileImagePath = profileImagePath;
    }

    public int getRequestStatus() {
        return requestStatus;
    }

    @JsonProperty("RequestStatus")
    public void setRequestStatus(int requestStatus) {
        this.requestStatus = requestStatus;
    }

    public int getRequestType() {
        return requestType;
    }

    @JsonProperty("RequestType")
    public void setRequestType(int requestType) {
        this.requestType = requestType;
    }

    public boolean isSex() {
        return sex;
    }

    @JsonProperty("Sex")
    public void setSex(boolean sex) {
        this.sex = sex;
    }

    public boolean isUserConfirm() {
        return userConfirm;
    }

    @JsonProperty("UserConfirm")
    public void setUserConfirm(boolean userConfirm) {
        this.userConfirm = userConfirm;
    }

    public int getUserFriendsID() {
        return userFriendsID;
    }

    @JsonProperty("UserFriendsID")
    public void setUserFriendsID(int userFriendsID) {
        this.userFriendsID = userFriendsID;
    }

    public int getUserID() {
        return userID;
    }

    @JsonProperty("UserID")
    public void setUserID(int userID) {
        this.userID = userID;
    }

    public boolean isFriendAlarm() {
        return friendAlarm;
    }

    @JsonProperty("FriendAlarm")
    public void setFriendAlarm(boolean friendAlarm) {
        this.friendAlarm = friendAlarm;
    }

    public boolean isUserAlarm() {
        return userAlarm;
    }

    @JsonProperty("UserAlarm")
    public void setUserAlarm(boolean userAlarm) {
        this.userAlarm = userAlarm;
    }

    public int getExerciseProgramID( ){ return this.exerciseProgramID; }
    @JsonProperty("ExerciseProgramID")
    public void setExerciseProgramID( int exerciseProgramID ){ this.exerciseProgramID = exerciseProgramID; }
    public float getHeight() {
        return height;
    }

    @JsonProperty("Height")
    public void setHeight(float height) {
        this.height = height;
    }

    public float getWeight() {
        return weight;
    }
    @JsonProperty("Weight")
    public void setWeight(float weight) {
        this.weight = weight;
    }

    public int getWearingLocation() {
        return wearingLocation;
    }
    @JsonProperty("WearingLocation")
    public void setWearingLocation(int wearingLocation) {
        this.wearingLocation = wearingLocation;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.requestType);
        dest.writeString(this.jaeum);
        dest.writeString(this.email);
        dest.writeString(this.name);
        dest.writeString(this.birthDate);
        dest.writeByte(sex ? (byte) 1 : (byte) 0);
        dest.writeString(this.profileImagePath);
        dest.writeString(this.introYourSelf);
        dest.writeInt(this.ID);
        dest.writeInt(this.userID);
        dest.writeInt(this.friendID);
        dest.writeInt(this.userFriendsID);
        dest.writeString(this.agreeDate);
        dest.writeInt(this.delFlag);
        dest.writeInt(this.requestStatus);
        dest.writeByte(userConfirm ? (byte) 1 : (byte) 0);
        dest.writeByte(friendConfirm ? (byte) 1 : (byte) 0);
        dest.writeByte(isCheckedRelationUser ? (byte) 1 : (byte) 0);
        dest.writeByte(isCheckedRelationFriend ? (byte) 1 : (byte) 0);
        dest.writeByte(isCheckedRequestUser ? (byte) 1 : (byte) 0);
        dest.writeByte(isCheckedRequestFriend ? (byte) 1 : (byte) 0);
        dest.writeByte(userAlarm ? (byte) 1 : (byte) 0);
        dest.writeByte(friendAlarm ? (byte) 1 : (byte) 0);
        dest.writeInt(this.exerciseProgramID);
        dest.writeByte(si ? (byte) 1 : (byte) 0);
        dest.writeFloat(this.height);
        dest.writeFloat(this.weight);
        dest.writeInt(this.wearingLocation);
    }

    public UserFriend() {
    }

    protected UserFriend(Parcel in) {
        this.requestType = in.readInt();
        this.jaeum = in.readString();
        this.email = in.readString();
        this.name = in.readString();
        this.birthDate = in.readString();
        this.sex = in.readByte() != 0;
        this.profileImagePath = in.readString();
        this.introYourSelf = in.readString();
        this.ID = in.readInt();
        this.userID = in.readInt();
        this.friendID = in.readInt();
        this.userFriendsID = in.readInt();
        this.agreeDate = in.readString();
        this.delFlag = in.readInt();
        this.requestStatus = in.readInt();
        this.userConfirm = in.readByte() != 0;
        this.friendConfirm = in.readByte() != 0;
        this.isCheckedRelationUser = in.readByte() != 0;
        this.isCheckedRelationFriend = in.readByte() != 0;
        this.isCheckedRequestUser = in.readByte() != 0;
        this.isCheckedRequestFriend = in.readByte() != 0;
        this.userAlarm = in.readByte() != 0;
        this.friendAlarm = in.readByte() != 0;
        this.exerciseProgramID = in.readInt();
        this.si = in.readByte() != 0;
        this.height = in.readFloat();
        this.weight = in.readFloat();
        this.wearingLocation = in.readInt();
    }

    public static final Parcelable.Creator<UserFriend> CREATOR = new Parcelable.Creator<UserFriend>() {
        public UserFriend createFromParcel(Parcel source) {
            return new UserFriend(source);
        }

        public UserFriend[] newArray(int size) {
            return new UserFriend[size];
        }
    };
}