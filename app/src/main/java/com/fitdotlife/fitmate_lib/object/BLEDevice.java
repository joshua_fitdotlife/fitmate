package com.fitdotlife.fitmate_lib.object;

import android.bluetooth.BluetoothDevice;

/**
 * Created by Joshua on 2016-07-01.
 */
public class BLEDevice {

    public static final int NO_PUSH_BUTTON = 0;
    public static final int PUSH_BUTTON_PRESSED = 1;
    public static final int PUSH_BUTTON_NORMAL = 2;

    private String serialNumber = null;
    private BluetoothDevice bluetoothDevice = null;
    private int rssi = 0;
    private int isPushButton = NO_PUSH_BUTTON;

    public BLEDevice(BluetoothDevice bluetoothDevice, String serialNumber , int rssi) {
        this.bluetoothDevice = bluetoothDevice;
        this.serialNumber = serialNumber;
        this.rssi = rssi;
    }

    public BLEDevice(BluetoothDevice bluetoothDevice, String serialNumber , int rssi , int isPushButton) {
        this.bluetoothDevice = bluetoothDevice;
        this.serialNumber = serialNumber;
        this.rssi = rssi;
        this.isPushButton = isPushButton;
    }

    public BluetoothDevice getBluetoothDevice() {
        return bluetoothDevice;
    }

    public void setBluetoothDevice(BluetoothDevice bluetoothDevice) {
        this.bluetoothDevice = bluetoothDevice;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public int getRssi() {
        return rssi;
    }

    public void setRssi(int rssi) {
        this.rssi = rssi;
    }

    public int isPushButton() {
        return isPushButton;
    }

    public void setIsPushButton(int isPushButton) {
        this.isPushButton = isPushButton;
    }
}
