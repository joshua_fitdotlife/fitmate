package com.fitdotlife.fitmate_lib.object;

/**
 * Created by Joshua on 2016-01-11.
 */
public class DayAchieve {

    private String date;
    private String achieve;

    public String getAchieve() {
        return achieve;
    }

    public void setAchieve(String achieve) {

        if( achieve == null ){
            this.achieve = "0";
            return;
        }

        this.achieve = achieve;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
