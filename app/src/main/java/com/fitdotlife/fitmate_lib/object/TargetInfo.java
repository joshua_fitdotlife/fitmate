package com.fitdotlife.fitmate_lib.object;

/**
 * Created by Joshua on 2016-01-11.
 */
public class TargetInfo {

    private int rangeFrom;
    private int rangeTo;
    private int rangeUnitType;

    public int getRangeFrom() {
        return rangeFrom;
    }

    public void setRangeFrom(int rangeFrom) {
        this.rangeFrom = rangeFrom;
    }

    public int getRangeTo() {
        return rangeTo;
    }

    public void setRangeTo(int rangeTo) {
        this.rangeTo = rangeTo;
    }

    public int getRangeUnitType() {
        return rangeUnitType;
    }

    public void setRangeUnitType(int rangeUnitType) {
        this.rangeUnitType = rangeUnitType;
    }
}
