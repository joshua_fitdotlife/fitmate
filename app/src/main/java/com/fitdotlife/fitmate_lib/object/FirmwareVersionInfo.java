package com.fitdotlife.fitmate_lib.object;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Joshua on 2016-06-30.
 */
public class FirmwareVersionInfo {

    public static final String APP_MAJORVERSION_KEY = "app_majorversion";
    public static final String APP_MINORVERSION_KEY = "app_minorversion";
    public static final String APP_DFUFILENAME_KEY = "app_filename";

    public static final String DEVICE_MAJORVERSION_KEY = "device_majorversion";
    public static final String DEVICE_MINORVERSION_KEY = "device_minorversion";

    private int majorVersion = 0;
    private int minorVersion = 0;
    private String fileName = null;
    private String fileHash = null;
    private long updateDate = 0;
    private String modelNumber = null;

    @JsonProperty("hash")
    public String getFileHash() {
        return fileHash;
    }
    @JsonProperty("hash")
    public void setFileHash(String fileHash) {
        this.fileHash = fileHash;
    }

    @JsonProperty("filename")
    public String getFileName() {
        return fileName;
    }

    @JsonProperty("filename")
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @JsonProperty("version_major")
    public int getMajorVersion() {
        return majorVersion;
    }

    @JsonProperty("version_major")
    public void setMajorVersion(int majorVersion) {
        this.majorVersion = majorVersion;
    }

    @JsonProperty("version_minor")
    public int getMinorVersion() {
        return minorVersion;
    }

    @JsonProperty("version_minor")
    public void setMinorVersion(int minorVersion) {
        this.minorVersion = minorVersion;
    }

    @JsonProperty("modelnumber")
    public String getModelNumber() {
        return modelNumber;
    }

    @JsonProperty("modelnumber")
    public void setModelNumber(String modelNumber) {
        this.modelNumber = modelNumber;
    }

    @JsonProperty("updated")
    public long getUpdateDate() {
        return updateDate;
    }

    @JsonProperty("updated")
    public void setUpdateDate(long updateDate) {
        this.updateDate = updateDate;
    }
}
