package com.fitdotlife.fitmate_lib.key;

/**
 * Created by Joshua on 2016-08-11.
 */
public enum FitmeterType {
    BLE , BAND
}