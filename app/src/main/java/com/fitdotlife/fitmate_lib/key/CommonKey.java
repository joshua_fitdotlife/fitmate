package com.fitdotlife.fitmate_lib.key;

/**
 * Created by Joshua on 2015-03-09.
 */
public class CommonKey {

    public static final String COMMENT_VALUE_KEY = "comment";
    public static final String RESPONSE_VALUE_KEY = "responsevalue";
    public static final String SUCCESS = "SUCCESS";
    public static final String FAIL = "FAIL";
    public static final String NODATA = "NoData";
    public static final String RESULT_KEY = "result";
    public static final String SEARCH_WORD_KEY = "searchword";
    public static final String TRUE = "true";
    public static final String FALSE = "false";
    public static final String NULL = "null";

}
