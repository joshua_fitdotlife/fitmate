package com.fitdotlife.fitmate_lib.key;

/**
 * Created by Joshua on 2015-02-10.
 */
public class ExerciseProgramType {
    public static final int USEREXERCISEPROGRAM = 0;
    public static final int EXERCISEPROGRAM = 1;
}
