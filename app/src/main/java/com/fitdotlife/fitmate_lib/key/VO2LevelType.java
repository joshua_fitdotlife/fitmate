package com.fitdotlife.fitmate_lib.key;
/**
 * 운동프로그램에서 목표 강도의 범위를 입력할때 입력 형식을 나타낸다.
 * @author fitlife.soondong
 *
 */
public enum VO2LevelType {
	/**
	 * maximal oxygen consumption, maximal oxygen uptake, peak oxygen uptake or maximal aerobic capacity
	 */
	SUPERIOR(0),
	/**
	 * relative exercise intensities
	 */
    EXCELLENT(1),
    /*&
     * rating of perceived exertion
     */
    GDDD(2),
    /**
     * Metabolic Equivalent of Task
     */

    FAIR(3),
    /**
     * classify four level of intensity- underlow, low, medium, high
     */
    POOR(4),
    /**
     * sum of calorie of a day
     */
    VERYPOOR(5);

    private int mValue;
    private VO2LevelType(int value)
    {
       this.mValue = value;
    }

    public int getValue(){
        return this.mValue;
    }

    public static VO2LevelType getVO2LevelType( int value ){
        VO2LevelType vo2LevelType = null;

            switch( value ){
                case 0:
                    vo2LevelType = VO2LevelType.SUPERIOR;
                    break;
                case 1:
                    vo2LevelType = VO2LevelType.EXCELLENT;
                    break;
                case 2:
                    vo2LevelType = VO2LevelType.GDDD;
                    break;
                case 3:
                    vo2LevelType = VO2LevelType.FAIR;
                    break;
                case 4:
                    vo2LevelType = VO2LevelType.POOR;
                    break;
                case 5:
                    vo2LevelType = VO2LevelType.VERYPOOR;
                    break;
            }

        return vo2LevelType;
    }

    public String getVO2LevelString( ){
        String vo2LevelString = null;

        switch( mValue ){
            case 0:
                vo2LevelString = "superior";
                break;
            case 1:
                vo2LevelString = "excellent";
                break;
            case 2:
                vo2LevelString = "good";
                break;
            case 3:
                vo2LevelString = "fair";
                break;
            case 4:
                vo2LevelString = "poor";
                break;
            case 5:
                vo2LevelString = "very poor";
                break;
        }

        return vo2LevelString;
    }


}
