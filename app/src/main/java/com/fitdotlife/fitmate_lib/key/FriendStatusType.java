package com.fitdotlife.fitmate_lib.key;

/**
 * Created by Joshua on 2015-08-03.
 */
public enum FriendStatusType {
    NOVALUE(0) , REQUEST(1) , CANCLE(2) , ACCEPT(3) , DECLINE(4) , DELETE(5);


    private int mValue;

    FriendStatusType( int value )
    {
        this.mValue = value;
    }

    public int getValue()
    {
        return this.mValue;
    }


    public static FriendStatusType getFriendStatusType( int value )
    {
        FriendStatusType friendStatusType = null;

        switch( value )
        {
            case 0:
                friendStatusType = FriendStatusType.NOVALUE;
                break;
            case 1:
                friendStatusType = FriendStatusType.REQUEST;
                break;
            case 2:
                friendStatusType = FriendStatusType.CANCLE;
                break;
            case 3:
                friendStatusType = FriendStatusType.ACCEPT;
                break;
            case 4:
                friendStatusType = FriendStatusType.DECLINE;
                break;
            case 5:
                friendStatusType = FriendStatusType.DELETE;
                break;
        }

        return friendStatusType;
    }
}
