package com.fitdotlife.fitmate_lib.key;

import java.util.Random;

/**
 * Created by Joshua on 2016-03-22.
 */
public enum FatFoodType {
    BACON , SIRLOIN , CHOCOLATE , BIGMAC , ICECREAM , CHICKEN_LEG;


    public static float getFatContent( FatFoodType fatFoodType ){
        float fatContent = 0;

        switch( fatFoodType ){
            case BACON:
                fatContent = 25.5f;
                break;
            case SIRLOIN:
                fatContent = 32;
                break;
            case CHOCOLATE:
                fatContent = 34.2f;
                break;
            case BIGMAC:
                fatContent = 30.24f;
                break;
            case ICECREAM:
                fatContent = 12;
                break;
            case CHICKEN_LEG:
                fatContent = 6.81f;
                break;
        }

        return fatContent;
    }


    public static int getReferenceAmount( FatFoodType fatFoodType ){
        int referenceAmount = 0;

        switch( fatFoodType ){
            case BACON:
                referenceAmount = 100;
                break;
            case SIRLOIN:
                referenceAmount = 200;
                break;
            case CHOCOLATE:
                referenceAmount = 100;
                break;
            case BIGMAC:
                referenceAmount = 213;
                break;
            case ICECREAM:
                referenceAmount = 100;
                break;
            case CHICKEN_LEG:
                referenceAmount = 100;
                break;
        }

        return referenceAmount;
    }


    public static FatFoodType selectFatFoodType( double fat )
    {
        FatFoodType selectFatFoodType = null;
        int selectIndex = 0;

        Random random = new Random();

        if( fat < 40  )
        {
            //전체중에서 랜덤선택
            selectIndex = random.nextInt( FatFoodType.values().length - 1 );
        }else
        {
            //베이컨, 소등심, 초콜릿 , 빅맥 중에서 랜덤 선택
            selectIndex =  random.nextInt( BIGMAC.ordinal() );
        }

        selectFatFoodType = FatFoodType.values()[ selectIndex ];
        return selectFatFoodType;
    }

    public static int getFoodCount( FatFoodType fatFoodType , double fat )
    {
        long foodCount = 0;
        double foodCountRatio = (( FatFoodType.getReferenceAmount( fatFoodType ) * fat ) / FatFoodType.getFatContent( fatFoodType ) / FatFoodType.getReferenceAmount(fatFoodType));
        foodCount = Math.round(  foodCountRatio  );
        return (int) foodCount;
    }
}
