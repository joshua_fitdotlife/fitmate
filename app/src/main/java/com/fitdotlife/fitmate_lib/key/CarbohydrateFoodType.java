package com.fitdotlife.fitmate_lib.key;

import java.util.Random;

/**
 * Created by Joshua on 2016-03-24.
 */
public enum CarbohydrateFoodType {

    BIGMAC , RICE , KIMBOB  , CHOCOLATE , COKE , BREAD , FRENCH_FRY;


    public static float getCarbohydrateContent( CarbohydrateFoodType carbohydrateFoodType ){
        float carbohydrateContent = 0;

        switch( carbohydrateFoodType ){
            case BIGMAC:
                carbohydrateContent = 43.68f;
                break;
            case RICE:
                carbohydrateContent = 66.4f;
                break;
            case KIMBOB:
                carbohydrateContent = 57.57f;
                break;
            case CHOCOLATE:
                carbohydrateContent = 60.4f;
                break;
            case COKE:
                carbohydrateContent = 56;
                break;
            case BREAD:
                carbohydrateContent = 20.4f;
                break;
            case FRENCH_FRY:
                carbohydrateContent = 18.5f;
                break;
        }

        return carbohydrateContent;
    }


    public static int getReferenceAmount( CarbohydrateFoodType carbohydrateFoodType ){
        int referenceAmount = 0;

        switch( carbohydrateFoodType ){
            case BIGMAC:
                referenceAmount = 213;
                break;
            case RICE:
                referenceAmount = 200;
                break;
            case KIMBOB:
                referenceAmount = 200;
                break;
            case CHOCOLATE:
                referenceAmount = 100;
                break;
            case COKE:
                referenceAmount = 200;
                break;
            case BREAD:
                referenceAmount = 40;
                break;
            case FRENCH_FRY:
                referenceAmount = 100;
                break;
        }

        return referenceAmount;
    }


    public static CarbohydrateFoodType selectCarbohydrateFoodType( double carbohydrate )
    {
        CarbohydrateFoodType selectCarbohydrateFoodType = null;
        int selectIndex = 0;

        Random random = new Random();

        if( carbohydrate < 70  )
        {
            //전체중에서 랜덤선택
            selectIndex = random.nextInt( CarbohydrateFoodType.values().length - 1 );
        }else
        {
            //빅맥, 쌀밥, 김밥, 초콜릿, 콜라 중 랜덤 선택
            selectIndex =  random.nextInt( COKE.ordinal() );
        }

        selectCarbohydrateFoodType = CarbohydrateFoodType.values()[ selectIndex ];
        return selectCarbohydrateFoodType;
    }

    public static int getFoodCount( CarbohydrateFoodType carbohydrateFoodType , double carbohydrate )
    {
        long foodCount = 0;
        double foodCountRatio = (( CarbohydrateFoodType.getReferenceAmount( carbohydrateFoodType ) * carbohydrate ) / CarbohydrateFoodType.getCarbohydrateContent( carbohydrateFoodType ) / CarbohydrateFoodType.getReferenceAmount(carbohydrateFoodType));
        foodCount = Math.round(  foodCountRatio  );
        return (int) foodCount;
    }


}
