package com.fitdotlife.fitmate_lib.key;

import android.content.Context;

import com.fitdotlife.fitmate.R;

public enum GenderType
{
	MALE(1) ,
	FEMALE(0),
	NONE(2);
	
	private int mValue;
	
	GenderType( int value )
	{
		this.mValue = value;
	}
	
	public int getValue()
	{
		return this.mValue;
	}

    public String getBooleanString(){
        String result = null;
        if( mValue == 0 ) result = "false";
        if( mValue == 1 ) result = "true";

        return result;
    }

    public String getGenderString( Context context ){
        String result = null;
        if( mValue == 0 ) result = context.getResources().getString(R.string.common_female);
        if( mValue == 1 ) result = context.getResources().getString(R.string.common_male);

        return result;
    }

    public boolean getBoolean(){
        return mValue != 0;
    }

	public static GenderType getGenderType( int value )
	{
		GenderType genderType = null;
		
		switch( value )
		{
		case 0:
			genderType = GenderType.FEMALE;
			break;
		case 1:
			genderType = GenderType.MALE;
			break;
		case 2:
			genderType = GenderType.NONE;
			break;
		}
		
		return genderType;
	}
}
