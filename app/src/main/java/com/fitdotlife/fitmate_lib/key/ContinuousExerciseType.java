package com.fitdotlife.fitmate_lib.key;

/**
 * Created by Joshua on 2015-02-10.
 */
public class ContinuousExerciseType {
    public static final int CONTINUOUS_EXERCISE = 1;
    public static final int NO_CONTINUOUS_EXERCISE = 0;
}
