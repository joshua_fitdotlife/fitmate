package com.fitdotlife.fitmate_lib.key;

/**
 * Created by Joshua on 2015-08-31.
 */
public enum NewsType {
    INIT(111),
    ACHIEVE_DAY_SUCCESS(0),
    ACHIEVE_DAY_FAIL(1),
    ACHIEVE_WEEK_SUCCESS(2),
    ACHIEVE_WEEK_FAIL(3) ,
    COMMENT_NEW(4),
    CHANGE_EXERCISEPROGRAM(5) ,
    FRIEND_REQUEST_FRIENDSHIP(6) ,
    FRIEND_REJECT_FRIENDSHIP(7) ,
    FRIEND_ACCEPT_FRIENDSHIP(8) ,
    FRIEND_DISMISS_FRIENDSHIP(9);

    private int mValue;

    NewsType( int value )
    {
        this.mValue = value;
    }

    public int getValue()
    {
        return this.mValue;
    }


    public static NewsType getNewsType( int value )
    {
        NewsType newsType = null;

        switch( value )
        {
            case 0:
                newsType = NewsType.ACHIEVE_DAY_SUCCESS;
                break;
            case 1:
                newsType = NewsType.ACHIEVE_DAY_FAIL;
                break;
            case 2:
                newsType = NewsType.ACHIEVE_WEEK_SUCCESS;
                break;
            case 3:
                newsType = NewsType.ACHIEVE_WEEK_FAIL;
                break;
            case 4:
                newsType = NewsType.COMMENT_NEW;
                break;
            case 5:
                newsType = NewsType.CHANGE_EXERCISEPROGRAM;
                break;
            case 6:
                newsType = NewsType.FRIEND_REQUEST_FRIENDSHIP;
                break;
            case 7:
                newsType = NewsType.FRIEND_REJECT_FRIENDSHIP;
                break;
            case 8:
                newsType = NewsType.FRIEND_ACCEPT_FRIENDSHIP;
                break;
            case 9:
                newsType = NewsType.FRIEND_DISMISS_FRIENDSHIP;
                break;
            case 111:
                newsType = NewsType.INIT;
                break;
        }

        return newsType;
    }

}
