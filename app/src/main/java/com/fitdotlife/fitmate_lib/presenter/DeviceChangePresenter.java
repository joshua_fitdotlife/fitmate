package com.fitdotlife.fitmate_lib.presenter;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;

import com.fitdotlife.fitmate.ActivityManager;
import com.fitdotlife.fitmate.ActivityReference;
import com.fitdotlife.fitmate.btmanager.BTManager;
import com.fitdotlife.fitmate.btmanager.ConnectionStatus;
import com.fitdotlife.fitmate.btmanager.FitmeterEventCallback;
import com.fitdotlife.fitmate.btmanager.ReceivedDataInfo;
import com.fitdotlife.fitmate.model.ActivityDataModel;
import com.fitdotlife.fitmate.model.UserInfoModel;
import com.fitdotlife.fitmate_lib.iview.IDeviceChangeView;
import com.fitdotlife.fitmate_lib.service.ServiceClient;
import com.fitdotlife.fitmate_lib.service.ServiceRequestCallback;
import com.fitdotlife.fitmate_lib.service.bluetooth.FitmeterDeviceDriver;
import com.fitdotlife.fitmate_lib.service.key.MessageType;
import com.fitdotlife.fitmate_lib.service.protocol.FileReadException;
import com.fitdotlife.fitmate_lib.service.protocol.ProtocolManager;
import com.fitdotlife.fitmate_lib.service.protocol.object.SerialCode;
import com.fitdotlife.fitmate_lib.service.protocol.object.SystemInfo_Request;
import com.fitdotlife.fitmate_lib.service.protocol.object.SystemInfo_Response;

import org.apache.log4j.Log;

import java.util.List;

/**
 * Created by Joshua on 2015-03-26.
 */
public class DeviceChangePresenter{

    private String TAG = "fitmate";
    private Context mContext = null;
    private UserInfoModel userInfoModel = null;
    private ServiceClient serviceClient = null;
    private ProtocolManager protocolManager = null;
    private IDeviceChangeView mView = null;
    private ActivityDataModel dataModel = null;
    private BTManager btManager = null;
    private int retryCount = 0;
    private String mBluetoothDeviceAddress = null;

    public DeviceChangePresenter( Context context , IDeviceChangeView view ){
        this.mContext = context;
        this.mView = view;
        this.userInfoModel = new UserInfoModel(this.mContext);
        this.dataModel = new ActivityDataModel(this.mContext);
    }



    public void setNeedMergeToday(){
        dataModel.setNeedMergeToday();
    }

    public void updateDeviceAddress( String deviceAddress ){
        userInfoModel.updateDeviceAddress(deviceAddress );
    }

}
