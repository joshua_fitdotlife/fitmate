package com.fitdotlife.fitmate_lib.presenter;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;

import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.WearingLocation;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ContinuousCheckPolicy;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseAnalyzer;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.FatAndCarbonhydrateConsumtion;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.StrengthInputType;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.UserInfoForAnalyzer;
import com.fitdotlife.fitmate.model.UserInfoModel;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.iview.IHomeView;
import com.fitdotlife.fitmate_lib.object.DayActivity;
import com.fitdotlife.fitmate_lib.object.ExerciseProgram;
import com.fitdotlife.fitmate_lib.object.ResponseDayActivity;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.object.WeekActivity;
import com.fitdotlife.fitmate_lib.service.ServiceClient;
import com.fitdotlife.fitmate_lib.service.ServiceRequestCallback;
import com.fitdotlife.fitmate_lib.service.key.MessageType;
import com.fitdotlife.fitmate_lib.service.key.SyncType;
import com.fitdotlife.fitmate_lib.util.Utils;
import com.fitdotlife.fitmate.model.ActivityDataListener;
import com.fitdotlife.fitmate.model.ActivityDataModel;
import com.fitdotlife.fitmate.model.ExerciseProgramModel;
import com.fitdotlife.fitmate.model.ServiceResultListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Joshua on 2015-03-23.
 */
public class HomePresenter implements ServiceResultListener , ActivityDataListener , ServiceRequestCallback {

    private static boolean isDataSynchronized = false;

    private final int MAX_RECEIVE_BYTE_LENGTH = 512;
    private final String TAG = "fitmate - " + HomePresenter.class.getSimpleName();

    private Context mContext = null;
    private IHomeView mView = null;
    private ExerciseProgramModel mExerciseProgramModel = null;
    private ActivityDataModel mActivityDataModel = null;
    private UserInfoModel mUserInfoModel = null;

    private Calendar activityCalendar = null;
    private boolean isRequestCalculate = false;
    private ServiceClient mServiceClient = null;

    public HomePresenter( Context context , IHomeView view  ){
        this.mContext = context;
        this.mView = view;
        this.mExerciseProgramModel = new ExerciseProgramModel( mContext);
        this.mActivityDataModel = new ActivityDataModel( mContext , this );
        this.mUserInfoModel = new UserInfoModel(mContext);
        this.mServiceClient = new ServiceClient(this.mContext , this);
        this.mServiceClient.open();
        this.mServiceClient.start();

        this.activityCalendar = Calendar.getInstance();
        this.activityCalendar.setTimeInMillis( System.currentTimeMillis() );
    }

    public ExerciseProgram getAppliedProgram(){
        return this.mExerciseProgramModel.getAppliedProgram();
    }

    public void getDayActivity(){

        //로컬에 DayActivity를 확인한다.
        String weekStartDate = this.getWeekStartDate();
        WeekActivity weekActivity = this.mActivityDataModel.getWeekActivity(weekStartDate);
        if( weekActivity != null ){ //있다면 보여준다.

            this.displayDayActivity();

        }else{

            if(Utils.isOnline(this.mContext) ) {
                this.mActivityDataModel.getDayActivityFromServer(weekStartDate);
            }else{
                this.displayDayActivity();
            }
        }
    }

    public ResponseDayActivity getResponseDayActivity(){

        String activityDate = this.getDateString( this.activityCalendar );
        DayActivity dayActivity = this.mActivityDataModel.getDayActivity(activityDate);
        String weekStartDate = this.getWeekStartDate();
        WeekActivity weekActivity = this.mActivityDataModel.getWeekActivity(weekStartDate);

        ResponseDayActivity responseDayActivity = null;

        if( dayActivity == null ){
            //this.mView.showResult( this.mContext.getResources().getString(R.string.activity_main_no_data));
        }else{
            responseDayActivity = new ResponseDayActivity();
            responseDayActivity.setActivityDate( dayActivity.getActivityDate() );
            responseDayActivity.setCalorie( dayActivity.getCalorieByActivity() );
            responseDayActivity.setStrengthHigh( dayActivity.getStrengthHigh() );
            responseDayActivity.setStrengthMedium( dayActivity.getStrengthMedium() );
            responseDayActivity.setStrengthLow( dayActivity.getStrengthLow() );
            responseDayActivity.setStrengthUnderLow( dayActivity.getStrengthUnderLow() );
            responseDayActivity.setAchieveOfTarget( dayActivity.getAchieveOfTarget() );
            responseDayActivity.setPredayScore( dayActivity.getPredayScore() );
            responseDayActivity.setTodayScore( dayActivity.getTodayScore() );
            responseDayActivity.setExtraScore( dayActivity.getExtraScore() );
            responseDayActivity.setAchieveMax( dayActivity.isAchieveMax() );
            responseDayActivity.setPossibleMaxScore( dayActivity.getPossibleMaxScore() );
            responseDayActivity.setPossibleTimes( dayActivity.getPossibleTimes() );
            responseDayActivity.setPossibleValue( dayActivity.getPossibleValue() );

            if( weekActivity != null ) {
                responseDayActivity.setWeekScore(weekActivity.getScore());
            }

            //byte array를 float array로 변환해야 한다. //TODO 수정해야함.
            //byte[] metByteArray = dayActivity.getMetByteArray();
            //MetInfo metInfo = this.convertByteArrayToFloatArray(metByteArray);
            MetInfo metInfo = new MetInfo();
            responseDayActivity.setAverageMET(metInfo.averageMet);
            responseDayActivity.setMetArray( metInfo.metFloatArray );
        }

        return responseDayActivity;
    }

    public void requestStopService()
    {

        try {
            this.mServiceClient.sendServiceMessage( MessageType.MSG_STOP_SERVICE);

        } catch (RemoteException e) {
            Log.e(TAG, "requestStopService 중에 에러가 발생했습니다.");
        }
    }

    public void requestRestartService()
    {

        try {
            this.mServiceClient.sendServiceMessage(MessageType.MSG_RESTART_SERVICE);

        } catch (RemoteException e) {
            Log.e(TAG, "requestRestartService 중에 에러가 발생했습니다.");
        }
    }


    public void requestCalculateActivity()
    {
        Log.i(TAG, "requestCalculateActivity()");

        if( this.isRequestCalculate ){
            return;
        }

        isRequestCalculate = true;


        try {
            this.mServiceClient.sendServiceMessage(MessageType.MSG_CALCULATE_ACTIVITY);

        } catch (RemoteException e) {
            Log.e(TAG , "계산 요청 중에 에러가 발생했습니다.");
            isRequestCalculate = false;
            this.getDayActivity();
            //this.displayWeekData();
        }
    }

    public void resetGetData(){
        Log.i(TAG, "resetGetData()");

        try {
            this.mServiceClient.sendServiceMessage(MessageType.MSG_RESET_GET_DATA);
        } catch (RemoteException e) {
            Log.e(TAG , "데이터 쓰레드를 다시 요청하는 중에 에러가 발생했습니다..");
            requestCalculateActivity();
        }
    }


    private String getWeekStartDate(  ) {

        Calendar weekCalendar = Calendar.getInstance();
        weekCalendar.setTimeInMillis(this.activityCalendar.getTimeInMillis());
        int weekNumber = weekCalendar.get(Calendar.DAY_OF_WEEK);
        weekCalendar.add(Calendar.DATE , -( weekNumber - 1 ) );
        String weekStartDate = this.getDateString( weekCalendar );
        return weekStartDate;
    }

    private String getDateString( Calendar calendar )
    {
        return calendar.get(Calendar.YEAR) + "-" +String.format("%02d", calendar.get(Calendar.MONTH) + 1) + "-" + String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH));
    }

    @Override
    public void onCalculateCompleted()
    {
        //앱에 분석된 활동량결과를 서버에 업로드한다.
        if( this.getServerUpdate() )
        {
            if(Utils.isOnline(this.mContext) ) {

                Log.i(TAG, "앱 서버 동기화");
                List<DayActivity> uploadDayActivityList = this.mActivityDataModel.getNotUploadDayActivity();
                List<WeekActivity> uploadWeekActivityList = this.mActivityDataModel.getNotUploadWeekActivity();

                this.setServerUpdate(false);
                this.mActivityDataModel.uploadActivityList(uploadDayActivityList, uploadWeekActivityList);
            }
        }

        Log.i(TAG, "Request Complete");
        isRequestCalculate = false;
        this.getDayActivity();
    }

    @Override
    public void onSyncEnd() {
        Log.i(TAG, "#### 동기화 끝");
        this.mView.onSyncEnd();
    }

    @Override
    public void onBatteryStatus(int ratio) {
        this.mView.onBatteryStatusChange(ratio);
    }

    @Override
    public void onSyncProgress(int receiveBytes, int totalBytes) {
        Log.i(TAG, "#### 동기화 중 -  RECEIVE LENGTH :" + receiveBytes + "  TOTAL LENGTH : " + totalBytes);

        //if( this.isCalculateAndView ) {

        this.setInitialViewComplete(true);
        if (totalBytes > this.MAX_RECEIVE_BYTE_LENGTH) {
            int progressPercent = ((receiveBytes * 100) / totalBytes);
            this.mView.onSyncProgress(progressPercent);
        }else{
            mView.dismissSyncView();
        }
        //}
    }

    private void setInitialViewComplete( boolean initViewComplete ){
        SharedPreferences pref = this.mContext.getSharedPreferences("fitmateservice", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean( LoginPresenter.INIT_VIEW_COMPLETE_KEY, initViewComplete);
        editor.commit();
    }

    public void displayDayActivity(){

        double calorie = 0;
        int achieveOfTarget = 0;
        int predayScore = 0;
        int todayScore = 0;
        int extraScore = 0;
        boolean isAchieveMax = false;
        int possibleMaxScore = 0;
        int possibleTimes = 0;
        int possibleValue = 0;
        double distance = 0;

        String activityDate = this.getDateString(this.activityCalendar);
        DayActivity dayActivity = this.mActivityDataModel.getDayActivity(activityDate);

        if(dayActivity != null){
            calorie = dayActivity.getCalorieByActivity();
            achieveOfTarget = dayActivity.getAchieveOfTarget();
            predayScore = dayActivity.getPredayScore();
            todayScore = dayActivity.getTodayScore();
            extraScore = dayActivity.getExtraScore();
            isAchieveMax = dayActivity.isAchieveMax();
            possibleMaxScore = dayActivity.getPossibleMaxScore();
            possibleTimes = dayActivity.getPossibleTimes();
            possibleValue = dayActivity.getPossibleValue();
        }

        ExerciseProgram program = this.getAppliedProgram();

        int rangeFrom = 0;
        int rangeTo = 0;
        double value = 0;
        StrengthInputType strengthInputType = StrengthInputType.getStrengthType(program.getCategory());
        UserInfo userInfo = this.mUserInfoModel.getUserInfo();
        UserInfoForAnalyzer userInfoForAnalyzer = com.fitdotlife.fitmate_lib.service.util.Utils.getUserInfoForAnalyzer(userInfo);

        if( strengthInputType.equals(StrengthInputType.CALORIE) || strengthInputType.equals( StrengthInputType.CALORIE_SUM ) ){
            rangeFrom = (int) program.getStrengthFrom();
            rangeTo = (int) program.getStrengthTo();
            value = calorie;
        }else if( strengthInputType.equals(StrengthInputType.VS_BMR) ){


            rangeFrom = (int) (userInfoForAnalyzer.getBMR() * ( program.getStrengthFrom() / 100 ));
            rangeTo = (int) (userInfoForAnalyzer.getBMR() * ( program.getStrengthTo() / 100 ));
            value = calorie;

        }else{
            rangeFrom = program.getMinuteFrom();
            rangeTo = program.getMinuteTo();
            value = achieveOfTarget;
        }
        float  fat =0;
        float carbon =0;
        if(dayActivity!=null){
            ContinuousCheckPolicy policy = ContinuousCheckPolicy.Loosely_SumOverXExercise;
            FitmateDBManager dbManager= new FitmateDBManager(mContext);
            com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseProgram appliedProgram = dbManager.getAppliedExerciseProgram();
            ExerciseAnalyzer analyzer = new ExerciseAnalyzer(appliedProgram, userInfoForAnalyzer, WearingLocation.WAIST , dayActivity.getDataSaveInterval() );


            float[] metArray= dayActivity.getMetArray();
            List<Float> metList= new ArrayList<>();
            for(float f:metArray){
                metList.add(f);
            }
            FatAndCarbonhydrateConsumtion fatBurn = analyzer.getFatandCarbonhydrate(metList, dayActivity.getDataSaveInterval());
            fat =fatBurn.getFatBurned();
            carbon =fatBurn.getCarbonhydrateBurned();

            distance = ExerciseAnalyzer.getDistanceFromMETArray_over2MET(metList ,dayActivity.getDataSaveInterval());

            metArray = null;
            metList.clear();
            metList = null;
        }




        this.mView.displayDayAchievement(rangeFrom, rangeTo, value, strengthInputType, extraScore);
        this.mView.setCalDisValue(calorie , distance );
        this.mView.displayExerciseProgram(program);

        this.mView.displayFatBurn(fat, carbon);
        this.mView.displayWeekAchievement( predayScore , todayScore , isAchieveMax , possibleMaxScore , possibleTimes , possibleValue , StrengthInputType.getStrengthType(program.getCategory()) );

    }

    public void close(){
        this.mServiceClient.close();
    }

    @Override
    public void onDayActivityReceived() {
        displayDayActivity();
    }

    @Override
    public void onWeekActivityReceived() {

    }

    @Override
    public void onMonthActivityReceived( ) {

    }

    @Override
    public void onUploadedData(  boolean isUpload ) {

    }

    class MetInfo
    {
        public float[] metFloatArray;
        public int averageMet = 0;
    }

    @Override
    public void onServiceResultReceived(int what, Bundle data) {
        if( what == MessageType.MSG_CALCULATE_ACTIVITY ){
            this.onCalculateCompleted();
        }else if( what == MessageType.MSG_DATA_SYNC ){
            SyncType syncType = SyncType.getSyncType( data.getInt( "synctype" ) );

            if( syncType.equals( SyncType.PROGRESS ) ){
                int recceiveBytes = data.getInt( "receivebyte" );
                int totalBytes = data.getInt( "totalbyte" );
                this.onSyncProgress(recceiveBytes, totalBytes);
            }else if( syncType.equals( SyncType.END )){
                this.onSyncEnd();
            }
        }
        else if(what== MessageType.MSG_BATTERYRATE){
            int batteryRatio= data.getInt("battery");
            this.onBatteryStatus(batteryRatio);
        }
        else if(what== MessageType.MSG_STOP_SERVICE){
            //Toast.makeText(mContext, "STOP 완료", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void serviceInitializeCompleted() {

    }

    private boolean getServerUpdate( ){
        SharedPreferences pref = this.mContext.getSharedPreferences( "fitmateservice" , this.mContext.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        return pref.getBoolean( DayActivity.DAY_ACTIVITY_SERVER_UPDATE_KEY, false);
    }

    private void setServerUpdate( boolean serverUpdate ){
        SharedPreferences pref = this.mContext.getSharedPreferences( "fitmateservice" , this.mContext.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(DayActivity.DAY_ACTIVITY_SERVER_UPDATE_KEY, serverUpdate);
        editor.commit();
    }

    private long getTimeMillisecond( String strDate ){

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = format.parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.getTimeInMillis();
    }

}
