package com.fitdotlife.fitmate_lib.presenter;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.SharedPreferences;

import com.fitdotlife.fitmate.ActivityManager;
import com.fitdotlife.fitmate.ActivityReference;
import com.fitdotlife.fitmate.R;
import com.fitdotlife.fitmate.SetAccountActivity;
import com.fitdotlife.fitmate.model.UserInfoModel;
import com.fitdotlife.fitmate.model.UserInfoModelResultListener;
import com.fitdotlife.fitmate_lib.iview.ISetUserInfoView;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.util.Utils;

/**
 * Created by Joshua on 2015-01-29.
 */
public class SetUserInfoPresenter implements UserInfoModelResultListener {

    private String TAG = "fitmate";
    private Context mContext = null;
    private ISetUserInfoView mView = null;
    private UserInfo mUserInfo = null;
    private UserInfoModel mModel = null;

    private String mEncodePassword = null;
    private String mEmail = null;

    private boolean mUsableEmail = false;

    private boolean mCallbackWaiting = false;

    public SetUserInfoPresenter( Context context, ISetUserInfoView view ) {
        this.mContext = context;
        this.mView = view;
        this.mModel = new UserInfoModel( context , this);
    }

    public void setUserInfo(UserInfo userInfo )
    {

        if(!Utils.isOnline(this.mContext) ) {
            mView.showResult( this.mContext.getString(R.string.common_connect_network) );
            return;
        }

        this.mView.startProgressDialog(this.mContext.getString(R.string.signin_dialog_title), this.mContext.getString(R.string.signin_dialog_content));
        this.mEmail = userInfo.getEmail();
        this.mEncodePassword = userInfo.getPassword();
        this.mUserInfo = userInfo;
        this.mModel.registerUserInfo(userInfo);
    }


    public void setUserAccount(UserInfo userInfo)
    {
        this.mView.startProgressDialog(this.mContext.getString(R.string.signin_dialog_title), this.mContext.getString(R.string.signin_dialog_content));
        this.mEmail = userInfo.getEmail();
        this.mEncodePassword = userInfo.getPassword();
        this.mUserInfo = userInfo;
        this.mModel.setUserAccount(userInfo);
    }


    public void ismUsableEmail( String email){

        if( !Utils.isOnline( this.mContext ) ){
            mView.showResult(this.mContext.getString(R.string.common_connect_network));
            return;
        }

        this.mView.startProgressDialog(this.mContext.getString(R.string.signin_email_dialog_title_duplicate), this.mContext.getString(R.string.signin_email_dialog_content_duplicate));
        this.mModel.isUsableEmail(email);
    }


    @Override
    public void onSuccess(int code) {
        if( code == UserInfoModel.JOIN_MEMBER_RESPONSE ) {

            mView.stopProgressDialog();

            this.setAutoLogin(true);
            this.setInitExecute(false);

            //설정화면이나 내정보 화면 그 외에 화면을 없앤다.
            ActivityManager.getInstance().finishAllActivity();

            //홈화면을 없앤다.
            ActivityReference.activityReferece.finish();

            this.setSettedUserInfo(true);
            this.mView.moveHomeActivity();

        }else if(code == UserInfoModel.USABLE_EMAIL_RESPONSE){
            this.mView.stopProgressDialog();

            this.mView.setDuplicateResult(true);
            //this.mView.showPopup(SetAccountActivity.EMAIL_CODE, this.mContext.getString(R.string.signin_email_available));
        }else if( code == UserInfoModel.SET_ACCOUNT_RESPONSE ){
            this.setAutoLogin(true);
            this.setInitExecute(false);
            this.setSettedUserInfo(false);
            this.mView.stopProgressDialog();

            //로그인 화면을 없앤다.
            ActivityReference.activityReferece.finish();
            this.mView.moveHomeActivity();
        }
    }

    @Override
    public void onSuccess(int code, boolean result) {

    }

    @Override
    public void onSuccess(int code, UserInfo userInfo) {

    }

    @Override
    public void onFail(int code) {
        if (code == UserInfoModel.JOIN_MEMBER_RESPONSE || code == UserInfoModel.SET_ACCOUNT_RESPONSE) {
            this.mView.stopProgressDialog();
            this.mView.showResult(this.mContext.getString(R.string.signin_setuserinfo_fail));
    }   else if( code == UserInfoModel.USABLE_EMAIL_RESPONSE ){
            this.mView.stopProgressDialog();
            this.mView.setDuplicateResult(false);
            this.mView.showPopup(SetAccountActivity.EMAIL_CODE, this.mContext.getString(R.string.signin_email_already_use));
        }
    }

    @Override
    public void onErrorOccured(int code, String message) {
        if (code == UserInfoModel.JOIN_MEMBER_RESPONSE || code == UserInfoModel.SET_ACCOUNT_RESPONSE) {
            this.mView.stopProgressDialog();
            this.mView.showResult(this.mContext.getString(R.string.signin_email_duplicate_error));
        }else if( code == UserInfoModel.USABLE_EMAIL_RESPONSE ){
            this.mView.stopProgressDialog();
            this.mView.setDuplicateResult( false );
            this.mView.showResult(this.mContext.getString(R.string.signin_email_duplicate_error));

        }
    }

    private void setAutoLogin( boolean autoLogin ){
        SharedPreferences pref = this.mContext.getSharedPreferences( "fitmateservice" , this.mContext.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean( UserInfo.AUTO_LOGIN_KEY, autoLogin);
        editor.commit();
    }

    private void setInitExecute(boolean initExecute){
        SharedPreferences pref = this.mContext.getSharedPreferences( "fitmateservice" , this.mContext.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean( LoginPresenter.INIT_EXECUTE_KEY , initExecute);
        editor.commit();
    }

    private void setSettedUserInfo(boolean settedUserInfo){
        SharedPreferences pref = this.mContext.getSharedPreferences("fitmateservice", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(LoginPresenter.SET_USERINFO_KEY, settedUserInfo);
        editor.commit();
    }
}
