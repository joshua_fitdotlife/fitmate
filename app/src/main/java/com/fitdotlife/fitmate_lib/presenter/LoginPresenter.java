package com.fitdotlife.fitmate_lib.presenter;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.SystemClock;

import com.fitdotlife.fitmate.MyApplication;
import com.fitdotlife.fitmate.R;
import com.fitdotlife.fitmate.alarm.ActivityNotiAlarmReceiver;
import com.fitdotlife.fitmate.alarm.WearNotiAlarmReceiver;
import com.fitdotlife.fitmate.model.ActivityDataListener;
import com.fitdotlife.fitmate.model.ActivityDataModel;
import com.fitdotlife.fitmate.model.ExerciseProgramModel;
import com.fitdotlife.fitmate.model.UserInfoModel;
import com.fitdotlife.fitmate.model.UserInfoModelResultListener;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.iview.ILoginView;
import com.fitdotlife.fitmate_lib.object.DayActivity;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.object.UserNotiSetting;
import com.fitdotlife.fitmate_lib.util.Utils;

import org.apache.log4j.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class LoginPresenter implements UserInfoModelResultListener, ActivityDataListener {

    private final String TAG = "fitmate";
    public static final String INIT_EXECUTE_KEY = "initialexecute";
    public static final String CHECK_BLUETOOTHON_KEY = "checkbluetooton";
    public static final String REQUEST_BLUETOOTHON_KEY = "requestbluetooton";
    public static final String INIT_VIEW_COMPLETE_KEY = "initialviewcomplete";
    public static final String SET_USERINFO_KEY = "setuserinfo";

	private ILoginView mView = null;
	private UserInfoModel mModel = null;
    private ActivityDataModel mActivityDataModel = null;
    private ExerciseProgramModel mProgrmaModel = null;
	private Context mContext = null;

    private String mEncodePassword = null;
    private String mID = null;


	public LoginPresenter(Context context, ILoginView view)
	{
		this.mView = view;
		this.mContext = context;
		this.mModel = new UserInfoModel( context , this );
        this.mActivityDataModel = new ActivityDataModel(context , this);
        this.mProgrmaModel = new ExerciseProgramModel(context);
	}

    public void login( String email , String pwd )
	{
        Log.d(TAG, "일반 로그인 시작");
        this.mView.startProgressDialog();

        this.mEncodePassword = Utils.encodePassword( pwd );
        this.mID = email;

        //this.mModel.login( email , mEncodePassword);
        if(this.mModel.localLogin(email, mEncodePassword)){
            Log.d(TAG, "로컬 로그인 성공");

            UserInfo userInfo = mModel.getUserInfo();
            if( userInfo.getDeviceAddress() != null && !userInfo.getDeviceAddress().equals("") && !userInfo.getDeviceAddress().equals("null") ){
                this.setSettedUserInfo(true);
            }else{
                this.setSettedUserInfo(false);
            }

            if(!Utils.isOnline(this.mContext) ) {
                this.setAutoLogin(true);
                this.setServerUpdate(true);
                this.setCheckBluetoothOn(false);
                this.setRequestBluetoothOn(false);
                this.setInitialViewComplete(false);

                mView.stopProgressDialog();
                mView.finishActivity();
                mView.navigateToHome();
                return;
            }

            this.mModel.registerUserInfo();
        }else{
            if(!Utils.isOnline(this.mContext) ) {
                Log.d(TAG, "인터넷 연결 없음" );
                this.mView.showResult(mContext.getString(R.string.common_connect_network));
                this.mView.stopProgressDialog();
                return;
            }

            Log.d(TAG, "서버 로그인 시작" );
            this.mModel.serverLogin( email , mEncodePassword );
        }
	}

    @Override
    public void onSuccess(int code) {

            Log.d(TAG, "서버 로그인 성공");
            //자동로그인을 위하여 아이디와 비밀번호를 저장한다.
            this.setAutoLogin(true);
            this.setServerUpdate(true);
            this.setCheckBluetoothOn(false);
            this.setRequestBluetoothOn(false);
            this.setInitialViewComplete(false);

            mView.stopProgressDialog();
            mView.finishActivity();
            mView.navigateToHome();
    }

    @Override
    public void onSuccess(int code, boolean result) {

    }
    @Override
    public void onSuccess(int code, UserInfo userInfo)
    {
        Log.d(TAG, "서버 로그인 성공");
        if( code == UserInfoModel.LOGIN_RESPONSE_CODE ) {
            //로컬 DB에 서버의 사용자 정보를 저장한다.
            if (userInfo != null) {
                this.mModel.setUserInfoToLocalDB(userInfo);

                //이전에 적용하고 있던 프로그램으로 적용한다.
                int appliedProgramID = userInfo.getExerprogramId();
                this.mProgrmaModel.applyExerciseProgram( appliedProgramID );

            }
        }

        this.setAutoLogin(true);
        this.setServerUpdate(true);
        this.setCheckBluetoothOn(false);
        this.setRequestBluetoothOn(false);
        this.setInitialViewComplete(false);

        if( this.getInitExecute() )
        {

            Log.d(TAG, "처음 실행하여 Mergy 데이터 요청" );
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(userInfo.getLastDataDate());
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);
            calendar.add(Calendar.DATE, -calendar.get(Calendar.DAY_OF_WEEK) + 1);
           // this.mActivityDataModel.getDayActivityFromServer( calendar.getTimeInMillis() );

            //this.mActivityDataModel.getDayActivityFromServer_Mergy( DateUtils.convertUTCTicktoDateString(userInfo.getLastDataDate()) );
            String activityDate = calendar.get(Calendar.YEAR) + "-" + String.format("%02d",  calendar.get( Calendar.MONTH ) + 1 ) + "-" + String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH) );
            this.mActivityDataModel.getDayActivityFromServer_Mergy(activityDate);


        }else {
            Log.d(TAG, "처음 실행이 아님." );
            mView.stopProgressDialog();
            mView.finishActivity();
            mView.navigateToHome();
        }
    }

    @Override
    public void onFail(int code) {
        mView.stopProgressDialog();

        if( code == UserInfoModel.LOGIN_RESPONSE_CODE ) {
            this.mView.showResult(mContext.getString(R.string.login_fail));
        }else{
            this.setAutoLogin(true);
            this.setServerUpdate(true);
            this.setCheckBluetoothOn(false);
            this.setRequestBluetoothOn(false);
            this.setInitialViewComplete(false);

            mView.finishActivity();
            mView.navigateToHome();
        }
    }

    @Override
    public void onErrorOccured(int code, String message) {
        mView.stopProgressDialog();

        if( code == UserInfoModel.LOGIN_RESPONSE_CODE ) {
            mView.showResult("로그인 중에 에러가 발생했습니다.");
        }else{
            this.setAutoLogin(true);
            this.setServerUpdate(true);
            this.setCheckBluetoothOn(false);
            this.setRequestBluetoothOn(false);
            this.setInitialViewComplete(false);

            mView.finishActivity();
            mView.navigateToHome();
        }
    }

    @Override
    public void onDayActivityReceived() {

        Log.d(TAG, "Mergy 데이터 수신 성공" );
        this.setInitExecute(false);
        mView.stopProgressDialog();
        mView.finishActivity();
        mView.navigateToHome();

    }

    @Override
    public void onWeekActivityReceived() {

    }

    @Override
    public void onMonthActivityReceived() {

    }

    @Override
    public void onUploadedData( boolean isUpload) {

    }

    private boolean getAutoLogin(){
        SharedPreferences pref = this.mContext.getSharedPreferences( "fitmateservice" , this.mContext.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        return pref.getBoolean(UserInfo.AUTO_LOGIN_KEY, false);
    }

    private void setAutoLogin( boolean autoLogin ){
        SharedPreferences pref = this.mContext.getSharedPreferences( "fitmateservice" , this.mContext.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(UserInfo.AUTO_LOGIN_KEY, autoLogin);
        editor.commit();
    }

    private void setServerUpdate( boolean serverUpdate ){
        SharedPreferences pref = this.mContext.getSharedPreferences( "fitmateservice" , this.mContext.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(DayActivity.DAY_ACTIVITY_SERVER_UPDATE_KEY, serverUpdate);
        editor.commit();
    }

    private boolean getInitExecute(){
        SharedPreferences pref = this.mContext.getSharedPreferences( "fitmateservice" , this.mContext.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        return pref.getBoolean( this.INIT_EXECUTE_KEY , true);
    }

    private void setInitExecute(boolean initExecute){
        SharedPreferences pref = this.mContext.getSharedPreferences( "fitmateservice" , this.mContext.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean( this.INIT_EXECUTE_KEY , initExecute);
        editor.commit();
    }

    private void setCheckBluetoothOn( boolean checkBluetoothOn ){
        SharedPreferences pref = this.mContext.getSharedPreferences( "fitmateservice" , this.mContext.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(this.CHECK_BLUETOOTHON_KEY, checkBluetoothOn);
        editor.commit();
    }

    private void setRequestBluetoothOn( boolean requestBluetoothOn ){
        SharedPreferences pref = this.mContext.getSharedPreferences( "fitmateservice" , this.mContext.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(this.REQUEST_BLUETOOTHON_KEY, requestBluetoothOn);
        editor.commit();
    }

    private void setInitialViewComplete( boolean initViewComplete ){
        SharedPreferences pref = this.mContext.getSharedPreferences( "fitmateservice" , this.mContext.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean( this.INIT_VIEW_COMPLETE_KEY, initViewComplete);
        editor.commit();
    }

    private void setSettedUserInfo(boolean settedUserInfo){
        SharedPreferences pref = this.mContext.getSharedPreferences( "fitmateservice" , this.mContext.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean( LoginPresenter.SET_USERINFO_KEY , settedUserInfo);
        editor.commit();
    }

}
