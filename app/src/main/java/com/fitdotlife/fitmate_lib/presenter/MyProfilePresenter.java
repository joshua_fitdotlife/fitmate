package com.fitdotlife.fitmate_lib.presenter;

import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import android.widget.Toast;

import com.fitdotlife.fitmate.R;
import com.fitdotlife.fitmate_lib.iview.IMyProfileView;
import com.fitdotlife.fitmate_lib.object.DayActivity;
import com.fitdotlife.fitmate_lib.object.ExerciseProgram;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.object.WeekActivity;
import com.fitdotlife.fitmate_lib.service.ServiceClient;
import com.fitdotlife.fitmate_lib.service.ServiceRequestCallback;
import com.fitdotlife.fitmate.model.ActivityDataListener;
import com.fitdotlife.fitmate.model.ActivityDataModel;
import com.fitdotlife.fitmate.model.ExerciseProgramModel;
import com.fitdotlife.fitmate.model.UserInfoModel;
import com.fitdotlife.fitmate.model.UserInfoModelResultListener;
import com.fitdotlife.fitmate_lib.service.key.MessageType;

import java.util.List;

/**
 * Created by Joshua on 2015-03-26.
 */
public class MyProfilePresenter  implements UserInfoModelResultListener, ActivityDataListener {

    public static final int STOP_FOR_CHANGE_FITMETER = 1;
    public static final int STOP_FOR_CHANGE_WEARINGLOCATION = 2;

    private int mStopMode = 0;
    private Context mContext = null;
    private IMyProfileView mView = null;
    private UserInfo mUserInfo = null;
    private UserInfoModel mUserInfoModel = null;
    private ExerciseProgramModel mProgramModel = null;
    private ActivityDataModel mActivityDataModel = null;
    final static String TAG = "MyProfilePresenter";

    public MyProfilePresenter( Context context, IMyProfileView view) {
        this.mContext = context;
        this.mView = view;
        this.mUserInfoModel = new UserInfoModel( context , this);
        this.mProgramModel = new ExerciseProgramModel( context);
        this.mActivityDataModel = new ActivityDataModel( context , this );

        mUserInfo = mUserInfoModel.getUserInfo();
    }

    public void ChangeUserInfo(){this.mUserInfoModel.registerUserInfo(); }

    public UserInfo getUserInfo(){
        return this.mUserInfoModel.getUserInfo();
    }

    public void modifyUserInfo( UserInfo userInfo ){ this.mUserInfoModel.modifyUserInfo(userInfo); }

    public ExerciseProgram getAppliedExerciseProgram(){
        return this.mProgramModel.getAppliedProgram();
    }

    public void deleteAllData(){
        this.mActivityDataModel.deleteAllDayActivity();
        this.mActivityDataModel.deleteAllWeekActivity();
        this.mUserInfoModel.deleteUserInfo();
    }


    @Override
    public void onSuccess(int code) {
        //mView.showDeviceChangeActivity();
    }

    @Override
    public void onSuccess(int code, boolean result) {

    }

    @Override
    public void onSuccess(int code, UserInfo userInfo) {

    }

    @Override
    public void onFail(int code) {

    }

    @Override
    public void onErrorOccured(int code, String message) {

    }

    @Override
    public void onDayActivityReceived() {

    }

    @Override
    public void onWeekActivityReceived() {
    }

    @Override
    public void onMonthActivityReceived() {

    }

    @Override
    public void onUploadedData(  boolean isUpload )
    {

        if( isUpload ){
            Log.e(TAG , "데이터 업로드 완료");
            this.mView.showDeviceChangeActivity();

        }else{
            mView.setShowing(false);
            mView.dismissProgress();
            Toast.makeText(mContext , "데이터 업로드를 실패하였습니다." , Toast.LENGTH_LONG).show();
        }

    }

    public void uploadActivityData()
    {
           Log.d(TAG, "데이터 업로드");
            mView.setProgressText(mContext.getResources().getString(R.string.setting_progress_upload_activitydata));
            List<DayActivity> uploadDayActivityList = mActivityDataModel.getNotUploadDayActivity();
            List<WeekActivity> uploadWeekActivityList = mActivityDataModel.getNotUploadWeekActivity();
            mActivityDataModel.uploadActivityList(uploadDayActivityList, uploadWeekActivityList);
    }

//    @Override
//    public void onServiceResultReceived(int what, Bundle data) {
//        if(what== MessageType.MSG_STOP_SERVICE){
//            Log.d(TAG , "SERVICE STOP 완료");
//            if( mStopMode == STOP_FOR_CHANGE_FITMETER ) {
//                try {
//                    Log.d(TAG, "활동량 계산 요청");
//                    mView.setProgressText( mContext.getResources().getString( R.string.setting_progress_request_calcualte ) );
//                    mServiceClient.sendServiceMessage(MessageType.MSG_CALCULATE_ACTIVITY);
//                } catch (RemoteException e) {
//                    Log.e(TAG, "requestStopService 중에 에러가 발생했습니다.");
//                    mView.setShowing(false);
//                    mView.dismissProgress();
//                    //TODO 서비스 재시작을 호출해야 하나?
//                }
//            }else if( mStopMode == STOP_FOR_CHANGE_WEARINGLOCATION ){
//                mView.showWearingLocationActivity();
//            }
//        }else if( what == MessageType.MSG_CALCULATE_ACTIVITY ){
//
//            Log.d(TAG, "데이터 업로드");
//            mView.setProgressText(mContext.getResources().getString(R.string.setting_progress_upload_activitydata));
//            List<DayActivity> uploadDayActivityList = mActivityDataModel.getNotUploadDayActivity();
//            List<WeekActivity> uploadWeekActivityList = mActivityDataModel.getNotUploadWeekActivity();
//            mActivityDataModel.uploadActivityList(uploadDayActivityList, uploadWeekActivityList);
//
//        }
//    }
//
//    @Override
//    public void serviceInitializeCompleted() {
//
//    }

}
