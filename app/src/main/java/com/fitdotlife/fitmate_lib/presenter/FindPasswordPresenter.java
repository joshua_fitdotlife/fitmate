package com.fitdotlife.fitmate_lib.presenter;

import android.content.Context;

import com.fitdotlife.fitmate.R;
import com.fitdotlife.fitmate_lib.iview.IFindPasswordView;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.util.Utils;
import com.fitdotlife.fitmate.model.UserInfoModel;
import com.fitdotlife.fitmate.model.UserInfoModelResultListener;

public class FindPasswordPresenter implements UserInfoModelResultListener {
	private final int FIND_USERINFO_RESPONSE_CODE = 0;
	
	private IFindPasswordView mView = null;
	private UserInfoModel mModel = null;
	private Context mContext = null;
	
	public FindPasswordPresenter(Context context, IFindPasswordView view)
	{
		this.mContext = context;
		this.mView = view;
		this.mModel = new UserInfoModel( this.mContext , this );
	}

	public void findUser( String email )
	{

		if(!Utils.isOnline(this.mContext) ) {
			mView.showResult(this.mContext.getString(R.string.common_connect_network));
			return;
		}

		this.mModel.findUser(email);
	}

	@Override
	public void onSuccess(int code) {

		//사용자 정보를 삭제한다.
		this.mModel.deleteUserInfo();

		this.mView.stopProgressDialog();
		this.mView.dismisDialog();
		this.mView.showResult(this.mContext.getString(R.string.find_password_send_mail_success));
	}

	@Override
	public void onSuccess(int code, boolean result) {

	}

	@Override
	public void onSuccess(int code, UserInfo userInfo) {

		this.mView.navigateToSearchDialog( userInfo );

	}

	@Override
	public void onFail(int code) {
		if( code == UserInfoModel.FIND_ACCOUNT_RESPONSE ) {
			this.mView.showResult(this.mContext.getString(R.string.find_password_not_user));
		}else if( code == UserInfoModel.SEND_PASSWORD_RESPONSE ){
			this.mView.stopProgressDialog();
			this.mView.showResult(this.mContext.getString(R.string.find_password_send_mail_fail));
		}

	}

	@Override
	public void onErrorOccured(int code, String message) {

	}

	public void sendPasswordEmail(String email) {

		if(!Utils.isOnline(this.mContext) ) {
			mView.showResult(this.mContext.getString(R.string.common_connect_network));
			return;
		}

		this.mView.startProgressDialog();
		this.mModel.sendPasswordEmail( email );
	}
}
