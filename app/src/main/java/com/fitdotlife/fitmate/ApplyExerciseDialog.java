package com.fitdotlife.fitmate;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Joshua on 2015-03-09.
 */
public class ApplyExerciseDialog extends Dialog {

    private Context mContext = null;
    private TextView mContetView = null;
    private View.OnClickListener mListener = null;

    private Button btnCancel = null;
    private Button btnApply = null;


    private String mProgramName = null;
    public ApplyExerciseDialog(Context context , View.OnClickListener listener , String programName ) {
        super(context , android.R.style.Theme_Black_NoTitleBar);
        this.mContext = context;
        this.mProgramName = programName;
        this.mListener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        lpWindow.dimAmount = 0.8f;
        getWindow().setAttributes( lpWindow );

        this.setContentView( R.layout.dialog_apply_exercise );

        this.mContetView = (TextView) this.findViewById(R.id.tv_dialog_appliy_exercise_content);
        this.mContetView.setText(this.mProgramName);

        this.btnApply = (Button) this.findViewById(R.id.btn_dialog_apply_exercise_ok);
        this.btnApply.setOnClickListener(this.mListener);
        this.btnCancel = (Button) this.findViewById(R.id.btn_dialog_apply_exercise_cancel);
        this.btnCancel.setOnClickListener(this.mListener);


    }
}
