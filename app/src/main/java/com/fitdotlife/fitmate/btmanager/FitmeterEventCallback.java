package com.fitdotlife.fitmate.btmanager;

import android.bluetooth.BluetoothDevice;

import com.fitdotlife.fitmate_lib.object.BLEDevice;

import java.util.List;

/**
 * Created by fitdotlife_dev on 2016. 5. 27..
 */
public interface FitmeterEventCallback {
/**
 *  전송률이나 진행률을 나타낸다
 *
 *  @param progress 현재 진행률
 *  @param max      최대값
 */
    public void onProgressValueChnaged(int progress, int max);

    /**
     * This function called when all file received completely.
     * @param receiveDataArray array of ReceivedData
     * @param isComplete true if received all files completely, else false(ex. because of disconnected etc.)
     */
    public void onReceiveAllFileComplete(List<ReceivedDataInfo> receiveDataArray, boolean isComplete);

/**
 *  This function called when a file received completely.
 *
 *  @param index    index if received file
 *  @param offset   offset from start of the file
 *  @param receivedbytes total received bytes
 */
    public void receiveCompleted(int index, int offset, byte[] receivedbytes);
/**
 *  This called when state of BTManager changed
 *
 *  @param status currentStatus
 */
    public void statusOfBTManagerChanged(ConnectionStatus status);

    /**
     * return results of new device discovery
     * @param discoveredPeripherals
     */
    void findNewDeviceResult(List<BLEDevice> discoveredPeripherals);


    /**
     * return result find device discovery
     * @param serialNumber
     */
    void findDeviceResult(  String deviceAddress , String serialNumber );

    /**
     * return results of dfu recovery device
     * @param recoveryDevice
     */
    void findRecoveryDeviceResult( BluetoothDevice recoveryDevice , List<BluetoothDevice> recoveryFitmeters );

}
