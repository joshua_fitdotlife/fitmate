package com.fitdotlife.fitmate.btmanager;

/**
 * Created by fitdotlife_dev on 2016. 5. 27..
 *
 * File data info in device
 */
public class ReceivedDataInfo {
    int index;
    int size;
    byte[] bytes;

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    int offset;
    /**
     * constructor
     * @param index index of file in device
     */
    public ReceivedDataInfo(int index) {
        this.index = index;
    }

    /**
     * getter
     * @return index
     */
    public int getIndex() {
        return index;
    }

    public void setSize(int size) {
        this.size = size;
    }

    /**
     * getter
     * @return size
     */
    public int getSize() {
        return size;
    }
    /**
     * getter
     * @return byte[]
     */
    public byte[] getBytes() {
        return bytes;
    }

    /**
     * setter
     * @param bytes received bytes from device. Length of bytes must equal to size of this object
     */
    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }
}
