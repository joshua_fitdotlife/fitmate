package com.fitdotlife.fitmate.btmanager;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.os.Build;

import com.fitdotlife.fitmate.MyApplication;
import android.util.Xml;
import android.view.View;

import org.apache.log4j.Log;

import com.fitdotlife.fitdotlifelib.protocol.Decoder;
import com.fitdotlife.fitdotlifelib.protocol.Encoder;
import com.fitdotlife.fitdotlifelib.protocol.ProtocolUtils;
import com.fitdotlife.fitdotlifelib.protocol.object.SystemInfo_Request;
import com.fitdotlife.fitdotlifelib.protocol.object.SystemInfo_Response;
import com.fitdotlife.fitdotlifelib.protocol.object.TimeInfo;
import com.fitdotlife.fitdotlifelib.protocol.type.HeaderType;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.database.FitmateServiceDBManager;
import com.fitdotlife.fitmate_lib.http.HttpApi;
import com.fitdotlife.fitmate_lib.http.HttpResponse;
import com.fitdotlife.fitmate_lib.key.FitmeterType;
import com.fitdotlife.fitmate_lib.object.LogInfo;
import com.fitdotlife.fitmate_lib.service.util.Utils;
import com.fitdotlife.fitmate_lib.object.BLEDevice;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;
import java.util.UUID;

/**
 * Created by fitdotlife_dev on 2016. 5. 27..
 */
public class BTManager {
    public static final int LED_COLOR_RED = 0xA1;
    public static final int LED_COLOR_GREEN = 0xA2;
    public static final int LED_COLOR_BLUE = 0xA3;
    public static final int LED_COLOR_YELLOW = 0xA4;
    public static final int LED_COLOR_WHITE = 0xA5;
    public static final int LED_COLOR_SKYBLUE = 0xA6;
    public static final int LED_COLOR_PUPPLE = 0xA7;
    public static final int LED_COLOR_OFF = 0xA8;

    private final static String TAG = BTManager.class.getSimpleName();

    private final int SCAN_TIME = 5000;
    private final String deviceName = "FITMETERBLE";
    //private final String deviceName = "FITMETERSHOW";
    private final String DFU_DEVICE_NAME = "Dfu11X4";
    private final int rssiLimit = -90;

    private Context context = null;
    private static BTManager ourInstance = null;
    private volatile FitmeterEventCallback fitmeterEventCallback;

    public BluetoothManager mBluetoothManager = null;
    public BluetoothAdapter mBluetoothAdapter = null;
    private BluetoothGatt mBluetoothGatt = null;

    private String mFindAddress = null;
    private List<BLEDevice> connectableFitmeters = new ArrayList<BLEDevice>();
    private List<BluetoothDevice> recoveryFitmeters = new ArrayList<BluetoothDevice>();

    //기기 검색시 한기기가 여러번 검색될 수도 있는데
    //검색된 리스트와 비교하여 한번 검색이 된 경우는 기기 검색 이후의 일들을 하지 않도록 한다.
    private List<BluetoothDevice> tempFitmeters = new ArrayList<BluetoothDevice>();

    private String mBluetoothDeviceAddress = null;
    private String mFoundDeviceAddress = null;

    private static final byte PC_COMM_START = (byte) 0xf5;
    private static final byte PC_COMM_END = (byte) 0xfa;

    private static final UUID CCCD = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
    private static final UUID serviceUUID = UUID.fromString("6e400001-b5a3-f393-e0a9-e50e24dcca9e");
    private static final UUID rxUUID = UUID.fromString("6e400002-b5a3-f393-e0a9-e50e24dcca9e");
    private static final UUID txUUID = UUID.fromString("6e400003-b5a3-f393-e0a9-e50e24dcca9e");

    BluetoothGattCharacteristic RxChar = null;
    BluetoothGattCharacteristic TxChar = null;

    private boolean mConnectedState = false;

    private int fileReceive_StartIndex = 0;

    private volatile boolean ResponseReceived;
    private final int timeoutSetting = 10000;
    private volatile byte[] receivedBytes;

    private boolean isDeviceFound = false;

    private FitmeterType mSearchFitmeterType = null;

    private int numberOfButtonPushedDevice = 0;

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
                switch (state) {
                    case BluetoothAdapter.STATE_TURNING_OFF: //블루투스가 꺼질 때
                        if (fitmeterEventCallback != null)
                            fitmeterEventCallback.statusOfBTManagerChanged(ConnectionStatus.POWERDOWN);

                        break;
                    case BluetoothAdapter.STATE_ON: //블루투스가 켜질 때
                        if (fitmeterEventCallback != null)
                            fitmeterEventCallback.statusOfBTManagerChanged(ConnectionStatus.POWERON);
                        break;
                }
            }
        }
    };


    BluetoothAdapter.LeScanCallback mScanRecoveryCallback = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {
            if (device != null) {
                if (device.getName() != null) {

                    if (device.getName().equals(DFU_DEVICE_NAME) || device.getName().equals("DfuTarg")) {
                        if (tempFitmeters.contains(device)) return;
                        tempFitmeters.add(device);

                        String serialNumber = getSerialNumber(scanRecord);
                        if (serialNumber != null) {
                            if (mFindAddress.equals(serialNumber)) {
                                Log.i(TAG, "복구 모드 장치 검색 성공 - " + serialNumber);
                                isStopScan = true;
                                isDeviceFound = true;

                                mBluetoothAdapter.stopLeScan(mScanRecoveryCallback);
                                if (fitmeterEventCallback != null)
                                    fitmeterEventCallback.findRecoveryDeviceResult(device, recoveryFitmeters);
                            }

                            return;
                        }

                        if (!recoveryFitmeters.contains(device)) {
                            recoveryFitmeters.add(device);
                        }

                    }
                }
            }
        }
    };

    BluetoothAdapter.LeScanCallback mScanCallback = new BluetoothAdapter.LeScanCallback() {

        @Override
        public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {
            if (device != null) {
                if (mFindAddress != null) {
                    if (tempFitmeters.contains(device)) return;
                    tempFitmeters.add(device);

                    String serialNumber = getSerialNumber(scanRecord);
                    if (serialNumber != null) {
                        if (mFindAddress.equals(serialNumber)) {

                            Log.i(TAG, "장치 검색 성공 - " + serialNumber);

                            isDeviceFound = true;
                            isStopScan = true;
                            mBluetoothAdapter.stopLeScan(mScanCallback);
                            if (fitmeterEventCallback != null) {
                                fitmeterEventCallback.findDeviceResult(device.getAddress(), mFindAddress);
                            }
                            return;
                        }
                    }

                    //기기 주소가 블루투스 주소인 경우
                    if (mFindAddress.equals(device.getAddress())) {
                        Log.i(TAG, "장치 검색 성공 - " + device.getAddress());
                        isDeviceFound = true;

                        isStopScan = true;
                        mBluetoothAdapter.stopLeScan(mScanCallback);
                        if (fitmeterEventCallback != null) {
                            fitmeterEventCallback.findDeviceResult(mFindAddress, serialNumber);
                        }
                    }
                }
            }
        }
    };

    BluetoothAdapter.LeScanCallback mScanFitmetersCallback = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {
            if (device != null) {
                if (deviceName.equals(device.getName())) {
                    if (rssi > rssiLimit) {
                        BLEDevice containDevice = contains(device);
                        if (containDevice == null)
                        {
                            byte[] manufactureData = getManufactureData(scanRecord);
                            if (manufactureData != null)
                            {
                                String serialNumber = null;
                                byte[] serialBytes = new byte[10];
                                System.arraycopy(manufactureData, 2, serialBytes, 0, serialBytes.length);
                                try {
                                    serialNumber = new String(serialBytes, "UTF-8");
                                } catch (UnsupportedEncodingException e) {
                                    e.printStackTrace();
                                }


                                if (serialNumber != null) {

                                    if (serialNumber.substring(3, 4).equals("4")) //밴드 시리얼일 때
                                    {
                                        if (mSearchFitmeterType.equals(FitmeterType.BAND)) {
                                            if (manufactureData.length > 12 && manufactureData[12] == 0x01) {
                                                BLEDevice bleDevice = new BLEDevice(device, serialNumber, rssi, BLEDevice.PUSH_BUTTON_PRESSED);
                                                connectableFitmeters.add(bleDevice);
                                                numberOfButtonPushedDevice++;

                                            } else {
                                                BLEDevice bleDevice = new BLEDevice(device, serialNumber, rssi, BLEDevice.PUSH_BUTTON_NORMAL);
                                                connectableFitmeters.add(bleDevice);
                                            }
                                        }

                                    } else if ( serialNumber.substring(3, 4).equals("3") ) { //BLE 시리얼일 때

                                        if (mSearchFitmeterType.equals(FitmeterType.BLE)) {

                                            if (manufactureData.length > 12 && manufactureData[12] == 0x01) {
                                                BLEDevice bleDevice = new BLEDevice(device, serialNumber, rssi, BLEDevice.PUSH_BUTTON_PRESSED);
                                                connectableFitmeters.add(bleDevice);
                                                numberOfButtonPushedDevice++;

                                                Log.e(TAG, "시리얼 번호 : " + serialNumber);
                                                Log.e(TAG, "타입 번호 : " + serialNumber.substring(3, 4));
                                                Log.e(TAG, "버튼 눌림");

                                            } else {
                                                BLEDevice bleDevice = new BLEDevice(device, serialNumber, rssi, BLEDevice.PUSH_BUTTON_NORMAL);
                                                connectableFitmeters.add(bleDevice);
                                            }
                                        }
                                    }
                                } else { //시리얼 데이터가 없으면 45로 기기로 등록
                                    if (mSearchFitmeterType.equals(FitmeterType.BLE)) {
                                        BLEDevice bleDevice = new BLEDevice(device, serialNumber, rssi , BLEDevice.PUSH_BUTTON_PRESSED);
                                        connectableFitmeters.add(bleDevice);
                                    }
                                }
                            } else { //Manufacture 데이터가 없으면 45기기로 보고 등록
                                if (mSearchFitmeterType.equals(FitmeterType.BLE)) {
                                    BLEDevice bleDevice = new BLEDevice(device, null, rssi , BLEDevice.PUSH_BUTTON_PRESSED);
                                    connectableFitmeters.add(bleDevice);
                                }
                            }
                        } else {
                            if (containDevice.isPushButton() != BLEDevice.NO_PUSH_BUTTON)
                            {
                                byte[] manufactureData = getManufactureData(scanRecord);
                                if (manufactureData != null)
                                {
                                    if (manufactureData.length > 12 && manufactureData[12] == 0x01)
                                    {
                                        if (containDevice.isPushButton() == BLEDevice.PUSH_BUTTON_NORMAL)
                                        {

                                            Log.e(TAG, "버튼 눌림 - 기기 이미 있음.");
                                            containDevice.setIsPushButton(BLEDevice.PUSH_BUTTON_PRESSED);
                                            numberOfButtonPushedDevice++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private BLEDevice contains(BluetoothDevice bluetoothDevice) {
            Iterator<BLEDevice> bleDeviceInfoIterator = connectableFitmeters.iterator();

            while (bleDeviceInfoIterator.hasNext()) {

                BLEDevice nextBleDevice = bleDeviceInfoIterator.next();
                if (bluetoothDevice.getAddress().equals(nextBleDevice.getBluetoothDevice().getAddress())) {
                    return nextBleDevice;
                }
            }

            return null;
        }
    };

    private synchronized byte[] getManufactureData(byte[] scanRecord) {
        String serialNumber = null;
        byte[] manufactureData = null;
        int index = 0;
        while (index < scanRecord.length) {
            int dataFieldLength = scanRecord[index++] & 0xFF;
            if (dataFieldLength > 0) {
                int dataType = scanRecord[index++] & 0xFF;
                if (dataType == 0xFF) {
                    manufactureData = new byte[dataFieldLength - 1];

                    for (int i = 0; i < manufactureData.length; i++) {
                        manufactureData[i] = scanRecord[index];
                        index++;
                    }

                } else {
                    index += (dataFieldLength - 1);
                }
            }
        }

        return manufactureData;
    }

    private synchronized String getSerialNumber(byte[] scanRecord) {
        String serialNumber = null;

        int index = 0;
        while (index < scanRecord.length) {
            int dataFieldLength = scanRecord[index++] & 0xFF;
            if (dataFieldLength > 0) {
                int dataType = scanRecord[index++] & 0xFF;
                if (dataType == 0xFF) {
                    index++; //0x59
                    index++; //0x00
                    byte[] serialBytes = new byte[10];
                    for (int i = 0; i < serialBytes.length; i++) {
                        serialBytes[i] = scanRecord[index + i];
                    }

                    try {
                        serialNumber = new String(serialBytes, "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    break;

                } else {
                    index += (dataFieldLength - 1);
                }
            }
        }

        return serialNumber;
    }

    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {

        private void makeUpConnection(ConnectionStatus connectionStatus) {
            if (fitmeterEventCallback != null) {
                fitmeterEventCallback.statusOfBTManagerChanged(connectionStatus);

                if (Mode_FileReceving) {
                    List<ReceivedDataInfo> temp = new ArrayList<ReceivedDataInfo>();
                    for (ReceivedDataInfo t : receivedDataInfoList) {
                        if (t.bytes != null) {
                            temp.add(t);
                        }
                    }
                    inFileDownLoad = false;
                    isDownLoadStarted = false;
                    fitmeterEventCallback.onReceiveAllFileComplete(temp, false);
                }
            }

            isDownLoadStarted = false;
            inFileDownLoad = false;
            close();
        }

        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, final int status, int newState) {
            Log.e(TAG, "onConnectionStateChange() : status - " + status + " newState - " + newState);

            if (newState == BluetoothProfile.STATE_CONNECTED) {
                if (status == BluetoothGatt.GATT_SUCCESS)
                {
                    Log.i(TAG, "Connected to GATT server.");
                    // Attempts to discover services after successful connection.
                    Log.i(TAG, "Attempting to start service discovery:" +
                            mBluetoothGatt.discoverServices());
                }else if(status == 133) {
                    makeUpConnection(ConnectionStatus.ERROR_DISCONNECTED_133);
                }else {
                    makeUpConnection(ConnectionStatus.ERROR_DISCONNECTED);
                }

            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                Log.i(TAG, "Disconnected from GATT server.");
                mConnectedState = false;
                isDownLoadStarted = false;
                inFileDownLoad = false;

                //129나 133으로 에러가 발생한 경우 로그를 보내도록 한다.
                if(status == 133){
                    makeUpConnection(ConnectionStatus.ERROR_DISCONNECTED_133);
                }else if (status == BluetoothGatt.GATT_SUCCESS) {
                    makeUpConnection(ConnectionStatus.DISCONNECTED);
                } else {
                    makeUpConnection(ConnectionStatus.ERROR_DISCONNECTED);
                }
            }
        }

        private String getMaxByteString(String str, int maxLen) {
            String result = "";
            int curLen = 0;
            String curChar;

            for (int i = str.length() - 1; i > 0; i--) {
                curChar = str.substring(i - 1, i);
                curLen += curChar.getBytes().length;
                if (curLen > maxLen)
                    break;
                else {
                    result = curChar + result;
                }
            }
            return result;
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {

                BluetoothGattService RxService = mBluetoothGatt.getService(serviceUUID);

                if (RxService == null) {
                    android.util.Log.i(TAG, "RxService disconver failed.");
                    makeUpConnection(ConnectionStatus.ERROR_DISCONNECTED);
                }

                TxChar = RxService.getCharacteristic(txUUID);
                if (TxChar == null) {
                    android.util.Log.i(TAG, "TxCharacteristic is null.");
                    makeUpConnection(ConnectionStatus.ERROR_DISCONNECTED);
                }

                RxChar = RxService.getCharacteristic(rxUUID);
                if (RxChar == null) {
                    android.util.Log.i(TAG, "RxCharacteristic is null.");
                    makeUpConnection(ConnectionStatus.ERROR_DISCONNECTED);
                }

                if (!setTXNotification()) {
                    android.util.Log.i(TAG, "TxNoti is failed.");
                    makeUpConnection(ConnectionStatus.ERROR_DISCONNECTED);
                }


            } else {
                android.util.Log.w(TAG, "onServicesDiscovered received: " + status);
                makeUpConnection(ConnectionStatus.ERROR_DISCONNECTED);
            }
        }

        public byte[] concat(byte[] a, byte[] b) {
            int aLen = a.length;
            int bLen = b.length;
            byte[] c = new byte[aLen + bLen];
            System.arraycopy(a, 0, c, 0, aLen);
            System.arraycopy(b, 0, c, aLen, bLen);
            return c;
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt,
                                         BluetoothGattCharacteristic characteristic,
                                         int status) {
            if (status != BluetoothGatt.GATT_SUCCESS) {
                Log.w(TAG, "onCharacteristicRead received: " + status);
                makeUpConnection(ConnectionStatus.ERROR_DISCONNECTED);
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt,
                                            BluetoothGattCharacteristic characteristic) {
            super.onCharacteristicChanged(gatt, characteristic);

            //maximum data size is only 20byte!! So if you want to read bigger than 20byte, you need do something here.
            if (Mode_FileReceving) {
                if (receivedBytes == null) {
                    receivedBytes = characteristic.getValue();
                    readSizePerPacket += receivedBytes.length;
                } else {
                    byte[] received = characteristic.getValue();
                    receivedBytes = concat(receivedBytes, received);
                    readSizePerPacket += received.length;
                }

                progressValue = (int) (progressValuePerFile * currentReceiveIndex + progressValuePerFile * receivedBytes.length / sizeToRead);

                if (fitmeterEventCallback != null) {
                    fitmeterEventCallback.onProgressValueChnaged(progressValue, 100);
                }


                if (receivedBytes.length == sizeToRead) {
                    //readComplete

                    Mode_FileReceving = false;
                    fileData = new byte[receivedBytes.length];
                    System.arraycopy(receivedBytes, 0, fileData, 0, receivedBytes.length);
                    Log.e(TAG, "Complete - 데이터 파일의 크기 " + receivedBytes.length);
                    receivedDataInfoList.get(currentReceiveIndex).bytes = fileData;

                    if (receivedDataInfoList.get(currentReceiveIndex).index == fileReceiveEndIndex) {
                        inFileDownLoad = false;
                        if (fitmeterEventCallback != null) {
                            fitmeterEventCallback.statusOfBTManagerChanged(ConnectionStatus.DISCONNECTED);
                        }

                        close();
                        isDownLoadStarted = false;
                        fitmeterEventCallback.onReceiveAllFileComplete(receivedDataInfoList, true);


                    } else {
                        currentReceiveIndex++;
                        new Thread(receiveFileThread).start();
                    }

                    return;
                }

                if (readSizePerPacket == sizePerpacketToRead) {
                    Thread thread = new Thread(new Runnable() {
                        public void run() {

                            int read = 0;
                            if (receivedBytes != null) {
                                read = receivedBytes.length;
                            }
                            int readAmount = sizeToRead - read;
                            if (readAmount > 512) {
                                readAmount = 512;
                            }

                            sizePerpacketToRead = readAmount;
                            readSizePerPacket = 0;

                            sendGetFileData(receivedDataInfoList.get(currentReceiveIndex));
                        }
                    });
                    thread.start();
                }
            } else {
                byte[] bytes = characteristic.getValue();

                if (receivedBytes == null) {
                    if (bytes[0] == BTManager.PC_COMM_START) {
                        receivedBytes = bytes;
                    } else {
                        return;
                    }

                } else {
                    receivedBytes = concat(receivedBytes, bytes);
                }

                if (receivedBytes[receivedBytes.length - 1] != BTManager.PC_COMM_END) {
                    ResponseReceived = false;
                } else {
                    ResponseReceived = true;
                }
            }

        }

        @Override
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            super.onDescriptorWrite(gatt, descriptor, status);

            if (status == BluetoothGatt.GATT_SUCCESS) {

                if (descriptor.getUuid().equals(CCCD)) {

                    Log.i(TAG, "Connection Success.");
                    mConnectedState = true;
                    if (fitmeterEventCallback != null)
                        fitmeterEventCallback.statusOfBTManagerChanged(ConnectionStatus.CONNECTED);
                }

            } else {
                Log.w(TAG, "onCharacteristicRead received: " + status);
            }
        }
    };


    public static BTManager getInstance(Context context, FitmeterEventCallback callback) {
        if (ourInstance == null) {
            ourInstance = new BTManager(context, callback);
        } else {
            ourInstance.fitmeterEventCallback = callback;
            ourInstance.context = context;
        }

        return ourInstance;
    }

    private BTManager(Context context, FitmeterEventCallback callback) {
        this.context = context;
        this.fitmeterEventCallback = callback;
        mBluetoothManager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = mBluetoothManager.getAdapter();
    }

    public void close() {

        if (mBluetoothGatt != null) {

            Log.d(TAG, "Call BluetoothGatt Close()");
            mBluetoothGatt.close();
            refreshDeviceCache(mBluetoothGatt);
            Log.d(TAG, "BluetoothGatt Connected Device - " + mBluetoothManager.getConnectedDevices(BluetoothProfile.GATT));

            mBluetoothGatt = null;
        }
    }

    private boolean refreshDeviceCache(BluetoothGatt gatt) {
        try {
            BluetoothGatt localBluetoothGatt = gatt;
            Method localMethod = localBluetoothGatt.getClass().getMethod("refresh", new Class[0]);
            if (localMethod != null) {
                boolean bool = ((Boolean) localMethod.invoke(localBluetoothGatt, new Object[0])).booleanValue();
                return bool;
            }
        } catch (Exception localException) {
            Log.e(TAG, localException.getMessage());
            return false;
        }

        return false;
    }

    public void disconnect() {
        if (mBluetoothGatt != null) {
            Log.d(TAG, "Call BluetoothGatt Disconnect()");
            mBluetoothGatt.disconnect();
        }
    }

    volatile boolean isStopScan = false;

    public void findFitmeter(String findAddress) {
        //계산이 중복되는 걸 막기 위해
        // 계산된 데이터가 있는데 클리어를 하지 않아 다시 계산된 경우가 있음
        // 연결되서 바로 떨어지는 경우. startDownload가 호출되지 않아서 그러는 경우임.
        receivedDataInfoList.clear();

        this.isDeviceFound = false;
        this.connectableFitmeters.clear();
        tempFitmeters.clear();
        this.mFindAddress = findAddress;
        isStopScan = false;
        this.mBluetoothAdapter.startLeScan(mScanCallback);

        new Thread(new Runnable() {
            @Override
            public void run() {

                double second = 0;
                while (second < 10000) {
                    if (isStopScan) {
                        break;
                    }

                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    second += 200;
                }

                if (!isStopScan) {
                    mBluetoothAdapter.stopLeScan(mScanCallback);
                }

                if (!isDeviceFound && fitmeterEventCallback != null) {
                    fitmeterEventCallback.statusOfBTManagerChanged(ConnectionStatus.NOTFOUND);
                }

            }
        }).start();

    }


    public void findFitmeters(FitmeterType fitmeterType) {
        this.numberOfButtonPushedDevice = 0;
        this.mSearchFitmeterType = fitmeterType;
        this.mFindAddress = null;
        this.connectableFitmeters.clear();
        isStopScan = false;
        this.mBluetoothAdapter.startLeScan(mScanFitmetersCallback);

        new Thread(new Runnable() {
            @Override
            public void run() {

                double second = 0;
                while (second < SCAN_TIME) {
                    if (isStopScan) {
                        break;
                    }

                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    second += 200;
                }

                if (!isStopScan)
                    mBluetoothAdapter.stopLeScan(mScanFitmetersCallback);

                if (connectableFitmeters.size() > 0)
                {
//                    if (numberOfButtonPushedDevice >= 1) {
//                        List<BLEDevice> buttonPushedDeviceList = new ArrayList<BLEDevice>();
//                        for (BLEDevice bleDevice : connectableFitmeters) {
//                            if (bleDevice.isPushButton() == BLEDevice.PUSH_BUTTON_PRESSED) {
//                                buttonPushedDeviceList.add(bleDevice);
//                            }
//                        }
//
//                        if (fitmeterEventCallback != null) {
//                            fitmeterEventCallback.findNewDeviceResult(buttonPushedDeviceList);
//                        }
//
//                    } else {
//                        Collections.sort(connectableFitmeters, myComparator);
//                        List<BLEDevice> tempFitmeters = new ArrayList<BLEDevice>();
//                        tempFitmeters.add(connectableFitmeters.get(0));
//                        if (fitmeterEventCallback != null) {
//                            fitmeterEventCallback.findNewDeviceResult(tempFitmeters);
//                        }
//                    }
                    List<BLEDevice> buttonPushedDeviceList = new ArrayList<BLEDevice>();
                    for (BLEDevice bleDevice : connectableFitmeters) {
                        if (bleDevice.isPushButton() == BLEDevice.PUSH_BUTTON_PRESSED)
                        {
                            buttonPushedDeviceList.add(bleDevice);
                        }
                    }

                    if (fitmeterEventCallback != null) {
                        fitmeterEventCallback.findNewDeviceResult(buttonPushedDeviceList);
                    }

                } else {
                    if (fitmeterEventCallback != null) {
                        fitmeterEventCallback.findNewDeviceResult(connectableFitmeters);
                    }
                }
            }
        }).start();
    }

    public void findRecoveryFitmeter(String findAddress) {
        isDeviceFound = false;
        mFindAddress = findAddress;
        recoveryFitmeters.clear();
        tempFitmeters.clear();

        isStopScan = false;
        this.mBluetoothAdapter.startLeScan(mScanRecoveryCallback);

        new Thread(new Runnable() {
            @Override
            public void run() {

                double second = 0;
                while (second < SCAN_TIME) {
                    if (isStopScan) {
                        break;
                    }

                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    second += 200;
                }

                if (!isStopScan)
                    mBluetoothAdapter.stopLeScan(mScanRecoveryCallback);
                if (!isDeviceFound & fitmeterEventCallback != null)
                    fitmeterEventCallback.findRecoveryDeviceResult(null, recoveryFitmeters);

            }
        }).start();
    }

    /**
     * 3초간 기다려서 연결되지 않으면 ConnectionStatus.NOTFOUND이벤트가 발생한다
     *
     * @param address
     * @return
     */
    public boolean connectFitmeter(final String address) {
        mConnectedState = false;
        Log.i(TAG, "connect()");

        if (mBluetoothAdapter == null || address == null) {
            android.util.Log.w(TAG, "BluetoothAdapter not initialized or unspecified address.");
            return false;
        }

        final BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        if (device == null) {
            Log.w(TAG, "Device not found.  Unable to connect.");
            return false;
        }
        // We want to directly connect to the device, so we are setting the autoConnect
        // parameter to false.
        mBluetoothGatt = device.connectGatt(context, false, mGattCallback);
        Log.d(TAG, "Trying to create a new connection.");
        mBluetoothDeviceAddress = address;
        return true;
    }

    public boolean setTXNotification() {
        boolean result = mBluetoothGatt.setCharacteristicNotification(TxChar, true);
        if (!result) {
            return result;
        }

        BluetoothGattDescriptor descriptor = TxChar.getDescriptor(CCCD);
        result = descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
        if (!result) {
            return result;
        }

        result = mBluetoothGatt.writeDescriptor(descriptor);
        if (!result) {
            return result;
        }

        return true;
    }

    public void stopScan() {
        isStopScan = true;
        if (mBluetoothAdapter != null)
            mBluetoothAdapter.stopLeScan(mScanCallback);
    }

    public void clear() {
        Log.d(TAG, "clear");
        this.fitmeterEventCallback = null;
        stopScan();

        //2016.10.12 , 박희정
        //NewActivity에서 OnStop일 때 지금 받고 있는 데이터를 계산해야 하는데 하지 않음. 그래서 데이터를 다 받지 못하면 계속 처음부터 시작하여 오래 걸림.
        //그래서 onStop일 때 를 추가하여 데어터 계산 호출 되도록 수정함. close는 discoonnect 콜백에서 호출됨.
        //disconnect();
        close();
    }

    /**
     * rx write to rx
     *
     * @param message     bytes to write
     * @param clearBuffer true if must clear receiving buffer, or false
     * @return true if writing success
     */
    private boolean writeToRx(byte[] message, boolean clearBuffer) {

        if (clearBuffer) {
            receivedBytes = null;
            Mode_FileReceving = false;
        }

        ResponseReceived = false;

        if (mBluetoothGatt == null) return false;
        BluetoothGattService service = mBluetoothGatt.getService(serviceUUID);
        BluetoothGattCharacteristic rx = service.getCharacteristic(rxUUID);

        boolean success = false;
        int length = message.length;
        int maxLength = 20;
        int index = 0;

        while (index < length) {

            int size = length - index;
            if (size > maxLength) {
                size = maxLength;
            }
            byte[] toSend = new byte[size];
            for (int i = 0; i < size; i++) {
                toSend[i] = message[i + index];
            }

            rx.setValue(toSend);
            success = mBluetoothGatt.writeCharacteristic(rx);

            if (!success) return success;

            index += size;

            if (index < length) {

                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        return success;

    }

    private volatile int sizeToRead;
    private volatile int indexToRead;
    volatile int sizePerpacketToRead = 0;
    volatile int readSizePerPacket = 0;

    private volatile boolean Mode_FileReceving = false;
    private volatile int currentReceiveIndex = 0;
    private volatile int fileReceiveEndIndex = 0;
    private volatile byte[] fileData;
    volatile List<ReceivedDataInfo> receivedDataInfoList = new ArrayList<ReceivedDataInfo>();
    volatile boolean inFileDownLoad = false;


    /**
     * 기기에 설정된 현재 시각을 읽는다
     *
     * @return
     */
    public TimeInfo getCurrentTime() {
        byte[] command = makeCommand(HeaderType.REQ_GET_TIMEINFO);

        boolean success = writeToRx(command, true);

        if (!success) {
            android.util.Log.e(TAG, "getCurrentTime write failed");
            return null;
        }

        waitResponse();
        if (!ResponseReceived) {
            //time out
            Log.e(TAG, "getCurrentTime response timeout");
            return null;
        } else {
            android.util.Log.i(TAG, "getCurrentTime read response");
        }

        //decoding
        byte[] readBytes = Decoder.decoding(receivedBytes);
        byte[] responseBytes = new byte[readBytes.length - 1];

        System.arraycopy(readBytes, 1, responseBytes, 0, readBytes.length - 1);
        TimeInfo rep = new TimeInfo(responseBytes);

        return rep;
    }

    /**
     * 배터리 용량을 읽는다 실패시 -1을 리턴한다
     *
     * @return 0~100
     */
    public int readBatteryRate( FitmeterType fitmeterType ) {
        byte[] command = this.makeCommand(HeaderType.REQ_GET_BATTERY);
        boolean success = writeToRx(command, true);

        if (!success) {
            Log.i(TAG, "readBatteryRate() write failed");
            return -1;
        }
        waitResponse();

        if (!ResponseReceived) {
            //timeout
            Log.i(TAG, "response timeout");
            return -1;

        } else {
            android.util.Log.i(TAG, "read response");
        }

        byte[] readBytes = Decoder.decoding(receivedBytes);
        //return  ProtocolUtils.calculateBatteryRatio( readBytes[1],  readBytes[2]);
        int batteryRatio = getBatteryChargeStatus( readBytes[1], readBytes[2] , fitmeterType );
        Log.e(TAG , "Battery Ratio : " + batteryRatio );
        return batteryRatio;
    }

    private int getBatteryChargeStatus(byte first, byte second , FitmeterType fitmeterType) {
        float voltage = first + second * 0.01f;
        float fullCharge = 0;

        if(fitmeterType.equals( FitmeterType.BAND )) {
            fullCharge = 4.2f;
        }else if( fitmeterType.equals( FitmeterType.BLE ) ){
            fullCharge = 4.1f;
        }

        float percent1 = 3.6f;

        Log.e(TAG , "Battery Voltage : " + voltage);
        if (voltage >= fullCharge) {
            return 100;
        } else if (voltage <= 3.6) {
            return 1;
        } else {

            float per_diff = 100 / (fullCharge - percent1);
            int remain = (int) Math.round(per_diff * (voltage - percent1));

            if (remain == 0) return 1;
            return remain;
        }
    }


    private volatile boolean isDownLoadStarted = false;

    private volatile int progressValue = 0;
    private double progressValuePerFile = 0;

    public void startDownload(int startIndex, int offsetOfStartIndex) {
        if (isDownLoadStarted) return;
        isDownLoadStarted = true;
        int[] numberOfFile = readNumberOfFile();

        if (numberOfFile == null) {

            this.Mode_FileReceving = false;
            if (mConnectedState) {
                disconnect();
            }

            return;
        }

        if (numberOfFile[0] <= startIndex && numberOfFile[1] >= startIndex) {
            numberOfFile[0] = startIndex;
        } else {
            offsetOfStartIndex = 0;
        }

        Log.e(TAG, "startIndex:" + numberOfFile[0] + ", offset:" + offsetOfStartIndex);

        fileReceive_StartIndex = numberOfFile[0];
        fileReceiveEndIndex = numberOfFile[1];

        int fileCount = fileReceiveEndIndex - fileReceive_StartIndex + 1;
        progressValue = 0;
        progressValuePerFile = 100.0 / fileCount;
        receivedDataInfoList.clear();

        for (int i = fileReceive_StartIndex; i <= fileReceiveEndIndex; i++) {
            ReceivedDataInfo receivedDataInfo = new ReceivedDataInfo(i);
            receivedDataInfo.setOffset(0);
            receivedDataInfoList.add(receivedDataInfo);
        }

        if (receivedDataInfoList.size() > 0) {
            receivedDataInfoList.get(0).offset = offsetOfStartIndex;
            currentReceiveIndex = 0;
            new Thread(receiveFileThread).start();

        } else {

            this.Mode_FileReceving = false;
            if (mConnectedState) {
                disconnect();
            }

        }
    }

    Runnable receiveFileThread = new Runnable() {
        @Override
        public void run() {

            synchronized (this) {

                Log.e(TAG, "receiveFileThread - 받을 파일 갯수  : " + receivedDataInfoList.size() + " 현재 인덱스 : " + currentReceiveIndex);
                ReceivedDataInfo dataToReceive = receivedDataInfoList.get(currentReceiveIndex);

                final int size = getSizeOfFile(dataToReceive.index);
                if (size == 0) {
                    if (mConnectedState) {
                        disconnect();
                    }

                    return;
                }

                if (size - dataToReceive.offset < 0) {
                    Log.e(TAG, "size minus!! : " + dataToReceive.size);

                    FitmateServiceDBManager serviceDBManager = new FitmateServiceDBManager(context);
                    serviceDBManager.deleteFile(dataToReceive.index);
                    dataToReceive.offset = 0;
                }
                dataToReceive.setSize(size - dataToReceive.offset);
                Log.e(TAG, "read size from device = " + size + " index = " + dataToReceive.index);
                Log.e(TAG, "size to read(size- offset) = " + dataToReceive.size);

                if (receivedDataInfoList.get(currentReceiveIndex).size == 0) {
                    //받을 파일이 없음...

                    if (currentReceiveIndex == receivedDataInfoList.size() - 1) { //마지막 파일이라면
                        isDownLoadStarted = false;
                        if (fitmeterEventCallback != null) {
                            isDownLoadStarted = false;
                            fitmeterEventCallback.onReceiveAllFileComplete(receivedDataInfoList, true);
                        }

                        if (mConnectedState) {
                            disconnect();
                        }

                    } else {
                        currentReceiveIndex++;
                        new Thread(receiveFileThread).start();
                    }

                } else {
                    receiveFile(dataToReceive);
                }
            }
        }
    };


    private void receiveFile(ReceivedDataInfo info) {
        Mode_FileReceving = true;

        sizeToRead = info.getSize();
        indexToRead = info.getIndex();
        receivedBytes = null;
        sendGetFileData(info);
    }


    private void sendGetFileData(ReceivedDataInfo info) {

//        byte[] indexBytes= ProtocolUtils.IntToByteArray(index);
//        byte[] sizeBytes= new byte[2];
//        byte[] offsetBytes= ProtocolUtils.IntToByteArray(offset);
//

        Log.e(TAG, "sendGetFileData index:" + info.index + " size:" + info.size + " offset:" + info.offset);
        int read = 0;
        if (receivedBytes != null) {
            read = receivedBytes.length;
            Log.e(TAG, "received bytes:" + read);
        }
        int readAmount = info.size - read;
        if (readAmount > 512) {
            readAmount = 512;
        }
        sizePerpacketToRead = readAmount;
        readSizePerPacket = 0;
//
//        sizeBytes[0]= (byte)((readAmount>>8) &0xff);Fc
//        sizeBytes[1]= (byte)(readAmount  & 0xff);

        byte[] arrFileIndex = ProtocolUtils.IntToByteArray(info.index);
        byte[] arrOffset = ProtocolUtils.IntToByteArray(info.offset + read);
        byte[] arrLength = ProtocolUtils.shortToByteArray(readAmount);

        byte[] arrSendData = new byte[]{HeaderType.REQ_FILE_READ, arrFileIndex[0], arrFileIndex[1], arrFileIndex[2], arrFileIndex[3], arrOffset[0], arrOffset[1], arrOffset[2], arrOffset[3], arrLength[0], arrLength[1]};

        boolean success = writeToRx(Encoder.encoding(arrSendData), false);

        if (!success) {
            Log.i(TAG, "sendGetFileData write failed");
            return;
        }
    }


    /**
     * read file indexes in the connected fitmeter.
     */
    private int[] readNumberOfFile() {
        byte[] command = makeCommand(HeaderType.REQ_NUMBER_OF_FILE);

        boolean success = writeToRx(command, true);

        if (!success) {
            Log.i(TAG, "readNumberOfFile write failed");
            return null;
        }

        waitResponse();

        if (!ResponseReceived) {
            //timeout
            Log.i(TAG, "response timeout");
            return null;

        } else {
            android.util.Log.i(TAG, "read response");
        }

        //decoding
        byte[] readBytes = Decoder.decoding(receivedBytes);

        byte[] startBytes = new byte[4];
        final byte[] endBytes = new byte[4];

        try {
            for (int i = 0; i < 4; i++) {
                startBytes[i] = readBytes[1 + i];
            }
            for (int i = 0; i < 4; i++) {
                endBytes[i] = readBytes[5 + i];
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            return null;
        }

        //if the device has 3,4,5,6  then return startIndex= 3, endIndex=6.( Index is id of the file)
        final int startIndex = byteArrayToInt(startBytes);
        final int endIndex = byteArrayToInt(endBytes);

        if (startIndex > endIndex) {
            return null;
        }

        if (startIndex < 0 || endIndex < 0) {
            return null;
        }

        return new int[]{startIndex, endIndex};
    }

    /**
     * make command
     *
     * @param header
     * @return
     */
    private byte[] makeCommand(byte header) {
        byte[] bytes = new byte[1];
        bytes[0] = header;
        return Encoder.encoding(bytes);

    }

    /**
     * make message to send
     *
     * @param header
     * @param body
     * @return
     */
    public byte[] makeMessage(byte header, byte[] body) {
        byte[] message = new byte[body.length + 1];
        message[0] = header;
        System.arraycopy(body, 0, message, 1, body.length);

        return message;
    }

    /**
     * get size of the file
     *
     * @param index
     * @return
     */
    public int getSizeOfFile(int index) {


        byte[] arrByteIndex = ProtocolUtils.IntToByteArray(index);
        byte[] arrFileInfo = this.makeMessage(HeaderType.REQ_FILE_INFO, arrByteIndex);


        boolean success = writeToRx(Encoder.encoding(arrFileInfo), true);

        if (!success) {
            Log.i(TAG, "getSizeOfFile write failed");
            return 0;
        }
        waitResponse();

        if (!ResponseReceived) {
            //timeout
            Log.i(TAG, "response timeout");
            return 0;

        } else {
            android.util.Log.i(TAG, "read response");
        }

        byte[] readBytes = Decoder.decoding(receivedBytes);
        byte[] sizeBytes = new byte[4];

        for (int i = 0; i < 4; i++) {
            sizeBytes[i] = readBytes[1 + i];
        }
        return byteArrayToInt(sizeBytes);
    }

    /**
     * wait to response
     */
    private void waitResponse() {
        ResponseReceived = false;
        final long k2 = 20;
        long k = k2;


        try {
            Thread.sleep(k2);
            while (!ResponseReceived) {
                if (k > timeoutSetting) {
                    break;
                }
                k += k2;
                Thread.sleep(k2);

            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return;
    }

    private static int byteArrayToInt(byte[] b) {
        return b[3] & 0xFF |
                (b[2] & 0xFF) << 8 |
                (b[1] & 0xFF) << 16 |
                (b[0] & 0xFF) << 24;
    }

    private static byte[] intToByteArray(int a) {
        return new byte[]{
                (byte) ((a >> 24) & 0xFF),
                (byte) ((a >> 16) & 0xFF),
                (byte) ((a >> 8) & 0xFF),
                (byte) (a & 0xFF)
        };
    }

    public boolean deleteAllFile() {
        byte[] command = makeCommand(HeaderType.REQ_DELETE_ALL_FILE);

        boolean success = writeToRx(command, true);
        if (!success) {
            android.util.Log.i(TAG, "write command failed-delete all");
            return false;
        }

        waitResponse();

        if (ResponseReceived) {
            android.util.Log.i(TAG, "delete success");
            return true;
        } else {
            android.util.Log.i(TAG, "no response - delete all");
            return false;
        }
    }

    /**
     * set led of fitmeter
     *
     * @param led
     * @return
     */
    private boolean setLed(byte led) {
        //skyblue led on
        android.util.Log.i("fitmate", "Mode_FileREceiving : " + Mode_FileReceving);

        byte[] command = makeCommand(led);
        boolean success = writeToRx(command, true);

        if (success) {
            android.util.Log.i(TAG, "tx success");
        } else {
            android.util.Log.i(TAG, "tx failed");
        }

        waitResponse();

        if (!ResponseReceived) {
            //timeout
            android.util.Log.i(TAG, "response timeout");
            return false;
        } else {
            android.util.Log.i(TAG, "read response");
        }


        return true;

    }

    /**
     * turn light on led
     */
    public boolean turnonled(int ledColor) {
        return setLed((byte) ledColor);
    }

    /**
     * turn the led off
     */
    public boolean turnoffled() {
        return setLed((byte) LED_COLOR_OFF);
    }

    /**
     * set time to fitmeter (if the fitmeter battery was total discharged, you must set time to current time before use)
     */
    public boolean setCurrentTime() {
        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        Date currentDate = calendar.getTime();
        TimeInfo timeInfo = new TimeInfo(currentDate.getTime());

        byte[] command = this.makeMessage(HeaderType.REQ_SET_TIMEINFO, timeInfo.getBytes());
        boolean success = writeToRx(Encoder.encoding(command), true);

        if (!success) {
            Log.i(TAG, "setCurrentTime write failed");
            return false;
        }

        waitResponse();

        if (!ResponseReceived) {
            //timeout
            Log.i(TAG, "no response - setCurrentTime()");
            return false;

        } else {
            android.util.Log.i(TAG, "setCurrentTime() Success");
            return true;
        }
    }

    public SystemInfo_Response getSystemInfo() {

        byte[] command = makeCommand(HeaderType.REQ_GET_SYSTEMINFO);
        boolean success = writeToRx(command, true);

        if (!success) {
            Log.i(TAG, " getSystemInfo write failed");
            return null;
        }
        waitResponse();

        if (!ResponseReceived) {
            //timeout
            Log.i(TAG, "response timeout");
            return null;

        } else {
            android.util.Log.i(TAG, "read response");
        }

        //decoding
        byte[] readBytes = Decoder.decoding(receivedBytes);

        byte[] removeHeader = new byte[readBytes.length - 1];

        System.arraycopy(readBytes, 1, removeHeader, 0, readBytes.length - 1);

        SystemInfo_Response systemInfoResponse = new SystemInfo_Response(removeHeader);

        return systemInfoResponse;
    }

    public boolean setSystemInfo(SystemInfo_Request systemInfo) {

        byte[] message = makeMessage(HeaderType.REQ_SET_SYSTEMINFO, systemInfo.getBytes());
        boolean success = writeToRx(Encoder.encoding(message), true);

        if (!success) {
            Log.i(TAG, "setSavinginterval write failed");
            return false;
        }

        waitResponse();

        if (!ResponseReceived) {
            //timeout
            Log.i(TAG, "response timeout");
            return false;

        } else {
            android.util.Log.i(TAG, "read response");
        }

        if (receivedBytes[1] == HeaderType.RES_SUCCESS) {
            android.util.Log.i(TAG, "setSystemInfo success");
        } else {
            Log.i(TAG, "setSystemInfo fail");
        }

        return true;
    }

    /**
     * set WearingLocation
     *
     * @param wearingLocation
     * @return
     */
    public boolean setWeringLocation(int wearingLocation) {

        SystemInfo_Response systemInfo_response = getSystemInfo();
        if (systemInfo_response == null) {
            android.util.Log.d(TAG, "getSystemInfo is null");
            return false;
        }
        SystemInfo_Request systemInfo_request = systemInfo_response.getSystemInfo();
        systemInfo_request.setWearingPosition(wearingLocation);

        boolean sucess = setSystemInfo(systemInfo_request);
        return sucess;
    }

    public void registerBTReceiver(Context context) {
        //블루투스가 꺼질때와 켜질때를 알수 있는 리시버를 등록한다.
        IntentFilter bluetoothFilter = new IntentFilter();
        bluetoothFilter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        context.registerReceiver(this.mReceiver, bluetoothFilter);
    }

    public void setBootMode() {

        byte[] value = new byte[4];
        value[0] = (byte) 0xf5;
        value[1] = (byte) 0xb2;
        value[3] = (byte) 0xfa;
        value[2] = (byte) 0xb2;
        writeToRx(value, true);

    }

    public List<ReceivedDataInfo> getReceivedDataInfoList()
    {
        List<ReceivedDataInfo> result = new ArrayList<ReceivedDataInfo>();
        if(Mode_FileReceving)
        {
            for (ReceivedDataInfo t : receivedDataInfoList) {
                if (t.bytes != null) {
                    result.add(t);
                }
            }
        }

        return result;
    }

    public void unRegisterBTReceiver( Context context ){
        context.unregisterReceiver(mReceiver);
    }

    final protected char[] hexArray = "0123456789ABCDEF".toCharArray();
    public String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    private Comparator myComparator= new Comparator<BLEDevice>()
    {
        @Override
        public int compare( BLEDevice object1,BLEDevice object2) {
            return object1.getRssi() > object2.getRssi() ? -1 : object1.getRssi() < object2.getRssi() ? 1:0;
        }
    };


}
