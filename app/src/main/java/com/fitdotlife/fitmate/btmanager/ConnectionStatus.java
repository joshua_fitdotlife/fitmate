package com.fitdotlife.fitmate.btmanager;

/**
 * Created by fitdotlife_dev on 2016. 5. 27..
 *
 * Status of BTManager
 */
public enum ConnectionStatus {
    /**
     *  idle
     */
    IDLE,
    /**
     *  start scanning
     */
    SCANNING,
    /**
     *  connected
     */
    CONNECTED,
    /**
     *  disconnected
     */
    DISCONNECTED,
    /**
     * disconnected error occured
     */
    ERROR_DISCONNECTED,
    /**
     *  search complete of new Fitemters
     */
    SCANNEWDEVICECOMPLETE,
    /**
     *  not found you want to connect
     */
    NOTFOUND,
    /**
     * Powered On of Centeral
     */
    POWERON,
    /**
     * Powered Down of Centeral
     */
    POWERDOWN,

    ERROR_DISCONNECTED_133
//    /**
//     *  found fitmeter
//     */
//    FOUND
}
