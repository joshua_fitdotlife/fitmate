package com.fitdotlife.fitmate;

import android.app.Activity;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.fitdotlife.fitmate_lib.object.ExerciseProgram;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringArrayRes;
import org.androidannotations.annotations.res.StringRes;

/**
 * Created by Joshua on 2016-08-17.
 */
@EActivity(R.layout.activity_activityclass)
public class ActivityClassActivity extends Activity {

    private final static String TAG = ActivityClassActivity.class.getSimpleName();
    public static final int COMMON = 0;
    public static final int MET = 1;
    public static final String TYPE_KEY = "type";

    private int mFirstTop = 0;
    private int mScrollGap = 0;

    @ViewById(R.id.ic_activity_activitiesclassification_actionbar)
    View actionBarView;

    @StringRes(R.string.activity_category_title)
    String title;

    @StringArrayRes(R.array.activityclass_metlist)
    String[] metList;

    @StringArrayRes(R.array.activityclasslist)
    String[] descriptionList;

    @ViewById(R.id.ll_activity_activityclass_metlist)
    LinearLayout llMetList;

    @ViewById(R.id.tv_activiy_activityclass_strengthrange)
    TextView tvStrengthRange;

    @ViewById(R.id.rl_activity_activityclass_fromstrength)
    RelativeLayout rlFromStrength;

    @ViewById(R.id.rl_activity_activityclass_tostrength)
    RelativeLayout rlToStrength;

    @ViewById(R.id.ll_activity_activityclass_targetmet)
    LinearLayout llTargetMet;

    @ViewById(R.id.sv_activity_activityclass)
    ScrollView svMetList;

    @ViewById(R.id.view_activityclass_gap)
    View viewGap;

    @AfterViews
    void onInit(){
        displayActionBar();
        int type = getIntent().getIntExtra( TYPE_KEY , 0 );
        double fromStrength = 0;
        double toStrength = 0;
        if( type == MET )
        {
            llTargetMet.setVisibility(View.VISIBLE);
            fromStrength = getIntent().getDoubleExtra(ExerciseProgram.STRENGTH_FROM_KEY, 0);
            toStrength = getIntent().getDoubleExtra(ExerciseProgram.STRENGTH_TO_KEY, 0);
            if (toStrength >= 99) {
                tvStrengthRange.setText(fromStrength + " MET~");
            } else {
                tvStrengthRange.setText(fromStrength + " - " + toStrength + " MET");
            }

            if( fromStrength == 1.0 ){
                viewGap.setVisibility(View.VISIBLE);
            }

            toStrength += 0.1;

        }else if(type == COMMON){

            rlFromStrength.setVisibility(View.INVISIBLE);
            rlToStrength.setVisibility(View.INVISIBLE);
            llTargetMet.setVisibility(View.GONE);
            fromStrength = -1;
            toStrength = 99;
        }

        diaplayMetList(fromStrength, toStrength , type);
    }

    private void displayActionBar( ){

        TextView tvBarTitle = (TextView) actionBarView.findViewById( R.id.tv_custom_actionbar_white_title );
        tvBarTitle.setText(title);

        ImageView imgBack = (ImageView) actionBarView.findViewById(R.id.img_custom_actionbar_white_left);
        imgBack.setVisibility(View.VISIBLE);
        imgBack.setBackgroundResource(R.drawable.icon_back_red_selector);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ImageView mImgRight = (ImageView) actionBarView.findViewById(R.id.img_custom_actionbar_white_right);
        mImgRight.setVisibility(View.GONE);
    }

    @Click(R.id.img_activity_activityclass_targetmet)
    void activityClassClick(){

        svMetList.scrollTo( 0 , mFirstTop - mScrollGap );
    }

    private void diaplayMetList( final double fromStrength , double toStrength , int type )
    {

        final int[] metIndex = new int[]{-1 , -1};
        LayoutInflater inflater = this.getLayoutInflater();
        for( int i = 0 ; i < metList.length ; i++ )
        {
            float currentMet = Float.parseFloat( metList[i] );

            View contentView = inflater.inflate( R.layout.item_activityclass_content , llMetList , false );

            TextView tvMet = (TextView) contentView.findViewById(R.id.tv_item_activityclass_cotnet_met);
            TextView tvContent = (TextView) contentView.findViewById(R.id.tv_item_activityclass_cotnet);

            if( fromStrength <= currentMet && toStrength >= currentMet ) {

                if( metIndex[0] > -1 ) {
                    metIndex[1] = i + 1;
                }else{
                    metIndex[0] = i + 1;
                }

                tvMet.setTextColor( getResources().getColor(R.color.activityclass_enable_content_met) );
                contentView.setBackgroundColor(getResources().getColor(R.color.activityclass_enable_content_background));
                tvContent.setTextColor(getResources().getColor(R.color.activityclass_enable_content_description));
            }else{
                tvMet.setTextColor( getResources().getColor(R.color.activityclass_disable_content_met) );
                contentView.setBackgroundColor(getResources().getColor(R.color.activityclass_disable_content_background));
                tvContent.setTextColor(getResources().getColor(R.color.activityclass_disable_content_description));
            }

            tvMet.setText(metList[i]);
            tvContent.setText(descriptionList[i]);
            llMetList.addView(contentView);
        }

        if( type == MET ) {
            llMetList.post(new Runnable() {
                @Override
                public void run() {
                    mFirstTop = llMetList.getChildAt(metIndex[0]).getTop();
                    int secondTop = llMetList.getChildAt(metIndex[1]).getTop();
                    int secondHeight = llMetList.getChildAt(metIndex[1]).getMeasuredHeight();
                    int fromStrengthLocationY = mFirstTop - (rlFromStrength.getMeasuredHeight() / 2);
                    int toStrengthLocationY = (secondTop + secondHeight) - (rlToStrength.getMeasuredHeight() / 2);

                    FrameLayout.LayoutParams firstParams = (FrameLayout.LayoutParams) rlFromStrength.getLayoutParams();
                    firstParams.setMargins(0, fromStrengthLocationY, 0, 0);
                    FrameLayout.LayoutParams secondParams = (FrameLayout.LayoutParams) rlToStrength.getLayoutParams();
                    secondParams.setMargins(0, toStrengthLocationY, 0, 0);

                    mScrollGap = (int) TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP , 11 , getResources().getDisplayMetrics() );
                    svMetList.scrollTo( 0 , mFirstTop - mScrollGap );

                }
            });
        }
    }

}