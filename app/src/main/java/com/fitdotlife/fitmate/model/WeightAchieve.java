package com.fitdotlife.fitmate.model;

/**
 * Created by fitlife.soondong on 2015-05-15.
 */
public enum WeightAchieve {
    /**
     * 미측정
     */
    NOTYET(-1),
    /**
     * 실패
     */
    FAIL(0),
    /**
     * 성공
     */
    SUCCESS(1);


    public int Value;
    WeightAchieve(int value){
        this.Value= value;
    }
}


