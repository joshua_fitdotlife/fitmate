package com.fitdotlife.fitmate.model;

import com.fitdotlife.fitmate_lib.object.UserInfo;

/**
 * Created by Joshua on 2015-02-25.
 */
public interface UserInfoModelResultListener {

    public void onSuccess( int code );
    public void onSuccess( int code , boolean result );
    public void onSuccess( int code , UserInfo userInfo );
    public void onFail( int code);
    public void onErrorOccured(int code , String message);
}
