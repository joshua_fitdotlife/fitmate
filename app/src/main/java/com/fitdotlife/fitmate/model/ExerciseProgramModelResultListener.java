package com.fitdotlife.fitmate.model;

import com.fitdotlife.fitmate_lib.object.ExerciseProgram;

import java.util.List;

/**
 * Created by Joshua on 2015-02-25.
 */
public interface ExerciseProgramModelResultListener {

    public void onExerciseProgramListReceived(int messageCode, List<ExerciseProgram> programList);
    public void onExerciseProgramReceived(int messageCode, ExerciseProgram program);
    public void onExerciseProgramModelErrorOccured(int messageCode, String message);
    public void onExerciseProgramModelResultReceived(int messageCode, String result);
    public void onExerciseProgramModelResultReceived(int messageCode, int result);

}
