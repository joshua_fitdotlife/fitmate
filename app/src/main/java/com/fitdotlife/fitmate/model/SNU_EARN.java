package com.fitdotlife.fitmate.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by fitlife.soondong on 2015-05-15.
 */
//public class SNU_EARN implements Parcelable {
public class SNU_EARN implements Parcelable, Serializable {
    public int current;
    public int lost;
    public int futurePossible;
    public int weeknum;

    public static final String KEY="SNU_EARN";


    public SNU_EARN(){

    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeInt((current));
        dest.writeInt((lost));
        dest.writeInt((futurePossible));
        dest.writeInt((weeknum));
    }

    private SNU_EARN(Parcel source) {
        // TODO Auto-generated constructor stub
        this.current=source.readInt();
        this.lost =source.readInt();
        this.futurePossible = source.readInt();
        this.weeknum = source.readInt();

    }
    public static final Parcelable.Creator<SNU_EARN> CREATOR = new Parcelable.Creator<SNU_EARN>() {

        @Override
        public SNU_EARN createFromParcel(Parcel in) {
            return new SNU_EARN(in);
        }

        @Override
        public SNU_EARN[] newArray(int size) {
            // TODO Auto-generated method stub
            return new SNU_EARN[size];
        }
    };
}
