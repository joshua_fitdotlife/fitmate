package com.fitdotlife.fitmate.model;

import android.content.Context;

import com.fitdotlife.fitmate_lib.object.ExerciseProgram;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;

import java.util.Calendar;
import java.util.List;

/**
 * Created by Joshua on 2015-02-10.
 */
public class ExerciseProgramModel{

    private Context mContext = null;
    private FitmateDBManager mDBManager = null;
    private ServiceResultListener mListener = null;

    public ExerciseProgramModel( Context context  )
    {
        this.mContext = context;
        this.mDBManager = new FitmateDBManager( context );
    }

    public List<ExerciseProgram> getUserProgramList() {
        return this.mDBManager.getUserExerciseProgramList();
    }

    public List<ExerciseProgram> getUserProgramList(String searchWord) {
        return this.mDBManager.getUserExerciseProgramList( searchWord );
    }


    public List<ExerciseProgram> getProgramList() {
        return this.mDBManager.getExerciseProgramList();
    }

    public List<ExerciseProgram> getProgramList( String searchWord ) {
        return this.mDBManager.getExerciseProgramList( searchWord );
    }

    public ExerciseProgram getExerciseProgram( int id )
    {
        return this.mDBManager.getUserExerciProgram(id);
    }

    public ExerciseProgram getUserExerciseProgram( int id )
    {
        return this.mDBManager.getUserExerciProgram(id);
    }

    public boolean createUserExerciseProgram(ExerciseProgram program)
    {
        return this.mDBManager.setUserProgram( program );
    }

    public ExerciseProgram getAppliedProgram()
    {
        return this.mDBManager.getAppliedProgram();
    }

    public void applyExerciseProgram( ExerciseProgram exerciseProgramSNU){

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -calendar.get(Calendar.DAY_OF_WEEK) + 1);
        String weekStartDate = calendar.get(Calendar.YEAR) + "-" + String.format("%02d", calendar.get(Calendar.MONTH) + 1) + "-" + String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH));

        this.mDBManager.setExerciseProgramIDonWeekActivity(weekStartDate , exerciseProgramSNU.getId());
        this.mDBManager.setAppliedExerciseProgram(exerciseProgramSNU);
    }

    public void modifyExerciseProgram( ExerciseProgram exerciseProgramSNU){
        this.mDBManager.modifyExerciseProgram(exerciseProgramSNU);
    }

    public void applyExerciseProgram( int exerciseProgramID ){
        this.mDBManager.setAppliedExerciseProgram( exerciseProgramID );
    }

}