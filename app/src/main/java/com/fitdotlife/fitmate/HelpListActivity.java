package com.fitdotlife.fitmate;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


public class HelpListActivity extends Activity implements View.OnClickListener {

    private LinearLayout llHomeHelp;
    private LinearLayout llActivityHelp;
    private LinearLayout llExerciseProgamHelp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help_list);

        View actionBarView = this.findViewById(R.id.ic_activity_helplist_actionbar);
        actionBarView.setBackgroundColor( 0xFF3C3C3C );
        TextView tvBarTitle = (TextView) actionBarView.findViewById( R.id.tv_custom_action_bar_title );
        ImageView imgLeft = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_left);
        imgLeft.setVisibility(View.GONE);
        ImageView mImgRight = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_right);
        mImgRight.setVisibility(View.GONE);
        tvBarTitle.setText( this.getString(R.string.help_bar_title));

        this.llHomeHelp = (LinearLayout) this.findViewById(R.id.ll_activity_helplist_home);
        this.llHomeHelp.setOnClickListener(this);
        //this.llActivityHelp = (LinearLayout) this.findViewById(R.id.ll_activity_helplist_activity);
        //this.llActivityHelp.setOnClickListener(this);
        this.llExerciseProgamHelp = (LinearLayout) this.findViewById(R.id.ll_activity_helplist_exerciseprogram);
        this.llExerciseProgamHelp.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch( view.getId() ){
            case R.id.ll_activity_helplist_home:
                    this.showHomeTutorialActivity();
                break;
            //case R.id.ll_activity_helplist_activity:
            //        this.showHelpActivity( HelpActivity.ACTIVITY_HELP_MODE );
            //    break;
            case R.id.ll_activity_helplist_exerciseprogram:
                    this.showHelpActivity( HelpActivity.EXERCISE_HELP_MODE );
                break;
        }
    }

    private void showHomeTutorialActivity(){
        Intent intent = new Intent( HelpListActivity.this , NewHomeTutorialActivity_.class );
        this.startActivity(intent);
    }

    private void showHelpActivity( int mode ){
        Intent intent = new Intent( HelpListActivity.this , HelpActivity.class );
        intent.putExtra( HelpActivity.MODE_KEY, mode );
        intent.putExtra( HelpActivity.VISIBLE_KEY, false );
        this.startActivity(intent);
    }
}
