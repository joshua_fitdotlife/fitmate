package com.fitdotlife.fitmate;

import android.app.Activity;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.fitdotlife.fitmate_lib.http.NetworkClass;

import java.util.Locale;


public class ServiceAgreementActivity extends Activity implements View.OnClickListener {

    private WebView wvServiceAgreement = null;
    private ImageView imgClose = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_agreement);

        this.wvServiceAgreement = (WebView) this.findViewById(R.id.wv_service_agreement);
        this.wvServiceAgreement.getSettings().setJavaScriptEnabled(true);
        this.wvServiceAgreement.setWebViewClient(new WebViewClient());
       // String htmlPath = "file:///android_asset/serviceagreement-" + this.getString(R.string.local_prefix) + ".html";

        Locale current = getResources().getConfiguration().locale;
        String language = current.getLanguage();
        String locale="en";
        if(language.equals("ko")){
            locale="ko";
        }else if(language.equals("ja")){
            locale="ja";
        }
        //String htmlPath = NetworkClass.baseURL +"/serviceagreement_"+locale+".html";
        String htmlPath = "file:///android_asset/serviceagreement_"+locale+".html";



        this.wvServiceAgreement.loadUrl(htmlPath);

        this.imgClose = (ImageView) this.findViewById(R.id.img_activity_service_agreement_close);
        this.imgClose.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch( view.getId() ){
            case R.id.img_activity_service_agreement_close:
                this.finish();
                break;
        }
    }
}
