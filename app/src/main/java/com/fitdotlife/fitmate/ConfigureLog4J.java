package com.fitdotlife.fitmate;

import java.io.File;
import org.apache.log4j.Level;

import android.content.Context;
import android.os.Environment;

import de.mindpipe.android.logging.log4j.LogConfigurator;

/**
 * Call {@link #configure()}} from your application's activity.
 */
public class ConfigureLog4J {
    public static String FilePath = "";
    public static String FolderPath = "";

    public static void configure(Context ctx) {
        final LogConfigurator logConfigurator = new LogConfigurator();

        LogConfigurator configurator = new LogConfigurator();

        // path
        String appName = ctx.getString(R.string.app_name);
        String logPaths = Environment.getExternalStorageDirectory() + File.separator + appName;

        FolderPath = logPaths;
        String logPath = logPaths;
        // create directory, which directory is not exists
        new File(logPath).mkdirs();

        logPath += File.separator + appName + ".log";

        FilePath = logPath;


        configurator.setFileName(logPath);
        configurator.setFilePattern("%d - [%p] - %m%n");     // log pattern
        configurator.setMaxFileSize(1 * 1024 * 1024);                 // file size(byte) 1MB
        configurator.setMaxBackupSize(5);                        // number of backup file

        configurator.setRootLevel(Level.DEBUG);                  // set log level
        configurator.setUseLogCatAppender(true);                 // and use Logcat

        // set log level of a specific logger
        configurator.setLevel("org.apache", Level.ERROR);
        configurator.configure();
    }
}