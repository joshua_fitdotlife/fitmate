package com.fitdotlife.fitmate;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.fitdotlife.fitmate_lib.customview.FitmeterDialog;
import com.fitdotlife.fitmate_lib.iview.IFindPasswordView;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.presenter.FindPasswordPresenter;

public class SeachPasswordDialog implements IFindPasswordView
{
	private EditText etxSearchEmail = null;
	private Context mContext = null;
	private FindPasswordPresenter mPresenter = null;
	private FitmeterDialog mDialog = null;

	public SeachPasswordDialog( Context context  )
	{
		this.mContext = context;
		this.mPresenter = new FindPasswordPresenter( this.mContext , this );
	}

	public void show(){


		this.mDialog = new FitmeterDialog(this.mContext);

		LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View contentView = inflater.inflate( R.layout.dialog_search_password , null );
		this.etxSearchEmail = (EditText) contentView.findViewById(R.id.etx_dialog_search_password_email);
		mDialog.setContentView( contentView );

		View buttonView = inflater.inflate( R.layout.dialog_button_two , null );
		Button btnLeft = (Button) buttonView.findViewById(R.id.btn_dialog_button_two_left);
		btnLeft.setText(mContext.getString(R.string.common_cancel));
		btnLeft.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mDialog.close();
			}
		});
		Button btnRight = (Button) buttonView.findViewById(R.id.btn_dialog_button_two_right);
		btnRight.setText(mContext.getString(R.string.common_find));
		btnRight.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if( validInput() )
				{
					mPresenter.findUser(etxSearchEmail.getText().toString());
				}
			}
		});
		mDialog.setButtonView(buttonView);

		this.mDialog.show();
	}

	@Override
	public void navigateToSearchDialog( UserInfo userInfo)
	{
		SendPasswordDialog sendPasswordDialog = new SendPasswordDialog( this.mContext );
		sendPasswordDialog.show(userInfo.getName(), this.etxSearchEmail.getText().toString());

		this.mDialog.close();
	}
	
	private boolean validInput()
	{
		if( this.etxSearchEmail.getText().length() == 0 )
		{
			this.showMessage(this.mContext.getString(R.string.find_password_input_email));
			this.etxSearchEmail.requestFocus();
			return false;
		}
		return true;
	}


	@Override
	public void showResult(String message)
	{
		this.showMessage(message);
	}

	@Override
	public void dismisDialog() {
		if( this.mDialog != null ){
			this.mDialog.close();
		}
	}

	@Override
	public void startProgressDialog() {

	}

	@Override
	public void stopProgressDialog() {

	}

	private void showMessage( String message )
	{
		Toast.makeText(this.mContext, message, Toast.LENGTH_LONG).show();
	}
	
	
}
