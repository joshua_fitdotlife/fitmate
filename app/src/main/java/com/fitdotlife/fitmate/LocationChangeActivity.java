package com.fitdotlife.fitmate;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.WearingLocation;
import com.fitdotlife.fitmate.btmanager.BTManager;
import com.fitdotlife.fitmate.btmanager.ConnectionStatus;
import com.fitdotlife.fitmate.btmanager.FitmeterEventCallback;
import com.fitdotlife.fitmate.btmanager.ReceivedDataInfo;
import com.fitdotlife.fitmate_lib.customview.FitmeterDialog;
import com.fitdotlife.fitmate_lib.customview.FitmeterProgressDialog;
import com.fitdotlife.fitmate_lib.object.BLEDevice;

import org.apache.log4j.Log;

import java.util.List;


public class LocationChangeActivity extends FragmentActivity
{
    private String TAG = "fitmate - " + LocationChangeActivity.class.getSimpleName();

    private WearingLocationFragment mWearingLocationFragment = null;

    public static final String INTENT_LOCATION = "LOCATION";
    public static final String INTENT_BTADDRESS = "ADDRESS";
    private String mDeviceAddress = "";

    private WearingLocation wearAt= WearingLocation.WRIST;
    private BTManager btManager = null;

    private int mRetryCount = 0;
    private boolean isDeviceChanged = false;


    private WearingLocationFragment.WearingLocationListener wearingLocationListener = new WearingLocationFragment.WearingLocationListener() {
        @Override
        public void onWearingLocationSelected(WearingLocation wearingLocation) {

            mRetryCount = 0;
            wearAt = wearingLocation;
            startProgressDialog(getString(R.string.myprofile_change_location), getString(R.string.myprofile_change_location_connection_content));
            btManager.findFitmeter(mDeviceAddress);
        }

        @Override
        public void onWearingLocationBack(){
            finish();
        }
    };

    private FitmeterEventCallback fitmeterEventCallback = new FitmeterEventCallback() {
        @Override
        public void onProgressValueChnaged(int progress, int max) { }

        @Override
        public void onReceiveAllFileComplete(List<ReceivedDataInfo> receiveDataArray, boolean isComplete) { }

        @Override
        public void receiveCompleted(int index, int offset, byte[] receivedbytes) { }

        @Override
        public void statusOfBTManagerChanged(ConnectionStatus status) {

            switch ( status ){
                case NOTFOUND:
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            stopProgressDialog();
                            Toast.makeText(getApplicationContext(), getString(R.string.myprofile_change_location_connect_request), Toast.LENGTH_SHORT).show();
                        }
                    });
                    break;
                case CONNECTED:
                    changeDeviceLocation();
                    break;
                case DISCONNECTED:
                    if( isDeviceChanged ) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                connectFitmeter();
                            }
                        });
                    }
                    break;
                case ERROR_DISCONNECTED_133:
                    if( isDeviceChanged )
                    {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                connectFitmeter();
                            }
                        });
                    }
                    break;
                case ERROR_DISCONNECTED:
                    if(isDeviceChanged)
                    {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                connectFitmeter();
                            }
                        });
                    }
                    break;
            }
        }

        @Override
        public void findNewDeviceResult( final  List<BLEDevice> discoveredPeripherals)
        {
        }

        @Override
        public void findDeviceResult(final String deviceAddress, String serialNumber) {

            mDeviceAddress = deviceAddress;
            isDeviceChanged = true;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    connectFitmeter();
                }
            });
        }

        @Override
        public void findRecoveryDeviceResult( BluetoothDevice recoveryDevice , List<BluetoothDevice> recoveryFitmeters ) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_change);

        Intent intent = getIntent();
        WearingLocation wearingLocation = null;
        if(intent != null)
        {
            int loc = intent.getIntExtra(INTENT_LOCATION, 0);
            mDeviceAddress = intent.getStringExtra(INTENT_BTADDRESS);
            wearingLocation =  WearingLocation.getWearingLocation(loc);
        }

        this.mWearingLocationFragment = WearingLocationFragment.newInstance(wearingLocation);
        this.mWearingLocationFragment.setWearingLocationSelectionListener(wearingLocationListener);

        this.btManager = BTManager.getInstance(this, fitmeterEventCallback);
        this.getSupportFragmentManager().beginTransaction().replace( R.id.fl_activity_location_change , mWearingLocationFragment).commit();
    }

    private void connectFitmeter()
    {
        if( mRetryCount < 5 )
        {
            mRetryCount++;
            Log.d(TAG, "기기 검색 중 기기 연결 " + mRetryCount + "번째");
            btManager.connectFitmeter( mDeviceAddress );

        }else{
            stopProgressDialog();

            showToast(getString(R.string.myprofeil_change_location_fail));
            Log.d(TAG, "착용 위치 변경 실패");
        }
    }

    private void changeDeviceLocation(){

        new Thread(new Runnable()
        {
            @Override
            public void run() {
                changeProgressMessage(getString(R.string.myprofile_changing_location));

                boolean isSuccess = false;

                //LED 켜기
                btManager.turnonled( BTManager.LED_COLOR_BLUE );

                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                //LED 끄기
                btManager.turnoffled();

                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                Log.i(TAG, "착용 위치 " + wearAt.Value);
                boolean result = btManager.setWeringLocation(wearAt.Value);

                stopProgressDialog();

                if (result) {
                    Log.d(TAG, "착용 위치 변경 성공");
                    isSuccess = true;
                    Intent intent = new Intent();
                    intent.putExtra(INTENT_LOCATION, wearAt.Value);
                    setResult( Activity.RESULT_OK, intent );
                    showToast(getString(R.string.myprofile_change_location_success));
                    finish();
                    btManager.clear();

                } else {
                    showToast(getString(R.string.myprofeil_change_location_fail));
                    Log.d(TAG, "착용 위치 변경 실패");
                }

            }
        }).start();
    }

    private void showToast( final String message )
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), message , Toast.LENGTH_SHORT).show();
            }
        });
    }

    FitmeterProgressDialog mDialog;
    public void startProgressDialog( final String title , final String message ){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                mDialog = new FitmeterProgressDialog( LocationChangeActivity.this );
                mDialog.setMessage(message);
                mDialog.setCancelable(false);
                mDialog.show();

            }
        });
    }

    public void changeProgressMessage( final String message ){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mDialog.setMessage(message);
            }
        });
    }

    public void stopProgressDialog()
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if( mDialog != null ) {
                    mDialog.dismiss();
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if( btManager != null )
        {
            btManager.clear();
            btManager = null;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
