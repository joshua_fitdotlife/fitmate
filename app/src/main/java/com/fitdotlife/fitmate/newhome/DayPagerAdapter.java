package com.fitdotlife.fitmate.newhome;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import com.fitdotlife.fitmate_lib.customview.NewHomeDayCircleView;
import com.fitdotlife.fitmate_lib.customview.NewHomeDayHalfCircleView;

/**
 * Created by Joshua on 2016-01-14.
 */
public class DayPagerAdapter extends PagerAdapter{

    private View[] mDayGraphs = null;
    private Context mContext = null;
    private CategoryType[] mViewOrder = null;

    public DayPagerAdapter( Context context , View[] dayGraphs  ){
        this.mContext = context;
        this.mDayGraphs = dayGraphs;
    }

    public DayPagerAdapter( Context context , View[] dayGraphs , CategoryType[] viewOrder ){
        this.mContext = context;
        this.mDayGraphs = dayGraphs;
        this.mViewOrder = viewOrder;
    }

    public View[] getDayGraphs(){
        return this.mDayGraphs;
    }

    @Override
    public int getCount() {
        return mDayGraphs.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position)
    {
        final View view = mDayGraphs[ position ];
        container.addView(view);

        if( mViewOrder != null ) {
            if (mViewOrder[position] == CategoryType.DAILY_ACHIEVE) {
                view.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        NewHomeDayHalfCircleView halfCircleView = (NewHomeDayHalfCircleView) view;
                        halfCircleView.changeMargin();
                    }
                } , 15);
            } else if (mViewOrder[position] == CategoryType.WEEKLY_ACHIEVE) {
                view.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        NewHomeDayCircleView circleView = (NewHomeDayCircleView) view;
                        circleView.changeMargin();
                    }
                } , 15 );
            }
        }

        return  view;
    }
}
