package com.fitdotlife.fitmate.newhome;

/**
 * Created by Joshua on 2016-01-19.
 */
public interface NewHomeWeekView {
    void updateView( int[] childIndex , double[] value );
    void updateView( int childIndex , double value );
    void removeSelectDay();
    void setSelectDay( int childIndex );
}
