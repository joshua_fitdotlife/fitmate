package com.fitdotlife.fitmate.newhome;

/**
 * Created by Joshua on 2016-01-08.
 */
public interface NewHomeProgramClickListener {
    void programClick( int exerprogramID );
}
