package com.fitdotlife.fitmate.newhome;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseAnalyzer;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.StrengthInputType;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.UserInfoForAnalyzer;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.WearingLocation;
import com.fitdotlife.fitmate.ActivityClassActivity;
import com.fitdotlife.fitmate.ActivityClassActivity_;
import com.fitdotlife.fitmate.ActivityManager;
import com.fitdotlife.fitmate.ExerciseProgramHomeActivity;
import com.fitdotlife.fitmate.R;
import org.apache.log4j.Log;
import org.json.JSONException;

import com.fitdotlife.fitmate_lib.customview.NewHomeCategoryView;
import com.fitdotlife.fitmate_lib.customview.NewHomeDayCircleView;
import com.fitdotlife.fitmate_lib.customview.NewHomeDayHalfCircleView;
import com.fitdotlife.fitmate_lib.customview.NewHomeDayTextView;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.http.HttpApi;
import com.fitdotlife.fitmate_lib.http.HttpResponse;
import com.fitdotlife.fitmate_lib.object.ExerciseProgram;
import com.fitdotlife.fitmate_lib.object.NewsProgramChange;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.util.Utils;

import java.lang.reflect.Type;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by Joshua on 2016-01-14.
 */
@SuppressWarnings("ResourceType")
public class NewHomeUtils
{
    private final String TAG = NewHomeUtils.class.getSimpleName();

    private Context mContext = null;
    private FitmateDBManager mDBManger = null;
    public PopupWindow programPopup = null;
    public PopupWindow categoryIntroPopup = null;
    private Activity mActivity = null;

    private double fromStrength = 0;
    private double toStrength = 0;

    public NewHomeUtils(Activity activity, FitmateDBManager dbManager )
    {
        this.mContext = activity;
        this.mActivity = activity;
        this.mDBManger = dbManager;
    }

    private ExerciseAnalyzer.ActivityRank getDailyAchieveRank( NewHomeCategoryData categoryData ){

        int dayAchieveValue = categoryData.getDayAchieve();

        float strengthFrom = categoryData.getStrengthFrom();
        float strengthTo = categoryData.getStrengthTo();

        int dayAchievePercent = (int) (( dayAchieveValue / strengthFrom ) * 100);
        if( dayAchievePercent > 100 ){
            dayAchievePercent = 100;
        }

        ExerciseAnalyzer.ActivityRank activityRank = null;
        if( dayAchievePercent < 50 ){
            activityRank = ExerciseAnalyzer.ActivityRank.NOTGOOD;
        }else if( dayAchievePercent < 80 ){
            activityRank = ExerciseAnalyzer.ActivityRank.NORMAL;
        }else if( dayAchievePercent < 100 ){
            activityRank = ExerciseAnalyzer.ActivityRank.GOOD;
        }else if( dayAchievePercent >= 100 ){
            activityRank = ExerciseAnalyzer.ActivityRank.VERYGOOD;
        }

        return activityRank;

    }

    public static String getComment(Context context , ExerciseAnalyzer.ActivityRank activityRank )
    {
        String comment = null;

        if( activityRank.equals(ExerciseAnalyzer.ActivityRank.VERYGOOD ) ){
            comment = context.getResources().getString( R.string.newhome_comment_verygood );
        }else if( activityRank.equals(ExerciseAnalyzer.ActivityRank.GOOD ) ){
            comment = context.getResources().getString( R.string.newhome_comment_good );
        }else if( activityRank.equals(ExerciseAnalyzer.ActivityRank.NORMAL ) ){
            comment = context.getResources().getString( R.string.newhome_comment_normal );
        }else if( activityRank.equals(ExerciseAnalyzer.ActivityRank.NOTGOOD ) ){
            comment = context.getResources().getString( R.string.newhome_comment_notgood );
        }
        return comment;

    }

    public static String getWeekStartDate( Calendar weekDayCalendar ){
        Calendar weekCalendar = Calendar.getInstance();
        weekCalendar.setTimeInMillis(weekDayCalendar.getTimeInMillis());
        int weekNumber = weekCalendar.get(Calendar.DAY_OF_WEEK);
        weekCalendar.add(Calendar.DATE, -(weekNumber - 1) );
        return getDateString(weekCalendar);
    }

    public static String getDateString( Calendar calendar )
    {
        return calendar.get(Calendar.YEAR) + "-" + String.format("%02d",  calendar.get( Calendar.MONTH ) + 1 ) + "-" + String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH) );
    }

    private void setCategoryViewData( CategoryType categotyType, NewHomeCategoryView categoryView, String valueText, String valueUnitText )
    {
        categoryView.setValueText(valueText);
        categoryView.setValueUnitText(valueUnitText);
        categoryView.setCategoryIntroListener( new CategoryIntroListener() );
        categoryView.setCategoryText( CategoryType.getCategoryChar( mContext , categotyType ) );
        categoryView.setCategoryType( categotyType );
    }

    public void setDayTextViewData( CategoryType categotyType , NewHomeDayTextView dayTextView , float value , String valueUnitText , String commentText , int pointNumber , boolean startZero )
    {

        dayTextView.setValueUnitText(valueUnitText);
        dayTextView.setCommnetText(commentText);
        dayTextView.setCategoryText( CategoryType.getDayViewChar( mContext , categotyType ) );
        dayTextView.setValue(value);
        dayTextView.setmPointNumber(pointNumber);
        dayTextView.setCategoryIntroListener( new CategoryIntroListener() , categotyType.ordinal() );
        dayTextView.display();
        //dayTextView.startAnimation(1000 , startZero);

    }

    public void setCategorySelect( NewHomeCategoryView categoryView , int index , int selectdCategoryIndex )
    {
        if( index == selectdCategoryIndex )
        {
            categoryView.select();
        }else{
            categoryView.unSelect();
        }
    }

    public void selectCategoryView( NewHomeCategoryView newHomeCategoryView )
    {
        //int[] attrs = new int[]{R.attr.newhomeCategorySelect , R.attr.newhomeCategoryTextSelect , R.attr.newhomeCategoryValueTextSelect };
        //TypedArray a = mContext.getTheme().obtainStyledAttributes(attrs);

        //Drawable selectDrawable = a.getDrawable(0);
        //int selectedCategoryColor = a.getColor( 1, 0 );
        //int selectedValueColor = a.getColor(2, 0);
        //a.recycle();

//        Drawable selectDrawable = mContext.getResources().getDrawable( R.drawable.category_day_sel );
//        int selectedCategoryColor = Color.BLACK;
//        int selectedValueColor = Color.BLACK;
//
//        newHomeCategoryView.setImage( selectDrawable );
//        newHomeCategoryView.setCategoryTextColor( selectedCategoryColor );
//        newHomeCategoryView.setValueTextColor( selectedValueColor ) ;

        newHomeCategoryView.unSelect();
    }

    public void unSelectCategoryView( NewHomeCategoryView newHomeCategoryView )
    {

//        Drawable unSelectDrawable = mContext.getResources().getDrawable( R.drawable.category_day_nor );
//        int unSelectedCategoryColor = mContext.getResources().getColor( R.color.day_category_normal_text );
//        int unSelectedValueColor = mContext.getResources().getColor( R.color.day_category_normal_valuetext );
//
//        newHomeCategoryView.setImage( unSelectDrawable );
//        newHomeCategoryView.setCategoryTextColor( unSelectedCategoryColor );
//        newHomeCategoryView.setValueTextColor( unSelectedValueColor );

        newHomeCategoryView.select();
    }

    public void setDayValue( NewHomeCategoryView categoryView , View dayView , NewHomeCategoryData categoryData , final int exerprogramID , CategoryType categoryType , boolean startZero)
    {
        String valueText = null;
        String valueUnitText = null;
        String categoryText = null;
        int pointNumber = 0;
        String commentText = NewHomeUtils.getComment(mContext, getDailyAchieveRank(categoryData));

        switch( categoryType )
        {
            case EXERCISE_TIME:
                int exerciseTime = categoryData.getExerciseTime();

                valueText = String.format("%d", exerciseTime);
                valueUnitText = mContext.getResources().getString(R.string.common_minute);

                setDayTextViewData(categoryType, (NewHomeDayTextView) dayView, exerciseTime, valueUnitText, commentText , pointNumber , startZero);
                break;
            case CALORIES:
                int calorie = categoryData.getCalories();
                valueText = String.format("%d", calorie);
                valueUnitText = mContext.getResources().getString(R.string.newhome_category_calorie_unit);

                setDayTextViewData(categoryType, (NewHomeDayTextView) dayView, calorie , valueUnitText, commentText , pointNumber , startZero );
                break;
            case DAILY_ACHIEVE:
                int dayAchieveValue = categoryData.getDayAchieve();

                float strengthFrom = categoryData.getStrengthFrom();
                float strengthTo = categoryData.getStrengthTo();
                String strengthTypeUnitText = categoryData.getStrengthUnitText();

                int dayAchievePercent = (int) (( dayAchieveValue / strengthFrom ) * 100);
                if( dayAchievePercent > 100 ){
                    dayAchievePercent = 100;
                }
                valueUnitText = "%";
                valueText = dayAchievePercent + "";

                final NewHomeDayHalfCircleView newHomeDayHalfCircleView = (NewHomeDayHalfCircleView) dayView;
                newHomeDayHalfCircleView.setCategoryText(  CategoryType.getCategoryChar( mContext , categoryType ) );
                newHomeDayHalfCircleView.setRange(strengthFrom, strengthTo);
                newHomeDayHalfCircleView.setAchieveValueText(valueUnitText);
                newHomeDayHalfCircleView.setStrengthTypeUnitText(strengthTypeUnitText);
                newHomeDayHalfCircleView.setValue(dayAchieveValue);
                newHomeDayHalfCircleView.setCategoryIntroListener( new CategoryIntroListener() , categoryType.ordinal() );
                //애니메이션일 때
                //newHomeDayHalfCircleView.startAnimation( 1000 , startZero );
                newHomeDayHalfCircleView.display();
//                newHomeDayHalfCircleView.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        newHomeDayHalfCircleView.changeMargin();
//                    }
//                }, 10);
                break;
            case WEEKLY_ACHIEVE:
                int weekScore = categoryData.getWeekScore();

                valueText = String.format("%d", weekScore);
                valueUnitText = mContext.getResources().getString(R.string.common_point);

                final NewHomeDayCircleView weekScoreDayCircleView = (NewHomeDayCircleView) dayView;
                weekScoreDayCircleView.setValueText( valueText );
                weekScoreDayCircleView.setValueUnitText(valueUnitText);
                weekScoreDayCircleView.setValue(weekScore);
                weekScoreDayCircleView.setCategoryText(CategoryType.getCategoryChar(mContext, categoryType));
                weekScoreDayCircleView.setCategoryIntroListener(new CategoryIntroListener(), categoryType.ordinal());
                weekScoreDayCircleView.display();
//                weekScoreDayCircleView.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        weekScoreDayCircleView.changeMargin();
//                    }
//                }, 10);
                //애니메이션일 때
                //weekScoreDayCircleView.startAnimation( 1000 , startZero );
                break;
            case FAT:
                double fat = categoryData.getFat();
                pointNumber = 2;

                valueText = Utils.get3DigitString(fat);
                valueUnitText = categoryData.getFatUnitText();

                float fatValue = 0;
                try {
                    fatValue = Utils.parseFloat( valueText );
                } catch (ParseException e) {
                    Log.e( "fitmate" , e.getMessage() );
                }

                setDayTextViewData(categoryType, (NewHomeDayTextView) dayView, fatValue , valueUnitText, commentText , pointNumber ,startZero);
                break;
            case CARBOHYDRATE:
                double carbon = categoryData.getCarbohydrate();
                valueUnitText = categoryData.getCarbohydrateUnitText();
                pointNumber = 2;

                valueText = Utils.get3DigitString( carbon );

                float carboValue = 0;
                try {
                    carboValue = Utils.parseFloat(valueText);
                } catch (ParseException e) {
                    Log.e( "fitmate" , e.getMessage() );
                }

                setDayTextViewData(categoryType, (NewHomeDayTextView) dayView, carboValue , valueUnitText, commentText , pointNumber , startZero );
                break;
            case DISTANCE:
                double distance = categoryData.getDistance();
                pointNumber = 2;

                valueText = Utils.get3DigitString(distance);
                valueUnitText = categoryData.getDistanceUnitText();

                float distanceValue = 0;
                try {
                    distanceValue = Utils.parseFloat(valueText);
                } catch (ParseException e) {
                    Log.e( "fitmate" , e.getMessage() );
                }

                setDayTextViewData(categoryType, (NewHomeDayTextView) dayView, distanceValue , valueUnitText, commentText , pointNumber ,startZero );
                break;
        }

        setCategoryViewData(categoryType, categoryView, valueText, valueUnitText);
    }



    public void showProgramDialog(  ExerciseProgram program , boolean availableChange , ExerciseAnalyzer analyzer )
    {
        String programName = null;
        String programInfo = null;

        if (program.getDefaultProgram()) {
            try {
                int nameResID = mContext.getResources().getIdentifier(program.getName(), "string", mContext.getPackageName());
                programName = mContext.getResources().getString(nameResID);
                int infoResID = mContext.getResources().getIdentifier(program.getInfo(), "string", mContext.getPackageName());
                programInfo = mContext.getResources().getString(infoResID);
            } catch (Exception e) {
            }

        } else {
            programName = program.getName();
            programInfo = program.getInfo();
        }

        displayProgramView( program , programName , programInfo , availableChange , analyzer );
    }

    public void showCategoryIntroDialog( int categoryIndex )
    {

        //팝업 윈드우를 이용한다.
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View programView = inflater.inflate(R.layout.dialog_category_intro, null);

        TextView tvCategoryTitle = (TextView) programView.findViewById(R.id.tv_dialog_category_intro_title);
        TextView tvCategoryIntro = (TextView) programView.findViewById( R.id.tv_dialog_category_intro_content );

        tvCategoryTitle.setText( CategoryType.getDayViewChar( mContext , CategoryType.values()[ categoryIndex ] ) );
        tvCategoryIntro.setText( CategoryType.getCategoryDescription( mContext , CategoryType.values()[ categoryIndex ] ) );

        categoryIntroPopup = new PopupWindow( programView , WindowManager.LayoutParams.MATCH_PARENT , WindowManager.LayoutParams.MATCH_PARENT);
        WindowManager windowManager = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);

        categoryIntroPopup.setOutsideTouchable(true);
        categoryIntroPopup.setTouchable(true);
        categoryIntroPopup.setBackgroundDrawable(new BitmapDrawable()) ;
        categoryIntroPopup.setTouchInterceptor(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                categoryIntroPopup.dismiss();
                return true;
            }
        });
        categoryIntroPopup.setAnimationStyle(-1);
        categoryIntroPopup.showAtLocation(programView, Gravity.CENTER, 0, 0);
    }

    public View getDefaultNewsView(){
        ImageView imgNews = new ImageView( mContext );
        LinearLayout layoutNews = new LinearLayout(mContext);
        layoutNews.setLayoutParams( new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT , ViewGroup.LayoutParams.MATCH_PARENT));

        LinearLayout.LayoutParams layoutParams =  new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        int leftMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP , 4.67f , mContext.getResources().getDisplayMetrics());
        int rightMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP , 4.67f , mContext.getResources().getDisplayMetrics());
        layoutParams.setMargins(leftMargin, 0, rightMargin, 0);
        imgNews.setLayoutParams(layoutParams);
        imgNews.setBackground(mContext.getResources().getDrawable(R.drawable.default_news));

        layoutNews.addView( imgNews );
        return layoutNews;
    }

    private class CategoryIntroListener implements View.OnClickListener{

        @Override
        public void onClick(View view) {
            showCategoryIntroDialog((Integer) view.getTag());
        }
    }

    private void displayProgramView( ExerciseProgram program , String programName , String programInfo , boolean availableChange , ExerciseAnalyzer analyzer )
    {
        final Dialog programDialog = new Dialog(mContext);
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View programView = inflater.inflate(R.layout.dialog_program, null);

        TextView tvProgramTitle = (TextView) programView.findViewById(R.id.tv_dialog_program_name);
        TextView tvProgramDescription = (TextView) programView.findViewById( R.id.tv_dialog_program_description );
        LinearLayout llProgramTime = (LinearLayout) programView.findViewById( R.id.ll_dialog_program_exercise_time);
        TextView tvProgramTime = (TextView) programView.findViewById( R.id.tv_dialog_program_exercise_time );
        TextView tvProgramTimes = (TextView) programView.findViewById( R.id.tv_dialog_program_times );
        ImageView imgProgramStrength = (ImageView) programView.findViewById( R.id.img_dialog_program_strength );
        TextView tvProgramStrengthTitle = (TextView) programView.findViewById( R.id.tv_dialog_program_strength_title );
        TextView tvProgramStrength = (TextView) programView.findViewById( R.id.tv_dialog_program_strength );
        ImageButton btnActivityClass = (ImageButton) programView.findViewById(R.id.btn_dialog_program_activityclass);


        btnActivityClass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, ActivityClassActivity_.class);
                intent.putExtra( ActivityClassActivity.TYPE_KEY , ActivityClassActivity.MET );
                intent.putExtra( ExerciseProgram.STRENGTH_FROM_KEY , fromStrength);
                intent.putExtra(ExerciseProgram.STRENGTH_TO_KEY, toStrength);
                mContext.startActivity(intent);
                programDialog.dismiss();

            }
        });

        final Button btnChangeProgram = (Button) programView.findViewById( R.id.btn_dialog_program_changeprogram );
        btnChangeProgram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent( mActivity , ExerciseProgramHomeActivity.class );
                mActivity.startActivity(intent);


                ActivityManager.getInstance().finishAllActivity();
            }
        });

        if( availableChange )
        {
            btnChangeProgram.setVisibility( View.VISIBLE );

        }else{
            btnChangeProgram.setVisibility(View.GONE);
        }

        tvProgramTitle.setText( programName );
        tvProgramDescription.setText( programInfo );

        StrengthInputType strengthType = StrengthInputType.getStrengthType(program.getCategory());
        String strengthUnit = Utils.getStrengthUnit(strengthType);

        if( strengthType.equals( StrengthInputType.CALORIE ) || strengthType.equals( StrengthInputType.CALORIE_SUM ) || strengthType.equals( StrengthInputType.VS_BMR ) ){

            llProgramTime.setVisibility(View.GONE);
            imgProgramStrength.setImageResource(R.drawable.icon_calories);
            tvProgramStrengthTitle.setText(mContext.getResources().getString(R.string.applied_program_week_calorie));

        }else{

            int timeFrom = program.getMinuteFrom();
            int timeTo = program.getMinuteTo();

            String timeText = null;
            String timeUnit = mContext.getResources().getString(R.string.common_minute);
            if( timeFrom == timeTo ){
                timeText = timeFrom  + timeUnit;
            }else{
                timeText = timeFrom + timeUnit + "-" + timeTo + timeUnit;
            }

            tvProgramTime.setText( timeText );
        }

        int strengthFrom= (int) program.getStrengthFrom();
        int strengthTo= (int) program.getStrengthTo();

        String strengthRageText = null;
        if( strengthFrom == strengthTo ){
            if(strengthType.equals( StrengthInputType.STRENGTH ) )
            {
                String[] arrStrengthType = mContext.getResources().getStringArray( R.array.exercise_strength_type_char );
                strengthRageText = arrStrengthType[ strengthFrom ] + Utils.getStrengthUnit(strengthType);
            }else {
                strengthRageText = strengthFrom + Utils.getStrengthUnit(strengthType);
            }
        }
        else{
            if(strengthType.equals( StrengthInputType.STRENGTH ) ){
                String[] arrStrengthType = mContext.getResources().getStringArray( R.array.exercise_strength_type_char );
                strengthRageText = arrStrengthType[strengthFrom] + "-" + arrStrengthType[strengthTo] + " " + strengthUnit;

            }else {
                strengthRageText = strengthFrom + "-"+ strengthTo + " " +strengthUnit;
            }
        }

        if( !strengthType.equals( StrengthInputType.CALORIE ) && !strengthType.equals( StrengthInputType.CALORIE_SUM ) && !strengthType.equals( StrengthInputType.VS_BMR ) )
        {
            fromStrength = analyzer.getMetFrom();
            toStrength = analyzer.getMetTo();
            fromStrength = Double.parseDouble( String.format( "%.1f" , fromStrength ) );
            toStrength = Double.parseDouble(String.format("%.1f", toStrength));

            if( toStrength >= 99 )
            {
                strengthRageText += " (" + fromStrength + "MET~)";
            }else{
                strengthRageText += " (" + fromStrength + "-" + toStrength + "MET)";
            }
            btnActivityClass.setVisibility(View.VISIBLE);

        }else{
            btnActivityClass.setVisibility(View.GONE);
        }

        tvProgramStrength.setText(strengthRageText);

        int timesFrom = program.getTimesFrom();
        int timesTo = program.getTimesTo();
        String timesText = null;
        String timesUnit = mContext.getResources().getString(R.string.common_day).toLowerCase();
        if( timesFrom == timesTo ){
            timesText = timesFrom + timesUnit;
        }else{
            timesText = timesFrom + timesUnit + "-" + timesTo + timesUnit;
        }
        tvProgramTimes.setText(timesText);

        programDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        programDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        programDialog.setContentView(programView);
        programDialog.show();

        programView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                programDialog.dismiss();
                return true;
            }
        });
    }

}
