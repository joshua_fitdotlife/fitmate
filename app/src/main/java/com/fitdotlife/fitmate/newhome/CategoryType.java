package com.fitdotlife.fitmate.newhome;

import android.content.Context;

import com.fitdotlife.fitmate.R;

/**
 * Created by Joshua on 2016-01-13.
 */
public enum CategoryType {
    DAILY_ACHIEVE, WEEKLY_ACHIEVE , EXERCISE_TIME , CALORIES , FAT , CARBOHYDRATE , DISTANCE;

    public static int getDefaultValue( CategoryType categoryType )
    {
        int defaultValue = 0;
        switch( categoryType )
        {
            case DAILY_ACHIEVE:
                defaultValue = 0;
                break;
            case WEEKLY_ACHIEVE:
                defaultValue = 0;
                break;
            case EXERCISE_TIME:
                defaultValue = 100;
                break;
            case CALORIES:
                defaultValue = 500;
                break;
            case FAT:
                defaultValue = 10;
                break;
            case CARBOHYDRATE:
                defaultValue = 60;
                break;
            case DISTANCE:
                defaultValue = 10;
                break;
        }

        return defaultValue;
    }

    public static int getPointNumber(  CategoryType categoryType )
    {
        int defaultValue = 0;
        switch( categoryType )
        {
            case DAILY_ACHIEVE:
                defaultValue = 0;
                break;
            case WEEKLY_ACHIEVE:
                defaultValue = 0;
                break;
            case EXERCISE_TIME:
                defaultValue = 0;
                break;
            case CALORIES:
                defaultValue = 0;
                break;
            case FAT:
                defaultValue = 2;
                break;
            case CARBOHYDRATE:
                defaultValue = 2;
                break;
            case DISTANCE:
                defaultValue = 2;
                break;
        }

        return defaultValue;
    }

    public static String getCategoryChar( Context context , CategoryType categoryType )
    {
        String categoryChar = null;

        switch( categoryType )
        {
            case DAILY_ACHIEVE:
                categoryChar = context.getResources().getString( R.string.newhome_category_char_day_achieve );
                break;
            case WEEKLY_ACHIEVE:
                categoryChar = context.getResources().getString( R.string.newhome_category_char_week_point );
                break;
            case EXERCISE_TIME:
                categoryChar = context.getResources().getString( R.string.newhome_category_char_exercisetime );
                break;
            case CALORIES:
                categoryChar = context.getResources().getString( R.string.newhome_category_char_calorie );
                break;
            case FAT:
                categoryChar = context.getResources().getString( R.string.newhome_category_char_fat );
                break;
            case CARBOHYDRATE:
                categoryChar = context.getResources().getString( R.string.newhome_category_char_carbohydrate );
                break;
            case DISTANCE:
                categoryChar = context.getResources().getString( R.string.newhome_category_char_distance );
                break;
        }

        return categoryChar;
    }

    public static String getDayViewChar( Context context , CategoryType categoryType ){
        String dayViewChar = null;

        switch( categoryType )
        {
            case DAILY_ACHIEVE:
                dayViewChar = context.getResources().getString( R.string.newhome_dayview_char_day_achieve );
                break;
            case WEEKLY_ACHIEVE:
                dayViewChar = context.getResources().getString( R.string.newhome_dayview_char_week_point );
                break;
            case EXERCISE_TIME:
                dayViewChar = context.getResources().getString( R.string.newhome_dayview_char_exercisetime );
                break;
            case CALORIES:
                dayViewChar = context.getResources().getString( R.string.newhome_dayview_char_calorie );
                break;
            case FAT:
                dayViewChar = context.getResources().getString( R.string.newhome_dayview_char_fat );
                break;
            case CARBOHYDRATE:
                dayViewChar = context.getResources().getString( R.string.newhome_dayview_char_carbohydrate );
                break;
            case DISTANCE:
                dayViewChar = context.getResources().getString( R.string.newhome_dayview_char_distance );
                break;
        }

        return dayViewChar;
    }

    public static String getCategoryDescription( Context context , CategoryType categoryType )
    {
        String categoryDescription = null;

        switch( categoryType )
        {
            case DAILY_ACHIEVE:
                categoryDescription = context.getResources().getString( R.string.newhome_category_description_day_achieve );
                break;
            case WEEKLY_ACHIEVE:
                categoryDescription = context.getResources().getString( R.string.newhome_category_description_week_point );
                break;
            case EXERCISE_TIME:
                categoryDescription = context.getResources().getString( R.string.newhome_category_description_exercisetime );
                break;
            case CALORIES:
                categoryDescription = context.getResources().getString( R.string.newhome_category_description_calorie );
                break;
            case FAT:
                categoryDescription = context.getResources().getString( R.string.newhome_category_description_fat );
                break;
            case CARBOHYDRATE:
                categoryDescription = context.getResources().getString( R.string.newhome_category_description_carbohydrate );
                break;
            case DISTANCE:
                categoryDescription = context.getResources().getString( R.string.newhome_category_description_distance );
                break;
        }

        return categoryDescription;
    }



}
