package com.fitdotlife.fitmate.newhome;

/**
 * Created by Joshua on 2016-03-10.
 */
public class CategoryEditInfo {

    private String categoryText;
    private String valueText;
    private String valueUnitText;
    private boolean isVisible;
    private CategoryType categoryType;
    private int viewIndex;

    public CategoryEditInfo(String categoryText, String valueText, String valueUnitText ) {
        this.categoryText = categoryText;
        this.valueText = valueText;
        this.valueUnitText = valueUnitText;
    }

    public String getCategoryText() {
        return categoryText;
    }

    public void setCategoryText(String categoryText) {
        this.categoryText = categoryText;
    }

    public String getValueText() {
        return valueText;
    }

    public void setValueText(String valueText) {
        this.valueText = valueText;
    }

    public String getValueUnitText() {
        return valueUnitText;
    }

    public void setValueUnitText(String valueUnitText) {
        this.valueUnitText = valueUnitText;
    }

    public CategoryType getCategoryType() {
        return categoryType;
    }

    public void setCategoryType( CategoryType categoryType) {
        this.categoryType = categoryType;
    }

    public int getViewIndex() {
        return viewIndex;
    }

    public void setViewIndex(int viewIndex) {
        this.viewIndex = viewIndex;
    }

    public boolean isVisible() {
        return isVisible;
    }

    public void setVisible(boolean isSelected) {
        this.isVisible = isSelected;
    }
}
