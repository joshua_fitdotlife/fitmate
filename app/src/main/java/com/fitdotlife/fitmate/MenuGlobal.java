package com.fitdotlife.fitmate;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fitdotlife.fitmate_lib.presenter.LoginPresenter;

public class MenuGlobal implements View.OnClickListener
{
	private String mTag = null;
	private Activity mActivity = null;
	
	//Activity , Group, Exercise , UserFriend , Notification
    private LinearLayout llMenuHome = null;
	private LinearLayout llMenuActivity = null;
	private LinearLayout llMenuExercise = null;
    private LinearLayout llMenuMyInfo = null;
    private LinearLayout llMenuHelp = null;
	private LinearLayout llMenuFriend = null;
	private LinearLayout llMenuNews = null;

    private View viewHomeMark = null;
	private View viewActivityMark = null;
	private View viewExerciseMark = null;
    private View viewMyinfoMark = null;
    private View viewHelpMark = null;
	private View viewFriendMark = null;
	private View viewNewsMark = null;

    private ImageView imgMenuHome = null;
    private TextView tvMenuHome = null;
	
	public MenuGlobal(Activity activity, ViewGroup parentView, String tag)
	{
		this.mTag = tag;
		this.mActivity = activity;
		
		View mGlobalMenuView = this.mActivity.getLayoutInflater().inflate( R.layout.menu_global , parentView , true );
        mGlobalMenuView.setBackgroundColor(Color.TRANSPARENT);

        this.llMenuHome = (LinearLayout) mGlobalMenuView.findViewById(R.id.ll_menu_home);
        this.llMenuHome.setOnClickListener(this);
        this.imgMenuHome = (ImageView) mGlobalMenuView.findViewById(R.id.img_menu_home);
        this.tvMenuHome = (TextView) mGlobalMenuView.findViewById(R.id.tv_menu_home);
        this.viewHomeMark = mGlobalMenuView.findViewById(R.id.menu_home_mark);

		this.llMenuActivity = (LinearLayout) mGlobalMenuView.findViewById(R.id.ll_menu_activity);
		this.llMenuActivity.setOnClickListener(this);
		this.viewActivityMark = mGlobalMenuView.findViewById(R.id.menu_activity_mark);

		this.llMenuExercise = (LinearLayout) mGlobalMenuView.findViewById(R.id.ll_menu_exercise);
		this.llMenuExercise.setOnClickListener(this);
		this.viewExerciseMark = mGlobalMenuView.findViewById(R.id.menu_exercise_mark);

        //this.llMenuMyInfo = (LinearLayout) mGlobalMenuView.findViewById(R.id.ll_menu_friend);
        //this.llMenuMyInfo.setOnClickListener(this);
        //this.viewMyinfoMark = mGlobalMenuView.findViewById(R.id.menu_myinfo_mark);

        this.llMenuFriend = (LinearLayout) mGlobalMenuView.findViewById(R.id.ll_menu_friend);
        this.llMenuFriend.setOnClickListener(this);
        this.viewFriendMark = mGlobalMenuView.findViewById(R.id.menu_friend_mark);

        //this.llMenuHelp = (LinearLayout) mGlobalMenuView.findViewById(R.id.ll_menu_news);
        //this.llMenuHelp.setOnClickListener(this);
        //this.viewHelpMark = mGlobalMenuView.findViewById(R.id.menu_help_mark);

        this.llMenuNews = (LinearLayout) mGlobalMenuView.findViewById(R.id.ll_menu_news);
        this.llMenuNews.setOnClickListener(this);
        this.viewNewsMark = mGlobalMenuView.findViewById(R.id.menu_news_mark);

//		this.llMenuFriend = (LinearLayout) mGlobalMenuView.findViewById(R.id.ll_menu_friend);
//		this.llMenuFriend.setOnClickListener(this);
//		this.viewFriendMark = mGlobalMenuView.findViewById(R.id.menu_friend_mark);
//
//		this.llMenuNotification = (LinearLayout) mGlobalMenuView.findViewById(R.id.ll_menu_notification);
//		this.llMenuNotification.setOnClickListener(this);
//		this.viewNotificationMark = mGlobalMenuView.findViewById(R.id.menu_notification_mark);


		this.changeMark();
	}
	
	private void changeMark()
	{
        if( mTag == mActivity.getResources().getString(R.string.gnb_home) ){
            this.viewHomeMark.setVisibility( View.VISIBLE );
            this.viewActivityMark.setVisibility(View.INVISIBLE);
            this.viewExerciseMark.setVisibility( View.INVISIBLE );
            this.viewFriendMark.setVisibility(View.INVISIBLE);
            this.viewNewsMark.setVisibility(View.INVISIBLE);
            //this.viewFriendMark.setVisibility(View.INVISIBLE );
            //this.viewNotificationMark.setVisibility(View.INVISIBLE );

        }else if( mTag == mActivity.getResources().getString(R.string.gnb_activity ) )
		{
            this.viewHomeMark.setVisibility( View.INVISIBLE );
            this.viewActivityMark.setVisibility(View.VISIBLE);
            this.viewExerciseMark.setVisibility( View.INVISIBLE );
            this.viewFriendMark.setVisibility(View.INVISIBLE);
            this.viewNewsMark.setVisibility(View.INVISIBLE);
			//this.viewFriendMark.setVisibility(View.INVISIBLE );
			//this.viewNotificationMark.setVisibility(View.INVISIBLE );
		}
		else if( mTag == mActivity.getResources().getString(R.string.gnb_exerciseprogram ) )
		{
            this.viewHomeMark.setVisibility( View.INVISIBLE );
            this.viewActivityMark.setVisibility(View.INVISIBLE);
            this.viewExerciseMark.setVisibility( View.VISIBLE );
            this.viewFriendMark.setVisibility(View.INVISIBLE);
            this.viewNewsMark.setVisibility(View.INVISIBLE);
			//this.viewFriendMark.setVisibility(View.INVISIBLE);
			//this.viewNotificationMark.setVisibility(View.INVISIBLE);
		}
        else if( mTag == mActivity.getResources().getString(R.string.gnb_friend ) )
        {
            this.viewHomeMark.setVisibility( View.INVISIBLE );
            this.viewActivityMark.setVisibility(View.INVISIBLE);
            this.viewExerciseMark.setVisibility( View.INVISIBLE );
            this.viewFriendMark.setVisibility(View.VISIBLE);
            this.viewNewsMark.setVisibility(View.INVISIBLE);
            //this.viewFriendMark.setVisibility(View.INVISIBLE);
            //this.viewNotificationMark.setVisibility(View.INVISIBLE);
        }
        else if( mTag == mActivity.getResources().getString( R.string.gnb_news ) )
        {
            this.viewHomeMark.setVisibility( View.INVISIBLE );
            this.viewActivityMark.setVisibility(View.INVISIBLE);
            this.viewExerciseMark.setVisibility( View.INVISIBLE );
            this.viewFriendMark.setVisibility(View.INVISIBLE);
            this.viewNewsMark.setVisibility(View.VISIBLE);
            //this.viewFriendMark.setVisibility(View.INVISIBLE);
            //this.viewNotificationMark.setVisibility(View.INVISIBLE);
        }
//		else if( mTag == mActivity.getResources().getString(R.string.menu_friend ) )
//		{
//			this.viewActivityMark.setVisibility(View.INVISIBLE);
//			this.viewExerciseMark.setVisibility( View.INVISIBLE );
//			this.viewFriendMark.setVisibility(View.VISIBLE);
//			this.viewNotificationMark.setVisibility(View.INVISIBLE);
//		}
//		else if( mTag == mActivity.getResources().getString(R.string.menu_notification ) )
//		{
//			this.viewActivityMark.setVisibility(View.INVISIBLE);
//			this.viewExerciseMark.setVisibility( View.INVISIBLE );
//			this.viewFriendMark.setVisibility(View.INVISIBLE);
//			this.viewNotificationMark.setVisibility(View.VISIBLE);
//		}
	}
	@Override
	public void onClick(View view)
	{
		if( view.getTag() == mTag ) return;

		switch( view.getId() )
		{
		case R.id.ll_menu_home:
            if(this.getSettedUserInfo()) {
                this.moveActivity(NewHomeActivity_.class);
            }else{
                this.moveActivity(NewHomeAsActivity_.class);
            }

            if( !this.getHomeNotView() ){
                this.showHelpActivity( HelpActivity.HOME_HELP_MODE );
            }
			break;
		case R.id.ll_menu_activity:
            if(this.getSettedUserInfo()) {
                this.moveActivity(MainActivity.class);
            }else {
                this.moveActivity(MainAsActivity_.class);
            }
            this.changeMark();
            if( !this.getActivityNotView() ){
                this.showHelpActivity( HelpActivity.ACTIVITY_HELP_MODE );
            }
			break;
		case R.id.ll_menu_exercise:
            this.moveActivity( ExerciseProgramHomeActivity.class );
			this.changeMark();
            if( !this.getExerciseNotView() ){
                this.showHelpActivity( HelpActivity.EXERCISE_HELP_MODE );
            }
			break;
		case R.id.ll_menu_friend:
            this.moveActivity( FriendActivity_.class );
			this.changeMark();
			break;
        case R.id.ll_menu_news:
            this.moveActivity( NewsActivity_.class );
            this.changeMark();
            break;
		}

        ActivityManager.getInstance().finishAllActivity();
	}

	private void moveActivity(Class<?> cls)
	{
        Intent intent = new Intent( this.mActivity , cls );
        this.mActivity.getWindow().setWindowAnimations(0);
		this.mActivity.startActivity(intent);
	}

    private boolean getHomeNotView(){
        return getNotView( HelpActivity.HOME_NOT_VIEW_KEY );
    }

    private boolean getActivityNotView(){
        return getNotView( HelpActivity.ACTIVITY_NOT_VIEW_KEY );
    }

    private boolean getExerciseNotView(){
        return getNotView( HelpActivity.EXERCISE_NOT_VIEW_KEY );
    }


    private boolean getNotView(String key){
        SharedPreferences pref = mActivity.getSharedPreferences( "fitmateservice" , Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        return pref.getBoolean(key, false);
    }

    private void showHelpActivity( int mode ){
        Intent intent = new Intent( this.mActivity , HelpActivity.class );
        intent.putExtra( HelpActivity.MODE_KEY, mode );
        intent.putExtra( HelpActivity.VISIBLE_KEY, true );
        this.mActivity.startActivity(intent);
    }

    private boolean getSettedUserInfo(){
        SharedPreferences pref =  this.mActivity.getSharedPreferences("fitmateservice", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        return pref.getBoolean( LoginPresenter.SET_USERINFO_KEY , false);
    }
}
