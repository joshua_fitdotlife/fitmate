package com.fitdotlife.fitmate;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.PermissionChecker;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fitdotlife.fitmate.log.Log;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.http.HttpApi;
import com.fitdotlife.fitmate_lib.http.HttpResponse;
import com.fitdotlife.fitmate_lib.http.NetworkClass;
import com.fitdotlife.fitmate_lib.iview.IMyProfileView;
import com.fitdotlife.fitmate_lib.key.CommonKey;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.presenter.MyProfilePresenter;
import com.fitdotlife.fitmate_lib.util.ImageUtil;
import com.fitdotlife.fitmate_lib.util.Utils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Joshua on 2016-10-25.
 */
@EActivity(R.layout.activity_friend_intro)
public class FriendIntroActivity  extends Activity implements IMyProfileView {

    private String TAG = FriendIntroActivity.class.getSimpleName();

    private final int PERMISSION_REQUEST_STORAGE = 3;
    private final int CAMERA_REQUEST = 4;

    private UserInfo mUserInfo = null;
    private MyProfilePresenter mPresenter = null;
    private FitmateDBManager mDBManager = null;

    @ViewById(R.id.ic_activity_friend_intro_actionbar)
    View actionBarView;

    @ViewById(R.id.iv_activity_friend_intro_profile)
    ImageView ivProfile;

    @ViewById(R.id.etx_activity_friend_intro_name)
    EditText etxName;

    @ViewById(R.id.etx_activity_friend_intro_introduction)
    EditText etxIntroduction;

    @ViewById(R.id.btn_activity_friend_intro)
    Button btnFriendIntro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.mPresenter = new MyProfilePresenter(this , this);
        this.mDBManager = new FitmateDBManager(this);
        this.mUserInfo = mDBManager.getUserInfo();
    }

    @AfterViews
    void init()
    {
        displayActionBar();

        String imageName = mUserInfo.getAccountPhoto();
        if( imageName != null )
        {
            String imgUri = null;
            if (imageName.startsWith("file://")) {
                imgUri = imageName;
            } else {
                imgUri = NetworkClass.imageBaseURL + imageName;
            }

            DisplayImageOptions options = new DisplayImageOptions.Builder()
                    .showImageForEmptyUri(R.drawable.profile_img1)
                    .showImageOnFail(R.drawable.profile_img1)
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .displayer(new RoundedBitmapDisplayer(300))
                    .build();

            ImageLoader.getInstance().displayImage(imgUri, ivProfile, options);
            ivProfile.invalidate();
        }

        String name = mUserInfo.getName();
        if( name != null && !TextUtils.isEmpty( name ))
        {
            etxName.setText( name );
        }

        etxName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                btnFriendIntro.setVisibility(View.VISIBLE);
            }

            @Override
            public void afterTextChanged(Editable editable) {}
        });

        String introcution = mUserInfo.getIntro();
        if( introcution != null && !TextUtils.isEmpty( introcution ) ){
            etxIntroduction.setText( introcution );
        }

        etxIntroduction.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                btnFriendIntro.setVisibility(View.VISIBLE);
            }

            @Override
            public void afterTextChanged(Editable editable) {}
        });
    }

    private void displayActionBar()
    {
        actionBarView.setBackgroundColor(Color.WHITE);
        TextView tvBarTitle = (TextView) actionBarView.findViewById( R.id.tv_custom_actionbar_white_title );
        tvBarTitle.setText(this.getString(R.string.friend_intro_title));

        ImageView imgLeft = (ImageView) actionBarView.findViewById(R.id.img_custom_actionbar_white_left);
        imgLeft.setBackgroundResource(R.drawable.icon_back_red_selector);
        imgLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Click(R.id.iv_activity_friend_intro_profile)
    void profileClick(){

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ){

            if( PermissionChecker.checkSelfPermission( this, Manifest.permission_group.STORAGE) == PackageManager.PERMISSION_GRANTED ){

                showCameraActivity();

            }else{

                ActivityCompat.requestPermissions( this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST_STORAGE);

            }

        }else{

            showCameraActivity();

        }
    }

    @Click(R.id.btn_activity_friend_intro)
    void saveClick(){

        if( !Utils.isOnline(this) ){
            Toast.makeText( this , this.getString(R.string.common_connect_network) , Toast.LENGTH_SHORT ).show();
            return;
        }

        mUserInfo.setName( etxName.getText().toString() );
        mUserInfo.setIntro( etxIntroduction.getText().toString() );

        mPresenter.modifyUserInfo(mUserInfo);

        HttpApi.chageUserInfo(mUserInfo, new HttpResponse()
        {
            @Override
            public void onResponse(int resultCode, String response)
            {

                Log.e(TAG , response );

                if (resultCode == 0) {
                    //Toast.makeText( getApplicationContext(), getString(R.string.myprofile_dont_send_photo), Toast.LENGTH_SHORT ).show();
                } else if (resultCode == 200) {
                    try {

                        JSONObject jsonObject = new JSONObject(response.toString());
                        String result = jsonObject.getString(CommonKey.RESPONSE_VALUE_KEY);

                        if (result.equals(CommonKey.SUCCESS)) {
                            String url = jsonObject.getString("url");
                            if (url != null && url != "") {
                                UserInfo.savePhotoUrlInSharedPreference(getApplicationContext(), url);
                                mUserInfo.setAccountPhoto(url);

                                finish();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void showCameraActivity(){
        Intent i = new Intent( this, ImageUtil.class);
        startActivityForResult(i, CAMERA_REQUEST );
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK)
        {

            Uri uri = (Uri) data.getExtras().get(ImageUtil.PICTURE_URI);
            DisplayImageOptions options = new DisplayImageOptions.Builder()
                    .showImageForEmptyUri(R.drawable.profile_img1)
                    .showImageOnFail(R.drawable.profile_img1)
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .displayer(new RoundedBitmapDisplayer(300))
                    .build();

            ImageLoader.getInstance().displayImage(uri.toString(), ivProfile, options);

            mUserInfo.setAccountPhoto(uri.toString());
            btnFriendIntro.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if( requestCode == PERMISSION_REQUEST_STORAGE )
        {
            if( grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED ){
                showCameraActivity();
            }
        }
    }

    @Override
    public void showDeviceChangeActivity() {

    }

    @Override
    public void showWearingLocationActivity() {

    }

    @Override
    public void setShowing(boolean showing) {

    }

    @Override
    public void dismissProgress() {

    }

    @Override
    public void setProgressText(String progressText) {

    }
}



