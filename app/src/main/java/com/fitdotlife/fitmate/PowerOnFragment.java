package com.fitdotlife.fitmate;

import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fitdotlife.fitmate_lib.key.FitmeterType;

/**
 * Created by Joshua on 2016-08-03.
 */
public class PowerOnFragment extends Fragment{

    private FitmeterType mDeviceType = null;
    private TextView tvDescription = null;
    private ImageView imgPowerOn = null;
    private TextView tvSecond = null;

    private AnimationDrawable frameAnimation;
    public static PowerOnFragment newInstance( FitmeterType deviceType )
    {
        Bundle args = new Bundle();
        PowerOnFragment fragment = new PowerOnFragment();
        args.putInt( SelectDeviceFragment.DEVICETYPE_KEY , deviceType.ordinal() );
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getArguments() != null){
            mDeviceType = FitmeterType.values()[ getArguments().getInt( SelectDeviceFragment.DEVICETYPE_KEY ) ];
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View rootView = inflater.inflate( R.layout.fragment_poweron, container , false );
        this.tvDescription = (TextView) rootView.findViewById(R.id.tv_fragment_poweron_description);
        this.imgPowerOn = (ImageView) rootView.findViewById(R.id.img_fragment_poweron);
        this.tvSecond = (TextView) rootView.findViewById(R.id.tv_fragment_poweron_second);

        RelativeLayout.LayoutParams secondParams = (RelativeLayout.LayoutParams) tvSecond.getLayoutParams();
        int secondTopMargin = 0;
        int secondRightMargin = 0;

        if(mDeviceType.equals( FitmeterType.BAND )){
            tvDescription.setText(this.getResources().getString(R.string.signin_poweron_description_band));
            imgPowerOn.setBackgroundResource(R.drawable.poweron_band);
            secondTopMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP , 36.3f , getResources().getDisplayMetrics());
            secondRightMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP , 102 , getResources().getDisplayMetrics());

        }else if(mDeviceType.equals( FitmeterType.BLE )){
            tvDescription.setText( this.getResources().getString( R.string.signin_poweron_description_ble) );
            imgPowerOn.setBackgroundResource(R.drawable.poweron_ble);
            secondTopMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP , 25.9f , getResources().getDisplayMetrics());
            secondRightMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP , 48.3f , getResources().getDisplayMetrics());
        }

        secondParams.setMargins(0, secondTopMargin, secondRightMargin, 0);
        tvSecond.setLayoutParams( secondParams );

        frameAnimation = (AnimationDrawable) imgPowerOn.getBackground();
        frameAnimation.start();

        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        frameAnimation.stop();
    }

    public void setDeviceType( FitmeterType deviceType)
    {
        mDeviceType = deviceType;
    }
}
