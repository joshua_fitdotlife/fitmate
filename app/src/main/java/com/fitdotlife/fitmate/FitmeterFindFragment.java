package com.fitdotlife.fitmate;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.fitdotlife.fitmate.btmanager.BTManager;
import com.fitdotlife.fitmate.btmanager.ConnectionStatus;
import com.fitdotlife.fitmate.btmanager.FitmeterEventCallback;
import com.fitdotlife.fitmate.btmanager.ReceivedDataInfo;
import com.fitdotlife.fitmate_lib.customview.FitmeterDialog;
import com.fitdotlife.fitmate_lib.customview.FitmeterProgressDialog;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.key.FitmeterType;
import com.fitdotlife.fitmate_lib.object.BLEDevice;
import com.fitdotlife.fitmate_lib.object.UserInfo;

import org.apache.log4j.Log;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Joshua on 2016-08-04.
 */
public class FitmeterFindFragment extends Fragment
{
    public interface FitmeterFoundListener
    {
        void onDeviceFound(String deviceAddress , String serialNumber );
        void moveSelectDeviceType();
        void moveBack();
    }

    private final String TAG = FitmeterFindFragment.class.getSimpleName();

    private FitmeterType mDeviceType = null;
    private TextView tvScanTime = null;
    private int mTimerValue = 0;
    private BTManager btManager = null;
    private UserInfo mUserInfo = null;

    private int mRetryCount = 0;
    private boolean isDeviceChanged = false;

    private FitmeterProgressDialog mDialog = null;
    private BluetoothDevice mBluetoothDevice = null;

    private boolean isScaned = false;

    private FitmeterFoundListener mListener = null;

    private List<BLEDevice> mDiscoveredPeripherals = null;

    private Timer connectTimer = null;

    private FitmeterEventCallback fitmeterEventCallback = new FitmeterEventCallback()
    {
        @Override
        public void onProgressValueChnaged(int progress, int max) {}

        @Override
        public void onReceiveAllFileComplete(List<ReceivedDataInfo> receiveDataArray, boolean isComplete) {}

        @Override
        public void receiveCompleted(int index, int offset, byte[] receivedbytes) {}

        @Override
        public void statusOfBTManagerChanged(ConnectionStatus status)
        {
            switch (status){
                case CONNECTED:
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            stopProgressDialog();

                            btManager.turnonled( BTManager.LED_COLOR_RED );
                            startConnectTimer();
                            checkLEDDialog();

                        }
                    });
                    break;
                case DISCONNECTED:
                    if( isDeviceChanged ) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                stopConnectTimer();
                                connectFitmeter();
                            }
                        });
                    }else{
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                stopConnectTimer();
                                stopProgressDialog();
                            }
                        });
                    }
                    break;
                case ERROR_DISCONNECTED:
                    if( isDeviceChanged ) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                stopConnectTimer();
                                connectFitmeter();
                            }
                        });
                    }else{
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                stopConnectTimer();
                                stopProgressDialog();
                            }
                        });
                    }
                    break;
                case ERROR_DISCONNECTED_133:
                    if( isDeviceChanged ) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                stopConnectTimer();
                                connectFitmeter();
                            }
                        });
                    }else{
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                stopConnectTimer();
                                stopProgressDialog();
                            }
                        });
                    }
                    break;
            }
        }

        @Override
        public void findNewDeviceResult(final List<BLEDevice> discoveredPeripherals)
        {
            getActivity().runOnUiThread(new Runnable()
            {
                @Override
                public void run() {
                    android.util.Log.d("fitmate", "findNewDeviceResult() - Peripheral Number : " + discoveredPeripherals.size());
                    checkFoundDevice( discoveredPeripherals );
                }
            });
        }

        @Override
        public void findDeviceResult(String deviceAddress, String serialNumber) {

        }

        @Override
        public void findRecoveryDeviceResult(BluetoothDevice recoveryDevice, List<BluetoothDevice> recoveryFitmeters) {

        }
    };

    private Handler mTimerHander = new Handler()
    {
        @Override
        public void handleMessage(Message msg) {

            tvScanTime.setText(5 - mTimerValue + "");

            if (mTimerValue < 5) {

                mTimerHander.sendEmptyMessageDelayed(0, 1000);
            }else if (mTimerValue == 5) {

            }

            mTimerValue++;
        }
    };

    public static FitmeterFindFragment newInstance( FitmeterType deviceType )
    {
        Bundle args = new Bundle();

        FitmeterFindFragment fragment = new FitmeterFindFragment();
        args.putInt( SelectDeviceFragment.DEVICETYPE_KEY , deviceType.ordinal() );
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.fragment_fitmeter_find , container , false);
        if(getArguments() != null){
            mDeviceType = FitmeterType.values()[ getArguments().getInt( SelectDeviceFragment.DEVICETYPE_KEY ) ];
        }
        this.tvScanTime = (TextView) rootView.findViewById(R.id.tv_fragment_fitmeter_find_scan_time);
        this.mUserInfo = new FitmateDBManager(getActivity()).getUserInfo();
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        this.btManager = BTManager.getInstance(getActivity(), fitmeterEventCallback);
        startScan();
    }

    private void startScan()
    {
        this.isScaned = true;
        this.mTimerValue = 0;
        this.mTimerHander.sendEmptyMessage(0);
        btManager.findFitmeters(mDeviceType);
    }

//    @Override
//    public void onStop() {
//        super.onStop();
//
//        if(isScaned){
//            if(btManager != null) {
//                btManager.stopScan();
//            }
//        }
//
//        if( btManager != null )
//        {
//            btManager.close();
//            btManager.clear();
//            btManager = null;
//        }
//
//        ((DeviceRegisterFragment) getParentFragment()).moveBack();
//    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if(isScaned){
            if(btManager != null) {
                btManager.stopScan();
            }
        }

        if( btManager != null )
        {
            btManager.close();
            btManager.clear();
            btManager = null;
        }
    }

    private void connectFitmeter()
    {
        if(mRetryCount < 5){

            mRetryCount++;
            Log.d(TAG, "기기 검색 중 기기 연결 " + mRetryCount + "번째");
            btManager.connectFitmeter( mDiscoveredPeripherals.get(0).getBluetoothDevice().getAddress() );

        }else{

            stopProgressDialog();

            final FitmeterDialog deleteDialog = new FitmeterDialog( getContext() );
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View contentView = inflater.inflate(R.layout.dialog_default_content, null);
            TextView tvTitle = (TextView) contentView.findViewById(R.id.tv_dialog_default_title);
            tvTitle.setVisibility(View.GONE);

            TextView tvContent = (TextView) contentView.findViewById(R.id.tv_dialog_default_content);
            tvContent.setText(getString(R.string.signin_device_disconnect));
            deleteDialog.setContentView(contentView);

            View buttonView = inflater.inflate(R.layout.dialog_button_two , null);
            Button btnLeft = (Button) buttonView.findViewById(R.id.btn_dialog_button_two_left);
            btnLeft.setText( getString(R.string.common_no));
            btnLeft.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    stopConnectTimer();

                    if (btManager != null)
                    {
                        btManager.turnoffled();
                        btManager.close();
                        btManager.clear();
                        btManager = null;
                    }


                    mListener.moveSelectDeviceType();
                    deleteDialog.close();
                }
            });

            Button btnRight = (Button) buttonView.findViewById(R.id.btn_dialog_button_two_right);
            btnRight.setText( getString(R.string.common_yes));
            btnRight.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    startConnectionDevice();
                    deleteDialog.close();
                }
            });

            deleteDialog.setButtonView(buttonView);
            deleteDialog.show();

//            Toast.makeText(getContext(), getContext().getString(R.string.signin_device_disconnect), Toast.LENGTH_SHORT).show();
//            ((DeviceRegisterFragment) getParentFragment()).moveBack();
        }
    }

    private void checkFoundDevice( final List<BLEDevice> discoveredPeripherals )
    {
        if( discoveredPeripherals.size() == 0 )
        {

            this.showFindDeviceDialog(this.getString(R.string.signin_device_find_result_no) , discoveredPeripherals);

//            getActivity().runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    ((DeviceRegisterFragment) getParentFragment()).moveSelectDeviceType();
//                    Toast.makeText( getContext() , getString(R.string.signin_device_find_result_no) , Toast.LENGTH_SHORT ).show();
//                }
//            });

        }else if( discoveredPeripherals.size() == 1 ){
            //mListener.onDeviceFound( discoveredPeripherals.get(0).getBluetoothDevice().getAddress() );
            if( getActivity() != null  ) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mDiscoveredPeripherals = discoveredPeripherals;
                        startConnectionDevice();
                    }
                });
            }


        }else{
            this.showFindDeviceDialog(this.getString(R.string.signin_device_find_result_one_more) , discoveredPeripherals);
        }
    }

    private void startConnectionDevice()
    {

        startProgressDialog("", getString(R.string.signin_device_connecting));
        mRetryCount = 0;
        isDeviceChanged = true;
        btManager.connectFitmeter( mDiscoveredPeripherals.get(0).getBluetoothDevice().getAddress() );
    }

    public void startProgressDialog(final String title , final String message )
    {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                Log.e(TAG , "startProgress()");

                mDialog = new FitmeterProgressDialog( getActivity() );
                mDialog.setMessage(message);
                mDialog.setCancelable(false);
                mDialog.show();

            }
        });
    }

    public void stopProgressDialog(){
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
            }
        });
    }

    private void showFindDeviceDialog(final String message , final List<BLEDevice> discoveredPeripherals)
    {
        getActivity().runOnUiThread(
                new Runnable() {
                    @Override
                    public void run() {


                        final FitmeterDialog fDialog = new FitmeterDialog(getContext());
                        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE );

                        View contentView = inflater.inflate(R.layout.dialog_default_content, null);
                        TextView tvTitle = (TextView) contentView.findViewById(R.id.tv_dialog_default_title);
                        tvTitle.setText( getString(R.string.signin_device_find_result) );

                        TextView tvContent = (TextView) contentView.findViewById(R.id.tv_dialog_default_content);
                        tvContent.setText(message);
                        fDialog.setContentView(contentView);

                        View buttonView = inflater.inflate(R.layout.dialog_button_one , null);
                        Button btnOne = (Button) buttonView.findViewById(R.id.btn_dialog_button_one);
                        btnOne.setText(getString(R.string.common_yes));
                        btnOne.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if (discoveredPeripherals.size() == 0) {
                                    mListener.moveSelectDeviceType();
                                } else if (discoveredPeripherals.size() > 1) {
                                    mListener.moveBack();
                                }

                                if (btManager != null) {
                                    btManager.clear();
                                    btManager = null;
                                }

                                fDialog.close();

                            }
                        });

                        fDialog.setButtonView(buttonView);
                        fDialog.setCancelable(false);
                        fDialog.show();
                    }
                }
        );
    }

    private void checkLEDDialog(){
        getActivity().runOnUiThread(
                new Runnable() {
                    @Override
                    public void run() {

                        final FitmeterDialog deleteDialog = new FitmeterDialog( getContext() );
                        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                        View contentView = inflater.inflate(R.layout.dialog_default_content, null);
                        TextView tvTitle = (TextView) contentView.findViewById(R.id.tv_dialog_default_title);
                        tvTitle.setText(getString(R.string.signin_device_find_result));

                        TextView tvContent = (TextView) contentView.findViewById(R.id.tv_dialog_default_content);
                        tvContent.setText(getString(R.string.signin_device_ledon));
                        deleteDialog.setContentView(contentView);

                        View buttonView = inflater.inflate(R.layout.dialog_button_two , null);
                        Button btnLeft = (Button) buttonView.findViewById(R.id.btn_dialog_button_two_left);
                        btnLeft.setText( getString(R.string.common_no));
                        btnLeft.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                stopConnectTimer();

                                if (btManager != null) {
                                    btManager.turnoffled();
                                    btManager.close();
                                    btManager.clear();
                                    btManager = null;
                                }

                                deleteDialog.close();
                                mListener.moveBack();
                            }
                        });

                        Button btnRight = (Button) buttonView.findViewById(R.id.btn_dialog_button_two_right);
                        btnRight.setText(getString(R.string.common_yes));
                        btnRight.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                stopConnectTimer();

                                if (btManager != null) {
                                    btManager.turnoffled();
                                    btManager.close();
                                    btManager.clear();
                                    btManager = null;
                                }
                                deleteDialog.close();
                                mListener.onDeviceFound( mDiscoveredPeripherals.get(0).getBluetoothDevice().getAddress(), mDiscoveredPeripherals.get(0).getSerialNumber() );
                            }
                        });

                        deleteDialog.setButtonView(buttonView);
                        deleteDialog.setCancelable(false);
                        deleteDialog.show();
                    }
                }
        );
    }

    public void setDeviceFoundListener( FitmeterFoundListener listener ){
        mListener = listener;
    }

    private void startConnectTimer() {
        this.connectTimer = new Timer();
        this.connectTimer.schedule(new ConnectTimerTask() , 5000 , 5000);
    }

    private void stopConnectTimer() {
        if (connectTimer != null)
        {
            this.connectTimer.cancel();
            this.connectTimer = null;
        }
    }

    private class ConnectTimerTask extends TimerTask
    {
        @Override
        public void run()
        {
            if( btManager != null )
            {
                android.util.Log.e( "FitmeterFindFragment" , "turnonled()" );
                btManager.turnonled( BTManager.LED_COLOR_RED );
            }
        }
    }
}
