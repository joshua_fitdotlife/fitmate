package com.fitdotlife.fitmate;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.WearingLocation;

/**
 * Created by Joshua on 2016-07-27.
 */
public class WearingLocationFragment extends Fragment{

    private final static String WEARINGLOCATION_KEY = "wearinglocation";

    private View mActionBarView = null;
    private TextView tvTitle = null;
    private ImageView imgBack = null;

    private ImageView imgWearingLocation  = null;

    private TextView tvWaist = null;
    private TextView tvUpperArm = null;
    private TextView tvWrist = null;
    private TextView tvPantsPocket = null;
    private TextView tvAnkle = null;
    private Button btnFinish = null;

    private WearingLocation mWearingLocation = null;
    private WearingLocationListener mListener = null;

    public interface WearingLocationListener
    {
        void onWearingLocationSelected( WearingLocation wearingLocation);
        void onWearingLocationBack();
    }

    public static WearingLocationFragment newInstance( WearingLocation wearingLocation )
    {
        Bundle args = new Bundle();
        WearingLocationFragment fragment = new WearingLocationFragment();
        if(wearingLocation != null) {
            args.putInt(WEARINGLOCATION_KEY , wearingLocation.ordinal());
        }
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getArguments() != null){
            int wearingLocationIndex = getArguments().getInt( WEARINGLOCATION_KEY , -1 );
            if( wearingLocationIndex > -1 ) {
                mWearingLocation = WearingLocation.values()[wearingLocationIndex];
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View rootView = inflater.inflate( R.layout.fragment_wearinglocation , container ,false );

        mActionBarView = rootView.findViewById(R.id.ic_fragment_deviceregister_actionbar);
        this.imgBack = (ImageView) rootView.findViewById(R.id.img_custom_actionbar_white_left);
        this.imgBack.setBackgroundResource(R.drawable.icon_back_red_selector);
        this.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onWearingLocationBack();
            }
        });

        this.btnFinish = (Button) rootView.findViewById(R.id.btn_fragment_wearinglocation_finish);
        this.btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onWearingLocationSelected(mWearingLocation);
            }
        });

        this.tvTitle = (TextView) rootView.findViewById(R.id.tv_custom_actionbar_white_title);
        this.tvTitle.setText( getResources().getString( R.string.signin_select_wearinglocation ) );

        this.imgWearingLocation = (ImageView) rootView.findViewById(R.id.img_fragment_wearinglocation);
        if(mWearingLocation != null)
        {
            setWear();
        }else {
            this.imgWearingLocation.setImageResource(R.drawable.wear0);
        }

        this.tvWaist = (TextView) rootView.findViewById(R.id.tv_fragment_wearinglocation_waist);
        this.tvWaist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mWearingLocation = WearingLocation.WAIST;
                imgWearingLocation.setImageResource( R.drawable.wear5 );
                btnFinish.setVisibility(View.VISIBLE);
            }
        });
        this.tvUpperArm = (TextView) rootView.findViewById(R.id.tv_fragment_wearinglocation_upperarm);
        this.tvUpperArm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mWearingLocation = WearingLocation.UPPERARM;
                imgWearingLocation.setImageResource( R.drawable.wear2 );
                btnFinish.setVisibility(View.VISIBLE);
            }
        });
        this.tvWrist  = (TextView) rootView.findViewById(R.id.tv_fragment_wearinglocation_wrist);
        this.tvWrist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mWearingLocation = WearingLocation.WRIST;
                imgWearingLocation.setImageResource( R.drawable.wear1 );
                btnFinish.setVisibility(View.VISIBLE);
            }
        });
        this.tvPantsPocket = (TextView) rootView.findViewById(R.id.tv_fragment_wearinglocation_pantspocket);
        this.tvPantsPocket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mWearingLocation = WearingLocation.PANTSPOCKET;
                imgWearingLocation.setImageResource( R.drawable.wear4 );
                btnFinish.setVisibility(View.VISIBLE);
            }
        });
        this.tvAnkle = (TextView) rootView.findViewById(R.id.tv_fragment_wearinglocation_ankle);
        this.tvAnkle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mWearingLocation = WearingLocation.ANKLE;
                imgWearingLocation.setImageResource( R.drawable.wear3 );
                btnFinish.setVisibility(View.VISIBLE);
            }
        });
        return rootView;
    }

    public void setWearingLocationSelectionListener( WearingLocationListener listener )
    {
        mListener = listener;
    }

    public void setWear()
    {
        switch( mWearingLocation )
        {
            case WAIST:
                imgWearingLocation.setImageResource( R.drawable.wear5 );
                btnFinish.setVisibility(View.VISIBLE);
                break;
            case UPPERARM:
                imgWearingLocation.setImageResource( R.drawable.wear2 );
                btnFinish.setVisibility(View.VISIBLE);
                break;
            case PANTSPOCKET:
                imgWearingLocation.setImageResource( R.drawable.wear4 );
                btnFinish.setVisibility(View.VISIBLE);
                break;
            case WRIST:
                imgWearingLocation.setImageResource( R.drawable.wear1 );
                btnFinish.setVisibility(View.VISIBLE);
                break;
            case ANKLE:
                imgWearingLocation.setImageResource( R.drawable.wear3 );
                btnFinish.setVisibility(View.VISIBLE);
                break;
        }
    }

}
