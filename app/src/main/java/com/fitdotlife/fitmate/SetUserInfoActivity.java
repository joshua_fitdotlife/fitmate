package com.fitdotlife.fitmate;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.PermissionChecker;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.WearingLocation;
import com.fitdotlife.fitmate.btmanager.BTManager;
import com.fitdotlife.fitmate.btmanager.ConnectionStatus;
import com.fitdotlife.fitmate.btmanager.FitmeterEventCallback;
import com.fitdotlife.fitmate.btmanager.ReceivedDataInfo;
import com.fitdotlife.fitmate_lib.customview.FitmeterDialog;
import com.fitdotlife.fitmate_lib.customview.FitmeterProgressDialog;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.database.FitmateServiceDBManager;
import com.fitdotlife.fitmate_lib.iview.ISetUserInfoView;
import com.fitdotlife.fitmate_lib.key.FitmeterType;
import com.fitdotlife.fitmate_lib.key.GenderType;
import com.fitdotlife.fitmate_lib.object.BLEDevice;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.presenter.SetUserInfoPresenter;
import com.fitdotlife.fitmate_lib.service.util.Utils;
import com.fitdotlife.fitmate_lib.util.BluetoothDeviceUtil;
import com.fitdotlife.fitmate_lib.util.DateUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class SetUserInfoActivity extends Activity implements OnClickListener , ISetUserInfoView, BluetoothAdapter.LeScanCallback, CompoundButton.OnCheckedChangeListener {
    private String TAG = "fitmate";

    private final int PERMISSION_REQUEST_LOCATION = 0;

    private static final int ENABLE_BT_REQUEST_ID = 1;
    private final float DEFAULT_HEIGHT = 170;
    private final float DEFAULT_WEIGHT = 70;

    private static final long SCANNING_TIMEOUT = 5 * 1000;
    public static final int SCAN_COUNT_VALUE = 0;
    public static final int RSSI_VALUE = -70;
    private final int RSSI_VALUE_UNDER = -35;
    private final String FITMETER_NAME = "FITMETERBLE";

    private int MAN = 1;
    private int WOMAN = 0;

    private LayoutInflater inflater = null;
    private LinearLayout llContainer = null;
    private LinearLayout llContainerMatch = null;

    //액션바
    private View actionBarView = null;
    private ImageView imgBack = null;
    private TextView tvTitle = null;

    //성별 정보
    private ImageView imgMan = null;
    private ImageView imgWoman = null;

    //체력관련 정보
    private EditText etxBirthDay = null;
    private TextView tvWeight = null;
    private TextView tvHeight = null;

    private int mStep = -1;
    private UserInfo mUserInfo = null;
    private View[] mChildViewList = null;

    private int mSelectedGender = -1;

    private SetUserInfoPresenter mPresenter = null;

    private Handler mHandler = new Handler();

    private InputMethodManager inputMethodManager = null;
    private boolean isDeviceSelected = false;

    private TextView tvPressButton = null;

    private FitmeterProgressDialog mProgressDialog = null;
    private TextView tvScanTime = null;
    private int mTimerValue = 0;

    private ImageView relativeLayout_wearing;

    private TextView selectedTextView_wrist;
    private TextView selectedTextView_waist;
    private TextView selectedTextView_upperarm;
    private TextView selectedTextView_panspocket;
    private TextView selectedTextView_ankle;

    private WearingLocation wearAt= WearingLocation.WAIST;

    private LinearLayout llMainContainer = null;

    private boolean isSelectWearingLocation = false;
    private boolean isScaned = false;

    private float mTempHeight = 0;
    private float mTempWeight = 0;

    private BTManager btManager;

    private BluetoothDevice mBluetoothDevice = null;
    private int retryCount = 0;

    private boolean isDeviceChanged = false;

    private Handler mTimerHander = new Handler()
    {
        @Override
        public void handleMessage(Message msg) {

            tvScanTime.setText(5 - mTimerValue + "");

            if (mTimerValue < 5) {

                mTimerHander.sendEmptyMessageDelayed(0, 1000);
            }else if (mTimerValue == 5) {

            }

            mTimerValue++;

        }
    };

    boolean isServiceAgree = false;

    private FitmeterEventCallback fitmeterEventCallback = new FitmeterEventCallback() {
        @Override
        public void onProgressValueChnaged(int progress, int max) {

        }

        @Override
        public void onReceiveAllFileComplete(List<ReceivedDataInfo> receiveDataArray, boolean isComplete) {

        }

        @Override
        public void receiveCompleted(int index, int offset, byte[] receivedbytes) {

        }

        @Override
        public void statusOfBTManagerChanged(ConnectionStatus status)
        {

            switch ( status ){
                case CONNECTED:

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            changeProgressDialogMessage( getString(R.string.signin_dialog_content) );

                            boolean result = resetFitmeter();
                            if( ! result )
                            {
                                btManager.close();

                                try {
                                    Thread.sleep( 1000 );
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }

                                connectFitmeter();

                            }else{
                                btManager.clear();

                                org.apache.log4j.Log.e(TAG, "기기 등록을 성공하였습니다.");
                                FitmateServiceDBManager serviceDBManager = new FitmateServiceDBManager(SetUserInfoActivity.this);
                                serviceDBManager.deleteAllFiles();

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mUserInfo.setWearAt(wearAt.Value);
                                        mPresenter.setUserInfo(mUserInfo);
                                    }
                                });
                            }
                        }
                    }).start();

                    break;

                case DISCONNECTED:

                    if(isDeviceChanged) {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                connectFitmeter();
                            }
                        }).start();
                    }
                    break;
            }
        }

        @Override
        public void findNewDeviceResult( final  List<BLEDevice> discoveredPeripherals) {

            runOnUiThread(new Runnable()
            {
                @Override
                public void run() {

                    Log.d("fitmate", "findNewDeviceResult() - Peripheral Number : " + discoveredPeripherals.size());
                    checkFoundDevice(discoveredPeripherals);
                }
            });
        }

        @Override
        public void findDeviceResult(String deviceAddress, String serialNumber) {

        }

        @Override
        public void findRecoveryDeviceResult(BluetoothDevice recoveryDevice, List<BluetoothDevice> recoveryFitmeters) {

        }


    };



    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_set_user_info);

        this.mPresenter = new SetUserInfoPresenter( this , this );
        FitmateDBManager mDBManager = new FitmateDBManager( this.getApplicationContext() );
        this.inputMethodManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);

        this.mUserInfo = mDBManager.getUserInfo();
        this.mUserInfo.setIsSI(true);

        //액션바 설정.
        this.actionBarView = this.findViewById(R.id.ic_activity_set_account_actionbar);
        this.tvTitle = (TextView) actionBarView.findViewById(R.id.tv_custom_actionbar_white_title);
        this.imgBack = (ImageView) actionBarView.findViewById(R.id.img_custom_actionbar_white_left);
        actionBarView.findViewById(R.id.img_custom_actionbar_white_right).setVisibility(View.GONE);

        this.llMainContainer = (LinearLayout) this.findViewById(R.id.ll_activity_set_user_info_container);
        this.inflater =  this.getLayoutInflater();
        this.llContainer = (LinearLayout) this.findViewById(R.id.scr_set_userinfo_container );
        this.llContainerMatch = (LinearLayout) this.findViewById(R.id.scr_set_userinfo_container_match );

        this.mChildViewList = new View[8];

        //사용자 정보
        this.mChildViewList[0] = this.inflater.inflate( R.layout.child_set_userinfo_step_1 , this.llContainer, false );
        this.imgMan = (ImageView) this.mChildViewList[0].findViewById(R.id.img_fragment_userinfo_man);
        this.imgMan.setOnClickListener(this);
        this.imgWoman = (ImageView) this.mChildViewList[0].findViewById(R.id.img_fragment_userinfo_woman);
        this.imgWoman.setOnClickListener(this);
        this.etxBirthDay = (EditText) this.mChildViewList[0].findViewById(R.id.etx_fragment_userinfo_birthday);
        this.tvHeight = (TextView) this.mChildViewList[0].findViewById(R.id.tv_fragment_userinfo_height);
        this.tvHeight.setOnClickListener(this);
        this.tvWeight = (TextView) this.mChildViewList[0].findViewById(R.id.tv_fragment_userinfo_weight);
        this.tvWeight.setOnClickListener(this);

        //기기 선택
        this.mChildViewList[1] = this.inflater.inflate( R.layout.child_device_change_step_1 , this.llContainerMatch, false );
        //this.tvPressButton = (TextView) this.mChildViewList[1].findViewById(R.id.tv_child_device_change_press_button);

        //휘트미터 구성
        this.mChildViewList[3] = this.inflater.inflate(R.layout.child_device_change_step_3 , this.llContainer, false);
        //전원켜기
        this.tvScanTime = (TextView) this.mChildViewList[3].findViewById(R.id.tv_child_device_change_scan_time);
        //전원상태 확인
        this.mChildViewList[4] = this.inflater.inflate(R.layout.child_device_change_step_4 , this.llContainerMatch, false);
        //휘트미터 앱 연결하기
        int changePosition = 4;
        //검색하기

        //착용위치

        final Context activityContext = this;

        this.relativeLayout_wearing =(ImageView) this.mChildViewList[changePosition].findViewById(R.id.relativelay_wearing);
        this.relativeLayout_wearing.setImageResource(R.drawable.wear0);

        selectedTextView_waist= (TextView) this.mChildViewList[changePosition].findViewById(R.id.wear_waist);
        selectedTextView_waist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isSelectWearingLocation = true;
                wearAt= WearingLocation.WAIST;

           //     recycleView(relativeLayout_wearing);
                relativeLayout_wearing.setImageResource(R.drawable.wear5);
              //  relativeLayout_wearing.setBackground(new BitmapDrawable(getResources(), BitmapFactory.decodeResource(getResources(), R.drawable.wear5)));
            }
        });
        selectedTextView_upperarm= (TextView) this.mChildViewList[changePosition].findViewById(R.id.wear_upperarm);
        selectedTextView_upperarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isSelectWearingLocation = true;
                wearAt= WearingLocation.UPPERARM;
                //  relativeLayout_wearing.setBackground(getResources().getDrawable(R.drawable.wear2));

            //    recycleView(relativeLayout_wearing);
                relativeLayout_wearing.setImageResource( R.drawable.wear2);
              //  relativeLayout_wearing.setBackground(new BitmapDrawable(getResources(), BitmapFactory.decodeResource(getResources(), R.drawable.wear2)));
            }
        });
        selectedTextView_panspocket= (TextView) this.mChildViewList[changePosition].findViewById(R.id.wear_pantspocket);
        selectedTextView_panspocket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isSelectWearingLocation = true;
                wearAt= WearingLocation.PANTSPOCKET;
                //relativeLayout_wearing.setBackground(getResources().getDrawable(R.drawable.wear4));
            //    recycleView(relativeLayout_wearing);
                relativeLayout_wearing.setImageResource(R.drawable.wear4);
                //relativeLayout_wearing.setBackground(new BitmapDrawable(getResources(), BitmapFactory.decodeResource(getResources(), R.drawable.wear4)));
            }
        });
        selectedTextView_wrist= (TextView) this.mChildViewList[changePosition].findViewById(R.id.wear_wrist);
        selectedTextView_wrist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isSelectWearingLocation = true;
                wearAt= WearingLocation.WRIST;
                //      relativeLayout_wearing.setBackground(getResources().getDrawable(R.drawable.wear1));
            //    recycleView(relativeLayout_wearing);
            //    relativeLayout_wearing.setBackground(new BitmapDrawable(getResources(), BitmapFactory.decodeResource(getResources(), R.drawable.wear1)));
                relativeLayout_wearing.setImageResource(R.drawable.wear1);
            }
        });
        selectedTextView_ankle= (TextView) this.mChildViewList[changePosition].findViewById(R.id.wear_ankle);
        selectedTextView_ankle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isSelectWearingLocation = true;
                wearAt = WearingLocation.ANKLE;
                //   recycleView(relativeLayout_wearing);
                //  relativeLayout_wearing.setBackground(new BitmapDrawable(getResources(), BitmapFactory.decodeResource(getResources(), R.drawable.wear3)));
                relativeLayout_wearing.setImageResource(R.drawable.wear3);
            }
        });



        //this.mStep = 0;
        this.moveFrontView();
        this.btManager = BTManager.getInstance( this , fitmeterEventCallback );

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

    }

    public static class TimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            // Do something with the time chosen by the user
        }
    }


    private void moveFrontView()
    {
        this.mStep += 1;
        drawChildView();
    }

    private void moveBackView()
    {
        if( mStep == 3 ){
            this.isScaned = false;
        }

        if( mStep == 4 ) {
            this.mStep -= 2;
        }else {
            this.mStep -= 1;
        }
        drawChildView();
    }

    private void drawChildView()
    {
        if( mStep == -1 ){
            this.finish();
            return;
        }

        switch (mStep)
        {
            case 2:
            case 4:
                this.llContainerMatch.addView(this.mChildViewList[mStep]);
                break;
            default:
                this.llContainer.addView(this.mChildViewList[mStep]);
                break;
        }

        this.llContainer.scrollTo(0, 0);
        this.llContainerMatch.scrollTo(0, 0);

        if( mStep == 0 ){

            this.imgBack.setVisibility(View.INVISIBLE);
            this.inputMethodManager.hideSoftInputFromWindow(this.imgMan.getWindowToken() , 0);
            this.tvTitle.setText(this.getResources().getString(R.string.signin_bodyinformation));
        }
        else{
            this.imgBack.setVisibility(View.VISIBLE);
        }

        if(mStep == 1){

        }

        if( this.mStep == 2 )
        {
            this.inputMethodManager.hideSoftInputFromWindow( this.tvPressButton.getWindowToken() , 0);
        }

        if(this.mStep == 3 ){

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M )
            {

                if( PermissionChecker.checkSelfPermission( SetUserInfoActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED ){
                    startScan();
                }else{
                    ActivityCompat.requestPermissions( SetUserInfoActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION }, PERMISSION_REQUEST_LOCATION);
                }

            }
            else
            {
                startScan();
            }


        }

        if( this.mStep == 4 ){

        }
    }

    @Override
    public void onClick(View view)
    {
        switch( view.getId() )
        {
            case R.id.tv_fragment_userinfo_height:
                showHeightDialog();
                break;
            case R.id.tv_fragment_userinfo_weight:
                showWeightDialog();
                break;
//            case R.id.btn_set_userinfo_next:
//                if( this.validCheckThenSave() )
//                {
//                    if( mStep == 4 )
//                    {
//                        mUserInfo.setWearAt(wearAt.Value);
//                        this.mPresenter.saveUserInfo( mUserInfo , foundedAddress , BluetoothDeviceUtil.getBluetoothDevice(foundedAddress)  );
//                        return;
//                    }
//
//                    this.moveFrontView();
//                }
//                break;
//            case R.id.rl_activity_set_userinfo_back:
//                this.moveBackView();
//                break;
//            case R.id.img_activity_set_userinfo_close:
//
//                if( mStep ==3 ) {
//                    this.isScaned = false;
//                }
//
//                this.finish();
//                break;
//            case R.id.img_activity_set_userinfo_back:
//                this.moveBackView();
//                break;
            case R.id.img_fragment_userinfo_man:
                this.imgMan.setImageResource( R.drawable.icon_male_sel );
                this.imgWoman.setImageResource( R.drawable.icon_female );
                this.mSelectedGender = MAN;
                break;
            case R.id.img_fragment_userinfo_woman:
                this.imgMan.setImageResource( R.drawable.icon_male);
                this.imgWoman.setImageResource( R.drawable.icon_female_sel );
                this.mSelectedGender = WOMAN;
                break;
            case R.id.tv_child_set_account_service_agreement:
                Intent serviceAgreementIntent = new Intent( SetUserInfoActivity.this , ServiceAgreementActivity.class );
                this.startActivity( serviceAgreementIntent );
                break;
        }
    }

    private void showHeightDialog(){
        boolean heightUnit = mUserInfo.isSI();

        final FitmeterDialog heightDialog = new FitmeterDialog(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View numberPickerView = inflater.inflate(R.layout.dialog_number_picker, null);
        TextView tvTitle = (TextView) numberPickerView.findViewById(R.id.tv_dialog_number_picker_title);
        tvTitle.setText(this.getString(R.string.common_height));
        final NumberPicker np = (NumberPicker) numberPickerView.findViewById(R.id.np_dialog_number_picker);
        np.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        final NumberPicker pp = (NumberPicker) numberPickerView.findViewById(R.id.np_dialog_point_picker);
        pp.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        final NumberPicker up = (NumberPicker) numberPickerView.findViewById( R.id.np_dialog_unit_picker );
        up.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        up.setDisplayedValues(new String[]{this.getString(R.string.common_centimeter), this.getString(R.string.common_ft)});

        float height = mUserInfo.getHeight();
        np.setMaxValue(300);
        np.setMinValue(1);
        pp.setMaxValue(9);
        pp.setMinValue(0);
        up.setMaxValue(1);
        up.setMinValue(0);

        if( height > 0 ) {

            String[] valueArray = this.getValueArray( height );
            np.setValue( Integer.parseInt( valueArray[0] ) );
            pp.setValue( Integer.parseInt( valueArray[1] ) );

            up.setTag( height );

        }else{

            if( mUserInfo.isSI() ) {
                np.setValue((int) DEFAULT_HEIGHT);
                up.setTag(DEFAULT_HEIGHT);
            }else{
                float dHeight = Utils.cm_to_ft( DEFAULT_HEIGHT );
                dHeight = Math.round( dHeight );
                np.setValue((int) dHeight);
                up.setTag( dHeight );
            }
        }

        if( heightUnit )
        {
            up.setValue(0);
        }
        else
        {
            up.setValue(1);
        }

        heightDialog.setContentView( numberPickerView );

        up.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                boolean heightSi = (newVal == 0);
                float tempHeight = (float) up.getTag();

                //몸무게를 변환한다.
                if (heightSi) {
                    //파운드에서 킬로그램으로 변환한다.
                    tempHeight = com.fitdotlife.fitmate_lib.service.util.Utils.ft_to_cm(tempHeight);
                } else {
                    //킬로그램에서 파운드로 변환한다.
                    tempHeight = com.fitdotlife.fitmate_lib.service.util.Utils.cm_to_ft(tempHeight);
                }

                String[] valueArray = getValueArray( tempHeight );
                np.setValue( Integer.parseInt( valueArray[0] ) );
                pp.setValue( Integer.parseInt( valueArray[1]) );

                up.setTag(tempHeight);
            }
        });

        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                up.setTag((float) (((np.getValue() * 10d) + pp.getValue()) / 10d));
            }
        });

        pp.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                up.setTag((float) (((np.getValue() * 10d) + pp.getValue()) / 10d));
            }
        });

        View buttonView = inflater.inflate( R.layout.dialog_button_two , null );
        Button btnLeft = (Button) buttonView.findViewById(R.id.btn_dialog_button_two_left);
        btnLeft.setText(this.getString(R.string.common_cancel));
        btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                heightDialog.close();
            }
        });
        Button btnRight = (Button) buttonView.findViewById(R.id.btn_dialog_button_two_right);
        btnRight.setText(this.getString(R.string.common_ok));
        btnRight.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean heightSi = (up.getValue() == 0);
                boolean isSi = mUserInfo.isSI();
                float tempWeight = mUserInfo.getWeight();
                String weightUnitString = "";

                if (heightSi != isSi) {
                    //몸무게를 변환한다.
                    if (tempWeight > 0) {
                        if (heightSi) {
                            //파운드에서 킬로그램으로 변환한다.
                            tempWeight = Utils.lb_to_kg(tempWeight);
                            weightUnitString = getString(R.string.common_kilogram);
                        } else {
                            //킬로그램에서 파운드로 변환한다.
                            tempWeight = Utils.kg_to_lb(tempWeight);
                            weightUnitString = getString(R.string.common_pound);
                        }

                        String[] weightValueArray = getValueArray(tempWeight);
                        tvWeight.setText(String.format("%s %s", weightValueArray[0] + "." + weightValueArray[1], weightUnitString));
                    }

                }

                mUserInfo.setHeight((Float) up.getTag());
                mUserInfo.setWeight(tempWeight);
                mUserInfo.setIsSI(heightSi);

                String[] heightValueArray = getValueArray(mUserInfo.getHeight());
                tvHeight.setText(String.format("%s %s", heightValueArray[0] + "." + heightValueArray[1], up.getDisplayedValues()[up.getValue()]));
            }
        });
        heightDialog.setButtonView( buttonView );
        heightDialog.show();
    }

    private void showWeightDialog(){
        boolean weightUnit = mUserInfo.isSI();

        final FitmeterDialog weightDialog = new FitmeterDialog(this);
        LayoutInflater weightInflater = this.getLayoutInflater();
        final View weightNumberPickerView = weightInflater.inflate(R.layout.dialog_number_picker, null);
        TextView tvWeightTitle = (TextView) weightNumberPickerView.findViewById(R.id.tv_dialog_number_picker_title);
        tvWeightTitle.setText(this.getString(R.string.common_weight));
        final NumberPicker weightNP = (NumberPicker) weightNumberPickerView.findViewById(R.id.np_dialog_number_picker);
        weightNP.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        final NumberPicker weightPP = (NumberPicker) weightNumberPickerView.findViewById(R.id.np_dialog_point_picker);
        weightPP.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        final NumberPicker weightUP = (NumberPicker) weightNumberPickerView.findViewById(R.id.np_dialog_unit_picker);
        weightUP.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        weightUP.setDisplayedValues(new String[]{this.getString(R.string.common_kilogram), this.getString(R.string.common_pound)});

        float weight = this.mUserInfo.getWeight();
        weightNP.setMaxValue(500);
        weightNP.setMinValue(1);
        weightPP.setMaxValue(9);
        weightPP.setMinValue(0);
        weightUP.setMaxValue(1);
        weightUP.setMinValue(0);

        if( weightUnit )
        {
            weightUP.setValue(0);
        }else{
            weightUP.setValue(1);
        }

        if( weight > 0 ) {

            String[] valueArray = this.getValueArray( weight );
            weightNP.setValue(Integer.parseInt(valueArray[0]));
            weightPP.setValue(Integer.parseInt(valueArray[1]));

            weightUP.setTag( weight );

        }else{
            if( mUserInfo.isSI() ) {
                weightNP.setValue((int) DEFAULT_WEIGHT);
                weightUP.setTag(DEFAULT_WEIGHT);
            }else{
                float dWeight = Utils.kg_to_lb( DEFAULT_WEIGHT );
                dWeight = Math.round( dWeight );
                weightNP.setValue((int) dWeight);
                weightUP.setTag( dWeight );
            }
        }

        weightUP.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                boolean weightSi = (newVal == 0);
                float tempWeight = (float) weightUP.getTag();

                //몸무게를 변환한다.
                if (weightSi) {
                    //파운드에서 킬로그램으로 변환한다.
                    tempWeight = com.fitdotlife.fitmate_lib.service.util.Utils.lb_to_kg(tempWeight);
                } else {
                    //킬로그램에서 파운드로 변환한다.
                    tempWeight = com.fitdotlife.fitmate_lib.service.util.Utils.kg_to_lb(tempWeight);
                }

                String[] valueArray = getValueArray( tempWeight );
                weightNP.setValue(Integer.parseInt(valueArray[0]));
                weightPP.setValue(Integer.parseInt(valueArray[1]));

                weightUP.setTag(tempWeight);
            }
        });

        weightNP.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                weightUP.setTag((float) (((weightNP.getValue() * 10d) + weightPP.getValue()) / 10d) );
            }
        });

        weightPP.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                weightUP.setTag((float) (((weightNP.getValue() * 10d) + weightPP.getValue()) / 10d));
            }
        });

        weightDialog.setContentView(weightNumberPickerView);

        View buttonView = inflater.inflate( R.layout.dialog_button_two , null );
        Button btnLeft = (Button) buttonView.findViewById(R.id.btn_dialog_button_two_left);
        btnLeft.setText(this.getString(R.string.common_cancel));
        btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                weightDialog.close();
            }
        });
        Button btnRight = (Button) buttonView.findViewById(R.id.btn_dialog_button_two_right);
        btnRight.setText(this.getString(R.string.common_ok));
        btnRight.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean weightSi = ( weightUP.getValue() ==0 );
                boolean isSi = mUserInfo.isSI();
                float tempHeight = mUserInfo.getHeight();
                String heightUnitString = "";

                if (weightSi != isSi ) {
                    //키를 변환한다.
                    if( tempHeight > 0 )
                    {
                        if (weightSi) {
                            tempHeight = Utils.ft_to_cm(tempHeight);
                            heightUnitString = getString(R.string.common_centimeter);
                        } else {
                            tempHeight = Utils.cm_to_ft(tempHeight);
                            heightUnitString = getString(R.string.common_ft);
                        }

                        String[] heightValueArray = getValueArray( tempHeight );
                        tvHeight.setText(String.format("%s %s", heightValueArray[0] + "." + heightValueArray[1] , heightUnitString ));
                    }

                }

                mUserInfo.setWeight((Float) weightUP.getTag());
                mUserInfo.setHeight(tempHeight);
                mUserInfo.setIsSI(weightSi);

                String[] weightValueArray = getValueArray( mUserInfo.getWeight() );
                tvWeight.setText(String.format("%s %s", weightValueArray[0] + "." + weightValueArray[1], weightUP.getDisplayedValues()[weightUP.getValue()]));
            }
        });
        weightDialog.setButtonView( buttonView );
        weightDialog.show();
    }

    private boolean validCheckThenSave()
    {

        if( mStep == 0 ) {
            if (this.mSelectedGender == -1) {
                Toast.makeText(this, this.getResources().getString(R.string.signin_gender_select), Toast.LENGTH_LONG).show();
                return false;
            }

            if (this.etxBirthDay.getText().length() == 0) {
                Toast.makeText(this, this.getResources().getString(R.string.signin_birthday_input), Toast.LENGTH_LONG).show();
                this.etxBirthDay.requestFocus();
                return false;
            }

            if (this.etxBirthDay.getText().length() != 8) {
                Toast.makeText(this, this.getResources().getString(R.string.signin_birthday_do_not_match_digits), Toast.LENGTH_LONG).show();
                this.etxBirthDay.requestFocus();
                return false;
            }

            //년도를 잘라낸다.
            int birthYear = Integer.parseInt(this.etxBirthDay.getText().toString().substring(0, 4));
            Calendar calendar = Calendar.getInstance();
            int currentYear = calendar.get(Calendar.YEAR);
            if ((currentYear - birthYear) > 150) {
                Toast.makeText(this, R.string.signin_birthday_wrong_year, Toast.LENGTH_LONG).show();
                this.etxBirthDay.requestFocus();
                return false;
            }

            if (currentYear < birthYear) {
                Toast.makeText(this, R.string.signin_birthday_wrong_year, Toast.LENGTH_LONG).show();
                this.etxBirthDay.requestFocus();
                return false;
            }

            //월을 잘라낸다
            int birthMonth = Integer.parseInt(this.etxBirthDay.getText().toString().substring(4, 6));
            if (birthMonth < 1 || birthMonth > 12) {
                Toast.makeText(this, R.string.signin_birthday_wrong_month, Toast.LENGTH_LONG).show();
                this.etxBirthDay.requestFocus();
                return false;
            }

            //일을 잘라낸다.
            int birthDay = Integer.parseInt(this.etxBirthDay.getText().toString().substring(6, 8));
            calendar.set(birthYear, birthMonth - 1, 1);
            int lastDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
            if (birthDay < 1 || birthDay > lastDay) {
                Toast.makeText(this, R.string.signin_birthday_wrong_day, Toast.LENGTH_LONG).show();
                this.etxBirthDay.requestFocus();
                return false;
            }

            if (this.tvHeight.getText().toString().trim().length() == 0) {
                Toast.makeText(this, this.getResources().getString(R.string.signin_height_input), Toast.LENGTH_LONG).show();
                this.tvHeight.requestFocus();
                return false;
            }

            if (this.tvWeight.getText().toString().trim().length() == 0) {
                Toast.makeText(this, this.getResources().getString(R.string.signin_weight_input), Toast.LENGTH_LONG).show();
                this.tvWeight.requestFocus();
                return false;
            }

            long longUtcTime = DateUtils.convertDateStringtoUTCTick(this.etxBirthDay.getText().toString(), "yyyyMMdd");
            this.mUserInfo.setBirthDate(longUtcTime);

            this.mUserInfo.setGender(GenderType.getGenderType(this.mSelectedGender));
        }else if(mStep == 1){

        }else if( mStep == 2 ){
            if(this.isBtEnabled() == false)
            {
                Intent enableBtIntent = new Intent( BluetoothAdapter.ACTION_REQUEST_ENABLE );
                this.startActivityForResult(enableBtIntent , this.ENABLE_BT_REQUEST_ID);
                return false;
            }
        }else if( mStep ==4 ){
            if( !this.isSelectWearingLocation ){
                Toast.makeText(this, this.getString(R.string.signin_wearinglocation_select) , Toast.LENGTH_LONG ).show();
                return false;
            }
        }

        return true;
    }

    @Override
    public void onLeScan(final BluetoothDevice device, final int rssi, byte[] scanRecord) {

            String deviceName = device.getName();

            if (deviceName != null) {
                if (deviceName.equals(FITMETER_NAME)) {
                    BluetoothDeviceUtil.checkList(device, rssi);

                }
            }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void startScan()
    {
        this.isScaned = true;
        BluetoothDeviceUtil.clearList();
        this.mTimerValue = 0;
        this.mTimerHander.sendEmptyMessage(0);
        btManager.findFitmeters(FitmeterType.BAND );
        android.util.Log.d("DeviceChangeActivity", "scan start");

    }

    @Override
    public void moveHomeActivity() {
        this.finish();

        Intent intent = new Intent(SetUserInfoActivity.this, NewHomeActivity_.class );
        this.startActivity(intent);
    }

    @Override
    public void showResult(String message) {
        Toast.makeText(this,message,Toast.LENGTH_LONG).show();
    }

    @Override
    public void showPopup(int code, String message) {

    }

    @Override
    public void finishActivity() {
        this.finish();
    }

    private Date getDate(String dateString)
    {
        Date date = null;
        SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat( "yyyyMMdd", Locale.getDefault() );
        try
        {
            date =  mSimpleDateFormat.parse(dateString);
        }
        catch (ParseException e) {}

        return date;
    }

    private String getDateString( Date date ){
        String dateString = null;
        SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat( "yyyyMMdd", Locale.getDefault() );
        dateString =  mSimpleDateFormat.format(date);
        return dateString;
    }

    public void startProgressDialog(final String title, final String message)
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                mProgressDialog = new FitmeterProgressDialog(SetUserInfoActivity.this);
                mProgressDialog.setMessage(message);
                mProgressDialog.setCancelable(false);
                mProgressDialog.show();

            }
        });
    }

    public void changeProgressDialogMessage( final String message ){
        runOnUiThread(new Runnable() {
            @Override
            public void run()
            {
                mProgressDialog.setMessage( message );
            }
        });
    }



    public void stopProgressDialog(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if( mProgressDialog != null ) {
                    mProgressDialog.dismiss();
                }
            }
        });
    }

    @Override
    public void setDuplicateResult(boolean duplicateResult) {

    }

    String foundedAddress = "";

    private void checkFoundDevice( List<BLEDevice> discoveredPeripherals )
    {
        if( discoveredPeripherals.size() == 0 ){
            this.showDialog(this.getString(R.string.signin_device_find_result_no));

        }else if( discoveredPeripherals.size() == 1 ){

            foundedAddress = discoveredPeripherals.get(0).getBluetoothDevice().getAddress();
            this.mUserInfo.setDeviceAddress( discoveredPeripherals.get(0).getBluetoothDevice().getAddress() );
            moveFrontView();

        }else{
            this.showDialog(this.getString(R.string.signin_device_find_result_one_more));
        }
    }

    private void showDialog( String message ){
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);

        dialog.setTitle("")
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton( this.getString(R.string.signin_device_research) , new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        //startScan();
                        mStep -= 1;
                        drawChildView();
                        dialog.dismiss();
                    }
                })
                .show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {

        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == ENABLE_BT_REQUEST_ID){
            if( resultCode == RESULT_OK ){
                moveFrontView();
            }else if(resultCode == RESULT_CANCELED){
                Toast.makeText(this , this.getString(R.string.signin_fitmeter_bluetooth_on) , Toast.LENGTH_LONG).show();
            }
        }

    }

    @Override
    public void onBackPressed() {
        this.moveBackView();
    }

    private boolean isBtEnabled()
    {
        final BluetoothManager manager = (BluetoothManager) this.getSystemService(Context.BLUETOOTH_SERVICE);
        if( manager == null ) return false;

        final BluetoothAdapter adapter = manager.getAdapter();
        if( adapter == null ) return false;

        return adapter.isEnabled();
    }

    private String[] getValueArray( float value ){
        String valueString = String.valueOf( value );
        String[] result = null;
        String[] splitedWeightString = valueString.split("\\.");

        if( splitedWeightString.length > 1 ){
            splitedWeightString[1] = splitedWeightString[1].substring(0,1);
            result = splitedWeightString;
        }else{
            result = new String[]{ splitedWeightString[0] , "0" };
        }

        return result;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if( requestCode == PERMISSION_REQUEST_LOCATION ){

            if( grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED ){

                startScan();

            }else{

                AlertDialog.Builder builder = new AlertDialog.Builder(SetUserInfoActivity.this);
                builder.setMessage( "위치 권한을 수락하지 않으면 Fitmeter 검색을 진행할 수 없습니다. 수락해 주십시오." )
                        .setCancelable(true)        // 뒤로 버튼 클릭시 취소 가능 설정
                        .setNeutralButton(R.string.common_ok, new DialogInterface.OnClickListener() {
                            // 확인 버튼 클릭시 설정
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.cancel();
                            }
                        });
                AlertDialog dialog = builder.create();    // 알림창 객체 생성
                dialog.show();    // 알림창 띄우기
            }

        }
    }

    private boolean resetFitmeter()
    {
        //현재시긴 설정
        org.apache.log4j.Log.d(TAG, "현재 시간을 설정합니다.");
        if( !btManager.setCurrentTime() ){
            org.apache.log4j.Log.e(TAG, "시간 설정 실패");
        }

        //LED 켜기
        btManager.turnonled( BTManager.LED_COLOR_BLUE );

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //LED 끄기
        btManager.turnoffled();

        //착용위치 설정
        org.apache.log4j.Log.d(TAG, "착용위치를 설정합니다.");
        if( !btManager.setWeringLocation(mUserInfo.getWearAt()) ){
            org.apache.log4j.Log.d(TAG, "착용위치 설정 실패");
            return false;
        }

        //이전 데이터를 삭제한다.
        org.apache.log4j.Log.d(TAG, "이전 데이터를 삭제합니다.");
        if( !btManager.deleteAllFile()){
            org.apache.log4j.Log.e(TAG, "이전 데이터 삭제 실패.");
        }

        return true;
    }

    private void connectFitmeter()
    {
        if( retryCount < 3 )
        {
            retryCount++;
            org.apache.log4j.Log.d(TAG, "기기 연결 시도 " + retryCount + "번째");
            btManager.connectFitmeter( foundedAddress );

        }else{

            btManager.clear();

            org.apache.log4j.Log.e(TAG, "기기 등록을 실패하였습니다.");
            stopProgressDialog();

            showResult( this.getString(R.string.signin_setuserinfo_fail) );
        }
    }

}
