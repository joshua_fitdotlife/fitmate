package com.fitdotlife.fitmate;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fitdotlife.fitmate_lib.key.FitmeterType;

/**
 * Created by Joshua on 2016-08-03.
 */
public class PowerCheckFragment extends Fragment
{
    private FitmeterType mDeviceType = null;

    private TextView tvDescription = null;
    private ImageView imgPowerCheckFull = null;
    private ImageView imgPowerCheckLack = null;
    private TextView tvFullColor = null;
    private TextView tvFullText = null;
    private View viewSpace = null;

    public static PowerCheckFragment newInstance( FitmeterType deviceType )
    {
        Bundle args = new Bundle();
        PowerCheckFragment fragment = new PowerCheckFragment();
        args.putInt( SelectDeviceFragment.DEVICETYPE_KEY , deviceType.ordinal() );
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getArguments() != null){
            mDeviceType = FitmeterType.values()[ getArguments().getInt( SelectDeviceFragment.DEVICETYPE_KEY ) ];
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View rootView = inflater.inflate( R.layout.fragment_power_check , container , false );

        this.tvDescription = (TextView) rootView.findViewById(R.id.tv_fragment_power_check_description);
        this.imgPowerCheckFull = (ImageView) rootView.findViewById(R.id.img_fragment_power_check_full);
        this.imgPowerCheckLack = (ImageView) rootView.findViewById(R.id.img_fragment_power_check_lack);
        this.tvFullColor = (TextView) rootView.findViewById(R.id.tv_fragment_power_check_fullcolor);
        this.tvFullText = (TextView) rootView.findViewById(R.id.tv_fragment_power_check_fulltext);
        //this.viewSpace = rootView.findViewById(R.id.view_fragment_power_check_space);

        //LinearLayout.LayoutParams spaceParams = (LinearLayout.LayoutParams) viewSpace.getLayoutParams();

        String description = null;
        String colorText = null;
        int imgResourceIdFull = 0;
        int imgResourceIdLack = 0;
        int fullTextColor = 0;

        if( mDeviceType.equals(FitmeterType.BAND ) ){

            description = this.getResources().getString( R.string.signin_power_check_description_band );
            colorText = this.getResources().getString( R.string.signin_power_check_blue );
            fullTextColor = 0xB30000FF;
            imgResourceIdFull = R.drawable.band_3_blue;
            imgResourceIdLack = R.drawable.band_3_red;

            //spaceParams.width = (int) TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP , 94.5f, getResources().getDisplayMetrics()  );


        }else if(mDeviceType.equals( FitmeterType.BLE )){

            description = this.getResources().getString( R.string.signin_power_check_description_ble );
            colorText = this.getResources().getString( R.string.signin_power_check_blue );
            fullTextColor = 0xB30000FF;
            imgResourceIdFull = R.drawable.ble_3_blue;
            imgResourceIdLack = R.drawable.ble_3_red;
            //spaceParams.width = (int) TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP , 104 , getResources().getDisplayMetrics()  );
        }

        this.tvDescription.setText(description);
        this.imgPowerCheckFull.setImageResource(imgResourceIdFull);
        this.imgPowerCheckLack.setImageResource(imgResourceIdLack);
        this.tvFullColor.setText(colorText);
        this.tvFullColor.setTextColor(fullTextColor);
        this.tvFullText.setTextColor(fullTextColor);
        //this.viewSpace.setLayoutParams( spaceParams );

        return rootView;
    }

    public void setDeviceType( FitmeterType deviceType){
        mDeviceType = deviceType;
    }

}
