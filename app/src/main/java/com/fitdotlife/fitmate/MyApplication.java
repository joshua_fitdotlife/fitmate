package com.fitdotlife.fitmate;

import android.app.AlertDialog;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Looper;
import android.support.multidex.MultiDex;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.fitdotlife.fitmate.btmanager.BTManager;
import com.fitdotlife.fitmate_lib.util.Utils;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import net.lineable.library.LineableLibrary;
import net.lineable.library.MissingLineable;
import net.lineable.library.State;
import net.lineable.library.tool.L;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.fabric.sdk.android.Fabric;
import org.apache.log4j.Logger;

/**
 * Created by wooseok on 2015-05-12.
 */
public class MyApplication extends Application
{
    public final static String TAG_NUM = "1.1.5_55";
    public static boolean LOCATION_PERMISSION_GRANTED = true;

    private boolean initialNetworkOnline = true;
    private List<NetWorkChangeListener> networkChangeListenerList = new ArrayList<NetWorkChangeListener>();
    private BTManager btManager = null;

    private static final String PROPERTY_ID = "UA-71110827-1";

    public static int[] tabStyleList = new int[]{ R.style.DayTheme , R.style.SunsetTheme , R.style.NightTheme };
    public static int mSelectedTabStyleIndex = 0;

    public enum TrackerName {
        APP_TRACKER,           // 앱 별로 트래킹
        GLOBAL_TRACKER,        // 모든 앱을 통틀어 트래킹
        ECOMMERCE_TRACKER,     // 아마 유료 결재 트래킹 개념 같음
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install( this );
    }

    HashMap<TrackerName, Tracker> mTrackers = new HashMap<TrackerName, Tracker>();

    synchronized Tracker getTracker(TrackerName trackerId){
        if(!mTrackers.containsKey(trackerId)){
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            Tracker t = (trackerId == TrackerName.APP_TRACKER) ? analytics.newTracker(PROPERTY_ID) :
                    (trackerId == TrackerName.GLOBAL_TRACKER) ? analytics.newTracker(R.xml.global_tracker) :
                            analytics.newTracker(R.xml.ecommerse_tracker);
            mTrackers.put(trackerId, t);
        }
        return mTrackers.get(trackerId);
    }

    private BroadcastReceiver LineableReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent) {

            State state = (State)intent.getSerializableExtra("state");
            String content = intent.getStringExtra("content");
            Log.i("fitmate", "onReceive : " + state.name() + ", content : " + content);

            switch (state) {
                case BLE_SCAN_START:
                    break;
                case BLE_SCAN_STOP:
                    break;
                case SET_NEXT_ALARM:
                    break;
                case CANCEL_NEXT_ALARM:
                    break;
                case DEPRECATED:
                    // this library is deprecated, please use updated library.
                    break;
                case ERR_BT_NULL:
                    break;
                case ERR_BT_OFF:
                    break;
                case ERR_LOCATION_NEED_M:
                    // user did not permit location service on your app.
                    showLocationServiceAlertForM();
                    break;
                case SERVER_RESPONSE:
                    break;
                case MISSING_LINEABLE:
                    // if missing reported lineable exist.
                    Type listType = new TypeToken<ArrayList<MissingLineable>>() {
                    }.getType();
                    ArrayList<MissingLineable> missingChildArray = new Gson().fromJson(content, listType);
                    processMissingChild(missingChildArray);
                    break;
                case APIKEY_INVALID:
                    // check your apikey
                    break;
                case APIKEY_EXPIRED:
                    // check your apikey
                    break;
            }
        }
    };

    private void showLocationServiceAlertForM() {
        AlertDialog.Builder builder = new AlertDialog.Builder( MyApplication.this );
        builder.setTitle("Error");
        builder.setMessage("Need location service to get location and bluetooth scan");
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                //ActivityCompat.requestPermissions( MyApplication.this , new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 0);
            }
        });
        builder.show();
    }


    private void processMissingChild(ArrayList<MissingLineable> missingChildArray) {
        for(MissingLineable missing : missingChildArray) {
            L.d("fitmate", "missing : " + new Gson().toJson(missing));
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();

        if( !Utils.isOnline(this) ){
            initialNetworkOnline = false;
        }

        //org.apache.log4j.Log.configure( getApplicationContext() );

        //리니어블 라이브러를 시작한다.
        //LineableLibrary.start(this, "eX1v0D1sQ8HJkIm3JlFesw6Dm6505b0o", 5 ,LineableReceiver);
        //리니어블 라이브러리 종료 코드는 아래와 같다.
        //LineableLibrary.stop( Context );

        //fabric.io 붙임
        Fabric.with(this.getApplicationContext(), new Crashlytics());
        initImageLoader(getApplicationContext());

        btManager = BTManager.getInstance( this.getApplicationContext() , null );
        btManager.registerBTReceiver(this.getApplicationContext());
    }

    public static void initImageLoader(Context context){
        ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(context);
        config.threadPriority(Thread.NORM_PRIORITY - 2);
        config.denyCacheImageMultipleSizesInMemory();
        config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
        config.diskCacheSize(50 * 1024 * 1024); // 50 MiB
        config.writeDebugLogs(); // Remove for release app

        ImageLoader.getInstance().init(config.build());
    }

    public void notifyNetworkStateChange(boolean availableNetwork)
    {
        if(!initialNetworkOnline) {
            if (availableNetwork) {
                initialNetworkOnline = false;
                for (NetWorkChangeListener listener : networkChangeListenerList) {
                    listener.OnNetworkAvailable();
                }
            }
        }
    }

    public void addNetworkChangeListener( NetWorkChangeListener listener ){
        networkChangeListenerList.add(listener);
    }

    @Override
    public void onTerminate()
    {
        super.onTerminate();

        btManager.unRegisterBTReceiver( this.getApplicationContext() );
    }


    public  void deleteNetworkChangeListener( NetWorkChangeListener listener ){
        networkChangeListenerList.remove(listener);
    }

}
