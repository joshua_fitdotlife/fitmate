package com.fitdotlife.fitmate;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ContinuousCheckPolicy;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ContinuousExerciseInfo;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseAnalyzer;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.StrengthInputType;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.UserInfoForAnalyzer;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.WearingLocation;
import com.fitdotlife.fitmate_lib.customview.ComboSeekBarView;
import com.fitdotlife.fitmate_lib.customview.FitmeterDialog;
import com.fitdotlife.fitmate_lib.customview.RangeSeekBarView;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.http.NetworkClass;
import com.fitdotlife.fitmate_lib.http.NewsService;
import com.fitdotlife.fitmate_lib.http.RestHttpErrorHandler;
import com.fitdotlife.fitmate_lib.iview.IExerciseProgramInfoView;
import com.fitdotlife.fitmate_lib.key.ContinuousExerciseType;
import com.fitdotlife.fitmate_lib.object.DayActivity;
import com.fitdotlife.fitmate_lib.object.ExerciseProgram;
import com.fitdotlife.fitmate_lib.object.ScoreClass;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.object.WeekActivity;
import com.fitdotlife.fitmate_lib.presenter.ExerciseProgramInfoPresenter;
import com.fitdotlife.fitmate_lib.util.Utils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.res.StringRes;
import org.androidannotations.rest.spring.annotations.RestService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

@EActivity(R.layout.activity_exercise_program_info)
public class ExerciseProgramInfoActivity extends Activity implements View.OnClickListener, IExerciseProgramInfoView, RangeSeekBarView.RangeSeekBarListener, ComboSeekBarView.ComboSeekBarListener, TextView.OnEditorActionListener {

    public static final String ACTION_PROGRAM_CHANGE = "com.fitdotlife.fitmate.PROGRAM_CHANGE";
    private final String TAG = "fitmate - " + ExerciseProgramInfoActivity.class.getSimpleName();

    private TextView tvName = null;
    private TextView etxInfo = null;
    private TextView tvStrengthType = null;
    private TextView tvContinuousExercise = null;

    private TextView tvStrengthUnit = null;
    private Spinner spnStrengthFrom = null;
    private Spinner spnStrengthTo = null;

    private RangeSeekBarView rsbStrengthRange = null;
    private ComboSeekBarView csbExerciseTimes = null;
    private RangeSeekBarView rsbExerciseTime = null;

    private LinearLayout llCalorieStrengthRange = null;
    private LinearLayout llExerciseStrengthRange = null;
    private LinearLayout llExerciseTimes = null;

    private LinearLayout llExerciseMinuite = null;
    private LinearLayout llExerciseContinuousExercise = null;

    private Button btnSave = null;
    private Button btnApply = null;

    private LinearLayout llMenu = null;

    private StrengthInputType strengthType = null;
    private ExerciseProgramInfoPresenter mPresenter = null;
    private int mProgramId = -1;
    private int mExerciseType = -1;
    private ExerciseProgram mExerciseProgram = null;

    private ApplyExerciseDialog mApplyExerciseDialog = null;

    private EditText etxStrengthRangeCalorieFrom = null;
    private EditText etxStrengthRangeCalorieTo = null;

    private FitmateDBManager mDBManager = null;

    private boolean isFriend = false;

    @StringRes(R.string.alarm_program_change)
    String strProgramChange;

    @RestService
    NewsService newsServiceClient;

    @Bean
    RestHttpErrorHandler restErrorHandler;

    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable edit) {
            btnSave.setEnabled(  true );
            btnSave.setBackgroundColor( getResources().getColor(R.color.fitmate_cyan) );
        }
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
        }
        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
        }
    };

    @AfterViews
    void onInit()
    {
        newsServiceClient.setRootUrl(NetworkClass.baseURL + "/api");

        this.mProgramId = this.getIntent().getIntExtra(ExerciseProgram.ID_KEY , -1);
        this.mExerciseType = this.getIntent().getIntExtra("exercisetype", -1);
        this.isFriend = this.getIntent().getBooleanExtra("Friend",false);

        this.mPresenter = new ExerciseProgramInfoPresenter(this, this , this.mExerciseType);
        this.mExerciseProgram = this.mPresenter.getExerciseProgram(this.mProgramId);

        this.mDBManager = new FitmateDBManager(this);

        String programName = null;
        String programInfo = null;

        if(mExerciseProgram.getDefaultProgram())
        {
            int nameResID =  this.getResources().getIdentifier( mExerciseProgram.getName(), "string", this.getPackageName());
            programName = this.getString(nameResID);
            int infoResID =  this.getResources().getIdentifier(mExerciseProgram.getInfo() , "string", this.getPackageName());
            programInfo = this.getString(infoResID);
        }
        else
        {
            programName = mExerciseProgram.getName();
            programInfo = mExerciseProgram.getInfo();
        }

        this.tvName = (TextView) this.findViewById(R.id.tv_activity_exercise_program_info_name);
        this.etxInfo = (TextView) this.findViewById(R.id.etx_activity_exercise_program_info_info);

        this.tvName.setText( programName );
        this.etxInfo.setText(programInfo);

        this.llExerciseContinuousExercise = (LinearLayout) this.findViewById(R.id.ll_activity_exercise_info_continuous_exercise);
        this.llExerciseMinuite = (LinearLayout) this.findViewById(R.id.ll_activity_exercise_info_exercise_minuite);
        this.llExerciseTimes = (LinearLayout) this.findViewById(R.id.ll_activity_exercise_info_exercise_times);

        this.llCalorieStrengthRange = (LinearLayout) this.findViewById(R.id.ll_activity_exercise_info_strength_range_calorie);
        this.etxStrengthRangeCalorieFrom = (EditText) this.findViewById(R.id.etx_activity_exercise_info_strength_range_calorie_from);
        this.etxStrengthRangeCalorieTo = (EditText) this.findViewById(R.id.etx_activity_exercise_info_strength_range_calorie_to);
        this.etxStrengthRangeCalorieFrom.setText((int) this.mExerciseProgram.getStrengthFrom() + "");
        this.etxStrengthRangeCalorieTo.setText((int) this.mExerciseProgram.getStrengthTo() + "");
        this.etxStrengthRangeCalorieTo.addTextChangedListener(textWatcher);
        this.etxStrengthRangeCalorieFrom.addTextChangedListener(textWatcher);

        this.llExerciseStrengthRange = (LinearLayout) this.findViewById(R.id.ll_activity_exercise_info_strength_range_exercise_strength);

        this.tvStrengthType = (TextView) this.findViewById(R.id.tv_activity_exercise_program_info_strength_type);
        this.strengthType = StrengthInputType.getStrengthType(this.mExerciseProgram.getCategory());
        this.tvStrengthType.setText(  this.getCategoryChar( this.strengthType ) );

        this.tvStrengthUnit = (TextView) this.findViewById(R.id.tv_activity_exercise_program_info_strength_unit);
        if(!TextUtils.isEmpty( Utils.getStrengthUnit( this.strengthType ) )){
            this.tvStrengthUnit.setText( "(" + Utils.getStrengthUnit( this.strengthType ) + ")" );
        }

        this.rsbStrengthRange = (RangeSeekBarView) this.findViewById(R.id.rsb_activity_exercise_info_strength_range);
        this.rsbStrengthRange.setScaleRangeMax((int) this.mExerciseProgram.getStrengthTo() + (int) (this.mExerciseProgram.getStrengthTo() - this.mExerciseProgram.getStrengthFrom()));
        this.rsbStrengthRange.setInitialValue((int) this.mExerciseProgram.getStrengthFrom(), (int) this.mExerciseProgram.getStrengthTo());
        this.rsbStrengthRange.setListener(this);

        this.csbExerciseTimes = (ComboSeekBarView) this.findViewById(R.id.csb_activity_exercise_info_exercise_times);
        this.csbExerciseTimes.setInitialValue(this.mExerciseProgram.getTimesFrom(), this.mExerciseProgram.getTimesTo());
        this.csbExerciseTimes.setListener(this);

        this.rsbExerciseTime = (RangeSeekBarView) this.findViewById(R.id.rsb_activity_exercise_info_time);
        this.rsbExerciseTime.setInitialValue(this.mExerciseProgram.getMinuteFrom(), this.mExerciseProgram.getMinuteTo());
        this.rsbExerciseTime.setListener(this);

        this.spnStrengthFrom = (Spinner) this.findViewById(R.id.spn_activity_exercise_info_strength_from);
        this.spnStrengthTo = (Spinner) this.findViewById(R.id.spn_activity_exercise_info_strength_to);

        this.tvContinuousExercise = (TextView) this.findViewById(R.id.tv_activity_exercise_program_info_continuous_exercise);
        this.tvContinuousExercise.setText(this.getContinuousExerciseChar(this.mExerciseProgram.getContinuousExercise()));

        if( strengthType == StrengthInputType.CALORIE || strengthType == StrengthInputType.CALORIE_SUM || strengthType == StrengthInputType.VS_BMR){

            this.llCalorieStrengthRange.setVisibility( View.VISIBLE );
            this.llExerciseStrengthRange.setVisibility( View.GONE );
            this.rsbStrengthRange.setVisibility(View.GONE);
            this.llExerciseContinuousExercise.setVisibility(View.GONE);
            this.llExerciseMinuite.setVisibility(View.GONE);
            this.rsbExerciseTime.setVisibility( View.GONE );

            if( strengthType == StrengthInputType.CALORIE_SUM ){

                this.llExerciseTimes.setVisibility(View.GONE);
                this.csbExerciseTimes.setInitialValue( 7 , 7 );
                this.csbExerciseTimes.setTouchable(false);

            }
        }else if( strengthType == StrengthInputType.STRENGTH ){
            this.llExerciseStrengthRange.setVisibility(View.VISIBLE);
            this.llCalorieStrengthRange.setVisibility(View.GONE);
            this.rsbStrengthRange.setVisibility(View.GONE);

            this.spnStrengthFrom.setSelection((int) mExerciseProgram.getStrengthFrom());
            this.spnStrengthFrom.setEnabled(false);
            this.spnStrengthTo.setSelection((int) mExerciseProgram.getStrengthTo());
            this.spnStrengthTo.setEnabled(false);

        }else{
            this.llExerciseStrengthRange.setVisibility(View.GONE);
            this.llCalorieStrengthRange.setVisibility(View.GONE);
            this.rsbStrengthRange.setVisibility(View.VISIBLE);
        }

        this.btnSave = (Button) this.findViewById(R.id.btn_activity_exercise_info_save);
        this.btnApply = (Button) this.findViewById(R.id.btn_activity_exercise_info_apply);

        UserInfo userInfo = mDBManager.getUserInfo();
        if( userInfo.getDeviceAddress() != null && !userInfo.getDeviceAddress().equals("") && !userInfo.getDeviceAddress().equals("null") ) { //기기등록을 안했다면 기기 변경을 할 수 없다.
            if (isFriend) {

                btnSave.setVisibility(View.GONE);
                btnApply.setVisibility(View.GONE);

            } else {
                this.btnSave.setOnClickListener(this);
                this.btnApply.setOnClickListener(this);

                if (this.mExerciseProgram.isApplied()) {
                    this.btnApply.setBackgroundResource(R.drawable.button_round_cyan);
                    this.btnApply.setTextColor(Color.WHITE);
                    this.btnApply.setText(this.getString(R.string.exercise_detail_current_exercise_program));
                    this.btnApply.setEnabled(false);
                }
            }
        }else{
            btnSave.setVisibility(View.GONE);
            btnApply.setVisibility(View.GONE);
        }

        //액션바 설정
        View actionBarView = this.findViewById(R.id.ic_activity_exercise_programinfo_actionbar);
        actionBarView.setBackgroundColor(this.getResources().getColor(R.color.fitmate_cyan));
        TextView tvBarTitle = (TextView) actionBarView.findViewById( R.id.tv_custom_action_bar_title );
        ImageView imgLeft = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_left);
        imgLeft.setVisibility(View.INVISIBLE);
        ImageView imgRight = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_right);
        imgRight.setVisibility(View.INVISIBLE);
        tvBarTitle.setText( this.getString(R.string.exercise_detail_bar_title) );

        this.getWeekStartDate();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View view)
    {
        switch( view.getId() )
        {
//            case R.id.tv_activity_exercise_info_apply_exercise:
//
//                String dialogContent = String.format(this.getString(R.string.activity_exerciseinfo_apply_exercise_detail) , this.mExerciseProgram.getName() );
//                this.mApplyExerciseDialog = new ApplyExerciseDialog(this , this , dialogContent);
//                this.mApplyExerciseDialog.show();
//
//                break;
//
//            case R.id.ll_activity_exercie_info_modify_exercise:
//                Toast.makeText(this , "준비중입니다.",Toast.LENGTH_SHORT).show();
//                break;

            case R.id.btn_activity_exercise_info_apply:

                String programName = null;

                if(mExerciseProgram.getDefaultProgram())
                {
                    int resId =  getResources().getIdentifier( mExerciseProgram.getName() , "string" , getPackageName());
                    programName = getString(resId);
                }
                else
                {
                    programName = mExerciseProgram.getName();
                }

                String dialogContent = String.format(this.getString(R.string.exercise_detail_want_change_exercise) , programName );

//                DialogInterface applyExerciseDialog = null;
//                AlertDialog.Builder applyExerciseDialogBuilder = new AlertDialog.Builder(this);
//                View applyExerciseView = this.getLayoutInflater().inflate(R.layout.dialog_apply_exercise, null);
//                applyExerciseDialogBuilder.setView(applyExerciseView);
//                applyExerciseDialog = applyExerciseDialogBuilder.show();
//
//                TextView contetView = (TextView) applyExerciseView.findViewById(R.id.tv_dialog_appliy_exercise_content);
//                contetView.setMessage( dialogContent );
//
//                Button btnOK = (Button) applyExerciseView.findViewById(R.id.btn_dialog_apply_exercise_ok);
//                final DialogInterface finalApplyExerciseDialog = applyExerciseDialog;
//                btnOK.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        finalApplyExerciseDialog.dismiss();
//                        mPresenter.applyExerciseProgram( mExerciseProgram );
//                        changeExerciseProgramwithID( mExerciseProgram.getId(), getWeekStartDate() );
//
//                        Intent programChangeIntent = new Intent( ACTION_PROGRAM_CHANGE );
//                        sendBroadcast( programChangeIntent );
//
//                        finish();
//                    }
//                });
//                Button btnCancel = (Button) applyExerciseView.findViewById(R.id.btn_dialog_apply_exercise_cancel);
//                btnCancel.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        finalApplyExerciseDialog.dismiss();
//                    }
//                });

                final FitmeterDialog dialog = new FitmeterDialog(this);
                LayoutInflater inflater = this.getLayoutInflater();

                View contentView = inflater.inflate( R.layout.dialog_default_content , null );
                TextView tvTitle = (TextView) contentView.findViewById(R.id.tv_dialog_default_title);
                tvTitle.setText( this.getString( R.string.exercise_detail_change_exercise ) );
                TextView tvContent = (TextView) contentView.findViewById(R.id.tv_dialog_default_content);
                tvContent.setText( dialogContent );
                dialog.setContentView(contentView);

                View buttonView = inflater.inflate( R.layout.dialog_button_two , null );
                Button btnLeft = (Button) buttonView.findViewById(R.id.btn_dialog_button_two_left);
                btnLeft.setText(this.getString(R.string.common_cancel));
                btnLeft.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.close();
                    }
                });
                Button btnRight = (Button) buttonView.findViewById(R.id.btn_dialog_button_two_right);
                btnRight.setText(this.getString(R.string.common_ok));
                btnRight.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mPresenter.applyExerciseProgram(mExerciseProgram);
                        changeExerciseProgramwithID(mExerciseProgram.getId(), getWeekStartDate());

                        Intent programChangeIntent = new Intent( ACTION_PROGRAM_CHANGE );
                        sendBroadcast(programChangeIntent);
                        finish();

                        dialog.close();
                    }
                });
                dialog.setButtonView(buttonView);
                dialog.show();
                break;
            case R.id.btn_activity_exercise_info_save:
                if( strengthType == StrengthInputType.CALORIE || strengthType == StrengthInputType.CALORIE_SUM || strengthType == StrengthInputType.VS_BMR ){

                    float calorieFrom = 0;
                    try {
                        calorieFrom = Utils.parseFloat( this.etxStrengthRangeCalorieFrom.getText().toString() );
                    } catch (ParseException e) {
                        Log.e("fitmate" , e.getMessage());
                    }

                    float calorieTo = 0;
                    try {
                        calorieTo = Utils.parseFloat( this.etxStrengthRangeCalorieTo.getText().toString() );
                    } catch (ParseException e) {
                        Log.e( "fitmate" , e.getMessage() );
                    }
                    if( calorieFrom > calorieTo ){
                        Toast.makeText(this , "칼로리 목표를 다시 설정하여 주십시오.", Toast.LENGTH_SHORT).show();
                        this.etxStrengthRangeCalorieFrom.requestFocus();
                        return;
                    }

                    this.mExerciseProgram.setStrengthFrom(calorieFrom);
                    this.mExerciseProgram.setStrengthTo(calorieTo);
                }
                this.mPresenter.modifyExerciseProgram( this.mExerciseProgram);
                break;
        }
    }

    @Background
    void addNews(){
//        UserInfo userInfo = mDBManager.getUserInfo();
//        News programChangeNews = new News();
//
//        int resId =  getResources().getIdentifier(mExerciseProgram.getName(), "string", getPackageName());
//        String programName = getString(resId);
//        programChangeNews.setContent(String.format(strProgramChange , userInfo.getName() , programName));
//        programChangeNews.setFriendcontent(String.format(strProgramChange, userInfo.getName(), programName));
//
//        programChangeNews.setType(NewsType.PROGRAM_CHANGED.getValue());
//        programChangeNews.setPublishdate( DateUtils.getUTCTimeMilliseconds() );
//        newsServiceClient.addNews( programChangeNews , userInfo.getEmail() );
    }

    private Date getWeekStartDate(  ) {

        Calendar today = resetTime(Calendar.getInstance().getTime());
         Calendar weekStartDate = Calendar.getInstance();
        weekStartDate.setTimeInMillis(today.getTimeInMillis());
        weekStartDate.add(Calendar.DATE, -today.get(Calendar.DAY_OF_WEEK) + 1);
        String weekStart = getDateString( weekStartDate.getTime() );
        return weekStartDate.getTime();
    }
    public Calendar resetTime (Date d) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(d);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal;
    }
    private String getDateString( Date date ){
        String dateString = null;
        SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat( "yyyy-MM-dd", Locale.getDefault() );
        dateString =  mSimpleDateFormat.format(date);
        return dateString;
    }
    public static Date getDateFromString(String dateString)
    {
        Date date = null;
        SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat( "yyyy-MM-dd", Locale.getDefault() );
        try
        {
            date =  mSimpleDateFormat.parse(dateString);
        }
        catch (ParseException e) {}

        return date;
    }

    //운동 프로그램이 바뀌면 이번주의 일일 활동량을 변경한다.
    private boolean  changeExerciseProgramwithID( int exerciseProgramId, Date weekStartDate )
    {
        FitmateDBManager dbManager = new FitmateDBManager(this);

        UserInfo userTable = dbManager.getUserInfo();

        userTable.setExerprogramId(exerciseProgramId);

        dbManager.setUserInfo(userTable);

        WeekActivity weekTable= dbManager.getWeekActivity( getDateString(weekStartDate) );
        Date weekEndDate = new Date(weekStartDate.getTime() + 86400*6*1000);
        List<DayActivity> dayActivitySet = dbManager.getDayActivityList(getDateString(weekStartDate), getDateString(weekEndDate));
        if(weekTable==null){

            return true;
        }
        DayActivity lastDayActivityTable= dayActivitySet.get(dayActivitySet.size()-1);
        weekTable.setExerciseProgramID(exerciseProgramId);

     //   weekTable = lastDayActivityTable.datasavinginterval;
        com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseProgram exTable =dbManager.getExerciseProgramForAnalyzer(exerciseProgramId);


        UserInfoForAnalyzer userInfoForAnalyzer =  com.fitdotlife.fitmate_lib.service.util.Utils.getUserInfoForAnalyzer(userTable);
        com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseProgram program =dbManager.getExerciseProgramForAnalyzer(exerciseProgramId);

        WearingLocation location = WearingLocation.WAIST;

        ExerciseAnalyzer analyzer = new ExerciseAnalyzer(exTable, userInfoForAnalyzer, location , (int) lastDayActivityTable.getDataSaveInterval());

        List<Integer> arrayforScore = new ArrayList<Integer>();



        boolean calorieType=false;

        if(program.getStrengthInputType()== StrengthInputType.CALORIE|| program.getStrengthInputType()== StrengthInputType.CALORIE_SUM ||program.getStrengthInputType()== StrengthInputType.VS_BMR){

            calorieType=true;

        }


        Date tempDate=weekStartDate;


        int str_h=0;

        int str_m=0;

        int str_l=0;

        int str_ul=0;

        int calorie=0;

        float metSm=0;

        int nullIndex=0;

        List<Integer> nullIndexArray= new ArrayList<Integer>();
        for(DayActivity table: dayActivitySet)
        {

            Date date = getDateFromString(table.getActivityDate());

            while( tempDate.compareTo(date) <0){

                arrayforScore.add(0);

                tempDate=new Date(tempDate.getTime()+86400000);
                nullIndexArray.add(nullIndex);
                nullIndex++;

            }



            if(calorieType){

                table.setAchieveOfTarget(table.getCalorieByActivity());

            }else{


                List<ContinuousExerciseInfo> continuousEx = analyzer.getContinuousExercise(ContinuousCheckPolicy.Loosely_SumOverXExercise, table.getMetArray(),date.getTime(), 1, 60,60, 600 );


                if(continuousEx==null || continuousEx.size()==0){

                    table.setAchieveOfTarget(0);
                }

                else{

                    if(analyzer.getUserInfo().getContinuousCheckPolicy() == ContinuousCheckPolicy.Loosely_SumOverXExercise){

                        int seconds = 0;

                        for (ContinuousExerciseInfo info : continuousEx) {

                            seconds+= info.getTimeSpan()/1000;

                        }

                        table.setAchieveOfTarget((int)Math.round(seconds / 60.0));

                    }

                    else{

                        int minutes =(int) Math.round( continuousEx.get(0).getTimeSpan()/(60.0*1000));

                        table.setAchieveOfTarget(minutes);

                    }

                }



            }

            table.setProgramID(exerciseProgramId);




            arrayforScore.add(table.getAchieveOfTarget());

            calorie += table.getCalorieByActivity();

            metSm += table.getAverageMET();

            str_h+= table.getStrengthHigh();

            str_m+= table.getStrengthMedium();

            str_l+= table.getStrengthLow();

            str_ul+= table.getStrengthUnderLow();

            tempDate=new Date(tempDate.getTime() +86400*1000);

            nullIndex++;

        }



        if(dayActivitySet.size()>0){

            str_h = str_h/dayActivitySet.size();

            str_m = str_m/dayActivitySet.size();

            str_l = str_l/dayActivitySet.size();

            str_ul = str_ul/dayActivitySet.size();

            calorie =calorie/dayActivitySet.size();

            metSm = metSm/dayActivitySet.size();

        }

        weekTable.setAverageCalorie(calorie);

        weekTable.setAverageStrengthHigh(str_h);
        weekTable.setAverageStrengthMedium(str_m);
        weekTable.setAverageStrengthLow(str_l);
        weekTable.setAverageStrengthUnderLow(str_ul);

        weekTable.setAverageMET(metSm);

        weekTable.setActivityDate(getDateString(weekStartDate));

        List<ScoreClass> scoreArray = new ArrayList<ScoreClass>();
        if(calorieType){
            scoreArray=CommonFunction.CalculateDayScores_Calories(arrayforScore,analyzer);
        }
        else{
            scoreArray=CommonFunction.CalculateDayScores_Minutes(arrayforScore ,analyzer);
        }
            int nth=0;

            for(int i=0;i<scoreArray.size() ;i++)

            {

                if(nth<nullIndexArray.size()){

                    if( nullIndexArray.get(nth)== i){

                        nth++;
                        continue;
                    }

                }



                ScoreClass score= scoreArray.get(i);

                DayActivity act= dayActivitySet.get(i-nth);

                act.setPredayScore(score.preDayAchieve);
                act.setTodayScore(score.todayGetPoint);
                act.setExtraScore(score.todayPossibleMorePoint);
                act.setAchieveMax(score.isAchieveMax);
                act.setPossibleMaxScore(score.possibleMaxScore);
                act.setPossibleTimes(score.possibleTimes);
                act.setPossibleValue(score.possibleValue);
                ///notuploaded==0
                act.setUploadState(0);

                dbManager.updateDayActivity(act);
            }

            ScoreClass score= scoreArray.get(scoreArray.size()-1);

            weekTable.setScore(score.weekAchieve);


        dbManager.updateWeekActivity(weekTable);
        return true;
    }

    private String getCategoryChar( StrengthInputType strengthType )
    {
        String result = null;

       switch( strengthType )
       {
           case VO2MAX:
               result = "VO2MAX";
               break;
           case VO2R:
               result = "V2R";
               break;
           case RPE:
               result = "RPE";
               break;
           case MET:
               result = "MET";
               break;
           case STRENGTH:
               result = "EXERCISE_STRENGTH";
               break;
           case CALORIE:
               result = "CALORIE";
               break;
           case CALORIE_SUM:
               result = "TOTAL CALORIE";
               break;
           case VS_BMR:
               result = "VS_BMR";
               break;
       }

        return result;
    }

    private String getContinuousExerciseChar( int continuousExercise )
    {
        String result = null;

        if( continuousExercise == ContinuousExerciseType.CONTINUOUS_EXERCISE)
        {
          result = this.getResources().getString(R.string.common_yes);
        }
        else if( continuousExercise == ContinuousExerciseType.NO_CONTINUOUS_EXERCISE )
        {
            result = this.getResources().getString(R.string.common_no );
        }

        return result;
    }

    @Override
    public void showResult(String message) {
        Toast.makeText(this , message , Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCreate(RangeSeekBarView rangeSeekBar, int index, float value) {

    }

    @Override
    protected void onStart() {
        super.onStart();


    }

    @Override
    public void onSeek(RangeSeekBarView rangeSeekBar, int index, float value) {
        Log.d( TAG , "RangeSeekBar OnSeek() - Value : " + value );

        if( rangeSeekBar.getId() == R.id.rsb_activity_exercise_info_time){

            this.btnSave.setEnabled(  true );
            this.btnSave.setBackgroundColor( this.getResources().getColor(R.color.fitmate_cyan) );

            if( index == 0 ){
                this.mExerciseProgram.setMinuteFrom((int) value);
            }else if( index == 1 ){
                this.mExerciseProgram.setMinuteTo((int) value);
            }

        }else if( rangeSeekBar.getId() == R.id.rsb_activity_exercise_info_strength_range ){

            this.btnSave.setEnabled(  true );
            this.btnSave.setBackgroundColor( this.getResources().getColor(R.color.fitmate_cyan) );

            if( index == 0 ){
                this.mExerciseProgram.setStrengthFrom(value);
            }else if( index == 1){
                this.mExerciseProgram.setStrengthTo(value);
            }
        }
    }

    @Override
    public void onSeekStart(RangeSeekBarView rangeSeekBar, int index, float value) {
        Log.d( TAG , "RangeSeekBar OnSeekStart() - Value : " + value );
    }

    @Override
    public void onSeekStop(RangeSeekBarView rangeSeekBar, int index, float value) {
        Log.d( TAG , "RangeSeekBar OnSeekStop() - Value : " + value );

        if( rangeSeekBar.getId() == R.id.rsb_activity_exercise_info_time){

            this.btnSave.setEnabled(  true );
            this.btnSave.setBackgroundColor( this.getResources().getColor(R.color.fitmate_cyan) );

            if( index == 0 ){
                this.mExerciseProgram.setMinuteFrom((int) value);
            }else if( index == 1 ){
                this.mExerciseProgram.setMinuteTo((int) value);
            }

        }else if( rangeSeekBar.getId() == R.id.rsb_activity_exercise_info_strength_range ){

            this.btnSave.setEnabled(  true );
            this.btnSave.setBackgroundColor( this.getResources().getColor(R.color.fitmate_cyan) );

            if( index == 0 ){
                this.mExerciseProgram.setStrengthFrom(value);
            }else if( index == 1){
                this.mExerciseProgram.setStrengthTo(value);
            }
        }
    }

    @Override
    public void onCreate(ComboSeekBarView rangeSeekBar, int index, float value) {

    }

    @Override
    public void onSeek(ComboSeekBarView comboSeekBar, int index, float value) {
        if( comboSeekBar.getId() == R.id.csb_activity_exercise_info_exercise_times )
        {

            this.btnSave.setEnabled(  true );
            this.btnSave.setBackgroundColor( this.getResources().getColor(R.color.fitmate_cyan) );

            if( index == 0 ){
                this.mExerciseProgram.setTimesFrom((int) value);
            }else if( index == 1 ){
                this.mExerciseProgram.setTimesTo((int) value);
            }
        }
    }

    @Override
    public void onSeekStart(ComboSeekBarView rangeSeekBar, int index, float value) {

    }

    @Override
    public void onSeekStop(ComboSeekBarView comboSeekBar, int index, float value) {
        if( comboSeekBar.getId() == R.id.csb_activity_exercise_info_exercise_times )
        {
            this.btnSave.setEnabled(  true );
            this.btnSave.setBackgroundColor( this.getResources().getColor(R.color.fitmate_cyan) );

            if( index == 0 ){
                this.mExerciseProgram.setTimesFrom((int) value);
            }else if( index == 1 ){
                this.mExerciseProgram.setTimesTo((int) value);
            }
        }
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        return false;
    }
}
