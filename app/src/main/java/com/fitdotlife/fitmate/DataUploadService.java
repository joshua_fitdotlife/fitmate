package com.fitdotlife.fitmate;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.crashlytics.android.Crashlytics;
import com.fitdotlife.fitmate.log.Log;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.http.HttpApi;
import com.fitdotlife.fitmate_lib.http.HttpResponse;
import com.fitdotlife.fitmate_lib.object.DayActivity;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.object.WeekActivity;
import com.fitdotlife.fitmate_lib.util.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.List;

/**
 * Created by Joshua on 2016-10-24.
 */
public class DataUploadService extends Service implements Runnable {

    private final String TAG = DataUploadService.class.getSimpleName();
    private Thread mThread = null;
    private FitmateDBManager mDBManager = null;
    private static boolean isRunThread = false;

    @Override
    public void onCreate()
    {
        super.onCreate();
        Log.d(TAG, "onCreate()");

        this.mDBManager = new FitmateDBManager( this.getApplicationContext() );
        Crashlytics.setUserIdentifier(MyApplication.TAG_NUM);
        Crashlytics.setUserEmail(mDBManager.getUserInfo().getEmail());
        Crashlytics.setUserName( mDBManager.getUserInfo().getName() );
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG , "onStartCommand()");
        super.onStartCommand(intent, flags, startId);

        if(!isRunThread ) {
            mThread = new Thread(this);
            mThread.start();
        }

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        android.util.Log.d(TAG, "onDestroy()");

        if(mThread != null)
        {
            mThread.interrupt();
        }
        super.onDestroy();
    }

    @Override
    public boolean onUnbind( Intent intent )
    {
        android.util.Log.d(TAG , "onUnBind()");
        return super.onUnbind(intent);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public void run()
    {
        isRunThread = true;

        if (Utils.isOnline( this ))
        {
            JSONObject jsonUploadObject = new JSONObject();
            List<DayActivity> uploadDayActivityList = mDBManager.getNotUploadDayActivity();
            List<WeekActivity> uploadWeekActivityList = mDBManager.getNotUploadWeekActivity();
            UserInfo userInfo = mDBManager.getUserInfo();
            String email = userInfo.getEmail();

            if( uploadWeekActivityList != null )
            {
                JSONArray jsonArrayWeekActivity = new JSONArray();
                for (int i = 0; i < uploadWeekActivityList.size(); i++) {

                    WeekActivity weekActivity = uploadWeekActivityList.get(i);
                    jsonArrayWeekActivity.put(weekActivity.toJSONObject( ) );
                }

                try {
                    jsonUploadObject.put("weekItemList", jsonArrayWeekActivity);
                } catch (JSONException e) {
                }
            }

            if( uploadDayActivityList != null )
            {
                JSONArray jsonArrayDayActivity = new JSONArray();
                for (int i = 0; i < uploadDayActivityList.size(); i++)
                {
                    DayActivity dayActivity = uploadDayActivityList.get(i);
                    Calendar dayCalednar = Calendar.getInstance();
                    Calendar weekCalendar = Calendar.getInstance();
                    dayCalednar.setTimeInMillis( dayActivity.getStartTime().getMilliSecond() );
                    weekCalendar.setTimeInMillis(dayActivity.getStartTime().getMilliSecond());
                    weekCalendar.add(Calendar.DATE, -dayCalednar.get(Calendar.DAY_OF_WEEK) + 1);
                    String weekStartDate = weekCalendar.get(Calendar.YEAR) + "-" + String.format("%02d",  weekCalendar.get( Calendar.MONTH ) + 1 ) + "-" + String.format("%02d", weekCalendar.get(Calendar.DAY_OF_MONTH) );
                    jsonArrayDayActivity.put( dayActivity.toJSONObject( mDBManager.getWeekActivity( weekStartDate ).getExerciseProgramID() ));
                }

                try {
                    jsonUploadObject.put("dayItemList", jsonArrayDayActivity);
                } catch (JSONException e) {
                }
            }

            HttpApi.uploadData( jsonUploadObject.toString() , email , new HttpResponse()
            {
                @Override
                public void onResponse(int resultCode, String response)
                {
                    isRunThread = false;
                    Log.e( TAG , "업로드 결과 : " + response );

                    if(resultCode == 200){
                    }
                }
            });
        }
    }
}
