package com.fitdotlife.fitmate.firmware;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.text.Html;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.fitdotlife.fitdotlifelib.protocol.object.SystemInfo_Response;
import com.fitdotlife.fitmate.R;
import com.fitdotlife.fitmate.btmanager.BTManager;
import com.fitdotlife.fitmate.btmanager.ConnectionStatus;
import com.fitdotlife.fitmate.btmanager.FitmeterEventCallback;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.http.FirmwareUpdateService;
import com.fitdotlife.fitmate_lib.http.NetworkClass;
import com.fitdotlife.fitmate.btmanager.ReceivedDataInfo;
import com.fitdotlife.fitmate_lib.key.FitmeterType;
import com.fitdotlife.fitmate_lib.object.BLEDevice;
import com.fitdotlife.fitmate_lib.object.FirmwareVersionInfo;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.util.Utils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringRes;
import org.androidannotations.rest.spring.annotations.RestService;
import org.springframework.web.client.RestClientException;

import org.apache.log4j.Log;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import no.nordicsemi.android.dfu.DfuBaseService;
import no.nordicsemi.android.dfu.DfuLogListener;
import no.nordicsemi.android.dfu.DfuProgressListener;
import no.nordicsemi.android.dfu.DfuServiceListenerHelper;

@EActivity(R.layout.activity_firmware_update)
public class FirmwareUpdateActivity extends Activity
{
    public static final String LASTEST_FIRMWARE_KEY = "lastest_firmware";

    public enum FirmwareUpdateState
    {
        SERVER_DOWNLOAD_COMPLETE , DEVICE_DOWNLOAD_ERROR ,
        DEVICE_DOWNLOAD_NOTFOUND , DEVICE_DOWNLOAD_RECOVERY_NOTFOUND , DEVICE_DOWNLOAD_MULTI_RECOVERY
    }

    private static final String TAG = FirmwareUpdateActivity.class.getSimpleName();

    public static final String EXTRA_OPEN_DFU = "no.nordicsemi.android.nrtbeacon.extra.open_dfu";
    private static final String NBEECON_UPDATE_SHAREDPREFERENCES = "nbeecon_update_sharedpreferences";
    private static final String ADDRESS = "preferences_address";

    private static final byte SEND_KEYINPUT_NOTI    = (byte)0x01;
    private static final byte SEND_BATTERY_NOTI     = (byte)0x02;
    private static final byte SEND_LINK_LOSS_WRITE  = (byte)0x04;
    private static final byte SEND_REMOCON_WRITE    = (byte)0x08;

    private Context mContext = null;
    private BTManager mBTManager = null;

    private ProgressDialog mProgressDialog = null;

    private FitmateDBManager mDBManager = null;
    private UserInfo mUserInfo = null;
    private FirmwareUpdateState mFirmwareUpdateState;

    private FirmwareVersionInfo mServerFirmwareVersionInfo = null;
    private BluetoothDevice mRecoveryDevice = null;

    private String mFilePath;
    private Uri mFileStreamUri;
    private String mInitFilePath;
    private Uri mInitFileStreamUri;
    private int mFileType;

    private boolean isRequestConnect = false;
    private String mPrefixSerialNumber = null;

    private FitmeterType mFitmeterType = null;

    @ViewById(R.id.ic_activity_firmware_update_actionbar)
    View actionBarView;

    @ViewById(R.id.tv_activity_firmware_update_version)
    TextView tvCurrentVersion;

    @ViewById(R.id.tv_activity_firmware_update_message)
    TextView tvMessage;

    @ViewById(R.id.img_activity_firmware_update_arrorw)
    ImageView imgArrow;

    @ViewById(R.id.btn_activity_firmware_update)
    Button btnFirmwareUpdate;

    @ViewById(R.id.tv_activity_firmware_update_recovery)
    TextView tvFirmwareUpdateRecovery;

    //현재 펌웨어 버전
    @StringRes(R.string.firmware_update_current_version)
    String strCurrentVersion;

    @StringRes(R.string.firmware_update_available)
    String strFirmwareAvailable;

    @StringRes(R.string.firmware_update_unavailable)
    String strFirmwareUnavailable;

    @StringRes(R.string.firmware_update_reset)
    String strFirmwareRecovery;

    //펌웨어 업데이트 서버 버전 확인 및 서버로부터 펌웨어 다운로드 관련
    @StringRes(R.string.firmware_update_checking_serverversion)
    String strCheckingServerVersion;

    @StringRes(R.string.firmware_update_network_not_available)
    String strNetworkNotAvailable;

    @StringRes(R.string.firmware_update_lastest_version)
    String strLastestVersion;

    @StringRes(R.string.firmware_update_server_downloading)
    String strServerDownloading;

    @StringRes(R.string.firmware_update_server_download_error)
    String strServerDownloadError;

    @StringRes(R.string.firmware_update_server_download_complete)
    String strServerDownloadComplete;

    //펌웨어 업데이트 - 휘트미터 검색 및 연결
    @StringRes(R.string.firmware_update_fitmeter_finding)
    String strFindingFitmeter;

    @StringRes(R.string.firmware_update_fitmeter_none)
    String strFitmeterNone;

    @StringRes(R.string.firmware_update_fitmeter_connecting)
    String strConnectingFitmeter;

    @StringRes(R.string.firmware_update_fitmeter_disconnect)
    String strDisconnectFitmeter;

    @StringRes(R.string.firmware_update_fitmeter_notsupport)
    String strFitmeterNotSupport;

    @StringRes(R.string.firmware_update_battery_low)
    String strBatteryLow;

    //펌웨어 업데이트 복구 모드 진입 및 복구모드 기기 검색
    @StringRes(R.string.firmware_update_recovery_init)
    String strRecoveryInit;

    @StringRes(R.string.firmware_update_recovery_finding)
    String strFindingRecovery;

    @StringRes(R.string.firmware_update_recovery_none)
    String strRecoveryNone;

    @StringRes(R.string.firmware_update_recovery_multi)
    String strRecoveryMulti;

    //펌웨어 업데이트 진행
    @StringRes(R.string.firmware_update_device_downloading)
    String strDeviceDownloading;

    @StringRes(R.string.firmware_update_device_download_error)
    String strDeviceDownloadError;

    @StringRes(R.string.firmware_update_device_download_complete)
    String strDeviceDownloadComplete;

    @StringRes(R.string.firmware_update_cancel)
    String strUpdateCancel;

    @StringRes(R.string.firmware_update_retry)
    String strUpdateRetry;


    @RestService
    FirmwareUpdateService firmwareUpdateServiceClient;

    private FitmeterEventCallback fitmeterEventCallback = new FitmeterEventCallback()
    {
        @Override
        public void onProgressValueChnaged(int progress, int max)
        {}

        @Override
        public void onReceiveAllFileComplete(List<ReceivedDataInfo> receiveDataArray, boolean isComplete)
        {}

        @Override
        public void receiveCompleted(int index, int offset, byte[] receivedbytes)
        {}

        @Override
        public void statusOfBTManagerChanged( ConnectionStatus status ) {
            switch(status){
                case CONNECTED:
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            isRequestConnect = false;
                            changeProgressMessage( strRecoveryInit );
                            checkDeviceDownload();
                        }
                    }).start();
                    break;
                case DISCONNECTED:
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            if(isRequestConnect) {
                                dismissProgressDialog();
                                showInfoDialog( strDisconnectFitmeter);
                            }
                        }
                    }).start();
                    break;
                case ERROR_DISCONNECTED:
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            if(isRequestConnect) {
                                dismissProgressDialog();
                                showInfoDialog( strDisconnectFitmeter);
                            }
                        }
                    }).start();
                    break;
                case ERROR_DISCONNECTED_133:
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            if(isRequestConnect) {
                                dismissProgressDialog();
                                showInfoDialog( strDisconnectFitmeter);
                            }
                        }
                    }).start();
                    break;
                case NOTFOUND:
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            dismissProgressDialog();
                        }
                    }).start();
                    break;
            }
        }



        @Override
        public void findNewDeviceResult(List<BLEDevice> discoveredPeripherals) {

        }

        @Override
        public void findDeviceResult(final String deviceAddress, final String serialNumber)
        {

            new Thread(new Runnable() {
                @Override
                public void run() {

                    //BLE 버전은 펌웨어 업데이트를 제공하지 않는다.
                    if( serialNumber == null )
                    {
                        dismissProgressDialog();
                        mFirmwareUpdateState = FirmwareUpdateState.DEVICE_DOWNLOAD_NOTFOUND;
                        showConfirmDialog( strFitmeterNone , strUpdateRetry  , strUpdateCancel );

                    }else{

                        if (serialNumber.substring(3, 4).equals("4")) //밴드일 때
                        {
                            mFitmeterType = FitmeterType.BAND;
                        } else if ( serialNumber.substring(3, 4).equals("3") ) { //BLE 45일 때
                            mFitmeterType = FitmeterType.BLE;
                        }

                        mPrefixSerialNumber = serialNumber.substring(0,4);
                        if( mPrefixSerialNumber.equals("1124") )
                        {
                            dismissProgressDialog();
                            showInfoDialog(strFitmeterNotSupport);
                            return;
                        }

                        isRequestConnect = true;
                        changeProgressMessage( strConnectingFitmeter);
                        connectFitmeter(deviceAddress);
                    }
                }
            }).start();
        }

        @Override
        public void findRecoveryDeviceResult( final BluetoothDevice recoveryDevice ,  final List<BluetoothDevice> recoveryFitmeters )
        {

            new Thread(new Runnable()
            {
                @Override
                public void run() {

                    if(recoveryDevice != null)
                    {
                        mRecoveryDevice = recoveryDevice;
                        startFirmwareUpdate();

                    }else{

                        if( recoveryFitmeters.size() == 0 ){

                            dismissProgressDialog();
                            mFirmwareUpdateState = FirmwareUpdateState.DEVICE_DOWNLOAD_RECOVERY_NOTFOUND;
                            showConfirmDialog( strRecoveryNone, strUpdateRetry , strUpdateCancel );

                        }else if( recoveryFitmeters.size() == 1 ){

                            mRecoveryDevice = recoveryFitmeters.get(0);
                            startFirmwareUpdate();

                        }else{

                            dismissProgressDialog();
                            mFirmwareUpdateState = FirmwareUpdateState.DEVICE_DOWNLOAD_MULTI_RECOVERY;
                            showConfirmDialog( strRecoveryMulti , strUpdateRetry , strUpdateCancel );
                        }
                    }
                }
            }).start();
        }
    };

    private void connectFitmeter(final String deviceAddress){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mBTManager.connectFitmeter( deviceAddress );
            }
        });
    }

    private final DfuProgressListener mDfuProgressListener = new DfuProgressListener()
    {

        @Override
        public void onDeviceConnecting(String deviceAddress) {
            mProgressDialog.setIndeterminate(true);
        }

        @Override
        public void onDeviceConnected(String deviceAddress) {
            mProgressDialog.setIndeterminate(true);
        }

        @Override
        public void onDfuProcessStarting(String deviceAddress) {
            mProgressDialog.setIndeterminate(true);
        }

        @Override
        public void onDfuProcessStarted(String deviceAddress) {
            mProgressDialog.setIndeterminate(true);
        }

        @Override
        public void onEnablingDfuMode(String deviceAddress) {
            mProgressDialog.setIndeterminate( true );

        }

        @Override
        public void onProgressChanged(String deviceAddress, int percent, float speed, float avgSpeed, int currentPart, int partsTotal) {
            mProgressDialog.setMessage( strDeviceDownloading);
            mProgressDialog.setIndeterminate( false );
            mProgressDialog.setProgress( percent );
        }

        @Override
        public void onFirmwareValidating(String deviceAddress) {
            mProgressDialog.setIndeterminate( true );
        }

        @Override
        public void onDeviceDisconnecting(String deviceAddress) {}

        @Override
        public void onDeviceDisconnected(String deviceAddress) {}

        @Override
        public void onDfuCompleted(String deviceAddress) {
            dismissProgressDialog();
            setVersionText(strCurrentVersion + " " + mServerFirmwareVersionInfo.getMajorVersion() + "." + mServerFirmwareVersionInfo.getMinorVersion());
            setDeviceFirmwareVersionInfo( mServerFirmwareVersionInfo.getMajorVersion() , mServerFirmwareVersionInfo.getMinorVersion() );
            showInfoDialog(strDeviceDownloadComplete);
        }

        @Override
        public void onDfuAborted(String deviceAddress) {
            dismissProgressDialog();
            mFirmwareUpdateState = FirmwareUpdateState.DEVICE_DOWNLOAD_ERROR;
            showConfirmDialog(strDeviceDownloadError, strUpdateRetry , strUpdateCancel );
        }

        @Override
        public void onError(String deviceAddress, int error, int errorType, String message) {
            dismissProgressDialog();
            mFirmwareUpdateState = FirmwareUpdateState.DEVICE_DOWNLOAD_ERROR;
            showConfirmDialog( strDeviceDownloadError , strUpdateRetry  , strUpdateCancel );
        }
    };

    private final DfuLogListener mDfuLogListener = new DfuLogListener()
    {
        @Override
        public void onLogEvent(String deviceAddress, int level, String message) {
            android.util.Log.d(TAG , message);
            if(DfuBaseService.LOG_LEVEL_ERROR == level || DfuBaseService.LOG_LEVEL_WARNING == level){
                Log.d(TAG , message);
            }
        }
    };


    @AfterViews
    void Init()
    {
        this.mDBManager = new FitmateDBManager(this);
        this.mUserInfo = this.mDBManager.getUserInfo();

        firmwareUpdateServiceClient.setRootUrl(NetworkClass.baseURL + "api");
        this.displayActionBar();

        Intent intent = getIntent();
        boolean lastestFirmware = intent.getBooleanExtra( FirmwareUpdateActivity.LASTEST_FIRMWARE_KEY , false );
        if(lastestFirmware){
            tvMessage.setText( strFirmwareUnavailable );
        }else{
            tvMessage.setText( strFirmwareAvailable );
        }

        FirmwareVersionInfo deviceVersion = getDeviceFirmwareVersionInfo();
        setVersionText(deviceVersion.getMajorVersion() + "." + deviceVersion.getMinorVersion());

        final Animation twitAnimation = AnimationUtils.loadAnimation(this, R.anim.firmware_update);
        imgArrow.setAnimation(twitAnimation);
        twitAnimation.start();

        mPrefixSerialNumber = mUserInfo.getDeviceAddress().substring(0,4);

        tvFirmwareUpdateRecovery.setText( Html.fromHtml( "<u>" + strFirmwareRecovery + "</u>" ) );

    }


    @Override
    protected void onResume()
    {
        super.onResume();
        DfuServiceListenerHelper.registerProgressListener(this, mDfuProgressListener);
        DfuServiceListenerHelper.registerLogListener(this, mDfuLogListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        DfuServiceListenerHelper.unregisterProgressListener(this, mDfuProgressListener);
        DfuServiceListenerHelper.unregisterLogListener(this, mDfuLogListener);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mBTManager = BTManager.getInstance(this, fitmeterEventCallback);
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();

        mBTManager.clear();
        imgArrow.clearAnimation();
    }

    private void displayActionBar(){
        actionBarView.setBackgroundColor(Color.WHITE);
        TextView tvBarTitle = (TextView) actionBarView.findViewById(R.id.tv_custom_action_bar_title);
        tvBarTitle.setText(this.getString(R.string.firmware_update_title));
        tvBarTitle.setTextColor( Color.BLACK );
        ImageView imgLeft = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_left);

        imgLeft.setImageResource(R.drawable.icon_back_red_selector);
        imgLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ImageView imgRight = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_right_more);
        imgRight.setVisibility(View.GONE);
    }

    private void setVersionText( final String versionText){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tvCurrentVersion.setText( versionText );
            }
        });
    }

    @Background
    void checkServerVersion( )
    {

        try {
            mServerFirmwareVersionInfo = firmwareUpdateServiceClient.getLastVersion( true , mPrefixSerialNumber );
        }catch(RestClientException e){
            Log.e( TAG , "서버 펌웨어를 읽어 오는 중에 오류가 발생하였습니다.");
        }

        FirmwareVersionInfo deviceFirmwareVersion = this.getDeviceFirmwareVersionInfo();
        FirmwareVersionInfo appFirmwareVersion = this.getAppFirmwareInfo();

        if(mServerFirmwareVersionInfo != null)
        {
            //서버와 기기의 버전을 비교한다.
            if (mServerFirmwareVersionInfo.getMajorVersion() <= deviceFirmwareVersion.getMajorVersion()) {
                if (mServerFirmwareVersionInfo.getMinorVersion() <= deviceFirmwareVersion.getMinorVersion()) {
                    dismissProgressDialog();
                    showInfoDialog(strLastestVersion);
                    return;
                }
            }

            //서버와 app이 보유한 버전을 비교한다. 다은로드를 해야 하는지 확인해야함.
            if(mServerFirmwareVersionInfo.getMajorVersion() > appFirmwareVersion.getMajorVersion() ){
                //다운로드 해야함.
                dismissProgressDialog();
                showProgressDialog(strServerDownloading, ProgressDialog.STYLE_HORIZONTAL);
                new DownloadFileFromURL().execute(NetworkClass.firmwareURL, mServerFirmwareVersionInfo.getFileName());

            }else{

                if(mServerFirmwareVersionInfo.getMinorVersion() > appFirmwareVersion.getMinorVersion() ){
                    //다운로드 해야함.
                    dismissProgressDialog();

                    showProgressDialog(strServerDownloading, ProgressDialog.STYLE_HORIZONTAL);
                    new DownloadFileFromURL().execute(NetworkClass.firmwareURL, mServerFirmwareVersionInfo.getFileName());

                }else{
                    //앱이 보유하고 있는 펌웨어 버전이 최신이므로 현재 저장되어 있는 펌웨어 파일로 업데이트를 시작한다.
                    //기기를 찾아야 함.
                    dismissProgressDialog();
                    findUserFitmeter();
                }
            }
        }else{
            dismissProgressDialog();
            this.showConfirmDialog( strNetworkNotAvailable, strUpdateCancel, strUpdateRetry );
        }
    }

    @Click(R.id.btn_activity_firmware_update)
    void firmwaredUpdateClick() {

        if( !Utils.isOnline(this) ) {
            this.showConfirmDialog( strNetworkNotAvailable, strUpdateCancel, strUpdateRetry );
        }else{
            showProgressDialog( strCheckingServerVersion , ProgressDialog.STYLE_SPINNER);
            checkServerVersion();
        }
    }

    @Click(R.id.tv_activity_firmware_update_recovery)
    void showRecoveryActivity(){
        Intent intent = new Intent(this , RecoveryUpdateActivity_.class);
        startActivity(intent);
    }

    private void findUserFitmeter() {
        showProgressDialog( strFindingFitmeter, ProgressDialog.STYLE_SPINNER);
        mBTManager.findFitmeter(mUserInfo.getDeviceAddress());
    }

    private void findReCoveryFitmeter() {
        changeProgressMessage(strFindingRecovery);
        mBTManager.findRecoveryFitmeter(mUserInfo.getDeviceAddress());
    }

    @UiThread
    void showInfoDialog( String message )
    {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setMessage(message);
        alert.setTitle(R.string.firmware_update_title);
        alert.setPositiveButton(this.getString(R.string.common_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        alert.show();
    }

    @UiThread
    void showConfirmDialog( final String message , final String okText , final String cancelText  )
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final AlertDialog.Builder alert_confirm = new AlertDialog.Builder(FirmwareUpdateActivity.this);
                alert_confirm.setTitle(getString(R.string.firmware_update_title));
                alert_confirm.setMessage(message).setCancelable(false).setPositiveButton(okText,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                confirmOk();
                                dialog.dismiss();
                            }
                        }).setNegativeButton(cancelText,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                AlertDialog alert = alert_confirm.create();
                alert.show();
            }
        });
    }


    private void confirmOk( )
    {

        switch( mFirmwareUpdateState )
        {
            case SERVER_DOWNLOAD_COMPLETE:
                findUserFitmeter();
                break;
            case DEVICE_DOWNLOAD_ERROR:
                startFirmwareUpdate();
                break;
            case DEVICE_DOWNLOAD_NOTFOUND:
                findUserFitmeter();
                break;
            case DEVICE_DOWNLOAD_RECOVERY_NOTFOUND:
                findReCoveryFitmeter();
                break;
            case DEVICE_DOWNLOAD_MULTI_RECOVERY:
                findReCoveryFitmeter();
                break;
        }
    }

    private void serverDownloadComplete() {
        dismissProgressDialog();

        setAppFirmwareInfo(mServerFirmwareVersionInfo.getMajorVersion(), mServerFirmwareVersionInfo.getMinorVersion(), mServerFirmwareVersionInfo.getFileName());

        //mFirmwareUpdateState = FirmwareUpdateState.SERVER_DOWNLOAD_COMPLETE;
        //showConfirmDialog(strServerDownloadComplete, strStartUpdate, strUpdateNext);
        findUserFitmeter();
    }

    private void startFirmwareUpdate()
    {
        dismissProgressDialog();

        if(mRecoveryDevice == null ){
            return;
        }

        FirmwareVersionInfo currentFirmwareInfo = this.getAppFirmwareInfo();
        if( currentFirmwareInfo.getFileName() == null )
        {
            //Toast.makeText(this , "펌웨어 파일이 없습니다." , Toast.LENGTH_LONG).show();
            return;
        }

        mFileType = DfuService.TYPE_AUTO;
        mFilePath = currentFirmwareInfo.getFileName();
        mFileStreamUri = null;
        mInitFilePath = null;
        mInitFileStreamUri = null;

        showProgressDialog( strDeviceDownloading , ProgressDialog.STYLE_HORIZONTAL);

        final Intent service = new Intent(this, DfuService.class);
        service.putExtra(DfuService.EXTRA_DEVICE_ADDRESS, mRecoveryDevice.getAddress());
        service.putExtra(DfuService.EXTRA_DEVICE_NAME, mRecoveryDevice.getName() );
        service.putExtra(DfuService.EXTRA_FILE_MIME_TYPE,mFileType == DfuService.TYPE_AUTO ? DfuService.MIME_TYPE_ZIP : DfuService.MIME_TYPE_OCTET_STREAM);
        service.putExtra(DfuService.EXTRA_FILE_TYPE,DfuService.TYPE_AUTO);
        service.putExtra(DfuService.EXTRA_FILE_PATH, mFilePath);
        service.putExtra(DfuService.EXTRA_FILE_URI, mFileStreamUri);
        service.putExtra(DfuService.EXTRA_INIT_FILE_PATH, mInitFilePath);
        service.putExtra(DfuService.EXTRA_INIT_FILE_URI, mInitFileStreamUri);
        startService(service);

    }

    private void checkDeviceDownload()
    {
        //배터리를 확인한다.
        int battery = 0;
        battery = mBTManager.readBatteryRate( mFitmeterType );
        if(battery == -1){
            Log.e(TAG, "배터리 값을 가져오는 중에 오류가 발생하였습니다.");
        }

        if (battery < 50) {
            dismissProgressDialog();
            showInfoDialog( strBatteryLow );
            return;
        }

        //버전을 확인한다.
        FirmwareVersionInfo appFirmwareInfo = this.getAppFirmwareInfo();
        SystemInfo_Response systemInfo = mBTManager.getSystemInfo();
        if(systemInfo != null)
        {

            //버전을 비교한다.
            int deviceMajorVersion = systemInfo.getMajorVersion();
            int deviceMinorVersion = systemInfo.getMinorVersion();

            if (deviceMajorVersion > appFirmwareInfo.getMajorVersion() )
            {
                dismissProgressDialog();
                showInfoDialog( strLastestVersion );
                return;
            } else if (deviceMajorVersion == appFirmwareInfo.getMajorVersion() ) {
                if (deviceMinorVersion >= appFirmwareInfo.getMinorVersion() ) {
                    dismissProgressDialog();
                    showInfoDialog( strLastestVersion );
                    return;
                }
            }
        }else{
            Log.e(TAG, "시스템 정보를 가져오는 중에 오류가 발생하였습니다.");
        }

        //DFU 진입 명령을 전송한다.
        try
        {
            Thread.sleep(2000);
            mBTManager.setBootMode();
            Thread.sleep(2000);
            mBTManager.disconnect();
            mBTManager.close();

        }catch(InterruptedException e){
            e.printStackTrace();
        }

        findReCoveryFitmeter();
    }

    void showProgressDialog( final String message , final int progressStyle )
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                mProgressDialog = new ProgressDialog( FirmwareUpdateActivity.this );
                mProgressDialog.setMessage(message);
                mProgressDialog.setProgressStyle(progressStyle);
                mProgressDialog.setProgress(0);
                mProgressDialog.setMax(100);
                mProgressDialog.show();
            }
        });
    }

    void changeProgressMessage(final String message){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mProgressDialog.setMessage(message);
            }
        });
    }

    void dismissProgressDialog()
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if ( mProgressDialog != null ) {
                    if( mProgressDialog.isShowing() ) {
                        mProgressDialog.dismiss();
                    }
                }
            }
        });
    }

    private FirmwareVersionInfo getDeviceFirmwareVersionInfo(){
        SharedPreferences pref = getSharedPreferences("fitmate", Activity.MODE_PRIVATE);
        FirmwareVersionInfo version = new FirmwareVersionInfo();
        version.setMajorVersion(pref.getInt(FirmwareVersionInfo.DEVICE_MAJORVERSION_KEY, -1));
        version.setMinorVersion(pref.getInt(FirmwareVersionInfo.DEVICE_MINORVERSION_KEY, -1));
        return version;
    }

    private FirmwareVersionInfo getAppFirmwareInfo()
    {
        SharedPreferences pref = getSharedPreferences("fitmate", Activity.MODE_PRIVATE);
        FirmwareVersionInfo version = new FirmwareVersionInfo();
        version.setMajorVersion(pref.getInt(FirmwareVersionInfo.APP_MAJORVERSION_KEY, 0));
        version.setMinorVersion(pref.getInt(FirmwareVersionInfo.APP_MINORVERSION_KEY, 0));
        version.setFileName(pref.getString(FirmwareVersionInfo.APP_DFUFILENAME_KEY, null));
        return version;
    }

    private void setAppFirmwareInfo(int majorVersion, int minorVersion, String fileName)
    {
        SharedPreferences pref = getSharedPreferences("fitmate", Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt( FirmwareVersionInfo.APP_MAJORVERSION_KEY, majorVersion);
        editor.putInt(FirmwareVersionInfo.APP_MINORVERSION_KEY, minorVersion);
        editor.putString(FirmwareVersionInfo.APP_DFUFILENAME_KEY, fileName);
        editor.commit();
    }

    private void setDeviceFirmwareVersionInfo(int majorVersion, int minorVersion )
    {
        SharedPreferences pref = getSharedPreferences("fitmate", Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt( FirmwareVersionInfo.DEVICE_MAJORVERSION_KEY, majorVersion);
        editor.putInt(FirmwareVersionInfo.DEVICE_MINORVERSION_KEY, minorVersion);
        editor.commit();
    }

    private class DownloadFileFromURL extends AsyncTask<String,String,String>
    {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params)
        {
            int count;
            try {
                URL url = new URL(params[0] + params[1]);
                URLConnection connection = url.openConnection();
                connection.connect();

                int lengthOfFile = connection.getContentLength();
                InputStream input = new BufferedInputStream(url.openStream());

                //OutputStream output = new FileOutputStream( filePath + params[1] );
                OutputStream output = openFileOutput( params[1] , Context.MODE_WORLD_READABLE );

                byte data[] = new byte[1024];

                long total =0;

                while( (count = input.read(data)) != -1 )
                {
                    total += count;
                    publishProgress("" + (int) ((total * 100) / lengthOfFile));
                    output.write(data,0,count);
                }

                output.flush();
                output.close();
                input.close();

            }catch(Exception e)
            {
                Log.e("Error: ", e.getMessage());
                dismissProgressDialog();
                showInfoDialog( strServerDownloadError );
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            mProgressDialog.setProgress( Integer.parseInt( values[0] ) );
        }

        @Override
        protected void onPostExecute(String s) {
            serverDownloadComplete();
        }
    }

}
