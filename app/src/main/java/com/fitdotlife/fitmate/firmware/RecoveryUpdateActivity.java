package com.fitdotlife.fitmate.firmware;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.fitdotlife.fitmate.R;
import com.fitdotlife.fitmate.btmanager.BTManager;
import com.fitdotlife.fitmate.btmanager.ConnectionStatus;
import com.fitdotlife.fitmate.btmanager.FitmeterEventCallback;
import com.fitdotlife.fitmate.btmanager.ReceivedDataInfo;
import org.apache.log4j.Log;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.object.BLEDevice;
import com.fitdotlife.fitmate_lib.object.FirmwareVersionInfo;
import com.fitdotlife.fitmate_lib.object.UserInfo;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringRes;

import java.util.List;

import no.nordicsemi.android.dfu.DfuBaseService;
import no.nordicsemi.android.dfu.DfuLogListener;
import no.nordicsemi.android.dfu.DfuProgressListener;
import no.nordicsemi.android.dfu.DfuServiceListenerHelper;

/**
 * Created by Joshua on 2016-07-15.
 */
@EActivity(R.layout.activity_recovery_update)
public class RecoveryUpdateActivity extends Activity{

    private final String TAG = RecoveryUpdateActivity.class.getSimpleName();

    private FitmateDBManager mDBManager = null;
    private UserInfo mUserInfo = null;

    private ProgressDialog mProgressDialog = null;
    private BTManager mBTManager = null;
    private FirmwareUpdateActivity.FirmwareUpdateState mFirmwareUpdateState;

    private BluetoothDevice mRecoveryDevice = null;

    private String mFilePath;
    private Uri mFileStreamUri;
    private String mInitFilePath;
    private Uri mInitFileStreamUri;
    private int mFileType;

    private FirmwareVersionInfo mCurrentVersionInfo = null;

    @ViewById(R.id.ic_activity_recovery_update_actionbar)
    View actionBarView;

    //펌웨어 업데이트 복구 모드 진입 및 복구모드 기기 검색
    @StringRes(R.string.firmware_update_recovery_init)
    String strRecoveryInit;

    @StringRes(R.string.firmware_update_recovery_finding)
    String strFindingRecovery;

    @StringRes(R.string.firmware_update_recovery_none)
    String strRecoveryNone;

    @StringRes(R.string.firmware_update_recovery_multi)
    String strRecoveryMulti;

    //펌웨어 업데이트 진행
    @StringRes(R.string.firmware_update_device_downloading)
    String strDeviceDownloading;

    @StringRes(R.string.firmware_update_device_download_error)
    String strDeviceDownloadError;

    @StringRes(R.string.firmware_update_device_download_complete)
    String strDeviceDownloadComplete;

    @StringRes(R.string.firmware_update_cancel)
    String strUpdateCancel;

    @StringRes(R.string.firmware_update_retry)
    String strUpdateRetry;

    private FitmeterEventCallback fitmeterEventCallback = new FitmeterEventCallback() {
        @Override
        public void onProgressValueChnaged(int progress, int max) {}

        @Override
        public void onReceiveAllFileComplete(List<ReceivedDataInfo> receiveDataArray, boolean isComplete) {}

        @Override
        public void receiveCompleted(int index, int offset, byte[] receivedbytes) {}

        @Override
        public void statusOfBTManagerChanged(ConnectionStatus status) {}

        @Override
        public void findNewDeviceResult(List<BLEDevice> discoveredPeripherals) {}

        @Override
        public void findDeviceResult(String deviceAddress, String serialNumber) {}

        @Override
        public void findRecoveryDeviceResult(final BluetoothDevice recoveryDevice, final List<BluetoothDevice> recoveryFitmeters) {

            new Thread(new Runnable() {
                @Override
                public void run() {

                    if(recoveryDevice != null)
                    {
                        mRecoveryDevice = recoveryDevice;
                        startFirmwareUpdate();

                    }else{

                        if( recoveryFitmeters.size() == 0 )
                        {
                            dismissProgressDialog();
                            mFirmwareUpdateState = FirmwareUpdateActivity.FirmwareUpdateState.DEVICE_DOWNLOAD_RECOVERY_NOTFOUND;
                            showConfirmDialog( strRecoveryNone , strUpdateRetry , strUpdateCancel );

                        }else if( recoveryFitmeters.size() == 1 ){

                            mRecoveryDevice = recoveryFitmeters.get(0);
                            startFirmwareUpdate();

                        }else{

                            dismissProgressDialog();
                            mFirmwareUpdateState = FirmwareUpdateActivity.FirmwareUpdateState.DEVICE_DOWNLOAD_MULTI_RECOVERY;
                            showConfirmDialog( strRecoveryMulti , strUpdateRetry , strUpdateCancel );
                        }
                    }
                }
            }).start();

        }
    };

    private final DfuProgressListener mDfuProgressListener = new DfuProgressListener()
    {
        @Override
        public void onDeviceConnecting(String deviceAddress) {
            mProgressDialog.setIndeterminate(true);
        }

        @Override
        public void onDeviceConnected(String deviceAddress) {
            mProgressDialog.setIndeterminate(true);
        }

        @Override
        public void onDfuProcessStarting(String deviceAddress) {
            mProgressDialog.setIndeterminate(true);
        }

        @Override
        public void onDfuProcessStarted(String deviceAddress) {
            mProgressDialog.setIndeterminate(true);
        }

        @Override
        public void onEnablingDfuMode(String deviceAddress) {
            mProgressDialog.setIndeterminate( true );
        }

        @Override
        public void onProgressChanged(String deviceAddress, int percent, float speed, float avgSpeed, int currentPart, int partsTotal) {
            mProgressDialog.setMessage( strDeviceDownloading );
            mProgressDialog.setIndeterminate( false );
            mProgressDialog.setProgress(percent);
        }

        @Override
        public void onFirmwareValidating(String deviceAddress) {
            mProgressDialog.setIndeterminate(true);
        }

        @Override
        public void onDeviceDisconnecting(String deviceAddress) {}

        @Override
        public void onDeviceDisconnected(String deviceAddress) {}

        @Override
        public void onDfuCompleted(String deviceAddress)
        {
            dismissProgressDialog();
            setDeviceFirmwareVersionInfo(mCurrentVersionInfo.getMajorVersion(), mCurrentVersionInfo.getMinorVersion());
            showInfoDialog( strDeviceDownloadComplete );
        }

        @Override
        public void onDfuAborted(String deviceAddress) {
            dismissProgressDialog();
            mFirmwareUpdateState = FirmwareUpdateActivity.FirmwareUpdateState.DEVICE_DOWNLOAD_ERROR;
            showConfirmDialog(strDeviceDownloadError, strUpdateRetry , strUpdateCancel);
        }

        @Override
        public void onError(String deviceAddress, int error, int errorType, String message) {
            dismissProgressDialog();
            mFirmwareUpdateState = FirmwareUpdateActivity.FirmwareUpdateState.DEVICE_DOWNLOAD_ERROR;
            showConfirmDialog( strDeviceDownloadError , strUpdateRetry , strUpdateCancel );
        }
    };

    private final DfuLogListener mDfuLogListener = new DfuLogListener()
    {
        @Override
        public void onLogEvent(String deviceAddress, int level, String message) {
            android.util.Log.d(TAG , message);
            if(DfuBaseService.LOG_LEVEL_ERROR == level || DfuBaseService.LOG_LEVEL_WARNING == level){
                Log.d(TAG, message);
            }
        }
    };


    @AfterViews
    void Init()
    {
        mDBManager = new FitmateDBManager(this);
        mUserInfo = mDBManager.getUserInfo();

        mBTManager = BTManager.getInstance(this , fitmeterEventCallback );

        displayActionBar();
    }

    private void displayActionBar()
    {
        actionBarView.setBackgroundColor(Color.WHITE);
        TextView tvBarTitle = (TextView) actionBarView.findViewById(R.id.tv_custom_action_bar_title);
        tvBarTitle.setText(this.getString(R.string.firmware_update_reset));
        tvBarTitle.setTextColor( Color.BLACK );
        ImageView imgLeft = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_left);
        imgLeft.setImageResource(R.drawable.icon_back_red_selector);
        imgLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ImageView imgRight = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_right_more);
        imgRight.setVisibility(View.GONE);
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        DfuServiceListenerHelper.registerProgressListener(this, mDfuProgressListener);
        DfuServiceListenerHelper.registerLogListener(this, mDfuLogListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        DfuServiceListenerHelper.unregisterProgressListener(this, mDfuProgressListener);
        DfuServiceListenerHelper.unregisterLogListener(this, mDfuLogListener);
    }

    @Click(R.id.btn_activity_recovery_update)
    void recoveryUpdate()
    {
        findRecoveryFitmeter();
    }

    private void findRecoveryFitmeter(){
        showProgressDialog( strFindingRecovery , ProgressDialog.STYLE_SPINNER);
        mBTManager.findRecoveryFitmeter(mUserInfo.getDeviceAddress());
    }

    void showProgressDialog( final String message , final int progressStyle )
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mProgressDialog = new ProgressDialog(RecoveryUpdateActivity.this);
                mProgressDialog.setMessage(message);
                mProgressDialog.setProgressStyle(progressStyle);
                mProgressDialog.setProgress(0);
                mProgressDialog.setMax(100);
                mProgressDialog.show();
            }
        });
    }

    void changeProgressMessage(final String message)
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mProgressDialog.setMessage(message);
            }
        });
    }


    void dismissProgressDialog()
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if ( mProgressDialog != null ) {
                    if( mProgressDialog.isShowing() ) {
                        mProgressDialog.dismiss();
                    }
                }
            }
        });
    }

    @UiThread
    void showInfoDialog( final String message )
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder alert = new AlertDialog.Builder(RecoveryUpdateActivity.this);
                alert.setMessage(message);
                alert.setTitle(R.string.firmware_update_title);
                alert.setPositiveButton( getString(R.string.common_ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alert.show();
            }
        });
    }

    @UiThread
    void showConfirmDialog( final String message , final String okText , final String cancelText  )
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final AlertDialog.Builder alert_confirm = new AlertDialog.Builder(RecoveryUpdateActivity.this);
                alert_confirm.setTitle( getString(R.string.firmware_update_title));
                alert_confirm.setMessage(message).setCancelable(false).setPositiveButton(okText,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                confirmOk();
                                dialog.dismiss();
                            }
                        }).setNegativeButton(cancelText,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                AlertDialog alert = alert_confirm.create();
                alert.show();
            }
        });
    }

    private void confirmOk( )
    {
        switch( mFirmwareUpdateState )
        {
            case DEVICE_DOWNLOAD_RECOVERY_NOTFOUND:
                findRecoveryFitmeter();
                break;
            case DEVICE_DOWNLOAD_MULTI_RECOVERY:
                findRecoveryFitmeter();
                break;
            case DEVICE_DOWNLOAD_ERROR:
                startFirmwareUpdate();
                break;
        }
    }

    private void startFirmwareUpdate()
    {
        dismissProgressDialog();

        if(mRecoveryDevice == null ){
            return;
        }

        mCurrentVersionInfo = this.getAppFirmwareInfo();
        if( mCurrentVersionInfo.getFileName() == null )
        {
            //Toast.makeText(this, "펌웨어 파일이 없습니다.", Toast.LENGTH_LONG).show();
            return;
        }

        mFileType = DfuService.TYPE_AUTO;
        mFilePath = mCurrentVersionInfo.getFileName();
        mFileStreamUri = null;
        mInitFilePath = null;
        mInitFileStreamUri = null;

        showProgressDialog( strDeviceDownloading , ProgressDialog.STYLE_HORIZONTAL);

        final Intent service = new Intent(this, DfuService.class);
        service.putExtra(DfuService.EXTRA_DEVICE_ADDRESS, mRecoveryDevice.getAddress());
        service.putExtra(DfuService.EXTRA_DEVICE_NAME, mRecoveryDevice.getName() );
        service.putExtra(DfuService.EXTRA_FILE_MIME_TYPE,mFileType == DfuService.TYPE_AUTO ? DfuService.MIME_TYPE_ZIP : DfuService.MIME_TYPE_OCTET_STREAM);
        service.putExtra(DfuService.EXTRA_FILE_TYPE,DfuService.TYPE_AUTO);
        service.putExtra(DfuService.EXTRA_FILE_PATH, mFilePath);
        service.putExtra(DfuService.EXTRA_FILE_URI, mFileStreamUri);
        service.putExtra(DfuService.EXTRA_INIT_FILE_PATH, mInitFilePath);
        service.putExtra(DfuService.EXTRA_INIT_FILE_URI, mInitFileStreamUri);
        startService(service);

    }

    private FirmwareVersionInfo getAppFirmwareInfo()
    {
        SharedPreferences pref = getSharedPreferences("fitmate", Activity.MODE_PRIVATE);
        FirmwareVersionInfo version = new FirmwareVersionInfo();
        version.setMajorVersion(pref.getInt(FirmwareVersionInfo.APP_MAJORVERSION_KEY, 0));
        version.setMinorVersion(pref.getInt(FirmwareVersionInfo.APP_MINORVERSION_KEY, 0));
        version.setFileName( pref.getString( FirmwareVersionInfo.APP_DFUFILENAME_KEY, null ) );
        return version;
    }

    private void setDeviceFirmwareVersionInfo(int majorVersion, int minorVersion )
    {
        SharedPreferences pref = getSharedPreferences("fitmate", Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(FirmwareVersionInfo.DEVICE_MAJORVERSION_KEY, majorVersion);
        editor.putInt(FirmwareVersionInfo.DEVICE_MINORVERSION_KEY, minorVersion);
        editor.commit();
    }
}
