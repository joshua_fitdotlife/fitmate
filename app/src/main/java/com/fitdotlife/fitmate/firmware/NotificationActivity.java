package com.fitdotlife.fitmate.firmware;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

/**
 * Created by Joshua on 2015-11-10.
 */
public class NotificationActivity extends Activity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // If this activity is the root activity of the task, the app is not running
        if (isTaskRoot()) {
            // Start the app before finishing
            final Intent parentIntent = new Intent(this, FirmwareUpdateActivity_.class);
            parentIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            parentIntent.putExtras(getIntent().getExtras());
            parentIntent.putExtra(FirmwareUpdateActivity.EXTRA_OPEN_DFU, true);
            startActivity(parentIntent);
        }

        // Now finish, which will drop the user in to the activity that was at the top of the task stack
        finish();
    }
}
