package com.fitdotlife.fitmate;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fitdotlife.fitmate_lib.customview.FitmeterDialog;
import com.fitdotlife.fitmate_lib.customview.FitmeterProgressDialog;
import com.fitdotlife.fitmate_lib.iview.IFindPasswordView;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.presenter.FindPasswordPresenter;

public class SendPasswordDialog implements IFindPasswordView {
	private Button btnSend = null;
	private Button btnFindAgain = null;
	private ImageView imgClose = null;
	private Context mContext = null;
	private TextView tvCheckUser = null;
	private TextView tvEmail = null;
	private FindPasswordPresenter mPresenter = null;
	private FitmeterProgressDialog mProcessDialog = null;

	private FitmeterDialog mDialog = null;
    public SendPasswordDialog( Context context ){
		this.mContext = context;
		this.mPresenter = new FindPasswordPresenter( this.mContext , this );
	}

	public void show(String name , String email){
		this.mDialog = new FitmeterDialog(this.mContext );

		LayoutInflater inflater = (LayoutInflater) mContext.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
		View contentView = inflater.inflate(R.layout.dialog_send_password, null);

		this.tvEmail = (TextView) contentView.findViewById(R.id.tv_send_password_email);
		this.tvEmail.setText(email);

		this.tvCheckUser = (TextView) contentView.findViewById(R.id.tv_send_password_dialog_check_user);
		String checkUserString = String.format(this.mContext.getString(R.string.find_password_are_you), name);
		this.tvCheckUser.setText(checkUserString);

		this.mDialog.setContentView( contentView );

		View buttonView = inflater.inflate( R.layout.dialog_button_two , null );
		Button btnLeftt = (Button) buttonView.findViewById(R.id.btn_dialog_button_two_left);
		btnLeftt.setText(mContext.getString(R.string.common_refind));
		btnLeftt.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				SeachPasswordDialog seachPasswordDialog = new SeachPasswordDialog(mContext);
				seachPasswordDialog.show();

				mDialog.close();
			}
		});

		Button btnRight = (Button) buttonView.findViewById(R.id.btn_dialog_button_two_right);
		btnRight.setText(mContext.getString(R.string.common_send));
		btnRight.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				sendEmail();
				mDialog.close();
			}
		});
		mDialog.setButtonView(buttonView);
		this.mDialog.show();
	}
	
	private void sendEmail() {
		this.mPresenter.sendPasswordEmail( this.tvEmail.getText().toString() );
	}

	@Override
	public void navigateToSearchDialog(UserInfo userInfo) {

	}

	@Override
	public void showResult(String message) {
		Toast.makeText(this.mContext, message, Toast.LENGTH_LONG).show();
	}

	@Override
	public void dismisDialog() {
		if( this.mDialog != null ){
			this.mDialog.close();
		}
	}

	@Override
	public void startProgressDialog(){
		this.mProcessDialog =  new FitmeterProgressDialog( mContext);

		LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View contentView = inflater.inflate( R.layout.dialog_progress , null );
		TextView tvTitle = (TextView) contentView.findViewById(R.id.tv_dialog_progress_title);
		tvTitle.setVisibility(View.GONE);

		TextView tvContent = (TextView) contentView.findViewById(R.id.tv_dialog_progress_content);
		tvContent.setText( this.mContext.getString(R.string.find_password_send_mail_dialog_content) );
		mProcessDialog.setMessage( this.mContext.getString(R.string.find_password_send_mail_dialog_content) );
		mProcessDialog.setCancelable( false );
		mProcessDialog.show();
	}

	@Override
	public void stopProgressDialog(){
		if( this.mProcessDialog != null ) {
			this.mProcessDialog.dismiss();
		}
	}

}
