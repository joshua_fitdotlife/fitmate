package com.fitdotlife.fitmate;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.WearingLocation;
import com.fitdotlife.fitmate.btmanager.BTManager;
import com.fitdotlife.fitmate.btmanager.ConnectionStatus;
import com.fitdotlife.fitmate.btmanager.FitmeterEventCallback;
import com.fitdotlife.fitmate.btmanager.ReceivedDataInfo;
import com.fitdotlife.fitmate_lib.customview.FitmeterDialog;
import com.fitdotlife.fitmate_lib.customview.FitmeterProgressDialog;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.database.FitmateServiceDBManager;
import com.fitdotlife.fitmate_lib.http.HttpApi;
import com.fitdotlife.fitmate_lib.http.HttpResponse;
import com.fitdotlife.fitmate_lib.iview.IDeviceChangeView;
import com.fitdotlife.fitmate_lib.key.FitmeterType;
import com.fitdotlife.fitmate_lib.object.BLEDevice;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.presenter.DeviceChangePresenter;

import org.apache.log4j.Log;

import java.util.Calendar;
import java.util.List;


public class DeviceChangeActivity extends FragmentActivity implements IDeviceChangeView , WearingLocationFragment.WearingLocationListener
{
    private final int PERMISSION_REQUEST_LOCATION = 0;

    public static final String INTENT_LOCATION = "LOCATION";

    private final String TAG = DeviceChangeActivity.class.getSimpleName();
    private FitmeterType mDeviceType = null;
    private FitmateDBManager mDBManager = null;
    private UserInfo mUserInfo = null;

    private DeviceChangeFragment mDeviceChangeFragment = null;
    private WearingLocationFragment mWearingLocationFragment = null;

    private String mDeviceAddress = null;
    private String mSerialNumber = null;

    private FitmeterProgressDialog mProgressDialog = null;

    private int mRetryCount = 0;
    private BTManager btManager = null;
    private DeviceChangePresenter mPresenter = null;
    private boolean isDeviceChanged = false;
    private FitmeterType mFitmeterType = null;

    private int mStep = 0;
    private int mWearingLocation = WearingLocation.WRIST.Value;

    private DeviceChangeFragment.DeviceRegisterListener fitmeterDeviceRegisterListener = new DeviceChangeFragment.DeviceRegisterListener(){

        @Override
        public void onSelectFitmeterType(FitmeterType fitmeterType)
        {
            mFitmeterType = fitmeterType;
        }

        @Override
        public void onDeviceFound(String deviceAddress, String serialNumber)
        {
            mDeviceAddress = deviceAddress;
            mSerialNumber = serialNumber;

            if(mFitmeterType.equals( FitmeterType.BAND )){
                mWearingLocation = WearingLocation.WRIST.Value;
                showDeleteDataDialog();
            }else if(mFitmeterType.equals( FitmeterType.BLE )){
                moveNext();
            }
        }

        @Override
        public void onMoveBackDeviceRegister() {
            finish();
        }
    };

    private FitmeterEventCallback fitmeterEventCallback = new FitmeterEventCallback() {
        @Override
        public void onProgressValueChnaged(int progress, int max) {

        }

        @Override
        public void onReceiveAllFileComplete(List<ReceivedDataInfo> receiveDataArray, boolean isComplete) {

        }

        @Override
        public void receiveCompleted(int index, int offset, byte[] receivedbytes) {

        }

        @Override
        public void statusOfBTManagerChanged(ConnectionStatus status) {

            switch ( status ){
                case CONNECTED:

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            boolean result = resetFitmeter();
                            if( ! result ){
                                btManager.close();

                                try {
                                    Thread.sleep( 1000 );
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }

                                connectFitmeter();
                            }else{

                                isDeviceChanged = false;
                                btManager.clear();

                                Log.e(TAG, "하드웨어 변경 완료");
                                changeUserInfo();
                            }
                        }
                    }).start();

                    break;

                case DISCONNECTED:
                    if(isDeviceChanged)
                    {
                        try {
                            Thread.sleep( 2000 );
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                connectFitmeter();
                            }
                        });

                    }
                    break;
                case ERROR_DISCONNECTED:

                    if(isDeviceChanged)
                    {
                        try {
                            Thread.sleep( 2000 );
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                connectFitmeter();
                            }
                        });

                    }
                    break;
                case ERROR_DISCONNECTED_133:
                    if(isDeviceChanged)
                    {
                        try {
                            Thread.sleep( 2000 );
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                connectFitmeter();
                            }
                        });

                    }
                    break;
            }
        }

        @Override
        public void findNewDeviceResult(final List<BLEDevice> discoveredPeripherals) {}

        @Override
        public void findDeviceResult(String deviceAddress, String serialNumber) {}

        @Override
        public void findRecoveryDeviceResult(BluetoothDevice recoveryDevice, List<BluetoothDevice> recoveryFitmeters) {}
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_change);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        this.mDBManager = new FitmateDBManager(this);
        this.mUserInfo = mDBManager.getUserInfo();

        Intent intent = getIntent();
        if(intent != null)
        {
            mWearingLocation = intent.getIntExtra( INTENT_LOCATION, -1 );
        }

        this.mDBManager = new FitmateDBManager(this);
        this.btManager = BTManager.getInstance(this , fitmeterEventCallback);
        this.mPresenter = new DeviceChangePresenter(this , this);

        this.mDeviceChangeFragment = DeviceChangeFragment.newInstance();
        this.mDeviceChangeFragment.setDeviceRegisterListener(fitmeterDeviceRegisterListener);

        moveNext();
    }

    private void showDeleteDataDialog(  )
    {
        final FitmeterDialog deleteDialog = new FitmeterDialog(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View contentView = inflater.inflate(R.layout.dialog_default_content, null);
        TextView tvTitle = (TextView) contentView.findViewById(R.id.tv_dialog_default_title);
        tvTitle.setText(this.getString(R.string.myprofile_delete_fitmeter_data_title));

        TextView tvContent = (TextView) contentView.findViewById(R.id.tv_dialog_default_content);
        tvContent.setText(this.getString(R.string.myprofile_delete_fitmeter_data_content));
        deleteDialog.setContentView(contentView);

        View buttonView = inflater.inflate(R.layout.dialog_button_two , null);
        Button btnLeft = (Button) buttonView.findViewById(R.id.btn_dialog_button_two_left);
        btnLeft.setText(this.getString(R.string.common_no));
        btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                deleteDialog.close();
            }
        });

        Button btnRight = (Button) buttonView.findViewById(R.id.btn_dialog_button_two_right);
        btnRight.setText(this.getString(R.string.common_yes));
        btnRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteDialog.close();
                showProgressDialog();
                changeDevice();
            }
        });

        deleteDialog.setButtonView(buttonView);
        deleteDialog.show();
    }

    @Override
    public void finishActivity() {
        this.finish();
    }

    @Override
    public void onBackPressed() {
        if(mStep == 1)
        {
            mDeviceChangeFragment.moveBack();
        }
        else if( mStep == 2 )
        {
            mDeviceChangeFragment = new DeviceChangeFragment().newInstance();
            mDeviceChangeFragment.setDeviceRegisterListener( fitmeterDeviceRegisterListener );
            moveBack();
        }
    }

    @Override
    public void showResultDialog( final boolean isSuccess )
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {


                final FitmeterDialog resultDialog = new FitmeterDialog(DeviceChangeActivity.this);
                LayoutInflater inflater = getLayoutInflater();
                View contentView = inflater.inflate(R.layout.dialog_default_content, null);
                TextView tvTitle = (TextView) contentView.findViewById(R.id.tv_dialog_default_title);
                TextView tvContent = (TextView) contentView.findViewById(R.id.tv_dialog_default_content);

                if (isSuccess) {

                    tvTitle.setText(getString(R.string.myprofile_change_fitmeter_result));
                    tvContent.setText(getString(R.string.myprofile_change_fitmeter_success));

                    View buttonView = inflater.inflate(R.layout.dialog_button_one, null);
                    Button btnOne = (Button) buttonView.findViewById(R.id.btn_dialog_button_one);

                    btnOne.setText(getString(R.string.common_ok));
                    btnOne.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            finishActivity();
                            resultDialog.close();
                        }
                    });

                    resultDialog.setButtonView(buttonView);

                } else {

                    tvTitle.setText(getString(R.string.myprofile_change_fitmeter_result));
                    tvContent.setText(getString(R.string.myprofile_change_fitmeter_fail));

                    View buttonView = inflater.inflate(R.layout.dialog_button_two, null);
                    Button btnLeft = (Button) buttonView.findViewById(R.id.btn_dialog_button_two_left);

                    btnLeft.setText(getString(R.string.common_cancel));
                    btnLeft.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            finishActivity();
                            resultDialog.close();
                        }
                    });

                    Button btnRight = (Button) buttonView.findViewById(R.id.btn_dialog_button_two_right);
                    btnRight.setText(getString(R.string.signin_device_research));
                    btnRight.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {


                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    mRetryCount = 0;
                                    connectFitmeter();
                                }
                            }).start();
                            resultDialog.close();
                        }
                    });
                    resultDialog.setButtonView(buttonView);
                }

                resultDialog.setContentView(contentView);
                resultDialog.show();


            }
        });
    }

    @Override
    public int getWearLocation() {
        return mWearingLocation;
    }


    private void showProgressDialog()
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.e("fitmate", "showProgressDialog()");
                mProgressDialog = new FitmeterProgressDialog(DeviceChangeActivity.this);
                mProgressDialog.setMessage(getString(R.string.myprofile_chaning_fitmeter));
                mProgressDialog.setCancelable(false);
                mProgressDialog.show();

            }
        });
    }

    @Override
    public void dismissProgressDialog(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mProgressDialog != null) {
                    mProgressDialog.dismiss();
                    mProgressDialog = null;
                }
            }
        });
    }

    @Override
    public void setProgressDialogText(final String progressDialogText) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if( mProgressDialog != null ){

                    mProgressDialog.setMessage( progressDialogText );

                }
            }
        });
    }

    public void changeDevice(  )
    {
        isDeviceChanged = true;

        this.btManager = BTManager.getInstance(this  , fitmeterEventCallback );

        Thread changeThread = new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                connectFitmeter();
            }
        });
        changeThread.start();

        //todo : 일단은 장치 변경 후 그냥 끝나는걸로. 추후에 해당 부분의 주석을 해제하고 장치 변경에 맞는 기능을 수행해야됨
        //FitmeterDeviceDriver mDriver = new FitmeterDeviceDriver(this.mContext , bluetoothAdapter , device , this);
        //this.protocolManager = new ProtocolManager( this.mContext , mDriver );
    }

    private void connectFitmeter()
    {
        if( mRetryCount < 10 )
        {
            mRetryCount++;
            Log.d(TAG, "기기 등록 시도 " + mRetryCount + "번째");
            btManager.connectFitmeter(mDeviceAddress );

        }else{

            isDeviceChanged = false;
            btManager.clear();

            Log.e(TAG, "기기 변경을 실패하였습니다.");
            dismissProgressDialog();
            showResultDialog( false );
        }
    }

    private boolean resetFitmeter()
    {
        //현재시긴 설정
        org.apache.log4j.Log.d(TAG, "현재 시간을 설정합니다.");
        if( !btManager.setCurrentTime() ){
            org.apache.log4j.Log.e(TAG, "시간 설정 실패");
        }

        //LED 켜기
        btManager.turnonled( BTManager.LED_COLOR_BLUE );

        try {
            Thread.sleep(500);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }

        //LED 끄기
        btManager.turnoffled();

        //착용위치 설정
        org.apache.log4j.Log.d(TAG, "착용위치를 설정합니다.");
        if( !btManager.setWeringLocation(getWearLocation()) )
        {
            org.apache.log4j.Log.d(TAG, "착용위치 설정 실패");
            return false;
        }

        //이전 데이터를 삭제한다.
        org.apache.log4j.Log.d(TAG, "이전 데이터를 삭제합니다.");
        if( !btManager.deleteAllFile()){
            org.apache.log4j.Log.e(TAG, "이전 데이터 삭제 실패.");
            return false;
        }

        return true;
    }

    private void setLastBatteryCheckTime(long batteryCheckTime ) {
        SharedPreferences pref = this.getSharedPreferences("fitmate", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putLong( NewHomeActivity.BATTERY_CHECK_TIME_KEY, batteryCheckTime);
        editor.commit();
    }


    @Override
    public void onWearingLocationSelected(WearingLocation wearingLocation) {
        this.mWearingLocation = wearingLocation.Value;
        showDeleteDataDialog();
    }

    @Override
    public void onWearingLocationBack() {
        mDeviceChangeFragment = new DeviceChangeFragment().newInstance();
        mDeviceChangeFragment.setDeviceRegisterListener( fitmeterDeviceRegisterListener );
        moveBack();
    }

    public void moveBack()
    {
        mStep -= 1;
        drawChildView();
    }

    public void moveNext()
    {
        mStep += 1;
        drawChildView();
    }

    private void drawChildView()
    {
        if( mStep == 1 )
        {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fl_activity_device_change, mDeviceChangeFragment );
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }

        if( mStep == 2 )
        {
            this.mWearingLocationFragment = WearingLocationFragment.newInstance( null );
            this.mWearingLocationFragment.setWearingLocationSelectionListener(this);

            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace( R.id.fl_activity_device_change , mWearingLocationFragment );
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }
    }

    private void changeUserInfo()
    {
        String deviceAddress = "";
        if (mSerialNumber != null) {
            deviceAddress = mSerialNumber;
        } else {
            deviceAddress = mDeviceAddress;
        }

        final String finalDeviceAddress = deviceAddress;
        HttpApi.chageUserInfo( mUserInfo.getEmail() , deviceAddress, mWearingLocation, new HttpResponse() {
            @Override
            public void onResponse(int resultCode, String response) {

                dismissProgressDialog();
                if( resultCode == 200 )
                {
                    FitmateServiceDBManager serviceDBManager = new FitmateServiceDBManager(DeviceChangeActivity.this);
                    serviceDBManager.deleteAllFiles();

                    mDBManager.deleteRecentConnectionInfo();
                    setLastBatteryCheckTime(0);

                    Calendar todayCalendar = Calendar.getInstance();
                    String todayDate = todayCalendar.get(Calendar.YEAR) + "-" + String.format("%02d", todayCalendar.get(Calendar.MONTH) + 1) + "-" + String.format("%02d", todayCalendar.get(Calendar.DAY_OF_MONTH));
                    mDBManager.setNeedMergeDate(todayDate);

                    mUserInfo.setDeviceAddress(finalDeviceAddress);
                    mUserInfo.setWearAt(mWearingLocation);
                    mDBManager.setUserInfo( mUserInfo );

                    showResultDialog(true);
                }else{
                    showResultDialog( false );
                }
            }
        });
    }

}
