package com.fitdotlife.fitmate;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.PermissionChecker;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fitdotlife.fitmate_lib.key.FitmeterType;

/**
 * Created by Joshua on 2016-07-27.
 */
public class DeviceRegisterFragment extends Fragment implements View.OnClickListener , FitmeterFindFragment.FitmeterFoundListener , SelectDeviceFragment.SelectDeviceListener{

    public static final int ENABLE_BT_REQUEST_ID = 1;
    private final int PERMISSION_REQUEST_LOCATION = 0;

    private FitmeterType mDeviceType = null;

    private int mStep = 0;
    private View mActionBarView = null;
    private TextView tvTitle = null;
    private ImageView imgBack = null;

    private FrameLayout flDeviceRegister = null;
    private Button btnNext = null;

    private SelectDeviceFragment mSelectDeviceFragment = null;
    private FitmeterContentFragment mFitmeterContentFragment = null;
    private PowerOnFragment mPowerOnFragment = null;
    private PowerCheckFragment mPowerCheckFragment = null;
    private FitmeterConnectFragment mFitmeterConnectFragment = null;
    private FitmeterFindFragment mFitmeterFindFragment = null;

    private DeviceRegisterListener mListener = null;

    @Override
    public void onDeviceFound(String deviceAddress , String serialNumber) {
        mListener.onDeviceFound(deviceAddress , serialNumber);
    }

    public interface DeviceRegisterListener{
        void onSelectFitmeterType(FitmeterType fitmeterType);
        void onDeviceFound(String deviceAddress , String serialNumber );
        void onMoveBackDeviceRegister();
    }

    public static DeviceRegisterFragment newInstance()
    {
        Bundle args = new Bundle();
        DeviceRegisterFragment fragment = new DeviceRegisterFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.mSelectDeviceFragment = SelectDeviceFragment.newInstance();
        this.mSelectDeviceFragment.setSelectDeviceListener( this );
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate( R.layout.fragment_deviceregister , container ,  false );

        mActionBarView = rootView.findViewById(R.id.ic_fragment_deviceregister_actionbar);
        this.imgBack = (ImageView) rootView.findViewById(R.id.img_custom_actionbar_white_left);
        this.imgBack.setBackgroundResource(R.drawable.icon_back_red_selector);
        this.imgBack.setOnClickListener(this);

        this.tvTitle = (TextView) rootView.findViewById(R.id.tv_custom_actionbar_white_title);

        this.btnNext = (Button) rootView.findViewById(R.id.btn_fragment_deviceregister_next);
        this.btnNext.setOnClickListener(this);

        moveNext();

        return rootView;
    }

    private void moveNext()
    {

        if( mStep == 5 )
        {
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M )
            {
                if( PermissionChecker.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED )
                {
                    this.requestPermissions( new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_LOCATION );
                    return;
                }
            }

            if (!isBtEnabled()) { //블루투스가 켜져 있지 않다면
                startBluetoothActivityRequest();
                return;
            }
        }

        mStep += 1;
        drawChildView();
    }

    public void moveBack()
    {
        mStep -= 1;
        drawChildView();
    }

    public void moveSelectDeviceType(){

        for( int i = 0 ; i < getChildFragmentManager().getBackStackEntryCount() ;i++ ) {
            getChildFragmentManager().popBackStackImmediate();
        }

        mStep = 1;
        drawChildView();
    }


    private void drawChildView()
    {
        //타이틀 변경
        if(mStep == 0){
            mListener.onMoveBackDeviceRegister();
        }else if( mStep == 1 ){
            mActionBarView.setVisibility(View.VISIBLE);
            tvTitle.setText(getContext().getResources().getString(R.string.signin_fitmeter_type));
            this.btnNext.setVisibility(View.GONE);

            FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fl_fragment_deviceregister, mSelectDeviceFragment);
            fragmentTransaction.commit();

        }else if( mStep == 2 ){
            mActionBarView.setVisibility(View.VISIBLE);
            tvTitle.setText( getContext().getResources().getString( R.string.signin_fitmeter_content ) );
            this.mFitmeterContentFragment = FitmeterContentFragment.newInstance( mDeviceType );
            this.btnNext.setVisibility(View.VISIBLE);

            FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fl_fragment_deviceregister, mFitmeterContentFragment);
            fragmentTransaction.commit();

        }else if( mStep == 3){
            mActionBarView.setVisibility(View.VISIBLE);
            tvTitle.setText(getContext().getResources().getString( R.string.signin_power_check ));
            mPowerCheckFragment = PowerCheckFragment.newInstance(mDeviceType);
            btnNext.setVisibility(View.VISIBLE);

            FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fl_fragment_deviceregister, mPowerCheckFragment);
            fragmentTransaction.commit();

        }else if(mStep == 4){
            mActionBarView.setVisibility(View.VISIBLE);
            tvTitle.setText( getContext().getResources().getString( R.string.signin_poweron ) );
            mPowerOnFragment = PowerOnFragment.newInstance(mDeviceType);
            this.btnNext.setVisibility(View.VISIBLE);

            FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fl_fragment_deviceregister, mPowerOnFragment);
            fragmentTransaction.commit();

        }else if(mStep == 5){

            mActionBarView.setVisibility(View.VISIBLE);
            tvTitle.setText(this.getResources().getString(R.string.signin_connect_fitmeter));
            mFitmeterConnectFragment = FitmeterConnectFragment.newInstance(mDeviceType);
            btnNext.setVisibility(View.VISIBLE);

            FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fl_fragment_deviceregister, mFitmeterConnectFragment);
            fragmentTransaction.commit();

        }else if(mStep == 6)
        {
            mActionBarView.setVisibility(View.GONE);
            btnNext.setVisibility(View.GONE);

            this.mFitmeterFindFragment = FitmeterFindFragment.newInstance( this.mDeviceType );
            this.mFitmeterFindFragment.setDeviceFoundListener(this);

            FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fl_fragment_deviceregister, mFitmeterFindFragment);
            fragmentTransaction.commitAllowingStateLoss();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.img_custom_actionbar_white_left:
                moveBack();
                break;
            case R.id.btn_fragment_deviceregister_next:
                moveNext();
                break;
        }
    }

    public void selectDeviceType( FitmeterType fitmeterType ){
        mDeviceType = fitmeterType;
        mListener.onSelectFitmeterType( fitmeterType );
        moveNext();
    }

    public void setDeviceRegisterListener( DeviceRegisterListener listener){
        this.mListener = listener;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == ENABLE_BT_REQUEST_ID){
            if( resultCode == Activity.RESULT_OK )
            {
                mStep += 1;
                drawChildView();
            }
            else if(resultCode == Activity.RESULT_CANCELED)
            {
                Toast.makeText(getActivity(), getString(R.string.signin_fitmeter_bluetooth_on), Toast.LENGTH_LONG).show();
            }
        }
    }

    private boolean isBtEnabled()
    {
        final BluetoothManager manager = (BluetoothManager) getActivity().getSystemService(Context.BLUETOOTH_SERVICE);
        if (manager == null) return false;

        final BluetoothAdapter adapter = manager.getAdapter();
        if (adapter == null) return false;
        return adapter.isEnabled();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if( requestCode == PERMISSION_REQUEST_LOCATION )
        {
            if( grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED )
            {
                if (isBtEnabled())
                {
                    mStep += 1;
                    drawChildView();
                }else{
                    startBluetoothActivityRequest();
                }

            }else{

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage( this.getResources().getString( R.string.home_location_permission_reject ) )
                        .setCancelable(true)        // 뒤로 버튼 클릭시 취소 가능 설정
                        .setNeutralButton(R.string.common_ok, new DialogInterface.OnClickListener() {
                            // 확인 버튼 클릭시 설정
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.cancel();
                            }
                        });
                AlertDialog dialog = builder.create();    // 알림창 객체 생성
                dialog.show();    // 알림창 띄우기
            }
        }
    }

    private void startBluetoothActivityRequest()
    {
        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        this.startActivityForResult(enableBtIntent, this.ENABLE_BT_REQUEST_ID);
    }
}
