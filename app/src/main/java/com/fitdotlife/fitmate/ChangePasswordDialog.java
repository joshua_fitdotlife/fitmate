package com.fitdotlife.fitmate;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.fitdotlife.fitmate_lib.customview.FitmeterDialog;
import com.fitdotlife.fitmate_lib.customview.FitmeterProgressDialog;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.http.HttpApi;
import com.fitdotlife.fitmate_lib.http.HttpResponse;
import com.fitdotlife.fitmate_lib.http.NetworkClass;
import com.fitdotlife.fitmate_lib.key.CommonKey;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.presenter.FindPasswordPresenter;
import com.fitdotlife.fitmate_lib.util.Utils;
import com.fitdotlife.fitmate.model.UserInfoModel;
import com.fitdotlife.fitmate.model.UserInfoModelResultListener;

import org.apache.log4j.Log;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class ChangePasswordDialog
{
	private EditText etxSearchContent = null;
	private EditText etxNewPassword = null;
	private EditText etxNewPasswordMore = null;
	private Context mContext = null;
	private FitmeterDialog mDialog = null;
	private String email = "";
	private FitmateDBManager dbManager = null;
	private PopupWindow errorPopup = null;

	private Button btnCancel = null;
	private Button btnChange = null;
	private TextView tvTitle = null;

	final static String TAG = " ChangePasswordDiaglog";

	public ChangePasswordDialog(Context context, String email)
	{
		this.mContext = context;
		this.email = email;

		this.mModel = new UserInfoModel(mContext, new UserInfoModelResultListener() {
			@Override
			public void onSuccess(int code) {
				//성공 mtvCurrentpasswordError.setVisibility(View.INVISIBLE);
				btnChange.setEnabled(true);
			}

			@Override
			public void onSuccess(int code, boolean result) {

			}

			@Override
			public void onSuccess(int code, UserInfo userInfo)
			{
				Log.d(TAG, "서버 로그인 성공");
				if( code == UserInfoModel.LOGIN_RESPONSE_CODE ) {
					//로컬 DB에 서버의 사용자 정보를 저장한다.

				}

				//성공 mtvCurrentpasswordError.setVisibility(View.INVISIBLE);
				btnChange.setEnabled(true);
			}

			@Override
			public void onFail(int code) {
				//불일치 mtvCurrentpasswordError.setVisibility(View.VISIBLE);
				showPopup( etxSearchContent , mContext.getResources().getString( R.string.myprofile_different_current_password )  );
				etxSearchContent.requestFocus();
			}

			@Override
			public void onErrorOccured(int code, String message) {
				//불일치 mtvCurrentpasswordError.setVisibility(View.VISIBLE);
				showPopup( etxSearchContent , mContext.getResources().getString( R.string.myprofile_different_current_password )  );
				etxSearchContent.requestFocus();
			}

		});

		dbManager = new FitmateDBManager(mContext);
		//this.mPresenter = new FindPasswordPresenter( this.mContext , this );
	}

	private UserInfoModel mModel = null;

	public void show()
	{
		this.mDialog = new FitmeterDialog(this.mContext);
		LayoutInflater inflater = (LayoutInflater) mContext.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
		View contentView = inflater.inflate( R.layout.dialog_change_password , null );

		this.tvTitle = (TextView) contentView.findViewById( R.id.tv_dialog_chnage_password_title );
		this.etxSearchContent = (EditText) contentView.findViewById(R.id.etx_dialog_change_password_current_password);
		this.etxNewPassword = (EditText) contentView.findViewById(R.id.editText);
		this.etxNewPasswordMore = (EditText) contentView.findViewById(R.id.editText2);

		etxSearchContent.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {
					//todo focusout 되면 비밀번호 확인하기 구현 필요

					String mEncodePassword = Utils.encodePassword(etxSearchContent.getText().toString());
					mModel.serverLogin(email, mEncodePassword);
				}
			}
		});

		this.mDialog.setContentView( contentView );

		View buttonView = inflater.inflate( R.layout.dialog_button_two , null);
		btnCancel = (Button) buttonView.findViewById(R.id.btn_dialog_button_two_left);
		btnCancel.setText(mContext.getString(R.string.common_cancel));
		btnCancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mDialog.close();
			}
		});
		btnChange = (Button) buttonView.findViewById(R.id.btn_dialog_button_two_right);
		btnChange.setEnabled(false);
		btnChange.setText(mContext.getString(R.string.myprofile_password_change));
		btnChange.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (validInput()) {
					startProgressDialog();

					String currentPw = Utils.encodePassword(etxSearchContent.getText().toString());
					String newPw = Utils.encodePassword(etxNewPassword.getText().toString());
					mEncodePw = newPw;

					try {

						HttpApi.changePassword(email, currentPw, newPw, new HttpResponse() {
							@Override
							public void onResponse(int resultCode, String response) {
								if(resultCode == 0){

									ConfirmBox(
											BoxType.Retry,
											mContext.getResources().getString(R.string.myprofile_dialog_change_title),
											mContext.getResources().getString(R.string.myprofile_dialog_change_fail) + " : " + mContext.getResources().getString(R.string.myprofile_dialog_change_fail_internet) ,
											new DialogInterface.OnClickListener() {
												@Override
												public void onClick(DialogInterface dialog, int which) {

												}
											},
											new DialogInterface.OnClickListener() {
												@Override
												public void onClick(DialogInterface dialog, int which) {
													CloseDialog();
												}
											});

									stopProgressDialog();

								}else{


									try {
										String sss = response.toString();

										JSONObject jsonObject = new JSONObject( sss );
										String result = jsonObject.getString(CommonKey.RESPONSE_VALUE_KEY);

										if( result.equals( CommonKey.SUCCESS ) ){
											//네트워크 버전

											//ShowMessage("변경을 성공하였습니다.");

											ConfirmBox(
													BoxType.Ok,
													mContext.getResources().getString(R.string.myprofile_dialog_change_title),
													mContext.getResources().getString(R.string.myprofile_dialog_change_success),
													new DialogInterface.OnClickListener() {
														@Override
														public void onClick(DialogInterface dialog, int which) {

														}
													},
													new DialogInterface.OnClickListener() {
														@Override
														public void onClick(DialogInterface dialog, int which) {
															CloseDialog();
														}
													});

											// todo 로컬 디비 내용 변경시키는거 구현 필요
											//mModel.setPassword(email, mEncodePw)
											dbManager.setPassword(email, mEncodePw);
											CloseDialog();

										}else if(result.equals(CommonKey.FAIL)){

											String reason  = jsonObject.getString(CommonKey.COMMENT_VALUE_KEY);

											throw new Exception(reason);
										}
									} catch (Exception e) {
										//ShowMessage("변경 실패 : " + e.getMessage());

										ConfirmBox(
												BoxType.Retry,
												mContext.getResources().getString(R.string.myprofile_dialog_change_title),
												mContext.getResources().getString(R.string.myprofile_dialog_change_fail) + " : " + e.getMessage(),
												new DialogInterface.OnClickListener() {
													@Override
													public void onClick(DialogInterface dialog, int which) {

													}
												},
												new DialogInterface.OnClickListener() {
													@Override
													public void onClick(DialogInterface dialog, int which) {
														CloseDialog();
													}
												});
										e.printStackTrace();
									}

									stopProgressDialog();

								}
							}
						});


					} catch (IOException e) {

						showPopup( tvTitle , mContext.getResources().getString(R.string.myprofile_dialog_change_fail_internet) );

//						ConfirmBox(
//								BoxType.Retry,
//								mContext.getResources().getString(R.string.myprofile_dialog_change_title),
//								mContext.getResources().getString(R.string.myprofile_dialog_change_fail) + " : " + mContext.getResources().getString(R.string.myprofile_dialog_change_fail_internet),
//								new DialogInterface.OnClickListener() {
//									@Override
//									public void onClick(DialogInterface dialog, int which) {
//
//									}
//								},
//								new DialogInterface.OnClickListener() {
//									@Override
//									public void onClick(DialogInterface dialog, int which) {
//										CloseDialog();
//									}
//								});
//						e.printStackTrace();
					}

				}
			}
		});
		mDialog.setButtonView(buttonView);
		this.mDialog.show();
	}

	void CloseDialog()
	{
		mDialog.close();
	}
	String mEncodePw = "";

	enum BoxType{
		YesNo,
		Retry,
		Ok
	}

	void ConfirmBox(final BoxType bt,final String title, final String content, final DialogInterface.OnClickListener okCallback, final DialogInterface.OnClickListener cancleCallback)
	{

		Handler mHandler = new Handler(Looper.getMainLooper());
		mHandler.postDelayed(new Runnable() {
			@Override
			public void run() {
				// 내용


				switch (bt)
				{
					case YesNo:

						new AlertDialog.Builder(mContext)
								.setTitle(title)
								.setMessage(content)
								.setIcon(android.R.drawable.ic_dialog_alert)
								.setPositiveButton(android.R.string.yes, okCallback).show();

						break;
					case Retry:

						new AlertDialog.Builder(mContext)
								.setTitle(title)
								.setMessage(content)
								.setIcon(android.R.drawable.ic_dialog_alert)
								.setPositiveButton(R.string.myprofile_dialog_button_retry, okCallback)
								.setNegativeButton(R.string.myprofile_dialog_button_cancel, cancleCallback).show();


						break;
					case Ok:

						new AlertDialog.Builder(mContext)
								.setTitle(title)
								.setMessage(content)
								.setIcon(android.R.drawable.ic_dialog_alert)
								.setPositiveButton(android.R.string.yes, okCallback).show();
						break;
				}
			}
		}, 0);

	}

	private boolean validInput()
	{
		if( this.etxSearchContent.getText().length() < 4 )
		{
			//this.showMessage( this.mContext.getString(R.string.myprofile_change_password_length));
			this.showPopup( etxSearchContent , this.mContext.getString(R.string.myprofile_change_password_length) );
			this.etxSearchContent.requestFocus();
			return false;
		}

		if( this.etxNewPassword.getText().length() < 4 )
		{
			//this.showMessage( this.mContext.getString(R.string.myprofile_change_password_length));
			this.showPopup( etxNewPassword , this.mContext.getString(R.string.myprofile_change_password_length) );
			this.etxNewPassword.requestFocus();
			return false;
		}

		if( this.etxNewPasswordMore.getText().length() < 4 )
		{
			this.showPopup( etxNewPasswordMore , this.mContext.getString(R.string.myprofile_change_password_length) );
			this.etxNewPasswordMore.requestFocus();
			return false;
		}

		if(!etxNewPassword.getText().toString().equals(etxNewPasswordMore.getText().toString()))
		{
			this.showPopup(etxNewPasswordMore, this.mContext.getString(R.string.myprofile_dialog_change_newpassworng));
			this.etxNewPasswordMore.requestFocus();

			return false;
		}

		return true;
	}


	private FitmeterProgressDialog mProcessDialog = null;

	//@Override
	public void startProgressDialog(){
		this.mProcessDialog =  new FitmeterProgressDialog( this.mContext );
		mProcessDialog.setMessage("비밀번호 전송중입니다...");
		mProcessDialog.setCancelable( false );
		mProcessDialog.show();
	}

	//@Override
	public void stopProgressDialog(){
		if( this.mProcessDialog != null ) {
			this.mProcessDialog.dismiss();
		}
	}

	//@Override
	public void showResult(String message)
	{
		this.showMessage(message);
	}

	private void showMessage( String message )
	{
		Toast.makeText(this.mContext, message, Toast.LENGTH_LONG).show();
	}

	private void showPopup( View view , String errorMessage )
	{
		LayoutInflater inflater = (LayoutInflater) mContext.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
		View errorPopView = inflater.inflate(R.layout.popup_signin_error, null);
		TextView tvMessage = (TextView) errorPopView.findViewById( R.id.tv_popup_signin_error_message );
		tvMessage.setText( errorMessage );

		errorPopup = new PopupWindow( errorPopView , WindowManager.LayoutParams.WRAP_CONTENT , WindowManager.LayoutParams.WRAP_CONTENT , true);
		errorPopup.setOutsideTouchable(true);
		errorPopup.setBackgroundDrawable(new BitmapDrawable());
		errorPopup.setTouchable(true);
		errorPopup.setTouchInterceptor(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View view, MotionEvent event) {

				errorPopup.dismiss();
				errorPopup = null;
				return false;

			}
		});

		int yOffset = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 2.6f, mContext.getResources().getDisplayMetrics());
		errorPopup.setAnimationStyle(-1);
		errorPopup.showAsDropDown(view, 0, -yOffset);
	}
}