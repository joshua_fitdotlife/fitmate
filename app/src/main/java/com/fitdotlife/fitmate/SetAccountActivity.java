package com.fitdotlife.fitmate;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.PermissionChecker;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.UnderlineSpan;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;

import com.fitdotlife.fitmate_lib.customview.FitmeterProgressDialog;
import com.fitdotlife.fitmate_lib.customview.RoundedImageView;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.image.ImageUtil;
import com.fitdotlife.fitmate_lib.iview.ISetUserInfoView;
import com.fitdotlife.fitmate_lib.util.Utils;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.presenter.SetUserInfoPresenter;

import org.apache.log4j.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class SetAccountActivity extends Activity implements OnClickListener , ISetUserInfoView, View.OnFocusChangeListener, CompoundButton.OnCheckedChangeListener
{
    private String TAG = "fitmate";

    private int PERMISSION_REQUEST_LOCATION = 0;
    private final int PERMISSION_REQUEST_STORAGE = 3;

    protected static final int CAMERA_REQUEST = 2;

    public static final int EMAIL_CODE = 1;
    private UserInfo mUserInfo = null;

    private PopupWindow errorPopup = null;

    private View actionBarView;


    //회원정보
    private EditText etxEmail = null;
    private EditText etxPassword = null;
    private TextView tvServiceAgreement = null;
    private boolean isServiceAgree = false;
    private boolean mCheckDuplicateEmail = false;
    private Button btnAccount = null;
    private SetUserInfoPresenter mPresenter = null;
    private InputMethodManager inputMethodManager = null;
    private LinearLayout llServiceAgreement = null;
    private ImageView ivServiceAgreement = null;

    private FitmeterProgressDialog mDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_set_account);

        this.mPresenter = new SetUserInfoPresenter( this , this );
        this.mUserInfo = new UserInfo();
        FitmateDBManager mDBManager = new FitmateDBManager( this.getApplicationContext() );
        this.inputMethodManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);

        this.actionBarView = this.findViewById(R.id.ic_activity_set_account_actionbar);
        displayActionBar();

        this.btnAccount = (Button) this.findViewById(R.id.btn_child_set_account);
        this.btnAccount.setOnClickListener(this);

        etxEmail = (EditText) this.findViewById(R.id.etx_child_set_account_email);
        etxEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                mCheckDuplicateEmail = false;
                if (etxEmail.getText().toString().length() > 0 && etxPassword.getText().toString().length() > 0) {
                    btnAccount.setBackground(getResources().getDrawable(R.drawable.login_btn_selector));
                } else {
                    btnAccount.setBackground(getResources().getDrawable(R.drawable.login_btn));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        etxEmail.setOnFocusChangeListener(this);


        etxPassword = (EditText) this.findViewById(R.id.etx_child_set_account_password);
        etxPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(etxEmail.getText().toString().length() > 0 && etxPassword.getText().toString().length() > 0){
                    btnAccount.setBackground( getResources().getDrawable(R.drawable.login_btn_selector) );
                }else{
                    btnAccount.setBackground( getResources().getDrawable(R.drawable.login_btn ) );
                }
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        this.ivServiceAgreement = (ImageView) this.findViewById(R.id.iv_activity_set_account_service_agreement);
        this.llServiceAgreement = (LinearLayout) this.findViewById(R.id.ll_activity_set_account_service_agreement);
        this.llServiceAgreement.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isServiceAgree)
                {
                    isServiceAgree = true;
                    ivServiceAgreement.setImageResource(R.drawable.checkbox_sel);
                }
                else
                {
                    isServiceAgree = false;
                    ivServiceAgreement.setImageResource(R.drawable.checkbox);
                }
            }
        });

        this.tvServiceAgreement = (TextView) this.findViewById(R.id.tv_child_set_account_service_agreement);
        SpannableString content = new SpannableString( getResources().getString( R.string.signin_account_agree_term ) );
        content.setSpan( new UnderlineSpan(), 0 , content.length(), 0);
        tvServiceAgreement.setText( content );
        this.tvServiceAgreement.setOnClickListener(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            int checkLocation = PermissionChecker.checkSelfPermission(SetAccountActivity.this, Manifest.permission.GET_ACCOUNTS);

            if (checkLocation == PackageManager.PERMISSION_GRANTED) {
                getEmail();
            }
            else{
                ActivityCompat.requestPermissions( SetAccountActivity.this , new String[]{ Manifest.permission.GET_ACCOUNTS } , PERMISSION_REQUEST_LOCATION );
            }
        }

        this.inputMethodManager.hideSoftInputFromWindow(this.etxEmail.getWindowToken(), 0);
    }

    private void displayActionBar()
    {
        TextView tvBarTitle = (TextView) actionBarView.findViewById( R.id.tv_custom_actionbar_white_title );
        tvBarTitle.setText(this.getResources().getString(R.string.signin_fitmeter));

        ImageView imgLeft = (ImageView) actionBarView.findViewById(R.id.img_custom_actionbar_white_left);
        imgLeft.setBackgroundResource(R.drawable.icon_back_red_selector);
        imgLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ImageView imgRight = (ImageView) actionBarView.findViewById(R.id.img_custom_actionbar_white_right);
        imgRight.setVisibility(View.GONE);
    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

    }

    @Override
    public void onClick(View view)
    {
        switch( view.getId() )
        {
            case R.id.btn_child_set_account:
                //this.inputMethodManager.hideSoftInputFromWindow(this.etxEmail.getWindowToken() , 0);

                if( this.validCheckThenSave() ){

                    //기본 자기소개를 넣는다.
                    int introResID =  this.getResources().getIdentifier( "signin_default_introduction" , "string", this.getPackageName() );
                    mUserInfo.setIntro( this.getResources().getString( introResID ) );
                    this.mPresenter.setUserAccount(mUserInfo);
                }
                break;
            case R.id.tv_child_set_account_service_agreement:
                Intent serviceAgreementIntent = new Intent( SetAccountActivity.this , ServiceAgreementActivity.class );
                this.startActivity( serviceAgreementIntent );
                break;
        }
    }

    private boolean validCheckThenSave()
    {

        if( this.etxEmail.getText().length() == 0 )
        {
            //Toast.makeText(this, this.getResources().getString(  R.string.signin_email_input ) , Toast.LENGTH_LONG ).show();
            showPopup( etxEmail , this.getResources().getString(  R.string.signin_email_input )  );
            this.etxEmail.requestFocus();
            return false;
        }

        String emailPattern ="^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        String email = this.etxEmail.getText().toString();
        if( !email.matches(emailPattern) ) {
            //Toast.makeText(this, this.getString(R.string.signin_email_different_formats), Toast.LENGTH_LONG).show();
            showPopup( etxEmail , this.getString(R.string.signin_email_different_formats) );
            this.etxEmail.requestFocus();
            return false;
        }

        if( !this.mCheckDuplicateEmail ){
            mPresenter.ismUsableEmail( this.etxEmail.getText().toString() );
            return false;
        }

        if( this.etxPassword.getText().length() == 0 ){
            //Toast.makeText(this, this.getResources().getString(  R.string.signin_account_input_password ) , Toast.LENGTH_LONG ).show();
            showPopup( etxPassword , this.getResources().getString(  R.string.signin_account_input_password ) );
            this.etxPassword.requestFocus();
            return false;
        }

        if( this.etxPassword.getText().length() < 4 )
        {
            showPopup( etxPassword , this.getResources().getString( R.string.signin_account_input_password_fourcharacter ) );
            this.etxPassword.requestFocus();
            return false;
        }

        if(!isServiceAgree)
        {
            showPopup(tvServiceAgreement , getResources().getString(R.string.signin_account_let_agree_term));
            return false;
        }

        String[] splitedEmail = this.etxEmail.getText().toString().split( "@" );
        this.mUserInfo.setName( splitedEmail[0] );
        this.mUserInfo.setEmail(this.etxEmail.getText().toString());
        this.mUserInfo.setPassword(Utils.encodePassword(this.etxPassword.getText().toString()));

        return true;
    }


    @Override
    public void moveHomeActivity() {
        this.finish();

        Intent intent = new Intent(SetAccountActivity.this, NewHomeAsActivity_.class);
        this.startActivity(intent);

        Intent tutorialIntent = new Intent( SetAccountActivity.this , NewHomeTutorialActivity_.class );
        this.startActivity(tutorialIntent);
    }

    @Override
    public void showResult(String message) {

        Toast.makeText(this,message,Toast.LENGTH_LONG).show();

    }

    @Override
    public void showPopup(int code, String message) {

        switch( code ){
            case EMAIL_CODE:
                showPopup(etxEmail, message);
                break;
        }
    }

    @Override
    public void finishActivity() {
        this.finish();
    }

    public void startProgressDialog( String title , String message ){
        this.mDialog =  new FitmeterProgressDialog(this);
        mDialog.setMessage( message );
        mDialog.setCancelable( false );
        mDialog.show();
    }

    public void stopProgressDialog(){
        if( this.mDialog != null ) {
            this.mDialog.dismiss();
        }
    }

    @Override
    public void setDuplicateResult(boolean duplicateResult) {
        this.mCheckDuplicateEmail = duplicateResult;
    }

    @Override
    public void onFocusChange(View view, boolean hasFocus) {

        if( view.getId() == R.id.etx_child_set_account_email  )
        {
            if( !hasFocus ){

                String emailPattern ="^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
                String email = this.etxEmail.getText().toString();
                if( !email.matches(emailPattern) ) {
                    //Toast.makeText(this, this.getString(R.string.signin_email_different_formats) , Toast.LENGTH_LONG).show();
                    showPopup( etxEmail , this.getString(R.string.signin_email_different_formats) );
                    return;
                }

                this.mPresenter.ismUsableEmail(this.etxEmail.getText().toString());
            }
        }
    }

    private void getEmail(){

        AccountManager manager = AccountManager.get(this);
        Account[]  accounts = manager.getAccounts();
        if( accounts.length > 0 ) {
            etxEmail.setText(accounts[0].name);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if( requestCode == PERMISSION_REQUEST_LOCATION ){

            if( grantResults[0] == PackageManager.PERMISSION_GRANTED ){
                getEmail();
            }
        }

        if( requestCode == PERMISSION_REQUEST_STORAGE ){

            if( grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED ){
                showCameraActivity();
            }else{
                AlertDialog.Builder builder = new AlertDialog.Builder( SetAccountActivity.this);
                builder.setMessage( "기기, 사진, 미디어, 파일 액세스 권한을 수락하지 않으면 사진 찍기를 진행할 수 없습니다. 수락하여 주십시오." )
                        .setCancelable(true)        // 뒤로 버튼 클릭시 취소 가능 설정
                        .setNeutralButton(R.string.common_ok, new DialogInterface.OnClickListener() {
                            // 확인 버튼 클릭시 설정
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.cancel();
                            }
                        });
                AlertDialog dialog = builder.create();    // 알림창 객체 생성
                dialog.show();    // 알림창 띄우기
            }

        }
    }

    private void showPopup( View view , String errorMessage )
    {

        View errorPopView = getLayoutInflater().inflate(R.layout.popup_signin_error, null);
        TextView tvMessage = (TextView) errorPopView.findViewById( R.id.tv_popup_signin_error_message );
        tvMessage.setText( errorMessage );

        errorPopup = new PopupWindow( errorPopView , WindowManager.LayoutParams.WRAP_CONTENT , WindowManager.LayoutParams.WRAP_CONTENT , true);
        errorPopup.setOutsideTouchable(true);
        errorPopup.setBackgroundDrawable(new BitmapDrawable());
        errorPopup.setTouchable(true);
        errorPopup.setTouchInterceptor(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {

                errorPopup.dismiss();
                errorPopup = null;
                return false;

            }
        });

        int yOffset = (int) TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP , 2.6f ,  this.getResources().getDisplayMetrics() );
        errorPopup.setAnimationStyle(-1);
        errorPopup.showAsDropDown(view, 0, -yOffset);
    }

    private void showCameraActivity(){

        Intent i = new Intent(SetAccountActivity.this, ImageUtil.class);
        startActivityForResult(i, CAMERA_REQUEST);

    }



}