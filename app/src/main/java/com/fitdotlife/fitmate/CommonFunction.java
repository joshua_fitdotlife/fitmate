package com.fitdotlife.fitmate;

import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ContinuousCheckPolicy;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseAnalyzer;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.MoreExerciseForMaxScore_StrengthType;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.StrengthInputType;
import com.fitdotlife.fitmate_lib.object.ScoreClass;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by fitlife.soondong on 2015-09-01.
 */
public class CommonFunction {
    public static int calculateAge( long birthDate )
    {
        Date todayDate= new Date();

        Calendar birthCalendar = Calendar.getInstance();
        Calendar todayCalendar = Calendar.getInstance();

        birthCalendar.setTimeInMillis( birthDate );
        todayCalendar.setTime(todayDate);

        int birthYear = birthCalendar.get(Calendar.YEAR);
        int todayYear = todayCalendar.get(Calendar.YEAR);
        int age = 0;

        if( todayCalendar.before( birthCalendar ) )
        {
            age = (todayYear - birthYear);
        }
        else
        {
            age = (todayYear - birthYear) + 1;
        }

        return age;
    }

    /**
     *  칼로리 리스트로 점수 리스트(ScoreClass)를 리턴한다.
     *
     *  @param calories NSArray filled with NSNumber(int)
     *  @param exerciseAnalyzer ExerciseAnalyzer
     *
     *  @return Array filled with ScoreClass
     */
    public static List<ScoreClass> CalculateDayScores_Calories(List<Integer> calories, ExerciseAnalyzer exerciseAnalyzer)
    {

        List<ScoreClass>result =new ArrayList<ScoreClass>();

        List<Integer>toYesterDay =new ArrayList<>();

        for(int i=0;i<calories.size();i++)
        {
            ScoreClass temp =new ScoreClass();
            //어제까지 점수
            if(i==0){
                toYesterDay.add(0);

            }
            int weekAchieveToYesterday = exerciseAnalyzer.CalculateWeekAchievement_Calorie(toYesterDay);


            if(i==0)
            {
                toYesterDay.remove(toYesterDay.size()-1);

            }


            //오늘 점수
            toYesterDay.add(calories.get(i));

            int weekAchieveToToday = exerciseAnalyzer.CalculateWeekAchievement_Calorie(toYesterDay);
            MoreExerciseForMaxScore_StrengthType moreStrengthType;
            if(exerciseAnalyzer.getProgram().getStrengthInputType()== StrengthInputType.CALORIE){
                moreStrengthType = exerciseAnalyzer.getNeedMoreExercise_Calorie(toYesterDay);
            }else{
                moreStrengthType = exerciseAnalyzer.getNeedMoreExercise_VS_BMR(toYesterDay);
            }


            int todayGetPoint = weekAchieveToToday - weekAchieveToYesterday;
            toYesterDay.remove(toYesterDay.size() - 1);

            //오늘 얻을 수 있는 최대 점수
            toYesterDay.add(exerciseAnalyzer.getCalorieFrom());

            int todaymore=exerciseAnalyzer.CalculateWeekAchievement_Calorie(toYesterDay)-weekAchieveToYesterday-todayGetPoint;

            if(todaymore<0) todaymore=0;
            temp.todayPossibleMorePoint =todaymore;
            temp.isAchieveMax = moreStrengthType.isAchieveMax();
            temp.possibleMaxScore= moreStrengthType.getPossibleMaxScore();
            temp.todayGetPoint = todayGetPoint;
            temp.weekAchieve = weekAchieveToToday;
            temp.preDayAchieve = weekAchieveToYesterday;
            temp.possibleTimes =moreStrengthType.getDays();
            temp.possibleValue =moreStrengthType.getRangeFrom();


            toYesterDay.remove(toYesterDay.size()-1);
            toYesterDay.add(calories.get(i));

            result.add(temp);

        }

        return result;
    }




    public static  List<ScoreClass> CalculateDayScores_Minutes(List<Integer>minutes, ExerciseAnalyzer exerciseAnalyzer)
    {

        List<ScoreClass>result =new ArrayList<ScoreClass>();

        List<List<Integer>>toYesterDay =new ArrayList<List<Integer>>();

        for(int i=0;i<minutes.size();i++)
        {
            ScoreClass temp =new ScoreClass();
            //어제까지 점수
            if(i==0){
                List<Integer> zeroList = new ArrayList<>();
                zeroList.add(0);
                toYesterDay.add(zeroList);
            }
            int weekAchieveToYesterday = exerciseAnalyzer.CalcuateWeekAchieveByMinutesList(ContinuousCheckPolicy.Loosely_SumOverXExercise, toYesterDay);

            if(i==0)
            {
                toYesterDay.remove(toYesterDay.size()-1);
            }


            //오늘 점수

            toYesterDay.add(new ArrayList<Integer>());
            toYesterDay.get(toYesterDay.size()-1).add(minutes.get(i));

            int weekAchieveToToday = exerciseAnalyzer.CalcuateWeekAchieveByMinutesList(ContinuousCheckPolicy.Loosely_SumOverXExercise, toYesterDay);
            MoreExerciseForMaxScore_StrengthType moreStrengthType;

            List<Integer> forMore = new ArrayList<>();
            for(List<Integer> t : toYesterDay){
                forMore.add((t.get(0)));
            }

            moreStrengthType = exerciseAnalyzer.getNeedMoreExercise(ContinuousCheckPolicy.Loosely_SumOverXExercise, forMore);


            int todayGetPoint = weekAchieveToToday - weekAchieveToYesterday;
            toYesterDay.remove(toYesterDay.size() - 1);

            //오늘 얻을 수 있는 최대 점수
            List<Integer>formax=new ArrayList<>();
            formax.add(exerciseAnalyzer.getProgram().getMinutes_From());
            toYesterDay.add(formax);

            int todaymore=exerciseAnalyzer.CalcuateWeekAchieveByMinutesList(ContinuousCheckPolicy.Loosely_SumOverXExercise, toYesterDay)-weekAchieveToYesterday-todayGetPoint;

            if(todaymore<0) todaymore=0;
            temp.todayPossibleMorePoint =todaymore;
            temp.isAchieveMax = moreStrengthType.isAchieveMax();
            temp.possibleMaxScore= moreStrengthType.getPossibleMaxScore();
            temp.todayGetPoint = todayGetPoint;
            temp.weekAchieve = weekAchieveToToday;
            temp.preDayAchieve = weekAchieveToYesterday;
            temp.possibleTimes =moreStrengthType.getDays();
            temp.possibleValue =moreStrengthType.getRangeFrom();


            toYesterDay.remove(toYesterDay.size()-1);
            toYesterDay.add(new ArrayList<Integer>());
            toYesterDay.get(toYesterDay.size()-1).add(minutes.get(i));

            result.add(temp);

        }

        return result;
//        List<>result =new ArrayList<>();
//
//        List<>toYesterDay =new ArrayList<>();
//
//        for(int i=0;i<[minutes count];i++)
//        {
//            ScoreClass *temp = [[ScoreClass alloc]init];
//            //어제까지 점수
//            if(i==0){
//
//                [toYesterDay addObject:[[NSMutableArray alloc]initWithObjects:[NSNumber numberWithInt:0],[NSNumber numberWithInt:0], nil]];
//            }
//            int weekAchieveToYesterday = [exerciseAnalyzer CalculateWeekAchieveByMinutesList:toYesterDay];
//
//            if(i==0)
//            {
//                [toYesterDay removeLastObject];
//            }
//
//            //오늘 점수
//            [toYesterDay addObject:[[NSMutableArray alloc]initWithObjects:[minutes objectAtIndex:i],[NSNumber numberWithInt:0], nil]];
//
//            int weekAchieveToToday = [exerciseAnalyzer CalculateWeekAchieveByMinutesList:toYesterDay];
//            MoreExerciseForMaxScore_StrAndCalorie *moreStrengthType = [exerciseAnalyzer getNeedMoreExercise:toYesterDay];
//            int todayGetPoint = weekAchieveToToday - weekAchieveToYesterday;
//            [toYesterDay removeLastObject];
//
//            //오늘 얻을 수 있는 최대 점수
//            List<>arrayForMaxScore=new ArrayList<>();
//            int k=1;
//            if(exerciseAnalyzer.exerciseProgram.isMultipleTimesPerDay==true){
//                k= exerciseAnalyzer.exerciseProgram.timesPerDayFrom;
//            }
//            for(int u=0;u<k;u++){
//                [arrayForMaxScore addObject:[NSNumber numberWithInt:exerciseAnalyzer.exerciseProgram.minutesFrom]];
//            }
//            [arrayForMaxScore addObject:[NSNumber numberWithInt:0]];
//            [toYesterDay addObject:arrayForMaxScore];
//
//            int todaymore=[exerciseAnalyzer CalculateWeekAchieveByMinutesList:toYesterDay]-weekAchieveToYesterday-todayGetPoint;
//
//            if(todaymore<0) todaymore=0;
//            temp.todayPossibleMorePoint =todaymore;
//
//            temp.isAchieveMax = [moreStrengthType isAchieveMax];
//            temp.possibleMaxScore= [moreStrengthType possibleMaxScore];
//            temp.todayGetPoint = todayGetPoint;
//            temp.weekAchieve = weekAchieveToToday;
//            temp.preDayAchieve = weekAchieveToYesterday;
//            temp.possibleTimes = [moreStrengthType days];
//            temp.possibleValue = [moreStrengthType rangeFrom];
//
//
//            [toYesterDay removeLastObject];
//
//            [toYesterDay addObject:[[NSMutableArray alloc]initWithObjects:[minutes objectAtIndex:i],[NSNumber numberWithInt:0], nil]];
//
//            [result addObject:temp];
//        }
//
//        return result;
    }


}
