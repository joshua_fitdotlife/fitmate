package com.fitdotlife.fitmate.log;

import android.content.Context;
import android.content.SharedPreferences;

import com.crashlytics.android.Crashlytics;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.Queue;

/**
 * Created by Joshua on 2016-04-08.
 */
public class Log {

    private static final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS:", Locale.getDefault() );
    private static Context mContext;
    private static final String LOG_KEY = "fitmeterlog";
    private static final int LOGGING_MAX_LENGTH = 1 * 1024 * 1024; //1MB

    private static Thread logThread = null;
    private static LogRunnable logRunnable = null;
    private static Queue<String> logMessageQueue = new LinkedList<String>();
    private static boolean mRunning = false;

    public static void configure( Context context){
        mContext = context;
        logRunnable = new LogRunnable();
        mRunning = true;
        logThread = new Thread(logRunnable);
        logThread.start();
    }

    public static void reStart()
    {
        logRunnable = new LogRunnable();
        mRunning = true;
        logThread = new Thread(logRunnable);
        logThread.start();
    }

    public static void v( String tag , String msg )
    {

        long timeMilliSeconds = System.currentTimeMillis();
        String content = CurrentText(tag, msg);
        android.util.Log.v(tag, content);
        Crashlytics.log(content);

        Date logDate = new Date();
        logDate.setTime(timeMilliSeconds);
        String logMessage = formatter.format( logDate ) + " - [VERBOSE] - " + content + "\n";
        logMessageQueue.offer(logMessage);
    }

    public static void i( String tag , String msg )
    {
        long timeMilliSeconds = System.currentTimeMillis();
        String content = CurrentText(tag, msg);
        android.util.Log.i(tag, content);
        Crashlytics.log(content);

        Date logDate = new Date();
        logDate.setTime(timeMilliSeconds);
        String logMessage = formatter.format( logDate ) + " - [INFO] - " + content + "\n";
        logMessageQueue.offer(logMessage);
    }

    public static void d( final String tag , final String msg )
    {
        long timeMilliSeconds = System.currentTimeMillis();
        String content = CurrentText(tag, msg);
        android.util.Log.d(tag, content);
        Crashlytics.log(content);

        Date logDate = new Date();
        logDate.setTime(timeMilliSeconds);
        String logMessage = formatter.format( logDate ) + " - [DEBUG] - " + content + "\n";
        logMessageQueue.offer(logMessage);
    }

    public static void w( final String tag , final String msg )
    {
        long timeMilliSeconds = System.currentTimeMillis();
        String content = CurrentText(tag, msg);
        android.util.Log.w(tag, content);
        Crashlytics.log(content);

        Date logDate = new Date();
        logDate.setTime(timeMilliSeconds);
        String logMessage = formatter.format( logDate ) + " - [WARN] - " + content + "\n";
        logMessageQueue.offer(logMessage);
    }

    public static void e( final String tag , final String msg ){
        long timeMilliSeconds = System.currentTimeMillis();
        String content = CurrentText(tag, msg);
        android.util.Log.e(tag, content);
        Crashlytics.log(content);

        Date logDate = new Date();
        logDate.setTime(timeMilliSeconds);
        String logMessage = formatter.format( logDate ) + " - [ERROR] - " + content + "\n";
        logMessageQueue.offer(logMessage);
    }

    public static String getLog()
    {
        SharedPreferences pref = mContext.getSharedPreferences("fitmate", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        return pref.getString(LOG_KEY, "");
    }

    private static String CurrentText(String tag, String msg)
    {
        StackTraceElement[] ste = Thread.currentThread().getStackTrace();
        StackTraceElement cur = ste[4];
        String className =cur.getClassName();
        String[] name = className.split("\\.");
        String Name = name[name.length - 1];
        return String.format("%s/%s[%d]\t %s\t %s", Name, cur.getMethodName(), cur.getLineNumber(), tag, msg);
    }

    public static void stopLogging(){
        mRunning = false;
    }

    public static void flush()
    {
        String logs = "";
        int size = logMessageQueue.size();
        for( int i = 0 ; i < size ; i++ )
        {
            try {
                logs += logMessageQueue.poll();
            }catch(NoSuchElementException e){ continue; }
        }

        if( !logs.equals("") )
        {
            writeLog(logs);
        }
    }

    private static void writeLog( String logMessage )
    {
        String preLog = getLog();
        StringBuffer stringBuffer = new StringBuffer(preLog);
        stringBuffer.append( logMessage );

        if( stringBuffer.length() >  LOGGING_MAX_LENGTH )
        {
            int overLength = stringBuffer.length() - LOGGING_MAX_LENGTH;
            if( overLength > ( LOGGING_MAX_LENGTH / 10 ) )
            {
                stringBuffer.delete( 0 , overLength );
            }
        }

        SharedPreferences pref = mContext.getSharedPreferences("fitmate", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(LOG_KEY, stringBuffer.toString() );
        editor.commit();

        stringBuffer.setLength(0);
    }

    private static class LogRunnable implements Runnable
    {
        @Override
        public void run() {


            while( mRunning )
            {
                String logInfo = "";
                try {

                    int size = logMessageQueue.size();
                    for( int i = 0 ; i < size ;i++ ){
                        logInfo += logMessageQueue.poll();
                    }

                }catch(NoSuchElementException e){

                }

                if (!logInfo.equals(""))
                {
                    writeLog(logInfo);
                }

                try {

                    int sleepMilliseconds = 0;

                    if( logMessageQueue.size() > 0 ){
                        sleepMilliseconds = 100;
                    }else {
                        sleepMilliseconds = 1000;
                    }

                    Thread.sleep(sleepMilliseconds);

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
