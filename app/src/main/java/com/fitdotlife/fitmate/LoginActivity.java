package com.fitdotlife.fitmate;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fitdotlife.fitmate_lib.customview.FitmeterProgressDialog;
import com.fitdotlife.fitmate_lib.customview.FitmeterDialog;
import com.fitdotlife.fitmate_lib.iview.ILoginView;
import com.fitdotlife.fitmate_lib.presenter.LoginPresenter;
import com.fitdotlife.fitmate_lib.service.alarm.AlarmService;

public class LoginActivity extends Activity implements ILoginView, OnClickListener
{
    private LoginPresenter mPresenter = null;

    private Button btnLogin = null;
    private EditText etxID = null;
    private EditText etxPassword = null;

    private TextView tvFindPassword = null;
    private TextView tvSignIn = null;
    private ImageView imgSignIn = null;

    private FitmeterDialog mDialog = null;
    private FitmeterProgressDialog mProgressDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mPresenter = new LoginPresenter( this , this );

        this.btnLogin = (Button) this.findViewById( R.id.btn_login );
        this.btnLogin.setOnClickListener(this);
        this.etxID = (EditText) this.findViewById(R.id.etx_id);
        this.etxID.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (etxID.getText().toString().length() > 0 && etxPassword.getText().toString().length() > 0) {
                    btnLogin.setBackground(getResources().getDrawable(R.drawable.login_btn_selector));
                } else {
                    btnLogin.setBackground(getResources().getDrawable(R.drawable.login_btn));
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        this.etxPassword = (EditText) this.findViewById(R.id.etx_pwd);
        this.etxPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if( etxID.getText().toString().length() > 0 && etxPassword.getText().toString().length() > 0 ){
                    btnLogin.setBackground( getResources().getDrawable(R.drawable.login_btn_selector) );
                }else{
                    btnLogin.setBackground( getResources().getDrawable(R.drawable.login_btn ) );
                }
            }
            @Override
            public void afterTextChanged(Editable s) { }
        });

        this.tvFindPassword = (TextView) this.findViewById(R.id.tv_find_password);
        SpannableString content = new SpannableString( getString( R.string.login_forgot_password ) );
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        tvFindPassword.setText(content);
        this.tvFindPassword.setOnClickListener(this);
        this.imgSignIn = (ImageView) this.findViewById(R.id.img_activity_login_sign_up);
        this.imgSignIn.setOnClickListener(this);

       /* Calendar birthCalendar = Calendar.getInstance();
        birthCalendar.set(Calendar.YEAR,1977);
        birthCalendar.set(Calendar.MONTH,7);
        birthCalendar.set(Calendar.DATE, 15);

        long datelong = birthCalendar.getTime().getTime();*/

    }

    @Override
    public void navigateToHome()
    {
        Intent intent = null;
        if( getSettedUserInfo() ) {
            //intent = new Intent(LoginActivity.this, NewHomeAsActivity_.class);
            //intent = new Intent(LoginActivity.this, HomeActivity.class);
            intent = new Intent(LoginActivity.this, NewHomeActivity_.class);
        }else{
            intent = new Intent(LoginActivity.this, NewHomeAsActivity_.class);
        }

        this.startActivity(intent);

        if( !getHomeNotView() ){
            this.showTutorialActivity();
        }
    }

    private boolean getHomeNotView()
    {
        SharedPreferences pref = getSharedPreferences( "fitmateservice" , Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        return pref.getBoolean( HelpActivity.HOME_NOT_VIEW_KEY  , false);
    }

    private void showHelpActivity( ){
        Intent intent = new Intent( LoginActivity.this , HelpActivity.class );
        intent.putExtra( HelpActivity.MODE_KEY, HelpActivity.HOME_HELP_MODE );
        intent.putExtra( HelpActivity.VISIBLE_KEY, true );
        this.startActivity(intent);
    }

    private void showTutorialActivity(){
        Intent intent = new Intent( LoginActivity.this , NewHomeTutorialActivity_.class );
        this.startActivity(intent);
    }

    private void startAlarmService(){
        Intent alarmIntent = new Intent( this , AlarmService.class);
        startService(alarmIntent);
    }

    @Override
    public void showResult(String message)
    {
        Toast.makeText(this, message , Toast.LENGTH_LONG ).show();
    }

    public void finishActivity()
    {
        this.finish();
    }

    @Override
    public void onClick(View view)
    {
        switch( view.getId() )
        {
            case R.id.btn_login:

                if( this.validInput() )
                {
                    this.mPresenter.login(  this.etxID.getText().toString() , this.etxPassword.getText().toString() );
                }
                break;
            case R.id.tv_find_password:

                SeachPasswordDialog seachPasswordDialog = new SeachPasswordDialog(this);
                seachPasswordDialog.show();

                break;
            case R.id.img_activity_login_sign_up:

                //Intent intent = new Intent( LoginActivity.this , SetUserInfoActivity.class );
                Intent intent = new Intent( LoginActivity.this , SetAccountActivity.class );
                ActivityReference.activityReferece = this;
                this.startActivity(intent);

                break;
        }
    }

    private boolean validInput()
    {
        if(this.etxID.getText().length() == 0)
        {
            Toast.makeText( this, this.getString(R.string.login_input_email), Toast.LENGTH_SHORT ).show();
            this.etxID.requestFocus();
            return false;
        }

        if( this.etxPassword.getText().length() == 0 )
        {
            Toast.makeText( this, this.getString(R.string.login_input_password) , Toast.LENGTH_SHORT ).show();
            this.etxPassword.requestFocus();
            return false;
        }

        return true;
    }

    public void startProgressDialog(){

//        this.mDialog =  new FitmeterDialog(this);
//        View contentView = this.getLayoutInflater().inflate( R.layout.dialog_progress , null );
//        TextView tvTitle = (TextView) contentView.findViewById(R.id.tv_dialog_progress_title);
//        tvTitle.setMessage( this.getString(R.string.login_dialog_title) );
//
//        TextView tvContent = (TextView) contentView.findViewById(R.id.tv_dialog_progress_content);
//        tvContent.setMessage( this.getString(R.string.login_dialog_content) );
//
//        mDialog.setContentView(contentView);
//        mDialog.setButtonView(null);
//        mDialog.show();


        this.mProgressDialog = new FitmeterProgressDialog(this);
        this.mProgressDialog.setMessage(this.getString(R.string.login_dialog_content));
        this.mProgressDialog.show();;

    }

    public void stopProgressDialog(){
        if( this.mProgressDialog != null ) {
            this.mProgressDialog.dismiss();
        }
    }

    @Override
    public void showUpdateDialog() {

        final FitmeterDialog updateDialog = new FitmeterDialog(this);
        LayoutInflater inflater = this.getLayoutInflater();

        View contentView = inflater.inflate( R.layout.dialog_default_content , null );
        TextView tvTitle = (TextView) contentView.findViewById(R.id.tv_dialog_default_title);
        tvTitle.setText( getString(R.string.required_update_title) );
        TextView tvContent = (TextView) contentView.findViewById(R.id.tv_dialog_default_content);
        tvContent.setText( getString(R.string.required_update_content) );
        updateDialog.setContentView(contentView);

        View buttonView = inflater.inflate( R.layout.dialog_button_two , null );
        Button btnLeft = (Button) buttonView.findViewById(R.id.btn_dialog_button_two_left);
        btnLeft.setText(this.getString(R.string.common_cancel));
        btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        Button btnRight = (Button) buttonView.findViewById(R.id.btn_dialog_button_two_right);
        btnRight.setText(this.getString(R.string.common_update));
        btnRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent updateIntent = new Intent(Intent.ACTION_VIEW);
                updateIntent.setData(Uri.parse("market://details?id=" + getPackageName()));
                startActivity(updateIntent);
                finish();
            }
        });
        updateDialog.setButtonView(buttonView);
        updateDialog.show();

    }

    @Override
    protected void onDestroy() {

        if( this.mProgressDialog != null ){
            this.mProgressDialog.dismiss();
        }

        super.onDestroy();
    }

    private boolean getSettedUserInfo(){
        SharedPreferences pref = this.getSharedPreferences("fitmateservice", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        return pref.getBoolean( LoginPresenter.SET_USERINFO_KEY , false);
    }
}
