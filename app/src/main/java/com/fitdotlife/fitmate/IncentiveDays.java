package com.fitdotlife.fitmate;

import android.content.Context;
import android.util.TypedValue;


public class IncentiveDays {


    public static int getDpToPixel(Context context, float DP) {
        float px = 0;
        try {
            px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, DP, context.getResources().getDisplayMetrics());
        } catch (Exception e) {

        }
        return (int) px;
    }

}
