package com.fitdotlife.fitmate;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.PermissionChecker;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fitdotlife.fitmate.firmware.FirmwareUpdateActivity;
import com.fitdotlife.fitmate.firmware.FirmwareUpdateActivity_;
import com.fitdotlife.fitmate_lib.customview.FitmeterDialog;
import com.fitdotlife.fitmate_lib.customview.FitmeterProgressDialog;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.http.FirmwareUpdateService;
import com.fitdotlife.fitmate_lib.http.NetworkClass;
import com.fitdotlife.fitmate_lib.http.VersionCheckService;
import com.fitdotlife.fitmate_lib.iview.IMyProfileView;
import com.fitdotlife.fitmate_lib.key.GenderType;
import com.fitdotlife.fitmate_lib.object.FirmwareVersionInfo;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.presenter.MyProfilePresenter;
import com.fitdotlife.fitmate_lib.util.Utils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringRes;
import org.androidannotations.rest.spring.annotations.RestService;
import org.apache.log4j.Log;

import org.springframework.web.client.RestClientException;

import java.text.SimpleDateFormat;
import java.util.Date;

@EActivity(R.layout.activity_setting)
public class SettingActivity extends Activity implements IMyProfileView{

    private static final String TAG = SettingActivity.class.getSimpleName();
    public static final String ACTION_USERINFO_CHANGE = "com.fitdotlife.fitmate.USERINFO_CHANGE";
    private UserInfo mUserInfo;
    private FitmateDBManager mDBManager;
    private MyProfilePresenter mPresenter = null;
    private Activity mActivity = null;

    private final int USERINFO_REQUEST_CODE = 0;
    private final int USERINFO_CHANGEDEVICE_CODE = 1;
    private final int USERINFO_CHANGELOCATION_CODE = 2;

    private int PERMISSION_REQUEST_STORAGE = 1;

    private boolean isShowing = false;

    private boolean isFirmwareUpdate = true;
    private boolean lastestFirmware = true;
    private String mPrefixSerialNumber = null;

    @ViewById(R.id.tv_activity_setting_name)
    TextView tvName;

    @ViewById(R.id.tv_activity_setting_introself)
    TextView tvIntroSelf;

    @ViewById(R.id.tv_activity_setting_birthdate)
    TextView tvBirthDate;

    @ViewById(R.id.tv_activity_setting_sex)
    TextView tvSex;

    @ViewById(R.id.tv_activity_setting_height)
    TextView tvHeight;

    //@ViewById(R.id.tv_activity_setting_height_unit)
    //TextView tvHeightUnit;

    @ViewById(R.id.tv_activity_setting_weight)
    TextView tvWeight;

    //@ViewById(R.id.tv_activity_setting_weight_unit)
    //TextView tvWeightUnit;

    @ViewById(R.id.tv_activity_setting_wearinglocation)
    TextView tvWearingLocation;

    @ViewById(R.id.tv_activity_setting_connectfitmeter)
    TextView tvConnectFitmeter;

    @StringRes(R.string.common_male)
    String strMale;

    @StringRes(R.string.common_female)
    String strFemale;

    @StringRes(R.string.setting_bar_title)
    String strSetting;

    @StringRes(R.string.common_connect_network)
    String strConnectNetwork;

    @ViewById(R.id.ic_activity_setting_actionbar)
    View actionBarView;

//    @ViewById(R.id.rl_activity_setting_progress)
//    RelativeLayout rlProgress;
//
//    @ViewById(R.id.tv_activity_setting_progress)
//    TextView tvProgress;

    @ViewById(R.id.rl_activity_setting_wearinglocation)
    RelativeLayout rlWearingLocation;

    @RestService
    VersionCheckService versionCheckServiceClient;

    @RestService
    FirmwareUpdateService firmwareUpdateServiceClient;

    @ViewById(R.id.tv_activity_setting_firmwareupdate)
    TextView tvFirmwareUpdate;

    private FitmeterProgressDialog mProgressDialog = null;

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Intent programChangeIntent = new Intent( ACTION_USERINFO_CHANGE );
        sendBroadcast(programChangeIntent);

    }

    @AfterViews
    void onInit()
    {

        displayActionBar();

        mDBManager = new FitmateDBManager(this);
        mUserInfo = mDBManager.getUserInfo();
        this.mPresenter = new MyProfilePresenter(this , this);

        this.mActivity = this;

        versionCheckServiceClient.setRootUrl(NetworkClass.baseURL + "/api");
        firmwareUpdateServiceClient.setRootUrl(NetworkClass.baseURL + "api");

        tvName.setText( mUserInfo.getName() );
        tvIntroSelf.setText(mUserInfo.getIntro());

        //생년월일이 있는지 확인한다.
        if( mUserInfo.getBirthDate() > 0 ) {
            long birthDataTimeMilliSecond = mUserInfo.getBirthDate();
            tvBirthDate.setText(this.getDateString(birthDataTimeMilliSecond));
        }

        //성별이 있는지 확인한다.
        if( !mUserInfo.getGender().equals( GenderType.NONE ) ) {
            if (mUserInfo.getGender() == GenderType.MALE) {
                tvSex.setText(strMale);
            } else if (mUserInfo.getGender() == GenderType.FEMALE) {
                tvSex.setText(strFemale);
            }
        }

        //Si가 있는지 확인한다.
        boolean isSi = mUserInfo.isSI();

        //키가 있는지 확인한다.
        if( mUserInfo.getHeight() > 0 ) {
            String[] valueArray = this.getValueArray( mUserInfo.getHeight() );
            tvHeight.setText( String.format("%s %s", valueArray[0] + "." + valueArray[1], UserInfo.getHeightUnit(isSi)) );
        }else{
            tvHeight.setText( this.getString(R.string.common_no_setting) );
        }
        //몸무게가 있는지 확인한다.
        if(mUserInfo.getWeight() > 0) {
            String[] valueArray = this.getValueArray( mUserInfo.getWeight() );
            tvWeight.setText( String.format("%s %s", valueArray[0] + "." + valueArray[1], UserInfo.getWeightUnit(isSi)) );
        }else{
            tvWeight.setText( this.getString(R.string.common_no_setting) );
        }

        //착용위치가 있는지 확인한다.
        if( mUserInfo.getWearAt() > -1 ) {
            if( mUserInfo.getDeviceAddress() != null && !mUserInfo.getDeviceAddress().equals("") && !mUserInfo.getDeviceAddress().equals("null") ) { //기기등록을 안했다면 기기 변경을 할 수 없다.
                tvWearingLocation.setText(getWearingLocationString(mUserInfo.getWearAt()));
            }else{
                tvWearingLocation.setText(this.getString(R.string.common_no_setting));
            }
        }else{
            tvWearingLocation.setText(this.getString(R.string.common_no_setting));
        }

        tvFirmwareUpdate.setText(this.getResources().getString(R.string.firmware_update_checking_serverversion));
        if( mUserInfo.getDeviceAddress() != null )
        {
            if (mUserInfo.getDeviceAddress().contains(":")) {
                isFirmwareUpdate = false;
                tvFirmwareUpdate.setText(this.getResources().getString(R.string.firmware_update_setting_unavailable));
            } else {
                mPrefixSerialNumber = mUserInfo.getDeviceAddress().substring(0, 4);

                if (Utils.isOnline(this)) {
                    checkServerVersion();

                } else {
                    FirmwareVersionInfo versionInfo = getDeviceFirmwareVersionInfo();
                    tvFirmwareUpdate.setText(versionInfo.getMajorVersion() + "." + versionInfo.getMinorVersion());
                    lastestFirmware = false;
                }
            }
        }else{
            isFirmwareUpdate = false;
            tvFirmwareUpdate.setText(this.getResources().getString(R.string.firmware_update_setting_unavailable));
        }
    }

    private void displayActionBar( ){
        actionBarView.setBackgroundColor(this.getResources().getColor(R.color.fitmate_bar_gray));
        TextView tvBarTitle = (TextView) actionBarView.findViewById( R.id.tv_custom_action_bar_title );
        tvBarTitle.setText(strSetting);

        ImageView imgBack = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_left);
        imgBack.setVisibility(View.GONE);

        ImageView mImgRight = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_right);
        mImgRight.setVisibility(View.GONE);
    }

    @Click(R.id.rl_activity_setting_alarm)
    void alarmSettingClick(){
        if(Utils.isOnline(this) ) {
            AlarmSettingActivity_.intent(this).start();
        }else{
            Toast.makeText(this,strConnectNetwork,Toast.LENGTH_SHORT).show();
        }
    }

    @Click(R.id.ll_activity_setting_change_password)
    void changePasswordClick(){

        if (!Utils.isOnline(this)) {
            Toast.makeText(this, this.getString(R.string.myprofile_connect_network), Toast.LENGTH_SHORT).show();
            return;
        }

        ChangePasswordDialog fpDialog = new ChangePasswordDialog(this, this.mUserInfo.getEmail());
        fpDialog.show();
    }

    private String getWearingLocationString( int wearingLocation ){
        String result = null;

        if( wearingLocation == 0 ){
            wearingLocation = 1;
        }

        result = this.getResources().getStringArray(R.array.weraing_location_string)[wearingLocation - 1];
        return result;
    }

    @Click(R.id.ll_activity_setting_userinfo)
    void userInfoClick(){

        if (!Utils.isOnline(this)) {
            Toast.makeText(this, this.getString(R.string.myprofile_connect_network), Toast.LENGTH_SHORT).show();
            return;
        }

        Intent userinfoChangeIntent = new Intent(SettingActivity.this , UserInfoChangeActivity.class);
        userinfoChangeIntent.putExtra(UserInfo.EMAIL_KEY, this.mUserInfo.getEmail());
        userinfoChangeIntent.putExtra(UserInfo.NAME_KEY, this.mUserInfo.getName());
        userinfoChangeIntent.putExtra(UserInfo.HELLO_MESSAGE_KEY, this.mUserInfo.getIntro());
        userinfoChangeIntent.putExtra(UserInfo.BIRTHDATEKEY, this.mUserInfo.getBirthDate());
        userinfoChangeIntent.putExtra(UserInfo.SEX_KEY, this.mUserInfo.getGender().getValue());
        userinfoChangeIntent.putExtra(UserInfo.PROFILE_MAGE_KEY, this.mUserInfo.getAccountPhoto());

        this.startActivityForResult(userinfoChangeIntent, USERINFO_REQUEST_CODE);
    }

    @Click(R.id.rl_activity_setting_height)
    void heightClick(){

        if (!Utils.isOnline(this))
        {
            Toast.makeText(this, this.getString(R.string.myprofile_connect_network), Toast.LENGTH_SHORT).show();
            return;
        }

        if(mUserInfo.getHeight() == 0){
            showNoSettingDialog();
            return;
        }

        boolean heightUnit = mUserInfo.isSI();
        final FitmeterDialog heightDialog = new FitmeterDialog(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View contentView = inflater.inflate(R.layout.dialog_number_picker, null);
        TextView tvTitle = (TextView) contentView.findViewById(R.id.tv_dialog_number_picker_title);
        tvTitle.setText(this.getString(R.string.common_height));
        final NumberPicker np = (NumberPicker) contentView.findViewById(R.id.np_dialog_number_picker);
        np.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        final NumberPicker pp = (NumberPicker) contentView.findViewById(R.id.np_dialog_point_picker);
        pp.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        final NumberPicker up = (NumberPicker) contentView.findViewById(R.id.np_dialog_unit_picker);
        up.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        up.setDisplayedValues(new String[]{this.getString(R.string.common_centimeter), this.getString(R.string.common_ft)});

        float height = this.mUserInfo.getHeight();
        np.setMaxValue(300);
        np.setMinValue(1);
        pp.setMaxValue(9);
        pp.setMinValue(0);
        up.setMaxValue(1);
        up.setMinValue(0);

        String[] valueArray = this.getValueArray( height );
        np.setValue( Integer.parseInt( valueArray[0] ) );
        pp.setValue( Integer.parseInt( valueArray[1] ) );

        if( heightUnit )
        {
            up.setValue(0);
        }else{

            up.setValue(1);
        }
        up.setTag( height );

        heightDialog.setContentView(contentView);

        up.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                boolean heightSi = (newVal == 0);
                float tempHeight = (float) up.getTag();

                if (heightSi) {
                    tempHeight = com.fitdotlife.fitmate_lib.service.util.Utils.ft_to_cm(tempHeight);
                } else {
                    tempHeight = com.fitdotlife.fitmate_lib.service.util.Utils.cm_to_ft(tempHeight);
                }

                String[] valueArray = getValueArray(tempHeight );
                np.setValue( Integer.parseInt( valueArray[0] ) );
                pp.setValue( Integer.parseInt( valueArray[1] ) );

                up.setTag(tempHeight);
            }
        });

        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                up.setTag( (float) (((np.getValue() * 10d) + pp.getValue()) / 10d) );
            }
        });

        pp.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                up.setTag( (float) (((np.getValue() * 10d) + pp.getValue()) / 10d) );
            }
        });

        View buttonView = inflater.inflate( R.layout.dialog_button_two , null );
        Button btnLeft = (Button) buttonView.findViewById(R.id.btn_dialog_button_two_left);
        btnLeft.setText(this.getString(R.string.common_cancel));
        btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                heightDialog.close();
            }
        });

        Button btnRight = (Button) buttonView.findViewById(R.id.btn_dialog_button_two_right);
        btnRight.setText(this.getString(R.string.common_ok));
        btnRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean heightSi = up.getValue() == 0;
                boolean isSi = mUserInfo.isSI();
                float tempWeight = mUserInfo.getWeight();
                String weightUnitString = "";

                if (heightSi != isSi) {
                    //몸무게를 변환한다.
                    if (heightSi) {
                        //파운드에서 킬로그램으로 변환한다.
                        tempWeight = com.fitdotlife.fitmate_lib.service.util.Utils.lb_to_kg(tempWeight);
                        weightUnitString = getString(R.string.common_kilogram);
                    } else {
                        //킬로그램에서 파운드로 변환한다.
                        tempWeight = com.fitdotlife.fitmate_lib.service.util.Utils.kg_to_lb(tempWeight);
                        weightUnitString = getString(R.string.common_pound);
                    }

                    String[] weightValueArray = getValueArray(tempWeight);
                    tvWeight.setText(String.format("%s %s", weightValueArray[0] + "." + weightValueArray[1], weightUnitString));

                }
                mUserInfo.setHeight((float) up.getTag());
                mUserInfo.setWeight(tempWeight);
                mUserInfo.setIsSI(heightSi);
                mPresenter.modifyUserInfo(mUserInfo);

                String[] heightValueArray = getValueArray(mUserInfo.getHeight());
                tvHeight.setText(String.format("%s %s", heightValueArray[0] + "." + heightValueArray[1], up.getDisplayedValues()[up.getValue()]));
                mPresenter.ChangeUserInfo();

                heightDialog.close();

            }
        });
        heightDialog.setButtonView( buttonView );
        heightDialog.show();
    }

    @Click(R.id.rl_activity_setting_weight)
    void weightClick(){

        if (!Utils.isOnline(this)) {
            Toast.makeText(this, this.getString(R.string.myprofile_connect_network), Toast.LENGTH_SHORT).show();
            return;
        }

        if( mUserInfo.getWeight() == 0 ){
            showNoSettingDialog();
            return;
        }

        boolean weightUnit = mUserInfo.isSI();
        final FitmeterDialog weightDialog = new FitmeterDialog(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View contentView = inflater.inflate(R.layout.dialog_number_picker, null);
        TextView tvTitle = (TextView) contentView.findViewById(R.id.tv_dialog_number_picker_title);
        tvTitle.setText(this.getString(R.string.common_weight));

        final NumberPicker nps = (NumberPicker) contentView.findViewById(R.id.np_dialog_number_picker);
        nps.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        final NumberPicker pps = (NumberPicker) contentView.findViewById(R.id.np_dialog_point_picker);
        pps.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        final NumberPicker ups = (NumberPicker) contentView.findViewById(R.id.np_dialog_unit_picker);
        ups.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        ups.setDisplayedValues(new String[]{this.getString(R.string.common_kilogram), this.getString(R.string.common_pound)});

        float weight = this.mUserInfo.getWeight();
        nps.setMaxValue(500);
        nps.setMinValue(1);
        pps.setMaxValue(9);
        pps.setMinValue(0);
        ups.setMaxValue(1);
        ups.setMinValue(0);

        String[] valueArray = this.getValueArray( weight );
        nps.setValue(Integer.parseInt(valueArray[0]));
        pps.setValue(Integer.parseInt(valueArray[1]));

        if( weightUnit )
        {
            ups.setValue(0);
        }else{

            ups.setValue(1);
        }
        ups.setTag( weight );
        weightDialog.setContentView(contentView);

        ups.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {

                boolean weightSi = (newVal == 0);
                float tempWeight = (float) ups.getTag();

                //몸무게를 변환한다.
                if (weightSi) {
                    //파운드에서 킬로그램으로 변환한다.
                    tempWeight = com.fitdotlife.fitmate_lib.service.util.Utils.lb_to_kg(tempWeight);
                } else {
                    //킬로그램에서 파운드로 변환한다.
                    tempWeight = com.fitdotlife.fitmate_lib.service.util.Utils.kg_to_lb(tempWeight);
                }

                String[] valueArray = getValueArray( tempWeight );
                nps.setValue(Integer.parseInt(valueArray[0]));
                pps.setValue(Integer.parseInt(valueArray[1]));

                ups.setTag(tempWeight);
            }
        });

        nps.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                ups.setTag( (float) (((nps.getValue() * 10d) + pps.getValue()) / 10d) );
            }
        });

        pps.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                ups.setTag( (float) (((nps.getValue() * 10d) + pps.getValue()) / 10d) );
            }
        });

        View buttonView = inflater.inflate( R.layout.dialog_button_two , null );
        Button btnLeft = (Button) buttonView.findViewById(R.id.btn_dialog_button_two_left);
        btnLeft.setText(this.getString(R.string.common_cancel));
        btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                weightDialog.close();
            }
        });

        Button btnRight = (Button) buttonView.findViewById(R.id.btn_dialog_button_two_right);
        btnRight.setText( this.getString(R.string.common_ok) );
        btnRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean weightSi = ( ups.getValue() == 0 );
                boolean isSi = mUserInfo.isSI();
                float tempHeight = mUserInfo.getHeight();
                String heightUnitString = "";

                if (weightSi != isSi) {
                    //키를 변환한다.
                    if (weightSi) {
                        //인치에서 센티미터로 변환
                        tempHeight = com.fitdotlife.fitmate_lib.service.util.Utils.ft_to_cm(tempHeight);
                        heightUnitString = getString(R.string.common_centimeter);
                    } else {
                        //센티미터에서 인치로 변환한다.
                        tempHeight = com.fitdotlife.fitmate_lib.service.util.Utils.cm_to_ft(tempHeight);
                        heightUnitString = getString(R.string.common_ft);
                    }

                    String[] heightValueArray = getValueArray( tempHeight );
                    tvHeight.setText(String.format("%s %s", heightValueArray[0] + "." + heightValueArray[1] , heightUnitString ));

                }
                mUserInfo.setWeight((Float) ups.getTag());
                mUserInfo.setHeight(tempHeight);
                mUserInfo.setIsSI(weightSi);
                mPresenter.modifyUserInfo(mUserInfo);

                String[] weightValueArray = getValueArray( mUserInfo.getWeight() );
                tvWeight.setText(String.format("%s %s", weightValueArray[0] + "." + weightValueArray[1], ups.getDisplayedValues()[ups.getValue()]));
                mPresenter.ChangeUserInfo();

                weightDialog.close();
            }
        });

        weightDialog.setButtonView(buttonView);
        weightDialog.show();
    }

    @Click(R.id.rl_activity_setting_wearinglocation)
    void wearingLocationClick(){
        if( mUserInfo.getDeviceAddress() != null && !mUserInfo.getDeviceAddress().equals("") && !mUserInfo.getDeviceAddress().equals("null") ) { //기기등록을 안했다면 기기 변경을 할 수 없다.

            if (!Utils.isOnline(this)) {
                Toast.makeText(this, this.getString(R.string.myprofile_connect_network), Toast.LENGTH_SHORT).show();
                return;
            }

            if (mUserInfo.getDeviceAddress().contains(":") || mUserInfo.getDeviceAddress().substring(3, 4).equals("3") ) {
                showWearingLocationActivity();
            }else
            {
                if (mUserInfo.getDeviceAddress().substring(3, 4).equals("4"))
                {
                    final FitmeterDialog fDialog = new FitmeterDialog(this);
                    LayoutInflater inflater = this.getLayoutInflater();
                    View contentView = inflater.inflate(R.layout.dialog_default_content, null);
                    TextView tvTitle = (TextView) contentView.findViewById(R.id.tv_dialog_default_title);
                    tvTitle.setVisibility( View.GONE );

                    TextView tvContent = (TextView) contentView.findViewById(R.id.tv_dialog_default_content);
                    tvContent.setText(this.getString( R.string.drawer_cananotchangelocation));
                    fDialog.setContentView(contentView);

                    View buttonView = inflater.inflate(R.layout.dialog_button_one , null);
                    Button btnOne = (Button) buttonView.findViewById(R.id.btn_dialog_button_one);
                    btnOne.setText(this.getString(R.string.common_ok));
                    btnOne.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            fDialog.close();
                        }
                    });

                    fDialog.setButtonView(buttonView);
                    fDialog.show();
                }
            }


        }else{
            showNoSettingDialog();
        }
    }

    @Click(R.id.rl_activity_setting_changefitmeter)
    void changeFitmeterClick(){
        if( mUserInfo.getDeviceAddress() != null && !mUserInfo.getDeviceAddress().equals("") && !mUserInfo.getDeviceAddress().equals("null") ) { //기기등록을 안했다면 기기 변경을 할 수 없다.

            if (!Utils.isOnline(this)) {
                Toast.makeText(this, this.getString(R.string.myprofile_connect_network), Toast.LENGTH_SHORT).show();
                return;
            }

            showDeviceChangeActivity();

        }else{
            showNoSettingDialog();
        }
    }

    private void showProgressDialog(){
        this.mProgressDialog = new FitmeterProgressDialog( SettingActivity.this );
        this.mProgressDialog.show();
    }

    @Override
    public void dismissProgress()
    {
        this.mProgressDialog.dismiss();
    }

    @Click(R.id.rl_activity_setting_firmwareupdate)
    void softwareUpdateClick(){
        if( mUserInfo.getDeviceAddress() != null && !mUserInfo.getDeviceAddress().equals("") && !mUserInfo.getDeviceAddress().equals("null") ) { //기기등록을 안했다면 기기 변경을 할 수 없다.

            if( !Utils.isOnline(this) ){
                Toast.makeText( this , this.getString(R.string.common_connect_network) , Toast.LENGTH_SHORT ).show();
                return;
            }

            if( isFirmwareUpdate ) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                    if (PermissionChecker.checkSelfPermission(this, Manifest.permission_group.STORAGE) == PackageManager.PERMISSION_GRANTED) {

                        moveFirmwareUpdateActivity();

                    } else {

                        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST_STORAGE);

                    }

                } else {

                    moveFirmwareUpdateActivity();

                }
            }else{
                Toast.makeText(this , "당신의 휘트미터는 펌웨어 기능을 지원하지 않습니다." , Toast.LENGTH_LONG ).show();
            }

        }else{
            showNoSettingDialog();
        }
    }

    private void moveFirmwareUpdateActivity()
    {
        Intent intent = new Intent(SettingActivity.this , FirmwareUpdateActivity_.class);
        intent.putExtra( FirmwareUpdateActivity.LASTEST_FIRMWARE_KEY , lastestFirmware );
        startActivity(intent);
    }

    @OnActivityResult( USERINFO_REQUEST_CODE )
    void onUserInfoResult( int resultCode , Intent data ){
        if (resultCode == RESULT_OK) {

            this.mUserInfo.setName(data.getStringExtra(UserInfo.NAME_KEY));
            this.tvName.setText(this.mUserInfo.getName());

            this.mUserInfo.setIntro(data.getStringExtra(UserInfo.HELLO_MESSAGE_KEY));
            this.tvIntroSelf.setText(this.mUserInfo.getIntro());

            long tempBirthDay = data.getLongExtra(UserInfo.BIRTHDATEKEY, 0);

            if( tempBirthDay > 0 )
            {
                this.mUserInfo.setBirthDate(tempBirthDay);
                this.tvBirthDate.setText(this.getDateString(this.mUserInfo.getBirthDate()));
            }

            GenderType tempGenderType = GenderType.getGenderType(data.getIntExtra(UserInfo.SEX_KEY, 0));
            if( !tempGenderType.equals(GenderType.NONE) ) {
                this.mUserInfo.setGender(tempGenderType);
                if (mUserInfo.getGender() == GenderType.MALE) {
                    tvSex.setText(strMale);
                } else if (mUserInfo.getGender() == GenderType.FEMALE) {
                    tvSex.setText(strFemale);
                }
            }

            this.mDBManager.modifyUserInfo(mUserInfo);
            mPresenter.ChangeUserInfo();
        }
    }

    @OnActivityResult(USERINFO_CHANGEDEVICE_CODE)
    void changeDeviceResult()
    {
        this.isShowing = false;
        mUserInfo = this.mPresenter.getUserInfo();
        this.tvWearingLocation.setText( this.getWearingLocationString( mUserInfo.getWearAt() ) );
        this.mPresenter.ChangeUserInfo();
        //this.mPresenter.requestRestartService();
    }

    @OnActivityResult(USERINFO_CHANGELOCATION_CODE)
    void changeWearingLocationResult(int resultCode , Intent data ){
        if(resultCode == RESULT_OK)
        {
            int location = data.getIntExtra(LocationChangeActivity.INTENT_LOCATION, 0);
            this.tvWearingLocation.setText( this.getWearingLocationString( location ) );
            mUserInfo.setWearAt( location );
            mPresenter.modifyUserInfo(mUserInfo);
            mPresenter.ChangeUserInfo();
        }

        this.isShowing = false;
    }

    @Override
    public void showDeviceChangeActivity() {
        Intent deviceChangeIntent = new Intent( SettingActivity.this , DeviceChangeActivity.class  );
        deviceChangeIntent.putExtra(DeviceChangeActivity.INTENT_LOCATION, mUserInfo.getWearAt());
        this.startActivityForResult(deviceChangeIntent, USERINFO_CHANGEDEVICE_CODE);
    }

    @Override
    public void showWearingLocationActivity() {
        Intent locationChangeIntent = new Intent( SettingActivity.this , LocationChangeActivity.class  );
        locationChangeIntent.putExtra(LocationChangeActivity.INTENT_LOCATION, mUserInfo.getWearAt() );
        locationChangeIntent.putExtra(LocationChangeActivity.INTENT_BTADDRESS, mUserInfo.getDeviceAddress());
        this.startActivityForResult(locationChangeIntent, USERINFO_CHANGELOCATION_CODE);
    }

    @Override
    public void setShowing(boolean showing) {
        this.isShowing = showing;
    }

    private String getDateString( long birthUtcTime ){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd");
        Date utcDate = new Date();
        utcDate.setTime(birthUtcTime);
        return dateFormat.format(utcDate);
    }

    private void showNoSettingDialog()
    {

        final FitmeterDialog startFitmeterDialog = new FitmeterDialog( this );
        LayoutInflater inflater = this.getLayoutInflater();

        View contentView = inflater.inflate(R.layout.dialog_default_content, null);
        TextView tvTitle = (TextView) contentView.findViewById(R.id.tv_dialog_default_title);
        tvTitle.setVisibility(View.GONE);

        TextView tvContent = (TextView) contentView.findViewById(R.id.tv_dialog_default_content);
        tvContent.setText(this.getString(R.string.userinfo_not_connect_text));
        startFitmeterDialog.setContentView(contentView);

        View buttonView = inflater.inflate(R.layout.dialog_button_two , null);
        Button btnLeft = (Button) buttonView.findViewById(R.id.btn_dialog_button_two_left);
        btnLeft.setText( getString(R.string.common_no));
        btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startFitmeterDialog.close();
            }
        });

        Button btnRight = (Button) buttonView.findViewById(R.id.btn_dialog_button_two_right);
        btnRight.setText( getString( R.string.userinfo_start_fitmeter));
        btnRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityManager.getInstance().addActivity(mActivity);
                Intent intent = new Intent(getApplicationContext(), SetUserActivity.class);
                startActivity(intent);
                startFitmeterDialog.close();
            }
        });

        startFitmeterDialog.setButtonView(buttonView);
        startFitmeterDialog.show();
    }

    private String[] getValueArray( float value ){
        String valueString = String.valueOf( value );
        String[] result = null;
        String[] splitedWeightString = valueString.split("\\.");

        if( splitedWeightString.length > 1 ){
            splitedWeightString[1] = splitedWeightString[1].substring(0,1);
            result = splitedWeightString;
        }else{
            result = new String[]{ splitedWeightString[0] , "0" };
        }

        return result;
    }

    @Override
    public void setProgressText( final String progressText ){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mProgressDialog.setMessage( progressText );
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if( requestCode == PERMISSION_REQUEST_STORAGE )
        {

            if( grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED ){
                moveFirmwareUpdateActivity();
            }
        }
    }

    private FirmwareVersionInfo getCurrentFirmwareInfo()
    {
        SharedPreferences pref = getSharedPreferences("fitmate", Activity.MODE_PRIVATE);
        FirmwareVersionInfo version = new FirmwareVersionInfo();
        version.setMajorVersion(pref.getInt(FirmwareVersionInfo.APP_MAJORVERSION_KEY, 0));
        version.setMinorVersion(pref.getInt(FirmwareVersionInfo.APP_MINORVERSION_KEY, 0));
        version.setFileName( pref.getString( FirmwareVersionInfo.APP_DFUFILENAME_KEY, null ) );
        return version;
    }

    private FirmwareVersionInfo getDeviceFirmwareVersionInfo(){
        SharedPreferences pref = getSharedPreferences("fitmate", Activity.MODE_PRIVATE);
        FirmwareVersionInfo version = new FirmwareVersionInfo();
        version.setMajorVersion(pref.getInt(FirmwareVersionInfo.DEVICE_MAJORVERSION_KEY, -1));
        version.setMinorVersion(pref.getInt(FirmwareVersionInfo.DEVICE_MINORVERSION_KEY, -1));
        return version;
    }

    @Background
    void checkServerVersion()
    {
        FirmwareVersionInfo mServerFirmwareVersionInfo = null;
        try {
            mServerFirmwareVersionInfo = firmwareUpdateServiceClient.getLastVersion( true , mPrefixSerialNumber );
        }catch(RestClientException e){
            Log.e( TAG , "서버 펌웨어를 읽어 오는 중에 오류가 발생하였습니다.");
        }

        final FirmwareVersionInfo deviceFirmwareVersion = this.getDeviceFirmwareVersionInfo();

        String firmwareVersionState = deviceFirmwareVersion.getMajorVersion() + "." + deviceFirmwareVersion.getMinorVersion();
        if (mServerFirmwareVersionInfo == null) {
            lastestFirmware = false;
        }else{

            if (deviceFirmwareVersion.getMajorVersion() == mServerFirmwareVersionInfo.getMajorVersion()) {
                if (deviceFirmwareVersion.getMinorVersion() < mServerFirmwareVersionInfo.getMinorVersion()) {
                    firmwareVersionState = this.getResources().getString( R.string.firmware_update_setting_available );
                    lastestFirmware = false;
                }
            }
        }

        checkServerVersionComplete( firmwareVersionState );
    }

    private void checkServerVersionComplete(final String firmwareVersionState ){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tvFirmwareUpdate.setText(firmwareVersionState);
            }
        });
    }

}
