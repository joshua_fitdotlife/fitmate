package com.fitdotlife.fitmate;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.fitdotlife.fitmate.alarm.FitmeterNotUseAlarmReceiver;
import com.fitdotlife.fitmate.alarm.WearNotiAlarmReceiver;
import com.fitdotlife.fitmate_lib.customview.FitmeterDialog;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.http.NetworkClass;
import com.fitdotlife.fitmate_lib.http.RestHttpErrorHandler;
import com.fitdotlife.fitmate_lib.http.UserInfoService;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.object.UserNotiSetting;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringRes;
import org.androidannotations.rest.spring.annotations.RestService;
import org.springframework.web.client.RestClientException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

@EActivity(R.layout.activity_alarm_setting)
public class AlarmSettingActivity extends Activity {

    private final String TAG = AlarmSettingActivity.class.getSimpleName();

    private final int USERINFO_CHANGEALARM_CODE = 0;

    private UserInfo mUserInfo = null;
    private UserNotiSetting mUserNotiSetting = null;
    private FitmateDBManager mDBManager = null;

    @ViewById(R.id.ic_activity_setting_alarm_actionbar)
    View actionBarView;

    @ViewById(R.id.sw_activity_alarm_setting_mynews_today_activity)
    Switch swMyNewsTodayActivity;

    @ViewById(R.id.sw_activity_alarm_setting_mynews_week_activity)
    Switch swMyNewsWeekActivity;

    @ViewById(R.id.sw_activity_alarm_setting_mynews_battery)
    Switch swMyNewsBattery;

    @ViewById(R.id.sw_activity_alarm_setting_mynews_fitmeter_notuse)
    Switch swMyNewsFitmeterNotUse;

    @ViewById(R.id.sw_activity_alarm_mynews_setting_wear)
    Switch swMyNewsFitmeterWear;

    @ViewById(R.id.sw_activity_alarm_setting_friendnews_report)
    Switch swFriendNewsReport;

    @ViewById(R.id.sw_activity_alarm_setting_friendnews_today_activity)
    Switch swFriendTodayActivity;

    @ViewById(R.id.sw_activity_alarm_setting_friendnews_week_activity)
    Switch swFriendWeekActivity;

    @ViewById(R.id.tv_activity_alarm_setting_alarm_set_title)
    TextView tvAlarmTimeTitle;

    @ViewById(R.id.tv_activity_alarm_setting_alarm_set)
    TextView tvAlarmTime;


    @ViewById(R.id.rl_activity_alarm_setting_receive_alarm)
    RelativeLayout rlReceiveAlarm;

    @ViewById(R.id.rl_activity_alarm_setting_alarmtime)
    RelativeLayout rlAlarmTime;

    @ViewById(R.id.tv_activity_alarm_setting_receive_alarm)
    TextView tvReceiveAlarm;

    @StringRes(R.string.setting_alarm_bar_title)
    String strSettingAlarmBarTitle;

    @RestService
    UserInfoService userInfoServiceClient;

    @Bean
    RestHttpErrorHandler restErrorHandler;

    @AfterViews
    void onInit(){

        userInfoServiceClient.setRootUrl(NetworkClass.baseURL + "/api");

        this.mDBManager = new FitmateDBManager(this);
        this.mUserInfo = this.mDBManager.getUserInfo();

        displayActionBar();

        getUserNotiSetting();

        swMyNewsFitmeterNotUse.setOnCheckedChangeListener(new Switch.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    setFitmeterNotUseAlarm();
                }else{
                    cancelFitmeterNotUseAlarm();
                }
            }
        });

        swFriendNewsReport.setOnCheckedChangeListener(new Switch.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                swFriendTodayActivity.setChecked(isChecked);
                swFriendWeekActivity.setChecked(isChecked);

                swFriendTodayActivity.setEnabled(isChecked);
                swFriendWeekActivity.setEnabled(isChecked);
                rlReceiveAlarm.setEnabled( isChecked );

                if ( isChecked ) {
                    tvReceiveAlarm.setTextColor(0xFF3C3C3C);
                } else {
                    tvReceiveAlarm.setTextColor(0x303C3C3C);
                }
            }
        });

        swMyNewsFitmeterWear.setOnCheckedChangeListener(new Switch.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                rlAlarmTime.setEnabled(isChecked);

                if(isChecked) {
                    tvAlarmTimeTitle.setTextColor( 0xFF3C3C3C );
                    tvAlarmTime.setTextColor(0xFF3C3C3C);
                    setWearNotiAlarm( getAlarmTime() );
                }else{

                    tvAlarmTimeTitle.setTextColor( 0x303C3C3C );
                    tvAlarmTime.setTextColor(0x303C3C3C);
                    cancelWearNotiAlarm();
                }
            }
        });
    }

    @Background
    void getUserNotiSetting(){

        try {
            UserNotiSetting userNotiSetting = userInfoServiceClient.getUserNotiSetting(mUserInfo.getEmail());

            if( userNotiSetting != null )
            {
                mUserNotiSetting = userNotiSetting;
                this.mDBManager.setUserNotiSetting( userNotiSetting );

            }else{
                mUserNotiSetting = new UserNotiSetting();
            }

            displayUserNotiSetting();

        }catch ( RestClientException e ){
            Log.e("fitmate", e.getMessage());
            displayMessage( "설정을 가져오는 중에 오류가 발생하였습니다." );
            finish();
        }
    }

    @UiThread
    void displayMessage( String message ){
        Toast.makeText(this , message , Toast.LENGTH_SHORT).show();
    }

    @UiThread
    void displayUserNotiSetting(  ){

        swMyNewsTodayActivity.setChecked( mUserNotiSetting.isMytodayactivitynews() );
        swMyNewsWeekActivity.setChecked(mUserNotiSetting.isMyweekactivitynews() );
        swMyNewsBattery.setChecked(mUserNotiSetting.isBatteryusagenoti());
        swMyNewsFitmeterNotUse.setChecked(mUserNotiSetting.isFitmeternotusednoti());
        swMyNewsFitmeterWear.setChecked(mUserNotiSetting.isFitmeterwearnoti());
        tvAlarmTime.setText(mUserNotiSetting.getAlarmtime());
        rlAlarmTime.setEnabled(mUserNotiSetting.isFitmeterwearnoti());

        swFriendNewsReport.setChecked(mUserNotiSetting.isFriendnews());
        swFriendTodayActivity.setChecked(mUserNotiSetting.isFriendtodayactivitynews());
        swFriendWeekActivity.setChecked(mUserNotiSetting.isFriendweekactivitynews());

        if( mUserNotiSetting.isFriendnews() == false )
        {
            swFriendTodayActivity.setEnabled(false);
            swFriendWeekActivity.setEnabled(false);
            rlReceiveAlarm.setEnabled(false);
            tvReceiveAlarm.setTextColor( 0x303C3C3C );
        }

        //알람을 설정해야 한다.

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        updateUserNotiSetting();
    }

    @Background
    void updateUserNotiSetting(){

        boolean result = false;
        try {

            AlarmSettingActivity.setChangeNewsSettingPreference(this, mUserNotiSetting.isMytodayactivitynews() != swMyNewsTodayActivity.isChecked());
            mUserNotiSetting.setMytodayactivitynews( swMyNewsTodayActivity.isChecked() );
            AlarmSettingActivity.setChangeNewsSettingPreference( this , mUserNotiSetting.isMyweekactivitynews() != swMyNewsWeekActivity.isChecked()  );
            mUserNotiSetting.setMyweekactivitynews(swMyNewsWeekActivity.isChecked());

            mUserNotiSetting.setBatteryusagenoti(swMyNewsBattery.isChecked());
            mUserNotiSetting.setFitmeternotusednoti(swMyNewsFitmeterNotUse.isChecked());
            mUserNotiSetting.setFitmeterwearnoti(swMyNewsFitmeterWear.isChecked());
            mUserNotiSetting.setAlarmtime( tvAlarmTime.getText().toString() );

            if(mUserNotiSetting.isFriendnews() == swFriendNewsReport.isChecked() ){ //친구 뉴스가 같다면

                if( swFriendNewsReport.isChecked() ){
                    AlarmSettingActivity.setChangeNewsSettingPreference(this, mUserNotiSetting.isFriendtodayactivitynews() != swFriendTodayActivity.isChecked());
                    AlarmSettingActivity.setChangeNewsSettingPreference(this, mUserNotiSetting.isFriendweekactivitynews() != swFriendWeekActivity.isChecked());
                }
            }else{ //달라졌다면

                AlarmSettingActivity.setChangeNewsSettingPreference(this, mUserNotiSetting.isFriendtodayactivitynews() != swFriendTodayActivity.isChecked());
                AlarmSettingActivity.setChangeNewsSettingPreference(this, mUserNotiSetting.isFriendweekactivitynews() != swFriendWeekActivity.isChecked());

            }

            mUserNotiSetting.setFriendnews(swFriendNewsReport.isChecked());
            mUserNotiSetting.setFriendtodayactivitynews(swFriendTodayActivity.isChecked());
            mUserNotiSetting.setFriendweekactivitynews(swFriendWeekActivity.isChecked());

            result = userInfoServiceClient.cuUserNotiSetting(mUserNotiSetting, mUserInfo.getEmail());
        }catch(RestClientException e){
            result = false;
        }

        showUpdateResult(result);
    }

    @UiThread
    void showUpdateResult( boolean result ){
        if( result){
            Toast.makeText(this , "저장되었습니다.", Toast.LENGTH_SHORT);
            mDBManager.setUserNotiSetting( mUserNotiSetting );
        }else{
            Toast.makeText(this , "저장 중에 문제가 발생했습니다.", Toast.LENGTH_SHORT);
        }
    }

    private void displayActionBar( ){
        actionBarView.setBackgroundColor(this.getResources().getColor(R.color.fitmate_bar_gray));
        TextView tvBarTitle = (TextView) actionBarView.findViewById( R.id.tv_custom_action_bar_title );
        tvBarTitle.setText(strSettingAlarmBarTitle);

        ImageView imgBack = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_left);
        imgBack.setVisibility(View.GONE);

        ImageView mImgRight = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_right);
        mImgRight.setVisibility(View.GONE);
    }

    @Click(R.id.rl_activity_alarm_setting_alarmtime)
    void alarmTimeClick(){

        Calendar cal = getAlarmTime();

        final FitmeterDialog fDialog = new FitmeterDialog(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View contentView = inflater.inflate(R.layout.dialog_time_select, null);

        TextView tvTitle = (TextView) contentView.findViewById(R.id.tv_dialog_time_select_title);
        tvTitle.setText(this.getString(R.string.myprofile_alarm_setting_time_title));

        final TimePicker tp = (TimePicker) contentView.findViewById(R.id.timePicker);
        tp.setCurrentHour(cal.get(Calendar.HOUR_OF_DAY));
        tp.setCurrentMinute(cal.get(Calendar.MINUTE));

        fDialog.setContentView(contentView);

        View buttonView = inflater.inflate( R.layout.dialog_button_two , null );
        Button btnLeft = (Button) buttonView.findViewById(R.id.btn_dialog_button_two_left);
        btnLeft.setText(this.getString(R.string.common_cancel));
        btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fDialog.close();
            }
        });

        Button btnRight = (Button) buttonView.findViewById(R.id.btn_dialog_button_two_right);
        btnRight.setText(this.getString(R.string.myprofile_alarm_setting_time));
        btnRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvAlarmTime.setText(String.format("%02d:%02d", tp.getCurrentHour(), tp.getCurrentMinute()));
                //이전 알람을 지운다.
                cancelWearNotiAlarm();
                //현재 시간으로 알람을 다시 지정한다.
                setWearNotiAlarm( tp.getCurrentHour(), tp.getCurrentMinute() );

                fDialog.close();
            }
        });
        fDialog.setButtonView( buttonView );
        fDialog.show();
    }

    private void setWearNotiAlarm(int hour, int minute)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);

        setWearNotiAlarm(calendar);
    }

    private void setWearNotiAlarm(Calendar tempCalendar)
    {
        Calendar alarmCalendar = Calendar.getInstance();
        alarmCalendar.set( Calendar.HOUR_OF_DAY , tempCalendar.get( Calendar.HOUR_OF_DAY ) );
        alarmCalendar.set( Calendar.MINUTE , tempCalendar.get( Calendar.MINUTE ) );

        AlarmManager alarmManager = (AlarmManager) getSystemService( Context.ALARM_SERVICE );
        Intent intent = new Intent( AlarmSettingActivity.this , WearNotiAlarmReceiver.class);
        PendingIntent alarmIntent = PendingIntent.getBroadcast( this , 0 , intent , 0 );

        Calendar currentTimeCalendar = Calendar.getInstance();
        long alarmTimeMilliSecond = alarmCalendar.getTimeInMillis();
        long currentTimeMilliSecond = currentTimeCalendar.getTimeInMillis();
        long timeGap = alarmTimeMilliSecond - currentTimeMilliSecond;

        Log.e(TAG , "Time Gap : " + timeGap );

        if( timeGap > 0 ){
            alarmManager.set( AlarmManager.ELAPSED_REALTIME_WAKEUP , SystemClock.elapsedRealtime() + timeGap , alarmIntent );
        }else{
            alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + timeGap + AlarmManager.INTERVAL_DAY, alarmIntent);
        }
    }

    private void cancelWearNotiAlarm()
    {
        AlarmManager alarmManager = (AlarmManager) getSystemService( Context.ALARM_SERVICE );
        Intent intent = new Intent( AlarmSettingActivity.this , WearNotiAlarmReceiver.class);
        PendingIntent alarmIntent = PendingIntent.getBroadcast( this , 0 , intent , 0 );
        alarmManager.cancel(alarmIntent);
    }

    private void setFitmeterNotUseAlarm()
    {
        long connectedTime = mDBManager.getLastConnectedTime(mUserInfo.getEmail());
        Calendar currentTimeCalendar = Calendar.getInstance();
        long currentTimeMillisecond = currentTimeCalendar.getTimeInMillis();
        long alarmTime = connectedTime + (5 * 60 * 1000);

        if( alarmTime > currentTimeCalendar.getTimeInMillis() )
        {
            AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            Intent intent = new Intent(AlarmSettingActivity.this, FitmeterNotUseAlarmReceiver.class);
            PendingIntent alarmIntent = PendingIntent.getBroadcast(this, 0, intent, 0);
            alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP,  SystemClock.elapsedRealtime() + ( alarmTime - currentTimeMillisecond )  , alarmIntent);
        }

    }

    private void cancelFitmeterNotUseAlarm()
    {
        AlarmManager alarmManager = (AlarmManager) getSystemService( Context.ALARM_SERVICE );
        Intent intent = new Intent( AlarmSettingActivity.this , FitmeterNotUseAlarmReceiver.class);
        PendingIntent alarmIntent = PendingIntent.getBroadcast( this , 0 , intent , 0 );
        alarmManager.cancel(alarmIntent);
    }

    @OnActivityResult(USERINFO_CHANGEALARM_CODE)
    void alarmSetResult( int resultCode , Intent data )
    {
        String alarmTime = data.getStringExtra("alarmtime");
        tvAlarmTime.setText(alarmTime);
    }

    @Click(R.id.rl_activity_alarm_setting_receive_alarm)
    void showReceiveAlarmActivity(){
        FriendAlarmSettingActivity_.intent( AlarmSettingActivity.this ).start();
    }

    public static boolean getChangeNewsSettingPreference( Context context )
    {
        SharedPreferences userInfo = context.getSharedPreferences("fitmate", Activity.MODE_PRIVATE);
        return userInfo.getBoolean( "changenewssetting" , true);
    }

    public static void setChangeNewsSettingPreference( Context context , boolean isSet)
    {
        SharedPreferences userInfo = context.getSharedPreferences( "fitmate" , Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = userInfo.edit();
        editor.putBoolean( "changenewssetting" , isSet);
        editor.commit();
    }

    private Calendar getAlarmTime(){
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm", Locale.getDefault());
        try {
            cal.setTime(sdf.parse( tvAlarmTime.getText().toString() ));
        } catch (ParseException e) {
            e.printStackTrace();
            cal = null;
        }

        return cal;
    }

    private boolean getFitmeterNotUseNoti()
    {
        SharedPreferences pref = getSharedPreferences(getString(R.string.app_name), Activity.MODE_PRIVATE);
        boolean fitmeterNotUseNoti = pref.getBoolean("FitmeterNotUseNoti", true);
        return fitmeterNotUseNoti;
    }

}
