package com.fitdotlife.fitmate;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fitdotlife.fitmate_lib.customview.FriendProfileCommonView;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.http.FriendService;
import com.fitdotlife.fitmate_lib.http.NetworkClass;
import com.fitdotlife.fitmate_lib.http.RestHttpErrorHandler;
import com.fitdotlife.fitmate_lib.key.FriendStatusType;
import com.fitdotlife.fitmate_lib.object.UserFriend;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.rest.spring.annotations.RestService;
import org.apache.log4j.Log;

@EActivity(R.layout.activity_friend_profile_agree)
public class FriendProfileAgreeActivity extends Activity {

    private int mInsertID;

    @RestService
    FriendService friendServiceClient;

    @ViewById(R.id.txt_activity_friend_profile_agree_introself)
    TextView txtIntroSelf;

    @ViewById(R.id.fpv_activity_friend_profile_agree)
    protected FriendProfileCommonView commonView;

    @ViewById(R.id.ic_activity_friend_actionbar)
    View actionBarView;

    @Bean
    RestHttpErrorHandler restErrorHandler;

    @AfterViews
    void Init(){

        friendServiceClient.setRootUrl(NetworkClass.baseURL + "/api");
        friendServiceClient.setRestErrorHandler( restErrorHandler );

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        UserFriend userFriend = extras.getParcelable("UserFriend");

        String imageName = userFriend.getProfileImagePath();
        if( !( imageName == null ) ) {
            commonView.setProfileImage(imageName);
        }

        String name =  userFriend.getName();
        commonView.setName( name);
        this.displayActionBar(name);

         txtIntroSelf.setText(userFriend.getIntroYourSelf());
         this.mInsertID = userFriend.getUserFriendsID();
    }

    private void displayActionBar( String name ){
        actionBarView.setBackgroundColor(this.getResources().getColor(R.color.fitmate_blue));
        TextView tvBarTitle = (TextView) actionBarView.findViewById( R.id.tv_custom_action_bar_title );
        tvBarTitle.setText(name + " " + this.getString(R.string.friend_friendprofile_title));
        ImageView imgBack = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_left);
        imgBack.setVisibility(View.GONE);

        ImageView mImgRight = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_right);
        mImgRight.setVisibility(View.GONE);
    }

    @Click(R.id.btn_activity_friend_profile_agree_accept)
    void acceptClick(){
        this.changeStatus(FriendStatusType.ACCEPT);
    }

    @Click(R.id.btn_activity_friend_profile_agree_decline)
    void declineClick(){
        this.changeStatus(FriendStatusType.DECLINE);
    }

    @Background
    void changeStatus(  FriendStatusType statusType  ){

        String email = new FitmateDBManager(this).getUserInfo().getEmail();
        boolean result = friendServiceClient.changeStatus( email , mInsertID , statusType.getValue());

        if( result ){
            this.finish();
        }else{
            Toast.makeText(this, getResources().getString(R.string.failedtoacceptfriend), Toast.LENGTH_SHORT).show();

        }
    }

}
