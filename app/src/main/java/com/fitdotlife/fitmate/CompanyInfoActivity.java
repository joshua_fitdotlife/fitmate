package com.fitdotlife.fitmate;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.util.GmailSender;
import com.fitdotlife.fitmate_lib.util.Utils;

import java.io.File;
import java.io.StringWriter;


public class CompanyInfoActivity extends Activity {

    private LinearLayout llMenu = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_info);

        ActivityManager.getInstance().addActivity(this);
        
        View actionBarView = this.findViewById(R.id.ic_activity_main_actionbar);
        TextView tvBarTitle = (TextView) actionBarView.findViewById( R.id.tv_custom_action_bar_title );
        ImageView imgLeft = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_left);
        imgLeft.setVisibility(View.GONE);
        ImageView mImgRight = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_right);
        mImgRight.setVisibility(View.GONE);
        tvBarTitle.setText(this.getString(R.string.help_bar_title));


        String version = "0.0.0";
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        final String fversion = version;


        ((TextView) findViewById( R.id.tv_company_version )).setText(String.format("Version V%s", version));




        final FitmateDBManager dbManager = new FitmateDBManager(this);

        final Activity act = this;

        ((LinearLayout) findViewById(R.id.ll_company_report)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!Utils.isOnline(getApplicationContext() )){
                    Toast.makeText(getApplicationContext() , getString(R.string.common_connect_network) , Toast.LENGTH_SHORT).show();
                    return;
                }

                final String  title = "[로그] Fitmate " + dbManager.getUserInfo().getEmail() + " " + dbManager.getUserInfo().getDeviceAddress();

                final StringWriter content = new StringWriter();
                content.write(String.format("사용자 이메일 : %s\n", dbManager.getUserInfo().getEmail()));
                content.write(String.format("MODEL : %s\n", Build.MODEL));
                content.write(String.format("OS version : %s\n", System.getProperty("os.version")));
                content.write(String.format("SDK version : %s\n", android.os.Build.VERSION.SDK_INT));
                content.write(String.format("DEVICE : %s\n", android.os.Build.DEVICE));
                content.write(String.format("PRODUCT : %s\n", Build.PRODUCT));
                content.write(String.format("사용자 이름 : %s\n", dbManager.getUserInfo().getName()));
                content.write(String.format("장치 아이디 : %s\n", dbManager.getUserInfo().getDeviceAddress()));
                content.write(String.format("인센티브 사용 여부 : %s\n", dbManager.getUserInfo().getIncentiveUser()));
                content.write(String.format("착용 위치 : %s\n", dbManager.getUserInfo().getWearAt()));
                content.write(String.format("App version : %s\n", fversion));

                File root = Environment.getExternalStorageDirectory();
                final File file = null;
                if (!file.exists() || !file.canRead()) {
                    Toast.makeText(getApplicationContext(), "Attachment Error", Toast.LENGTH_SHORT).show();
                    //finish();
                    return;
                }

//                final Dialog mDialog = new Dialog(act , R.style.CustomDialog);
//                mDialog.setContentView(R.layout.dialog_report_error);
//                mDialog.findViewById(R.id.img_search_email_dialog_close).setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        mDialog.dismiss();
//                    }
//                });
//
//
//                final EditText etContent = (EditText) mDialog.findViewById(R.id.etx_dialog_search_password_search_content);
//
//                mDialog.findViewById(R.id.btn_search_email).setOnClickListener(new View.OnClickListener() {
//
//                    @Override
//                    public void onClick(View v) {
//                        content.write("추가 로그 : \n" + etContent.getText().toString());
//                        new GmailSender(title, content.toString(), file.getAbsolutePath());
//                        Toast.makeText(act, getString(R.string.help_send_report_success) , Toast.LENGTH_SHORT).show();
//                        mDialog.dismiss();
//                    }
//                });
//                mDialog.show();

                //startActivity(Intent.createChooser(intent, "Send Email"));
                return;
            }
        });

        this.findViewById(R.id.ll_activity_companyinfo_help).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent helpIntent = new Intent(CompanyInfoActivity.this, HelpListActivity.class);
                startActivity(helpIntent);
            }
        });

        this.findViewById(R.id.ll_activity_companyinfo_faq).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent faqIntent = new Intent(CompanyInfoActivity.this, FAQActivity.class);
                startActivity(faqIntent);
            }
        });

        this.findViewById(R.id.ll_activity_companyinfo_service_agreement).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent serviecAgreementIntent = new Intent(CompanyInfoActivity.this, ServiceAgreementActivity.class);
                startActivity(serviecAgreementIntent);
            }
        });

    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);     // 여기서 this는 Activity의 this

// 여기서 부터는 알림창의 속성 설정
        builder.setTitle(getString(R.string.common_quit_title))        // 제목 설정
                .setMessage(getString(R.string.common_quit_content))        // 메세지 설정
                .setCancelable(true)        // 뒤로 버튼 클릭시 취소 가능 설정
                .setPositiveButton(R.string.common_yes, new DialogInterface.OnClickListener() {
                    // 확인 버튼 클릭시 설정
                    public void onClick(DialogInterface dialog, int whichButton) {
                        moveTaskToBack(true);

                        finish();

                        //  android.os.Process.killProcess(android.os.Process.myPid());
                        dialog.cancel();

                        //   MainActivity.this.onBackPressed();
                    }
                })
                .setNegativeButton(R.string.common_no , new DialogInterface.OnClickListener() {
                    // 취소 버튼 클릭시 설정
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.cancel();
                    }
                });

        AlertDialog dialog = builder.create();    // 알림창 객체 생성
        dialog.show();    // 알림창 띄우기

    }


}
