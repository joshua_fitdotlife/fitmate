package com.fitdotlife.fitmate;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.widget.Toast;

import com.fitdotlife.fitmate.alarm.ActivityNotiAlarmReceiver;
import com.fitdotlife.fitmate.alarm.WearNotiAlarmReceiver;
import org.apache.log4j.Log;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.object.UserNotiSetting;
import com.fitdotlife.fitmate_lib.service.FitmateService;
import com.fitdotlife.fitmate_lib.service.alarm.AlarmService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Joshua on 2015-05-29.
 */
public class BootCompleteReceiver extends BroadcastReceiver{

    private String TAG = BootCompleteReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {

        //6시에 발생하는 알람 리시버를 등록 한다.
        setActivityNotiAlarm(context);

        UserNotiSetting userNotiSetting = new FitmateDBManager(context).getUserNotiSetting();
        if( userNotiSetting.isFitmeterwearnoti() )
        {
            //특정시간에 행하는 알람을 실행하는 리시버를 등록해야 한다.
            setWearNotiAlarm(context, userNotiSetting.getAlarmtime() );
        }
    }

    private void setActivityNotiAlarm(Context context)
    {
        Calendar alarmCalendar = Calendar.getInstance();
        alarmCalendar.set( Calendar.HOUR_OF_DAY, 18 );
        alarmCalendar.set( Calendar.MINUTE , 0 );

        Calendar currentTimeCalendar = Calendar.getInstance();
        AlarmManager alarmManager = (AlarmManager) context.getSystemService( Context.ALARM_SERVICE );
        Intent intent = new Intent( context , ActivityNotiAlarmReceiver.class);
        PendingIntent alarmIntent = PendingIntent.getBroadcast(context, 0, intent, 0);

        long alarmTimeMilliSecond = alarmCalendar.getTimeInMillis();
        long currentTimeMilliSecond = currentTimeCalendar.getTimeInMillis();
        long timeGap = alarmTimeMilliSecond - currentTimeMilliSecond;

        if( timeGap > 0 ){
            alarmManager.set( AlarmManager.ELAPSED_REALTIME_WAKEUP , SystemClock.elapsedRealtime() + timeGap , alarmIntent );
        }else{
            alarmManager.set( AlarmManager.ELAPSED_REALTIME_WAKEUP , SystemClock.elapsedRealtime() + timeGap + AlarmManager.INTERVAL_DAY , alarmIntent );
        }
    }

    private void setWearNotiAlarm(Context context , String alarmTime)
    {
        Calendar alarmCalendar = Calendar.getInstance();
        Calendar tempCalendar = Calendar.getInstance();

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm", Locale.getDefault() );
        try {
            tempCalendar.setTime(sdf.parse(alarmTime));
        } catch (ParseException e) {
            e.printStackTrace();
            alarmCalendar = null;
        }

        alarmCalendar.set(Calendar.HOUR_OF_DAY , tempCalendar.get( Calendar.HOUR_OF_DAY ) );
        alarmCalendar.set(Calendar.MINUTE , tempCalendar.get(Calendar.MINUTE) );
        
        Calendar currentTimeCalendar = Calendar.getInstance();
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent( context , WearNotiAlarmReceiver.class);
        PendingIntent alarmIntent = PendingIntent.getBroadcast(context, 0, intent, 0);

        long alarmTimeMilliSecond = alarmCalendar.getTimeInMillis();
        long currentTimeMilliSecond = currentTimeCalendar.getTimeInMillis();
        long timeGap = alarmTimeMilliSecond - currentTimeMilliSecond;

        if( timeGap > 0 ) {
            alarmManager.set( AlarmManager.ELAPSED_REALTIME_WAKEUP , SystemClock.elapsedRealtime() + timeGap , alarmIntent );
        }else{
            alarmManager.set( AlarmManager.ELAPSED_REALTIME_WAKEUP , SystemClock.elapsedRealtime() + timeGap + AlarmManager.INTERVAL_DAY , alarmIntent );
        }
    }
}
