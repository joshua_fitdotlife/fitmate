package com.fitdotlife.fitmate.adapter;

import android.content.Context;
import android.content.res.TypedArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fitdotlife.fitmate.R;
import com.fitdotlife.fitmate_lib.object.ExerciseProgram;

import java.util.List;

public class MyExerciseListAdapter extends AbstractBaseAdapter {

    private String TAG = "fitmate";
    private Context mContext = null;

	public MyExerciseListAdapter(Context context, int layoutResource, List<?> list)
	{
		super(context, layoutResource, list);
        this.mContext = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
    {
        ExerciseProgram exerciseProgram = (ExerciseProgram) mList.get(position);
        if( convertView == null )
        {
            convertView = this.mInflater.inflate(mResource , null);
        }

        if( exerciseProgram != null )
        {
            TypedArray arrCategoryColor = this.mContext.getResources().obtainTypedArray(R.array.disease_category_color);
            TypedArray arrCategoryImage = this.mContext.getResources().obtainTypedArray(R.array.disease_category_image);
            TypedArray arrCategoryText = this.mContext.getResources().obtainTypedArray(R.array.disease_category_name);

            String programName = null;
            if( exerciseProgram.getDefaultProgram() )
            {
                int resId =  mContext.getResources().getIdentifier( exerciseProgram.getName() , "string" , mContext.getPackageName());
                programName = mContext.getString(resId);
            }
            else
            {
                programName = exerciseProgram.getName();
            }


            TextView tvName = (TextView) convertView.findViewById( R.id.tv_listview_activity_execisehome_myexercise_name );
            tvName.setText( programName );
            convertView.setTag(exerciseProgram.getId());

            TextView tvCategoryName = (TextView) convertView.findViewById(R.id.tv_listview_item_activity_exercisehome_myexercise_category_name);
            tvCategoryName.setText(arrCategoryText.getString(exerciseProgram.getDiseaseCategory()));
            tvCategoryName.setTextColor( arrCategoryColor.getColor(exerciseProgram.getDiseaseCategory() , 0) );

            ImageView imgCategory = (ImageView) convertView.findViewById(R.id.img_listview_item_activity_exercisehome_myexercise_category);
            imgCategory.setImageResource( arrCategoryImage.getResourceId(exerciseProgram.getDiseaseCategory() , 0) );
        }
        return convertView;
	}

    class ViewHolder{

    }

}