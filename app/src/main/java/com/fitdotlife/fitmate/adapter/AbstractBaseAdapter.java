package com.fitdotlife.fitmate.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.BaseAdapter;

import java.util.List;

public abstract class AbstractBaseAdapter extends BaseAdapter
{
	protected Context mContext;
	protected int mResource;
	protected List<?> mList;
	protected LayoutInflater mInflater;
	
	public AbstractBaseAdapter( Context context, int layoutResource, List<?> list )
	{
		super();
		this.mContext = context;
		this.mResource = layoutResource;
		this.mList = list;
		this.mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public int getCount() 
	{
		if(mList== null) return 0;
		return mList.size();
	}

	@Override
	public Object getItem(int position)
	{
		return mList.get(position);
	}

	@Override
	public long getItemId(int id ) 
	{
		return id;
	}
}
