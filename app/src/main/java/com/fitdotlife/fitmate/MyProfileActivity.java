package com.fitdotlife.fitmate;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.PermissionChecker;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.fitdotlife.fitmate_lib.customview.AppliedProgramSummaryLayout;
import com.fitdotlife.fitmate_lib.customview.DrawerView;
import com.fitdotlife.fitmate_lib.customview.FitmeterDialog;
import com.fitdotlife.fitmate_lib.http.HttpApi;
import com.fitdotlife.fitmate_lib.http.HttpResponse;
import com.fitdotlife.fitmate_lib.http.NetworkClass;
import com.fitdotlife.fitmate_lib.iview.IMyProfileView;
import com.fitdotlife.fitmate_lib.key.CommonKey;
import com.fitdotlife.fitmate_lib.key.GenderType;
import com.fitdotlife.fitmate_lib.object.ExerciseProgram;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.presenter.MyProfilePresenter;
import com.fitdotlife.fitmate_lib.util.ImageUtil;
import com.fitdotlife.fitmate_lib.util.Utils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MyProfileActivity extends Activity implements IMyProfileView, View.OnClickListener
{

    private final String TAG = "fitmate - " + MyProfileActivity.class.getSimpleName();

    private final int USERINFO_REQUEST_CODE = 0;
    private final int USERINFO_CHANGEDEVICE_CODE = 1;
    private final int USERINFO_CHANGELOCATION_CODE = 2;
    private final int USERINFO_CHANGEALARM_CODE = 3;
    protected static final int CAMERA_REQUEST = 4;

    private final int PERMISSION_REQUEST_STORAGE = 3;
    private Activity mActivity = null;
    //private LinearLayout llMenu = null;
    private ImageView imgProfile = null;

    private LinearLayout llUserInfo = null;
    private TextView tvName = null;
    private ImageView imgGender = null;
    private ImageView imgGenderDot = null;
    private TextView tvBirthDate = null;
    private ImageView imgBirthDateDot = null;
    private TextView tvEmail = null;
    private TextView tvIntro = null;
    private TextView tvProgramName = null;
    private AppliedProgramSummaryLayout appliedProgramLayout = null;

    private TextView tvHeight = null;
    private TextView tvAlarm = null;
    private TextView tvWeight = null;
    private TextView tvVO2MaxLevel = null;


    private FrameLayout flAlarm = null;
    private FrameLayout flHeight = null;
    private FrameLayout flWeight = null;
    private FrameLayout flVO2MaxLevel = null;
    private FrameLayout flWearingLocation = null;
    private FrameLayout flChangeFitmeter = null;
    private FrameLayout flLogout = null;

    private TextView tvWearingLocation = null;
    private TextView tvChangeFitmeter = null;
    private ImageView imgWearingLocation = null;

    private MyProfilePresenter mPresenter = null;
    private UserInfo mUserInfo = null;

    private boolean isShowing = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);

        this.mPresenter = new MyProfilePresenter(this , this);
        ActivityManager.getInstance().addActivity(this);

        this.mActivity = this;
        this.mUserInfo = this.mPresenter.getUserInfo();

        //this.llMenu = (LinearLayout) this.findViewById(R.id.activity_global_menu);

        this.llUserInfo = (LinearLayout) this.findViewById(R.id.ll_activity_my_profile_user_info);
        this.llUserInfo.setOnClickListener(this);

        final Activity act = this;

        this.imgProfile = (ImageView) this.findViewById(R.id.img_activity_my_profile_picture);

        imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!Utils.isOnline(act) ) {
                    //mView.showResult("인터넷을 연결하여 주십시오.");
                    Toast.makeText(act, getString(R.string.common_connect_network), Toast.LENGTH_SHORT).show();
                    return;
                }

                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ){

                    if( PermissionChecker.checkSelfPermission( MyProfileActivity.this , Manifest.permission_group.STORAGE ) == PackageManager.PERMISSION_GRANTED ){

                        showCameraActivity();

                    }else{

                        ActivityCompat.requestPermissions(MyProfileActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST_STORAGE);

                    }

                }else{

                    showCameraActivity();

                }
            }
        });

        this.tvName = (TextView) this.findViewById(R.id.tv_activity_my_profile_name);
        this.tvName.setText( this.mUserInfo.getName() );


        this.imgGenderDot = (ImageView) this.findViewById(R.id.img_activity_my_profile_gender_dot);
        this.imgGender = (ImageView) this.findViewById(R.id.img_activity_my_profile_gender);
        final AQuery aq = new AQuery(act);

        if(this.mUserInfo.getGender().equals( GenderType.NONE )){
            this.imgGender.setVisibility(View.GONE);
            this.imgGenderDot.setVisibility(View.GONE);
        }
        else
        {
            if (this.mUserInfo.getGender() == GenderType.MALE) {
                this.imgGender.setImageResource(R.drawable.profile_sex2);
            } else if (this.mUserInfo.getGender() == GenderType.FEMALE) {
                this.imgGender.setImageResource(R.drawable.profile_sex1);
            }
        }


//        try {
//            String ss = UserInfo.getPhotoUrlSharedPreference(getApplicationContext());
//            //String ss = mUserInfo.getAccountPhoto();
//
//            if(ss != null)
//            {
//                if(!(ss.equals("") || ss.equals("null"))) {
//                    final String finalSs = ss;
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            BitmapAjaxCallback cb = new BitmapAjaxCallback();
//                            cb.url(NetworkClass.imageBaseURL + finalSs).rotate(true);
//                            aq.id( imgProfile.getId() ).image(cb);
//                        }
//                    });
//                }
//            }
//        }
//        catch (Exception ex)
//        {
//
//        }

        String imageName = mUserInfo.getAccountPhoto();
        if( imageName != null )
        {
            String imgUri = null;
            if(imageName.startsWith("file://")){
                imgUri = imageName;
            }
            else{
                imgUri = NetworkClass.imageBaseURL + imageName;
            }

            int cornerRadius = imgProfile.getLayoutParams().width / 2;
            DisplayImageOptions options = new DisplayImageOptions.Builder()
                    .showImageForEmptyUri( R.drawable.profile_img1 )
                    .showImageOnFail( R.drawable.profile_img1 )
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .displayer(new RoundedBitmapDisplayer( cornerRadius ))
                    .build();

            ImageLoader.getInstance().displayImage( imgUri , imgProfile, options);
        }

        this.tvBirthDate = (TextView) this.findViewById(R.id.tv_activity_my_profile_birthdate);
        this.imgBirthDateDot = (ImageView) this.findViewById(R.id.img_activity_my_profile_birthdate_dot);
        long birthDataTimeMilliSecond = this.mUserInfo.getBirthDate();
        if(birthDataTimeMilliSecond > 0) {
            this.tvBirthDate.setText(this.getDateString(birthDataTimeMilliSecond));
        }else{
            this.tvBirthDate.setVisibility(View.GONE);
            this.imgBirthDateDot.setVisibility(View.GONE);
        }

        this.tvEmail = (TextView) this.findViewById(R.id.tv_activity_my_profile_email);
        this.tvEmail.setText( this.mUserInfo.getEmail() );

        this.tvIntro = (TextView) this.findViewById(R.id.tv_activity_my_profile_intro);
        if( this.mUserInfo.getIntro() != null ) {
            this.tvIntro.setText( this.mUserInfo.getIntro() );
        }

        ExerciseProgram program = this.mPresenter.getAppliedExerciseProgram();
        this.tvProgramName = (TextView) this.findViewById(R.id.tv_activity_my_profile_program_name);
        int resId =  getResources().getIdentifier(program.getName(), "string", getPackageName());
        String programName = getString(resId);
        this.tvProgramName.setText(programName);
        this.tvProgramName.setSelected(true);
        this.appliedProgramLayout = (AppliedProgramSummaryLayout) this.findViewById(R.id.apl_activity_my_profile_appliedprogram_info);
        this.appliedProgramLayout.setAppliedExerciseProgram(program);

        //Si가 있는지 확인한다.
        boolean isSi = mUserInfo.isSI();

        this.flHeight = (FrameLayout) this.findViewById(R.id.fl_activity_my_profile_height);
        this.tvHeight = (TextView) this.findViewById(R.id.tv_activity_my_profile_height);
        this.flHeight.setOnClickListener(this);

        if( mUserInfo.getHeight() > 0 ) {

            String heightString = String.valueOf( mUserInfo.getHeight() );
            String[] splitedHeightString = heightString.split("\\.");
            if( splitedHeightString.length > 1 ){
                tvHeight.setText(String.format("%s %s", splitedHeightString[0] + "." + splitedHeightString[1].substring(0, 1), UserInfo.getHeightUnit(isSi)) );
            }else{
                tvHeight.setText(String.format("%s %s", splitedHeightString[0] + ".0", UserInfo.getHeightUnit(isSi)));
            }

        }else{
            this.tvHeight.setText(this.getString(R.string.common_no_setting));
        }

        this.flWeight = (FrameLayout) this.findViewById( R.id.fl_activity_my_profile_weight );
        this.tvWeight = (TextView) this.findViewById(R.id.tv_activity_my_profile_weight);
        this.flWeight.setOnClickListener(this);
        if(mUserInfo.getWeight() > 0) {

            String weightString = String.valueOf( mUserInfo.getWeight() );
            String[] splitedWeightString = weightString.split("\\.");
            if( splitedWeightString.length > 1 ){
                tvWeight.setText(String.format("%s %s", splitedWeightString[0] + "." + splitedWeightString[1].substring(0, 1), UserInfo.getWeightUnit(isSi)));
            }else{
                tvWeight.setText(String.format("%s %s", splitedWeightString[0] + ".0", UserInfo.getWeightUnit(isSi)));
            }

        }else{
            this.tvWeight.setText(this.getString(R.string.common_no_setting));
        }


        this.flWearingLocation = (FrameLayout) this.findViewById(R.id.fl_activity_my_profile_wearinglocation);
        this.tvWearingLocation = (TextView) this.findViewById(R.id.tv_activity_my_profile_wearinglocation);
        this.flWearingLocation.setOnClickListener(this);
        if( mUserInfo.getDeviceAddress() != null && !mUserInfo.getDeviceAddress().equals("") && !mUserInfo.getDeviceAddress().equals("null") ) { //기기등록을 안했다면 기기 변경을 할 수 없다.
            this.tvWearingLocation.setText(this.getWearingLocationString(mUserInfo.getWearAt()));
        }else{
            this.tvWearingLocation.setText(this.getString(R.string.common_no_setting));
        }

        this.flChangeFitmeter = (FrameLayout) this.findViewById(R.id.fl_activity_my_profile_change_fitmeter);
        this.tvChangeFitmeter = (TextView) this.findViewById(R.id.tv_activity_my_profile_change_fitmeter);
        this.flChangeFitmeter.setOnClickListener(this);
        if( mUserInfo.getDeviceAddress() != null && !mUserInfo.getDeviceAddress().equals("") && !mUserInfo.getDeviceAddress().equals("null") ) { //기기등록을 안했다면 기기 변경을 할 수 없다.
        }else{
            this.tvChangeFitmeter.setText(this.getString(R.string.common_no_setting));
        }

        this.flLogout = (FrameLayout) this.findViewById(R.id.fl_activity_my_profile_logout);
        this.flLogout.setOnClickListener(this);

        this.flAlarm = (FrameLayout) this.findViewById(R.id.fl_activity_my_profile_alarm);

        this.tvAlarm = (TextView) this.findViewById(R.id.tv_myprofile_alarm);
        tvAlarm.setText(UserInfo.getAlarmSetSharedPreference(this)?this.getString(R.string.common_on) :this.getString(R.string.common_off));

        View actionBarView = this.findViewById(R.id.ic_activity_main_actionbar);
        actionBarView.setBackgroundColor( 0xFF3C3C3C );
        TextView tvBarTitle = (TextView) actionBarView.findViewById( R.id.tv_custom_action_bar_title );
        ImageView imgLeft = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_left);
        imgLeft.setVisibility(View.GONE);
        ImageView mImgRight = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_right);
        mImgRight.setImageResource(R.drawable.home_btn_sync);
        mImgRight.setOnClickListener(this);
        mImgRight.setVisibility(View.GONE);
        tvBarTitle.setText( R.string.myprofile_bar_title );

    }

    private Bitmap getCircleBitmap(int resourceId) {
        Bitmap bitmap = this.drawableToBitmap(this.getResources().getDrawable(resourceId));
        Bitmap output = Bitmap.createBitmap( bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);
        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        int size = (bitmap.getWidth()/2);
        canvas.drawCircle(size, size, size, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return output;
    }

    public Bitmap drawableToBitmap(Drawable drawable) {
        if (drawable == null) {
            return null;
        } else if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    @Override
    public void onClick(View view) {
        switch( view.getId() ){
            case R.id.fl_activity_my_profile_height:
                showHeightDialog();
                break;
            case R.id.fl_activity_my_profile_weight:
                showWeightDialog();
                break;
            case R.id.fl_activity_my_profile_wearinglocation:

                if( mUserInfo.getDeviceAddress() != null && !mUserInfo.getDeviceAddress().equals("") && !mUserInfo.getDeviceAddress().equals("null") ) { //기기등록을 안했다면 기기 변경을 할 수 없다.

                    if( mUserInfo.getDeviceAddress().substring(3,4).equals( "4" ) ){ //밴드라면 착용위치에 진입할 수 없다.
                        final FitmeterDialog fDialog = new FitmeterDialog(this);
                        LayoutInflater inflater = this.getLayoutInflater();
                        View contentView = inflater.inflate(R.layout.dialog_default_content, null);
                        TextView tvTitle = (TextView) contentView.findViewById(R.id.tv_dialog_default_title);
                        tvTitle.setVisibility( View.GONE );

                        TextView tvContent = (TextView) contentView.findViewById(R.id.tv_dialog_default_content);
                        tvContent.setText(this.getString( R.string.drawer_cananotchangelocation));
                        fDialog.setContentView(contentView);

                        View buttonView = inflater.inflate(R.layout.dialog_button_one , null);
                        Button btnOne = (Button) buttonView.findViewById(R.id.btn_dialog_button_one);
                        btnOne.setText(this.getString(R.string.common_ok));
                        btnOne.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                fDialog.close();
                            }
                        });

                        fDialog.setButtonView(buttonView);
                        fDialog.show();

                        return;
                    }

                    if (!Utils.isOnline(this)) {
                        Toast.makeText(this, this.getString(R.string.myprofile_connect_network), Toast.LENGTH_SHORT).show();
                        return;
                    }


                    showWearingLocationActivity();

                }else{
                    showNoSettingDialog();
                }

                break;
            case R.id.fl_activity_my_profile_change_fitmeter:
                // todo 서비스 중지
                if( mUserInfo.getDeviceAddress() != null && !mUserInfo.getDeviceAddress().equals("") && !mUserInfo.getDeviceAddress().equals("null") ) { //기기등록을 안했다면 기기 변경을 할 수 없다.

                    if (!Utils.isOnline(this)) {
                        Toast.makeText(this, this.getString(R.string.myprofile_connect_network), Toast.LENGTH_LONG).show();
                        return;
                    }

                    mPresenter.uploadActivityData();
                }else{
                    showNoSettingDialog();
                }
                break;
            case R.id.fl_activity_my_profile_logout:
                //임시로 비밀번호 변경으로
                //FragmentManager fm = this.getFragmentManager();
                if( !Utils.isOnline(this) ){
                    Toast.makeText(this , this.getString(R.string.myprofile_connect_network)  , Toast.LENGTH_SHORT).show();
                    return;
                }

                ChangePasswordDialog fpDialog = new ChangePasswordDialog(this, this.mUserInfo.getEmail());
                fpDialog.show();

                break;
            case R.id.ll_activity_my_profile_user_info:
                if( !Utils.isOnline(this) ){
                    Toast.makeText(this , this.getString(R.string.myprofile_connect_network)  , Toast.LENGTH_SHORT).show();
                    return;
                }

                Intent userinfoChangeIntent = new Intent(MyProfileActivity.this , UserInfoChangeActivity.class);
                userinfoChangeIntent.putExtra(UserInfo.EMAIL_KEY , this.mUserInfo.getEmail());
                userinfoChangeIntent.putExtra(UserInfo.NAME_KEY ,this.mUserInfo.getName());
                userinfoChangeIntent.putExtra(UserInfo.HELLO_MESSAGE_KEY , this.mUserInfo.getIntro());
                userinfoChangeIntent.putExtra(UserInfo.BIRTHDATEKEY , this.mUserInfo.getBirthDate());
                userinfoChangeIntent.putExtra(UserInfo.SEX_KEY , this.mUserInfo.getGender().getValue());
                userinfoChangeIntent.putExtra(UserInfo.PROFILE_MAGE_KEY , this.mUserInfo.getAccountPhoto());

                this.startActivityForResult(userinfoChangeIntent, USERINFO_REQUEST_CODE);
                //this.startActivity(userinfoChangeIntent);
                break;
            case R.id.fl_activity_my_profile_alarm:
                Intent alarmChangeIntent = new Intent(MyProfileActivity.this , MyAlarmActivity.class);
                //this.startActivityForResult(alarmChangeIntent, USERINFO_REQUEST_CODE);
                this.startActivityForResult(alarmChangeIntent, USERINFO_CHANGEALARM_CODE);
                break;


                }
        }

    private void showHeightDialog(){

        if( mUserInfo.getHeight() <= 0 ){
            showNoSettingDialog();
            return;
        }

        if( !Utils.isOnline(this) ){
            Toast.makeText(this , this.getString(R.string.myprofile_connect_network)  , Toast.LENGTH_SHORT).show();
            return;
        }

        boolean heightUnit = mUserInfo.isSI();

        final FitmeterDialog heightDialog = new FitmeterDialog(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View numberPickerView = inflater.inflate(R.layout.dialog_number_picker, null);
        TextView tvTitle = (TextView) numberPickerView.findViewById(R.id.tv_dialog_number_picker_title);
        tvTitle.setText(this.getString(R.string.common_height));
        final NumberPicker np = (NumberPicker) numberPickerView.findViewById(R.id.np_dialog_number_picker);
        np.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        final NumberPicker pp = (NumberPicker) numberPickerView.findViewById(R.id.np_dialog_point_picker);
        pp.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        final NumberPicker up = (NumberPicker) numberPickerView.findViewById(R.id.np_dialog_unit_picker);
        up.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        up.setDisplayedValues(new String[]{this.getString(R.string.common_centimeter), this.getString(R.string.common_ft)});

        float height = this.mUserInfo.getHeight();
        np.setMaxValue(300);
        np.setMinValue(1);
        pp.setMaxValue(9);
        pp.setMinValue(0);
        up.setMaxValue(1);
        up.setMinValue(0);

        String heightString = String.valueOf( height );
        String[] splitedHeightString = heightString.split("\\.");
        if( splitedHeightString.length > 1 ){
            np.setValue( Integer.parseInt( splitedHeightString[0] ) );
            pp.setValue(  Integer.parseInt( splitedHeightString[1].substring( 0 , 1) ) );
        }else{
            np.setValue( Integer.parseInt( splitedHeightString[0] ) );
            pp.setValue(0);
        }

        if( heightUnit )
        {
            up.setValue(0);
        }else{

            up.setValue(1);
        }
        up.setTag(height);
        heightDialog.setContentView(numberPickerView);

        up.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                boolean heightSi = (newVal == 0);
                float tempHeight = (float) up.getTag();

                //몸무게를 변환한다.
                if (heightSi) {
                    //파운드에서 킬로그램으로 변환한다.
                    tempHeight = com.fitdotlife.fitmate_lib.service.util.Utils.ft_to_cm(tempHeight);
                } else {
                    //킬로그램에서 파운드로 변환한다.
                    tempHeight = com.fitdotlife.fitmate_lib.service.util.Utils.cm_to_ft(tempHeight);
                }

                String heightString = String.valueOf( tempHeight );
                String[] splitedHeightString = heightString.split("\\.");
                if( splitedHeightString.length > 1 ){
                    np.setValue( Integer.parseInt( splitedHeightString[0] ) );
                    pp.setValue(  Integer.parseInt( splitedHeightString[1].substring( 0 , 1) ) );
                }else{
                    np.setValue( Integer.parseInt( splitedHeightString[0] ) );
                    pp.setValue(0);
                }

                up.setTag(tempHeight);

            }
        });

        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                up.setTag((float) (((np.getValue() * 10d) + pp.getValue()) / 10d));
            }
        });

        pp.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                up.setTag((float) (((np.getValue() * 10d) + pp.getValue()) / 10d));
            }
        });

        View buttonView = inflater.inflate( R.layout.dialog_button_two , null );
        Button btnLeft = (Button) buttonView.findViewById(R.id.btn_dialog_button_two_left);
        btnLeft.setText(this.getString(R.string.common_cancel));
        btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                heightDialog.close();
            }
        });
        Button btnRight = (Button) buttonView.findViewById(R.id.btn_dialog_button_two_right);
        btnRight.setText(this.getString(R.string.common_ok));
        btnRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean heightSi = up.getValue() == 0;
                boolean isSi = mUserInfo.isSI();
                float tempWeight = mUserInfo.getWeight();
                String weightUnitString = "";

                if (heightSi != isSi) {
                    //몸무게를 변환한다.
                    if (heightSi) {
                        //파운드에서 킬로그램으로 변환한다.
                        tempWeight = com.fitdotlife.fitmate_lib.service.util.Utils.lb_to_kg(tempWeight);
                        weightUnitString = getString(R.string.common_kilogram);
                    } else {
                        //킬로그램에서 파운드로 변환한다.
                        tempWeight = com.fitdotlife.fitmate_lib.service.util.Utils.kg_to_lb(tempWeight);
                        weightUnitString = getString(R.string.common_pound);
                    }

                    String weightString = String.valueOf(tempWeight);
                    String[] splitedWeightString = weightString.split("\\.");
                    if (splitedWeightString.length > 1) {
                        tvWeight.setText(String.format("%s %s", splitedWeightString[0] + "." + splitedWeightString[1].substring(0, 1), weightUnitString));
                    } else {
                        tvWeight.setText(String.format("%s %s", splitedWeightString[0] + ".0", weightUnitString));
                    }

                }
                mUserInfo.setHeight((float) up.getTag());
                mUserInfo.setWeight(tempWeight);
                mUserInfo.setIsSI(heightSi);
                mPresenter.modifyUserInfo(mUserInfo);

                String heightString = String.valueOf(mUserInfo.getHeight());
                String[] splitedHeightString = heightString.split("\\.");
                if (splitedHeightString.length > 1) {
                    tvHeight.setText(String.format("%s %s", splitedHeightString[0] + "." + splitedHeightString[1].substring(0, 1), up.getDisplayedValues()[up.getValue()]));
                } else {
                    tvHeight.setText(String.format("%s %s", splitedHeightString[0] + ".0", up.getDisplayedValues()[up.getValue()]));
                }

                mPresenter.ChangeUserInfo();
                heightDialog.close();
            }
        });
        heightDialog.setButtonView(buttonView);
        heightDialog.show();
    }

    private void showWeightDialog(){
        if( mUserInfo.getWeight() <= 0 ){
            showNoSettingDialog();
            return;
        }

        if( !Utils.isOnline(this) ){
            Toast.makeText(this , this.getString(R.string.myprofile_connect_network)  , Toast.LENGTH_SHORT).show();
            return;
        }

        boolean weightUnit = mUserInfo.isSI();

        final FitmeterDialog weightDialog = new FitmeterDialog(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View numberPickerViews = this.getLayoutInflater().inflate(R.layout.dialog_number_picker, null);
        TextView tvTitle = (TextView) numberPickerViews.findViewById(R.id.tv_dialog_number_picker_title);
        tvTitle.setText(this.getString(R.string.common_weight));
        final NumberPicker nps = (NumberPicker) numberPickerViews.findViewById(R.id.np_dialog_number_picker);
        nps.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        final NumberPicker pps = (NumberPicker) numberPickerViews.findViewById(R.id.np_dialog_point_picker);
        pps.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        final NumberPicker ups = (NumberPicker) numberPickerViews.findViewById(R.id.np_dialog_unit_picker);
        ups.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        ups.setDisplayedValues(new String[]{this.getString(R.string.common_kilogram), this.getString(R.string.common_pound)});

        final float weight = this.mUserInfo.getWeight();

        nps.setMaxValue(500);
        nps.setMinValue(1);
        pps.setMaxValue(9);
        pps.setMinValue(0);
        ups.setMaxValue(1);
        ups.setMinValue(0);

        String weightString = String.valueOf( weight );
        String[] splitedWeightString = weightString.split("\\.");
        if( splitedWeightString.length > 1 ){
            nps.setValue( Integer.parseInt( splitedWeightString[0] ) );
            pps.setValue(  Integer.parseInt( splitedWeightString[1].substring( 0 , 1) ) );
        }else{
            nps.setValue( Integer.parseInt( splitedWeightString[0] ) );
            pps.setValue(0);
        }

        if( weightUnit )
        {
            ups.setValue(0);
        }else{

            ups.setValue(1);
        }
        ups.setTag(weight);
        weightDialog.setContentView(numberPickerViews);
        ups.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                boolean weightSi = (newVal == 0);
                float tempWeight = (float) ups.getTag();

                //몸무게를 변환한다.
                if (weightSi) {
                    //파운드에서 킬로그램으로 변환한다.
                    tempWeight = com.fitdotlife.fitmate_lib.service.util.Utils.lb_to_kg(tempWeight);
                } else {
                    //킬로그램에서 파운드로 변환한다.
                    tempWeight = com.fitdotlife.fitmate_lib.service.util.Utils.kg_to_lb(tempWeight);
                }

                String weightString = String.valueOf( tempWeight );
                String[] splitedWeightString = weightString.split("\\.");
                if( splitedWeightString.length > 1 ){
                    nps.setValue( Integer.parseInt( splitedWeightString[0] ) );
                    pps.setValue(  Integer.parseInt( splitedWeightString[1].substring( 0 , 1) ) );
                }else{
                    nps.setValue( Integer.parseInt( splitedWeightString[0] ) );
                    pps.setValue(0);
                }

                ups.setTag(tempWeight);
            }
        });

        nps.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                ups.setTag((float) (((nps.getValue() * 10d) + pps.getValue()) / 10d));
            }
        });

        pps.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                ups.setTag((float) (((nps.getValue() * 10d) + pps.getValue()) / 10d));
            }
        });

        View buttonView = inflater.inflate( R.layout.dialog_button_two , null );
        Button btnLeft = (Button) buttonView.findViewById(R.id.btn_dialog_button_two_left);
        btnLeft.setText(this.getString(R.string.common_cancel));
        btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                weightDialog.close();
            }
        });

        Button btnRight = (Button) buttonView.findViewById(R.id.btn_dialog_button_two_right);
        btnRight.setText(this.getString(R.string.common_ok));
        btnRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean weightSi = (ups.getValue() == 0);
                boolean isSi = mUserInfo.isSI();
                float tempHeight = mUserInfo.getHeight();
                String heightUnitString = "";

                if (weightSi != isSi) {
                    if (weightSi) {
                        //인치에서 센티미터로 변환
                        tempHeight = com.fitdotlife.fitmate_lib.service.util.Utils.ft_to_cm(tempHeight);
                        heightUnitString = getString(R.string.common_centimeter);
                    } else {
                        //센티미터에서 인치로 변환한다.
                        tempHeight = com.fitdotlife.fitmate_lib.service.util.Utils.cm_to_ft(tempHeight);
                        heightUnitString = getString(R.string.common_ft);
                    }

                    String heightString = String.valueOf( tempHeight );
                    String[] splitedHeightString = heightString.split("\\.");
                    if( splitedHeightString.length > 1 ){
                        tvHeight.setText(String.format("%s %s", splitedHeightString[0] + "." + splitedHeightString[1].substring(0, 1), heightUnitString ));
                    }else{
                        tvHeight.setText(String.format("%s %s", splitedHeightString[0] + ".0", heightUnitString ));
                    }

                }

                mUserInfo.setWeight((Float) ups.getTag());
                mUserInfo.setHeight(tempHeight);
                mUserInfo.setIsSI(weightSi);
                mPresenter.modifyUserInfo(mUserInfo);

                String weightString = String.valueOf( mUserInfo.getWeight() );
                String[] splitedWeightString = weightString.split("\\.");
                if( splitedWeightString.length > 1 ){
                    tvWeight.setText(String.format("%s %s", splitedWeightString[0] + "." + splitedWeightString[1].substring(0, 1), ups.getDisplayedValues()[ups.getValue()]));
                }else{
                    tvWeight.setText(String.format("%s %s", splitedWeightString[0] + ".0", ups.getDisplayedValues()[ups.getValue()]));
                }

                mPresenter.ChangeUserInfo();
                weightDialog.close();
            }
        });

        weightDialog.setButtonView( buttonView );
        weightDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if( requestCode == this.USERINFO_REQUEST_CODE ) {
            if (resultCode == RESULT_OK) {

                this.mUserInfo.setName(data.getStringExtra(UserInfo.NAME_KEY));
                this.tvName.setText(this.mUserInfo.getName());

                this.mUserInfo.setIntro(data.getStringExtra(UserInfo.HELLO_MESSAGE_KEY));
                this.tvIntro.setText(this.mUserInfo.getIntro());

                long tempBirthDay = data.getLongExtra(UserInfo.BIRTHDATEKEY, 0);
                if(tempBirthDay > 0 ) {
                    this.mUserInfo.setBirthDate(data.getLongExtra(UserInfo.BIRTHDATEKEY, 0));
                    this.tvBirthDate.setText(this.getDateString(this.mUserInfo.getBirthDate()));
                }

                GenderType tempGenderType = GenderType.getGenderType(data.getIntExtra(UserInfo.SEX_KEY, 0));
                if( !tempGenderType.equals(GenderType.NONE) ) {
                    this.mUserInfo.setGender(tempGenderType);
                    if (this.mUserInfo.getGender() == GenderType.MALE) {
                        this.imgGender.setImageResource(R.drawable.profile_sex2);
                        //this.imgProfile.setImageBitmap( this.getCircleBitmap( R.drawable.profile_img5 ) );

                    } else if (this.mUserInfo.getGender() == GenderType.FEMALE) {
                        this.imgGender.setImageResource(R.drawable.profile_sex1);
                        //this.imgProfile.setImageBitmap( this.getCircleBitmap( R.drawable.profile_img2 ) );

                    }
                }

                mPresenter.modifyUserInfo(mUserInfo);
                mPresenter.ChangeUserInfo();
            }
        }
        else if(requestCode == this.USERINFO_CHANGEDEVICE_CODE)
        {
            if(resultCode == RESULT_OK)
            {

            }

            this.isShowing = false;
            mUserInfo = this.mPresenter.getUserInfo();
            this.mPresenter.ChangeUserInfo();
            //this.mPresenter.requestRestartService();
        }
        else if(requestCode == this.USERINFO_CHANGELOCATION_CODE)
        {
            if(resultCode == RESULT_OK)
            {
                int location = data.getIntExtra(LocationChangeActivity.INTENT_LOCATION, 0);
                this.tvWearingLocation.setText( this.getWearingLocationString( location ) );
                mUserInfo.setWearAt(location);
                mPresenter.modifyUserInfo(mUserInfo);
                mPresenter.ChangeUserInfo();
            }

            this.isShowing = false;
            //this.mPresenter.requestRestartService();
        }
        else if(requestCode == this.USERINFO_CHANGEALARM_CODE)
        {
            boolean isAlarm = UserInfo.getAlarmSetSharedPreference(this);
            tvAlarm.setText(isAlarm ? this.getString(R.string.common_on) : this.getString(R.string.common_off));
        }
        else if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK)
        {

            Uri uri = (Uri) data.getExtras().get(ImageUtil.PICTURE_URI);
            DisplayImageOptions options = new DisplayImageOptions.Builder()
                    .showImageForEmptyUri(R.drawable.profile_img1)
                    .showImageOnFail(R.drawable.profile_img1)
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .displayer(new RoundedBitmapDisplayer(300))
                    .build();

            ImageLoader.getInstance().displayImage(uri.toString(), imgProfile, options);

            mUserInfo.setAccountPhoto(uri.toString());
            mPresenter.modifyUserInfo(mUserInfo);

            HttpApi.chageUserInfo(mUserInfo, new HttpResponse() {
                @Override
                public void onResponse(int resultCode, String response) {

                    if( resultCode == 0 ){

                        Toast.makeText(getApplicationContext(), getString(R.string.myprofile_dont_send_photo), Toast.LENGTH_SHORT).show();

                    }else if(resultCode == 200){

                        try {

                            JSONObject jsonObject = new JSONObject(response.toString() );
                            String result = jsonObject.getString(CommonKey.RESPONSE_VALUE_KEY);

                            if (result.equals(CommonKey.SUCCESS)) {
                                String url = jsonObject.getString("url");
                                if(url != null && url != "")
                                {
                                    UserInfo.savePhotoUrlInSharedPreference(getApplicationContext(), url);
                                    mUserInfo.setAccountPhoto( url );
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }
    }


    public void callback(String url, String html, AjaxStatus status) {
        Log.d(TAG, " EEEE");
        //showResult(html);
    }

    private void setAutoLogin( boolean autoLogin ){
        SharedPreferences pref = this.getSharedPreferences( "fitmateservice" , Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(UserInfo.AUTO_LOGIN_KEY, autoLogin);
        editor.commit();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //this.mPresenter.close();
        this.mPresenter.modifyUserInfo(this.mUserInfo);

        this.setResult(DrawerView.REQUEST_PROFILE_IMAGE);

        Intent programChangeIntent = new Intent( SettingActivity.ACTION_USERINFO_CHANGE );
        sendBroadcast(programChangeIntent);

    }

    private String getWearingLocationString( int wearingLocation ){
        String result = null;

        if( wearingLocation == 0 ){
            wearingLocation = 1;
        }

        result = this.getResources().getStringArray(R.array.weraing_location_string)[wearingLocation - 1];
        return result;
    }

    @Override
    public void showDeviceChangeActivity(){
        Intent deviceChangeIntent = new Intent( MyProfileActivity.this , DeviceChangeActivity.class  );
        deviceChangeIntent.putExtra(DeviceChangeActivity.INTENT_LOCATION, mUserInfo.getWearAt());
        this.startActivityForResult(deviceChangeIntent, USERINFO_CHANGEDEVICE_CODE);
    }

    @Override
    public void showWearingLocationActivity() {
        Intent locationChangeIntent = new Intent( MyProfileActivity.this , LocationChangeActivity.class  );
        locationChangeIntent.putExtra(LocationChangeActivity.INTENT_LOCATION, mUserInfo.getWearAt());
        locationChangeIntent.putExtra(LocationChangeActivity.INTENT_BTADDRESS, mUserInfo.getDeviceAddress());
        this.startActivityForResult(locationChangeIntent, USERINFO_CHANGELOCATION_CODE);
    }

    public void setShowing( boolean showing ){
        this.isShowing = showing;
    }

    @Override
    public void dismissProgress() {

    }

    @Override
    public void setProgressText(String progressText) {

    }

    private String getDateString( long birthUtcTime ){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd");
        Date utcDate = new Date();
        utcDate.setTime(birthUtcTime);
        return dateFormat.format(utcDate);
    }

    private void showNoSettingDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);     // 여기서 this는 Activity의 this

        String message = this.getString(R.string.userinfo_not_connect_text);
        builder.setMessage(message)        // 메세지 설정
                .setCancelable(true)        // 뒤로 버튼 클릭시 취소 가능 설정
                .setNegativeButton(R.string.common_close, new DialogInterface.OnClickListener() {
                    // 확인 버튼 클릭시 설정
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                })
                .setPositiveButton(R.string.userinfo_start_fitmeter, new DialogInterface.OnClickListener() {
                    // 취소 버튼 클릭시 설정
                    public void onClick(DialogInterface dialog, int whichButton) {
                        Intent intent = new Intent(getApplicationContext(), SetUserInfoActivity.class);
                        startActivity(intent);

                        dialog.dismiss();
                    }
                });

        AlertDialog dialog = builder.create();    // 알림창 객체 생성
        dialog.show();    // 알림창 띄우기
    }

    private void showCameraActivity(){

        Intent i = new Intent(MyProfileActivity.this, ImageUtil.class);
        startActivityForResult(i, CAMERA_REQUEST);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if( requestCode == PERMISSION_REQUEST_STORAGE )
        {

            if( grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED ){
                showCameraActivity();
            }else{
                AlertDialog.Builder builder = new AlertDialog.Builder(MyProfileActivity.this);
                builder.setMessage( this.getResources().getString( R.string.storage_permission_reject ) )
                        .setCancelable(true)        // 뒤로 버튼 클릭시 취소 가능 설정
                        .setNeutralButton(R.string.common_ok, new DialogInterface.OnClickListener() {
                            // 확인 버튼 클릭시 설정
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.cancel();
                            }
                        });
                AlertDialog dialog = builder.create();    // 알림창 객체 생성
                dialog.show();    // 알림창 띄우기
            }

        }
    }
}
