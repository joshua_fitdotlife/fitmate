package com.fitdotlife.fitmate;

import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.fitdotlife.fitmate_lib.key.FitmeterType;

/**
 * Created by Joshua on 2016-08-04.
 */
public class FitmeterConnectFragment extends Fragment{

    private FitmeterType mDeviceType = null;
    private ImageView imgConnectFitmeter = null;

    private AnimationDrawable frameAnimation = null;

    public static FitmeterConnectFragment newInstance( FitmeterType deviceType)
    {
        Bundle args = new Bundle();
        FitmeterConnectFragment fragment = new FitmeterConnectFragment();
        args.putInt( SelectDeviceFragment.DEVICETYPE_KEY , deviceType.ordinal() );
        fragment.setArguments(args);
        return fragment;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);
        if(getArguments() != null){
            mDeviceType = FitmeterType.values()[ getArguments().getInt( SelectDeviceFragment.DEVICETYPE_KEY ) ];
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate( R.layout.fragment_connect_fitmeter , container , false  );
        this.imgConnectFitmeter = (ImageView) rootView.findViewById(R.id.img_fragment_connect_fitmeter);

        if( mDeviceType.equals( FitmeterType.BAND) ){
            imgConnectFitmeter.setBackgroundResource( R.drawable.connectfitmeter_band );
        }else if( mDeviceType.equals( FitmeterType.BLE ) ){
            imgConnectFitmeter.setBackgroundResource( R.drawable.connectfitmeter_ble );
        }

        frameAnimation = (AnimationDrawable) imgConnectFitmeter.getBackground();
        frameAnimation.start();

        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        frameAnimation.stop();
    }
}
