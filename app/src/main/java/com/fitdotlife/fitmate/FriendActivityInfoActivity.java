package com.fitdotlife.fitmate;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.text.format.DateFormat;
import android.util.Base64;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.WearingLocation;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ContinuousCheckPolicy;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseAnalyzer;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.FatAndCarbonhydrateConsumtion;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.StrengthInputType;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.USERVO2MAXLEVEL;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.UserInfoForAnalyzer;
import com.fitdotlife.fitmate_lib.customview.ArcChartView;
import com.fitdotlife.fitmate_lib.customview.Day24HourCalorieChartView;
import com.fitdotlife.fitmate_lib.customview.DayCalorieBarChartView;
import com.fitdotlife.fitmate_lib.customview.DayHMLIntensityBarChartView;
import com.fitdotlife.fitmate_lib.customview.LineChartView;
import com.fitdotlife.fitmate_lib.customview.MonthCalorieBarChartView;
import com.fitdotlife.fitmate_lib.customview.MonthPointBarChartView;
import com.fitdotlife.fitmate_lib.customview.MonthPointView;
import com.fitdotlife.fitmate_lib.customview.RoundedRectangleChartView;
import com.fitdotlife.fitmate_lib.customview.WeekCalorieBarChartView;
import com.fitdotlife.fitmate_lib.customview.WeekExerciseTimeBarChartView;
import com.fitdotlife.fitmate_lib.customview.WeekPointView;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.http.ActivityService;
import com.fitdotlife.fitmate_lib.http.FriendService;
import com.fitdotlife.fitmate_lib.http.NetworkClass;
import com.fitdotlife.fitmate_lib.http.RestHttpErrorHandler;
import com.fitdotlife.fitmate_lib.object.DayActivity;
import com.fitdotlife.fitmate_lib.object.DayActivity_Rest;
import com.fitdotlife.fitmate_lib.object.ExerciseProgram;
import com.fitdotlife.fitmate_lib.object.UserFriend;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.object.WeekActivity;
import com.fitdotlife.fitmate_lib.object.WeekActivity_Rest;
import com.fitdotlife.fitmate_lib.util.DateUtils;
import com.fitdotlife.fitmate_lib.util.Utils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringRes;
import org.androidannotations.rest.spring.annotations.RestService;

import java.security.PrivateKey;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Random;

@EActivity(R.layout.activity_friend_activity_info)
public class FriendActivityInfoActivity extends Activity implements View.OnTouchListener {


    private enum SelectType
    {
        DAY, WEEK , MONTH , NONE
    }

    private enum MoveDateType
    {
        PRE , NEXT
    }

    private enum DataStateType
    {
        AVAILABLE , NONE
    }

    private UserFriend mUserFriend = null;
    private UserInfo mUserInfo = null;

    private ExerciseProgram appliedProgram_day = null;
    private ExerciseProgram appliedProgram_week = null;
    private ExerciseProgram appliedProgram_month = null;
    private final int ANIMATION_DURATION = 3000;
    private DataStateType mDataStateType = DataStateType.NONE;

    private View activityChartView_1 = null;
    private View activityChartView_1_1 = null;
    private View activityChartView_2 = null;
    private View activityChartView_3 = null;

    private Calendar activityCalendar_day = null;
    private Calendar activityCalendar_week = null;
    private Calendar activityCalendar_month= null;
    private SelectType mSelectType = null;
    private long mTodayMilliSecond = 0;
    private FitmateDBManager dbManager;

    private TextView tvFat = null;
    private TextView tvCarbohydrate = null;

    @ViewById(R.id.ic_activity_friend_activityinfo_actionbar)
    View actionBarView;

    @ViewById(R.id.btn_activity_friend_activityinfo_select_day)
    TextView btnSelectDay;

    @ViewById(R.id.ll_activity_friend_activityinfo_select_day_mark)
    LinearLayout llSelectDayMark;

    @ViewById(R.id.btn_activity_friend_activityinfo_select_week)
    TextView btnSelectWeek;

    @ViewById(R.id.ll_activity_friend_activityinfo_select_week_mark)
    LinearLayout llSelectWeekMark;

    @ViewById(R.id.btn_activity_friend_activityinfo_select_month)
    TextView btnSelectMonth;

    @ViewById(R.id.ll_activity_friend_activityinfo_select_month_mark)
    LinearLayout llSelectMonthMark;

    @ViewById(R.id.tv_activity_friend_activityinfo_activitydate)
    TextView tvActivityDate;

    @ViewById(R.id.tv_activity_friend_activityinfo_pre_date)
    TextView tvPreDate;

    @ViewById(R.id.tv_activity_friend_activityinfo_next_date)
    TextView tvNextDate;

    @ViewById(R.id.tv_activity_friend_activityinfo_applied_program)
    TextView tvAppliedProgram;


    @ViewById(R.id.ll_activity_friend_activityinfo_activity_list_1)
    LinearLayout llActivityList_1;

    @ViewById(R.id.img_activity_friend_activityinfo_activity_list_1_img)
    ImageView imgActivityList_1;

    @ViewById(R.id.tv_activity_friend_activityinfo_activity_list_1_title)
    TextView tvActivityListTitle_1;

    @ViewById(R.id.img_activity_friend_activityinfo_activity_list_1_arrow)
    ImageView imgActivityListArrow_1;

    @ViewById(R.id.ll_activity_friend_activityinfo_activity_list_1_chart)
    LinearLayout llActivityListChart_1;

    @ViewById(R.id.ll_activity_friend_activityinfo_line_1)
    LinearLayout llLine_1;


    @ViewById(R.id.ll_activity_friend_activityinfo_activity_list_1_1)
    LinearLayout llActivityList_1_1;

    @ViewById(R.id.img_activity_friend_activityinfo_activity_list_1_1_img)
    ImageView imgActivityList_1_1;

    @ViewById(R.id.tv_activity_friend_activityinfo_activity_list_1_1_title)
    TextView tvActivityListTitle_1_1;

    @ViewById(R.id.img_activity_friend_activityinfo_activity_list_1_1_arrow)
    ImageView imgActivityListArrow_1_1;

    @ViewById(R.id.ll_activity_friend_activityinfo_activity_list_1_1_chart)
    LinearLayout llActivityListChart_1_1;

    @ViewById(R.id.ll_activity_friend_activityinfo_line_1_1)
    LinearLayout llLine_1_1;

    @ViewById(R.id.ll_activity_friend_activityinfo_activity_list_2)
    LinearLayout llActivityList_2;

    @ViewById(R.id.img_activity_friend_activityinfo_activity_list_2_img)
    ImageView imgActivityList_2;

    @ViewById(R.id.tv_activity_friend_activityinfo_activity_list_2_title)
    TextView tvActivityListTitle_2;

    @ViewById(R.id.img_activity_friend_activityinfo_activity_list_2_arrow)
    ImageView imgActivityListArrow_2;

    @ViewById(R.id.ll_activity_friend_activityinfo_activity_list_2_chart)
    LinearLayout llActivityListChart_2;

    @ViewById(R.id.ll_activity_friend_activityinfo_line_2)
    LinearLayout llLine_2;


    @ViewById(R.id.ll_activity_friend_activityinfo_activity_list_3)
    LinearLayout llActivityList_3;

    @ViewById(R.id.img_activity_friend_activityinfo_activity_list_3_img)
    ImageView imgActivityList_3;

    @ViewById(R.id.tv_activity_friend_activityinfo_activity_list_3_title)
    TextView tvActivityListTitle_3;

    @ViewById(R.id.img_activity_friend_activityinfo_activity_list_3_arrow)
    ImageView imgActivityListArrow_3;

    @ViewById(R.id.ll_activity_friend_activityinfo_activity_list_3_chart)
    LinearLayout llActivityListChart_3;

    @RestService
    ActivityService activityServiceClient;

    @RestService
    FriendService friendServiceClient;

    @ViewById(R.id.tv_activity_friend_activityinfo_next_date)
    View moveToNext;

    @StringRes(R.string.exercise_promoting_calorie)
    String strCalorieName;

    @Bean
    RestHttpErrorHandler restErrorHandler;


    @AfterViews
    void onInit() {

        Intent intent = this.getIntent();

        activityServiceClient.setRootUrl(NetworkClass.baseURL + "/api");
        friendServiceClient.setRootUrl(NetworkClass.baseURL + "/api");

        dbManager = new FitmateDBManager(this);
        this.mUserInfo = dbManager.getUserInfo();

        FriendActivityManager.getInstance().addActivity(this);

        actionBarView.setBackgroundColor(this.getResources().getColor(R.color.fitmate_orange));

        this.mSelectType = SelectType.NONE;
        Calendar today = resetTime(Calendar.getInstance().getTime());
        this.activityCalendar_day = Calendar.getInstance();
        String searchDayDate = intent.getStringExtra("ActivityDate");

        if( searchDayDate != null ) {
            this.activityCalendar_day.setTimeInMillis( DateUtils.getDateFromString_yyyy_MM_dd(searchDayDate).getTime() );
            this.mSelectType = SelectType.DAY;
        }else{
            this.mTodayMilliSecond = today.getTimeInMillis();
            this.activityCalendar_day.setTimeInMillis(today.getTimeInMillis());
        }

        //해당 주의 첫째날로
        this.activityCalendar_week = Calendar.getInstance();
        String searchWeekDate = intent.getStringExtra("WeekStartDate");
        if(searchWeekDate != null){
            this.activityCalendar_week.setTimeInMillis( DateUtils.getDateFromString_yyyy_MM_dd(searchWeekDate).getTime() );
            this.mSelectType = SelectType.WEEK;
        }else{

            this.activityCalendar_week.setTimeInMillis(today.getTimeInMillis());
            this.activityCalendar_week.add(Calendar.DATE, -today.get(Calendar.DAY_OF_WEEK) + 1);

        }


        //해당월의 1일로
        this.activityCalendar_month = Calendar.getInstance();
        this.activityCalendar_month.setTimeInMillis(today.getTimeInMillis());
        activityCalendar_month.set(Calendar.DAY_OF_MONTH, 1);


        this.mUserFriend = intent.getParcelableExtra("UserFriend");
        if( mUserFriend != null )
        {
            displayActionBar();
            setProgram();
            if(mSelectType.equals(SelectType.DAY.NONE) || mSelectType.equals(SelectType.DAY)) {
                this.getDayActivity();
            }else if(mSelectType.equals(SelectType.WEEK)){
                this.getWeekActivity();
            }

        }else{
            String friendEmail = intent.getStringExtra("FriendEmail");
            //친구 정보를 가져온다.
            getUserFriend(friendEmail);
        }

        this.imgActivityListArrow_1.setVisibility(View.INVISIBLE);
        this.imgActivityListArrow_1_1.setVisibility(View.INVISIBLE);
        this.imgActivityListArrow_2.setVisibility(View.INVISIBLE);
        this.imgActivityListArrow_3.setVisibility(View.INVISIBLE);

        tvAppliedProgram.setSelected(true);

    }

    @UiThread
    void displayActionBar(){
        TextView tvBarTitle = (TextView) actionBarView.findViewById( R.id.tv_custom_action_bar_title );
        tvBarTitle.setText(mUserFriend.getName() + " " + this.getString(R.string.activity_bar_title));

        ImageView imgBack = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_left);
        imgBack.setVisibility(View.GONE);

        ImageView mImgRight = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_right);
        mImgRight.setImageResource(R.drawable.profile);
        mImgRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(FriendActivityInfoActivity.this, FriendProfileActivity_.class);
                intent.putExtra("UserFriend", mUserFriend);

                startActivity(intent);

            }
        });
        mImgRight.setOnTouchListener(this);
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        ImageView actionbarImageView = (ImageView) view;
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            actionbarImageView.setColorFilter(this.getResources().getColor(R.color.selected_colorfilter), PorterDuff.Mode.DARKEN);
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            actionbarImageView.setColorFilter(null);
        }

        return false;
    }

    @Background
    void getUserFriend(String friendEmail){

        mUserFriend = friendServiceClient.getUsersFriendsInfo(mUserInfo.getEmail(), friendEmail);
        if(mUserFriend != null) {
            displayActionBar();
            setProgram();
        }

        if(mSelectType.equals(SelectType.DAY.NONE) || mSelectType.equals(SelectType.DAY)) {
            this.getDayActivity();
        }else if(mSelectType.equals(SelectType.WEEK)){
            this.getWeekActivity();
        }
    }

    void setProgram(){
        ExerciseProgram program = dbManager.getUserExerciProgram(mUserFriend.getExerciseProgramID());
        this.appliedProgram_day = program;
        this.appliedProgram_week = program;
        this.appliedProgram_month = program;
    }

    @Background
    void getDayActivity(){
        String activityDate = this.getDateString(this.activityCalendar_day);
        DayActivity_Rest dayActivity = activityServiceClient.getDayActivity(mUserInfo.getEmail(), mUserFriend.getEmail(), activityDate);
        if( dayActivity != null ) {
            displayDayActivity(dayActivity);
        }
    }

    @Background
    void getWeekActivity(){

        Calendar weekCalendar = Calendar.getInstance();
        weekCalendar.setTimeInMillis(this.activityCalendar_week.getTimeInMillis());
        weekCalendar.add(Calendar.DATE, 6);
        String weekEndDate = this.getDateString(weekCalendar);
        String weekStartDate = this.getDateString(activityCalendar_week);

        WeekActivity_Rest weekActivity = activityServiceClient.getWeekActivity(mUserInfo.getEmail(), mUserFriend.getEmail(), weekStartDate);
        List<DayActivity_Rest> dayActivityList = activityServiceClient.getDayActivityList(mUserInfo.getEmail(), mUserFriend.getEmail(), weekStartDate, weekEndDate);
        if( weekActivity != null ) {
            displayWeekActivity(weekActivity, dayActivityList);
        }
    }

    @Background
    void getMonthActivity(){
        Calendar monthCalendar = Calendar.getInstance();
        monthCalendar.setTimeInMillis(this.activityCalendar_month.getTimeInMillis());
        int lastDay  = monthCalendar.getActualMaximum(Calendar.DATE);
        monthCalendar.set(Calendar.DAY_OF_MONTH, lastDay);
        int monthWeekNumber = monthCalendar.get(Calendar.WEEK_OF_MONTH);
        String monthEndDate = this.getDateString(monthCalendar);
        monthCalendar.set(Calendar.DAY_OF_MONTH, 1);
        int dayOfWeek = monthCalendar.get(Calendar.DAY_OF_WEEK);
        monthCalendar.add(Calendar.DATE, -(dayOfWeek - 1));
        String monthWeekStartDate = this.getDateString(monthCalendar);

        List<WeekActivity_Rest> monthActivity = activityServiceClient.getMonthActivity(mUserInfo.getEmail(), mUserFriend.getEmail(), monthWeekStartDate, monthEndDate);
        if( monthActivity != null ) {
            displayMonthActivity(monthActivity);
        }

    }

    @UiThread
    void displayDayActivity( DayActivity_Rest dayActivity ){


        this.mSelectType = SelectType.DAY;

        if(DateUtils.isToday(activityCalendar_day)){
            setVisible_NextButton(false);
        }else{
            setVisible_NextButton(true);
        }




        if(dayActivity!=null){
            appliedProgram_day = dbManager.getUserExerciProgram(dayActivity.getExerciseProgramID());
        }else{
            appliedProgram_day = dbManager.getUserExerciProgram(mUserFriend.getExerciseProgramID());
        }


        String programName = null;
        if(appliedProgram_day.getDefaultProgram())
        {
            int resId =  getResources().getIdentifier(appliedProgram_day.getName(), "string", getPackageName());
            programName = getString(resId);
        }
        else
        {
            programName = appliedProgram_day.getName();
        }
        this.tvAppliedProgram.setText(programName);

        this.btnSelectDay.setTextColor(Color.BLACK);
        this.btnSelectWeek.setTextColor(0xFFA9A9A9);
        this.btnSelectMonth.setTextColor(0xFFA9A9A9);

        this.llSelectDayMark.setBackgroundColor(this.getResources().getColor(R.color.fitmate_orange));
        this.llSelectWeekMark.setBackgroundColor(Color.WHITE);
        this.llSelectMonthMark.setBackgroundColor(Color.WHITE);

        this.tvActivityDate.setText(DateFormat.format(this.getString(R.string.activity_day_date_format), activityCalendar_day.getTime()));


        this.imgActivityList_1.setImageResource(R.drawable.icon_medal);
        this.tvActivityListTitle_1.setText(this.getString(R.string.activity_day_achievement));

        this.llActivityListChart_1_1.setVisibility(View.VISIBLE);
        this.llActivityList_1_1.setVisibility(View.VISIBLE);
        this.llLine_1_1.setVisibility(View.VISIBLE);

        //this.imgActivityList_2.setImageResource( R.drawable.icon_chart );
        this.tvActivityListTitle_2.setText(this.getString(R.string.activity_day_calorie_consumption));

        this.llActivityListChart_3.setVisibility(View.VISIBLE);
        this.llActivityList_3.setVisibility(View.VISIBLE);

        this.imgActivityList_3.setImageResource(R.drawable.icon_chart);
        this.tvActivityListTitle_3.setText(this.getString(R.string.activity_day_intensity));

        if( dayActivity != null ) {

            this.mDataStateType = DataStateType.AVAILABLE;


            activityChartView_1 = this.getLayoutInflater().inflate(R.layout.child_day_achievement_friend, null);
            LinearLayout llScoreChart = (LinearLayout) activityChartView_1.findViewById(R.id.ll_child_day_achievement_activity_chart);
           // LinearLayout llAverageMetChart = (LinearLayout) activityChartView_1.findViewById(R.id.ll_child_day_achievement_average_chart);
         //   ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,0);

           // llAverageMetChart.setLayoutParams(params);
         //   activityChartView_1.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, llScoreChart.getHeight()));
            ArcChartView DAYScoreArcChartView = new ArcChartView(this);
            DAYScoreArcChartView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            DAYScoreArcChartView.setWeekValue(dayActivity.getPredayScore() + dayActivity.getTodayScore());

            Calendar mToday = Calendar.getInstance();

            DAYScoreArcChartView.setIsToday(mToday.get(Calendar.YEAR) == activityCalendar_day.get(Calendar.YEAR) && mToday.get(Calendar.MONTH) == ( activityCalendar_day.get(Calendar.YEAR) + 1 )  && mToday.get(Calendar.DAY_OF_MONTH) == activityCalendar_day.get(Calendar.DAY_OF_MONTH));    // 오늘인지 과거인지 확인하는 함수
            DAYScoreArcChartView.setWeekUnit(this.getString(R.string.common_point));

            StrengthInputType strengthType = StrengthInputType.getStrengthType( appliedProgram_day.getCategory() );

            if( strengthType.equals( StrengthInputType.CALORIE ) || strengthType.equals( StrengthInputType.CALORIE_SUM )|| strengthType.equals(StrengthInputType.VS_BMR)) {

                UserInfoForAnalyzer userInfoForAnalyzer =  com.fitdotlife.fitmate_lib.service.util.Utils.getUserInfoForAnalyzer( mUserFriend.getBirthDate() , mUserFriend.isSex() , mUserFriend.getHeight() , mUserFriend.getWeight() , mUserFriend.isSi()  );


                int calorieFrom= (int) appliedProgram_day.getStrengthFrom();
                int calorieTo= (int) appliedProgram_day.getStrengthFrom();
                int bmr = userInfoForAnalyzer.getBMR();
                int max = 0;
                if(strengthType.equals(StrengthInputType.VS_BMR)){
                    calorieFrom=(int)Math.round(calorieFrom /100.0 * bmr);
                    calorieTo=(int)Math.round(calorieTo /100.0 * bmr);
                }

                if(calorieFrom==calorieTo){
                    max= calorieFrom*2;
                }
                else{
                    max = (calorieFrom+calorieTo);
                }

                DAYScoreArcChartView.setRangMax(max);

                DAYScoreArcChartView.setTodayValue(dayActivity.getCalorieByActivity());
                DAYScoreArcChartView.setTodayUnit(this.getString(R.string.common_calorie), StrengthInputType.getStrengthType(appliedProgram_day.getCategory()));
                DAYScoreArcChartView.setTodayRange((int) appliedProgram_day.getStrengthFrom(), (int) appliedProgram_day.getStrengthTo());
            }else{
                DAYScoreArcChartView.setRangMax(appliedProgram_day.getMinuteTo());
                DAYScoreArcChartView.setTodayValue(dayActivity.getAchieveOfTarget());
                DAYScoreArcChartView.setTodayUnit(this.getString(R.string.common_minute), StrengthInputType.getStrengthType(appliedProgram_day.getCategory()));
                DAYScoreArcChartView.setTodayRange(appliedProgram_day.getMinuteFrom(), appliedProgram_day.getMinuteTo());
            }
            DAYScoreArcChartView.startAnimation(this.ANIMATION_DURATION);
            llScoreChart.addView(DAYScoreArcChartView);

//            LineChartView dayMetLineChartView = new LineChartView(this);
//            dayMetLineChartView.setxAxisTextTitle(this.getString(R.string.activity_day_metchart_time));
//            dayMetLineChartView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
//
            byte[] byteMetArray = Base64.decode(dayActivity.getMetArray(), Base64.NO_WRAP);

            float[] metArray = null;
            if (byteMetArray != null) {
                metArray = DayActivity_Rest.convertByteArrayToFloatArray(byteMetArray);
            }
//
//            dayMetLineChartView.setData( metArray );
          //  llAverageMetChart.addView(dayMetLineChartView);

            activityChartView_1_1 = this.getLayoutInflater().inflate(R.layout.child_day_activity_fat_carbo_chart, null);
            this.tvFat = (TextView) activityChartView_1_1.findViewById(R.id.tv_child_day_activity_fat);
            this.tvCarbohydrate = (TextView) activityChartView_1_1.findViewById(R.id.tv_child_day_activity_carbohydrate);

            FitmateDBManager dbManager= new FitmateDBManager(this);
            UserInfo userInfo = dbManager.getUserInfo();
            UserInfoForAnalyzer userInfoForAnalyzer = com.fitdotlife.fitmate_lib.service.util.Utils.getUserInfoForAnalyzer(mUserFriend.getBirthDate(), mUserFriend.isSex(), mUserFriend.getHeight(), mUserFriend.getWeight(), mUserFriend.isSi());

            com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseProgram programforAnalyzer = dbManager.getExerciseProgramForAnalyzer( mUserFriend.getExerciseProgramID() );
            ExerciseAnalyzer analyzer = new ExerciseAnalyzer(programforAnalyzer, userInfoForAnalyzer, WearingLocation.getWearingLocation( mUserFriend.getWearingLocation() ) , dayActivity.getDataSavingInterval());

            List<Float> metList= new ArrayList<>();
            for(float f:metArray){
                metList.add(f);
            }

            FatAndCarbonhydrateConsumtion fatBurn = analyzer.getFatandCarbonhydrate( metList , dayActivity.getDataSavingInterval() );
            float fat =fatBurn.getFatBurned();
            float carbon =fatBurn.getCarbonhydrateBurned();


            if( fat >= 100) {
                tvFat.setText(String.format("%d g", Math.round(fat)));
            }else{
                tvFat.setText(String.format("%.1f g", fat));
            }

            if( carbon >= 100 ) {
                tvCarbohydrate.setText(String.format("%d g", Math.round(carbon)));
            }else{
                tvCarbohydrate.setText(String.format("%.1f g", carbon));
            }

            activityChartView_2 = this.getLayoutInflater().inflate(R.layout.child_day_activity_metabolism_chart, null);
            LinearLayout CalorieChart24 = (LinearLayout) activityChartView_2.findViewById(R.id.ll_child_day_achievement_calorie_chart);

            //24시간 칼로리 소모량 그리는 차트
            //일일 칼로리 소모량 바 1개 그리는 차트
            DayCalorieBarChartView dayCalorieBarChartView = new DayCalorieBarChartView(this);
            dayCalorieBarChartView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            dayCalorieBarChartView.setValue(dayActivity.getCalorieByActivity());
            dayCalorieBarChartView.startAnimation(this.ANIMATION_DURATION);
            CalorieChart24.addView( dayCalorieBarChartView );

            LinearLayout llCalorieChart = (LinearLayout) activityChartView_2.findViewById(R.id.ll_child_day_24calorie_chart);
            Day24HourCalorieChartView dayHourCalorieBarChartView = new Day24HourCalorieChartView(this);
            dayHourCalorieBarChartView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            dayHourCalorieBarChartView.setXAxisTitle(this.getString(R.string.activity_day_caloriechart_time));


            if( dayActivity.getMetArray() == null || metArray.length == 0  ){

            }else {
                int[] caloreArray = getDay24CalorieArray(metArray);

                dayHourCalorieBarChartView.setValues(caloreArray);
                dayHourCalorieBarChartView.startAnimation(this.ANIMATION_DURATION);
                llCalorieChart.addView(dayHourCalorieBarChartView);
            }

            activityChartView_3 = this.getLayoutInflater().inflate(R.layout.child_day_activity_intensity_chart, null);
            LinearLayout llExerciseAverageChart = (LinearLayout) activityChartView_3.findViewById(R.id.ll_child_day_activity_intensity_average_exercisetime_chart);

            RoundedRectangleChartView dayTimeMetRoundRectangleChartView = new RoundedRectangleChartView(this);
            dayTimeMetRoundRectangleChartView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            dayTimeMetRoundRectangleChartView.setExerciseTime(dayActivity.getStrengthHigh() + dayActivity.getStrengthMedium());
            dayTimeMetRoundRectangleChartView.setAverageMET(((int) (dayActivity.getAverageMet() * 100d)) / 100d);
            dayTimeMetRoundRectangleChartView.setLeftText(this.getString(R.string.activity_day_intensitytimechart_1), this.getString(R.string.activity_day_intensitytimechart_2));
            dayTimeMetRoundRectangleChartView.setRightText(this.getString(R.string.activity_day_averagemetchart_1), this.getString(R.string.activity_day_averagemetchart_2));
            llExerciseAverageChart.addView(dayTimeMetRoundRectangleChartView);

            LinearLayout llHMLIntensityChart = (LinearLayout) activityChartView_3.findViewById(R.id.ll_child_day_activity_intensity_strength_chart);
            DayHMLIntensityBarChartView dayHmlIntensityBarChartView = new DayHMLIntensityBarChartView(this);
            dayHmlIntensityBarChartView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            int[] colors = new int[]{this.getResources().getColor(R.color.fitmate_blue), this.getResources().getColor(R.color.fitmate_cyan), this.getResources().getColor(R.color.fitmate_orange)};
            int[] values = new int[]{ dayActivity.getStrengthLow() , dayActivity.getStrengthMedium() , dayActivity.getStrengthHigh() };

            String[] arrYText = new String[]{
                    this.getString(R.string.activity_day_hmlchart_low),
                    this.getString(R.string.activity_day_hmlchart_medium),
                    this.getString(R.string.activity_day_hmlchart_high)
            };

            dayHmlIntensityBarChartView.setYAxisTitle(this.getString(R.string.activity_day_hmlchart_intensity));
            dayHmlIntensityBarChartView.setXAxisTitle(this.getString(R.string.activity_day_hmlchart_time));
            dayHmlIntensityBarChartView.setYAxisTextList(arrYText);
            dayHmlIntensityBarChartView.setValues(values);
            dayHmlIntensityBarChartView.setColors(colors);
            dayHmlIntensityBarChartView.startAnimation(this.ANIMATION_DURATION);
            llHMLIntensityChart.addView(dayHmlIntensityBarChartView);

            this.llActivityListChart_1.removeAllViews();
            this.llActivityListChart_1.addView(activityChartView_1);
            this.imgActivityListArrow_1.setImageResource(R.drawable.icon_arrow_up);


            this.llActivityListChart_1_1.removeAllViews();
            this.llActivityListChart_1_1.addView(activityChartView_1_1);
            this.imgActivityListArrow_1_1.setImageResource(R.drawable.icon_arrow_up);

            this.llActivityListChart_2.removeAllViews();
            this.llActivityListChart_2.addView(activityChartView_2);
            this.imgActivityListArrow_2.setImageResource(R.drawable.icon_arrow_up);

            this.llActivityListChart_3.removeAllViews();
            this.llActivityListChart_3.addView(activityChartView_3);
            this.imgActivityListArrow_3.setImageResource(R.drawable.icon_arrow_up);



        }
        else
        {
            this.mDataStateType = DataStateType.NONE;
            this.llActivityListChart_1.removeAllViews();
            this.llActivityListChart_1_1.removeAllViews();
            this.llActivityListChart_2.removeAllViews();
            this.llActivityListChart_3.removeAllViews();
        }
    }

    @UiThread
    void displayWeekActivity( WeekActivity_Rest weekActivity , List<DayActivity_Rest> dayActivityList )
    {
        this.mSelectType = SelectType.WEEK;
        if(weekActivity!=null){
            appliedProgram_week = dbManager.getUserExerciProgram(weekActivity.getExerciseProgramID());
        }else{
            appliedProgram_week = dbManager.getUserExerciProgram(mUserFriend.getExerciseProgramID());
        }

        if(DateUtils.isThisWeek(activityCalendar_week)){
            setVisible_NextButton(false);
        }else{
            setVisible_NextButton(true);
        }
        String programName = null;
        if(appliedProgram_day.getDefaultProgram())
        {
            int resId =  getResources().getIdentifier(appliedProgram_week.getName(), "string", getPackageName());
            programName = getString(resId);
        }
        else
        {
            programName = appliedProgram_day.getName();
        }
        this.tvAppliedProgram.setText(programName);

        this.btnSelectDay.setTextColor(0xFFA9A9A9);
        this.btnSelectWeek.setTextColor(Color.BLACK);
        this.btnSelectMonth.setTextColor(0xFFA9A9A9);

        this.llSelectDayMark.setBackgroundColor(Color.WHITE);
        this.llSelectWeekMark.setBackgroundColor(this.getResources().getColor(R.color.fitmate_orange));
        this.llSelectMonthMark.setBackgroundColor(Color.WHITE);

        Calendar weekCalendar = Calendar.getInstance();
        weekCalendar.setTimeInMillis(activityCalendar_week.getTimeInMillis());
        int weekNumber = weekCalendar.get(Calendar.WEEK_OF_MONTH);

        Locale current = getResources().getConfiguration().locale;
        this.tvActivityDate.setText(Utils.toWeekString(weekNumber, weekCalendar, current));

        this.imgActivityList_1.setImageResource(R.drawable.icon_medal);
        this.tvActivityListTitle_1.setText(this.getResources().getString(R.string.activity_week_achievement));

        this.llActivityListChart_1_1.setVisibility(View.GONE);
        this.llActivityList_1_1.setVisibility(View.GONE);
        this.llLine_1_1.setVisibility(View.GONE);

        this.imgActivityList_2.setImageResource(R.drawable.icon_chart);
        this.tvActivityListTitle_2.setText(this.getResources().getString(R.string.activity_week_calorie_consumption));

        this.llActivityListChart_3.setVisibility(View.GONE);
        this.llActivityList_3.setVisibility(View.GONE);

        if(weekActivity != null) {

            int[] arrExerciseTime = new int[7];
            int[] arrCalorie = new int[7];
            String[] arrDayDate = new String[7];

            if(dayActivityList != null) {
                for (int index = 0; index < 7; index++) {
                    String searchDate = this.getDateString(weekCalendar);
                    arrDayDate[index] = searchDate;
                    Iterator<DayActivity_Rest> dayActivityIterator = dayActivityList.iterator();

                    while (dayActivityIterator.hasNext()) {
                        DayActivity_Rest dayActivity = dayActivityIterator.next();
                        if (dayActivity != null) {
                            if (dayActivity.getDate().equals(searchDate)) {
                                arrExerciseTime[index] = dayActivity.getAchieveOfTarget();
                                arrCalorie[index] = dayActivity.getCalorieByActivity();
                            }
                        }
                    }
                    weekCalendar.add(Calendar.DATE, 1);
                }
            }


            this.mDataStateType = DataStateType.AVAILABLE;

            activityChartView_1 = this.getLayoutInflater().inflate(R.layout.child_daily_achievement_chart, null);
            LinearLayout llScoreBarChart = (LinearLayout) activityChartView_1.findViewById(R.id.ll_child_daily_achievement_bar_chart);

            WeekExerciseTimeBarChartView WeekExerciseTimeBarChartView = new WeekExerciseTimeBarChartView(this);
            WeekExerciseTimeBarChartView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            int[] values;

            String[] arrYText = this.getResources().getStringArray(R.array.week_char);
            for( int i = 0 ; i < arrDayDate.length ; i++ ){
                arrDayDate[i] = DateUtils.getDateStringForPattern(arrDayDate[i], this.getString(R.string.activity_week_day_date_format));
            }

            if(appliedProgram_week.getCategory() == StrengthInputType.CALORIE.Value || appliedProgram_week.getCategory() == StrengthInputType.CALORIE_SUM.Value) {
                WeekExerciseTimeBarChartView.setYAxisTitle(this.getString(R.string.common_calorie));
                values = arrCalorie;
                WeekExerciseTimeBarChartView.setValues(values);
                WeekExerciseTimeBarChartView.setRange((int) appliedProgram_week.getStrengthFrom(), (int) appliedProgram_week.getStrengthTo());
            } else if( appliedProgram_week.getCategory() == StrengthInputType.VS_BMR.Value ){
                FitmateDBManager dbManager = new FitmateDBManager(this);
                UserInfo userInfo = dbManager.getUserInfo();
                UserInfoForAnalyzer userInfoForAnalyzer = com.fitdotlife.fitmate_lib.service.util.Utils.getUserInfoForAnalyzer(mUserFriend.getBirthDate(), mUserFriend.isSex(), mUserFriend.getHeight(), mUserFriend.getWeight(), mUserFriend.isSi());

                int bmr = userInfoForAnalyzer.getBMR();

                WeekExerciseTimeBarChartView.setYAxisTitle(this.getString(R.string.common_calorie));
                values = arrCalorie;
                WeekExerciseTimeBarChartView.setValues(values);
                if(appliedProgram_week.getStrengthFrom() == appliedProgram_week.getStrengthTo()){
                    WeekExerciseTimeBarChartView.setRange((int) (bmr * (appliedProgram_week.getStrengthFrom() / 100)), 2* (int) (bmr * (appliedProgram_week.getStrengthTo() / 100)));
                }else{
                    WeekExerciseTimeBarChartView.setRange((int) (bmr * (appliedProgram_week.getStrengthFrom() / 100)),  (int) (bmr * (appliedProgram_week.getStrengthTo() / 100)));
                }


            } else{
                WeekExerciseTimeBarChartView.setYAxisTitle(this.getString(R.string.common_minute));
                values = arrExerciseTime;
                WeekExerciseTimeBarChartView.setValues(values);
                WeekExerciseTimeBarChartView.setRange((int) appliedProgram_week.getMinuteFrom(), (int) appliedProgram_week.getMinuteTo());
            }
            WeekExerciseTimeBarChartView.setXAxisTextList(arrYText);

            WeekExerciseTimeBarChartView.setXAxisDateList(arrDayDate);

            WeekExerciseTimeBarChartView.startAnimation(this.ANIMATION_DURATION);
            llScoreBarChart.addView(WeekExerciseTimeBarChartView);

            LinearLayout llOvalChart = (LinearLayout) activityChartView_1.findViewById(R.id.ll_child_daily_achievement_oval_chart);
            WeekPointView WeekPointOvalChartView = new WeekPointView(this);
            WeekPointOvalChartView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            WeekPointOvalChartView.setColor(this.getResources().getColor(R.color.fitmate_cyan));
            WeekPointOvalChartView.setPoint(weekActivity.getScore());
            llOvalChart.addView(WeekPointOvalChartView);

            this.activityChartView_2 = this.getLayoutInflater().inflate(R.layout.child_daily_activity_metabolism_chart, null);
            LinearLayout llCalorieChart = (LinearLayout) activityChartView_2.findViewById(R.id.ll_child_daily_activity_calorie_bar_chart);
            WeekCalorieBarChartView weekCalorieBarChartView = new WeekCalorieBarChartView(this);
            weekCalorieBarChartView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            int[] calorieArray = arrCalorie;
            weekCalorieBarChartView.setYAxisTitle(this.getString(R.string.common_calorie));
            weekCalorieBarChartView.setXAxisTextList(arrYText);
            weekCalorieBarChartView.setValues(calorieArray);
            weekCalorieBarChartView.setXAxisDateList(arrDayDate);

            weekCalorieBarChartView.startAnimation(ANIMATION_DURATION);
            llCalorieChart.addView(weekCalorieBarChartView);

            this.llActivityListChart_1.removeAllViews();
            this.llActivityListChart_1.addView(activityChartView_1);
            this.imgActivityListArrow_1.setImageResource(R.drawable.icon_arrow_up);

            this.llActivityListChart_2.removeAllViews();
            this.llActivityListChart_2.addView(activityChartView_2);
            this.imgActivityListArrow_2.setImageResource(R.drawable.icon_arrow_up);




        }
        else
        {
            this.mDataStateType = DataStateType.NONE;
            this.llActivityListChart_1.removeAllViews();
            this.llActivityListChart_2.removeAllViews();
        }
    }

    @UiThread
    void displayMonthActivity(List<WeekActivity_Rest> monthActivity){

        int lastExProgramID =mUserFriend.getExerciseProgramID();
        if(monthActivity!=null && monthActivity.size()>0){
            lastExProgramID = monthActivity.get(monthActivity.size() - 1).getExerciseProgramID();
        }

        appliedProgram_month= dbManager.getUserExerciProgram(lastExProgramID);

        if(DateUtils.isThisMonth(activityCalendar_month)){
            setVisible_NextButton(false);
        }else{
            setVisible_NextButton(true);
        }
        this.mSelectType = SelectType.MONTH;
        String programName = null;
        if(appliedProgram_day.getDefaultProgram())
        {
            int resId =  getResources().getIdentifier(appliedProgram_month.getName(), "string", getPackageName());
            programName = getString(resId);
        }
        else
        {
            programName = appliedProgram_day.getName();
        }
        this.tvAppliedProgram.setText(programName);

        this.btnSelectDay.setTextColor(0xFFA9A9A9);
        this.btnSelectWeek.setTextColor(0xFFA9A9A9);
        this.btnSelectMonth.setTextColor(Color.BLACK);

        this.llSelectDayMark.setBackgroundColor(Color.WHITE);
        this.llSelectWeekMark.setBackgroundColor(Color.WHITE);
        this.llSelectMonthMark.setBackgroundColor(this.getResources().getColor(R.color.fitmate_orange));

        Calendar monthCalendar =Calendar.getInstance();
        monthCalendar.setTimeInMillis(this.activityCalendar_month.getTimeInMillis());
        int lastDay  = monthCalendar.getActualMaximum(Calendar.DATE);
        monthCalendar.set(Calendar.DAY_OF_MONTH, lastDay);
        int monthWeekNumber = monthCalendar.get(Calendar.WEEK_OF_MONTH);
        monthCalendar.set(Calendar.DAY_OF_MONTH, 1);
        int weekNumber = monthCalendar.get(Calendar.DAY_OF_WEEK);
        monthCalendar.add(Calendar.DATE, -(weekNumber - 1));

        Locale current = getResources().getConfiguration().locale;
        this.tvActivityDate.setText(Utils.toMonthString(activityCalendar_month, current));

        this.imgActivityList_1.setImageResource(R.drawable.icon_medal);
        this.tvActivityListTitle_1.setText(this.getString(R.string.activity_month_achievement));

        this.llActivityListChart_1_1.setVisibility(View.GONE);
        this.llActivityList_1_1.setVisibility(View.GONE);
        this.llLine_1_1.setVisibility(View.GONE);

        this.imgActivityList_2.setImageResource(R.drawable.icon_chart);
        this.tvActivityListTitle_2.setText(this.getString(R.string.activity_month_calorie_consumption));

        this.llActivityListChart_3.setVisibility(View.GONE);
        this.llActivityList_3.setVisibility(View.GONE);

        if(monthActivity != null) {

            int scoreSum = 0;
            int scoreCount = 0;
            int[] arrScore = new int[monthWeekNumber];
            int[] arrCalorie = new int[monthWeekNumber];

            for (int index = 0; index < monthWeekNumber; index++) {

                String activityDate = this.getDateString(monthCalendar);

                Iterator<WeekActivity_Rest> weekActivityListIter = monthActivity.iterator();
                while (weekActivityListIter.hasNext()) {
                    WeekActivity_Rest weekActivity = weekActivityListIter.next();

                    if (weekActivity != null) {
                        if (weekActivity.getWeekStartDate().equals(activityDate)) {
                            int score = weekActivity.getScore();
                            arrScore[index] = score;
                            arrCalorie[index] = weekActivity.getAveCalorie();
                            scoreSum += score;
                            scoreCount += 1;
                            break;
                        }
                    }
                }
                monthCalendar.add(Calendar.DATE, 7);
            }

            int averageScore = 0;
            if (scoreSum > 0) {
                averageScore = scoreSum / scoreCount;
            }

            this.mDataStateType = DataStateType.AVAILABLE;
            activityChartView_1 = this.getLayoutInflater().inflate(R.layout.child_monthly_achievement_chart, null);
            LinearLayout llScoreBarChart = (LinearLayout) activityChartView_1.findViewById(R.id.ll_child_monthly_achievement_bar_chart);

            MonthPointBarChartView monthlyScoreBarChartView = new MonthPointBarChartView(this);
            monthlyScoreBarChartView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            int[] scoreValues = arrScore;
            String[] arrXText = new String[ scoreValues.length ];

            for( int i = 0 ; i < scoreValues.length ;i++ ){
                arrXText[i] = this.getWeekNumberChar( i + 1 );
            }

            monthlyScoreBarChartView.setYAxisTitle(this.getString(R.string.common_score));
            monthlyScoreBarChartView.setXAxisTextList(arrXText);
            monthlyScoreBarChartView.setValues(scoreValues);
            monthlyScoreBarChartView.startAnimation(this.ANIMATION_DURATION);
            llScoreBarChart.addView(monthlyScoreBarChartView);

            LinearLayout llOvalChart = (LinearLayout) activityChartView_1.findViewById(R.id.ll_child_monthly_achievement_oval_chart);
            MonthPointView monthPointChartView = new MonthPointView(this);
            monthPointChartView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            monthPointChartView.setColor(this.getResources().getColor(R.color.fitmate_orange));
            monthPointChartView.setPoint( averageScore );
            llOvalChart.addView(monthPointChartView);

            this.activityChartView_2 = this.getLayoutInflater().inflate(R.layout.child_monthly_activity_metabolism_chart, null);
            LinearLayout llMetabolismBarChart = (LinearLayout) this.activityChartView_2.findViewById(R.id.ll_child_monthly_activity_metabolism_bar_chart);

            MonthCalorieBarChartView monthlyCalorieBarChartView = new MonthCalorieBarChartView(this);
            monthlyCalorieBarChartView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            int[] metaValues = arrCalorie;
            monthlyCalorieBarChartView.setYAxisTitle(this.getString(R.string.common_calorie));
            monthlyCalorieBarChartView.setXAxisTextList(arrXText);
            monthlyCalorieBarChartView.setValues(metaValues);

            monthlyCalorieBarChartView.startAnimation(this.ANIMATION_DURATION);
            llMetabolismBarChart.addView(monthlyCalorieBarChartView);

            this.llActivityListChart_1.removeAllViews();
            this.llActivityListChart_1.addView(activityChartView_1);
            this.imgActivityListArrow_1.setImageResource(R.drawable.icon_arrow_up);

            this.llActivityListChart_2.removeAllViews();
            this.llActivityListChart_2.addView(activityChartView_2);
            this.imgActivityListArrow_2.setImageResource(R.drawable.icon_arrow_up);

        }else{
            this.mDataStateType = DataStateType.NONE;
            this.llActivityListChart_1.removeAllViews();
            this.llActivityListChart_2.removeAllViews();
        }
    }

    private String getWeekNumberChar( int weekNumber )
    {
        return Utils.toWeekString(weekNumber,  getResources().getConfiguration().locale);
    }

    @Click(R.id.rl_activity_friend_activityinfo_select_day)
    void selectDayClick(){
        this.getDayActivity();
    }

    @Click(R.id.rl_activity_friend_activityinfo_select_week)
    void selectWeekClick(){
        this.getWeekActivity();
    }

    @Click(R.id.rl_activity_friend_activityinfo_select_month)
    void selectMonthClick(){
        this.getMonthActivity();
    }


    private String getDateString( Calendar calendar )
    {
        return calendar.get(Calendar.YEAR) + "-" + String.format("%02d", calendar.get(Calendar.MONTH) + 1) + "-" + String.format("%02d", calendar.get(Calendar.DATE));
    }

    @Click(R.id.tv_activity_friend_activityinfo_next_date)
    void moveNextDate()
    {
        switch( this.mSelectType )
        {
            case DAY:
                activityCalendar_day.add( Calendar.DATE , 1 );
                this.getDayActivity();
                break;
            case WEEK:
                activityCalendar_week.add( Calendar.DATE , 7 );
                this.getWeekActivity();
                break;
            case MONTH:
                activityCalendar_month.add( Calendar.MONTH , 1 );
                this.getMonthActivity();
                break;
        }
    }

    @Click(R.id.tv_activity_friend_activityinfo_pre_date)
    void movePreDate()
    {
        switch( this.mSelectType )
        {
            case DAY:
                this.activityCalendar_day.add( Calendar.DATE , -1 );
                this.getDayActivity();
                break;
            case WEEK:
                this.activityCalendar_week.add( Calendar.DATE , -7 );
                this.getWeekActivity();
                break;
            case MONTH:
                this.activityCalendar_month.add( Calendar.MONTH , -1 );
                this.getMonthActivity();
                break;
        }
    }

    private Calendar resetTime (Date d) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(d);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal;
    }

    private void setVisible_NextButton(boolean setVisible){

        if(moveToNext!=null)
            if(setVisible){
                moveToNext.setVisibility(View.VISIBLE);
            }else{
                moveToNext.setVisibility(View.INVISIBLE);
            }
    }

    private int[] getDay24CalorieArray(float[] metarray) {

        FitmateDBManager dbManager = new FitmateDBManager(this);

        float weight =  dbManager.getUserInfo().getWeight();
        int[] result = new int[24];

        int datasavinginterval= 86400 / metarray.length;

        int count=60*60/datasavinginterval;
        int calsum=0;



        for(int i=0;i<metarray.length;i+=count)
        {
            for(int j=0;j<count;j++)
            {
                if(metarray[i+j]>1) {
                    calsum += ExerciseAnalyzer.Calculate_CalorieFromMET(datasavinginterval, weight, metarray[i + j]);
                }
            }
            result[i/count]= Math.round(calsum/1000.f);
            calsum=0;
        }
        return result;
    }

}