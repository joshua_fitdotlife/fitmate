package com.fitdotlife.fitmate;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.fitdotlife.fitmate_lib.customview.FitmeterDialog;
import com.fitdotlife.fitmate_lib.key.GenderType;
import com.fitdotlife.fitmate_lib.service.util.Utils;
import com.fitdotlife.fitmate_lib.util.DateUtils;

import java.util.Calendar;

/**
 * Created by Joshua on 2016-07-27.
 */
public class UserInfoFragment extends Fragment implements View.OnClickListener {

    private final float DEFAULT_HEIGHT = 170;
    private final float DEFAULT_WEIGHT = 70;

    private View actionBarView = null;
    private ImageView imgBack = null;
    private TextView tvTitle = null;

    //성별 정보
    private int MAN = 1;
    private int WOMAN = 0;
    private ImageView imgMan = null;
    private ImageView imgWoman = null;
    private int mSelectedGender = -1;

    //체력관련 정보
    private EditText etxBirthDay = null;
    private TextView tvWeight = null;
    private TextView tvHeight = null;

    private Button btnNext = null;

    private boolean mSI = true;
    private float mHeight = 0;
    private float mWeight = 0;
    private long mBirthDate = 0;
    private GenderType mGender = null;

    public static UserInfoFragment newInstance(  )
    {
        Bundle args = new Bundle();
        UserInfoFragment fragment = new UserInfoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();

        if( mSelectedGender > -1 )
        {
            if( mSelectedGender == MAN ) {
                this.imgMan.setImageResource(R.drawable.icon_male_sel);
                this.imgWoman.setImageResource(R.drawable.icon_female);
            }else if(mSelectedGender == WOMAN){
                this.imgMan.setImageResource(R.drawable.icon_male);
                this.imgWoman.setImageResource(R.drawable.icon_female_sel);
            }
        }

        if( mWeight > 0 )
        {
            String weightUnitString = "";
            if (mSI) {
                //파운드에서 킬로그램으로 변환한다.
                weightUnitString  = getString( R.string.common_kilogram );
            } else {
                //킬로그램에서 파운드로 변환한다.
                weightUnitString  = getString( R.string.common_pound );
            }

            String[] weightValueArray = getValueArray(mWeight);
            tvWeight.setText(String.format("%s %s", weightValueArray[0] + "." + weightValueArray[1], weightUnitString));

        }

        if( mHeight > 0 ){

            String heightUnitString = "";
            if (mSI) {
                //파운드에서 킬로그램으로 변환한다.
                heightUnitString = getString( R.string.common_centimeter );
            } else {
                //킬로그램에서 파운드로 변환한다.
                heightUnitString = getString( R.string.common_ft );
            }
            String[] heightValueArray = getValueArray( mHeight );
            tvHeight.setText(String.format("%s %s", heightValueArray[0] + "." + heightValueArray[1] , heightUnitString  ) );
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, final Bundle savedInstanceState) {
        View rootView = inflater.inflate( R.layout.fragment_userinfo , container , false );

        this.actionBarView = rootView.findViewById(R.id.ic_fragment_userinfo_actionbar);

        this.actionBarView = rootView.findViewById(R.id.ic_fragment_userinfo_actionbar);
        this.imgBack = (ImageView) rootView.findViewById(R.id.img_custom_actionbar_white_left);
        this.imgBack.setBackgroundResource(R.drawable.icon_back_red_selector);
        this.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }
        });
        this.tvTitle = (TextView) rootView.findViewById(R.id.tv_custom_actionbar_white_title);
        tvTitle.setText( this.getResources().getString( R.string.signin_bodyinformation) );

        this.imgMan = (ImageView) rootView.findViewById(R.id.img_fragment_userinfo_man);
        this.imgMan.setOnClickListener(this);
        this.imgWoman = (ImageView) rootView.findViewById(R.id.img_fragment_userinfo_woman);
        this.imgWoman.setOnClickListener(this);
        this.etxBirthDay = (EditText) rootView.findViewById(R.id.etx_fragment_userinfo_birthday);
        this.tvHeight = (TextView) rootView.findViewById(R.id.tv_fragment_userinfo_height);
        this.tvHeight.setOnClickListener(this);
        this.tvWeight = (TextView) rootView.findViewById(R.id.tv_fragment_userinfo_weight);
        this.tvWeight.setOnClickListener(this);

        this.btnNext = (Button) rootView.findViewById(R.id.btn_fragment_userinfo_next);
        this.btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveUserInfo();
            }
        });
        return rootView;
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.tv_fragment_userinfo_height:
                showHeightDialog();
                break;
            case R.id.tv_fragment_userinfo_weight:
                showWeightDialog();
                break;
            case R.id.img_fragment_userinfo_man:
                this.imgMan.setImageResource( R.drawable.icon_male_sel );
                this.imgWoman.setImageResource( R.drawable.icon_female );
                this.mSelectedGender = MAN;
                break;
            case R.id.img_fragment_userinfo_woman:
                this.imgMan.setImageResource( R.drawable.icon_male);
                this.imgWoman.setImageResource( R.drawable.icon_female_sel );
                this.mSelectedGender = WOMAN;
                break;
        }
    }

    private void showHeightDialog(){
        boolean heightUnit = mSI;

        final FitmeterDialog heightDialog = new FitmeterDialog( getActivity() );
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View numberPickerView = inflater.inflate(R.layout.dialog_number_picker, null);
        TextView tvTitle = (TextView) numberPickerView.findViewById(R.id.tv_dialog_number_picker_title);
        tvTitle.setText(this.getString(R.string.common_height));
        final NumberPicker np = (NumberPicker) numberPickerView.findViewById(R.id.np_dialog_number_picker);
        np.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        final NumberPicker pp = (NumberPicker) numberPickerView.findViewById(R.id.np_dialog_point_picker);
        pp.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        final NumberPicker up = (NumberPicker) numberPickerView.findViewById( R.id.np_dialog_unit_picker );
        up.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        up.setDisplayedValues(new String[]{this.getString(R.string.common_centimeter), this.getString(R.string.common_ft)});

        final float height = mHeight;
        np.setMaxValue(300);
        np.setMinValue(1);
        pp.setMaxValue(9);
        pp.setMinValue(0);
        up.setMaxValue(1);
        up.setMinValue(0);

        if( height > 0 ) {

            String[] valueArray = this.getValueArray( height );
            np.setValue( Integer.parseInt( valueArray[0] ) );
            pp.setValue( Integer.parseInt( valueArray[1] ) );

            up.setTag(height);

        }else{

            if( mSI )
            {
                np.setValue((int) DEFAULT_HEIGHT);
                up.setTag(DEFAULT_HEIGHT);
            }else{
                float dHeight = Utils.cm_to_ft(DEFAULT_HEIGHT);
                dHeight = Math.round( dHeight );
                np.setValue((int) dHeight);
                up.setTag( dHeight );
            }
        }

        if( heightUnit )
        {
            up.setValue(0);
        }
        else
        {
            up.setValue(1);
        }

        heightDialog.setContentView(numberPickerView);

        up.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                boolean heightSi = (newVal == 0);
                float tempHeight = (float) up.getTag();

                //몸무게를 변환한다.
                if (heightSi) {
                    //파운드에서 킬로그램으로 변환한다.
                    tempHeight = com.fitdotlife.fitmate_lib.service.util.Utils.ft_to_cm(tempHeight);
                } else {
                    //킬로그램에서 파운드로 변환한다.
                    tempHeight = com.fitdotlife.fitmate_lib.service.util.Utils.cm_to_ft(tempHeight);
                }

                String[] valueArray = getValueArray( tempHeight );
                np.setValue( Integer.parseInt( valueArray[0] ) );
                pp.setValue( Integer.parseInt( valueArray[1]) );

                up.setTag(tempHeight);
            }
        });

        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                up.setTag((float) (((np.getValue() * 10d) + pp.getValue()) / 10d));
            }
        });

        pp.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                up.setTag((float) (((np.getValue() * 10d) + pp.getValue()) / 10d));
            }
        });

        View buttonView = inflater.inflate(R.layout.dialog_button_two, null);
        Button btnLeft = (Button) buttonView.findViewById(R.id.btn_dialog_button_two_left);
        btnLeft.setText(this.getString(R.string.common_cancel));
        btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                heightDialog.close();
            }
        });
        Button btnRight = (Button) buttonView.findViewById(R.id.btn_dialog_button_two_right);
        btnRight.setText(this.getString(R.string.common_ok));
        btnRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean heightSi = (up.getValue() == 0);
                boolean isSi = mSI;
                float tempWeight = mWeight;
                String weightUnitString = "";

                if (heightSi != isSi) {
                    //몸무게를 변환한다.
                    if (tempWeight > 0) {
                        if (heightSi) {
                            //파운드에서 킬로그램으로 변환한다.
                            tempWeight = Utils.lb_to_kg(tempWeight);
                            weightUnitString = getString(R.string.common_kilogram);
                        } else {
                            //킬로그램에서 파운드로 변환한다.
                            tempWeight = Utils.kg_to_lb(tempWeight);
                            weightUnitString = getString(R.string.common_pound);
                        }

                        String[] weightValueArray = getValueArray(tempWeight);
                        tvWeight.setText(String.format("%s %s", weightValueArray[0] + "." + weightValueArray[1], weightUnitString));
                    }

                }

                mHeight = (Float) up.getTag();
                mWeight = tempWeight;
                mSI = heightSi;

                String[] heightValueArray = getValueArray( mHeight );
                tvHeight.setText(String.format("%s %s", heightValueArray[0] + "." + heightValueArray[1], up.getDisplayedValues()[up.getValue()]));

                heightDialog.close();
            }
        });
        heightDialog.setButtonView( buttonView );
        heightDialog.show();
    }

    private void showWeightDialog(){
        boolean weightUnit = mSI;

        final FitmeterDialog weightDialog = new FitmeterDialog( getActivity() );
        LayoutInflater weightInflater = getActivity().getLayoutInflater();

        final View weightNumberPickerView = weightInflater.inflate(R.layout.dialog_number_picker, null);
        TextView tvWeightTitle = (TextView) weightNumberPickerView.findViewById(R.id.tv_dialog_number_picker_title);
        tvWeightTitle.setText(this.getString(R.string.common_weight));
        final NumberPicker weightNP = (NumberPicker) weightNumberPickerView.findViewById(R.id.np_dialog_number_picker);
        weightNP.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        final NumberPicker weightPP = (NumberPicker) weightNumberPickerView.findViewById(R.id.np_dialog_point_picker);
        weightPP.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        final NumberPicker weightUP = (NumberPicker) weightNumberPickerView.findViewById(R.id.np_dialog_unit_picker);
        weightUP.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        weightUP.setDisplayedValues(new String[]{this.getString(R.string.common_kilogram), this.getString(R.string.common_pound)});

        float weight = mWeight;
        weightNP.setMaxValue(500);
        weightNP.setMinValue(1);
        weightPP.setMaxValue(9);
        weightPP.setMinValue(0);
        weightUP.setMaxValue(1);
        weightUP.setMinValue(0);

        if( weightUnit )
        {
            weightUP.setValue(0);
        }else{
            weightUP.setValue(1);
        }

        if( weight > 0 ) {

            String[] valueArray = this.getValueArray( weight );
            weightNP.setValue(Integer.parseInt(valueArray[0]));
            weightPP.setValue(Integer.parseInt(valueArray[1]));

            weightUP.setTag(weight);

        }else{
            if( mSI ) {
                weightNP.setValue((int) DEFAULT_WEIGHT);
                weightUP.setTag(DEFAULT_WEIGHT);
            }else{
                float dWeight = Utils.kg_to_lb( DEFAULT_WEIGHT );
                dWeight = Math.round( dWeight );
                weightNP.setValue((int) dWeight);
                weightUP.setTag( dWeight );
            }
        }

        weightUP.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                boolean weightSi = (newVal == 0);
                float tempWeight = (float) weightUP.getTag();

                //몸무게를 변환한다.
                if (weightSi) {
                    //파운드에서 킬로그램으로 변환한다.
                    tempWeight = com.fitdotlife.fitmate_lib.service.util.Utils.lb_to_kg(tempWeight);
                } else {
                    //킬로그램에서 파운드로 변환한다.
                    tempWeight = com.fitdotlife.fitmate_lib.service.util.Utils.kg_to_lb(tempWeight);
                }

                String[] valueArray = getValueArray(tempWeight);
                weightNP.setValue(Integer.parseInt(valueArray[0]));
                weightPP.setValue(Integer.parseInt(valueArray[1]));

                weightUP.setTag(tempWeight);
            }
        });

        weightNP.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                weightUP.setTag((float) (((weightNP.getValue() * 10d) + weightPP.getValue()) / 10d) );
            }
        });

        weightPP.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                weightUP.setTag((float) (((weightNP.getValue() * 10d) + weightPP.getValue()) / 10d));
            }
        });

        weightDialog.setContentView(weightNumberPickerView);

        View buttonView = getActivity().getLayoutInflater().inflate(R.layout.dialog_button_two, null);
        Button btnLeft = (Button) buttonView.findViewById(R.id.btn_dialog_button_two_left);
        btnLeft.setText(this.getString(R.string.common_cancel));
        btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                weightDialog.close();
            }
        });
        Button btnRight = (Button) buttonView.findViewById(R.id.btn_dialog_button_two_right);
        btnRight.setText(this.getString(R.string.common_ok));
        btnRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean weightSi = ( weightUP.getValue() ==0 );
                boolean isSi = mSI;
                float tempHeight = mHeight;
                String heightUnitString = "";

                if (weightSi != isSi ) {
                    //키를 변환한다.
                    if( tempHeight > 0 )
                    {
                        if (weightSi) {
                            tempHeight = Utils.ft_to_cm(tempHeight);
                            heightUnitString = getString(R.string.common_centimeter);
                        } else {
                            tempHeight = Utils.cm_to_ft(tempHeight);
                            heightUnitString = getString(R.string.common_ft);
                        }

                        String[] heightValueArray = getValueArray( tempHeight );
                        tvHeight.setText(String.format("%s %s", heightValueArray[0] + "." + heightValueArray[1] , heightUnitString ));
                    }
                }

                mWeight = (Float) weightUP.getTag();
                mHeight = tempHeight;
                mSI = weightSi;

                String[] weightValueArray = getValueArray( mWeight );
                tvWeight.setText(String.format("%s %s", weightValueArray[0] + "." + weightValueArray[1], weightUP.getDisplayedValues()[weightUP.getValue()]));
                weightDialog.close();
            }
        });

        weightDialog.setButtonView( buttonView );
        weightDialog.show();
    }

    private String[] getValueArray( float value ){
        String valueString = String.valueOf( value );
        String[] result = null;
        String[] splitedWeightString = valueString.split("\\.");

        if( splitedWeightString.length > 1 ){
            splitedWeightString[1] = splitedWeightString[1].substring(0,1);
            result = splitedWeightString;
        }else{
            result = new String[]{ splitedWeightString[0] , "0" };
        }

        return result;
    }

    private boolean valid(){
        if (this.mSelectedGender == -1) {
            showToast(getResources().getString(R.string.signin_gender_select));
            return false;
        }

        if (this.etxBirthDay.getText().length() == 0) {
            showToast(getResources().getString(R.string.signin_birthday_input));
            this.etxBirthDay.requestFocus();
            return false;
        }

        if (this.etxBirthDay.getText().length() != 8) {
            showToast( getResources().getString(R.string.signin_birthday_do_not_match_digits) );
            this.etxBirthDay.requestFocus();
            return false;
        }

        //년도를 잘라낸다.
        int birthYear = Integer.parseInt(this.etxBirthDay.getText().toString().substring(0, 4));
        Calendar calendar = Calendar.getInstance();
        int currentYear = calendar.get(Calendar.YEAR);
        if ((currentYear - birthYear) > 150) {
            showToast( getResources().getString(R.string.signin_birthday_wrong_year) );
            this.etxBirthDay.requestFocus();
            return false;
        }

        if (currentYear < birthYear) {
            showToast(getResources().getString(R.string.signin_birthday_wrong_year));
            this.etxBirthDay.requestFocus();
            return false;
        }

        //월을 잘라낸다
        int birthMonth = Integer.parseInt(this.etxBirthDay.getText().toString().substring(4, 6));
        if (birthMonth < 1 || birthMonth > 12) {
            showToast(getResources().getString(R.string.signin_birthday_wrong_month));
            this.etxBirthDay.requestFocus();
            return false;
        }

        //일을 잘라낸다.
        int birthDay = Integer.parseInt(this.etxBirthDay.getText().toString().substring(6, 8));
        calendar.set(birthYear, birthMonth - 1, 1);
        int lastDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        if (birthDay < 1 || birthDay > lastDay) {
            showToast(getResources().getString(R.string.signin_birthday_wrong_day));
            this.etxBirthDay.requestFocus();
            return false;
        }

        if (this.tvHeight.getText().toString().trim().length() == 0) {
            showToast( this.getResources().getString(R.string.signin_height_input) );
            this.tvHeight.requestFocus();
            return false;
        }

        if (this.tvWeight.getText().toString().trim().length() == 0) {
            showToast( this.getResources().getString(R.string.signin_weight_input) );
            this.tvWeight.requestFocus();
            return false;
        }

        long longUtcTime = DateUtils.convertDateStringtoUTCTick(this.etxBirthDay.getText().toString(), "yyyyMMdd");
        mBirthDate = longUtcTime;
        mGender = GenderType.getGenderType(this.mSelectedGender);
        return true;
    }

    private void saveUserInfo()
    {
        if( valid() ){
            ( (SetUserActivity)getActivity()).saveUserInfo( mGender , mBirthDate , mWeight , mHeight , mSI );
        }
    }

    private void showToast(String message){
        Toast.makeText(getActivity() , message , Toast.LENGTH_LONG).show();
    }
}
