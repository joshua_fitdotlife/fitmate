package com.fitdotlife.fitmate;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.StrengthInputType;
import com.fitdotlife.fitmate_lib.iview.ISetExerciseProgramView;
import com.fitdotlife.fitmate_lib.key.ContinuousExerciseType;
import com.fitdotlife.fitmate_lib.object.ExerciseProgram;
import com.fitdotlife.fitmate_lib.presenter.SetExerciseProgramPresenter;
import com.fitdotlife.fitmate_lib.customview.ComboSeekBarView;
import com.fitdotlife.fitmate_lib.customview.RangeSeekBarView;


public class SetExerciseProgramActivity extends Activity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener , ISetExerciseProgramView, RangeSeekBarView.RangeSeekBarListener, ComboSeekBarView.ComboSeekBarListener, CompoundButton.OnCheckedChangeListener, AdapterView.OnItemClickListener {

    private final int STRENGTH_TYPE_MET = 4;
    private final int INITIAL_STRENGTH_FROM = 2;
    private final int INITIAL_STRENGTH_TO = 4;
    private final int INITIAL_MINUTE_TO = 20;
    private final int INITIAL_MINUTE_FROM = 40;
    private final int INITIAL_TIMES_FROM = 3;
    private final int INITIAL_TIMES_TO = 5;

    private LinearLayout llMarkContainer = null;
    private LayoutInflater inflater = null;
    private ScrollView scrContainer = null;
    private ImageView imgClose = null;
    private ImageView imgBack = null;

    private Button btnNext = null;

    private int mStep = -1;

    private View[] mChildCiewList = null;
    private View[] mMarkViewList = null;

    private EditText etxExerciseProgramName = null;
    private EditText etxExerciseProgramInformation = null;

    private RadioGroup rgSelectStrengthInputType = null;

    private TextView tvSettingTime = null;
    private TextView tvExerciseTime = null;
    private TextView tvExerciseTimes = null;
    private RangeSeekBarView rsbSelectStrengthRange = null;
    private ComboSeekBarView csbSelectExerciseTimes = null;
    private RangeSeekBarView rsbSelectExerciseTime = null;
    private LinearLayout llStrengthInputType = null;
    private Spinner spnExerciseStrengthFrom = null;
    private Spinner spnExerciseStrengthTo = null;

    private CheckBox cbContinueExercise = null;

    private SetExerciseProgramPresenter mPresenter = null;
    private ExerciseProgram mExerciseProgramSNU = null;
    
    private InputMethodManager inputMethodManager = null;
    private int mLastStep = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        this.setContentView( R.layout.activity_set_exercise_program );

        this.mPresenter = new SetExerciseProgramPresenter( this , this );

        this.inputMethodManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        
        this.inflater = this.getLayoutInflater();
        this.llMarkContainer = (LinearLayout) this.findViewById( R.id.ll_activity_set_exerciseprogram_step_mark);
        this.scrContainer = (ScrollView) this.findViewById(R.id.scr_activity_set_exerciseprogram_container);
        this.btnNext = (Button) this.findViewById(R.id.btn_activity_set_exerciseprogram_next);
        this.btnNext.setOnClickListener(this);

        this.imgClose = (ImageView) this.findViewById(R.id.img_activity_set_exerciseprogram_close);
        this.imgClose.setOnClickListener(this);
        this.imgBack = (ImageView) this.findViewById(R.id.img_activity_set_exerciseprogram_back);
        this.imgBack.setOnClickListener(this);

        this.mChildCiewList = new View[4];
        this.mMarkViewList = new View[4];

        this.mChildCiewList[0] = this.inflater.inflate(R.layout.child_create_exercise_step_1, this.scrContainer , false );
        this.etxExerciseProgramName = (EditText) this.mChildCiewList[0].findViewById(R.id.etx_child_create_exercise_name);
        this.etxExerciseProgramInformation = (EditText) this.mChildCiewList[0].findViewById(R.id.etx_child_create_exercise_information);
        this.mMarkViewList[0] = this.inflater.inflate(R.layout.child_step_mark_1of4, this.llMarkContainer , false );

        this.mChildCiewList[1] = this.inflater.inflate(R.layout.child_create_exercise_step_2, this.scrContainer , false );
        this.rgSelectStrengthInputType = (RadioGroup) this.mChildCiewList[1].findViewById(R.id.rg_child_create_exercise_select_strength_type);
        this.rgSelectStrengthInputType.setOnCheckedChangeListener(this);
        this.mMarkViewList[1] = this.inflater.inflate(R.layout.child_step_mark_2of4, this.llMarkContainer , false );

        this.mChildCiewList[2] = this.inflater.inflate(R.layout.child_create_exercise_step_3, this.scrContainer , false );
        this.rsbSelectStrengthRange = (RangeSeekBarView) this.mChildCiewList[2].findViewById(R.id.rsb_child_create_exercise_select_strength_range);
        this.rsbSelectStrengthRange.setListener(this);
        this.rsbSelectStrengthRange.setInitialValue(this.INITIAL_STRENGTH_FROM, this.INITIAL_STRENGTH_TO);
        this.llStrengthInputType = (LinearLayout) this.mChildCiewList[2].findViewById(R.id.ll_child_create_exercise_strength_range);

        this.csbSelectExerciseTimes = (ComboSeekBarView) this.mChildCiewList[2].findViewById(R.id.csb_child_create_exercise_select_exercise_times);
        this.csbSelectExerciseTimes.setListener(this);
        this.csbSelectExerciseTimes.setInitialValue(this.INITIAL_TIMES_FROM, this.INITIAL_TIMES_TO);
        this.tvSettingTime = (TextView) this.mChildCiewList[2].findViewById(R.id.tv_child_create_exercise_setting_exercise_time);
        this.tvExerciseTime = (TextView) this.mChildCiewList[2].findViewById(R.id.tv_child_create_exercise_time);
        this.tvExerciseTimes = (TextView) this.mChildCiewList[2].findViewById(R.id.tv_child_create_exercise_times);
        this.mMarkViewList[2] = this.inflater.inflate(R.layout.child_step_mark_3of4, this.llMarkContainer , false );

        this.mChildCiewList[3] = this.inflater.inflate(R.layout.child_create_exercise_step_4, this.scrContainer , false );
        this.cbContinueExercise = (CheckBox) this.mChildCiewList[3].findViewById(R.id.cb_child_create_exercise_continue_exercise);
        this.cbContinueExercise.setOnCheckedChangeListener(this);
        this.mMarkViewList[3] = this.inflater.inflate(R.layout.child_step_mark_4of4, this.llMarkContainer , false );

        //운동 프로그램 초기 정보 입력
        this.mExerciseProgramSNU = new ExerciseProgram();
        this.mExerciseProgramSNU.setStrengthFrom(this.INITIAL_STRENGTH_FROM);
        this.mExerciseProgramSNU.setStrengthTo(this.INITIAL_STRENGTH_TO);
        this.mExerciseProgramSNU.setMinuteFrom(this.INITIAL_MINUTE_FROM);
        this.mExerciseProgramSNU.setMinuteTo(this.INITIAL_MINUTE_TO);
        this.mExerciseProgramSNU.setTimesFrom(this.INITIAL_TIMES_FROM);
        this.mExerciseProgramSNU.setTimesTo(this.INITIAL_TIMES_TO);
        this.mExerciseProgramSNU.setCategory( this.STRENGTH_TYPE_MET );
        this.mExerciseProgramSNU.setContinuousExercise( ContinuousExerciseType.NO_CONTINUOUS_EXERCISE );

        this.moveFrontView();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
    }

    @Override
    public void onClick(View view) {

        switch( view.getId() )
        {
            case R.id.btn_activity_set_exerciseprogram_next:
                if( this.validCheckThenSave() )
                {
                    if( this.mStep == mLastStep )
                    {
                        this.mExerciseProgramSNU.setApplied(true);
                        this.mPresenter.createUserExerciseProgram(this.mExerciseProgramSNU);
                        return;
                    }

                    this.moveFrontView();
                }
                break;
            case R.id.img_activity_set_exerciseprogram_close:
                this.finish();
                break;
            case R.id.img_activity_set_exerciseprogram_back:
                this.moveBackView();
                break;
        }
    }

    private void moveFrontView()
    {
        this.mStep += 1;
        if( mStep == 3 )
        {
        	this.btnNext.setText("Finish");
        }

        if( this.mStep == 2 ) {

            this.mLastStep = 3;
            llStrengthInputType.removeAllViews();
            StrengthInputType strengthType = StrengthInputType.getStrengthType(this.mExerciseProgramSNU.getCategory());

            if ( strengthType == StrengthInputType.STRENGTH ) {

                View strengthTypeView = this.getLayoutInflater().inflate(R.layout.child_create_exercise_strength_type, null);
                this.rsbSelectExerciseTime = (RangeSeekBarView) strengthTypeView.findViewById(R.id.rsb_child_create_exercise_select_exercise_time);
                this.rsbSelectExerciseTime.setListener(this);
                this.rsbSelectExerciseTime.setInitialValue(this.INITIAL_MINUTE_FROM, this.INITIAL_MINUTE_TO);
                this.llStrengthInputType.addView(strengthTypeView);

            } else {

                View strengthESTypeView = this.getLayoutInflater().inflate(R.layout.child_create_exercise_strength_type_es, null);
                this.spnExerciseStrengthFrom = (Spinner) strengthESTypeView.findViewById(R.id.spn_child_create_exercise_strength_type_from);
                this.spnExerciseStrengthFrom.setOnItemClickListener(this);
                this.spnExerciseStrengthTo = (Spinner) strengthESTypeView.findViewById(R.id.spn_child_create_exercise_strength_type_to);
                this.spnExerciseStrengthTo.setOnItemClickListener(this);

                if (strengthType == StrengthInputType.CALORIE || strengthType == StrengthInputType.CALORIE_SUM || strengthType == StrengthInputType.VS_BMR) {
                    //연속운동 설정과 운동시간 설정이 없어짐. 바로 Finish
                    this.mLastStep = 2;
                    this.btnNext.setText("Finish");

                    this.tvExerciseTime.setVisibility(View.INVISIBLE);
                    this.rsbSelectExerciseTime.setVisibility(View.INVISIBLE);

                    if (strengthType == StrengthInputType.CALORIE_SUM){
                        this.tvExerciseTimes.setVisibility(View.INVISIBLE);
                        this.csbSelectExerciseTimes.setVisibility(View.INVISIBLE);
                        this.tvSettingTime.setVisibility(View.INVISIBLE);
                    }
                }
            }
        }

        this.drawChildView();
    }

    private void moveBackView()
    {
        this.mStep -= 1;
        if( mStep == 2 )
        {
            this.btnNext.setText( "Next" );
        }

        if( mStep == 1 ){

            this.tvExerciseTime.setVisibility(View.VISIBLE);
            this.rsbSelectExerciseTime.setVisibility( View.VISIBLE );
            this.tvExerciseTimes.setVisibility( View.VISIBLE );
            this.csbSelectExerciseTimes.setVisibility( View.VISIBLE );
            this.tvSettingTime.setVisibility(View.VISIBLE);

            this.btnNext.setText("Next");
        }

        this.drawChildView();
    }

    private void drawChildView()
    {
        this.scrContainer.removeAllViews();
        this.llMarkContainer.removeAllViews();

        this.scrContainer.addView( this.mChildCiewList[ mStep ] );
        this.llMarkContainer.addView( this.mMarkViewList[ mStep ] );

        if( this.mStep == 0 )
        {
            this.imgBack.setVisibility( View.INVISIBLE );
        }
        else
        {
            this.imgBack.setVisibility(View.VISIBLE);
        }
        
        if( this.mStep == 1 ){
        	this.scrContainer.fullScroll( 0 );
        	this.inputMethodManager.hideSoftInputFromWindow(this.etxExerciseProgramName.getWindowToken() , 0);
        }

    }

    private boolean validCheckThenSave()
    {
        if( mStep == 0 )
        {
            if( this.etxExerciseProgramName.getText().length() == 0 )
            {
                Toast.makeText(this, "운동프로그램 이름을 입력하여 주십시오.", Toast.LENGTH_LONG).show();
                this.etxExerciseProgramName.requestFocus();
                return false;
            }
            else if( this.etxExerciseProgramInformation.getText().length() == 0 )
            {
                Toast.makeText(this, "운동프로그램 설명을 입력하여 주십시오." , Toast.LENGTH_LONG ).show();
                this.etxExerciseProgramInformation.requestFocus();
                return false;
            }

            this.mExerciseProgramSNU.setName(this.etxExerciseProgramName.getText().toString());
            this.mExerciseProgramSNU.setInfo(this.etxExerciseProgramInformation.getText().toString());
        }
        else if( mStep == 1 )
        {
//            if( !this.isCheckedStrengthInputType )
//            {
//                Toast.makeText(this, "운동프로그램  강도의 종류를 선택하여 주십시오." , Toast.LENGTH_LONG ).show();
//                this.rgSelectStrengthInputType.requestFocus();
//                return false;
//            }


        }

        return true;
    }
    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId)
    {
        int strengthType = Integer.parseInt( (String) this.findViewById( checkedId ).getTag() );
        this.mExerciseProgramSNU.setCategory( strengthType );
    }

    @Override
    public void onCheckedChanged(CompoundButton view , boolean isChecked)
    {
        switch( view.getId() )
        {
            case R.id.cb_child_create_exercise_continue_exercise:
                if( isChecked )
                {
                    this.mExerciseProgramSNU.setContinuousExercise( ContinuousExerciseType.CONTINUOUS_EXERCISE );
                }
                else
                {
                    this.mExerciseProgramSNU.setContinuousExercise( ContinuousExerciseType.NO_CONTINUOUS_EXERCISE );
                }
                break;
        }
    }
    @Override
    public void onCreate(RangeSeekBarView rangeSeekBar, int index, float value)
    {	}
    @Override
    public void onSeek(RangeSeekBarView rangeSeekBar, int index, float value)
    {
        if( rangeSeekBar.getId() == R.id.rsb_child_create_exercise_select_strength_range )
        {
            if( index == 0 )
            {
                this.mExerciseProgramSNU.setStrengthFrom(value);
            }
            else if( index == 1 )
            {
                this.mExerciseProgramSNU.setStrengthTo(value);
            }
        }
        else if( rangeSeekBar.getId() == R.id.rsb_child_create_exercise_select_exercise_time )
        {
            if( index == 0 ){
                this.mExerciseProgramSNU.setMinuteFrom((int) value);
            }else if(index == 1){
                this.mExerciseProgramSNU.setMinuteTo((int) value);
            }
        }
    }
    @Override
    public void onSeekStart(RangeSeekBarView rangeSeekBar, int index, float value)
    {	}
    @Override
    public void onSeekStop(RangeSeekBarView rangeSeekBar, int index, float value)
    {	}
    @Override
    public void onCreate(ComboSeekBarView comboSeekBar, int index, float value)
    {	}
    @Override
    public void onSeek(ComboSeekBarView comboSeekBar, int index, float value)
    {
        if( comboSeekBar.getId() == R.id.csb_child_create_exercise_select_exercise_times )
        {
            if( index == 0 ){
            this.mExerciseProgramSNU.setTimesFrom((int) value);
        }else if( index == 1 ){
            this.mExerciseProgramSNU.setTimesTo((int) value);
        }
        }
    }
    @Override
    public void onSeekStart(ComboSeekBarView comboSeekBar, int index, float value)
    {	}
    @Override
    public void onSeekStop(ComboSeekBarView comboSeekBar, int index, float value)
    {   }
	@Override
	public void showResult( String message ) {
		Toast.makeText(this, message , Toast.LENGTH_SHORT).show();
		
	}
	@Override
	public void finishActivity() {
		this.finish();
		
	}

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if( view.getId() == R.id.spn_child_create_exercise_strength_type_from ){
            
        }else if( view.getId() == R.id.spn_child_create_exercise_strength_type_to ){

        }
    }
}
