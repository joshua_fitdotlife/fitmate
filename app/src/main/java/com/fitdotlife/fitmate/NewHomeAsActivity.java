package com.fitdotlife.fitmate;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fitdotlife.Preprocess;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseAnalyzer;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.UserInfoForAnalyzer;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.WearingLocation;
import com.fitdotlife.fitmate.newhome.CategoryType;
import com.fitdotlife.fitmate.newhome.DayPagerAdapter;
import com.fitdotlife.fitmate.newhome.NewHomeCategoryData;
import com.fitdotlife.fitmate.newhome.NewHomeConstant;
import com.fitdotlife.fitmate.newhome.NewHomeProgramClickListener;
import com.fitdotlife.fitmate.newhome.NewHomeUtils;
import com.fitdotlife.fitmate.newhome.WeekPagerActivityInfo;
import com.fitdotlife.fitmate.newhome.WeekPagerAdapter;
import com.fitdotlife.fitmate_lib.customview.DrawerView;
import com.fitdotlife.fitmate_lib.customview.FitmeterDialog;
import com.fitdotlife.fitmate_lib.customview.NewHomeCategoryView;
import com.fitdotlife.fitmate_lib.customview.NewHomeCategoryView_;
import com.fitdotlife.fitmate_lib.customview.NewHomeDayCircleView_;
import com.fitdotlife.fitmate_lib.customview.NewHomeDayHalfCircleView_;
import com.fitdotlife.fitmate_lib.customview.NewHomeDayTextView_;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.object.DayActivity;
import com.fitdotlife.fitmate_lib.object.ExerciseProgram;
import com.fitdotlife.fitmate_lib.object.FriendWeekActivity;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.util.DateUtils;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Joshua on 2015-12-22.
 */
@EActivity(R.layout.activity_newhome_as)
public class NewHomeAsActivity extends Activity
{
    private Calendar mTodayCalendar = null;
    private int mTodayWeekNumber = 0;
    private Calendar mDayCalendar = null;
    private Calendar mWeekCalendar = null;

    private FitmateDBManager mDBManager = null;
    private UserInfo mUserInfo = null;

    private int selectdCategoryIndex = 3;

    private int[] mCategoryDefaultValues = new int[]{ 2, 100 , 500 , 0 , 0, 10 , 60 , 10 };
    NewHomeCategoryView[] categoryViews = new NewHomeCategoryView[CategoryType.values().length];

    private NewHomeUtils newHomeUtils = null;
    private NewHomeAsWeekPagerAdaper mWeekPagerAdapter = null;
    private WeekPagerActivityInfo[] mWeekPagerActivityInfoList = new WeekPagerActivityInfo[1];

    private Timer animationTimer = null;

    @ViewById(R.id.ll_activity_newhome_as_actionbar)
    RelativeLayout llActionBar;

    @ViewById(R.id.iv_activity_newhome_as_category_swipe)
    ImageView ivCategorySwipe;

    @ViewById(R.id.tv_activity_newhome_as_weekday)
    TextView tvWeekDay;

    @ViewById(R.id.tv_activity_newhome_as_date)
    TextView tvDate;

    @ViewById(R.id.rl_activity_newhome_as_category)
    RelativeLayout rlCategory;

    @ViewById(R.id.hsv_activity_newhome_as_category)
    HorizontalScrollView hsvCategory;

    @ViewById(R.id.vp_activity_newhome_as_day)
    ViewPager vpDay;

    @ViewById(R.id.vp_activity_newhome_as_week)
    ViewPager vpWeek;

    @ViewById(R.id.vp_activity_newhome_as_news)
    ViewPager vpNews;

    @ViewById(R.id.dl_activity_newhome_as)
    DrawerLayout dlNewHome;

    @ViewById(R.id.dv_activity_newhome_as_drawer)
    DrawerView drawerView;

    @ViewById(R.id.img_activity_newhome_as_drawer)
    ImageView imgDrawer;

    @ViewById(R.id.iv_activity_newhome_as_startfitmeter_button)
    ImageView ivStartFitmeterButton;

    @ViewById(R.id.iv_activity_newhome_as_startfitmeter_finger)
    ImageView ivStartFitmeterFinger;

    @ViewById(R.id.tv_activity_newhome_as_startfitmeter)
    TextView tvStartFitmeter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTheme(R.style.OrangeTheme);

        ActivityManager.getInstance().addActivity(this);

        if(!Preprocess.IsTest) {
            Tracker t = ((MyApplication) this.getApplication()).getTracker(MyApplication.TrackerName.APP_TRACKER);
            t.setScreenName("HomeAs");
            t.send(new HitBuilders.AppViewBuilder().build());
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(!Preprocess.IsTest) {
            GoogleAnalytics.getInstance(this).reportActivityStart(this);
        }

        startAnimationTimer();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(!Preprocess.IsTest) {
            GoogleAnalytics.getInstance(this).reportActivityStop(this);
        }

        stopAnimationTimer();
    }

    @Override
    public void onBackPressed() {

        if( dlNewHome.isDrawerOpen( drawerView ) )
        {
            dlNewHome.closeDrawers();
            return;
        }


        final FitmeterDialog exitDialog = new FitmeterDialog(this);
        LayoutInflater inflater = this.getLayoutInflater();

        View contentView = inflater.inflate( R.layout.dialog_default_content , null );
        TextView tvTitle = (TextView) contentView.findViewById(R.id.tv_dialog_default_title);
        tvTitle.setText(this.getString(R.string.common_quit_title));
        TextView tvContent = (TextView) contentView.findViewById(R.id.tv_dialog_default_content);
        tvContent.setText(this.getString(R.string.common_quit_content));
        exitDialog.setContentView(contentView);

        View buttonView = inflater.inflate( R.layout.dialog_button_two , null );
        Button btnLeft = (Button) buttonView.findViewById(R.id.btn_dialog_button_two_left);
        btnLeft.setText(this.getString(R.string.common_no));
        btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exitDialog.close();
            }
        });
        Button btnRight = (Button) buttonView.findViewById(R.id.btn_dialog_button_two_right);
        btnRight.setText(this.getString(R.string.common_yes));
        btnRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moveTaskToBack(true);
                finish();
                exitDialog.close();
            }
        });
        exitDialog.setButtonView(buttonView);
        exitDialog.show();


    }

    @AfterViews
    void init()
    {
        newHomeUtils = new NewHomeUtils( this , mDBManager );

        ActivityReference.activityReferece = this;

        this.mDBManager = new FitmateDBManager(this);
        this.mUserInfo = this.mDBManager.getUserInfo();

        //날짜 관련 뷰
        Calendar calendar = Calendar.getInstance();
        calendar.setTime( new Date() );

        mTodayCalendar = Calendar.getInstance();
        mTodayCalendar.setTimeInMillis( calendar.getTimeInMillis() );

        mDayCalendar = Calendar.getInstance();
        mDayCalendar.setTimeInMillis( calendar.getTimeInMillis() );

        this.mWeekCalendar = Calendar.getInstance();
        this.mWeekCalendar.setTimeInMillis(calendar.getTimeInMillis());
        int weekNumber = mWeekCalendar.get(Calendar.DAY_OF_WEEK);
        mWeekCalendar.add(Calendar.DATE, -(weekNumber - 1));

        setDateText();

        //사이드 메뉴
        this.drawerView.setParentActivity(this);
        this.drawerView.setTag(this.getResources().getString(R.string.gnb_home));
        this.drawerView.setDrawerLayout(dlNewHome);

        DayActivity dayActivity = new DayActivity();
        dayActivity.setAverageMET(1.23f);
        dayActivity.setHmlStrengthTime(new int[]{40, 30, 20, 10});
        dayActivity.setCalorieByActivity(400);
        dayActivity.setAchieveOfTarget(3000);
        dayActivity.setPredayScore(15);
        dayActivity.setTodayScore(24);
        dayActivity.setFat(12.3f);
        dayActivity.setCarbohydrate(24.3f);
        dayActivity.setDistance(15.4f);

        ExerciseProgram exerciseProgram = mDBManager.getUserExerciProgram( 0 );
        addDayData( dayActivity , exerciseProgram );
        addWeekData();
        addNews();
        selectTabStyle();

        }

    private void addNews(){
        View[] newsViews = new View[]{newHomeUtils.getDefaultNewsView()};

        NewsPagerAdapter newsAdapter = new NewsPagerAdapter(this , newsViews);
        vpNews.setAdapter(newsAdapter);
    }

    private void addWeekData(){

        mWeekPagerAdapter = new NewHomeAsWeekPagerAdaper(this , NewHomeConstant.AllTabViewOrder[selectdCategoryIndex] , mDayCalendar.getTimeInMillis() , WeekPagerAdapter.WeekPagerType.WEEK_BAR , mWeekPagerActivityInfoList );
        vpWeek.setAdapter( mWeekPagerAdapter );
    }

    private void addDayData( DayActivity dayActivity ,  ExerciseProgram exerciseProgram )
    {
        View[] dayGraphs = new View[NewHomeConstant.AllTabViewOrder.length ];

        float lastViewRight = 0;
        float defaultMargin = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 26.9f, getResources().getDisplayMetrics());
        float categoryViewWidth = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 107.5f, getResources().getDisplayMetrics());

        for( int viewIndex = 0 ; viewIndex < NewHomeConstant.AllTabViewOrder.length ;viewIndex++ ) {
            CategoryType categoryType = NewHomeConstant.AllTabViewOrder[viewIndex];

            NewHomeCategoryView categoryView = NewHomeCategoryView_.build(this);
            View dayView = null;

            if (categoryType.equals(CategoryType.DAILY_ACHIEVE)) {
                dayView = NewHomeDayHalfCircleView_.build(this);
            } else if (categoryType.equals(CategoryType.WEEKLY_ACHIEVE)) {
                dayView = NewHomeDayCircleView_.build(this);
            } else {
                dayView = NewHomeDayTextView_.build(this);
            }

            categoryView.setSelectListener(new CategoryClickListener());
            categoryView.setViewIndex( viewIndex );
            categoryViews[ viewIndex ] = categoryView;
            mUserInfo.setHeight( 170 );
            mUserInfo.setWeight( 70 );
            UserInfoForAnalyzer analyzer = com.fitdotlife.fitmate_lib.service.util.Utils.getUserInfoForAnalyzer(mUserInfo);
            newHomeUtils.setDayValue(categoryView, dayView, NewHomeCategoryData.getNewHomeCategoryData(this , dayActivity , exerciseProgram ,mUserInfo), 0 , categoryType, false);

            if( selectdCategoryIndex == viewIndex ){
                categoryView.select();
            }else{
                categoryView.unSelect();
            }

            dayGraphs[viewIndex] = dayView;

            if ( viewIndex > 0)
            {
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
                params.leftMargin = (int) ( lastViewRight - defaultMargin );
                lastViewRight = lastViewRight + ( categoryViewWidth - defaultMargin );
                rlCategory.addView( categoryView , params);
            }else {
                lastViewRight = categoryViewWidth;
                rlCategory.addView(categoryView);
            }
        }

        final DayPagerAdapter dayPagerAdapter = new DayPagerAdapter(this , dayGraphs);
        vpDay.setAdapter(dayPagerAdapter);
        vpDay.setCurrentItem(selectdCategoryIndex);
        vpDay.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {


                (( NewHomeCategoryView) rlCategory.getChildAt(selectdCategoryIndex) ).unSelect();
                int preSelectedCategoryIndex = selectdCategoryIndex;
                selectdCategoryIndex = position;
                NewHomeCategoryView newHomeCategoryView = (NewHomeCategoryView) rlCategory.getChildAt(selectdCategoryIndex);
                newHomeCategoryView.select();

                if (preSelectedCategoryIndex > selectdCategoryIndex) { //카테고리를 왼쪽에서 오른쪽으로 이동하는 경우

                    int viewLeft = newHomeCategoryView.getLeft();
                    if (hsvCategory.getScrollX() > viewLeft) {
                        hsvCategory.smoothScrollTo(viewLeft, 0);
                    }

                } else if (preSelectedCategoryIndex < selectdCategoryIndex) { //카테고리를 오른족에서 왼쪽으로 이동하는 경우

                    int viewRight = newHomeCategoryView.getRight();
                    if (viewRight > (hsvCategory.getScrollX() + hsvCategory.getRight())) {

                        hsvCategory.smoothScrollTo(viewRight - hsvCategory.getRight(), 0);
                    }
                }

                CategoryType categoryType = NewHomeConstant.AllTabViewOrder[selectdCategoryIndex];

                // 주 그래프를 선택한다.
                //mWeekPagerAdapter.changeCategoryType(categoryType);
                //mWeekPagerAdapter.notifyDataSetChanged();
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

    }

    @Click(R.id.img_activity_newhome_as_drawer)
    void drawerClick(){

        dlNewHome.openDrawer(drawerView);
    }

    private void setDateText(){

        Date activityDate = mTodayCalendar.getTime();
        Date todayDate = new Date();

        String weekDay = null;
        weekDay = this.getResources().getStringArray(R.array.week_string)[ mTodayCalendar.get( Calendar.DAY_OF_WEEK ) - 1 ];

        tvWeekDay.setText(weekDay);
        tvDate.setText(DateUtils.getDateStringForPattern(mTodayCalendar.getTime(), this.getString(R.string.newhome_date_format)));
    }

    private class CategoryClickListener implements View.OnClickListener{
        @Override
        public void onClick(View view)
        {
            vpDay.setCurrentItem( (int) view.getTag() );
        }
    }

    private class NewsPagerAdapter extends PagerAdapter
    {
        private View[] mNewsList = null;
        private Context mContext = null;

        public NewsPagerAdapter( Context context , View[] newsList )
        {
            this.mContext = context;
            this.mNewsList = newsList;
        }

        @Override
        public int getCount() {
            return mNewsList.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View view = mNewsList[ position ];
            container.addView(view);
            return  view;
        }
    }

    private String getDateString( Calendar calendar )
    {
        return calendar.get(Calendar.YEAR) + "-" + String.format("%02d",  calendar.get( Calendar.MONTH ) + 1 ) + "-" + String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH) );
    }

    @Click(R.id.rl_activity_newhome_as_start_fitmeter)
    void startFitmeterClick(){
        //Intent intent = new Intent( this.getApplicationContext() , SetUserInfoActivity.class );
        Intent intent = new Intent( this.getApplicationContext() , SetUserActivity.class );
        startActivity(intent);
    }

    private class NewHomeAsWeekPagerAdaper extends WeekPagerAdapter
    {

        public NewHomeAsWeekPagerAdaper(Context context, CategoryType categoryType , long dayMilliseconds, WeekPagerType  weekPagerType , WeekPagerActivityInfo[] weekPagerActivityInfoList ) {
            super(context, 1, categoryType, null , null, dayMilliseconds , weekPagerType , weekPagerActivityInfoList );
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            View view = null;

            WeekPagerActivityInfo weekActivityInfo = null;
            if (mWeekPagerActivityInfoList[position] == null) {

                weekActivityInfo = getWeekPagerActivityInfo(position);
                mWeekPagerActivityInfoList[position] = weekActivityInfo;

            } else {

                weekActivityInfo = mWeekPagerActivityInfoList[position];
            }

            view = getWeekGraphView(weekActivityInfo, position);

            container.addView(view);

            return view;
        }

        private WeekPagerActivityInfo getWeekPagerActivityInfo(int position) {

            int weekIndex = mPagerLength - (position + 1);

            Calendar weekCalendar = Calendar.getInstance();
            weekCalendar.setTimeInMillis( mWeekCalendar.getTimeInMillis() );
            weekCalendar.add(Calendar.DATE, -(weekIndex * 7));
            String weekStartDate = newHomeUtils.getDateString(weekCalendar);

            String weekRange = "";
            double[] exerciseTimeValues = new double[]{ 0 , 0 , 0 , 0 , 0 , 0 , 0 };
            double[] calorieValues = new double[]{ 0 , 0 , 0 , 0 , 0 , 0 , 0 };
            double[] dailyAchieveValues = new double[]{ 0 , 0 , 0 , 0 , 0 , 0 , 0 };
            double[] weeklyAchieveValues = new double[]{ 0 , 0 , 0 , 0 , 0 , 0 , 0 };
            double[] fatValues = new double[]{ 0 , 0 , 0 , 0 , 0 , 0 , 0 };
            double[] carboHydrateValues = new double[]{ 0 , 0 , 0 , 0 , 0 , 0 , 0 };
            double[] distanceValues = new double[]{ 0 , 0 , 0 , 0 , 0 , 0 , 0 };
            String[] dayStrings = new String[7];
            long[] timeMillis = new long[7];
            int selectedDayIndex = -1;
            int preMonth = -1;

            for (int i = 0; i < dailyAchieveValues.length; i++)
            {

                String activityDate = newHomeUtils.getDateString(weekCalendar);
                DayActivity dayActivity = mDBManager.getDayActivity(activityDate);

                timeMillis[i] = weekCalendar.getTimeInMillis();

                int currentMonth = weekCalendar.get(Calendar.MONTH);
                if( preMonth != currentMonth ) {
                dayStrings[i] = DateUtils.getDateStringForPattern(weekCalendar.getTime(), getResources().getString(R.string.newhome_week_bar_date_format));
                    preMonth = currentMonth;
                }else{
                    dayStrings[i] = DateUtils.getDateStringForPattern(weekCalendar.getTime(), getResources().getString(R.string.newhome_week_bar_date_simple_format));
                }

                //선택된 날짜인지 확인한다.
                boolean isSameDay = DateUtils.isSameDay( mDayCalendar, weekCalendar );
                if (isSameDay) {
                    selectedDayIndex = i;
                    mSelectedDayIndex = i;
                }

                if( i == 0 ){
                    weekRange += DateUtils.getDateStringForPattern( weekCalendar.getTime() , getResources().getString( R.string.newhome_week_table_week_range_format ) );
                }

                if( i == dailyAchieveValues.length -1 ){
                    weekRange += " - " + DateUtils.getDateStringForPattern( weekCalendar.getTime() , getResources().getString( R.string.newhome_week_table_week_range_format ) );
                }

                weekCalendar.add(Calendar.DATE, 1);
            }

                    float strengthFrom = 0;
                    float strengthTo = 0;

            WeekPagerActivityInfo weekActivityInfo = new WeekPagerActivityInfo( weekStartDate , weekRange , dayStrings , timeMillis , preMonth , strengthFrom , strengthTo
                    ,exerciseTimeValues , calorieValues , dailyAchieveValues , weeklyAchieveValues , fatValues , carboHydrateValues , distanceValues);
            return weekActivityInfo;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        protected void getWeekActivityInfo(int position) { }

        @Override
        public void OnWeekActivityReceived(FriendWeekActivity friendWeekActivity) {

    }

        @Override
        public void OnWeekActivityReceived(FriendWeekActivity[] friendWeekActivityList) {

    }
}

    private void selectTabStyle()
    {
        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get( Calendar.HOUR_OF_DAY );

        if(  hour >= 20   ){
            MyApplication.mSelectedTabStyleIndex = 2;
            changeTabStyle( MyApplication.tabStyleList[MyApplication.mSelectedTabStyleIndex] );
        }else if( hour >= 18  ){
            MyApplication.mSelectedTabStyleIndex = 1;
            changeTabStyle( MyApplication.tabStyleList[MyApplication.mSelectedTabStyleIndex] );
        }else if( hour >= 6 ){
            MyApplication.mSelectedTabStyleIndex = 0;
            changeTabStyle( MyApplication.tabStyleList[MyApplication.mSelectedTabStyleIndex] );
        }else{
            MyApplication.mSelectedTabStyleIndex = 2;
            changeTabStyle( MyApplication.tabStyleList[MyApplication.mSelectedTabStyleIndex] );
        }

    }

    @SuppressWarnings("ResourceType") //이거 안하면 sign application 만들때 에러 발생함.
    private void changeTabStyle( int themeResource )
    {
//        int[] attrs = new int[]{ R.attr.newhomeBatteryCover , R.attr.newhomeCategoryBackground , R.attr.newhomeCategoryNormal, R.attr.newhomeCategorySelect ,
//        R.attr.newhomeCategoryTextNormal , R.attr.newhomeCategoryTextSelect, R.attr.newhomeCategoryValueTextSelect , R.attr.newhomeCategoryValueTextNormal ,
//        R.attr.newhomeBatteryEmpty , R.attr.newhomeBatteryFill , R.attr.newhomeBatteryText , R.attr.newhomeSync , R.attr.newhomeDrawer, R.attr.newhomeDateText , R.attr.newhomeDateArrow };

        //카테고리
        int[] categoryAttrs = new int[]{ R.attr.newhomeCategoryBackground ,  R.attr.newhomeCategoryEdit ,R.attr.newhomeCategoryNormal, R.attr.newhomeCategorySelect , R.attr.newhomeCategorySwipe ,
                R.attr.newhomeCategoryTextNormal , R.attr.newhomeCategoryTextSelect , R.attr.newhomeCategoryValueTextNormal , R.attr.newhomeCategoryValueTextSelect, R.attr.newhomeDateArrow , R.attr.newhomeDateText  };

        TypedArray categoryTypedArray = this.obtainStyledAttributes(themeResource, categoryAttrs);
        Drawable newhomeTabBackground = categoryTypedArray.getDrawable(0);
        Drawable newhomeCategoryEdit = categoryTypedArray.getDrawable(1);
        Drawable newhomeCategoryNormal = categoryTypedArray.getDrawable(2);
        Drawable newhomeCategorySelect = categoryTypedArray.getDrawable(3);
        Drawable newhomeCategorySwipe = categoryTypedArray.getDrawable(4);

        int newhomeCategoryTextNormal = categoryTypedArray.getColor(5, 0);
        int newhomeCategoryTextSelect = categoryTypedArray.getColor(6, 0);
        int newhomeCategoryValueTextNormal = categoryTypedArray.getColor(7, 0);
        int newhomeCategoryValueTextSelect = categoryTypedArray.getColor(8, 0);
        Drawable newhomeDateArrow = categoryTypedArray.getDrawable(9);
        int newhomeDateText = categoryTypedArray.getColor(10, 0);

        categoryTypedArray.recycle();

        int[] batteryAttrs = new int[]{ R.attr.newhomeBatteryCover , R.attr.newhomeBatteryEmpty , R.attr.newhomeBatteryFill , R.attr.newhomeBatteryText , R.attr.newhomeDrawer , R.attr.newhomeSync  };
        TypedArray batteryTypedArray = this.obtainStyledAttributes(themeResource, batteryAttrs);

        Drawable newhomeBatteryCover = batteryTypedArray.getDrawable(0);
        int newhomeBatteryEmpty = batteryTypedArray.getColor(1, 0);
        int newhomeBatteryFill = batteryTypedArray.getColor(2, 0);
        int newhomeBatteryText = batteryTypedArray.getColor(3, 0);
        Drawable newhomeDrawer = batteryTypedArray.getDrawable(4);
        Drawable newhomeSync = batteryTypedArray.getDrawable(5);


        batteryTypedArray.recycle();

        llActionBar.setBackground(newhomeTabBackground);
        ivCategorySwipe.setBackground( newhomeCategorySwipe );

        for (NewHomeCategoryView categoryView : categoryViews )
        {
            categoryView.changeNewHomeCategoryStyle( newhomeCategorySelect , newhomeCategoryNormal , newhomeCategoryTextSelect , newhomeCategoryTextNormal , newhomeCategoryValueTextSelect , newhomeCategoryValueTextNormal );
        }

        imgDrawer.setImageDrawable(newhomeDrawer);

        tvDate.setTextColor( newhomeDateText );
        tvWeekDay.setTextColor(newhomeDateText);

    }

    private void startAnimationTimer(){
        animationTimer = new Timer();
        animationTimer.schedule(new AnimationTimerTask() , 500 , 500);
    }

    private void stopAnimationTimer(){
        if( animationTimer != null ){
            animationTimer.cancel();
            animationTimer = null;
        }
    }

    private class AnimationTimerTask extends TimerTask
    {
        private int mIndex = 0;

        @Override
        public void run() {

            runOnUiThread(new Runnable()
            {
                @Override
                public void run()
                {
                    if( mIndex == 0 ){

                        RelativeLayout.LayoutParams onTextParams = (RelativeLayout.LayoutParams) tvStartFitmeter.getLayoutParams();
                        onTextParams.setMargins( 0 , (int) (TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 17.1f, getResources().getDisplayMetrics())), 0, 0  );

                        ivStartFitmeterButton.setImageResource(R.drawable.btn_start_on);
                        RelativeLayout.LayoutParams onButtonParams = (RelativeLayout.LayoutParams) ivStartFitmeterButton.getLayoutParams();
                        onButtonParams.setMargins( 0 , (int) (TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5.6f, getResources().getDisplayMetrics())), 0, 0  );

                        ivStartFitmeterFinger.setImageResource(R.drawable.finger_on);
                        RelativeLayout.LayoutParams onFingerParams = (RelativeLayout.LayoutParams) ivStartFitmeterFinger.getLayoutParams();
                        onFingerParams.setMargins(0, (int) (TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 21.7f, getResources().getDisplayMetrics())), 0, 0);

                        mIndex = 1;

                    }else{

                        RelativeLayout.LayoutParams offTextParams = (RelativeLayout.LayoutParams) tvStartFitmeter.getLayoutParams();
                        offTextParams.setMargins( 0 , (int) (TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 11.5f, getResources().getDisplayMetrics())) , 0, 0  );

                        ivStartFitmeterButton.setImageResource(R.drawable.btn_start_off);
                        RelativeLayout.LayoutParams offButtonParams = (RelativeLayout.LayoutParams) ivStartFitmeterButton.getLayoutParams();
                        offButtonParams.setMargins(0, 0 , 0, 0);

                        ivStartFitmeterFinger.setImageResource(R.drawable.finger_off);
                        RelativeLayout.LayoutParams offFingerParams = (RelativeLayout.LayoutParams) ivStartFitmeterFinger.getLayoutParams();
                        offFingerParams.setMargins(0, (int) (TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 43.3f, getResources().getDisplayMetrics())), 0, 0);

                        mIndex = 0;
                    }

                }
            });
        }
    }

}
