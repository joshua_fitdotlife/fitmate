package com.fitdotlife.fitmate.alarm;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.StrengthInputType;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.UserInfoForAnalyzer;
import com.fitdotlife.fitmate.LoginActivity;
import com.fitdotlife.fitmate.NewHomeActivity;
import com.fitdotlife.fitmate.NewHomeActivity_;
import com.fitdotlife.fitmate.R;
import com.fitdotlife.fitmate.SplashActivity;
import com.fitdotlife.fitmate.SplashActivity_;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.object.DayActivity;
import com.fitdotlife.fitmate_lib.object.WeekActivity;
import com.fitdotlife.fitmate_lib.service.ActivityDataCalculator;
import com.fitdotlife.fitmate_lib.service.util.Utils;
import com.fitdotlife.fitmate_lib.util.DateUtils;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Joshua on 2016-08-12.
 */
public class ActivityNotiAlarmReceiver extends BroadcastReceiver{
    @Override
    public void onReceive(Context context, Intent intent) {

        FitmateDBManager mDBManager = new FitmateDBManager(context);

        Date todayDate = new Date();
        String strTodayDate = DateUtils.getDateString_yyyy_MM_dd(todayDate);
        DayActivity todayActivity = mDBManager.getDayActivity( strTodayDate );

        //오늘 날짜 데이터가 없으면
        if( todayActivity == null )
        {

            String achieveNoMessage = String.format(context.getString(R.string.noti_day_achieve_below_50), 0);
            showNotification(context, "Fitmeter", achieveNoMessage, achieveNoMessage, 519);
            setDayAchieve(context, strTodayDate, ActivityDataCalculator.DayAchieveType.DAY_ACHIEVE_BELOW_50.ordinal());

            setAlarm( context );
            return;
        }

        //주간 목표 확인
        Calendar weekCalendar = Calendar.getInstance();
        int dayOfWeek = weekCalendar.get(Calendar.DAY_OF_WEEK);
        weekCalendar.add(Calendar.DATE, -(dayOfWeek - 1));
        String weekStartDate = DateUtils.getDateString( weekCalendar );
        WeekActivity weekActivity = mDBManager.getWeekActivity(weekStartDate);

        if( weekActivity.getScore() >= 100 )
        {
            //주간 목표를 달성했으면 오늘 운동 목표에 대한 푸쉬를 보내지 않는다.
            return;
        }


        float goalValue = 0;
        //운동 프로그램을 가져온다.
        com.fitdotlife.fitmate_lib.object.ExerciseProgram program = mDBManager.getAppliedProgram();
        //운동프로그램 종류를 확인한다.
        StrengthInputType strengthInputType = StrengthInputType.values()[ program.getCategory() ];

        if( strengthInputType== StrengthInputType.CALORIE|| strengthInputType == StrengthInputType.CALORIE_SUM || strengthInputType == StrengthInputType.VS_BMR)
        {
            if( strengthInputType.equals( StrengthInputType.CALORIE) ){
                goalValue = program.getStrengthFrom();
            }else if(strengthInputType.equals( StrengthInputType.CALORIE_SUM )){
                goalValue = program.getStrengthFrom() / 7;
            }else{
                UserInfoForAnalyzer userInfoForAnalyzer = Utils.getUserInfoForAnalyzer(mDBManager.getUserInfo());
                goalValue = (int)Math.round( program.getStrengthFrom() / 100.0 * userInfoForAnalyzer.getBMR() );
            }

        }else{
            goalValue = program.getMinuteFrom();
        }

        int achieveOfTarget = todayActivity.getAchieveOfTarget();
        int achievePercent = (int) (( achieveOfTarget / goalValue ) * 100);
        ActivityDataCalculator.DayAchieveType dayAchieveType = ActivityDataCalculator.DayAchieveType.values()[ getDayAchieve(context, strTodayDate) ];

        if( achievePercent < 50 )
        {
            String achieveMessage = String.format( context.getString( R.string.noti_day_achieve_below_50 ) , achievePercent );
            showNotification(context , "Fitmeter", achieveMessage, achieveMessage , 519);
            setDayAchieve(context , strTodayDate , ActivityDataCalculator.DayAchieveType.DAY_ACHIEVE_BELOW_50.ordinal() );
        }

        setAlarm( context );
    }

    private void setAlarm( Context context )
    {
        Calendar alarmCalendar = Calendar.getInstance();
        alarmCalendar.add( Calendar.DATE , 1 );
        alarmCalendar.set(Calendar.HOUR_OF_DAY, 18);
        alarmCalendar.set( Calendar.MINUTE , 0 );
        long alarmTimeMillisecond = alarmCalendar.getTimeInMillis();

        Calendar currentTimeCalendar = Calendar.getInstance();
        long currentTimeMilliSecond = currentTimeCalendar.getTimeInMillis();

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent repeatIntent = new Intent( context , ActivityNotiAlarmReceiver.class );

        PendingIntent alarmIntent = PendingIntent.getBroadcast( context , 1 , repeatIntent , 0);
        alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() +  ( alarmTimeMillisecond - currentTimeMilliSecond )  , alarmIntent);
    }


    void showNotification(Context context , String title, String content, String noti , int id)
    {
        NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, new Intent(context, SplashActivity_.class), PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mCompatBuilder = new NotificationCompat.Builder(context);
        mCompatBuilder.setSmallIcon(R.drawable.fitlife_ico);
        mCompatBuilder.setTicker(noti);
        mCompatBuilder.setWhen(System.currentTimeMillis());
        mCompatBuilder.setContentTitle(title);
        mCompatBuilder.setContentText( content );
        mCompatBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(content));
        //mCompatBuilder.setContentText(content);
        mCompatBuilder.setVisibility(Notification.VISIBILITY_PUBLIC);
        mCompatBuilder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);
        mCompatBuilder.setContentIntent(pendingIntent);
        mCompatBuilder.setAutoCancel(true);

        nm.notify( id , mCompatBuilder.build());
    }

    private void setDayAchieve( Context context , String dayDate , int dayAchieve   )
    {
        SharedPreferences pref = context.getSharedPreferences("fitmate", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(ActivityDataCalculator.DAY_ACHIEVE_KEY + "_" + dayDate, dayAchieve );
        editor.commit();
    }

    private int getDayAchieve( Context context , String dayDate  )
    {
        SharedPreferences pref = context.getSharedPreferences("fitmate", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        return pref.getInt(ActivityDataCalculator.DAY_ACHIEVE_KEY + "_" + dayDate, 0 );
    }
}
