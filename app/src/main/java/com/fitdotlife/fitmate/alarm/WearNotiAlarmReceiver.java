package com.fitdotlife.fitmate.alarm;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;

import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseAnalyzer;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseProgram;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.StrengthInputType;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.UserInfoForAnalyzer;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.WearingLocation;
import com.fitdotlife.fitmate.LoginActivity;
import com.fitdotlife.fitmate.NewHomeActivity;
import com.fitdotlife.fitmate.NewHomeActivity_;
import com.fitdotlife.fitmate.R;
import com.fitdotlife.fitmate.SplashActivity;
import com.fitdotlife.fitmate.SplashActivity_;
import org.apache.log4j.Log;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.object.UserNotiSetting;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by Joshua on 2016-08-12.
 */
public class WearNotiAlarmReceiver extends BroadcastReceiver
{

    private final String TAG = WearNotiAlarmReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent )
    {

        FitmateDBManager mDBManager = new FitmateDBManager(context);
        UserNotiSetting userNotiSetting = mDBManager.getUserNotiSetting();

        if( !userNotiSetting.isFitmeterwearnoti() ) {return;}


        Log.e("WearNotiAlarmReceiver" , "WearNotiAlarmReceiver 발생");


        ExerciseProgram appliedExerciseProgram = mDBManager.getAppliedExerciseProgram();
        UserInfo userInfo = mDBManager.getUserInfo();

        UserInfoForAnalyzer userInfoForAnalyzer = com.fitdotlife.fitmate_lib.service.util.Utils.getUserInfoForAnalyzer(userInfo);

        //Analyzer를 생성한다.
        ExerciseAnalyzer exerciseAnalyzer = new ExerciseAnalyzer(appliedExerciseProgram, userInfoForAnalyzer, WearingLocation.WAIST , 10);
        StrengthInputType strengthType = appliedExerciseProgram.getStrengthInputType();

        String dayStartMessae = null;
        if (strengthType.equals(StrengthInputType.CALORIE)) {
            dayStartMessae = String.format( context.getString(R.string.alarm_today_start_calorie), appliedExerciseProgram.getStrength_From());
        } else if (strengthType.equals(StrengthInputType.CALORIE_SUM)) {
            dayStartMessae = String.format(context.getString(R.string.alarm_today_start_calorie), (int) appliedExerciseProgram.getStrength_From() / 7);
        } else if (strengthType.equals(StrengthInputType.VS_BMR)) {
            int bmr = exerciseAnalyzer.getUserInfo().getBMR();
            dayStartMessae = String.format(context.getString(R.string.alarm_today_start_calorie), (int) (bmr * (appliedExerciseProgram.getStrength_From() / 100)));
        } else {
            dayStartMessae = String.format(context.getString(R.string.alarm_today_start_time), appliedExerciseProgram.getMinutes_From());
        }

        showNotification(context, "Fitmeter", dayStartMessae, dayStartMessae, 517);

        Calendar todayCalendar = Calendar.getInstance();
        int weekDay = todayCalendar.get(Calendar.DAY_OF_WEEK);
        if (weekDay == Calendar.SUNDAY)
        {
            String weekStartMessae = null;
            if (strengthType.equals(StrengthInputType.CALORIE)) {
                weekStartMessae = String.format(context.getString(R.string.alarm_week_start_calorie), appliedExerciseProgram.getStrength_From() * appliedExerciseProgram.getTimes_From());
            } else if (strengthType.equals(StrengthInputType.CALORIE_SUM)) {
                weekStartMessae = String.format(context.getString(R.string.alarm_week_start_calorie), appliedExerciseProgram.getStrength_From());
            } else if (strengthType.equals(StrengthInputType.VS_BMR)) {
                int bmr = exerciseAnalyzer.getUserInfo().getBMR();

                weekStartMessae = String.format(context.getString(R.string.alarm_week_start_calorie), (int) (bmr * (appliedExerciseProgram.getStrength_From() / 100) * 7));
            } else {
                weekStartMessae = String.format(context.getString(R.string.alarm_week_start_time), appliedExerciseProgram.getTimes_From(), appliedExerciseProgram.getTimes_From() * appliedExerciseProgram.getMinutes_From());
            }

            showNotification( context , "Fitmeter", weekStartMessae, weekStartMessae , 518);
        }


        String alarmTime = userNotiSetting.getAlarmtime();

        Calendar alarmCalendar = Calendar.getInstance();
        Calendar tempCalendar = Calendar.getInstance();

        SimpleDateFormat sdf = new SimpleDateFormat( "HH:mm"  , Locale.getDefault());
        try {
            tempCalendar.setTime(sdf.parse(alarmTime));
        } catch (ParseException e) {
            e.printStackTrace();
            alarmCalendar = null;
        }

        alarmCalendar.set(Calendar.HOUR_OF_DAY, tempCalendar.get(Calendar.HOUR_OF_DAY));
        alarmCalendar.set(Calendar.MINUTE, tempCalendar.get(Calendar.MINUTE));
        alarmCalendar.add( Calendar.DATE , 1 );
        long alarmTimeMillisecond = alarmCalendar.getTimeInMillis();

        Calendar currentTimeCalendar = Calendar.getInstance();
        long currentTimeMilliSecond = currentTimeCalendar.getTimeInMillis();

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent repeatIntent = new Intent( context , WearNotiAlarmReceiver.class );
        PendingIntent alarmIntent = PendingIntent.getBroadcast( context , 1 , repeatIntent , 0);
        alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + ( alarmTimeMillisecond - currentTimeMilliSecond ) , alarmIntent );
    }

    void showNotification( Context context ,  String title, String content, String noti , int id)
    {
        NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, new Intent(context, SplashActivity_.class), PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mCompatBuilder = new NotificationCompat.Builder(context);
        mCompatBuilder.setSmallIcon(R.drawable.fitlife_ico);
        mCompatBuilder.setTicker(noti);
        mCompatBuilder.setWhen(System.currentTimeMillis());
        mCompatBuilder.setContentTitle(title);
        mCompatBuilder.setContentText( content );
        mCompatBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(content));
        //mCompatBuilder.setContentText(content);
        mCompatBuilder.setVisibility(Notification.VISIBILITY_PUBLIC);
        mCompatBuilder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);
        mCompatBuilder.setContentIntent(pendingIntent);
        mCompatBuilder.setAutoCancel(true);

        nm.notify( id , mCompatBuilder.build());
    }

}
