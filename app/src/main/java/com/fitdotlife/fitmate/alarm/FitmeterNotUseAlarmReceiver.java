package com.fitdotlife.fitmate.alarm;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.NotificationCompat;

import com.fitdotlife.fitmate.LoginActivity;
import com.fitdotlife.fitmate.NewHomeActivity;
import com.fitdotlife.fitmate.NewHomeActivity_;
import com.fitdotlife.fitmate.R;
import com.fitdotlife.fitmate.SplashActivity_;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.object.UserNotiSetting;

/**
 * Created by Joshua on 2016-08-12.
 */
public class FitmeterNotUseAlarmReceiver extends BroadcastReceiver{
    @Override
    public void onReceive(Context context, Intent intent)
    {
        FitmateDBManager dbManager = new FitmateDBManager( context );
        UserNotiSetting userNotiSetting = dbManager.getUserNotiSetting();

        if( !userNotiSetting.isFitmeternotusednoti() ){return;}


        showNotification( context , "Fitmate", context.getString(R.string.myprofile_not_connect_eight_hour), context.getString(R.string.myprofile_connecting_device) , 521);

        SharedPreferences pref = context.getSharedPreferences( context.getString(R.string.app_name) ,  Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean("FitmeterNotUseNoti", true );
        editor.commit();

    }

    void showNotification(Context context , String title, String content, String noti , int id)
    {
        NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, new Intent(context, SplashActivity_.class), PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mCompatBuilder = new NotificationCompat.Builder(context);
        mCompatBuilder.setSmallIcon(R.drawable.fitlife_ico);
        mCompatBuilder.setTicker(noti);
        mCompatBuilder.setWhen(System.currentTimeMillis());
        mCompatBuilder.setContentTitle(title);
        mCompatBuilder.setContentText( content );
        mCompatBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(content));
        //mCompatBuilder.setContentText(content);
        mCompatBuilder.setVisibility(Notification.VISIBILITY_PUBLIC);
        mCompatBuilder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);
        mCompatBuilder.setContentIntent(pendingIntent);
        mCompatBuilder.setAutoCancel(true);

        nm.notify( id , mCompatBuilder.build());
    }
}
