package com.fitdotlife.fitmate;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fitdotlife.fitmate_lib.key.FitmeterType;

/**
 * Created by Joshua on 2016-08-03.
 */
public class FitmeterContentFragment extends Fragment
{
    private ImageView imgContent = null;
    private FitmeterType mDeviceType = null;

    private TextView tvButton = null;
    private TextView tvLED = null;
    private TextView tvDescription = null;

    public static FitmeterContentFragment newInstance( FitmeterType deviceType) {

        Bundle args = new Bundle();

        FitmeterContentFragment fragment = new FitmeterContentFragment();
        args.putInt( SelectDeviceFragment.DEVICETYPE_KEY , deviceType.ordinal() );
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getArguments() != null){
            mDeviceType = FitmeterType.values()[ getArguments().getInt( SelectDeviceFragment.DEVICETYPE_KEY ) ];
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate( R.layout.fragment_fitmeter_content , container , false );
        this.imgContent = (ImageView) rootView.findViewById(R.id.img_fragment_fitmeter_content);
        this.tvButton = (TextView) rootView.findViewById(R.id.tv_fragment_fitmeter_content_button);
        this.tvLED = (TextView) rootView.findViewById(R.id.tv_fragment_fitmeter_content_led);
        this.tvDescription = (TextView) rootView.findViewById(R.id.tv_fragment_fitmeter_content_description);

        RelativeLayout.LayoutParams ledParams = (RelativeLayout.LayoutParams) tvLED.getLayoutParams();
        //FrameLayout.LayoutParams ledParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT , ViewGroup.LayoutParams.WRAP_CONTENT);
        //FrameLayout.LayoutParams buttonParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT , ViewGroup.LayoutParams.WRAP_CONTENT);

        int imgResourceID = 0;
        int ledGravity = 0;
        int descriptionID = 0;

        if( mDeviceType.equals( FitmeterType.BAND ) )
        {
            //int ledTopMargin = (int) TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP , 227 , getResources().getDisplayMetrics() );
            //int ledLeftMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP , 54 , getResources().getDisplayMetrics() );
            //ledParams.setMargins(ledLeftMargin , ledTopMargin , 0, 0);
            //int buttonTopMargin = (int) TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP , 248 , getResources().getDisplayMetrics() );
            //int buttonLeftMargin = (int) TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP , 298 , getResources().getDisplayMetrics() );
            //buttonParams.setMargins( buttonLeftMargin , buttonTopMargin , 0 , 0 );
            //ledGravity = Gravity.CENTER_VERTICAL;

            descriptionID = R.string.signin_fitmeter_content_description_bands;
            ledParams.addRule( RelativeLayout.CENTER_VERTICAL , RelativeLayout.TRUE);
            imgResourceID = R.drawable.band_1;
        }else if( mDeviceType.equals( FitmeterType.BLE ) )
        {
            //int ledTopMargin = (int) TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP , 150 , getResources().getDisplayMetrics() );
            //int ledLeftMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP , 42 , getResources().getDisplayMetrics() );
            //ledParams.setMargins(ledLeftMargin , ledTopMargin , 0, 0);
            //int buttonTopMargin = (int) TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP , 220 , getResources().getDisplayMetrics() );
            //int buttonLeftMargin = (int) TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP , 275 , getResources().getDisplayMetrics() );
            //buttonParams.setMargins( buttonLeftMargin , buttonTopMargin , 0 , 0 );
            //ledGravity = Gravity.TOP;

            descriptionID = R.string.signin_fitmeter_content_description_life45;
            ledParams.addRule( RelativeLayout.ALIGN_PARENT_TOP , RelativeLayout.TRUE );
            imgResourceID = R.drawable.ble_1;
        }

        //tvLED.setGravity( ledGravity );
        tvLED.setLayoutParams( ledParams );
        imgContent.setImageResource(imgResourceID);
        tvDescription.setText( this.getResources().getString( descriptionID ) );

        return rootView;
    }
}
