package com.fitdotlife.fitmate;

import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.WearingLocation;
import com.fitdotlife.fitmate.btmanager.BTManager;
import com.fitdotlife.fitmate.btmanager.ConnectionStatus;
import com.fitdotlife.fitmate.btmanager.FitmeterEventCallback;
import com.fitdotlife.fitmate.btmanager.ReceivedDataInfo;
import org.apache.log4j.Log;
import com.fitdotlife.fitmate_lib.customview.FitmeterDialog;
import com.fitdotlife.fitmate_lib.customview.FitmeterProgressDialog;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.database.FitmateServiceDBManager;
import com.fitdotlife.fitmate_lib.iview.ISetUserInfoView;
import com.fitdotlife.fitmate_lib.key.FitmeterType;
import com.fitdotlife.fitmate_lib.key.GenderType;
import com.fitdotlife.fitmate_lib.object.BLEDevice;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.presenter.SetUserInfoPresenter;

import java.util.List;

/**
 * Created by Joshua on 2016-07-27.
 */
public class SetUserActivity extends FragmentActivity implements ISetUserInfoView , WearingLocationFragment.WearingLocationListener
{
    private String TAG = SetUserActivity.class.getSimpleName();

    private FitmeterType mDeviceType = null;

    private FitmateDBManager mDBManager = null;
    private UserInfo mUserInfo = null;

    private UserInfoFragment mUserInfoFragment = null;
    private DeviceRegisterFragment mDeviceRegisterFragment = null;
    private WearingLocationFragment mWearingLocationFragment = null;
    private int mStep = 0;

    private String mDeviceAddress = null;
    private FitmeterProgressDialog mProgressDialog = null;

    private int mRetryCount = 0;
    private BTManager btManager = null;
    private SetUserInfoPresenter mPresenter = null;

    private boolean isDeviceChanged = false;
    private FitmeterType mFitmeterType = null;

    private DeviceRegisterFragment.DeviceRegisterListener deviceRegisterListener = new DeviceRegisterFragment.DeviceRegisterListener() {
        @Override
        public void onSelectFitmeterType(FitmeterType fitmeterType) {
            mFitmeterType = fitmeterType;
        }

        @Override
        public void onDeviceFound(String deviceAddress , String serialNumber )
        {
            if(serialNumber != null)
            {
                mUserInfo.setDeviceAddress( serialNumber );
            }else{
                mUserInfo.setDeviceAddress( deviceAddress );
            }

            mDeviceAddress = deviceAddress;

            if(mFitmeterType.equals( FitmeterType.BAND )){

                mUserInfo.setWearAt( WearingLocation.WRIST.Value  );
                setUserInfo();

            }else if(mFitmeterType.equals( FitmeterType.BLE )){
                moveNext();
            }
        }

        @Override
        public void onMoveBackDeviceRegister() {
            moveBack();
        }
    };

    private FitmeterEventCallback fitmeterEventCallback = new FitmeterEventCallback()
    {
        @Override
        public void onProgressValueChnaged(int progress, int max) {}

        @Override
        public void onReceiveAllFileComplete(List<ReceivedDataInfo> receiveDataArray, boolean isComplete) {}

        @Override
        public void receiveCompleted(int index, int offset, byte[] receivedbytes) {}

        @Override
        public void statusOfBTManagerChanged(ConnectionStatus status)
            {
            switch(status){
                case CONNECTED:

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            changeProgressDialogMessage( getString( R.string.signin_dialog_content ) );
                        }
                    });

                    new Thread(new Runnable()
                    {
                        @Override
                        public void run() {

                            boolean result = resetFitmeter();

                            if(!result){
                                btManager.close();

                                try {
                                    Thread.sleep(1000);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }

                                connectFitmeter();
                            }else{
                                isDeviceChanged = false;
                                btManager.clear();

                                org.apache.log4j.Log.e(TAG, "기기 등록을 성공하였습니다.");

                                FitmateServiceDBManager serviceDBManager = new FitmateServiceDBManager(SetUserActivity.this);
                                serviceDBManager.deleteAllFiles();

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mPresenter.setUserInfo(mUserInfo);
                                    }
                                });
                            }

                        }
                    }).start();
                    break;
                case DISCONNECTED:
                    if(isDeviceChanged)
                    {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                connectFitmeter();
                            }
                        });
                    }

                    break;
                case ERROR_DISCONNECTED:
                    if(isDeviceChanged)
                    {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                connectFitmeter();
                            }
                        });
                    }
                    break;
                case ERROR_DISCONNECTED_133:
                    if(isDeviceChanged)
                    {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                connectFitmeter();
                            }
                        });
                    }
                    break;
            }
        }

        @Override
        public void findNewDeviceResult(List<BLEDevice> discoveredPeripherals) {
        }

        @Override
        public void findDeviceResult(String deviceAddress, String serialNumber) {
        }

        @Override
        public void findRecoveryDeviceResult(BluetoothDevice recoveryDevice, List<BluetoothDevice> recoveryFitmeters) {
        }

    };

    @Override
    public void onWearingLocationSelected(WearingLocation wearingLocation)
    {
        mUserInfo.setWearAt( wearingLocation.Value );
        setUserInfo();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_user);

        this.mDBManager = new FitmateDBManager(this);
        this.mUserInfo = mDBManager.getUserInfo();

        //this.mUserInfoFragment = UserInfoFragment.newInstance( mUserInfo.getHeight(), mUserInfo.getWeight(), true );
        this.mUserInfoFragment = UserInfoFragment.newInstance( );
        this.mDeviceRegisterFragment = DeviceRegisterFragment.newInstance();
        this.mDeviceRegisterFragment.setDeviceRegisterListener(deviceRegisterListener);

        this.mPresenter = new SetUserInfoPresenter(this , this);

        moveNext();
    }

    public void moveBack()
    {
        mStep -= 1;
        drawChildView();
    }

    public void moveNext()
    {
        mStep += 1;
        drawChildView();
    }

    private void drawChildView()
    {
        if(mStep == 1)
        {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace( R.id.fl_activity_setuser , mUserInfoFragment );
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }

        if( mStep == 2 )
        {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fl_activity_setuser, mDeviceRegisterFragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }

        if( mStep == 3 )
        {
            this.mWearingLocationFragment = WearingLocationFragment.newInstance( null );
            this.mWearingLocationFragment.setWearingLocationSelectionListener(this);

            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace( R.id.fl_activity_setuser , mWearingLocationFragment );
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }
    }

    public void saveUserInfo(GenderType gender, long birthDate, float weight, float height, boolean isSi)
    {
        mUserInfo.setGender(gender);
        mUserInfo.setBirthDate(birthDate);
        mUserInfo.setWeight(weight);
        mUserInfo.setHeight(height);
        mUserInfo.setIsSI(isSi);

        moveNext();
    }

    public void setDeviceAddress( String deviceAddress )
    {

    }

    public void setUserInfo()
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                startProgressDialog(getString(R.string.signin_dialog_title), getString(R.string.myprofile_change_location_connection_content) );
            }
        });

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                isDeviceChanged = true;
                btManager = BTManager.getInstance( SetUserActivity.this  , fitmeterEventCallback);
                //기기와 연결한다.
                connectFitmeter();

            }
        });

    }

    private boolean resetFitmeter()
    {
        //현재시간 설정
        Log.d(TAG , "현재 시간을 설정합니다.");
        if(!btManager.setCurrentTime()){
            Log.e(TAG , "시간 설정 실패");
        }

        //LED 켜기
        btManager.turnonled( BTManager.LED_COLOR_BLUE );

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //LED 끄기
        btManager.turnoffled();

        //착용위치 설정
        Log.d(TAG , "착용위치를 설정합니다.");
        if( !btManager.setWeringLocation( mUserInfo.getWearAt() )){
            Log.d(TAG , "착용위치 설정 실패");
            return false;
        }

        //이전 데이터를 삭제한다.
        Log.d(TAG , "이전 데이터를 삭제합니다.");
        if( !btManager.deleteAllFile() ){
            Log.e(TAG , "이전 데이터 삭제 실패");
        }

        return true;
    }

    private void connectFitmeter()
    {
        if(mRetryCount < 10 )
        {
            mRetryCount++;
            Log.d(TAG , "기기 등록 시도 중 연결 " + mRetryCount + "번째");
            btManager.connectFitmeter( mDeviceAddress );
        }else{
            btManager.clear();
            Log.e(TAG , "기기 등록을 실패하였습니다.");
            stopProgressDialog();
            mDeviceRegisterFragment.moveBack();
        }
    }

    @Override
    public void moveHomeActivity()
    {

        final FitmeterDialog fDialog = new FitmeterDialog(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View contentView = inflater.inflate(R.layout.dialog_default_content, null);
        TextView tvTitle = (TextView) contentView.findViewById(R.id.tv_dialog_default_title);
        tvTitle.setVisibility( View.GONE );
        TextView tvContent = (TextView) contentView.findViewById(R.id.tv_dialog_default_content);
        tvContent.setText(this.getString( R.string.signin_setuserinfo_success));
        fDialog.setContentView(contentView);

        View buttonView = inflater.inflate(R.layout.dialog_button_one , null);
        Button btnOne = (Button) buttonView.findViewById(R.id.btn_dialog_button_one);
        btnOne.setText(this.getString(R.string.signin_setuserinfo_success_start));
        btnOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fDialog.close();

                Intent intent = new Intent(SetUserActivity.this, NewHomeActivity_.class);
                startActivity(intent);
                finish();
            }
        });

        fDialog.setButtonView(buttonView);
        fDialog.setCancelable(false);
        fDialog.show();
    }

    @Override
    public void showResult(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showPopup(int code, String message)
    {

    }

    @Override
    public void finishActivity() {
        this.finish();
    }

    public void startProgressDialog( final String title , final String message )
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mProgressDialog = new FitmeterProgressDialog(SetUserActivity.this);
                mProgressDialog.setMessage(message);
                mProgressDialog.setCancelable(false);
                mProgressDialog.show();
            }
        });
    }

    private void changeProgressDialogMessage( final String message ){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mProgressDialog.setMessage(message);
            }
        });
    }

    public void stopProgressDialog(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if( mProgressDialog != null ){
                    mProgressDialog.dismiss();
                }
            }
        });
    }

    @Override
    public void setDuplicateResult(boolean duplicateResult) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if( requestCode == DeviceRegisterFragment.ENABLE_BT_REQUEST_ID ){
            mDeviceRegisterFragment.onActivityResult( requestCode , resultCode , data );
        }
    }

    @Override
    public void onBackPressed()
    {
        //super.onBackPressed();

        if( mStep == 1 ) {
            finish();
        }else if(mStep == 2)
        {
            mDeviceRegisterFragment.moveBack();
        }
        else if( mStep == 3 )
        {

            mDeviceRegisterFragment = new DeviceRegisterFragment().newInstance();
            mDeviceRegisterFragment.setDeviceRegisterListener( deviceRegisterListener );
            moveBack();

            //FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            //fragmentTransaction.replace(R.id.fl_activity_setuser, mDeviceRegisterFragment);
            //fragmentTransaction.addToBackStack(null);
            //fragmentTransaction.commit();
        }

    }

    public void onWearingLocationBack() {

        mDeviceRegisterFragment = new DeviceRegisterFragment().newInstance();
        mDeviceRegisterFragment.setDeviceRegisterListener( deviceRegisterListener );
        moveBack();

        //FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        //fragmentTransaction.replace(R.id.fl_activity_setuser, mDeviceRegisterFragment);
        //fragmentTransaction.addToBackStack(null);
        //fragmentTransaction.commit();



    }
}
