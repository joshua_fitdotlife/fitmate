package com.fitdotlife.fitmate;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v4.widget.DrawerLayout;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.StrengthInputType;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.UserInfoForAnalyzer;
import com.fitdotlife.fitmate_lib.customview.ArcChartView;
import com.fitdotlife.fitmate_lib.customview.DrawerView;
import com.fitdotlife.fitmate_lib.customview.MonthPointBarChartView;
import com.fitdotlife.fitmate_lib.customview.WeekExerciseTimeBarChartView;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.object.ExerciseProgram;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.presenter.LoginPresenter;
import com.fitdotlife.fitmate_lib.util.DateUtils;
import com.fitdotlife.fitmate_lib.util.Utils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringRes;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Random;

@EActivity(R.layout.activity_main_as)
public class MainAsActivity extends Activity {

    private ExerciseProgram appliedProgram = null;
    private final int ANIMATION_DURATION = 3000;

    @ViewById(R.id.ic_activity_mainas_actionbar)
    View actionBarView;

    @ViewById(R.id.btn_activity_mainas_select_day)
    TextView btnSelectDay;

    @ViewById(R.id.ll_activity_mainas_select_day_mark)
    LinearLayout llSelectDayMark;

    @ViewById(R.id.btn_activity_mainas_select_week)
    TextView btnSelectWeek;

    @ViewById(R.id.ll_activity_mainas_select_week_mark)
    LinearLayout llSelectWeekMark;

    @ViewById(R.id.btn_activity_mainas_select_month)
    TextView btnSelectMonth;

    @ViewById(R.id.ll_activity_mainas_select_month_mark)
    LinearLayout llSelectMonthMark;

    @ViewById(R.id.tv_activity_mainas_activitydate)
    TextView tvActivityDate;

    @ViewById(R.id.tv_activity_mainas_pre_date)
    TextView tvPreDate;

    @ViewById(R.id.tv_activity_mainas_next_date)
    TextView tvNextDate;

    @ViewById(R.id.tv_activity_mainas_applied_program)
    TextView tvAppliedProgram;

    @ViewById(R.id.ll_activity_mainas_activity_list_1)
    LinearLayout llActivityList_1;

    @ViewById(R.id.img_activity_mainas_activity_list_1_img)
    ImageView imgActivityList_1;

    @ViewById(R.id.tv_activity_mainas_activity_list_1_title)
    TextView tvActivityListTitle_1;

    @ViewById(R.id.img_activity_mainas_activity_list_1_arrow)
    ImageView imgActivityListArrow_1;

    @ViewById(R.id.ll_activity_mainas_activity_list_1_chart)
    LinearLayout llActivityListChart_1;

    @ViewById(R.id.dl_activity_mainas)
    DrawerLayout dlMainAs;

    @ViewById(R.id.dv_activity_mainas_drawer)
    DrawerView drawerView;

    @StringRes(R.string.exercise_promoting_calorie)
    String strCalorieName;

    @AfterViews
    void onInit(){

        ActivityManager.getInstance().addActivity(this);

        actionBarView.setBackgroundColor(this.getResources().getColor(R.color.fitmate_orange));
        TextView tvBarTitle = (TextView) actionBarView.findViewById( R.id.tv_custom_action_bar_title );
        ImageView imgLeft = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_left);
        imgLeft.setVisibility(View.GONE);
        ImageView imgSliding = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_right);
        imgSliding.setBackground(this.getResources().getDrawable(R.drawable.icon_slide_selector));
        imgSliding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dlMainAs.openDrawer(drawerView);
            }
        });
        drawerView.setParentActivity(this);
        drawerView.setTag( this.getResources().getString(R.string.gnb_activity) );
        this.drawerView.setDrawerLayout( dlMainAs );

        tvBarTitle.setText(this.getString(R.string.activity_bar_title));

        this.appliedProgram = new ExerciseProgram();
        appliedProgram.setCategory(StrengthInputType.CALORIE.Value);
        appliedProgram.setStrengthFrom(450);
        appliedProgram.setStrengthTo(450);
        appliedProgram.setName(strCalorieName);
        appliedProgram.setMinuteFrom(30);
        appliedProgram.setMinuteTo(30);
        appliedProgram.setTimesFrom(7);
        appliedProgram.setTimesTo(7);

        displayDayActivity();

    }

    private void displayDayActivity(){

        this.tvAppliedProgram.setText(appliedProgram.getName());

        this.btnSelectDay.setTextColor(Color.BLACK);
        this.btnSelectWeek.setTextColor(0xFFA9A9A9);
        this.btnSelectMonth.setTextColor(0xFFA9A9A9);

        this.llSelectDayMark.setBackgroundColor(this.getResources().getColor(R.color.fitmate_orange));
        this.llSelectWeekMark.setBackgroundColor(Color.WHITE);
        this.llSelectMonthMark.setBackgroundColor(Color.WHITE);

        DateFormat format3 = DateFormat.getDateInstance(DateFormat.LONG);
        Calendar calendar =Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());

        this.tvActivityDate.setText(format3.format(calendar.getTime()));

        this.imgActivityList_1.setImageResource(R.drawable.icon_medal);
        this.tvActivityListTitle_1.setText(this.getString(R.string.activity_day_achievement));

        this.llActivityListChart_1.removeAllViews();

        ArcChartView dayScoreArcChartView = new ArcChartView(this);
        dayScoreArcChartView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 170, this.getResources().getDisplayMetrics())));
        dayScoreArcChartView.setWeekValue(45);

        Calendar mToday = Calendar.getInstance();
        dayScoreArcChartView.setWeekUnit(this.getString(R.string.common_point));

        StrengthInputType strengthType = StrengthInputType.getStrengthType( appliedProgram.getCategory() );

        if( strengthType.equals( StrengthInputType.CALORIE ) || strengthType.equals( StrengthInputType.CALORIE_SUM )|| strengthType.equals(StrengthInputType.VS_BMR)) {

            dayScoreArcChartView.setRangMax((int) appliedProgram.getStrengthTo());


            dayScoreArcChartView.setTodayValue( 300 );
            dayScoreArcChartView.setTodayUnit(this.getString(R.string.common_calorie), StrengthInputType.getStrengthType(appliedProgram.getCategory()));
            dayScoreArcChartView.setTodayRange((int) appliedProgram.getStrengthFrom(), (int) appliedProgram.getStrengthTo());
        }else{
            dayScoreArcChartView.setRangMax(appliedProgram.getMinuteTo());
            dayScoreArcChartView.setTodayValue( 30 );
            dayScoreArcChartView.setTodayUnit(this.getString(R.string.common_minute), StrengthInputType.getStrengthType(appliedProgram.getCategory()));
            dayScoreArcChartView.setTodayRange(appliedProgram.getMinuteFrom(), appliedProgram.getMinuteTo());
        }
        dayScoreArcChartView.startAnimation(this.ANIMATION_DURATION);

        this.llActivityListChart_1.addView(dayScoreArcChartView);
    }

    private void displayWeekActivity(){

        this.tvAppliedProgram.setText(appliedProgram.getName());

        this.btnSelectDay.setTextColor(0xFFA9A9A9);
        this.btnSelectWeek.setTextColor(Color.BLACK);
        this.btnSelectMonth.setTextColor(0xFFA9A9A9);

        this.llSelectDayMark.setBackgroundColor(Color.WHITE);
        this.llSelectWeekMark.setBackgroundColor(this.getResources().getColor(R.color.fitmate_orange));
        this.llSelectMonthMark.setBackgroundColor(Color.WHITE);

        Calendar calendar =Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        int weekNumber = calendar.get(Calendar.WEEK_OF_MONTH);

        Locale current = getResources().getConfiguration().locale;
        this.tvActivityDate.setText(Utils.toWeekString(weekNumber, calendar, current));

        this.imgActivityList_1.setImageResource(R.drawable.icon_medal);
        this.tvActivityListTitle_1.setText(this.getResources().getString(R.string.activity_week_achievement));


        WeekExerciseTimeBarChartView weekExerciseTimeBarChartView = new WeekExerciseTimeBarChartView(this);
        weekExerciseTimeBarChartView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 140, this.getResources().getDisplayMetrics())));
        int[] values;

        String[] arrYText = this.getResources().getStringArray(R.array.week_char);
        int[] arrCalorie = new int[]{203,405,608,205,502,606,206};
        int[] arrExerciseTime = new int[]{23,45,68,25,52,66,26};
        String[] arrDayDate = new String[7];

        Calendar weekCalendar = Calendar.getInstance();
        //이번주의 첫날로 이동한다.
        weekCalendar.add(Calendar.DATE, -(weekCalendar.get(Calendar.DAY_OF_WEEK) - 1));

        for (int index = 0; index < 7; index++) {
            String searchDate = this.getDateString(weekCalendar);
            arrDayDate[index] = DateUtils.getDateStringForPattern(searchDate , this.getString(R.string.activity_week_day_date_format));
            weekCalendar.add(Calendar.DATE, 1);
        }

        if(appliedProgram.getCategory() == StrengthInputType.CALORIE.Value || appliedProgram.getCategory() == StrengthInputType.CALORIE_SUM.Value) {
            weekExerciseTimeBarChartView.setYAxisTitle(this.getString(R.string.common_calorie));
            values = arrCalorie;
            weekExerciseTimeBarChartView.setRange((int) appliedProgram.getStrengthFrom(), (int) appliedProgram.getStrengthTo());
        } else if( appliedProgram.getCategory() == StrengthInputType.VS_BMR.Value ){
            FitmateDBManager dbManager = new FitmateDBManager(this);
            UserInfo userInfo = dbManager.getUserInfo();
            UserInfoForAnalyzer userInfoForAnalyzer = com.fitdotlife.fitmate_lib.service.util.Utils.getUserInfoForAnalyzer(userInfo);

            int bmr = userInfoForAnalyzer.getBMR();

            weekExerciseTimeBarChartView.setYAxisTitle(this.getString(R.string.common_calorie));
            values = arrCalorie;
            weekExerciseTimeBarChartView.setRange((int) (bmr * (appliedProgram.getStrengthFrom() / 100)), (int) (bmr * (appliedProgram.getStrengthTo() / 100)));

        } else{
            weekExerciseTimeBarChartView.setYAxisTitle(this.getString(R.string.common_minute));
            values = arrExerciseTime;
            weekExerciseTimeBarChartView.setRange((int) appliedProgram.getMinuteFrom(), (int) appliedProgram.getMinuteTo());
        }
        weekExerciseTimeBarChartView.setXAxisTextList(arrYText);
        weekExerciseTimeBarChartView.setXAxisDateList(arrDayDate);
        weekExerciseTimeBarChartView.setValues(values);

        weekExerciseTimeBarChartView.startAnimation(this.ANIMATION_DURATION);

        this.llActivityListChart_1.removeAllViews();
        this.llActivityListChart_1.addView( weekExerciseTimeBarChartView );
    }

    private void displayMonthActivity(){

        this.tvAppliedProgram.setText(appliedProgram.getName());

        this.btnSelectDay.setTextColor(0xFFA9A9A9);
        this.btnSelectWeek.setTextColor(0xFFA9A9A9);
        this.btnSelectMonth.setTextColor(Color.BLACK);

        this.llSelectDayMark.setBackgroundColor(Color.WHITE);
        this.llSelectWeekMark.setBackgroundColor(Color.WHITE);
        this.llSelectMonthMark.setBackgroundColor(this.getResources().getColor(R.color.fitmate_orange));

        Calendar calendar =Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        int lastDay  = calendar.getActualMaximum(Calendar.DATE);
        calendar.set(Calendar.DAY_OF_MONTH, lastDay);
        int monthWeekNumber = calendar.get(Calendar.WEEK_OF_MONTH);

        Locale current = getResources().getConfiguration().locale;
        this.tvActivityDate.setText(Utils.toMonthString(calendar, current));

        this.imgActivityList_1.setImageResource(R.drawable.icon_medal);
        this.tvActivityListTitle_1.setText(this.getString(R.string.activity_month_achievement));

        MonthPointBarChartView monthlyScoreBarChartView = new MonthPointBarChartView(this);
        monthlyScoreBarChartView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 140, this.getResources().getDisplayMetrics())));

        int[] scoreValues = new int[monthWeekNumber];
        Random random = new Random();
        for( int i = 0 ; i < monthWeekNumber ; i++ )
        {
            scoreValues[i] = random.nextInt(90);
        }

        String[] arrXText = new String[ scoreValues.length ];

        for( int i = 0 ; i < scoreValues.length ;i++ ){
            arrXText[i] = this.getWeekNumberChar( i + 1 );
        }

        monthlyScoreBarChartView.setYAxisTitle(this.getString(R.string.common_score));
        monthlyScoreBarChartView.setXAxisTextList(arrXText);
        monthlyScoreBarChartView.setValues(scoreValues);
        //monthlyScoreBarChartView.setRange( 20, 40 );
        monthlyScoreBarChartView.startAnimation(this.ANIMATION_DURATION);

        this.llActivityListChart_1.removeAllViews();
        this.llActivityListChart_1.addView(monthlyScoreBarChartView);
    }


    @Click(R.id.rl_activity_mainas_select_day)
    void selectDayClick(){
        displayDayActivity();
    }

    @Click(R.id.rl_activity_mainas_select_week)
    void selectWeekClick(){
        displayWeekActivity();
    }

    @Click(R.id.rl_activity_mainas_select_month)
    void selectMonthClick(){
        displayMonthActivity();
    }

    private String getWeekNumberChar( int weekNumber )
    {
        return Utils.toWeekString(weekNumber,  getResources().getConfiguration().locale);
    }



    @Override
    public void onBackPressed() {

        if(dlMainAs.isDrawerOpen(drawerView)){
            dlMainAs.closeDrawers();
            return;
        }

//        AlertDialog.Builder builder = new AlertDialog.Builder(this);     // 여기서 this는 Activity의 this
//
//// 여기서 부터는 알림창의 속성 설정
//        builder.setTitle(getString(R.string.common_quit_title))        // 제목 설정
//                .setMessage(getString(R.string.common_quit_content))        // 메세지 설정
//                .setCancelable(true)        // 뒤로 버튼 클릭시 취소 가능 설정
//                .setPositiveButton(R.string.common_yes, new DialogInterface.OnClickListener() {
//                    // 확인 버튼 클릭시 설정
//                    public void onClick(DialogInterface dialog, int whichButton) {
//                        moveTaskToBack(true);
//
//                        finish();
//
//                        //  android.os.Process.killProcess(android.os.Process.myPid());
//                        dialog.cancel();
//
//                        //   MainActivity.this.onBackPressed();
//                    }
//                })
//                .setNegativeButton(R.string.common_no, new DialogInterface.OnClickListener() {
//                    // 취소 버튼 클릭시 설정
//                    public void onClick(DialogInterface dialog, int whichButton) {
//                        dialog.cancel();
//                    }
//                });
//
//
//        AlertDialog dialog = builder.create();    // 알림창 객체 생성
//        dialog.show();    // 알림창 띄우기

        ActivityManager.getInstance().finishAllActivity();
        showNewHomeActivity();
    }

    @Click(R.id.btn_activity_mainas_start_fitmeter)
    void startFitmeterClick(){
        Intent intent = new Intent( this.getApplicationContext() , SetUserInfoActivity.class );
        startActivity(intent);
    }

    private String getDateString( Calendar calendar )
    {
        return calendar.get(Calendar.YEAR) + "-" + String.format("%02d",  calendar.get( Calendar.MONTH ) + 1 ) + "-" + String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH) );
    }

    private boolean getSettedUserInfo()
    {
        SharedPreferences pref = this.getSharedPreferences("fitmateservice", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        return pref.getBoolean( LoginPresenter.SET_USERINFO_KEY , false);
    }

    //백버튼을 누르거나 액션바에 홈 버튼을 누를 때 홈 화면을 띄운다.
    private void showNewHomeActivity()
    {
        Intent intent = new Intent();

        if( getSettedUserInfo() ) {
            intent.setClass( this , NewHomeActivity_.class );
            intent.putExtra(NewHomeActivity.HOME_LAUNCH_KEY, NewHomeActivity.LaunchMode.FROM_OTHERMENU.ordinal());
        }else{
            intent.setClass( this , NewHomeAsActivity_.class );
        }

        startActivity( intent );
    }


}
