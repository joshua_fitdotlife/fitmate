package com.fitdotlife.fitmate;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitdotlife.fitmate_lib.key.FitmeterType;

/**
 * Created by Joshua on 2016-08-03.
 */
public class SelectDeviceFragment extends Fragment
{
    private View viewBLE = null;
    private View viewBAND = null;
    public static final String DEVICETYPE_KEY = "devicetype";

    private SelectDeviceListener mListener = null;

    public interface SelectDeviceListener
    {
        void selectDeviceType( FitmeterType fitmeterType );
    }

    public static SelectDeviceFragment newInstance() {

        Bundle args = new Bundle();

        SelectDeviceFragment fragment = new SelectDeviceFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.fragment_select_device , container , false);
        this.viewBAND  = rootView.findViewById(R.id.view_fragment_select_device_band);
        this.viewBAND.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.selectDeviceType( FitmeterType.BAND );
            }
        });

        this.viewBLE = rootView.findViewById(R.id.view_fragment_select_device_ble);
        this.viewBLE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.selectDeviceType( FitmeterType.BLE );
            }
        });

        return rootView;
    }

    public void setSelectDeviceListener( SelectDeviceListener listener ){
        mListener = listener;
    }
}
