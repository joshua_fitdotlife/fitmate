package com.fitdotlife.fitmate;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.fitdotlife.fitmate_lib.database.FitmateDBManager;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

/**
 * Created by Joshua on 2016-09-19.
 */
@EActivity(R.layout.activity_metdetail)
public class METDetailActivity extends Activity
{
    private FitmateDBManager mDBManager = null;

    @ViewById(R.id.tv_activity_metdeail_actionbar_title)
    TextView tvActionBarTitle;

    @ViewById(R.id.iv_activity_metdetail_actionbar_left)
    ImageView ivActionBarLeft;

    @ViewById(R.id.iv_activity_metdetail_actionbar_right)
    ImageView ivActionBarRight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDBManager = new FitmateDBManager(this);
    }

    @Click(R.id.iv_activity_metdetail_actionbar_left)
    void actionBarLeftClick(){}
}
