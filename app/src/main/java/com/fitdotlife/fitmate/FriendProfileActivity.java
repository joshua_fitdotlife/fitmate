package com.fitdotlife.fitmate;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseAnalyzer;
import com.fitdotlife.fitmate_lib.customview.AppliedProgramLayout;
import com.fitdotlife.fitmate_lib.customview.AppliedProgramSummaryLayout;
import com.fitdotlife.fitmate_lib.customview.FitmeterDialog;
import com.fitdotlife.fitmate_lib.customview.FriendProfileCommonView;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.http.FriendService;
import com.fitdotlife.fitmate_lib.http.HttpApi;
import com.fitdotlife.fitmate_lib.http.HttpResponse;
import com.fitdotlife.fitmate_lib.http.NetworkClass;
import com.fitdotlife.fitmate_lib.http.RestHttpErrorHandler;
import com.fitdotlife.fitmate_lib.key.FriendRequestType;
import com.fitdotlife.fitmate_lib.key.FriendStatusType;
import com.fitdotlife.fitmate_lib.object.ExerciseProgram;
import com.fitdotlife.fitmate_lib.object.UserFriend;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.rest.spring.annotations.RestService;
import org.json.JSONException;
import org.springframework.web.client.RestClientException;

import java.io.File;
import java.util.Calendar;
import java.util.Locale;

@EActivity(R.layout.activity_friend_profile)
public class FriendProfileActivity extends Activity implements View.OnTouchListener {

    private final String TAG = FriendProfileActivity.class.getSimpleName();

    private FitmateDBManager mDBManager;
    private AppliedProgramSummaryLayout aplProgramInfo = null;
    private UserFriend mUserFriend = null;

    @ViewById(R.id.img_activity_friend_profile_gender)
    ImageView imgGender;

    @ViewById(R.id.tv_activity_friend_profile_birthdate)
    TextView tvBirthDate;

    @ViewById(R.id.tv_activity_friend_profile_email)
    TextView tvEmail;

    @ViewById(R.id.tv_activity_friend_profile_intro)
    TextView tvIntro;

    @ViewById(R.id.tv_activity_friend_profile_program_name)
    TextView tvProgramName;

    @ViewById(R.id.fpv_activity_friend_profile)
    protected FriendProfileCommonView commonView;

    @ViewById(R.id.ic_activity_friend_actionbar)
    View actionBarView;

    @RestService
    FriendService friendServiceClient;

    @Bean
    RestHttpErrorHandler restErrorHandler;

    @AfterViews
    void init()
    {
        friendServiceClient.setRootUrl(NetworkClass.baseURL + "/api");
        friendServiceClient.setRestErrorHandler( restErrorHandler );

        this.mDBManager = new FitmateDBManager(this);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        mUserFriend = extras.getParcelable("UserFriend");

        FriendActivityManager.getInstance().addActivity(this);

        String imageName = mUserFriend.getProfileImagePath();
        if( !( imageName == null ) ) {
            commonView.setProfileImage(imageName);
        }

        String name =  mUserFriend.getName();
        commonView.setName(name);
        this.displayActionBar( name );

        tvIntro.setText( mUserFriend.getIntroYourSelf() );
        this.tvEmail.setText( mUserFriend.getEmail() );

        boolean sex = false;
         sex = mUserFriend.isSex();

        if( sex ){
            imgGender.setImageResource( R.drawable.profile_sex2 );
        }else{
            imgGender.setImageResource( R.drawable.profile_sex1 );
        }

        String strBirthdate = mUserFriend.getBirthDate();
        strBirthdate = strBirthdate.substring(0,10);
        this.tvBirthDate.setText(strBirthdate);


        ExerciseProgram exerciseProgram = this.mDBManager.getUserExerciProgram( mUserFriend.getExerciseProgramID() );
        String programName = null;
        if(exerciseProgram != null) {
            if (exerciseProgram.getDefaultProgram()) {
                try {
                    int resId = getResources().getIdentifier(exerciseProgram.getName(), "string", getPackageName());
                    programName = getString(resId);
                }catch(Exception e){}

            } else {
                programName = exerciseProgram.getName();
            }
            if( programName != null ) {
                displayExerciseProgram(exerciseProgram , programName);
            }else{
                getExerciseProgram( mUserFriend.getExerciseProgramID() );
            }

        }else{
            getExerciseProgram( mUserFriend.getExerciseProgramID() );
        }


    }

    private void displayExerciseProgram( ExerciseProgram exerciseProgram , String programName )
    {
        tvProgramName.setText( programName );
        this.aplProgramInfo = (AppliedProgramSummaryLayout) this.findViewById(R.id.apl_activity_friend_profile_appliedprogram_info);
        this.aplProgramInfo.setAppliedExerciseProgram(exerciseProgram);
    }

    private void displayActionBar( String name ){
        actionBarView.setBackgroundColor(this.getResources().getColor(R.color.fitmate_blue));
        TextView tvBarTitle = (TextView) actionBarView.findViewById( R.id.tv_custom_action_bar_title );
        tvBarTitle.setText(name + " " + this.getString(R.string.friend_friendprofile_title));
        ImageView imgBack = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_left);
        imgBack.setVisibility(View.GONE);

        ImageView mImgRight = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_right);
        mImgRight.setImageResource(R.drawable.friend_del);
        mImgRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                final FitmeterDialog imageDialog = new FitmeterDialog( FriendProfileActivity.this );
                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                View contentView = inflater.inflate(R.layout.dialog_default_content, null);
                TextView tvTitle = (TextView) contentView.findViewById(R.id.tv_dialog_default_title);
                tvTitle.setText(getString(R.string.friend_friendprofile_delete_friend_title));

                TextView tvContent = (TextView) contentView.findViewById(R.id.tv_dialog_default_content);
                tvContent.setText(getString(R.string.friend_friendprofile_delete_friend_content));
                imageDialog.setContentView(contentView);

                View buttonView = inflater.inflate(R.layout.dialog_button_two , null);
                Button btnLeft = (Button) buttonView.findViewById(R.id.btn_dialog_button_two_left);
                btnLeft.setText( getString(R.string.common_no));
                btnLeft.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        imageDialog.close();
                    }
                });

                Button btnRight = (Button) buttonView.findViewById(R.id.btn_dialog_button_two_right);
                btnRight.setText( getString(R.string.common_yes));
                btnRight.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        deleteFriend();
                        imageDialog.close();
                    }
                });

                imageDialog.setButtonView(buttonView);
                imageDialog.show();
            }
        });
        mImgRight.setOnTouchListener(this);
    }

    @Background
    void deleteFriend(){
        boolean result = false;

        try {

            result = friendServiceClient.changeStatus(mDBManager.getUserInfo().getEmail(), mUserFriend.getUserFriendsID(), FriendStatusType.DELETE.getValue() );

        }catch( RestClientException e){
            Log.e("fitmate", e.getMessage());
        }
        if(result) {
            finishActivity(mUserFriend.getEmail());
        }
    }

    @UiThread
    void finishActivity( String friendEmail ){

        this.setDeletedFriendEmail(friendEmail);
        FriendActivityManager.getInstance().finishAllActivity();

    }

    private void setDeletedFriendEmail( String friendEmail ){
        SharedPreferences pref = this.getSharedPreferences( "fitmateservice" , Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(FriendActivity.FRIEND_DELETED_FRIEND_EMAIL_KEY, friendEmail);
        editor.commit();
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        ImageView actionbarImageView = (ImageView) view;
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            actionbarImageView.setColorFilter(this.getResources().getColor(R.color.selected_colorfilter) , PorterDuff.Mode.DARKEN );
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            actionbarImageView.setColorFilter(null);
        }

        return false;
    }

    private void getExerciseProgram( int programID  )
    {
        HttpApi.getExerciseProgram(programID, Locale.getDefault().getLanguage(), new HttpResponse() {
            @Override
            public void onResponse(int resultCode, String response) {
                if (resultCode == 200) {
                    if( response != null && !response.equals( "null" ))
                    {
                        ExerciseProgram program = null;
                        try {
                            program = ExerciseProgram.getExerciseProgram(response);
                        } catch (JSONException e) {
                            org.apache.log4j.Log.e(TAG, e.getMessage());
                        }

                        if (program != null) {
                            displayExerciseProgram(program , program.getName());
                        }
                    }
                } else {
                    Toast.makeText( FriendProfileActivity.this , getResources().getString(R.string.common_connect_network), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
