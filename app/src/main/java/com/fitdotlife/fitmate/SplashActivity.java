package com.fitdotlife.fitmate;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.PermissionChecker;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.log4j.Log;
import com.fitdotlife.fitmate.model.ActivityDataListener;
import com.fitdotlife.fitmate.model.ActivityDataModel;
import com.fitdotlife.fitmate.model.ExerciseProgramModel;
import com.fitdotlife.fitmate.model.UserInfoModel;
import com.fitdotlife.fitmate.model.UserInfoModelResultListener;
import com.fitdotlife.fitmate_lib.customview.FitmeterDialog;
import com.fitdotlife.fitmate_lib.customview.FitmeterProgressDialog;
import com.fitdotlife.fitmate_lib.object.BLEDevice;
import com.fitdotlife.fitmate_lib.object.DayActivity;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.presenter.LoginPresenter;
import com.fitdotlife.fitmate_lib.util.Utils;

import org.androidannotations.annotations.EActivity;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

/**
 * Created by Joshua on 2016-06-21.
 */
@EActivity(R.layout.activity_splash)
public class SplashActivity extends Activity implements UserInfoModelResultListener , ActivityDataListener{

    private final String TAG = SplashActivity.class.getSimpleName();

    private final int PERMISSION_REQUEST_STORAGE = 3;

    private final int INIT_PERIOD = 2000;
    private Handler mHandler;
    private UserInfoModel mModel = null;
    private ActivityDataModel mActivityDataModel = null;
    private ExerciseProgramModel mProgrmaModel = null;
    private FitmeterDialog mDialog = null;
    private String mEncodePassword = null;
    private String mID = null;
    private Runnable finishRunable = null;
    private FitmeterProgressDialog mProgressDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.mModel = new UserInfoModel(this , this);
        this.mActivityDataModel = new ActivityDataModel(this , this);
        this.mProgrmaModel = new ExerciseProgramModel(this);

        this.finishRunable = new Runnable() {
            @Override
            public void run()
            {
                checkFilePermission();
            }
        };

        mHandler = new Handler( );
    }

    @Override
    protected void onStop() {
        super.onStop();
        mHandler.removeCallbacks( finishRunable );
    }

    @Override
    protected void onStart() {
        super.onStart();
        mHandler.postDelayed(finishRunable, INIT_PERIOD);
    }

    public void checkFilePermission()
    {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ){

            if( PermissionChecker.checkSelfPermission(this , Manifest.permission_group.STORAGE) == PackageManager.PERMISSION_GRANTED ) {
                initLogSystem();
                checkVersion();
            }else{
                ActivityCompat.requestPermissions( this , new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST_STORAGE);
            }
        }else{
            initLogSystem();
            checkVersion();
        }
    }

    public void checkVersion()
    {
        if( this.getAutoLogin() )
        {
            //서버와 통신이 가능한지 확인한다.
            if (!Utils.isOnline(this)) {
                autoLogin();
            } else {
                this.startProgressDialog();

                String version = "0.0.0";
                PackageInfo pInfo = null;
                try {
                    pInfo = getPackageManager().getPackageInfo( getPackageName(), 0);
                    version = pInfo.versionName;
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }

                mModel.checkVersion(version);
            }
        }else{
            finish();
            navigateLogin();
        }
    }

    public void autoLogin( )
    {

        Log.d(TAG, "자동 로그인 시작");

        UserInfo userInfo = this.mModel.getUserInfo();
        String email = userInfo.getEmail();
        String pwd = userInfo.getPassword();

        this.mEncodePassword = pwd;
        this.mID = email;

        if( this.mModel.localLogin(email, mEncodePassword) )
        {
            Log.d(TAG, "로컬 로그인 성공" );
            if( userInfo.getDeviceAddress() != null && !userInfo.getDeviceAddress().equals("") && !userInfo.getDeviceAddress().equals("null") ){
                this.setSettedUserInfo(true);
            }else{
                this.setSettedUserInfo(false);
            }

            if(!Utils.isOnline(this) ) {
                Log.d(TAG, "인터넷 연결 없음" );
                this.setAutoLogin(true);
                this.setServerUpdate(true);
                this.setCheckBluetoothOn(false);
                this.setRequestBluetoothOn(false);
                this.setInitialViewComplete(false);

                stopProgressDialog();
                finish();
                navigateToHome();
                return;
            }

            Log.d(TAG, "서버에 사용자 정보 저장 시작" );
            this.mModel.registerUserInfo();

        }else{

            if(!Utils.isOnline(this) ) {
                Log.d(TAG, "인터넷 연결 없음");
                Toast.makeText(this, this.getString(R.string.common_connect_network), Toast.LENGTH_LONG);
                this.stopProgressDialog();
                finish();
                navigateLogin();
                return;
            }

            Log.d(TAG, "서버 로그인 시작");
            this.mModel.serverLogin(email, mEncodePassword);
        }
    }

    private void startProgressDialog(){


        this.mProgressDialog = new FitmeterProgressDialog( this );
        this.mProgressDialog.setMessage(this.getString(R.string.login_dialog_content));
        this.mProgressDialog.show();;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if( this.mProgressDialog != null ){
            this.mProgressDialog.dismiss();
        }
    }

    private void stopProgressDialog(){
        if( this.mProgressDialog != null ) {
            this.mProgressDialog.dismiss();
        }
    }

    public void navigateToHome()
    {
        // Get the metrics
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int heightPixels = metrics.heightPixels;
        int widthPixels = metrics.widthPixels;
        int densityDpi = metrics.densityDpi;
        float density = metrics.density;
        float scaledDensity = metrics.scaledDensity;
        float xdpi = metrics.xdpi;
        float ydpi = metrics.ydpi;

        Log.e("Main onCreate", "Screen W x H pixels: " + widthPixels  + " x " + heightPixels);
        Log.e("Main onCreate", "Screen X x Y dpi: " + xdpi + " x " + ydpi);
        Log.e("Main onCreate", "density = " + density + "  scaledDensity = " + scaledDensity + "  densityDpi = " + densityDpi);

        Intent intent = null;
        if( getSettedUserInfo() ) {
            intent = new Intent( SplashActivity.this, NewHomeActivity_.class );
        }else{
            intent = new Intent( SplashActivity.this, NewHomeAsActivity_.class );
        }

        this.startActivity(intent);

        if( !getHomeNotView() ){
            this.showTutorialActivity();
        }
    }

    public void navigateLogin(){
        Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
        startActivity(intent);
    }

    private void showTutorialActivity(){
        Intent intent = new Intent( SplashActivity.this , NewHomeTutorialActivity_.class );
        this.startActivity(intent);
    }

    @Override
    public void onSuccess(int code) {

        Log.d(TAG, "서버 로그인 성공");
        //자동로그인을 위하여 아이디와 비밀번호를 저장한다.
        this.setAutoLogin(true);
        this.setServerUpdate(true);
        this.setCheckBluetoothOn(false);
        this.setRequestBluetoothOn(false);
        this.setInitialViewComplete(false);

        stopProgressDialog();
        finish();
        navigateToHome();
    }

    @Override
    public void onSuccess(int code, boolean result) {
        Log.d(TAG, "앱 버전 체크 완료 - 업데이트 유무 : " + result);
        if(result){

            this.stopProgressDialog();
            this.showUpdateDialog();

        }else {

            this.autoLogin();

        }
    }

    public void showUpdateDialog()
    {
        AlertDialog.Builder updateDialog = new AlertDialog.Builder(this);
        updateDialog.setTitle( getString(R.string.required_update_title) );
        updateDialog.setCancelable(false)
        ;        updateDialog.setMessage(getString(R.string.required_update_content));

        updateDialog.setNegativeButton( getString(R.string.common_cancel) , new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        updateDialog.setPositiveButton( getString(R.string.common_update)  , new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                Intent updateIntent = new Intent(Intent.ACTION_VIEW);
                updateIntent.setData(Uri.parse("market://details?id=" + getPackageName()));
                startActivity(updateIntent);
                finish();
            }
        });
        updateDialog.show();
    }

    @Override
    public void onSuccess(int code, UserInfo userInfo) {

        Log.d(TAG, "서버 로그인 성공");
        if( code == UserInfoModel.LOGIN_RESPONSE_CODE ) {
            //로컬 DB에 서버의 사용자 정보를 저장한다.
            if (userInfo != null) {
                this.mModel.setUserInfoToLocalDB(userInfo);

                //이전에 적용하고 있던 프로그램으로 적용한다.
                int appliedProgramID = userInfo.getExerprogramId();
                this.mProgrmaModel.applyExerciseProgram( appliedProgramID );

            }
        }

        this.setAutoLogin(true);
        this.setServerUpdate(true);
        this.setCheckBluetoothOn(false);
        this.setRequestBluetoothOn(false);
        this.setInitialViewComplete(false);

        if( this.getInitExecute() )
        {

            Log.d(TAG, "처음 실행하여 Mergy 데이터 요청");
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(userInfo.getLastDataDate());
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);
            calendar.add(Calendar.DATE, -calendar.get(Calendar.DAY_OF_WEEK) + 1);
            // this.mActivityDataModel.getDayActivityFromServer( calendar.getTimeInMillis() );

            //this.mActivityDataModel.getDayActivityFromServer_Mergy( DateUtils.convertUTCTicktoDateString(userInfo.getLastDataDate()) );
            String activityDate = calendar.get(Calendar.YEAR) + "-" + String.format("%02d",  calendar.get( Calendar.MONTH ) + 1 ) + "-" + String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH) );
            this.mActivityDataModel.getDayActivityFromServer_Mergy(activityDate);

        }else {
            Log.d(TAG, "처음 실행이 아님." );
            this.stopProgressDialog();
            finish();
            this.navigateToHome();
        }

    }

    @Override
    public void onFail(int code) {
        this.stopProgressDialog();

        if( code == UserInfoModel.LOGIN_RESPONSE_CODE ) {

            //mView.showResult("로그인에 실패하였습니다.");
            finish();
            navigateLogin();

        }else{
            this.setAutoLogin(true);
            this.setServerUpdate(true);
            this.setCheckBluetoothOn(false);
            this.setRequestBluetoothOn(false);
            this.setInitialViewComplete(false);

            finish();
            this.navigateToHome();
        }
    }

    @Override
    public void onErrorOccured(int code, String message) {

        this.stopProgressDialog();

        if( code == UserInfoModel.LOGIN_RESPONSE_CODE ) {
            //mView.showResult("로그인 중에 에러가 발생했습니다.");
            finish();
            navigateLogin();
        }else{
            this.setAutoLogin(true);
            this.setServerUpdate(true);
            this.setCheckBluetoothOn(false);
            this.setRequestBluetoothOn(false);
            this.setInitialViewComplete(false);

            finish();
            this.navigateToHome();
        }
    }

    private boolean getSettedUserInfo()
    {
        SharedPreferences pref = this.getSharedPreferences("fitmateservice", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        return pref.getBoolean( LoginPresenter.SET_USERINFO_KEY , false);
    }

    private void setSettedUserInfo(boolean settedUserInfo){
        SharedPreferences pref = this.getSharedPreferences("fitmateservice", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean( LoginPresenter.SET_USERINFO_KEY , settedUserInfo);
        editor.commit();
    }

    private boolean getHomeNotView()
    {
        SharedPreferences pref = getSharedPreferences("fitmateservice", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        return pref.getBoolean( HelpActivity.HOME_NOT_VIEW_KEY  , false);
    }

    private void setAutoLogin( boolean autoLogin ){
        SharedPreferences pref = this.getSharedPreferences("fitmateservice", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(UserInfo.AUTO_LOGIN_KEY, autoLogin);
        editor.commit();
    }

    private void setServerUpdate( boolean serverUpdate ){
        SharedPreferences pref = this.getSharedPreferences( "fitmateservice" , Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(DayActivity.DAY_ACTIVITY_SERVER_UPDATE_KEY, serverUpdate);
        editor.commit();
    }

    private void setCheckBluetoothOn( boolean checkBluetoothOn ){
        SharedPreferences pref = this.getSharedPreferences( "fitmateservice" , Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean( LoginPresenter.CHECK_BLUETOOTHON_KEY, checkBluetoothOn);
        editor.commit();
    }

    private void setRequestBluetoothOn( boolean requestBluetoothOn ){
        SharedPreferences pref = this.getSharedPreferences("fitmateservice", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean( LoginPresenter.REQUEST_BLUETOOTHON_KEY, requestBluetoothOn);
        editor.commit();
    }

    private void setInitialViewComplete( boolean initViewComplete ){
        SharedPreferences pref = this.getSharedPreferences("fitmateservice", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean( LoginPresenter.INIT_VIEW_COMPLETE_KEY, initViewComplete);
        editor.commit();
    }

    private boolean getAutoLogin(){
        SharedPreferences pref = this.getSharedPreferences("fitmateservice", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        return pref.getBoolean(UserInfo.AUTO_LOGIN_KEY, false);
    }

    private boolean getInitExecute(){
        SharedPreferences pref = this.getSharedPreferences("fitmateservice", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        return pref.getBoolean( LoginPresenter.INIT_EXECUTE_KEY , true);
    }

    private void setInitExecute(boolean initExecute){
        SharedPreferences pref = this.getSharedPreferences("fitmateservice", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean( LoginPresenter.INIT_EXECUTE_KEY , initExecute);
        editor.commit();
    }

    @Override
    public void onDayActivityReceived() {
        Log.d(TAG, "Mergy 데이터 수신 성공");
        this.setInitExecute(false);
        this.stopProgressDialog();
        finish();
        this.navigateToHome();
    }

    @Override
    public void onWeekActivityReceived() {

    }

    @Override
    public void onMonthActivityReceived() {

    }

    @Override
    public void onUploadedData(boolean isUpload) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if( requestCode == PERMISSION_REQUEST_STORAGE )
        {

            if( grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED ){
                initLogSystem();
                checkVersion();
            }else{

                final FitmeterDialog fDialog = new FitmeterDialog(this);
                LayoutInflater inflater = this.getLayoutInflater();
                View contentView = inflater.inflate(R.layout.dialog_default_content, null);
                TextView tvTitle = (TextView) contentView.findViewById(R.id.tv_dialog_default_title);
                tvTitle.setVisibility( View.GONE );

                TextView tvContent = (TextView) contentView.findViewById(R.id.tv_dialog_default_content);
                tvContent.setText(this.getString( R.string.file_permission_reject));
                fDialog.setContentView(contentView);

                View buttonView = inflater.inflate(R.layout.dialog_button_one , null);
                Button btnOne = (Button) buttonView.findViewById(R.id.btn_dialog_button_one);
                btnOne.setText(this.getString(R.string.common_ok));
                btnOne.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        fDialog.close();
                        finish();
                    }
                });

                fDialog.setButtonView(buttonView);
                fDialog.show();

            }

        }
    }

    private void initLogSystem()
    {
        ConfigureLog4J.configure(getApplicationContext());

        Logger logger = Logger.getLogger(MyApplication.class);
        org.apache.log4j.Log.MakeLog("FITMATE");
    }

    private Comparator myComparator= new Comparator<BLEDevice>()
    {
        @Override
        public int compare( BLEDevice object1,BLEDevice object2) {
            return object1.getRssi() > object2.getRssi() ? -1 : object1.getRssi() < object2.getRssi() ? 1:0;
        }
    };

}
