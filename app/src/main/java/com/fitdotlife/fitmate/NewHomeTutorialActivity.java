package com.fitdotlife.fitmate;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseAnalyzer;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.StrengthInputType;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.UserInfoForAnalyzer;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.WearingLocation;
import com.fitdotlife.fitmate.newhome.CategoryType;
import com.fitdotlife.fitmate.newhome.DayPagerAdapter;
import com.fitdotlife.fitmate.newhome.NewHomeCategoryData;
import com.fitdotlife.fitmate.newhome.NewHomeConstant;
import com.fitdotlife.fitmate.newhome.NewHomeProgramClickListener;
import com.fitdotlife.fitmate.newhome.NewHomeUtils;
import com.fitdotlife.fitmate.newhome.NewsPagerAdapter;
import com.fitdotlife.fitmate.newhome.WeekPagerActivityInfo;
import com.fitdotlife.fitmate.newhome.WeekPagerAdapter;
import com.fitdotlife.fitmate_lib.customview.BatteryCircleView;
import com.fitdotlife.fitmate_lib.customview.DrawerView;
import com.fitdotlife.fitmate_lib.customview.NewHomeCategoryView;
import com.fitdotlife.fitmate_lib.customview.NewHomeCategoryView_;
import com.fitdotlife.fitmate_lib.customview.NewHomeDayCircleView_;
import com.fitdotlife.fitmate_lib.customview.NewHomeDayHalfCircleView_;
import com.fitdotlife.fitmate_lib.customview.NewHomeDayTextView_;
import com.fitdotlife.fitmate_lib.customview.NewHomeNewsView;
import com.fitdotlife.fitmate_lib.customview.NewHomeNewsView_;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.http.NetworkClass;
import com.fitdotlife.fitmate_lib.object.DayActivity;
import com.fitdotlife.fitmate_lib.object.ExerciseProgram;
import com.fitdotlife.fitmate_lib.object.FriendWeekActivity;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.object.WeekActivity;
import com.fitdotlife.fitmate_lib.util.DateUtils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringRes;
import org.apache.log4j.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

@EActivity(R.layout.activity_newhome_tutorial)
public class NewHomeTutorialActivity extends Activity implements WeekPagerAdapter.WeekMoveListener
{

    private enum TutorialStep{ CATEGORY , WEEK , DAY , NEWS , DATE , EXERCISEPROGRAM_ICON , BATTERY , DRAWER , DRAWER_HOME , DRAWER_ACTIVITY , DRAWER_EXERCISEPROGRAM , DRAWER_FRIEND , DRAWER_NEWS , CLOSE  }
    private TutorialStep mCurrentStep = null;

    private int mWeekPagerLength = 0;
    private final String TAG = "fitmate - " + NewHomeActivity.class.getSimpleName();

    private int selectdCategoryIndex = 0;

    private Calendar mTodayCalendar = null;
    private Calendar mDayCalendar = null;
    private Calendar mWeekCalendar = null;

    private FitmateDBManager mDBManager = null;
    private UserInfo mUserInfo = null;
    private DayPagerAdapter mDayPagerAdapter = null;
    private boolean mInitialDayDisplay = true;
    private boolean mInitialWeekDisplay = true;

    NewHomeCategoryView[] categoryViews = new NewHomeCategoryView[CategoryType.values().length];

    private NewHomeUtils newHomeUtils  = null;
    private WeekPagerActivityInfo[] mWeekPagerActivityInfoList = null;
    private NewHomeWeekPagerAdapter mWeekBarPagerAdapter = null;
    private NewHomeWeekPagerAdapter mWeekTablePagerAdapter = null;

    private int mScreenWidth = 0;
    private int mScreentHeight = 0;

    private GestureDetector swipeDetector;

    @StringRes(R.string.alarm_today_achieve_success)
    String strTodayAchieveSuccess;

    @ViewById(R.id.dv_activity_newhome_tutorial_drawer)
    DrawerView drawerView;

    @ViewById(R.id.ll_activity_newhome_tutorial_actionbar)
    RelativeLayout llActionBar;

    @ViewById(R.id.iv_activity_newhome_tutorial_category_swipe)
    ImageView ivCategorySwipe;

    @ViewById(R.id.ll_activity_newhome_tutorial_calendar)
    LinearLayout llCalendar;

    @ViewById(R.id.img_activity_newhome_tutorial_calendar_arrow)
    ImageView imgArrow;

    @ViewById(R.id.vp_activity_newhome_tutorial_day)
    ViewPager vpDay;

    @ViewById(R.id.vp_activity_newhome_tutorial_week_bar)
    ViewPager vpWeekBar;

    @ViewById(R.id.vp_activity_newhome_tutorial_week_table)
    ViewPager vpWeekTable;

    @ViewById(R.id.rl_activity_newhome_tutorial_week)
    RelativeLayout rlWeek;

    @ViewById(R.id.vp_activity_newhome_tutorial_news)
    ViewPager vpNews;

    @ViewById(R.id.tv_activity_newhome_tutorial_date)
    TextView tvDate;

    @ViewById(R.id.tv_activity_newhome_tutorial_weekday)
    TextView tvWeekDay;

    @ViewById(R.id.tv_activity_newhome_tutorial_date)
    TextView newHomeDate;

    @ViewById(R.id.hsv_activity_newhome_tutorial_category)
    HorizontalScrollView hsvCategory;

    @ViewById(R.id.rl_activity_newhome_tutorial_category)
    RelativeLayout rlCategory;

    @ViewById(R.id.iv_activity_newhome_tutorial_batterybackground)
    ImageView ivBatteryBackground;

    @ViewById(R.id.rl_activity_newhome_tutorial_actionbar_battery)
    RelativeLayout rlBattery;

    @ViewById(R.id.img_activity_newhome_tutorial_drawer)
    ImageView imgDrawer;

    @ViewById(R.id.bcv_activity_newhome_tutorial_battery)
    BatteryCircleView batteryCircleView;

    @ViewById(R.id.img_activity_newhome_tutorial_battery_led)
    ImageView imgBatteryLed;

    @ViewById(R.id.tv_activity_newhome_tutorial_battery_value)
    TextView tvBatteryValue;

    @ViewById(R.id.rl_activity_newhome_tutorial_actionbar)
    RelativeLayout rlActionBar;

    @ViewById(R.id.rl_activity_newhome_tutorial_mask)
    RelativeLayout rlMask;

    @ViewById(R.id.img_activity_newhome_tutorial_mask)
    ImageView imgMask;

    @ViewById(R.id.img_activity_newhome_tutorial_finger_tap)
    ImageView imgFingerTap;

    @ViewById(R.id.img_activity_newhome_tutorial_finger_swipe)
    ImageView imgFingerSwipe;

    @ViewById(R.id.img_activity_newhome_tutorial_show_draw)
    ImageView imgShowDraw;

    @ViewById(R.id.ll_activity_newhome_tutorial_intro)
    LinearLayout llIntro;

    @ViewById(R.id.tv_activity_newhome_tutorial_intro_title)
    TextView tvTutorialIntroTitle;

    @ViewById(R.id.tv_activity_newhome_tutorial_intro_content)
    TextView tvTutorialIntroContent;

    @ViewById(R.id.sv_activity_newhome_tutorial)
    ScrollView svTutorial;

    @ViewById(R.id.btn_activity_newhome_tutorial_next)
    Button btnNext;

    @ViewById(R.id.rl_acrivity_newhome_tutorial_swipe)
    View swipeView;

    @ViewById(R.id.img_activity_newhome_tutorial_program)
    ImageView imgProgram;

    @ViewById(R.id.img_activity_newhome_tutorial_calendar_arrow)
    ImageView imgCalendarArrow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i("fitmate", "View onCreate()");

        this.setTheme(R.style.OrangeTheme);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics( displayMetrics );
        mScreenWidth = displayMetrics.widthPixels;
        mScreentHeight = displayMetrics.heightPixels;

        this.mCurrentStep = TutorialStep.CATEGORY;

        this.mDBManager = new FitmateDBManager(this);
        this.mUserInfo = this.mDBManager.getUserInfo();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());

        mTodayCalendar = Calendar.getInstance();
        mTodayCalendar.setTimeInMillis( calendar.getTimeInMillis() );

        mDayCalendar = Calendar.getInstance();
        mDayCalendar.setTimeInMillis( calendar.getTimeInMillis() );

        this.mWeekCalendar = Calendar.getInstance();
        this.mWeekCalendar.setTimeInMillis(calendar.getTimeInMillis());
        int weekNumber = mWeekCalendar.get(Calendar.DAY_OF_WEEK);
        mWeekCalendar.add(Calendar.DATE, -(weekNumber - 1));

    }

    @AfterViews
    void init()
    {
        this.newHomeUtils = new NewHomeUtils( this , mDBManager );

        //날짜 관련 뷰
        setDateText();

        rlMask.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });

        //주간 그래프의 크기를 결정한다.
        try {

            Calendar endCalendar = Calendar.getInstance();
            endCalendar.set( mWeekCalendar.get(Calendar.YEAR) , mWeekCalendar.get( Calendar.MONTH ) , mWeekCalendar.get(Calendar.DATE) );
            long diffDates = DateUtils.diffOfDate(endCalendar);
            mWeekPagerLength = ((int) (diffDates / 7) + 1);
        } catch (Exception e) {
            mWeekPagerLength = 1000000;
        }

        //주간 그래프의 주 정보 배열을 초기화한다.
        mWeekPagerActivityInfoList = new WeekPagerActivityInfo[ mWeekPagerLength ];

        setBattery(60);

        display();

        displayNews();

        selectTabStyle();

        swipeDetector = new GestureDetector(this , new SwipeListener());
        swipeView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return swipeDetector.onTouchEvent(event);
            }
        });

        imgMask.post(new Runnable() {
            @Override
            public void run() {
                drawTutorial( );
            }
        });

    }

    private void setBattery( int batteryRatio )
    {
        batteryCircleView.setBatteryValue(batteryRatio);
        batteryCircleView.invalidate();

        if( batteryRatio < 21 ){
            imgBatteryLed.setImageResource(R.drawable.batter_light_empty);
        }else if( batteryRatio < 51 ){
            imgBatteryLed.setImageResource( R.drawable.batter_light_medium );
        }else{
            imgBatteryLed.setImageResource(R.drawable.batter_light_full);
        }

        String batteryValueStrng = "!";
        if( batteryRatio > 0 ){
            batteryValueStrng = batteryRatio + "";
        }
        tvBatteryValue.setText(batteryValueStrng);
    }

    private void setDateText(){

        Date activityDate = mDayCalendar.getTime();
        Date todayDate = new Date();

        String weekDay = null;
        weekDay = this.getResources().getStringArray(R.array.week_string)[mDayCalendar.get(Calendar.DAY_OF_WEEK) - 1];

        tvWeekDay.setText(weekDay);
        tvDate.setText(DateUtils.getDateStringForPattern(mDayCalendar.getTime(), this.getString(R.string.newhome_date_format)));
    }


    void display()
    {
        displayDayData(true);
        displayWeekData();
    }

    private void displayDayData(boolean startZero)
    {
        DayActivity dayActivity = new DayActivity();
        dayActivity.setAverageMET(1.23f);
        dayActivity.setHmlStrengthTime(new int[]{40, 30, 20, 10});
        dayActivity.setCalorieByActivity(400);
        dayActivity.setAchieveOfTarget(3000);
        dayActivity.setPredayScore(15);
        dayActivity.setTodayScore(24);
        dayActivity.setFat(12.3f);
        dayActivity.setCarbohydrate(24.3f);
        dayActivity.setDistance(15.4f);

        ExerciseProgram program = getExerciseProgram();

        if( mInitialDayDisplay ){
            addDayData( dayActivity , program , startZero);
            mInitialDayDisplay = false;
        }else{
            updateDayData(dayActivity, program, startZero);
        }

    }

    private ExerciseProgram getExerciseProgram()
    {

        ExerciseProgram program = null;
        String weekStartDate = newHomeUtils.getWeekStartDate(mDayCalendar);
        WeekActivity weekActivity = mDBManager.getWeekActivity(weekStartDate);

        if( weekActivity != null ) {
            program = mDBManager.getUserExerciProgram(weekActivity.getExerciseProgramID());
        }else{
            program = mDBManager.getAppliedProgram();
        }

        return program;
    }

    private void addDayData( DayActivity dayActivity , ExerciseProgram program , boolean startZero )
    {

        View[] dayGraphs = new View[NewHomeConstant.AllTabViewOrder.length ];

        float lastViewRight = 0;
        float defaultMargin = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 26.9f, getResources().getDisplayMetrics());
        float categoryViewWidth = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 107.5f, getResources().getDisplayMetrics());

        for( int viewIndex = 0 ; viewIndex < NewHomeConstant.AllTabViewOrder.length ;viewIndex++ )
        {
            CategoryType categoryType = NewHomeConstant.AllTabViewOrder[viewIndex];

            NewHomeCategoryView categoryView = NewHomeCategoryView_.build(this);
            View dayView = null;

            if( categoryType.equals( CategoryType.DAILY_ACHIEVE ) ){
                dayView = NewHomeDayHalfCircleView_.build(this);
            }else if(categoryType.equals( CategoryType.WEEKLY_ACHIEVE )){
                dayView = NewHomeDayCircleView_.build(this);
            }else{
                dayView = NewHomeDayTextView_.build(this);
            }

            mUserInfo.setHeight( 170 );
            mUserInfo.setWeight( 70 );

            dayView.setTag(viewIndex);
            categoryView.setOnClickListener(new CategoryClickListener());
            newHomeUtils.setDayValue(categoryView, dayView, NewHomeCategoryData.getNewHomeCategoryData(this, dayActivity, program, mUserInfo), program.getId(), categoryType, startZero);
            newHomeUtils.setCategorySelect(categoryView, viewIndex, selectdCategoryIndex);

            categoryViews[ viewIndex ] = categoryView;
            dayGraphs[viewIndex] = dayView;

            if ( viewIndex > 0)
            {
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
                params.leftMargin = (int) ( lastViewRight - defaultMargin );
                lastViewRight = lastViewRight + ( categoryViewWidth - defaultMargin );
                rlCategory.addView( categoryView , params);
            }else {
                lastViewRight = categoryViewWidth;
                rlCategory.addView(categoryView);
            }
        }


        mDayPagerAdapter = new DayPagerAdapter(this , dayGraphs  );
        vpDay.setAdapter(mDayPagerAdapter);
        vpDay.setCurrentItem(selectdCategoryIndex);
        vpDay.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

            @Override
            public void onPageSelected(int position) {
                newHomeUtils.unSelectCategoryView((NewHomeCategoryView) rlCategory.getChildAt(selectdCategoryIndex));
                int preSelectedCategoryIndex = selectdCategoryIndex;
                selectdCategoryIndex = position;
                NewHomeCategoryView newHomeCategoryView = (NewHomeCategoryView) rlCategory.getChildAt(selectdCategoryIndex);
                newHomeUtils.selectCategoryView(newHomeCategoryView);

                if( preSelectedCategoryIndex > selectdCategoryIndex ){ //카테고리를 왼쪽에서 오른쪽으로 이동하는 경우

                    int viewLeft = newHomeCategoryView.getLeft();
                    if( hsvCategory.getScrollX() > viewLeft )
                    {
                        hsvCategory.smoothScrollTo( viewLeft , 0);
                    }

                }else if(preSelectedCategoryIndex < selectdCategoryIndex){ //카테고리를 오른족에서 왼쪽으로 이동하는 경우

                    int viewRight = newHomeCategoryView.getRight();
                    if( viewRight > ( hsvCategory.getScrollX() + hsvCategory.getRight() ) ) {

                        hsvCategory.smoothScrollTo( viewRight - hsvCategory.getRight()  , 0);
                    }
                }

//                //일 그래프를 애니메이션 시킨다.
                CategoryType preCategoryType = NewHomeConstant.AllTabViewOrder[ preSelectedCategoryIndex ];
                CategoryType categoryType = NewHomeConstant.AllTabViewOrder[ selectdCategoryIndex ];

                // 주 그래프를 선택한다.
                if( categoryType.equals( CategoryType.WEEKLY_ACHIEVE ) )
                {

                    vpWeekBar.setVisibility( View.INVISIBLE );
                    vpWeekTable.setVisibility(View.VISIBLE);
                    mWeekTablePagerAdapter.setDayTimeMilliseconds( mDayCalendar.getTimeInMillis() );
                    mWeekTablePagerAdapter.setSelectedDayIndex( mWeekBarPagerAdapter.getSelectedDayIndex() );

                    if( vpWeekTable.getCurrentItem() != vpWeekBar.getCurrentItem() )
                    {

                        mWeekTablePagerAdapter.setMoveWeek(false);
                        vpWeekTable.setCurrentItem( vpWeekBar.getCurrentItem() );

                    }

                }else if( preCategoryType.equals( CategoryType.WEEKLY_ACHIEVE ) ){

                    vpWeekBar.setVisibility(View.VISIBLE);
                    vpWeekTable.setVisibility(View.INVISIBLE);
                    mWeekBarPagerAdapter.setDayTimeMilliseconds(mDayCalendar.getTimeInMillis());
                    mWeekBarPagerAdapter.setSelectedDayIndex( mWeekTablePagerAdapter.getSelectedDayIndex() );
                    mWeekBarPagerAdapter.changeCategoryType(categoryType);
                    if( vpWeekTable.getCurrentItem() != vpWeekBar.getCurrentItem() ){

                        mWeekBarPagerAdapter.setMoveWeek(false);
                        vpWeekBar.setCurrentItem( vpWeekTable.getCurrentItem() );

                    }

                }else{

                    mWeekBarPagerAdapter.changeCategoryType(categoryType);

                }
            }

            @Override
            public void onPageScrollStateChanged(int state)
            {

            }
        });

    }

    private void updateDayData( DayActivity dayActivity , ExerciseProgram program , boolean startZero ){

        for( int viewIndex = 0 ; viewIndex < NewHomeConstant.AllTabViewOrder.length ; viewIndex++ )
        {
            NewHomeCategoryView categoryView = (NewHomeCategoryView) rlCategory.getChildAt( viewIndex );
            View dayView = mDayPagerAdapter.getDayGraphs()[ viewIndex ];
            newHomeUtils.setDayValue(categoryView, dayView, NewHomeCategoryData.getNewHomeCategoryData(this, dayActivity, program, mUserInfo), program.getId(), NewHomeConstant.AllTabViewOrder[viewIndex], startZero);
        }
    }

    /**
     * 주간 데이터를 그린다.
     */
    private void displayWeekData(){

        if(mInitialWeekDisplay){
            addWeekData();
            mInitialWeekDisplay = false;
        }else{
            updateWeekData();
        }
    }

    private void addWeekData(){

        mWeekBarPagerAdapter = new NewHomeWeekPagerAdapter( this, mWeekPagerLength , NewHomeConstant.AllTabViewOrder[selectdCategoryIndex] , new WeekCellClickListener() , this , mDayCalendar.getTimeInMillis() , WeekPagerAdapter.WeekPagerType.WEEK_BAR , mWeekPagerActivityInfoList );
        mWeekBarPagerAdapter.setMoveWeek( false );
        vpWeekBar.addOnPageChangeListener(mWeekBarPagerAdapter);
        vpWeekBar.setAdapter(mWeekBarPagerAdapter);
        vpWeekBar.setCurrentItem(mWeekPagerLength);

        mWeekTablePagerAdapter = new NewHomeWeekPagerAdapter(this , mWeekPagerLength , CategoryType.WEEKLY_ACHIEVE , new WeekCellClickListener() , this , mDayCalendar.getTimeInMillis() , WeekPagerAdapter.WeekPagerType.WEEK_TABLE , mWeekPagerActivityInfoList);
        mWeekTablePagerAdapter.setMoveWeek( false );
        vpWeekTable.addOnPageChangeListener(mWeekTablePagerAdapter);
        vpWeekTable.setAdapter(mWeekTablePagerAdapter);
        vpWeekTable.setCurrentItem(mWeekPagerLength);

        CategoryType categoryType = NewHomeConstant.AllTabViewOrder[ selectdCategoryIndex ];
        if( categoryType.equals( CategoryType.WEEKLY_ACHIEVE ) ){

            vpWeekBar.setVisibility(View.INVISIBLE);
            vpWeekTable.setVisibility(View.VISIBLE);

        }else{

            vpWeekBar.setVisibility( View.VISIBLE );
            vpWeekTable.setVisibility(View.INVISIBLE);

        }
    }

    private int getWeekDiff(){

        Calendar weekCalendar = Calendar.getInstance();
        weekCalendar.setTimeInMillis(mDayCalendar.getTimeInMillis());
        int weekNumber = weekCalendar.get(Calendar.DAY_OF_WEEK);
        weekCalendar.add(Calendar.DATE, -(weekNumber - 1));

        long diff = mWeekCalendar.getTimeInMillis() - weekCalendar.getTimeInMillis();
        long diffDays = diff / (24 * 60 * 60 * 1000);

        int weekDiff = (int) (diffDays / 7);

        return mWeekPagerLength - ( weekDiff + 1 );
    }

    private void updateWeekData()
    {
        int position = getWeekDiff();
        int selectedDayIndex = mDayCalendar.get( Calendar.DAY_OF_WEEK );

        if( vpWeekBar.getVisibility() == View.VISIBLE ) {

            mWeekBarPagerAdapter.setDayTimeMilliseconds(mDayCalendar.getTimeInMillis());
            mWeekBarPagerAdapter.setSelectedDayIndex(selectedDayIndex - 1);
            mWeekBarPagerAdapter.setMoveWeek(false);
            vpWeekBar.setCurrentItem(position);

        }else if( vpWeekTable.getVisibility()  == View.VISIBLE ) {

            mWeekTablePagerAdapter.setDayTimeMilliseconds(mDayCalendar.getTimeInMillis());
            mWeekTablePagerAdapter.setSelectedDayIndex(selectedDayIndex - 1);
            mWeekTablePagerAdapter.setMoveWeek(false);
            vpWeekTable.setCurrentItem(position);

        }
    }

    @UiThread
    void displayNews(){


        NewHomeNewsView newHomeNewsView = NewHomeNewsView_.build(this);
        newHomeNewsView.setProfileImage( NetworkClass.imageBaseURL + mUserInfo.getAccountPhoto() );
        newHomeNewsView.setContent(strTodayAchieveSuccess);
        SimpleDateFormat dateFormat = new SimpleDateFormat( this.getString(R.string.news_pastday_dateformat));
        newHomeNewsView.setDate( dateFormat.format( new Date() ) );

        View[] newsViews = new View[]{ newHomeNewsView };
        NewsPagerAdapter newsPagerAdapter = new NewsPagerAdapter(this , newsViews);
        vpNews.setAdapter(newsPagerAdapter);
    }


    private void showBatteryView(){
        rlBattery.setVisibility(View.VISIBLE);
    }

    @Override
    public void onWeekMove( long dayMilliseconds , int position , WeekPagerAdapter.WeekPagerType weekPagerType ) {
        //주간 그래프에 따라 일 날짜를 변경시킨다.
        mDayCalendar.setTimeInMillis( dayMilliseconds );

        //일 그래프를 다시 그린다.
        setDateText();
        displayDayData(false);

    }

    private class CategoryClickListener implements View.OnClickListener
    {
        @Override
        public void onClick(View view)
        {
            vpDay.setCurrentItem( (int) view.getTag() );
        }
    }

    private class WeekCellClickListener implements View.OnClickListener
    {
        @Override
        public void onClick(View view) {

            long selectedDayTimeMillis = (long) view.getTag( R.string.newhome_weekview_time_key );
            int index = (int) view.getTag(R.string.newhome_weekview_cellindex_key);
            mDayCalendar.setTimeInMillis(selectedDayTimeMillis);

            setDateText();
            displayDayData(false);

            if( vpWeekBar.getVisibility() == View.VISIBLE ) {
                mWeekBarPagerAdapter.setDayTimeMilliseconds(selectedDayTimeMillis);
                mWeekBarPagerAdapter.setSelectedDayIndex(index);
            }else if(vpWeekTable.getVisibility() == View.VISIBLE) {
                mWeekTablePagerAdapter.setDayTimeMilliseconds( selectedDayTimeMillis );
                mWeekTablePagerAdapter.setSelectedDayIndex(index);
            }
        }
    }

    private class NewHomeWeekPagerAdapter extends WeekPagerAdapter
    {

        private String[] mWeekStringArray = null;
        private HashMap<String , View> mViewMap = new HashMap< String , View >();

        public NewHomeWeekPagerAdapter(Context context, int weekPagerLength, CategoryType categoryType , View.OnClickListener weekCellClickListener , WeekMoveListener weekMoveListener , long dayMilliseconds , WeekPagerType weekPagerType , WeekPagerActivityInfo[] weekPagerActivityInfoList) {

            super(context, weekPagerLength, categoryType, weekCellClickListener, weekMoveListener, dayMilliseconds, weekPagerType, weekPagerActivityInfoList);
            mWeekStringArray = new String[ mWeekPagerLength ];

        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            View view = null;

            WeekPagerActivityInfo weekActivityInfo = null;
            if (mWeekPagerActivityInfoList[position] == null) {

                weekActivityInfo = getWeekPagerActivityInfo(position);
                mWeekPagerActivityInfoList[position] = weekActivityInfo;

            } else {

                weekActivityInfo = mWeekPagerActivityInfoList[position];
            }

            view = getWeekGraphView(weekActivityInfo , position);
            addItem(weekActivityInfo.getWeekStartDate(), view);

            container.addView(view);

            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object){

            removeItem(position, object);
            container.removeView((View) object);

        }


        private WeekPagerActivityInfo getWeekPagerActivityInfo(int position) {

            int weekIndex = mPagerLength - (position + 1);

            Calendar weekCalendar = Calendar.getInstance();
            weekCalendar.setTimeInMillis( mWeekCalendar.getTimeInMillis() );
            weekCalendar.add(Calendar.DATE, -(weekIndex * 7));
            String weekStartDate = NewHomeUtils.getDateString(weekCalendar);

            mWeekStringArray[position] = weekStartDate;
            WeekActivity weekActivity = mDBManager.getWeekActivity(weekStartDate);

            String weekRange = "";
            double[] exerciseTimeValues = new double[]{ 0 , 0 , 0 , 0 , 0 , 0 , 0 };
            double[] calorieValues = new double[]{ 0 , 0 , 0 , 0 , 0 , 0 , 0 };
            double[] dailyAchieveValues = new double[]{ 1 , 5 , 2 , 1 , 6 , 2 , 3 };
            double[] weeklyAchieveValues = new double[]{ 0 , 0 , 0 , 0 , 0 , 0 , 0 };
            double[] fatValues = new double[]{ 0 , 0 , 0 , 0 , 0 , 0 , 0 };
            double[] carboHydrateValues = new double[]{ 0 , 0 , 0 , 0 , 0 , 0 , 0 };
            double[] distanceValues = new double[]{ 0 , 0 , 0 , 0 , 0 , 0 , 0 };

            String[] dayStrings = new String[7];
            long[] timeMillis = new long[7];
            int selectedDayIndex = -1;
            int preMonth = -1;

            for (int i = 0; i < dailyAchieveValues.length ; i++)
            {
                timeMillis[i] = weekCalendar.getTimeInMillis();

                int currentMonth = weekCalendar.get(Calendar.MONTH);
                if( preMonth != currentMonth ) {
                    dayStrings[i] = DateUtils.getDateStringForPattern(weekCalendar.getTime(), getResources().getString(R.string.newhome_week_bar_date_format));
                    preMonth = currentMonth;
                }else{
                    dayStrings[i] = DateUtils.getDateStringForPattern(weekCalendar.getTime(), getResources().getString(R.string.newhome_week_bar_date_simple_format));
                }

                if( i == 0 ){
                    weekRange += DateUtils.getDateStringForPattern( weekCalendar.getTime() , getResources().getString( R.string.newhome_week_table_week_range_format ) );
                }

                if( i == dailyAchieveValues.length -1 ){
                    weekRange += " - " + DateUtils.getDateStringForPattern( weekCalendar.getTime() , getResources().getString( R.string.newhome_week_table_week_range_format ) );
                }

                weekCalendar.add(Calendar.DATE, 1);
            }

            float strengthFrom = 0;
            float strengthTo = 0;

            ExerciseProgram program = mDBManager.getAppliedProgram();;

            StrengthInputType strengthType = StrengthInputType.getStrengthType( program.getCategory() );
            if( strengthType.equals( StrengthInputType.CALORIE ) || strengthType.equals( StrengthInputType.CALORIE_SUM )|| strengthType.equals(StrengthInputType.VS_BMR))
            {

                UserInfoForAnalyzer userInfoForAnalyzer = com.fitdotlife.fitmate_lib.service.util.Utils.getUserInfoForAnalyzer(mUserInfo);

                strengthFrom= (int) program.getStrengthFrom();
                strengthTo= (int) program.getStrengthFrom();
                int bmr = userInfoForAnalyzer.getBMR();

                if(strengthType.equals(StrengthInputType.VS_BMR)){
                    strengthFrom=(int)Math.round(strengthFrom /100.0 * bmr);
                    strengthTo=(int)Math.round(strengthTo /100.0 * bmr);
                }

            }else{
                strengthFrom = program.getMinuteFrom();
                strengthTo = program.getMinuteTo();
            }

            WeekPagerActivityInfo weekActivityInfo = new WeekPagerActivityInfo( weekStartDate , weekRange , dayStrings , timeMillis , preMonth , strengthFrom , strengthTo
                    ,exerciseTimeValues , calorieValues , dailyAchieveValues , weeklyAchieveValues , fatValues , carboHydrateValues , distanceValues);

            return weekActivityInfo;
        }


        private void addItem( String weekStartDate , View view ) {
            mViewMap.put(weekStartDate, view);
        }

        @Override
        protected void getWeekActivityInfo(int position) {

            int weekIndex = mPagerLength - (position + 1);

            Calendar weekCalendar = Calendar.getInstance();
            weekCalendar.setTimeInMillis(mWeekCalendar.getTimeInMillis());
            weekCalendar.add(Calendar.DATE, -(weekIndex * 7));
            String weekStartDate = newHomeUtils.getDateString(weekCalendar);

            if( mWeekPagerActivityInfoList[position] == null ){

                mWeekPagerActivityInfoList[position] = getWeekPagerActivityInfo( position );

            }

        }

        private void removeItem(int position, Object object) {
            mViewMap.remove(mWeekStringArray[position]);
            mWeekStringArray[ position ] = null;
        }


        @Override
        public void OnWeekActivityReceived(FriendWeekActivity friendWeekActivity) {

        }

        @Override
        public void OnWeekActivityReceived(FriendWeekActivity[] friendWeekActivityList) {

        }
    }

    private Bitmap drawCirleHole(float centerX , float centerY , float radius)
    {

        Bitmap bitmap = Bitmap.createBitmap( imgMask.getMeasuredWidth() , imgMask.getMeasuredHeight()  , Bitmap.Config.ARGB_8888 );

        bitmap.setDensity(this.getResources().getDisplayMetrics().densityDpi);
        Canvas canvas = new Canvas(bitmap);

        canvas.drawColor( this.getResources().getColor(R.color.newhome_tutorial_background) );

        Paint xferModePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        xferModePaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));

        canvas.drawCircle(centerX, centerY, radius, xferModePaint);

        return bitmap;
    }

    private Bitmap drawRectangleHole(float left , float top , float right , float bottom )
    {
        Bitmap bitmap = Bitmap.createBitmap( imgMask.getMeasuredWidth() , imgMask.getMeasuredHeight()  , Bitmap.Config.ARGB_8888 );

        bitmap.setDensity(this.getResources().getDisplayMetrics().densityDpi);
        Canvas canvas = new Canvas(bitmap);

        canvas.drawColor(this.getResources().getColor(R.color.newhome_tutorial_background));

        Paint xferModePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        xferModePaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));

        canvas.drawRect(left, top, right, bottom, xferModePaint);

        return bitmap;
    }

    private Bitmap drawRoundedRectangle( float left , float top , float right , float bottom , float radius ){

        Bitmap bitmap = Bitmap.createBitmap( imgMask.getMeasuredWidth() , imgMask.getMeasuredHeight()  , Bitmap.Config.ARGB_8888 );

        bitmap.setDensity(this.getResources().getDisplayMetrics().densityDpi);
        Canvas canvas = new Canvas(bitmap);

        canvas.drawColor(this.getResources().getColor(R.color.newhome_tutorial_background));

        Paint xferModePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        xferModePaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));

        RectF rect = new RectF( left , top , right , bottom );
        canvas.drawRoundRect( rect , radius, radius , xferModePaint );

        return bitmap;
    }

    private Bitmap drawDayHole( float upperCircleCenterX , float upperCircleCenterY , float upperCircleRadius ,
                                float centerRectangleLeft , float centerRectangleTop , float centerRectangleRight , float centerRectangleBottom ,
                                float lowerCircleCenterX , float lowerCircleCenterY , float lowerCircleRadius){
        Bitmap bitmap = Bitmap.createBitmap( imgMask.getMeasuredWidth() , imgMask.getMeasuredHeight()  , Bitmap.Config.ARGB_8888 );

        bitmap.setDensity(this.getResources().getDisplayMetrics().densityDpi);
        Canvas canvas = new Canvas(bitmap);

        canvas.drawColor(this.getResources().getColor(R.color.newhome_tutorial_background));

        Paint xferModePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        xferModePaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));

        canvas.drawCircle(upperCircleCenterX, upperCircleCenterY, upperCircleRadius, xferModePaint);
        canvas.drawRect(centerRectangleLeft, centerRectangleTop, centerRectangleRight, centerRectangleBottom, xferModePaint);
        canvas.drawCircle(lowerCircleCenterX, lowerCircleCenterY, lowerCircleRadius, xferModePaint);

        return bitmap;
    }


    private float convertDPToPx( float dpValue )
    {
        return TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP , dpValue , this.getResources().getDisplayMetrics() );
    }

    private void drawDrawerTutorial(){

        drawerView.setVisibility( View.INVISIBLE );
        imgShowDraw.setVisibility( View.INVISIBLE );
        imgFingerTap.setVisibility( View.VISIBLE );
        tvTutorialIntroTitle.setVisibility( View.VISIBLE );

        int[] initPoint = new int[2];
        int[] viewPoint = new int[2];
        imgDrawer.getLocationOnScreen(viewPoint);
        rlActionBar.getLocationInWindow(initPoint);

        float centerX = viewPoint[0] + (imgDrawer.getMeasuredWidth() / 2);
        float centerY = viewPoint[1] + (imgDrawer.getMeasuredHeight() / 2) - initPoint[1];

        float radius = convertDPToPx(77) / 2;
        Bitmap bitmap = drawCirleHole(centerX, centerY, radius);
        imgMask.setImageBitmap(bitmap);

        //손을 그린다.
        imgFingerTap.setImageResource( R.drawable.finger_tap );
        RelativeLayout.LayoutParams fingerTapParams = (RelativeLayout.LayoutParams) imgFingerTap.getLayoutParams();
        fingerTapParams.leftMargin = (int) convertDPToPx(253);
        fingerTapParams.topMargin = viewPoint[1] + imgDrawer.getMeasuredHeight() - initPoint[1];
        imgFingerTap.setLayoutParams(fingerTapParams);

        //텍스트 내용을 그린다.
        tvTutorialIntroTitle.setText( this.getResources().getString( R.string.tutorial_menu_title) );
        tvTutorialIntroContent.setText( this.getResources().getString(R.string.tutorial_menu_content) );

        RelativeLayout.LayoutParams introParams = (RelativeLayout.LayoutParams) llIntro.getLayoutParams();
        introParams.topMargin = (int) convertDPToPx( 182 );
        llIntro.setLayoutParams( introParams );
    }

    private void drawBatteryTutorial(){

        imgFingerTap.setVisibility(View.VISIBLE);
        imgFingerSwipe.setVisibility(View.INVISIBLE);
        imgShowDraw.setVisibility( View.INVISIBLE );

        int[] initPoint = new int[2];
        int[] viewPoint = new int[2];
        rlBattery.getLocationOnScreen(viewPoint);
        rlActionBar.getLocationInWindow(initPoint);

        float centerX = viewPoint[0] + (rlBattery.getMeasuredWidth() / 2);
        float centerY = viewPoint[1] + (rlBattery.getMeasuredHeight() / 2) - initPoint[1];

        float radius = convertDPToPx(77) / 2;
        Bitmap bitmap = drawCirleHole(centerX, centerY, radius);
        imgMask.setImageBitmap(bitmap);

        //손을 그린다.
        imgFingerTap.setImageBitmap( flip(BitmapFactory.decodeResource(this.getResources(), R.drawable.finger_tap)) );
        RelativeLayout.LayoutParams fingerTapParams = (RelativeLayout.LayoutParams) imgFingerTap.getLayoutParams();
        fingerTapParams.leftMargin = (int) convertDPToPx(28.67f);
        fingerTapParams.topMargin = (int) convertDPToPx(31.67f);
        imgFingerTap.setLayoutParams(fingerTapParams);

        //텍스트 내용을 그린다.
        tvTutorialIntroTitle.setText( this.getResources().getString( R.string.tutorial_battery_title) );
        tvTutorialIntroContent.setText( this.getResources().getString(R.string.tutorial_battery_content) );

        RelativeLayout.LayoutParams introParams = (RelativeLayout.LayoutParams) llIntro.getLayoutParams();
        introParams.topMargin = (int) ( convertDPToPx(31.67f) + imgFingerTap.getMeasuredHeight() + convertDPToPx(16.67f));
        llIntro.setLayoutParams( introParams );
    }

    private void drawCategoryTutorial(){

        drawerView.setVisibility(View.INVISIBLE);
        hsvCategory.setEnabled(false);
        imgFingerTap.setVisibility(View.INVISIBLE);
        imgShowDraw.setVisibility(View.INVISIBLE);

        int[] initPoint = new int[2];
        int[] viewPoint = new int[2];
        hsvCategory.getLocationOnScreen(viewPoint);
        rlActionBar.getLocationInWindow(initPoint);

        float left = viewPoint[0];
        float top = viewPoint[1] - initPoint[1];
        float right = left + hsvCategory.getMeasuredWidth();
        float bottom = top + hsvCategory.getMeasuredHeight();

        Bitmap bitmap = drawRectangleHole(left, top, right, bottom);
        imgMask.setImageBitmap(bitmap);

        //손을 그린다.
        RelativeLayout.LayoutParams fingerSwipeParams = (RelativeLayout.LayoutParams) imgFingerSwipe.getLayoutParams();
        fingerSwipeParams.leftMargin = (int) (right / 2) - ( imgFingerSwipe.getMeasuredWidth() / 2 );
        int fingerSwipeTop = (int) (bottom - convertDPToPx( 56.33f ));
        fingerSwipeParams.topMargin = fingerSwipeTop;
        imgFingerSwipe.setLayoutParams(fingerSwipeParams);

        //텍스트 내용을 그린다.
        tvTutorialIntroTitle.setText( this.getResources().getString( R.string.tutorial_navitab_title) );
        tvTutorialIntroContent.setText( this.getResources().getString(R.string.tutorial_navitab_content) );

        RelativeLayout.LayoutParams introParams = (RelativeLayout.LayoutParams) llIntro.getLayoutParams();
        introParams.topMargin = (int) (fingerSwipeTop + imgFingerSwipe.getMeasuredHeight() + convertDPToPx(18.67f));
        llIntro.setLayoutParams( introParams );
    }

    private void drawDayTutorial(){

        svTutorial.scrollTo(0, 0);

        int[] initPoint = new int[2];
        int[] viewPoint = new int[2];
        int[] circlePoint = new int[2];
        rlActionBar.getLocationInWindow(initPoint);

        //반지름 구하기
        DayPagerAdapter dayPagerAdapter = (DayPagerAdapter) vpDay.getAdapter();
        View dayView = dayPagerAdapter.getDayGraphs()[selectdCategoryIndex];

        RelativeLayout rlDayHalfCircle = (RelativeLayout) dayView.findViewById(R.id.rl_child_newhome_day_halfcircle);
        rlDayHalfCircle.getLocationOnScreen(circlePoint);

        vpDay.getLocationOnScreen(viewPoint);

        float top = viewPoint[1] - initPoint[1];
        float centerX = circlePoint[0] + (rlDayHalfCircle.getMeasuredWidth() / 2);
        float centerY = circlePoint[1] + (rlDayHalfCircle.getMeasuredHeight() / 2) - initPoint[1];
        float radius = centerY - top;

        Bitmap bitmap = drawCirleHole( centerX , centerY , radius );
        imgMask.setImageBitmap(bitmap);

        imgFingerTap.setVisibility(View.INVISIBLE);
        imgShowDraw.setVisibility(View.INVISIBLE);

//        //손을 그린다.
        RelativeLayout.LayoutParams fingerSwipeParams = (RelativeLayout.LayoutParams) imgFingerSwipe.getLayoutParams();
        fingerSwipeParams.leftMargin = (int) (centerX) - ( imgFingerSwipe.getMeasuredWidth() / 2 );
        float fingerSwipeTop = centerY;
        fingerSwipeParams.topMargin = (int) fingerSwipeTop;
        imgFingerSwipe.setLayoutParams(fingerSwipeParams);

//        //텍스트 내용을 그린다.
        tvTutorialIntroTitle.setText( this.getResources().getString( R.string.tutorial_dayactivity_title) );
        tvTutorialIntroContent.setText( this.getResources().getString(R.string.tutorial_dayactivity_content) );

        RelativeLayout.LayoutParams introParams = (RelativeLayout.LayoutParams) llIntro.getLayoutParams();
        introParams.topMargin = (int) (top - llIntro.getMeasuredHeight() - convertDPToPx(24.33f));
        llIntro.setLayoutParams( introParams );
    }

    private void drawWeekTutorial()
    {
        svTutorial.scrollTo(0, rlWeek.getBottom());

        int[] initPoint = new int[2];
        int[] viewPoint = new int[2];
        rlWeek.getLocationOnScreen(viewPoint);
        rlActionBar.getLocationInWindow(initPoint);

        float left = viewPoint[0];
        final float top = viewPoint[1] - initPoint[1];
        float right = left + rlWeek.getMeasuredWidth();
        float bottom = top + rlWeek.getMeasuredHeight();

        Bitmap bitmap = drawRectangleHole(left, top, right, bottom);
        imgMask.setImageBitmap(bitmap);

        imgFingerTap.setVisibility(View.INVISIBLE);

        //손을 그린다.
        RelativeLayout.LayoutParams fingerSwipeParams = (RelativeLayout.LayoutParams) imgFingerSwipe.getLayoutParams();
        fingerSwipeParams.leftMargin = (int) (right / 2) - ( imgFingerSwipe.getMeasuredWidth() / 2 );
        int fingerSwipeTop = (int) (bottom - ( rlWeek.getMeasuredHeight() /2 ) );
        fingerSwipeParams.topMargin = fingerSwipeTop;
        imgFingerSwipe.setLayoutParams(fingerSwipeParams);

        //텍스트 내용을 그린다.
        tvTutorialIntroTitle.setText(this.getResources().getString(R.string.tutorial_moveweek_title));
        tvTutorialIntroContent.setText( this.getResources().getString(R.string.tutorial_moveweek_content) );
        llIntro.post(new Runnable() {
            @Override
            public void run() {
                RelativeLayout.LayoutParams introParams = (RelativeLayout.LayoutParams) llIntro.getLayoutParams();
                introParams.topMargin = (int) (top - convertDPToPx(18.67f) - llIntro.getMeasuredHeight());
                llIntro.setLayoutParams(introParams);
            }
        });
    }

    private void drawNewsTutorial(){
        svTutorial.scrollTo(0, vpNews.getBottom());

        int[] initPoint = new int[2];
        int[] viewPoint = new int[2];
        vpNews.getLocationOnScreen(viewPoint);
        rlActionBar.getLocationInWindow(initPoint);

        float left = viewPoint[0];
        final float top = viewPoint[1] - initPoint[1];
        float right = left + vpNews.getMeasuredWidth();
        float bottom = top + vpNews.getMeasuredHeight();

        Bitmap bitmap = drawRectangleHole(left, top, right, bottom);
        imgMask.setImageBitmap(bitmap);

        imgFingerTap.setVisibility(View.INVISIBLE);

        //손을 그린다.
        Bitmap fingerSwipeBitmap =  BitmapFactory.decodeResource( this.getResources() , R.drawable.finger_swipe );

        float bitmapWidth =  convertDPToPx(69);
        float bitmapHeight = convertDPToPx( 114.33f );

        Bitmap sizingBitmap  = Bitmap.createScaledBitmap(fingerSwipeBitmap , (int)bitmapWidth , (int)bitmapHeight , true);

        RelativeLayout.LayoutParams fingerSwipeParams = new RelativeLayout.LayoutParams( (int)bitmapWidth ,  (int)bitmapHeight );
        fingerSwipeParams.leftMargin = (int) (right / 2) - ( imgFingerSwipe.getMeasuredWidth() / 2 );
        fingerSwipeParams.topMargin = (int) (top + convertDPToPx( 29 ));
        imgFingerSwipe.setLayoutParams(fingerSwipeParams);
        imgFingerSwipe.setImageBitmap(sizingBitmap);

        //텍스트 내용을 그린다.
        tvTutorialIntroTitle.setText(this.getResources().getString(R.string.tutorial_news_title));
        tvTutorialIntroContent.setText( this.getResources().getString(R.string.tutorial_news_content) );

        llIntro.post(new Runnable() {
            @Override
            public void run() {
                RelativeLayout.LayoutParams introParams = (RelativeLayout.LayoutParams) llIntro.getLayoutParams();
                introParams.topMargin = (int) (top - convertDPToPx(18.67f) - llIntro.getMeasuredHeight());
                llIntro.setLayoutParams(introParams);
            }
        });
    }

    private void drawProgramTutorial(){

        svTutorial.scrollTo(0,0);

        int[] initPoint = new int[2];
        int[] viewPoint = new int[2];

        imgProgram.getLocationOnScreen(viewPoint);
        rlActionBar.getLocationInWindow(initPoint);

        float centerX = viewPoint[0] + (imgProgram.getMeasuredWidth() / 2);
        float centerY = viewPoint[1] + (imgProgram.getMeasuredHeight() / 2) - initPoint[1];
        float radius = convertDPToPx(77) / 2;
        Bitmap bitmap = drawCirleHole(centerX, centerY, radius);
        imgMask.setImageBitmap(bitmap);

        imgFingerSwipe.setVisibility(View.INVISIBLE);

        //손을 그린다.
        imgFingerTap.setImageResource( R.drawable.finger_tap );
        float top = centerY - radius;
        RelativeLayout.LayoutParams fingerTapParams = (RelativeLayout.LayoutParams) imgFingerTap.getLayoutParams();
        fingerTapParams.leftMargin = (int) (mScreenWidth - convertDPToPx(41.33f)-imgFingerTap.getMeasuredWidth());
        fingerTapParams.topMargin = (int) (top + convertDPToPx(63));
        imgFingerTap.setLayoutParams(fingerTapParams);

        //텍스트 내용을 그린다.
        tvTutorialIntroTitle.setText( this.getResources().getString( R.string.tutorial_exprogram_title) );
        tvTutorialIntroContent.setText( this.getResources().getString(R.string.tutorial_exprogram_content) );

        RelativeLayout.LayoutParams introParams = (RelativeLayout.LayoutParams) llIntro.getLayoutParams();
        introParams.topMargin = (int) (centerY + radius + imgFingerTap.getMeasuredHeight() + convertDPToPx(3.67f));
        llIntro.setLayoutParams( introParams );
    }

    private void drawDateTutorial(  ){

        imgFingerSwipe.setVisibility(View.INVISIBLE);
        imgFingerTap.setVisibility(View.VISIBLE);

        int[] initPoint = new int[2];
        int[] calendarPoint = new int[2];
        int[] arrowPoint = new int[2];
        llCalendar.getLocationOnScreen( calendarPoint );
        imgArrow.getLocationOnScreen(arrowPoint);
        rlActionBar.getLocationInWindow(initPoint);

        float left = mScreenWidth / 3;
        float top = calendarPoint[1] - initPoint[1];
        float right = arrowPoint[0] + imgArrow.getMeasuredWidth();
        float bottom = top + llCalendar.getMeasuredHeight();

        //Bitmap bitmap = drawRectangleHole(left, top, right, bottom);
        float radius = TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP , 5 , getResources().getDisplayMetrics() );
        Bitmap bitmap = drawRoundedRectangle(left, top, right, bottom, radius);
        imgMask.setImageBitmap(bitmap);

        //손을 그린다.
        imgFingerTap.setImageResource( R.drawable.finger_tap );
        RelativeLayout.LayoutParams fingerTapParams = (RelativeLayout.LayoutParams) imgFingerTap.getLayoutParams();
        fingerTapParams.leftMargin = (int) ( left + convertDPToPx(7) );
        fingerTapParams.topMargin = (int) (top + convertDPToPx(35));
        imgFingerTap.setLayoutParams(fingerTapParams);

        //텍스트 내용을 그린다.
        tvTutorialIntroTitle.setText( this.getResources().getString( R.string.tutorial_selectdate_title) );
        tvTutorialIntroContent.setText( this.getResources().getString(R.string.tutorial_selectdate_content) );

        RelativeLayout.LayoutParams introParams = (RelativeLayout.LayoutParams) llIntro.getLayoutParams();
        introParams.topMargin = (int) (top + convertDPToPx(35) + imgFingerTap.getMeasuredHeight() + convertDPToPx(21));
        llIntro.setLayoutParams( introParams );

    }

    private void drawDrawerHomeTutorial()
    {

        drawerView.setVisibility(View.VISIBLE);
        drawDrawerContentTutorial(R.id.ll_child_drawer_layout_home, R.string.tutorial_menu_home);

//        Animation rightInAnimation = AnimationUtils.loadAnimation(this , R.anim.left_slide);
//        rightInAnimation.setAnimationListener(new Animation.AnimationListener() {
//            @Override
//            public void onAnimationStart(Animation animation) { }
//
//            @Override
//            public void onAnimationEnd(Animation animation) {
//
//
//            }
//
//            @Override
//            public void onAnimationRepeat(Animation animation) { }
//        });
//
//        drawerView.startAnimation( rightInAnimation );

    }

//    private void drawDrawerActivityTutorial()
//    {
//        drawDrawerContentTutorial(R.id.ll_child_drawer_layout_activity, R.string.tutorial_menu_activity);
//    }

    private void drawDrawerExerciseProgramTutorial()
    {
        drawDrawerContentTutorial(R.id.ll_child_drawer_layout_exercise_program, R.string.tutorial_menu_exeprogram);
    }

    private void drawDrawerFriendTutorial()
    {
        drawDrawerContentTutorial( R.id.ll_child_drawer_layout_friend , R.string.tutorial_menu_friend);
    }
    private void drawDrawerNewsTutorial(){
        drawDrawerContentTutorial( R.id.ll_child_drawer_layout_news , R.string.tutorial_menu_news);
    }



    private void drawDrawerContentTutorial( int drawerContentID , int tutorialContentID )
    {
        imgFingerTap.setVisibility(View.INVISIBLE);
        imgFingerSwipe.setVisibility(View.INVISIBLE);

        int[] initPoint = new int[2];
        int[] drawerPoint = new int[2];
        int[] viewPoint = new int[2];

        LinearLayout llDrawerContent = (LinearLayout) drawerView.findViewById( drawerContentID );

        llDrawerContent.getLocationOnScreen(viewPoint);
        drawerView.getLocationOnScreen( drawerPoint );
        rlActionBar.getLocationInWindow(initPoint);

        float left = drawerPoint[0];
        float top = viewPoint[1] - initPoint[1];
        float right = left + drawerView.getMeasuredWidth();
        float bottom = top + llDrawerContent.getMeasuredHeight();

        Bitmap bitmap = drawRectangleHole(left, top, right, bottom);
        imgMask.setImageBitmap(bitmap);

        RelativeLayout.LayoutParams showDrawParams = (RelativeLayout.LayoutParams) imgShowDraw.getLayoutParams();
        showDrawParams.leftMargin = (int) (right - imgShowDraw.getMeasuredWidth());
        showDrawParams.topMargin = (int) top;
        imgShowDraw.setLayoutParams(showDrawParams);

        imgShowDraw.setVisibility(View.VISIBLE);

        //텍스트 내용을 그린다.
        tvTutorialIntroTitle.setVisibility(View.GONE);
        tvTutorialIntroContent.setText( this.getResources().getString(tutorialContentID) );

        RelativeLayout.LayoutParams introParams = (RelativeLayout.LayoutParams) llIntro.getLayoutParams();
        introParams.topMargin = (int) (top + imgShowDraw.getMeasuredHeight() + convertDPToPx(14.67f));
        llIntro.setLayoutParams( introParams );
    }

    private void drawTutorial(  ){

        switch( mCurrentStep ){

            case CATEGORY:
                drawCategoryTutorial();
                break;
            case DAY:
                drawDayTutorial();
                break;
            case WEEK:
                drawWeekTutorial();
                break;
            case NEWS:
                drawNewsTutorial();
                break;
            case DATE:
                drawDateTutorial();
                break;
            case EXERCISEPROGRAM_ICON:
                drawProgramTutorial();
                break;
            case BATTERY:
                drawBatteryTutorial();
                break;
            case DRAWER:
                drawDrawerTutorial();
                break;
            case DRAWER_HOME:
                drawDrawerHomeTutorial();
                break;
//            case DRAWER_ACTIVITY:
//                drawDrawerActivityTutorial();
//                break;
            case DRAWER_EXERCISEPROGRAM:
                drawDrawerExerciseProgramTutorial();
                break;
            case DRAWER_FRIEND:
                drawDrawerFriendTutorial();
                break;
            case DRAWER_NEWS:
                drawDrawerNewsTutorial();
                break;
        }
    }

    @Click(R.id.btn_activity_newhome_tutorial_next)
    void nextTutorial()
    {
        moveNext();
    }

    @Click(R.id.btn_activity_newhome_tutorial_already_know)
    void alreadyKonwClick(){
        SharedPreferences pref = getSharedPreferences( "fitmateservice" , Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean( HelpActivity.HOME_NOT_VIEW_KEY, true );
        editor.commit();

        finish();
    }

    private Bitmap flip( Bitmap src){
        Matrix m = new Matrix();
        m.preScale(-1, 1);
        Bitmap dst = Bitmap.createBitmap(src, 0, 0, src.getWidth(), src.getHeight(), m, false);
        dst.setDensity(DisplayMetrics.DENSITY_DEFAULT);
        return dst;
    }

    private void moveNext(){

        mCurrentStep = TutorialStep.values()[ mCurrentStep.ordinal() + 1 ];

        if( mCurrentStep.equals( TutorialStep.CLOSE ) ){
            finish();
        }else{

            drawTutorial();

            if( mCurrentStep.equals( TutorialStep.DRAWER_NEWS ) ){
                btnNext.setText( this.getString( R.string.tutorial_button_end ) );
            }
        }

    }

    private void movePrevious(){

        if( !mCurrentStep.equals( TutorialStep.CATEGORY ) )
        {
            mCurrentStep = TutorialStep.values()[mCurrentStep.ordinal() - 1];
            drawTutorial();
        }
    }

    private void onSwipeRight(){
        movePrevious();
    }

    private void onSwipeLeft(){
        moveNext();
    }

    private class SwipeListener extends GestureDetector.SimpleOnGestureListener{

        private static final int SWIPE_THRESHOLD = 100;
        private static final int SWIPE_VELOCITY_THRESHOLD = 100;

        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            boolean result = false;
            try {
                float diffY = e2.getY() - e1.getY();
                float diffX = e2.getX() - e1.getX();

                if (Math.abs(diffX) > Math.abs(diffY)) {
                    if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                        if (diffX > 0) {
                            onSwipeRight();
                        } else {
                            onSwipeLeft();
                        }
                    }
                    result = true;
                }

                result = true;

            } catch (Exception exception) {
                exception.printStackTrace();
            }
            return result;
        }
    }

    private void selectTabStyle()
    {

        //튜토리얼은 데이 테마로 가기로 하였음.
        changeTabStyle( R.style.DayTheme );

//        Calendar calendar = Calendar.getInstance();
//        int hour = calendar.get( Calendar.HOUR_OF_DAY );
//
//        if(  hour > 20   ){
//            MyApplication.mSelectedTabStyleIndex = 2;
//            changeTabStyle( MyApplication.tabStyleList[MyApplication.mSelectedTabStyleIndex] );
//        }else if( hour > 18  ){
//            MyApplication.mSelectedTabStyleIndex = 1;
//            changeTabStyle( MyApplication.tabStyleList[MyApplication.mSelectedTabStyleIndex] );
//        }else if( hour > 6 ){
//            MyApplication.mSelectedTabStyleIndex = 0;
//            changeTabStyle( MyApplication.tabStyleList[MyApplication.mSelectedTabStyleIndex] );
//        }else{
//            MyApplication.mSelectedTabStyleIndex = 2;
//            changeTabStyle( MyApplication.tabStyleList[MyApplication.mSelectedTabStyleIndex] );
//        }

    }

    @SuppressWarnings("ResourceType") //이거 안하면 sign application 만들때 에러 발생함.
    private void changeTabStyle( int themeResource )
    {
//        int[] attrs = new int[]{ R.attr.newhomeBatteryCover , R.attr.newhomeCategoryBackground , R.attr.newhomeCategoryNormal, R.attr.newhomeCategorySelect ,
//        R.attr.newhomeCategoryTextNormal , R.attr.newhomeCategoryTextSelect, R.attr.newhomeCategoryValueTextSelect , R.attr.newhomeCategoryValueTextNormal ,
//        R.attr.newhomeBatteryEmpty , R.attr.newhomeBatteryFill , R.attr.newhomeBatteryText , R.attr.newhomeSync , R.attr.newhomeDrawer, R.attr.newhomeDateText , R.attr.newhomeDateArrow };

        //카테고리
        int[] categoryAttrs = new int[]{ R.attr.newhomeCategoryBackground ,  R.attr.newhomeCategoryEdit ,R.attr.newhomeCategoryNormal, R.attr.newhomeCategorySelect , R.attr.newhomeCategorySwipe ,
                R.attr.newhomeCategoryTextNormal , R.attr.newhomeCategoryTextSelect , R.attr.newhomeCategoryValueTextNormal , R.attr.newhomeCategoryValueTextSelect, R.attr.newhomeDateArrow , R.attr.newhomeDateText  };

        TypedArray categoryTypedArray = this.obtainStyledAttributes(themeResource, categoryAttrs);
        Drawable newhomeTabBackground = categoryTypedArray.getDrawable(0);
        Drawable newhomeCategoryEdit = categoryTypedArray.getDrawable(1);
        Drawable newhomeCategoryNormal = categoryTypedArray.getDrawable(2);
        Drawable newhomeCategorySelect = categoryTypedArray.getDrawable(3);
        Drawable newhomeCategorySwipe = categoryTypedArray.getDrawable(4);

        int newhomeCategoryTextNormal = categoryTypedArray.getColor(5, 0);
        int newhomeCategoryTextSelect = categoryTypedArray.getColor(6, 0);
        int newhomeCategoryValueTextNormal = categoryTypedArray.getColor(7, 0);
        int newhomeCategoryValueTextSelect = categoryTypedArray.getColor(8, 0);
        Drawable newhomeDateArrow = categoryTypedArray.getDrawable(9);
        int newhomeDateText = categoryTypedArray.getColor(10, 0);

        categoryTypedArray.recycle();

        int[] batteryAttrs = new int[]{ R.attr.newhomeBatteryCover , R.attr.newhomeBatteryEmpty , R.attr.newhomeBatteryFill , R.attr.newhomeBatteryText , R.attr.newhomeDrawer , R.attr.newhomeSync  };
        TypedArray batteryTypedArray = this.obtainStyledAttributes(themeResource, batteryAttrs);

        Drawable newhomeBatteryCover = batteryTypedArray.getDrawable(0);
        int newhomeBatteryEmpty = batteryTypedArray.getColor(1, 0);
        int newhomeBatteryFill = batteryTypedArray.getColor(2, 0);
        int newhomeBatteryText = batteryTypedArray.getColor(3, 0);
        Drawable newhomeDrawer = batteryTypedArray.getDrawable(4);
        Drawable newhomeSync = batteryTypedArray.getDrawable(5);


        batteryTypedArray.recycle();

        llActionBar.setBackground(newhomeTabBackground);
        ivCategorySwipe.setBackground( newhomeCategorySwipe );

        for (NewHomeCategoryView categoryView : categoryViews )
        {
            categoryView.changeNewHomeCategoryStyle( newhomeCategorySelect , newhomeCategoryNormal , newhomeCategoryTextSelect , newhomeCategoryTextNormal , newhomeCategoryValueTextSelect, newhomeCategoryValueTextNormal );
        }

        ivBatteryBackground.setImageDrawable(newhomeBatteryCover);
        batteryCircleView.setBatteryEmptyColor(newhomeBatteryEmpty);
        batteryCircleView.setBatteryFillColor(newhomeBatteryFill);
        batteryCircleView.invalidate();

        tvBatteryValue.setTextColor(newhomeBatteryText);

        imgDrawer.setImageDrawable(newhomeDrawer);

        tvDate.setTextColor(newhomeDateText);
        tvWeekDay.setTextColor( newhomeDateText );
        imgCalendarArrow.setImageDrawable(newhomeDateArrow);
    }

}
