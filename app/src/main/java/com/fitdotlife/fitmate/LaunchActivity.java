package com.fitdotlife.fitmate;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.fitdotlife.fitmate_lib.object.UserInfo;


import java.util.Timer;
import java.util.TimerTask;


public class LaunchActivity extends Activity {

    private Timer launchTimer;
    private final int DELAY = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView( R.layout.activity_launch);

        this.launchTimer = new Timer();
        this.launchTimer.schedule(new LaunchTimerTask(this), this.DELAY);

        //TODO 블루투스가 꺼져 있다면 켜야 한다.
    }

    private boolean isRegisteredUser() {
        SharedPreferences userInfo = this.getSharedPreferences(UserInfo.USERINFO_KEY, Activity.MODE_PRIVATE);
        return userInfo.getBoolean(UserInfo.REGISTERED_KEY, false);
    }

    public void moveActivity( )
    {
        Class<?> cls = null;
//        if( isRegisteredUser() )
//        {
//            cls = HomeActivity.class;
//        }
//        else
//        {
//            cls = SetUserInfoActivity.class;
//        }

        Intent intent = new Intent( LaunchActivity.this , HomeActivity.class );
        //Intent intent = new Intent( LaunchActivity.this , IncentivHistoryActivity.class );
        this.startActivity(intent);
        this.finish();
    }
}

class LaunchTimerTask extends TimerTask
{
    private LaunchActivity mActivity;
    public LaunchTimerTask(LaunchActivity launch) {
        this.mActivity = launch;
    }

    @Override
    public void run() {
        this.mActivity.moveActivity();
    }
}