package com.fitdotlife.fitmate;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Messenger;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fitdotlife.Preprocess;
import com.fitdotlife.fitmate.adapter.ExerciseProgramListAdapter;
import com.fitdotlife.fitmate.adapter.MyExerciseListAdapter;
import com.fitdotlife.fitmate_lib.customview.AppliedProgramLayout;
import com.fitdotlife.fitmate_lib.customview.DrawerView;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.iview.IExerciseHomeView;
import com.fitdotlife.fitmate_lib.key.ExerciseProgramType;
import com.fitdotlife.fitmate_lib.object.ExerciseProgram;
import com.fitdotlife.fitmate_lib.presenter.ExerciseProgramHomePresenter;
import com.fitdotlife.fitmate_lib.presenter.LoginPresenter;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.List;


public class ExerciseProgramHomeActivity extends Activity implements View.OnClickListener, IExerciseHomeView, AdapterView.OnItemClickListener, View.OnTouchListener {

    private String TAG ="fitmate - " + ExerciseProgramHomeActivity.class.getSimpleName();

    private ImageView imgSearch = null;
    private EditText etxSearchWord = null;
    private int selectType = 0;

    private AppliedProgramLayout aplAppliedProgramInfo = null;

    //private Button btnMyExercise = null;
    //private Button btnExerciseProgram = null;

    private RelativeLayout rlCreateExercise = null;
    private TextView tvCreateExercise = null;

    private ListView lvExerciseList = null;
    private LinearLayout llMenu = null;

    private ExerciseProgramHomePresenter mPresenter = null;

    private DrawerLayout dlExercie = null;
    private DrawerView drawerView = null;

    private InputMethodManager imm;

    private FitmateDBManager dbManager = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_exercise_home);
        
        if(!Preprocess.IsTest) {
            Tracker t = ((MyApplication) this.getApplication()).getTracker(MyApplication.TrackerName.APP_TRACKER);
            t.setScreenName("ExerciseProgram");
            t.send(new HitBuilders.AppViewBuilder().build());
        }

        Messenger service = this.getIntent().getParcelableExtra("messenger");
        imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);

        dbManager = new FitmateDBManager(this);
        this.mPresenter = new ExerciseProgramHomePresenter(this, this, service);
        ActivityManager.getInstance().addActivity(this);

        this.etxSearchWord = (EditText) this.findViewById(R.id.etx_activity_exercise_home_search_word);
        this.etxSearchWord.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                etxSearchWord.setFocusableInTouchMode(true);
                return false;
            }
        });

        this.imgSearch = (ImageView) this.findViewById(R.id.img_activity_exercise_home_search);
        this.imgSearch.setOnClickListener(this);

        this.lvExerciseList = (ListView) this.findViewById(R.id.lv_activity_exercise_home_exercise_list);
        this.lvExerciseList.setOnItemClickListener(this);

        this.aplAppliedProgramInfo = new AppliedProgramLayout(this);
        this.aplAppliedProgramInfo.setOnClickListener(this);
        lvExerciseList.addHeaderView(aplAppliedProgramInfo);

        //this.aplAppliedProgramInfo = (AppliedProgramLayout) this.findViewById(R.id.apl_activity_exercisehome_applied_program_info);
        //this.aplAppliedProgramInfo.setAppliedExerciseProgram(this.mPresenter.getAppliedExerciseProgram());
        //this.aplAppliedProgramInfo.setOnClickListener(this);

        this.dlExercie = (DrawerLayout) this.findViewById(R.id.dl_activity_exercise);
        this.drawerView = (DrawerView) this.findViewById(R.id.dv_activity_exercise_drawer);
        this.drawerView.setParentActivity(this);
        this.drawerView.setTag(this.getResources().getString(R.string.gnb_exerciseprogram));
        this.drawerView.setDrawerLayout( dlExercie );

        this.selectType = ExerciseProgramType.USEREXERCISEPROGRAM;

        View actionBarView = this.findViewById(R.id.ic_activity_main_actionbar);
        actionBarView.setBackgroundColor(this.getResources().getColor(R.color.fitmate_cyan));
        TextView tvBarTitle = (TextView) actionBarView.findViewById( R.id.tv_custom_action_bar_title );

        ImageView imgLeft = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_left);
        imgLeft.setBackgroundResource( R.drawable.icon_home_selector );
        imgLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityManager.getInstance().finishAllActivity();
                showNewHomeActivity();
            }
        });

        ImageView imgSliding = (ImageView) actionBarView.findViewById(R.id.img_custom_action_bar_right);
        imgSliding.setImageResource(R.drawable.icon_slide);
        imgSliding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dlExercie.openDrawer(drawerView);
            }
        });
        imgSliding.setOnTouchListener(this);

        tvBarTitle.setText(this.getString(R.string.exercise_bar_title));
    }
    
    @Override
    protected void onResume(){
    	super.onResume();

        Log.d(TAG, "onResume");

        if( this.selectType == ExerciseProgramType.USEREXERCISEPROGRAM  ) {
            this.selectType = ExerciseProgramType.USEREXERCISEPROGRAM;
            List<ExerciseProgram> userProgramList = this.mPresenter.getUserProgramList();
            this.displayUserExerciseMode(userProgramList);

        }else if( this.selectType == ExerciseProgramType.EXERCISEPROGRAM ) {

            if (this.selectType == ExerciseProgramType.EXERCISEPROGRAM) return;
            this.selectType = ExerciseProgramType.EXERCISEPROGRAM;
            List<ExerciseProgram> exerciseProgramSNUList = this.mPresenter.getExerciseProgramList();
            this.displayExerciseProgramMode(exerciseProgramSNUList);
        }

        ExerciseProgram appliedProgram = this.mPresenter.getAppliedExerciseProgram();
        this.aplAppliedProgramInfo.setAppliedExerciseProgram(appliedProgram);
        this.aplAppliedProgramInfo.setTag(appliedProgram.getId());
    }

    @Override
    public void onClick(View view)
    {
        switch( view.getId() )
        {

            case R.id.img_activity_exercise_home_search:
                etxSearchWord.setFocusable(false);

                if( this.selectType == ExerciseProgramType.USEREXERCISEPROGRAM  ){
                    List<ExerciseProgram> userProgramList = this.mPresenter.getUserProgramList(this.etxSearchWord.getText().toString());
                    this.displayUserExerciseMode(userProgramList);
                }else if( this.selectType == ExerciseProgramType.EXERCISEPROGRAM ){
                    List<ExerciseProgram> exerciseProgramSNUList = this.mPresenter.getExerciseProgramList( this.etxSearchWord.getText().toString() );
                    this.displayExerciseProgramMode(exerciseProgramSNUList);
                }

                imm.hideSoftInputFromWindow(etxSearchWord . getWindowToken(), 0);
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(!Preprocess.IsTest) {
            GoogleAnalytics.getInstance(this).reportActivityStart(this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(!Preprocess.IsTest) {
            GoogleAnalytics.getInstance(this).reportActivityStop(this);
        }
    }

    private void displayUserExerciseMode(List<ExerciseProgram> userProgramList)
    {
        MyExerciseListAdapter adapter = new MyExerciseListAdapter( this , R.layout.listview_item_activity_exercisehome_myexercise , userProgramList  );
        this.lvExerciseList.setAdapter(adapter);
    }

    private void displayExerciseProgramMode( List<ExerciseProgram> exerciseProgramSNUList)
    {
        this.tvCreateExercise.setText("Upload My Exercise");
        ExerciseProgramListAdapter adapter = new ExerciseProgramListAdapter( this ,  R.layout.listview_item_activity_exercisehome_exerciseprogram , exerciseProgramSNUList);
        this.lvExerciseList.setAdapter(adapter);
    }

    private void moveActivity(Class<?> cls)
    {
        Intent intent = new Intent( ExerciseProgramHomeActivity.this , cls );
        intent.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP );
        this.startActivity(intent);
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        int programId = (Integer) view.getTag();
        this.moveActivity(programId);
    }

    public void moveActivity( int programId )
    {
        Intent intent = new Intent( ExerciseProgramHomeActivity.this , ExerciseProgramInfoActivity_.class );
        intent.putExtra( ExerciseProgram.ID_KEY, programId );
        intent.putExtra("exercisetype" , this.selectType);
        this.startActivity(intent);

    }
    @Override
    public void onBackPressed() {

        if( dlExercie.isDrawerOpen(drawerView) ){
            dlExercie.closeDrawers();
            return;
        }

//        AlertDialog.Builder builder = new AlertDialog.Builder(this);     // 여기서 this는 Activity의 this
//
//// 여기서 부터는 알림창의 속성 설정
//        builder.setTitle(getString(R.string.common_quit_title))        // 제목 설정
//                .setMessage(getString(R.string.common_quit_content))        // 메세지 설정
//                .setCancelable(true)        // 뒤로 버튼 클릭시 취소 가능 설정
//                .setPositiveButton(R.string.common_yes, new DialogInterface.OnClickListener() {
//                    // 확인 버튼 클릭시 설정
//                    public void onClick(DialogInterface dialog, int whichButton) {
//                        moveTaskToBack(true);
//
//                        finish();
//
//                        //  android.os.Process.killProcess(android.os.Process.myPid());
//                        dialog.cancel();
//
//                        //   MainActivity.this.onBackPressed();
//                    }
//                })
//                .setNegativeButton(R.string.common_no, new DialogInterface.OnClickListener() {
//                    // 취소 버튼 클릭시 설정
//                    public void onClick(DialogInterface dialog, int whichButton) {
//                        dialog.cancel();
//                    }
//                });
//
//        AlertDialog dialog = builder.create();    // 알림창 객체 생성
//        dialog.show();    // 알림창 띄우기

        ActivityManager.getInstance().finishAllActivity();
        showNewHomeActivity();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == DrawerView.REQUEST_PROFILE_IMAGE){
            this.drawerView.refreshProfileImage();
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        ImageView actionbarImageView = (ImageView) view;
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            actionbarImageView.setColorFilter(this.getResources().getColor(R.color.selected_colorfilter) , PorterDuff.Mode.DARKEN );
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            actionbarImageView.setColorFilter(null);
        }

        return false;
    }

    private boolean getSettedUserInfo()
    {
        SharedPreferences pref = this.getSharedPreferences("fitmateservice", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        return pref.getBoolean( LoginPresenter.SET_USERINFO_KEY , false);
    }

    private void showNewHomeActivity(){
        Intent intent = new Intent();

        if( getSettedUserInfo() ) {
            intent.setClass( this , NewHomeActivity_.class );
            intent.putExtra(NewHomeActivity.HOME_LAUNCH_KEY, NewHomeActivity.LaunchMode.FROM_OTHERMENU.ordinal());
        }else{
            intent.setClass( this , NewHomeAsActivity_.class );
        }

        startActivity( intent );
    }

}
