package com.fitdotlife.fitmate;

import android.Manifest;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.PermissionChecker;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.util.Base64;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.fitdotlife.Preprocess;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ContinuousCheckPolicy;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ContinuousExerciseInfo;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseAnalyzer;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.FatAndCarbonhydrateConsumtion;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.StrengthInputType;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.UserInfoForAnalyzer;
import com.fitdotlife.fitdotlifelib.ExerciseAnalysis.WearingLocation;
import com.fitdotlife.fitdotlifelib.protocol.object.SystemInfo_Response;
import com.fitdotlife.fitmate.alarm.ActivityNotiAlarmReceiver;
import com.fitdotlife.fitmate.alarm.FitmeterNotUseAlarmReceiver;
import com.fitdotlife.fitmate.alarm.WearNotiAlarmReceiver;
import com.fitdotlife.fitmate.btmanager.BTManager;
import com.fitdotlife.fitmate.btmanager.ConnectionStatus;
import com.fitdotlife.fitmate.btmanager.FitmeterEventCallback;
import com.fitdotlife.fitmate.btmanager.ReceivedDataInfo;
import com.fitdotlife.fitmate.model.ActivityDataListener;
import com.fitdotlife.fitmate.model.ActivityDataModel;
import com.fitdotlife.fitmate.model.UserInfoModel;
import com.fitdotlife.fitmate.model.UserInfoModelResultListener;
import com.fitdotlife.fitmate.newhome.CategoryEditInfo;
import com.fitdotlife.fitmate.newhome.CategoryType;
import com.fitdotlife.fitmate.newhome.DayPagerAdapter;
import com.fitdotlife.fitmate.newhome.NewHomeCategoryData;
import com.fitdotlife.fitmate.newhome.NewHomeProgramClickListener;
import com.fitdotlife.fitmate.newhome.NewHomeUtils;
import com.fitdotlife.fitmate.newhome.NewHomeWeekView;
import com.fitdotlife.fitmate.newhome.NewsPagerAdapter;
import com.fitdotlife.fitmate.newhome.WeekActivityListener;
import com.fitdotlife.fitmate.newhome.WeekPagerActivityInfo;
import com.fitdotlife.fitmate.newhome.WeekPagerAdapter;
import com.fitdotlife.fitmate_lib.customview.BatteryCircleView;
import com.fitdotlife.fitmate_lib.customview.BatteryPopupCircleView;
import com.fitdotlife.fitmate_lib.customview.DrawerView;
import com.fitdotlife.fitmate_lib.customview.FitmeterDialog;
import com.fitdotlife.fitmate_lib.customview.NewHomeCategoryEditItemView;
import com.fitdotlife.fitmate_lib.customview.NewHomeCategoryEditItemView_;
import com.fitdotlife.fitmate_lib.customview.NewHomeCategoryView;
import com.fitdotlife.fitmate_lib.customview.NewHomeCategoryView_;
import com.fitdotlife.fitmate_lib.customview.NewHomeDayCircleView;
import com.fitdotlife.fitmate_lib.customview.NewHomeDayCircleView_;
import com.fitdotlife.fitmate_lib.customview.NewHomeDayHalfCircleView;
import com.fitdotlife.fitmate_lib.customview.NewHomeDayHalfCircleView_;
import com.fitdotlife.fitmate_lib.customview.NewHomeDayTextView;
import com.fitdotlife.fitmate_lib.customview.NewHomeDayTextView_;
import com.fitdotlife.fitmate_lib.customview.NewHomeMETChartView;
import com.fitdotlife.fitmate_lib.customview.NewHomeMetChartInfoView;
import com.fitdotlife.fitmate_lib.customview.NewHomeNewsView;
import com.fitdotlife.fitmate_lib.customview.NewHomeNewsView_;
import com.fitdotlife.fitmate_lib.database.FitmateDBManager;
import com.fitdotlife.fitmate_lib.database.FitmateServiceDBManager;
import com.fitdotlife.fitmate_lib.http.ActivityService;
import com.fitdotlife.fitmate_lib.http.NetworkClass;
import com.fitdotlife.fitmate_lib.http.NewsService;
import com.fitdotlife.fitmate_lib.http.UserInfoService;
import com.fitdotlife.fitmate_lib.key.CommonKey;
import com.fitdotlife.fitmate_lib.key.FitmeterType;
import com.fitdotlife.fitmate_lib.object.BLEDevice;
import com.fitdotlife.fitmate_lib.object.DayActivity;
import com.fitdotlife.fitmate_lib.object.DayActivity_Rest;
import com.fitdotlife.fitmate_lib.object.ExerciseProgram;
import com.fitdotlife.fitmate_lib.object.FirmwareVersionInfo;
import com.fitdotlife.fitmate_lib.object.FriendWeekActivity;
import com.fitdotlife.fitmate_lib.object.News;
import com.fitdotlife.fitmate_lib.object.NewsResult;
import com.fitdotlife.fitmate_lib.object.ScoreClass;
import com.fitdotlife.fitmate_lib.object.UserInfo;
import com.fitdotlife.fitmate_lib.object.UserNotiSetting;
import com.fitdotlife.fitmate_lib.object.WeekActivity;
import com.fitdotlife.fitmate_lib.object.WeekActivityWithDayActivityList;
import com.fitdotlife.fitmate_lib.presenter.LoginPresenter;
import com.fitdotlife.fitmate_lib.service.activity.ActivityParser;
import com.fitdotlife.fitmate_lib.service.activity.DataParseException;
import com.fitdotlife.fitmate_lib.service.activity.HeaderParseException;
import com.fitdotlife.fitmate_lib.service.bluetooth.BluetoothRawData;
import com.fitdotlife.fitmate_lib.service.protocol.object.ActivityData;
import com.fitdotlife.fitmate_lib.service.protocol.object.TimeInfo;
import com.fitdotlife.fitmate_lib.util.DateUtils;
import com.fitdotlife.fitmate_lib.util.Utils;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.Touch;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringRes;
import org.androidannotations.rest.spring.annotations.RestService;
import org.apache.log4j.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.client.RestClientException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

@EActivity(R.layout.activity_newhome)
public class NewHomeActivity extends Activity implements NetWorkChangeListener, WeekPagerAdapter.WeekMoveListener, FitmeterEventCallback
{
    public static final String INIT_AlARM_KEY = "initialalarm";
    public static final String VIEWORDER_KEY = "vieworder";
    public static final String SELECTEDCATEGORYINDEX_KEY = "selectedcategoryindex";
    private final int PERMISSION_REQUEST_LOCATION = 0;
    public static final String BATTERY_CHECK_TIME_KEY = "batterychecktime";
    private final int RE_CONNECT_TIME = 10000;
    //한번이라도 연결되었다면
    private boolean isConnectedOnce = false;
    private String mDeviceAddress = null;
    private FitmateServiceDBManager mServiceDBManager = null;

    private FitmeterType mFitmeterType = null;

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    public void onProgressValueChnaged(final int progress, final int max) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tvSyncPercent.setText(""+ (progress*100/max) );
            }
        });
    }

    @Override
    public void onReceiveAllFileComplete( final List<ReceivedDataInfo> receiveDataArray, boolean isComplete) {
        if( receiveDataArray!=null && receiveDataArray.size()>0){

//            runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//
//                    showBatteryView();
//                    setBattery();
//
//                }
//            });

            new Thread(new Runnable() {
                @Override
                public void run() {


                    processWithReceivedFiles( receiveDataArray );

                }
            }).start();

        }
//        else{
//
//            runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    if (isDeviceFound == false) {
//
//                        showBatteryView();
//                        setBattery();
//
//
//                    } else {
//                        //기존에는 성공이었는데 실패한 경우
//
//                        //showConnectionStatePopup(false);
//                        setBattery();
//
//                    }
//                    isDeviceFound = false;
//                }
//            });
//        }
    }

    @Override
    public void receiveCompleted(int index, int offset, byte[] receivedbytes) {

    }

    private volatile int notFoundCount =0;
    private volatile boolean isDeviceFound = true;
    private volatile boolean isBatteryCheck = false;
    private Calendar lastbatteryCheckTime;

    private  volatile boolean isScanning= false;

    @Override
    public void statusOfBTManagerChanged(ConnectionStatus status) {

        switch (status) {
            case POWERDOWN:
                Log.d(TAG, "Power Down");

                stopAutoReceive = true;
                btManager.stopScan();
                isScanning = false;
                btManager.disconnect();
                btManager.close();

                break;
            case POWERON:
                Log.d(TAG, "Power On");
                if(MyApplication.LOCATION_PERMISSION_GRANTED)
                {
                    stopAutoReceive = false;
                    startScan();
                }
                break;
            case SCANNING:
                android.util.Log.d(TAG, "Scanning");
                break;

            case IDLE:
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (isDeviceFound == false) {
                            isDeviceFound = false;
                            showBatteryView();
                            setBattery();


                        } else {
                            //기존에는 성공이었는데 실패한 경우
                            isDeviceFound = false;
                            //showConnectionStatePopup(false);
                            showBatteryView();
                            setBattery();
                        }
                        isDeviceFound = false;
                    }
                });

            case CONNECTED:
                android.util.Log.d(TAG, "Connected");
                if(btManager==null) break;
                new Thread(new Runnable()
                {
                    @Override
                    public void run() {

                        int startIndex = 0;
                        int offset=0;
                        if( mServiceDBManager.getLastFileData() != null)
                        {
                            BluetoothRawData lastData=  mServiceDBManager.getLastFileData();
                            startIndex =lastData.getFileIndex();
                            if(lastData.getFileData()!=null){
                                offset = lastData.getFileData().length;
                            }
                        }


                        //장치 펌웨어 버전이 저장되어 있지 않다면 저장한다.
                        FirmwareVersionInfo deviceFirmwareVersionInfo = getDeviceFirmwareVersionInfo();

                        if( deviceFirmwareVersionInfo.getMajorVersion() == -1 )
                        {
                            SystemInfo_Response systemInfo = btManager.getSystemInfo();

                            if(systemInfo != null) {
                                setDeviceFirmwareVersionInfo(systemInfo.getMajorVersion(), systemInfo.getMinorVersion());
                            }
                        }

                        isConnectedOnce = true;

                        //TODO check Battery 조건 설정
                        if( isBatteryCheck )
                        {
                            lastbatteryCheckTime = Calendar.getInstance();
                            lastbatteryCheckTime.setTimeInMillis( getLastBatteryCheckTime() );
                            if(isPastOverOneHour(lastbatteryCheckTime)) {

                                int batteryRate= btManager.readBatteryRate( mFitmeterType );
                                if(batteryRate>0){
                                    //DB에 갱신
                                    mDBManager.setLastBatteryRatio(mUserInfo.getEmail(), batteryRate, mUserInfo.getDeviceAddress());
                                    setLastBatteryCheckTime( System.currentTimeMillis() );
                                    Log.d(TAG, "배터리 체크 완료");
                                }
                            }
                        }else{
                            int batteryRate= btManager.readBatteryRate( mFitmeterType );
                            if(batteryRate>0){
                                //DB에 갱신
                                mDBManager.setLastBatteryRatio(mUserInfo.getEmail(), batteryRate, mUserInfo.getDeviceAddress());
                                setLastBatteryCheckTime( System.currentTimeMillis() );
                                isBatteryCheck = true;
                                android.util.Log.d(TAG, "배터리 체크 완료" + batteryRate);
                            }
                        }

                        //배터리 사용량 경고를 추가한다.
                        UserNotiSetting userNotiSetting = mDBManager.getUserNotiSetting();
                        if( userNotiSetting.isBatteryusagenoti() )
                        {
                            int batteryRatio = mDBManager.getLastBatteryRatio(mDBManager.getUserInfo().getEmail());
                            boolean alreadyBatteryNotification = getBatteryNoti();

                            if (batteryRatio <= 10 && batteryRatio != -1)
                            {

                                if (!alreadyBatteryNotification)
                                {
                                    showNotification("Fitmate", getString(R.string.alarm_battery_low), getString(R.string.alarm_battery_low), 520);
                                    setBatteryNoti(true);
                                }
                            } else if (batteryRatio > 10) {
                                setBatteryNoti(false);
                            }
                        }

                        //기기에 시간을 설정하기 전에 시간대가 변했는지 확인한다.
                        //기기에 시간을 설정한다.
                        try {

                            //현재 기기와 폰의 시간이 10분 이상 차이가 난다면 시간 설정을 하도록 한다.
                            com.fitdotlife.fitdotlifelib.protocol.object.TimeInfo timeInfo = btManager.getCurrentTime();
                            if (isTimeToSetCurrentTime()) {
                                Log.e(TAG , "기기 시간" + timeInfo.getTimeString() );
                                if (Math.abs(timeInfo.getMilliSecond() - System.currentTimeMillis()) > 10 * 60000) {

                                    Log.i(TAG, "기기와의 시간이 10분 이상 차이가 남.");
                                    if (!btManager.setCurrentTime()) {
                                        Log.e(TAG, "시간 설정 중에 오류가 발생했습니다.");
                                    }else {
                                        Log.i(TAG, "시간설정 완료");
                                    }

                                    //TODO 설정 해야함
                                    com.fitdotlife.fitdotlifelib.protocol.object.SystemInfo_Response systemInfo = btManager.getSystemInfo();
                                    com.fitdotlife.fitdotlifelib.protocol.object.SystemInfo_Request  systemInfo_request = systemInfo.getSystemInfo();
                                    btManager.setSystemInfo(systemInfo_request);
                                }
                            }

                        }catch (Exception err){
                            Log.e(TAG, "시간 설정 중에 오류가 발생했습니다.");
                        }

                        mDBManager.setLastConnectedTime(mUserInfo.getEmail(), Calendar.getInstance().getTime(), mUserInfo.getDeviceAddress());

                        if(btManager!=null){
                            btManager.startDownload(startIndex, offset);
                            //파일 다운로드 할때마다 syncview 돌리도록 수정
//                            runOnUiThread(new Runnable() {
//                                @Override
//                                public void run() {
//                                    showSyncView();
//                                }
//                            });

                            Log.e(TAG, "CONNECTED call startDownload");
                        }

                    }
                }).start();


                break;
            case ERROR_DISCONNECTED_133:
            {
                isScanning = false;
                new Thread(new Runnable() {
                    @Override
                    public void run() {

                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        if(!stopAutoReceive){
                            if(!isScanning){
                                Log.e(TAG, "133 error call connectFitmeter");

                                if(btManager!=null){
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            btManager.connectFitmeter( mDeviceAddress );
                                        }
                                    });

                                }
                            }

                        }
                    }
                }).start();

                break;
            }
            case ERROR_DISCONNECTED:
                isScanning = false;
                android.util.Log.d(TAG , "Error Disconnected");
                notFoundCount = 0;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        isDeviceFound = isBatteryCheck;
                        showBatteryView();
                        setBattery();

                    }//run
                });

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        if(!stopAutoReceive){

                            if(!isScanning){
                                Log.e(TAG, "ERROR DISCONNECTED call findFitmeter");
                                if(btManager!=null)
                                {
                                    showSyncView();
                                    btManager.findFitmeter( mDBManager.getUserInfo().getDeviceAddress() );
                                }

                                isScanning= true;
                            }
                        }
                    }
                }).start();
                break;
            case DISCONNECTED:
                isScanning= false;
                android.util.Log.d(TAG, "DisConnected");
                notFoundCount =0;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        isDeviceFound = isBatteryCheck;
                        showBatteryView();
                        setBattery();

                    }//run
                });

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(RE_CONNECT_TIME);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        if(!stopAutoReceive){

                            if(!isScanning){
                                Log.e(TAG, "DISCONNECTED call findFitmeter");
                                if(btManager!=null){
                                    showSyncView();
                                    btManager.findFitmeter( mDBManager.getUserInfo().getDeviceAddress() );
                                }

                                isScanning= true;
                            }

                        }
                    }
                }).start();

                break;
            case NOTFOUND:

                isScanning= false;
                android.util.Log.d(TAG, "NotFound");
                android.util.Log.d(TAG , "NOTFOUND COUNT -" + notFoundCount);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (notFoundCount > 3) {
                            isDeviceFound = false;
                        }
                        else
                        {
                            Calendar batteryCheckTime = Calendar.getInstance();
                            batteryCheckTime.setTimeInMillis( getLastBatteryCheckTime() );
                            if( Calendar.getInstance().getTimeInMillis() - batteryCheckTime.getTimeInMillis() > 2 * 60 * 60 * 1000 )
                            {
                                //연결된 시간이 2시간 이상이면
                                isDeviceFound = false;
                            }else{
                                //연결된 시간이 2시간 이내면
                                isDeviceFound = true;
                            }
                        }

                        showBatteryView();
                        setBattery();
                    }//run
                });

                new Thread(new Runnable() {
                    @Override
                    public void run() {

                        int sleepCount = notFoundCount++;
                        sleepCount += notFoundCount;
                        long sleepTime = sleepCount * 1000;
                        if(sleepTime > RE_CONNECT_TIME){
                            sleepTime = RE_CONNECT_TIME;
                        }

                        android.util.Log.d(TAG , "NOTFOUND SLEEP TIME -" + sleepTime);

                        try {
                            Thread.sleep( sleepTime );
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        if (!stopAutoReceive) {
                            if (!isScanning) {
                                Log.e(TAG, "NOTFOUND 기기 검색 시작 - " + mUserInfo.getDeviceAddress());
                                if (btManager != null) {
                                    showSyncView();
                                    btManager.findFitmeter( mDBManager.getUserInfo().getDeviceAddress() );
                                }

                                isScanning = true;
                            }
                        }

                    }
                }).start();

                break;
        }

    }

    private boolean isPastOverOneHour(Calendar prev){

        if( Calendar.getInstance().getTimeInMillis() - prev.getTimeInMillis() >3600000)
        {
            return true;
        }
        return false;
    }
    private boolean isTimeToSetCurrentTime(  )
    {

        Calendar current = Calendar.getInstance();
        Log.i(TAG , "날짜 : " + current.getTime().toString() );
        //하루마다 휘트미터에서 데이터 파일을 생성하니까 자정기준으로 1시간 이내는 시간 설정을 안하는 것으로 함.
        if (current.get(Calendar.HOUR_OF_DAY ) <= 1 || current.get(Calendar.HOUR_OF_DAY) >= 23) {
            return false;
        }
        else{
            return true;
        }
    }
    @Override
    public void findNewDeviceResult(List<BLEDevice> discoveredPeripherals) {

    }

    @Override
    public void findDeviceResult(final String deviceAddress, final String serialNumber) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                //isDeviceFound = true;
                //showBatteryView();
                //setBattery();

                mDeviceAddress = deviceAddress;
                //삼성 갤럭시 노트2에서 connect를 UI쓰레드 에서 하지 않으면 콜백이 null이 되는 문제 발생
                if (btManager != null)
                    btManager.connectFitmeter( deviceAddress );

                if(serialNumber != null)
                {
                    if (mUserInfo.getDeviceAddress().contains(":"))
                    {
                        mUserInfo.setDeviceAddress(serialNumber);
                        mDBManager.setUserInfo(mUserInfo);
                        mUserInfoModel.registerUserInfo( mUserInfo );
                    }

                    if (serialNumber.substring(3, 4).equals("4")) //밴드일 때
                    {
                        mFitmeterType = FitmeterType.BAND;
                    } else if ( serialNumber.substring(3, 4).equals("3") ) { //BLE 45일 때
                        mFitmeterType = FitmeterType.BLE;
                    }
                }else{
                    mFitmeterType = FitmeterType.BLE;
                }
            }
        });
    }

    @Override
    public void findRecoveryDeviceResult( BluetoothDevice recoveryDevice , List<BluetoothDevice> recoveryFitmeters ) {

    }

    public enum LaunchMode {START, FROM_OTHERMENU, FROM_NEWSCLICK}

    ;
    private LaunchMode mLaunchMode = null;

    public static final String HOME_LAUNCH_KEY = "homelaunch";
    public static final String HOME_NEWS_KEY = "homenews";
    public static final String HOME_NEWSDATE_KEY = "homenewsdate";

    private final int PagerAnimationDuration = 4000;
    private int mWeekPagerLength = 0;
    private int mWeekCurrentItem = 0;

    private final int ENABLE_BT_REQUEST_ID = 1;
    private final int MAX_RECEIVE_BYTE_LENGTH = 512;

    private final String TAG = "fitmate - " + NewHomeActivity.class.getSimpleName();

    private View calendarMain = null;
    private PopupWindow calendarPopup = null;

    private int selectedCategoryIndex = 0;
    private Calendar mTodayCalendar = null;
    private int mTodayWeekNumber = 0;
    private Calendar mDayCalendar = null;
    private Calendar mWeekCalendar = null;

    private FitmateDBManager mDBManager = null;
    private UserInfo mUserInfo = null;

    private boolean isRequestCalculate = false;

    private ActivityDataModel mActivityDataModel = null;
    private UserInfoModel mUserInfoModel = null;

    //Activity가 끝나는 시점에 서비스에게 계산 요청을 하지 않도록 막는 변수임.
    //onStop이나 onPause 시에 이 변수를 false로 설정. onResume 일 때 true로 설정.
    //onSyncInfo 함수에서 이 변수를 확인하고 계산 요청 유무를 판단한다.
    private boolean isRequestAndView = false;

    private DayPagerAdapter mDayPagerAdapter = null;
    private boolean mInitialDayDisplay = true;
    private boolean mInitialWeekDisplay = true;

    private Handler mConnectTimerHandler = null;
    private Runnable mConnectionRunnable = null;
    private MyApplication myApplication = null;

    private NewHomeUtils newHomeUtils = null;
    public PopupWindow batteryPopup = null;

    private PagerRunnable pagerRunnable = new PagerRunnable();
    private WeekPagerActivityInfo[] mWeekPagerActivityInfoList = null;
    private NewHomeWeekPagerAdapter mWeekBarPagerAdapter = null;
    private NewHomeWeekPagerAdapter mWeekTablePagerAdapter = null;

    private NewHomeCategoryView mCategoryEditCategoryView = null;

    //카테고리 뷰의 위치에 해당 카테고리 타입을 저장하고 있는 배열.
    //다시 말하자면 ViewIndex에 카테고리 타입을 저장하고 있음.
    public CategoryType[] mCategoryViewOrder = null;

    //카테고리 수정용 카테고리 배열.
    NewHomeCategoryView[] categoryViews = new NewHomeCategoryView[CategoryType.values().length];
    NewHomeCategoryView[] categoryEditViews = new NewHomeCategoryView[CategoryType.values().length];

    View[] dayViews = new View[CategoryType.values().length];

    private AlertDialog metDialog = null;

    private int mDay = 0;

    private Handler connectPopupDismissHandler = null;
    private Runnable conntPopupDismissRunnable = null;
    private PopupWindow connectPopup = null;

    private BTManager btManager = null;

    @RestService
    NewsService newsServiceClient;

    @ViewById(R.id.vp_activity_newhome_day)
    ViewPager vpDay;

    @ViewById(R.id.vp_activity_newhome_week_bar)
    ViewPager vpWeekBar;

    @ViewById(R.id.vp_activity_newhome_week_table)
    ViewPager vpWeekTable;

    @ViewById(R.id.rl_activity_newhome_week)
    RelativeLayout rlWeek;

    @ViewById(R.id.vp_activity_newhome_news)
    ViewPager vpNews;

    @ViewById(R.id.tv_activity_newhome_date)
    TextView tvDate;

    @ViewById(R.id.tv_activity_newhome_weekday)
    TextView tvWeekDay;

    @ViewById(R.id.img_activity_newhome_calendar_arrow)
    ImageView imgCalendarArrow;

    @ViewById(R.id.tv_activity_newhome_date)
    TextView newHomeDate;

    @ViewById(R.id.hsv_activity_newhome_category)
    HorizontalScrollView hsvCategory;

    @ViewById(R.id.rl_activity_newhome_tabbar)
    RelativeLayout rlTabBar;

    @ViewById(R.id.rl_activity_newhome_category)
    RelativeLayout rlCategory;

    @ViewById(R.id.img_activity_newhome_sync)
    ImageView imgSync;

    @ViewById(R.id.dl_activity_newhome)
    DrawerLayout dlNewHome;

    @ViewById(R.id.dv_activity_newhome_drawer)
    DrawerView drawerView;

    @ViewById(R.id.iv_activity_newhome_batterybackground)
    ImageView ivBatteryBackground;

    @ViewById(R.id.rl_activity_newhome_actionbar_battery)
    RelativeLayout rlBattery;

    @ViewById(R.id.rl_activity_newhome_actionbar_sync)
    RelativeLayout rlSync;

    @ViewById(R.id.img_activity_newhome_drawer)
    ImageView imgDrawer;

    @ViewById(R.id.bcv_activity_newhome_battery)
    BatteryCircleView batteryCircleView;

    @ViewById(R.id.img_activity_newhome_battery_led)
    ImageView imgBatteryLed;

    @ViewById(R.id.tv_activity_newhome_battery_value)
    TextView tvBatteryValue;

    @ViewById(R.id.img_activity_newhome_battery_value)
    ImageView imgBatteryValue;

    @ViewById(R.id.ll_activity_newhome_calendar)
    LinearLayout llCalendar;

    @ViewById(R.id.tv_activity_newhome_sync_percent)
    TextView tvSyncPercent;

    @ViewById(R.id.rl_activity_newhome_actionbar)
    RelativeLayout rlActionBar;

    @ViewById(R.id.rl_activity_newhome_actionbar_categoryedit)
    RelativeLayout rlCategoryEditActionBar;

    @ViewById(R.id.rl_activity_newhome_week_progress)
    RelativeLayout rlWeekProgress;

    @ViewById(R.id.ll_activity_newhome_categoryedit_popup)
    LinearLayout llCategoryEditPopup;

    @ViewById(R.id.gv_activity_newhome_categoryedit_popup)
    GridView gvCategoryEditPopup;

    @ViewById(R.id.tv_activity_newhome_actionbar_categoryedit_save)
    TextView tvCategoryEditSave;

    @ViewById(R.id.tv_activity_newhome_actionbar_categoryedit_cancel)
    TextView tvCategoryEditCancel;

    @ViewById(R.id.tv_activity_newhome_actionbar_categoryedit_title)
    TextView tvCategoryEditTitle;

    @ViewById(R.id.hsv_activity_newhome_categoryedit_category)
    HorizontalScrollView hsvCategoryEdit;

    @ViewById(R.id.rl_activity_newhome_categoryedit_category)
    RelativeLayout rlCategoryEdit;

    @ViewById(R.id.ll_activity_newhome_met_container)
    LinearLayout llMetContainer;

    @ViewById(R.id.mc_activity_newhome_metchart)
    NewHomeMETChartView metChartView;

    @ViewById(R.id.mci_activity_newhome_metchart)
    NewHomeMetChartInfoView metChartInfoView;

    @ViewById(R.id.iv_activity_newhome_category_swipe)
    ImageView ivCategorySwipe;


    @RestService
    ActivityService activityServiceClient;

    @RestService
    UserInfoService userInfoServiceClient;

    @StringRes(R.string.newhome_met_high_intensity)
    String newhomeHighIntensity;

    @StringRes(R.string.newhome_met_medium_intensity)
    String newhomeMediumIntensity;

    @StringRes(R.string.newhome_met_low_intensity)
    String newhomeLowIntensity;

    //운동 프로그램이 변경되는 것을 감지하여 일 주 그래프를 업데이트 한다.
//    private BroadcastReceiver mProgramChangetReceiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent)
//        {
//            displayDayData( false );
//            displayWeekData();
//        }
//    };

    //사용자 정보가 변경되는 것을 감지하여 일 주 그래프를 업데이트한다.
    private BroadcastReceiver mChangeReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            mUserInfo = mDBManager.getUserInfo();
            displayDayData(false);
            displayWeekData();
        }
    };

    @Override
    protected void onRestart() {
        super.onRestart();

        int currentDay = Calendar.getInstance().get( Calendar.DAY_OF_MONTH );
        if( currentDay != mDay ){
            updateHome();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        this.setTheme(R.style.OrangeTheme);

        //홈 화면을 계속 띄우는 걸로 변경되어서 아래 코드가 필요없음.
        //운동 프로그램이 변경되는 것을 감지하여 일 주 그래프를 업데이트 한다.
//        IntentFilter programChangeFilter = new IntentFilter();
//        programChangeFilter.addAction(ExerciseProgramInfoActivity.ACTION_PROGRAM_CHANGE);
//        this.registerReceiver( mProgramChangetReceiver , programChangeFilter  );
        //사용자 정보가 변경되는 것을 감지하여 일 주 그래프를 업데이트한다.


        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(SettingActivity.ACTION_USERINFO_CHANGE);
        this.registerReceiver(mChangeReceiver, intentFilter);

        ActivityManager.getInstance().addActivity(this);
        myApplication = (MyApplication) this.getApplication();

        Intent intent = this.getIntent();
        mLaunchMode = mLaunchMode.values()[intent.getIntExtra(HOME_LAUNCH_KEY, 0)];

        if (!Preprocess.IsTest) {
            Tracker t = myApplication.getTracker(MyApplication.TrackerName.APP_TRACKER);
            t.setScreenName("Home");
            t.send(new HitBuilders.AppViewBuilder().build());
        }

        activityServiceClient.setRootUrl(NetworkClass.baseURL + "/api");
        userInfoServiceClient.setRootUrl(NetworkClass.baseURL + "/api");

        this.mDBManager = new FitmateDBManager(this);
        this.mServiceDBManager = new FitmateServiceDBManager(this);
        this.mUserInfo = this.mDBManager.getUserInfo();

        //처음 알람 실행을 행했는지 확인한다.
        if(  !getInitialAlarm()  )
        {
            //기존에 등록된 알람을 취소시킨다.
            cancelWearNotiAlarm();
            cancelActivityNotiAlarm();

            //6시에 발생하는 알람 리시버를 등록 한다.
            setActivityNotiAlarm();
            //특정시간에 행하는 알람을 실행하는 리시버를 등록해야 한다.
            setWearNotiAlarm( );

            setInitialAlarm( true );
        }

        Crashlytics.setUserIdentifier(MyApplication.TAG_NUM);
        Crashlytics.setUserEmail(mUserInfo.getEmail());
        Crashlytics.setUserName(mUserInfo.getName());

        Calendar calendar = Calendar.getInstance();

        mTodayCalendar = Calendar.getInstance();
        mTodayCalendar.setTimeInMillis(calendar.getTimeInMillis());
        mDay = mTodayCalendar.get( Calendar.DAY_OF_MONTH );
        mTodayWeekNumber = mTodayCalendar.get(Calendar.DAY_OF_WEEK);

        mDayCalendar = Calendar.getInstance();
        mDayCalendar.setTimeInMillis(calendar.getTimeInMillis());

        this.mWeekCalendar = Calendar.getInstance();
        this.mWeekCalendar.setTimeInMillis(calendar.getTimeInMillis());
        int weekNumber = mWeekCalendar.get(Calendar.DAY_OF_WEEK);
        mWeekCalendar.add(Calendar.DATE, -(weekNumber - 1));

        this.mActivityDataModel = new ActivityDataModel(this, new ActivityDataListener() {
            @Override
            public void onDayActivityReceived() {
            }

            @Override
            public void onWeekActivityReceived() {
            }

            @Override
            public void onMonthActivityReceived() {
            }

            @Override
            public void onUploadedData(boolean isUpload) {
            }
        });

        this.mUserInfoModel = new UserInfoModel(this, new UserInfoModelResultListener()
        {
            @Override
            public void onSuccess(int code) { }

            @Override
            public void onSuccess(int code, boolean result) { }

            @Override
            public void onSuccess(int code, UserInfo userInfo) { }

            @Override
            public void onFail(int code) { }

            @Override
            public void onErrorOccured(int code, String message) { }
        });

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    private void initCategoryView()
    {
        for (int categoryIndex = 0; categoryIndex < CategoryType.values().length; categoryIndex++)
        {
            CategoryType categoryType = CategoryType.values()[categoryIndex];

            NewHomeCategoryView categoryView = NewHomeCategoryView_.build(this);
            categoryView.setSelectListener(new CategoryClickListener());

            View dayView = null;

            if (categoryType.equals(CategoryType.DAILY_ACHIEVE)) {
                dayView = NewHomeDayHalfCircleView_.build(this);
            } else if (categoryType.equals(CategoryType.WEEKLY_ACHIEVE)) {
                dayView = NewHomeDayCircleView_.build(this);

            } else {
                dayView = NewHomeDayTextView_.build(this);
            }

            categoryViews[categoryType.ordinal()] = categoryView;
            dayViews[categoryType.ordinal()] = dayView;
        }

        this.mCategoryEditCategoryView = NewHomeCategoryView_.build(this);
    }

    private void initBluetooth(){
        if (!isBtEnabled()) { //블루투스가 켜져 있지 않다면

            if (!this.getRequestBluetoothOn())
            {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                this.startActivityForResult(enableBtIntent, this.ENABLE_BT_REQUEST_ID);
                this.setRequestBluetoothOn(true);
            }

        } else {
            this.setCheckBluetoothOn(true);
            startScan();
        }
    }

    private void startScan(){
        Log.e(TAG, "기기 검색 시작 - " + mUserInfo.getDeviceAddress());

        showSyncView();
        btManager.findFitmeter(mDBManager.getUserInfo().getDeviceAddress());
    }

    private volatile boolean stopAutoReceive=false;

    @Override
    protected void onStart() {
        super.onStart();

        selectTabStyle();

        stopAutoReceive= false;
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        if (!Preprocess.IsTest) {
            GoogleAnalytics.getInstance(this).reportActivityStart(this);
        }
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "NewHome Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.fitdotlife.fitmate/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
        btManager= null;

        btManager= BTManager.getInstance(this, this);

        this.isRequestAndView = true;
        this.isBatteryCheck = false;

        //홈이 시작할 때
        // 기기 연결시간과 배터리가 없다면 배터리를 뷰를 보여 주지 않는다.
        // 기기 연결시간과 배터리가 있다면 배터리를 보여준다.
        //long connectTime = mDBManager.getLastConnectedTime( mUserInfo.getEmail() );
        //int batteryStatus = mDBManager.getLastBatteryRatio( mUserInfo.getEmail() );
        //if(connectTime > -1 && batteryStatus > -1)
        //{
        //    showBatteryView();
        //    setBattery();
        //}

        if( mLaunchMode.equals(LaunchMode.START) )
        {

            if (!this.getCheckBluetoothOn())
            { // 블루투스를 체크를 안했다면


                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                {
                    //마쉬 멜로우 이상버전 인경우
                    int checkLocation = PermissionChecker.checkSelfPermission(NewHomeActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);

                    if (checkLocation == PackageManager.PERMISSION_GRANTED)
                    {
                        initBluetooth();

                    } else {

                        final FitmeterDialog fDialog = new FitmeterDialog(this);
                        LayoutInflater inflater = this.getLayoutInflater();
                        View contentView = inflater.inflate(R.layout.dialog_default_content, null);
                        TextView tvTitle = (TextView) contentView.findViewById(R.id.tv_dialog_default_title);
                        tvTitle.setVisibility(View.GONE);

                        TextView tvContent = (TextView) contentView.findViewById(R.id.tv_dialog_default_content);
                        tvContent.setText(this.getResources().getString(R.string.home_location_permission_reject));
                        fDialog.setContentView(contentView);

                        View buttonView = inflater.inflate(R.layout.dialog_button_one , null);
                        Button btnLeft = (Button) buttonView.findViewById(R.id.btn_dialog_button_one);
                        btnLeft.setText(this.getString(R.string.common_ok));
                        btnLeft.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                ActivityCompat.requestPermissions(NewHomeActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_LOCATION);
                                fDialog.close();
                            }
                        });

                        fDialog.setButtonView(buttonView);
                        fDialog.show();
                    }

                }else{
                    //마쉬멜로우 이하버전일 때
                    initBluetooth();
                }
            }else {

                if(MyApplication.LOCATION_PERMISSION_GRANTED) {
                    if (isBtEnabled()) {
                        startScan();
                    }
                }
            }

        }else{
            if(MyApplication.LOCATION_PERMISSION_GRANTED) {
                if (isBtEnabled()) {
                    startScan();
                }
            }
        }
    }


    private void selectTabStyle()
    {
        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get( Calendar.HOUR_OF_DAY );

        if(  hour >= 20   ){
            MyApplication.mSelectedTabStyleIndex = 2;
            changeTabStyle( MyApplication.tabStyleList[MyApplication.mSelectedTabStyleIndex] );
        }else if( hour >= 18  ){
            MyApplication.mSelectedTabStyleIndex = 1;
            changeTabStyle( MyApplication.tabStyleList[MyApplication.mSelectedTabStyleIndex] );
        }else if( hour >= 6 ){
            MyApplication.mSelectedTabStyleIndex = 0;
            changeTabStyle( MyApplication.tabStyleList[MyApplication.mSelectedTabStyleIndex] );
        }else{
            MyApplication.mSelectedTabStyleIndex = 2;
            changeTabStyle( MyApplication.tabStyleList[MyApplication.mSelectedTabStyleIndex] );
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        stopAutoReceive= true;
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "NewHome Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.fitdotlife.fitmate/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        if (!Preprocess.IsTest) {
            GoogleAnalytics.getInstance(this).reportActivityStop(this);
        }
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.disconnect();

        
        if(btManager != null)
        {
            android.util.Log.e(TAG, "New Home Activity onSTOP() Call CLEAR");
            btManager.clear();

            List<ReceivedDataInfo> receivedDataInfoList =  btManager.getReceivedDataInfoList();
            if( receivedDataInfoList.size() > 0 )
            {
                processWithReceivedFiles( receivedDataInfoList );
            }
        }


        //8시간 동안 휘트미터를 앱에 연결안했을 경우 발생하는 알람을 설정하는거다
        //이전 알람을 제거하고 새로운 알람을 등록한다.
        //기기 연결되었으면 알람을 설정한다.
        //기기 연결은 isBatteryCheck로 판단함. isBatteryCheck가 true이면 연결된것으로 함.
        if( isConnectedOnce )
        {
            cancelFitmeterNotUseAlarm();

            UserNotiSetting userNotiSetting = mDBManager.getUserNotiSetting();
            if (userNotiSetting.isFitmeternotusednoti())
            {
                setFitmeterNotUseAlarm();
            }
        }



        //현재까지 받은 데이터를 업로드한다.
        if(Utils.isOnline(this)) {
            Intent uploadIntent = new Intent(this, DataUploadService.class);
            startService(uploadIntent);
        }

    }

    private void updateHome()
    {
        mInitialWeekDisplay = true;

        //액티비티가 백그라운드에서 포어그라운드로 변경될 때
        //오늘 날짜, 주 날짜를 초기화한다.
        Calendar calendar = Calendar.getInstance();
        calendar.setTime( new Date() );

        mTodayCalendar = Calendar.getInstance();
        mTodayCalendar.setTimeInMillis(calendar.getTimeInMillis());
        mDay = mTodayCalendar.get( Calendar.DAY_OF_MONTH );
        mTodayWeekNumber = mTodayCalendar.get(Calendar.DAY_OF_WEEK);

        this.mWeekCalendar = Calendar.getInstance();
        this.mWeekCalendar.setTimeInMillis(calendar.getTimeInMillis());
        int weekNumber = mWeekCalendar.get(Calendar.DAY_OF_WEEK);
        mWeekCalendar.add(Calendar.DATE, -(weekNumber - 1));

        //날짜 관련 뷰
        setDateText();
        
        //주간 그래프의 크기를 결정한다.
        try {

            Calendar endCalendar = Calendar.getInstance();
            endCalendar.set(mWeekCalendar.get(Calendar.YEAR), mWeekCalendar.get(Calendar.MONTH), mWeekCalendar.get(Calendar.DATE));
            long diffDates = DateUtils.diffOfDate(endCalendar);
            mWeekPagerLength = ((int) (diffDates / 7) + 1);
        } catch (Exception e) {
            mWeekPagerLength = 1000000;
        }

        mWeekCurrentItem = getWeekDiff();

        //주간 그래프의 주 정보 배열을 초기화한다.
        mWeekPagerActivityInfoList = new WeekPagerActivityInfo[ mWeekPagerLength ];

        //배터리를 설정한다.
        setBattery();

        //뉴스를 보여준다.
        if (Utils.isOnline(this)) {
            this.getNewsList();
        } else {
            myApplication.addNetworkChangeListener(this);
            this.displayNews(null);
        }

        if( mWeekBarPagerAdapter != null ) {
            vpWeekBar.removeOnPageChangeListener(mWeekBarPagerAdapter);
        }

        if( mWeekTablePagerAdapter != null ) {
            vpWeekTable.removeOnPageChangeListener(mWeekTablePagerAdapter);
        }

        getWeekActivity();
    }

    /**
     * 액티비티가 꺼질 때 이벤트 핸들러
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(connectPopup != null)
        {
            if (connectPopup.isShowing() )
            {
                connectPopup.dismiss();
                connectPopup = null;
                connectPopupDismissHandler.removeCallbacks( conntPopupDismissRunnable);
            }
        }

        myApplication.deleteNetworkChangeListener(this);

        //운동 프로그램이나 사용자 정보가 변경되면 일 주 그래프를 업데이트한다.
        //this.unregisterReceiver(mProgramChangetReceiver);
        this.unregisterReceiver(mChangeReceiver);

        writeSelectedCategoryIndex();

        Log.e(TAG, "OnDestroy()");
    }

    private void writeSelectedCategoryIndex() {
        SharedPreferences pref = this.getSharedPreferences("fitmate", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(SELECTEDCATEGORYINDEX_KEY, selectedCategoryIndex);
        editor.commit();
    }

    /**
     * 백버튼 눌렀을 때 이벤트 핸들러.
     */
    @Override
    public void onBackPressed() {

        if (dlNewHome.isDrawerOpen(drawerView)) {
            dlNewHome.closeDrawers();
            return;
        }

        if (newHomeUtils.programPopup != null) {
            if (newHomeUtils.programPopup.isShowing()) {
                newHomeUtils.programPopup.dismiss();
                newHomeUtils.programPopup = null;
                return;
            }
        }

        if (newHomeUtils.categoryIntroPopup != null) {

            if (newHomeUtils.categoryIntroPopup.isShowing()) {
                newHomeUtils.categoryIntroPopup.dismiss();
                newHomeUtils.categoryIntroPopup = null;
                return;
            }
        }

        if (calendarPopup != null) {
            if (calendarPopup.isShowing()) {
                calendarPopup.dismiss();
                return;
            }
        }

        if (batteryPopup != null) {
            if (batteryPopup.isShowing()) {
                batteryPopup.dismiss();
                return;
            }
        }

        if (hsvCategoryEdit.getVisibility() == View.VISIBLE) {
            cancelCategoryEdit();
            return;
        }

        if( llMetContainer.getVisibility() == View.VISIBLE ){
            dismissMetGraph();
            return;
        }

        if( metDialog != null ){
            if( metDialog.isShowing() ){
                metDialog.dismiss();
                metDialog = null;
            }
        }

        final FitmeterDialog fDialog = new FitmeterDialog(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View contentView = inflater.inflate(R.layout.dialog_default_content, null);
        TextView tvTitle = (TextView) contentView.findViewById(R.id.tv_dialog_default_title);
        tvTitle.setText(this.getString(R.string.common_quit_title));
        TextView tvContent = (TextView) contentView.findViewById(R.id.tv_dialog_default_content);
        tvContent.setText(this.getString(R.string.common_quit_content));
        fDialog.setContentView(contentView);

        View buttonView = inflater.inflate(R.layout.dialog_button_two , null);
        Button btnLeft = (Button) buttonView.findViewById(R.id.btn_dialog_button_two_left);
        btnLeft.setText(this.getString(R.string.common_no));
        btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fDialog.close();
            }
        });

        Button btnRight = (Button) buttonView.findViewById(R.id.btn_dialog_button_two_right);
        btnRight.setText(this.getString(R.string.common_yes));
        btnRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moveTaskToBack(true);
                finish();
                fDialog.close();
            }
        });

        fDialog.setButtonView(buttonView);
        fDialog.show();
    }

    /**
     * 사용자가 블루투스를 켰는지 확인하는 이벤트 핸들러.
     *
     * @param resultCode
     */
    @OnActivityResult(ENABLE_BT_REQUEST_ID)
    void btResult(int resultCode) {
        this.setCheckBluetoothOn(true);
        if (resultCode == RESULT_CANCELED) {
            Log.i(TAG, "블루투스 요청 거부");
            this.setInitialViewComplete(true);
            isDeviceFound = false;
        } else if (resultCode == RESULT_OK) {
            Log.i(TAG, "블루투스 요청 허용");
            //showBatteryView();
            //setBattery();
        }
    }

    @AfterViews
    void init()
    {
        this.mCategoryViewOrder = Utils.readTabViewOrder(this);
        this.selectedCategoryIndex = Utils.readSelectedCategoryIndex(this);

        if( selectedCategoryIndex > (mCategoryViewOrder.length - 1 ) )
        {
            selectedCategoryIndex = mCategoryViewOrder.length - 1;
        }

        newsServiceClient.setRootUrl(NetworkClass.baseURL + "/api");
        this.newHomeUtils = new NewHomeUtils(this, mDBManager);

        //주간 그래프의 크기를 결정한다.
        try {

            Calendar endCalendar = Calendar.getInstance();
            endCalendar.set(mWeekCalendar.get(Calendar.YEAR), mWeekCalendar.get(Calendar.MONTH), mWeekCalendar.get(Calendar.DATE));
            long diffDates = DateUtils.diffOfDate(endCalendar);
            mWeekPagerLength = ((int) (diffDates / 7) + 1);
        } catch (Exception e) {
            mWeekPagerLength = 1000000;
        }

        mWeekCurrentItem = mWeekPagerLength;

        //뉴스 액티비티에서 자신의 활동 뉴스를 클릭하면 홈이 뜨게 되는데
        //활동 뉴스에 해당하는 날짜의 데이타가 보여줘야 한다. 뉴스의 날짜로 설정한다.
        if (mLaunchMode.equals(LaunchMode.FROM_NEWSCLICK))
        {
            mDayCalendar.setTimeInMillis(this.getIntent().getLongExtra(HOME_NEWSDATE_KEY, System.currentTimeMillis()));
            mWeekCurrentItem = getWeekDiff();
        }

        //날짜 관련 뷰
        setDateText();

        //사이드 메뉴
        this.drawerView.setParentActivity(this);
        this.drawerView.setTag(this.getResources().getString(R.string.gnb_home));
        this.drawerView.setDrawerLayout(dlNewHome);

        //주간 그래프의 주 정보 배열을 초기화한다.
        mWeekPagerActivityInfoList = new WeekPagerActivityInfo[mWeekPagerLength];

        //뉴스를 보여준다.
        if (Utils.isOnline(this)) {
            this.getNewsList();
        } else {
            myApplication.addNetworkChangeListener(this);
            this.displayNews(null);
        }

        //상단 탭들의 뷰를 생성한다.
        initCategoryView();
        selectTabStyle();
        rlTabBar.setOnClickListener(new ActionBarClickListener());

        getWeekActivity();
    }

    private void setBattery()
    {
        if( !isDeviceFound)
        {

            int[] batteryAttrs = new int[]{ R.attr.newhomeBatteryNone  };
            TypedArray batteryTypedArray = this.obtainStyledAttributes(  MyApplication.tabStyleList[ MyApplication.mSelectedTabStyleIndex ]  , batteryAttrs);
            int batteryNone = batteryTypedArray.getColor( 0, 0 );
            batteryTypedArray.recycle();

            imgBatteryValue.setVisibility( View.VISIBLE );
            tvBatteryValue.setVisibility( View.INVISIBLE );
            //imgBatteryLed.setVisibility( View.INVISIBLE );

            imgBatteryValue.setColorFilter( batteryNone );

            final Animation batteryNoneAnimation = AnimationUtils.loadAnimation(this, R.anim.battery_none);
            imgBatteryValue.setAnimation(batteryNoneAnimation);
            batteryNoneAnimation.start();

            batteryCircleView.setBatteryValue(100);
            batteryCircleView.invalidate();

            //tvBatteryValue.setMessage( "!" );

        }else{

            imgBatteryValue.setVisibility( View.INVISIBLE );
            imgBatteryValue.setAnimation(null);
            tvBatteryValue.setVisibility(View.VISIBLE);
            //imgBatteryLed.setVisibility( View.VISIBLE );
            int batteryRatio = mDBManager.getLastBatteryRatio( mUserInfo.getEmail() );

            batteryCircleView.setBatteryValue( batteryRatio );
            batteryCircleView.invalidate();
//            if (batteryRatio < 21) {
//                imgBatteryLed.setImageResource(R.drawable.batter_light_empty);
//            } else if (batteryRatio < 51) {
//                imgBatteryLed.setImageResource(R.drawable.batter_light_medium);
//            } else {
//                imgBatteryLed.setImageResource(R.drawable.batter_light_full);
//            }

            String batteryValueStrng = "!";
            if (batteryRatio > 0) {
                batteryValueStrng = batteryRatio + "";
            }
            tvBatteryValue.setText(batteryValueStrng);
        }
    }

    private void setDateText() {

        Date activityDate = mDayCalendar.getTime();
        Date todayDate = new Date();

        String weekDay = null;
        weekDay = this.getResources().getStringArray(R.array.week_string)[mDayCalendar.get(Calendar.DAY_OF_WEEK) - 1];

        tvWeekDay.setText(weekDay);
        //tvWeekDay.setTextColor( getResources().getColor( R.color.day_caledanr ) );
        tvDate.setText(DateUtils.getDateStringForPattern(mDayCalendar.getTime(), this.getString(R.string.newhome_date_format)));
        //tvDate.setTextColor( getResources().getColor( R.color.day_caledanr ) );
    }

    //저장되어 있는 카테고리 View 순서를 가져온다.
    private void getViewOrder() {

    }

    /**
     * 주간 데이터를 가져온다.
     * 로컬 DB에 주간 데이터가 없으면 서버로부터 가져온다.
     */
    @Background
    void getWeekActivity()
    {
        String weekStartDate = newHomeUtils.getWeekStartDate(mDayCalendar);
        WeekActivity weekActivity = mDBManager.getWeekActivity(weekStartDate);

        if (weekActivity == null)
        {
            Log.d(TAG , "DB에 주간 데이터가 널임");
            WeekActivityWithDayActivityList weekActivityWithDayActivityList = null;
            try {
                weekActivityWithDayActivityList = activityServiceClient.getWeekActivityWithDayActivityList(mUserInfo.getEmail(), weekStartDate);
            } catch (RestClientException e) {
                Log.d(TAG, weekStartDate + " 주간 데이터를 가져오는 중에 오류가 발생하였습니다. 오류내용 :" + e.getMessage());
            }


            if (weekActivityWithDayActivityList != null)
            {
                if (!weekActivityWithDayActivityList.getResponseValue().equalsIgnoreCase("nodata") )
                {
                    setWeekInfoDayActivityList( weekActivityWithDayActivityList );

                }else{
                    Log.e(TAG , weekStartDate + " 주간데이터가 서버에 없습니다. nodata");
                }

            }else{
                Log.e(TAG , weekStartDate + " 주간데이터가 널입니다.");
            }

            Log.e(TAG , "주간 데이터 저장 완료.");
        }

        display();
    }

    @Background
    void getWeekActivity(String weekStartDate, WeekActivityListener listener) {

        WeekActivityWithDayActivityList weekActivityWithDayActivityList = null;
        try {
            weekActivityWithDayActivityList = activityServiceClient.getWeekActivityWithDayActivityList(mUserInfo.getEmail(), weekStartDate);
        } catch (RestClientException e) {

            Log.d(TAG, weekStartDate + " 주간 데이터를 가져오는 중에 오류가 발생하였습니다. 오류내용 :" + e.getMessage());
        }

        if (weekActivityWithDayActivityList != null) {
            if (!weekActivityWithDayActivityList.getResponseValue().equals("nodata")) {
                setWeekInfoDayActivityList(weekActivityWithDayActivityList);
            }else{
                Log.d( TAG , weekStartDate + " 주간 데이터가 서버에 없습니다. nodata" );
            }
        }else{
            Log.d(TAG, weekStartDate + " 주간 데이터가 널입니다.");
        }

        listener.OnWeekActivityReceived();

    }


    @UiThread
    void display() {
        displayDayData(true);
        displayWeekData();

        if (rlWeekProgress.getVisibility() == View.VISIBLE) {
            dismissProgressView();
        }
    }

    private void displayDayData(boolean startZero) {

        DayActivity dayActivity = mDBManager.getDayActivity(newHomeUtils.getDateString(mDayCalendar));

        if (dayActivity != null) {
            if (dayActivity.getFat() < 0) {
                FatCarboDistance fatCarboDistance = this.calculateFatCarboDistance(dayActivity);
                dayActivity.setFat(fatCarboDistance.fat);
                dayActivity.setCarbohydrate(fatCarboDistance.carbohydrate);
                dayActivity.setDistance(fatCarboDistance.distance);
            }
        } else {
            dayActivity = new DayActivity();
        }

        ExerciseProgram program = getExerciseProgram();

        if (mInitialDayDisplay) {
            addDayData(dayActivity, program, startZero);

            mInitialDayDisplay = false;
        } else {
            updateDayData(dayActivity, program, startZero);
        }

    }

    private ExerciseProgram getExerciseProgram() {

        ExerciseProgram program = null;
        String weekStartDate = newHomeUtils.getWeekStartDate(mDayCalendar);
        WeekActivity weekActivity = mDBManager.getWeekActivity(weekStartDate);

        if (weekActivity != null) {
            program = mDBManager.getUserExerciProgram(weekActivity.getExerciseProgramID());
        } else {
            program = mDBManager.getAppliedProgram();
        }

        return program;
    }



    private void addDayData(DayActivity dayActivity, ExerciseProgram program, boolean startZero) {

        for (int categoryIndex = 0; categoryIndex < CategoryType.values().length; categoryIndex++)
        {
            CategoryType categoryType = CategoryType.values()[categoryIndex];

            NewHomeCategoryView categoryView = categoryViews[categoryIndex];

            View dayView = dayViews[CategoryType.values()[categoryIndex].ordinal()];
            newHomeUtils.setDayValue(categoryView, dayView, NewHomeCategoryData.getNewHomeCategoryData(this, dayActivity, program, mUserInfo), program.getId(), categoryType, startZero);
            categoryView.unSelect();
        }
        View[] dayGraphs = new View[mCategoryViewOrder.length];
        float lastViewRight = 0;
        float defaultMargin = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 26.9f, getResources().getDisplayMetrics());
        float categoryViewWidth = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 107.5f, getResources().getDisplayMetrics());
        for (int viewIndex = 0; viewIndex < mCategoryViewOrder.length; viewIndex++) {
            CategoryType categoryType = mCategoryViewOrder[viewIndex];
            final NewHomeCategoryView categoryView = categoryViews[categoryType.ordinal()];
            View dayView = dayViews[categoryType.ordinal()];

            dayView.setTag(viewIndex);
            categoryView.setViewIndex(viewIndex);
            newHomeUtils.setCategorySelect(categoryView, viewIndex, selectedCategoryIndex);
            dayGraphs[viewIndex] = dayView;

            if ( viewIndex > 0)
            {
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
                params.leftMargin = (int) ( lastViewRight - defaultMargin );
                lastViewRight = lastViewRight + ( categoryViewWidth - defaultMargin );
                rlCategory.addView( categoryView , params);
            }else {
                lastViewRight = categoryViewWidth;
                rlCategory.addView(categoryView);
            }
        }

        //카테고리 수정버튼을 추가한다.
        mCategoryEditCategoryView.setCategoryText(getResources().getString(R.string.tab_setting));
        mCategoryEditCategoryView.setOnClickListener(new CategoryEditClickListener());

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
        params.leftMargin = (int) ( lastViewRight - defaultMargin );
        rlCategory.addView(mCategoryEditCategoryView , params);

        rlCategory.post(new Runnable() {
            @Override
            public void run() {
                changeCategoryViewGravity(rlCategory, hsvCategory);
            }
        });

        mDayPagerAdapter = new DayPagerAdapter(this, dayGraphs , mCategoryViewOrder);
        vpDay.setAdapter(mDayPagerAdapter);
        vpDay.setCurrentItem(selectedCategoryIndex);
        vpDay.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                (( NewHomeCategoryView) rlCategory.getChildAt(selectedCategoryIndex) ).unSelect();
                int preSelectedCategoryIndex = selectedCategoryIndex;
                selectedCategoryIndex = position;
                NewHomeCategoryView newHomeCategoryView = (NewHomeCategoryView) rlCategory.getChildAt(selectedCategoryIndex);
                newHomeCategoryView.select();

                if (preSelectedCategoryIndex > selectedCategoryIndex) { //카테고리를 왼쪽에서 오른쪽으로 이동하는 경우

                    int viewLeft = newHomeCategoryView.getLeft();
                    if (hsvCategory.getScrollX() > viewLeft) {
                        hsvCategory.smoothScrollTo(viewLeft, 0);
                    }
                } else if (preSelectedCategoryIndex < selectedCategoryIndex) { //카테고리를 오른족에서 왼쪽으로 이동하는 경우

                    int viewRight = newHomeCategoryView.getRight();
                    if (viewRight > (hsvCategory.getScrollX() + hsvCategory.getRight())) {

                        hsvCategory.smoothScrollTo(viewRight - hsvCategory.getRight(), 0);
                    }
                }

//                //일 그래프를 애니메이션 시킨다.
                CategoryType preCategoryType = mCategoryViewOrder[preSelectedCategoryIndex];
                CategoryType categoryType = mCategoryViewOrder[selectedCategoryIndex];
                DayPagerAdapter dayPagerAdapter = (DayPagerAdapter) vpDay.getAdapter();
                View view = dayPagerAdapter.getDayGraphs()[selectedCategoryIndex];
                if( categoryType.equals( CategoryType.WEEKLY_ACHIEVE ) ){

                    NewHomeDayCircleView dayCircleView = (NewHomeDayCircleView)view;
                    //dayCircleView.startAnimation( true );
                    dayCircleView.changeMargin();

                }else if( categoryType.equals( CategoryType.DAILY_ACHIEVE ) ) {

                    NewHomeDayHalfCircleView dayHalfCircleView = (NewHomeDayHalfCircleView) view;
                    //dayHalfCircleView.startAnimation(true);
                    dayHalfCircleView.changeMargin();
                }
//                }else {
//                    NewHomeDayTextView dayTextView = (NewHomeDayTextView) view;
//                    dayTextView.startAnimation( true );
//                }


                // 주 그래프를 선택한다.
                if (categoryType.equals(CategoryType.WEEKLY_ACHIEVE)) {

                    vpWeekBar.setVisibility(View.INVISIBLE);
                    vpWeekTable.setVisibility(View.VISIBLE);
                    mWeekTablePagerAdapter.setDayTimeMilliseconds(mDayCalendar.getTimeInMillis());
                    mWeekTablePagerAdapter.setSelectedDayIndex(mWeekBarPagerAdapter.getSelectedDayIndex());

                    if (vpWeekTable.getCurrentItem() != vpWeekBar.getCurrentItem()) {

                        mWeekTablePagerAdapter.setMoveWeek(false);
                        vpWeekTable.setCurrentItem(vpWeekBar.getCurrentItem());

                    }

                } else if (preCategoryType.equals(CategoryType.WEEKLY_ACHIEVE)) {

                    vpWeekBar.setVisibility(View.VISIBLE);
                    vpWeekTable.setVisibility(View.INVISIBLE);
                    mWeekBarPagerAdapter.setDayTimeMilliseconds(mDayCalendar.getTimeInMillis());
                    mWeekBarPagerAdapter.setSelectedDayIndex(mWeekTablePagerAdapter.getSelectedDayIndex());
                    mWeekBarPagerAdapter.changeCategoryType(categoryType);
                    if (vpWeekTable.getCurrentItem() != vpWeekBar.getCurrentItem()) {

                        mWeekBarPagerAdapter.setMoveWeek(false);
                        vpWeekBar.setCurrentItem(vpWeekTable.getCurrentItem());

                    }

                } else {
                    mWeekBarPagerAdapter.changeCategoryType(categoryType);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    private void updateDayData(DayActivity dayActivity, ExerciseProgram program, boolean startZero)
    {

        for (int categoryIndex = 0; categoryIndex < CategoryType.values().length; categoryIndex++) {
            NewHomeCategoryView categoryView = categoryViews[categoryIndex];
            View dayView = dayViews[CategoryType.values()[categoryIndex].ordinal()];
            newHomeUtils.setDayValue(categoryView, dayView, NewHomeCategoryData.getNewHomeCategoryData(this, dayActivity, program, mUserInfo), program.getId(), CategoryType.values()[categoryIndex], startZero);
        }

    }

    /**
     * 주간 데이터를 그린다.
     */
    private void displayWeekData() {

        if (mInitialWeekDisplay) {
            addWeekData();
            mInitialWeekDisplay = false;
        } else {
            updateWeekData();
        }
    }

    private void addWeekData() {

        CategoryType barCategoryType = mCategoryViewOrder[selectedCategoryIndex];
        if (mCategoryViewOrder[selectedCategoryIndex].equals(CategoryType.WEEKLY_ACHIEVE)) {
            barCategoryType = CategoryType.DAILY_ACHIEVE;
        }

        mWeekBarPagerAdapter = new NewHomeWeekPagerAdapter(this, mWeekPagerLength, barCategoryType, new WeekCellClickListener(), this, mDayCalendar.getTimeInMillis(), WeekPagerAdapter.WeekPagerType.WEEK_BAR, mWeekPagerActivityInfoList);
        mWeekBarPagerAdapter.setMoveWeek(false);
        vpWeekBar.addOnPageChangeListener(mWeekBarPagerAdapter);
        vpWeekBar.setAdapter(mWeekBarPagerAdapter);
        vpWeekBar.setCurrentItem(mWeekCurrentItem);

        mWeekTablePagerAdapter = new NewHomeWeekPagerAdapter(this, mWeekPagerLength, CategoryType.WEEKLY_ACHIEVE, new WeekCellClickListener(), this, mDayCalendar.getTimeInMillis(), WeekPagerAdapter.WeekPagerType.WEEK_TABLE, mWeekPagerActivityInfoList);
        mWeekTablePagerAdapter.setMoveWeek(false);
        vpWeekTable.addOnPageChangeListener(mWeekTablePagerAdapter);
        vpWeekTable.setAdapter(mWeekTablePagerAdapter);
        vpWeekTable.setCurrentItem(mWeekCurrentItem);

        CategoryType categoryType = mCategoryViewOrder[selectedCategoryIndex];
        if (categoryType.equals(CategoryType.WEEKLY_ACHIEVE)) {

            vpWeekBar.setVisibility(View.INVISIBLE);
            vpWeekTable.setVisibility(View.VISIBLE);

        } else {

            vpWeekBar.setVisibility(View.VISIBLE);
            vpWeekTable.setVisibility(View.INVISIBLE);

        }
    }

    private int getWeekDiff() {

        Calendar weekCalendar = Calendar.getInstance();
        weekCalendar.setTimeInMillis(mDayCalendar.getTimeInMillis());
        int weekNumber = weekCalendar.get(Calendar.DAY_OF_WEEK);
        weekCalendar.add(Calendar.DATE, -(weekNumber - 1));

        long diff = mWeekCalendar.getTimeInMillis() - weekCalendar.getTimeInMillis();
        long diffDays = diff / (24 * 60 * 60 * 1000);

        int weekDiff = (int) (diffDays / 7);

        return mWeekPagerLength - (weekDiff + 1);
    }

    private void updateWeekData() {
        int position = getWeekDiff();
        int selectedDayIndex = mDayCalendar.get(Calendar.DAY_OF_WEEK);

        if (vpWeekBar.getVisibility() == View.VISIBLE) {

            mWeekBarPagerAdapter.setDayTimeMilliseconds(mDayCalendar.getTimeInMillis());
            mWeekBarPagerAdapter.setSelectedDayIndex(selectedDayIndex - 1);
            mWeekBarPagerAdapter.setMoveWeek(false);
            vpWeekBar.setCurrentItem(position);

        } else if (vpWeekTable.getVisibility() == View.VISIBLE) {

            mWeekTablePagerAdapter.setDayTimeMilliseconds(mDayCalendar.getTimeInMillis());
            mWeekTablePagerAdapter.setSelectedDayIndex(selectedDayIndex - 1);
            mWeekTablePagerAdapter.setMoveWeek(false);
            vpWeekTable.setCurrentItem(position);

        }
    }

    @Click(R.id.ll_activity_newhome_calendar)
    void calendarClick()
    {
        showCalendar();
    }

    @Click(R.id.img_activity_newhome_calendar_arrow)
    void calendarArrowClick(){
        showCalendar();
    }

    private void showCalendar(){
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.calendarMain = inflater.inflate(R.layout.calendar_view, null);
        this.calendarPopup = new PopupWindow(calendarMain, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        this.calendarPopup.setAnimationStyle(-1);
        //this.calendarPopup.showAsDropDown(imgClendar);

        //팝업의 y좌표를 가져온다.
        //상단의 statusbar 아래에서 보여줘야 하기 때문에
        //홈에서 제일 상단의 뷰의 위치의 y좌표부터 팝업을 보여주는 것으로 함.
        int[] loc = new int[2];
        rlActionBar.getLocationOnScreen(loc);

        this.calendarPopup.showAtLocation(calendarMain, Gravity.CENTER, 0, loc[1]);

        final CalendarView calendarView = (CalendarView) calendarMain.findViewById(R.id.cv_activity_newhome);
        calendarView.setDate(mDayCalendar.getTimeInMillis(), false, true);
        calendarView.setMaxDate(mTodayCalendar.getTimeInMillis());
        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {

            private int preYear = mDayCalendar.get(Calendar.YEAR);
            private int preMonth = mDayCalendar.get(Calendar.MONTH);
            private int preDay = mDayCalendar.get(Calendar.DAY_OF_MONTH);

            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {

                if (preYear != year || preMonth != month || preDay != dayOfMonth) {

                    //같은 주인지 확인한다.
                    Calendar preCalendar = Calendar.getInstance();
                    preCalendar.setTimeInMillis( mDayCalendar.getTimeInMillis() );

                    mDayCalendar.set(year, month, dayOfMonth);
                    boolean isSameWeek = true;
                    if( preYear != year ){
                        isSameWeek = false;
                    }else {
                        int preWeekNumber = preCalendar.get(Calendar.WEEK_OF_YEAR);
                        int weekNumber = mDayCalendar.get(Calendar.WEEK_OF_YEAR);
                        if( preWeekNumber != weekNumber ){
                            isSameWeek = false;
                        }
                    }

                    changeDay( isSameWeek );
                    calendarPopup.dismiss();
                }
            }

        });
    }

    @Click(R.id.img_activity_newhome_drawer)
    void drawerClick() {

        //dismissConnectPopup();
        dlNewHome.openDrawer(drawerView);

    }

    private boolean isBtEnabled() {
        final BluetoothManager manager = (BluetoothManager) this.getSystemService(Context.BLUETOOTH_SERVICE);
        if (manager == null) return false;

        final BluetoothAdapter adapter = manager.getAdapter();
        if (adapter == null) return false;
        return adapter.isEnabled();
    }

    @Background
    void getNewsList()
    {

        Log.i("fitmate", "GetNewsList()");
        try {
            NewsResult newsResult = newsServiceClient.getNewsList( mUserInfo.getEmail(), 0);
            displayNews(newsResult);
        } catch (RestClientException e) {
            Log.e("fitamte", "GetNewsList : " + e.getMessage());
        }
    }

    @UiThread
    void displayNews(NewsResult newsResult) {

        View[] newsViews = null;

        if (newsResult != null) {
            if (newsResult.getResponsevalue().equals(CommonKey.SUCCESS)) {
                List<News> newsList = newsResult.getNewsList();
                if (newsList != null) {
                    if (newsList.size() > 0) {

                        newsViews = new View[newsList.size()];
                        for (int i = 0; i < newsList.size(); i++) {
                            News news = newsList.get(i);

                            NewHomeNewsView newHomeNewsView = NewHomeNewsView_.build(this);
                            newHomeNewsView.setNews(news);
                            newHomeNewsView.setTag(news.getId());
                            newHomeNewsView.setOnClickListener(new NewsClickListener());
                            newsViews[i] = newHomeNewsView;
                        }
                    } else {
                        newsViews = new View[]{newHomeUtils.getDefaultNewsView()};
                    }
                } else {
                    newsViews = new View[]{newHomeUtils.getDefaultNewsView()};
                }
            } else {
                newsViews = new View[]{newHomeUtils.getDefaultNewsView()};
            }
        } else {
            newsViews = new View[]{newHomeUtils.getDefaultNewsView()};
        }

        NewsPagerAdapter newsPagerAdapter = new NewsPagerAdapter(this, newsViews);
        vpNews.setAdapter(newsPagerAdapter);

        //vpNews.setPageTransformer(true, new ZoomOutPageTransformer());

        pagerRunnable.setPosition(vpNews.getCurrentItem());
        pagerRunnable.setCount(newsPagerAdapter.getCount());
        vpNews.postDelayed(pagerRunnable, PagerAnimationDuration);
        vpNews.setTag(0);
    }

    private void setCheckBluetoothOn(boolean checkBluetoothOn) {
        SharedPreferences pref = this.getSharedPreferences("fitmateservice", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(LoginPresenter.CHECK_BLUETOOTHON_KEY, checkBluetoothOn);
        editor.commit();
    }

    private Boolean getCheckBluetoothOn() {
        SharedPreferences pref = this.getSharedPreferences("fitmateservice", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        return pref.getBoolean(LoginPresenter.CHECK_BLUETOOTHON_KEY, false);
    }

    private Boolean getRequestBluetoothOn() {
        SharedPreferences pref = this.getSharedPreferences("fitmateservice", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        return pref.getBoolean(LoginPresenter.REQUEST_BLUETOOTHON_KEY, false);
    }

    private void setRequestBluetoothOn(boolean requestBluetoothOn) {
        SharedPreferences pref = this.getSharedPreferences("fitmateservice", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(LoginPresenter.REQUEST_BLUETOOTHON_KEY, requestBluetoothOn);
        editor.commit();
    }

    private Boolean getInitialViewComplete() {
        SharedPreferences pref = this.getSharedPreferences("fitmateservice", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        return pref.getBoolean(LoginPresenter.INIT_VIEW_COMPLETE_KEY, false);
    }

    private void setInitialViewComplete(boolean initViewComplete) {
        SharedPreferences pref = this.getSharedPreferences("fitmateservice", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(LoginPresenter.INIT_VIEW_COMPLETE_KEY, initViewComplete);
        editor.commit();
    }

    private boolean getServerUpdate() {
        SharedPreferences pref = this.getSharedPreferences("fitmateservice", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        return pref.getBoolean(DayActivity.DAY_ACTIVITY_SERVER_UPDATE_KEY, false);
    }

    public void dismissSyncView() {
        rlSync.setVisibility(View.GONE);
        imgSync.clearAnimation();
    }

    private void showBatteryView() {
        dismissSyncView();
        rlBattery.setVisibility(View.VISIBLE);
    }

    public void showSyncView()
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //싱크뷰 돌리는 애니메이션.
                rlBattery.setVisibility(View.INVISIBLE);
                rlSync.setVisibility(View.VISIBLE);
                imgSync.startAnimation(AnimationUtils.loadAnimation(NewHomeActivity.this, R.anim.rotate_sync));
                tvSyncPercent.setText("0");
            }
        });
    }

    public void calculateCompleted(final String weekUpdateList) {

        isRequestCalculate = false;

        if (weekUpdateList == null) {
            return;
        }

        //홈을 끄지 않고 오래동안 두면 홈 화면이 업데이트가 되지 않는 문제가 생긴다.
        //홈을 끄지 않는 채로 시간이 다음주로 넘어가게 되면 주간 그래프가 업데이트 되지 않아 이번주 그래프만 확인하게 된다.
        //그래서 날이 변할 때 홈 화면을 갱신하기로 함. 날이 변하는 건 alarmservice가 알려주는 것으로 함.
        //그런데 갱신 과정에 있을 때 이 함수(calculateCompleted)가 호출되면 에러가 발생한다. 갱신하는 과정에 있는 객체를 불러올려고 하기 때문이다.
        //그래서 아래 코드와 같이 mInitialWeekDisplay가 true일 때 즉 갱신과정이 끝나지 않았다면 이 함수를 실행하지 않도록 하였다.
        if( mInitialWeekDisplay ) return;

        NewHomeWeekPagerAdapter updateWeekPager = null;
        if (vpWeekBar.getVisibility() == View.VISIBLE) {
            updateWeekPager = mWeekBarPagerAdapter;
        } else if (vpWeekTable.getVisibility() == View.VISIBLE) {
            updateWeekPager = mWeekTablePagerAdapter;
        }

        if (updateWeekPager == null) {
            return;
        }

        //업데이트 된 주목록에 현재 날짜가 포함 되는지 포함된다면 일 데이터를 업데이트한다.
        //업데이트 된 주 목록에 현재 주간 그래프 날짜가 포함된다면 주 데이터를 업데이트한다.
        String currentWeekStartDate = newHomeUtils.getWeekStartDate(mDayCalendar);
        CategoryType categoryType = mCategoryViewOrder[selectedCategoryIndex];

        if (weekUpdateList != null)
        {

            try {
                JSONArray jsonWeekArray = new JSONArray(weekUpdateList);
                for (int weekUpdateIndex = 0; weekUpdateIndex < jsonWeekArray.length(); weekUpdateIndex++)
                {

                    JSONObject jsonWeekObject = jsonWeekArray.getJSONObject(weekUpdateIndex);
                    String weekUpdateDate = jsonWeekObject.getString("weekdate");

                    //서비스로부터 받은 주시작 날짜와 현재 보여지고 있는 일의 주시작 날짜가 같다면 일 그래프를 업데이트한다.
                    if (weekUpdateDate.equals(currentWeekStartDate)) {
                        displayDayData(false);
                    }

                    JSONArray jsonDayArray = jsonWeekObject.getJSONArray("daylist");

                    int[] childIndexList = new int[ jsonDayArray.length() ];
                    double[] valueList = new double[jsonDayArray.length()];
                    DayActivity[] dayActivityList = new DayActivity[ jsonDayArray.length() ];

                    for (int daysIndex = 0; daysIndex < jsonDayArray.length(); daysIndex++)
                    {
                        JSONObject jsonDayObject = jsonDayArray.getJSONObject(daysIndex);
                        String dayDate = jsonDayObject.getString("daydate");
                        int dayIndex = jsonDayObject.getInt("dayindex");

                        childIndexList[daysIndex] = dayIndex - 1;

                        DayActivity dayActivity = mDBManager.getDayActivity(dayDate);
                        dayActivityList[ daysIndex ] = dayActivity;

                        valueList[daysIndex] = getDayValue( categoryType, dayActivity );

                    }

                    int position = updateWeekPager.getPosition( weekUpdateDate );
                    if (position > -1)
                    {
                        mWeekPagerActivityInfoList[position].updateData(childIndexList, dayActivityList , mUserInfo.isSI() );
                    }

                    //업데이트 된 주 목록에 현재 주간 그래프 날짜가 포함된다면 주 데이터를 업데이트한다.
                    String[] weekStringList = updateWeekPager.getWeekStringList();
                    if (weekStringList != null)
                    {
                        for (int weekIndex = 0; weekIndex < weekStringList.length; weekIndex++)
                        {

                            if (weekStringList[weekIndex] != null) {
                                String weekStartDate = weekStringList[weekIndex];
                                if (weekUpdateDate.equals(weekStartDate))
                                {
                                    NewHomeWeekView newHomeWeekView = (NewHomeWeekView) updateWeekPager.getView(weekStartDate);
                                    newHomeWeekView.updateView(childIndexList, valueList);
                                    weekStringList[weekIndex] = null;
                                    break;
                                }
                            }
                        }
                    }

                }

            } catch (JSONException e) {

            }
        }

        //앱에 분석된 활동량결과를 서버에 업로드한다.
        if (getServerUpdate())
        {
            if (Utils.isOnline( NewHomeActivity.this )) {

                Log.i(TAG, "앱 서버 동기화");
                List<DayActivity> uploadDayActivityList = mDBManager.getNotUploadDayActivity();
                List<WeekActivity> uploadWeekActivityList = mDBManager.getNotUploadWeekActivity();

                mActivityDataModel.uploadActivityList(uploadDayActivityList, uploadWeekActivityList);
            }

        }

        Log.i(TAG, "기기로부터 데이터 동기화 완료");
    }

    @Override
    public void OnNetworkAvailable() {
        getNewsList();

    }

    @Override
    public void onWeekMove(long dayMilliseconds, int position, WeekPagerAdapter.WeekPagerType weekPagerType) {
        //주간 그래프에 따라 일 날짜를 변경시킨다.
        mDayCalendar.setTimeInMillis( dayMilliseconds );

        //일 그래프를 다시 그린다.
        setDateText();
        displayDayData(false);

    }

    private double getDayValue(CategoryType categoryType, DayActivity dayActivity ) {
        double value = 0;
        switch (categoryType) {
            case EXERCISE_TIME:
                value = dayActivity.getStrengthHigh() + dayActivity.getStrengthMedium();
                break;
            case CALORIES:
                value = dayActivity.getCalorieByActivity();
                break;
            case DAILY_ACHIEVE:
                value = dayActivity.getAchieveOfTarget();
                break;
            case WEEKLY_ACHIEVE:
                value = dayActivity.getTodayScore();
                break;
            case FAT:
                double fat = dayActivity.getFat();
                if (fat >= 0) {
                    value = dayActivity.getFat();
                } else {
                    value = this.calculateFatCarboDistance(dayActivity).fat;
                }

                if( !mUserInfo.isSI() )
                {
                    value = Utils.convertGramToOunce( value );
                }
                break;
            case CARBOHYDRATE:
                double carbo = dayActivity.getCarbohydrate();
                if (carbo >= 0) {
                    value = dayActivity.getCarbohydrate();
                } else {
                    value = this.calculateFatCarboDistance(dayActivity).carbohydrate;
                }


                if( !mUserInfo.isSI() ){
                    value = Utils.convertGramToOunce( value );
                }
                break;
            case DISTANCE:
                double distance = dayActivity.getDistance();
                if (distance >= 0) {
                    value = dayActivity.getDistance();
                } else {
                    value = this.calculateFatCarboDistance(dayActivity).distance;
                }

                if( !mUserInfo.isSI() ){
                    value = Utils.convertKMToMile( value );
                }

                break;
        }

        return value;
    }

    private FatCarboDistance calculateFatCarboDistance( DayActivity dayActivity )
    {
        UserInfoForAnalyzer userInfoForAnalyzer = com.fitdotlife.fitmate_lib.service.util.Utils.getUserInfoForAnalyzer(mUserInfo);
        ContinuousCheckPolicy policy = ContinuousCheckPolicy.Loosely_SumOverXExercise;
        com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseProgram appliedProgram = mDBManager.getAppliedExerciseProgram();
        WearingLocation location = WearingLocation.getWearingLocation( mDBManager.getUserInfo().getWearAt() );
        ExerciseAnalyzer analyzer = new ExerciseAnalyzer(appliedProgram, userInfoForAnalyzer, location , dayActivity.getDataSaveInterval());

        float fat = 0;
        float carbon = 0;
        double distance = 0;

        float[] metArray = dayActivity.getMetArray();
        List<Float> metList = new ArrayList<>();
        for (float f : metArray) {
            metList.add(f);
        }
        FatAndCarbonhydrateConsumtion fatBurn = analyzer.getFatandCarbonhydrate(metList, dayActivity.getDataSaveInterval());
        fat = fatBurn.getFatBurned();
        carbon = fatBurn.getCarbonhydrateBurned();
        distance = ExerciseAnalyzer.getDistanceFromMETArray_over2MET(metList, dayActivity.getDataSaveInterval());

        mDBManager.setFatAndCarbonAndDistance(dayActivity.getActivityDate(), fat, carbon, distance);

        FatCarboDistance fatCarboDistance = new FatCarboDistance();
        fatCarboDistance.fat = fat;
        fatCarboDistance.carbohydrate = carbon;
        fatCarboDistance.distance = distance;
        return fatCarboDistance;
    }

    private class CategoryClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            vpDay.setCurrentItem((int) view.getTag());
        }
    }

    private class WeekCellClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {

            long selectedDayTimeMillis = (long) view.getTag(R.string.newhome_weekview_time_key);
            int index = (int) view.getTag(R.string.newhome_weekview_cellindex_key);
            mDayCalendar.setTimeInMillis(selectedDayTimeMillis);

            setDateText();
            displayDayData(false);

            if (vpWeekBar.getVisibility() == View.VISIBLE) {
                mWeekBarPagerAdapter.setDayTimeMilliseconds(selectedDayTimeMillis);
                mWeekBarPagerAdapter.setSelectedDayIndex(index);
            } else if (vpWeekTable.getVisibility() == View.VISIBLE) {
                mWeekTablePagerAdapter.setDayTimeMilliseconds(selectedDayTimeMillis);
                mWeekTablePagerAdapter.setSelectedDayIndex(index);
            }
        }
    }


    private void changeDay( boolean isSameWeek ) {
        setDateText();
        Log.e("fitmate", "isSameWeek : " + isSameWeek);
        if( isSameWeek ) {

            int selectedDayIndex = mDayCalendar.get(Calendar.DAY_OF_WEEK);
            displayDayData(false);

            if (vpWeekBar.getVisibility() == View.VISIBLE) {
                mWeekBarPagerAdapter.setDayTimeMilliseconds(mDayCalendar.getTimeInMillis());
                mWeekBarPagerAdapter.setSelectedDayIndex(selectedDayIndex - 1);
            } else if (vpWeekTable.getVisibility() == View.VISIBLE) {
                mWeekTablePagerAdapter.setDayTimeMilliseconds(mDayCalendar.getTimeInMillis());
                mWeekTablePagerAdapter.setSelectedDayIndex(selectedDayIndex - 1);
            }

        }else {
            showProgressView();
            getWeekActivity();
        }
    }

    private void setWeekInfoDayActivityList(WeekActivityWithDayActivityList weekActivityWithDayActivityList) {

        //데이터를 저장한다.
        WeekActivity weekActivity = new WeekActivity();
        weekActivity.setActivityDate(weekActivityWithDayActivityList.getWeekStartDate());
        weekActivity.setScore(weekActivityWithDayActivityList.getScore());
        weekActivity.setAverageCalorie(weekActivityWithDayActivityList.getAveCalorie());
        weekActivity.setAverageMET(weekActivityWithDayActivityList.getAveMET());
        weekActivity.setAverageStrengthHigh(weekActivityWithDayActivityList.getAveStrengthHigh());
        weekActivity.setAverageStrengthMedium(weekActivityWithDayActivityList.getAveStrengthMedium());
        weekActivity.setAverageStrengthLow(weekActivityWithDayActivityList.getAveStrengthLow());
        weekActivity.setAverageStrengthUnderLow(weekActivityWithDayActivityList.getAveStrengthUnderLow());
        weekActivity.setExerciseProgramID(weekActivityWithDayActivityList.getExerciseProgramID());

        mDBManager.setWeekActivity(weekActivity, FitmateDBManager.UPLOADED);

        List<DayActivity_Rest> daysList = weekActivityWithDayActivityList.getDaysList();
        if (daysList != null) {
            List<DayActivity> dayActivityList = new ArrayList<DayActivity>();
            List<Integer> CalorieList = new ArrayList<Integer>();
            for (int index = 0; index < daysList.size(); index++)
            {
                DayActivity_Rest dayActivity_rest = daysList.get(index);
                DayActivity dayActivity = new DayActivity();
                dayActivity.setStartTime(new TimeInfo(dayActivity_rest.getDate()));
                dayActivity.setCalorieByActivity(dayActivity_rest.getCalorieByActivity());
                CalorieList.add(dayActivity_rest.getCalorieByActivity());
                dayActivity.setAchieveOfTarget(dayActivity_rest.getAchieveOfTarget());
                dayActivity.setDataSaveInterval(dayActivity_rest.getDataSavingInterval());
                dayActivity.setAverageMET(dayActivity_rest.getAverageMet());
                dayActivity.setHmlStrengthTime(new int[]{dayActivity_rest.getStrengthUnderLow(), dayActivity_rest.getStrengthLow(), dayActivity_rest.getStrengthMedium(), dayActivity_rest.getStrengthHigh()});

                byte[] byteMetArray = Base64.decode(dayActivity_rest.getMetArray(), Base64.NO_WRAP);
                float[] metArray = null;
                if (byteMetArray != null) {
                    metArray = DayActivity.convertByteArrayToFloatArray(byteMetArray);
                }

                dayActivity.setMetArray(metArray);

                float fat = 0;
                float carboHydrate = 0;
                double distance = 0;

                if (dayActivity_rest.getFat() == 0) {
                    FatCarboDistance fatCarboDistance = this.calculateFatCarboDistance(dayActivity);
                    fat = fatCarboDistance.fat;
                    carboHydrate = fatCarboDistance.carbohydrate;
                    distance = fatCarboDistance.distance;
                } else {
                    fat = dayActivity_rest.getFat();
                    carboHydrate = dayActivity.getCarbohydrate();
                    distance = dayActivity_rest.getDistance();
                }

                dayActivity.setFat(fat);
                dayActivity.setCarbohydrate(carboHydrate);
                dayActivity.setDistance(distance);

                dayActivityList.add(dayActivity);
            }

            ExerciseProgram exerciseProgram = this.mDBManager.getUserExerciProgram(weekActivity.getExerciseProgramID());
            UserInfo userInfo = mDBManager.getUserInfo();
            UserInfoForAnalyzer userInfoForAnalyzer = com.fitdotlife.fitmate_lib.service.util.Utils.getUserInfoForAnalyzer(userInfo);

            com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseProgram program = Utils.ConvertExerciseProgram(exerciseProgram);
            ExerciseAnalyzer analyzer = new ExerciseAnalyzer(program, userInfoForAnalyzer, WearingLocation.WRIST, 10);
            StrengthInputType strengthType = program.getStrengthInputType();

            List<ScoreClass> scoreClassList = null;
            if (strengthType.equals(StrengthInputType.CALORIE) || strengthType.equals(StrengthInputType.VS_BMR)) {
                scoreClassList = Utils.CalculateDayScores_Calorie(CalorieList, analyzer);
            } else if (strengthType.equals(StrengthInputType.CALORIE_SUM)) {
                scoreClassList = Utils.CalculateDayScores_CalorieSUM(CalorieList, analyzer);
            } else {
                scoreClassList = Utils.CalculateDayScores_Strength(dayActivityList, analyzer);
            }

            for (int i = 0; i < dayActivityList.size(); i++)
            {
                DayActivity act = dayActivityList.get(i);
                ScoreClass score = scoreClassList.get(i);
                act.setPredayScore(score.preDayAchieve);
                act.setTodayScore(score.todayGetPoint);
                act.setExtraScore(score.todayPossibleMorePoint);
                act.setAchieveMax(score.isAchieveMax);
                act.setPossibleMaxScore(score.possibleMaxScore);
                act.setPossibleTimes(score.possibleTimes);
                act.setPossibleValue(score.possibleValue);
                android.util.Log.d("recalcul_point", String.format("pre:%d, today:%d, extra:%d ", score.preDayAchieve, score.todayGetPoint, score.todayPossibleMorePoint));
                this.mDBManager.setDayActivity(act);
            }

        }

        this.mDBManager.setNotUploadData();
    }

    private class NewsClickListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(NewHomeActivity.this, NewsActivity_.class);
            intent.putExtra("newsid", (int) view.getTag());
            startActivity(intent);

            ActivityManager.getInstance().finishAllActivity();

            if ((int)vpNews.getTag() == 0) {
                vpNews.removeCallbacks(pagerRunnable);
                vpNews.setTag(1);
            }
        }
    }

    private void showProgressView() {
        rlWeekProgress.setVisibility(View.VISIBLE);
    }

    private void dismissProgressView() {
        rlWeekProgress.setVisibility(View.INVISIBLE);
    }

    private class FatCarboDistance {
        public float fat;
        public float carbohydrate;
        public double distance;
    }

    @OnActivityResult( DrawerView.REQUEST_PROFILE_IMAGE )
    void onProfileImageChangeResult(){
        this.drawerView.refreshProfileImage();
    }

    @Click(R.id.rl_activity_newhome_actionbar_battery)
    void showBatteryPopup()
    {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View batteryView = inflater.inflate(R.layout.dialog_battery, null);
        BatteryPopupCircleView bpcv = (BatteryPopupCircleView) batteryView.findViewById(R.id.bpcv_dialog_battery_popup);
        TextView tvBatteryValue = (TextView) batteryView.findViewById(R.id.tv_dialog_battery_value);
        TextView tvReccontedTime = (TextView) batteryView.findViewById(R.id.tv_dialog_battery_reconnet_time);
        LinearLayout llNotFoundHelp = (LinearLayout) batteryView.findViewById( R.id.ll_dialog_battery_notfound_help );

        if (!isDeviceFound)
        {

            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) tvBatteryValue.getLayoutParams();
            int topMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 23.7f, this.getResources().getDisplayMetrics());
            params.setMargins( 0, topMargin , 0, 0 );
            tvBatteryValue.setLayoutParams( params );
            tvBatteryValue.setText(this.getResources().getString(R.string.newhome_battery_popup_not_connected));
            tvBatteryValue.setTextSize(TypedValue.COMPLEX_UNIT_SP, 28f);
            bpcv.setBatteryValue(0);
            llNotFoundHelp.setVisibility(View.VISIBLE);

        } else {

            llNotFoundHelp.setVisibility(View.GONE);

            int batteryRatio = mDBManager.getLastBatteryRatio(mUserInfo.getEmail());
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) tvBatteryValue.getLayoutParams();
            int topMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 12.3f, this.getResources().getDisplayMetrics());
            params.setMargins( 0, topMargin , 0, 0 );
            tvBatteryValue.setLayoutParams( params );
            tvBatteryValue.setText(batteryRatio + "%");
            tvBatteryValue.setTextSize(TypedValue.COMPLEX_UNIT_SP, 65.9f);
            bpcv.setBatteryValue(batteryRatio);
        }

        long connectedTime = mDBManager.getLastConnectedTime( mUserInfo.getEmail() );
        if( connectedTime > 0 ) {
            SimpleDateFormat format = new SimpleDateFormat(this.getResources().getString(R.string.newhome_battery_popup_reconnected_date_format));
            Calendar connectedCalendar = Calendar.getInstance();
            connectedCalendar.setTimeInMillis(connectedTime);
            tvReccontedTime.setText(format.format(connectedCalendar.getTime()));
        }else{
            tvReccontedTime.setText(this.getResources().getString(R.string.newhome_battery_popup_not_connected));
        }

        batteryPopup = new PopupWindow(batteryView, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT, true);
        WindowManager windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);

//        Point p = new Point();
//        windowManager.getDefaultDisplay().getSize( p );
//        int screenWidth = p.x;
//        int screenHeight = p.y;
//        int x = screenWidth / 2 - batteryView.getMeasuredWidth() / 2;
//        int y = screenHeight / 2 - batteryView.getMeasuredHeight() / 2;

        batteryPopup.setOutsideTouchable(true);
        batteryPopup.setTouchable(true);
        batteryPopup.setBackgroundDrawable(new BitmapDrawable());
        batteryPopup.setTouchInterceptor(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                batteryPopup.dismiss();
                return true;
            }
        });
        batteryPopup.setAnimationStyle(-1);
        //batteryPopup.showAtLocation(batteryView, Gravity.TOP, x, y);
        batteryPopup.showAtLocation(batteryView, Gravity.CENTER, 0, 0);

    }

    private class PagerRunnable implements Runnable {

        int mPosition = 0;
        int mCount = 0;

        @Override
        public void run() {

            mPosition++;

            if (mPosition < mCount) {
                vpNews.setCurrentItem(mPosition);
                vpNews.postDelayed(pagerRunnable, PagerAnimationDuration);
            } else {

                vpNews.setCurrentItem(0);

            }
        }

        public void setPosition(int position) {
            this.mPosition = position;
        }

        public void setCount(int count) {
            this.mCount = count;
        }
    }

    private class NewHomeWeekPagerAdapter extends WeekPagerAdapter {

        private String[] mWeekStringArray = null;
        private HashMap<String, View> mViewMap = new HashMap<String, View>();

        public NewHomeWeekPagerAdapter(Context context, int weekPagerLength, CategoryType categoryType, View.OnClickListener weekCellClickListener, WeekMoveListener weekMoveListener, long dayMilliseconds, WeekPagerType weekPagerType, WeekPagerActivityInfo[] weekPagerActivityInfoList) {

            super(context, weekPagerLength, categoryType, weekCellClickListener, weekMoveListener, dayMilliseconds, weekPagerType, weekPagerActivityInfoList);
            mWeekStringArray = new String[mWeekPagerLength];

        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            View view = null;

            WeekPagerActivityInfo weekActivityInfo = null;

            if (mWeekPagerActivityInfoList[position] == null) {

                weekActivityInfo = getWeekPagerActivityInfo(position);
                mWeekPagerActivityInfoList[position] = weekActivityInfo;

            } else {
                weekActivityInfo = mWeekPagerActivityInfoList[position];
            }

            view = getWeekGraphView(weekActivityInfo, position);
            addItem(weekActivityInfo.getWeekStartDate(), view);
            container.addView(view);

            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {

            removeItem(position, object);
            container.removeView((View) object);

        }


        private WeekPagerActivityInfo getWeekPagerActivityInfo(int position) {

            int weekIndex = mPagerLength - (position + 1);

            Calendar weekCalendar = Calendar.getInstance();
            weekCalendar.setTimeInMillis(mWeekCalendar.getTimeInMillis());
            weekCalendar.add(Calendar.DATE, -(weekIndex * 7));
            String weekStartDate = NewHomeUtils.getDateString(weekCalendar);

            mWeekStringArray[position] = weekStartDate;
            WeekActivity weekActivity = mDBManager.getWeekActivity(weekStartDate);

            if (weekActivity == null) {
                getWeekActivity(weekStartDate, this);

                while (true)
                {
                    if (mDownloadData) {
                        mDownloadData = false;
                        break;
                    }

                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                weekActivity = mDBManager.getWeekActivity(weekStartDate);
            }

            float strengthFrom = 0;
            float strengthTo = 0;

            ExerciseProgram program = null;
            if (weekActivity != null) {
                program = mDBManager.getUserExerciProgram(weekActivity.getExerciseProgramID());
            } else {
                program = mDBManager.getAppliedProgram();
            }

            StrengthInputType strengthType = StrengthInputType.getStrengthType(program.getCategory());
            if (strengthType.equals(StrengthInputType.CALORIE) || strengthType.equals(StrengthInputType.CALORIE_SUM) || strengthType.equals(StrengthInputType.VS_BMR)) {

                UserInfoForAnalyzer userInfoForAnalyzer = com.fitdotlife.fitmate_lib.service.util.Utils.getUserInfoForAnalyzer(mUserInfo);

                strengthFrom = (int) program.getStrengthFrom();
                strengthTo = (int) program.getStrengthFrom();
                int bmr = userInfoForAnalyzer.getBMR();

                if (strengthType.equals(StrengthInputType.VS_BMR)) {
                    strengthFrom = (int) Math.round(strengthFrom / 100.0 * bmr);
                    strengthTo = (int) Math.round(strengthTo / 100.0 * bmr);
                }

            } else {
                strengthFrom = program.getMinuteFrom();
                strengthTo = program.getMinuteTo();
            }

            String weekRange = "";
            double[] exerciseTimeValues = new double[]{0, 0, 0, 0, 0, 0, 0};
            double[] calorieValues = new double[]{0, 0, 0, 0, 0, 0, 0};
            double[] dailyAchieveValues = new double[]{0, 0, 0, 0, 0, 0, 0};
            double[] weeklyAchieveValues = new double[]{0, 0, 0, 0, 0, 0, 0};
            double[] fatValues = new double[]{0, 0, 0, 0, 0, 0, 0};
            double[] carboHydrateValues = new double[]{0, 0, 0, 0, 0, 0, 0};
            double[] distanceValues = new double[]{0, 0, 0, 0, 0, 0, 0};

            String[] dayStrings = new String[7];
            long[] timeMillis = new long[7];
            int selectedDayIndex = -1;
            int preMonth = -1;

            for (int i = 0; i < dailyAchieveValues.length; i++) {
                String activityDate = newHomeUtils.getDateString(weekCalendar);
                DayActivity dayActivity = mDBManager.getDayActivity(activityDate);

                if (dayActivity != null) {
                    exerciseTimeValues[i] = dayActivity.getStrengthHigh() + dayActivity.getStrengthMedium();
                    calorieValues[i] = dayActivity.getCalorieByActivity();

                    dailyAchieveValues[i] = dayActivity.getAchieveOfTarget();

                    weeklyAchieveValues[i] = dayActivity.getTodayScore();
                    double fat = dayActivity.getFat();
                    if (fat >= 0) {
                        fatValues[i] = dayActivity.getFat();
                    } else {
                        fatValues[i] = calculateFatCarboDistance(dayActivity).fat;
                    }

                    double carbo = dayActivity.getCarbohydrate();
                    if (carbo >= 0) {
                        carboHydrateValues[i] = dayActivity.getCarbohydrate();
                    } else {
                        carboHydrateValues[i] = calculateFatCarboDistance(dayActivity).carbohydrate;
                    }

                    double distance = dayActivity.getDistance();
                    if (distance >= 0) {
                        distanceValues[i] = dayActivity.getDistance();
                    } else {
                        distanceValues[i] = calculateFatCarboDistance(dayActivity).distance;
                    }

                } else {

                    boolean todayAfter = DateUtils.isAfterDay(weekCalendar.getTime(), new Date());
                    if (todayAfter) { //오늘이후 시간이라면 데이터를 없앤다.
                        exerciseTimeValues[i] = TODAY_AFTER_NODATA;
                        calorieValues[i] = TODAY_AFTER_NODATA;
                        dailyAchieveValues[i] = TODAY_AFTER_NODATA;
                        weeklyAchieveValues[i] = TODAY_AFTER_NODATA;
                        fatValues[i] = TODAY_AFTER_NODATA;
                        carboHydrateValues[i] = TODAY_AFTER_NODATA;
                        distanceValues[i] = TODAY_AFTER_NODATA;
                    } else {            //오늘이전 시간이라면 회색으로 보이도록 한다.
                        exerciseTimeValues[i] = TODAY_BEFORE_NODATA;
                        calorieValues[i] = TODAY_BEFORE_NODATA;
                        dailyAchieveValues[i] = TODAY_BEFORE_NODATA;
                        weeklyAchieveValues[i] = TODAY_BEFORE_NODATA;
                        fatValues[i] = TODAY_BEFORE_NODATA;
                        carboHydrateValues[i] = TODAY_BEFORE_NODATA;
                        distanceValues[i] = TODAY_BEFORE_NODATA;
                    }
                }

                timeMillis[i] = weekCalendar.getTimeInMillis();

                int currentMonth = weekCalendar.get(Calendar.MONTH);
                if (preMonth != currentMonth) {
                    dayStrings[i] = DateUtils.getDateStringForPattern(weekCalendar.getTime(), getResources().getString(R.string.newhome_week_bar_date_format));
                    preMonth = currentMonth;
                } else {
                    dayStrings[i] = DateUtils.getDateStringForPattern(weekCalendar.getTime(), getResources().getString(R.string.newhome_week_bar_date_simple_format));
                }

                if (i == 0) {
                    weekRange += DateUtils.getDateStringForPattern(weekCalendar.getTime(), getResources().getString(R.string.newhome_week_table_week_range_format));
                }

                if (i == dailyAchieveValues.length - 1) {
                    weekRange += " - " + DateUtils.getDateStringForPattern(weekCalendar.getTime(), getResources().getString(R.string.newhome_week_table_week_range_format));
                }

                weekCalendar.add(Calendar.DATE, 1);
            }

            if( !mUserInfo.isSI() )
            {

                for( int i = 0 ; i < fatValues.length ; i++ )
                {

                    if( fatValues[i] > -1 ) {
                        fatValues[i] = Utils.convertGramToOunce(fatValues[i]);
                    }
                    if( carboHydrateValues[i] > -1 ) {
                        carboHydrateValues[i] = Utils.convertGramToOunce(carboHydrateValues[i]);
                    }

                    if( distanceValues[i] > -1 ) {
                        distanceValues[i] = Utils.convertKMToMile(distanceValues[i]);
                    }
                }
            }


            WeekPagerActivityInfo weekActivityInfo = new WeekPagerActivityInfo(weekStartDate, weekRange, dayStrings, timeMillis, preMonth, strengthFrom, strengthTo
                    , exerciseTimeValues, calorieValues, dailyAchieveValues, weeklyAchieveValues, fatValues, carboHydrateValues, distanceValues);

            return weekActivityInfo;
        }


        private void addItem(String weekStartDate, View view) {
            mViewMap.put(weekStartDate, view);
        }

        @Override
        protected void getWeekActivityInfo(int position)
        {

            int weekIndex = mPagerLength - (position + 1);

            Calendar weekCalendar = Calendar.getInstance();
            weekCalendar.setTimeInMillis(mWeekCalendar.getTimeInMillis());
            weekCalendar.add(Calendar.DATE, -(weekIndex * 7));
            String weekStartDate = newHomeUtils.getDateString(weekCalendar);

            if (mWeekPagerActivityInfoList[position] == null) {

                mWeekPagerActivityInfoList[position] = getWeekPagerActivityInfo(position);

            }

        }

        private void removeItem(int position, Object object) {
            mViewMap.remove(mWeekStringArray[position]);
            mWeekStringArray[position] = null;
        }

        public String[] getWeekStringList() {
            String[] weekStringList = null;

            if ((mCurrentPosition - 1) >= 0) {

                weekStringList = new String[2];
                weekStringList[0] = mWeekStringArray[mCurrentPosition - 1];
                weekStringList[1] = mWeekStringArray[mCurrentPosition];

            } else if ((mCurrentPosition + 1) < mPagerLength) {

                weekStringList = new String[2];
                weekStringList[0] = mWeekStringArray[mCurrentPosition];
                weekStringList[1] = mWeekStringArray[mCurrentPosition + 1];

            } else {

                weekStringList = new String[3];
                weekStringList[0] = mWeekStringArray[mCurrentPosition - 1];
                weekStringList[1] = mWeekStringArray[mCurrentPosition];
                weekStringList[2] = mWeekStringArray[mCurrentPosition + 1];

            }

            return weekStringList;
        }

        public int getPosition(String weekStartDate) {

            int position = -1;
            if (mWeekStringArray != null) {

                for (int i = mWeekStringArray.length; i > 0; i--) {

                    if (weekStartDate.equals(mWeekStringArray[i - 1])) {
                        position = i - 1;
                        break;
                    }
                }

            } else {
                position = -1;
            }

            return position;

        }

        public View getView(String weekDate) {
            return mViewMap.get(weekDate);
        }

        @Override
        public void OnWeekActivityReceived(FriendWeekActivity friendWeekActivity) {

        }

        @Override
        public void OnWeekActivityReceived(FriendWeekActivity[] friendWeekActivityList) {

        }
    }

    @Click(R.id.tv_activity_newhome_actionbar_categoryedit_cancel)
    void categoryEditCancelClick() {
        cancelCategoryEdit();
    }

    private void cancelCategoryEdit() {

        rlActionBar.setVisibility(View.VISIBLE);
        hsvCategory.setVisibility(View.VISIBLE);
        rlTabBar.setOnClickListener(new ActionBarClickListener());

        rlCategoryEditActionBar.setVisibility(View.GONE);
        llCategoryEditPopup.setVisibility(View.GONE);
        hsvCategoryEdit.setVisibility(View.GONE);


        rlCategoryEdit.removeAllViews();
//        for( int categoryIndex = 0 ; categoryIndex < CategoryType.values().length ; categoryIndex++ ){
//            CategoryType categoryType = CategoryType.values()[ categoryIndex ];
//            NewHomeCategoryView categoryView = categoryViews[ categoryType.ordinal() ];
//            categoryView.setVisible(false);
//        }
//
//        for( int viewIndex = 0 ; viewIndex < mCategoryViewOrder.length ; viewIndex++ ){
//            CategoryType categoryType = mCategoryViewOrder[ viewIndex ];
//            NewHomeCategoryView categoryView = categoryViews[ categoryType.ordinal() ];
//            categoryView.setVisible( true );
//            rlCategory.addView( categoryView );
//        }

        rlCategory.addView(mCategoryEditCategoryView);
        Animation slide_down = AnimationUtils.loadAnimation(NewHomeActivity.this, R.anim.slide_down);
        llCategoryEditPopup.startAnimation(slide_down);

    }

    @Click(R.id.tv_activity_newhome_actionbar_categoryedit_save)
    void categoryEditSaveClick() {
        //저장을 누를 때 최소한 1개는 선택되어 있어야 함.
        if (rlCategoryEdit.getChildCount() < 1) {

            AlertDialog.Builder builder = new AlertDialog.Builder(NewHomeActivity.this);

            builder.setMessage(getString(R.string.tab_setting_select_one))
                    .setCancelable(true)        // 뒤로 버튼 클릭시 취소 가능 설정
                    .setPositiveButton(R.string.common_ok, new DialogInterface.OnClickListener() {
                        // 확인 버튼 클릭시 설정
                        public void onClick(DialogInterface dialog, int whichButton) {

                            dialog.cancel();
                        }
                    });
            AlertDialog dialog = builder.create();    // 알림창 객체 생성
            dialog.show();    // 알림창 띄우기

            return;
        }


        rlActionBar.setVisibility(View.VISIBLE);
        hsvCategory.setVisibility(View.VISIBLE);
        rlTabBar.setOnClickListener(new ActionBarClickListener());

        rlCategoryEditActionBar.setVisibility(View.GONE);
        llCategoryEditPopup.setVisibility(View.GONE);
        hsvCategoryEdit.setVisibility(View.GONE);

        //이전에 선택된 카테고리 타입을 가져온다.
        CategoryType preSelectCategoryType = mCategoryViewOrder[selectedCategoryIndex];

        mCategoryViewOrder = new CategoryType[rlCategoryEdit.getChildCount()];
        View[] dayGraphs = new View[rlCategoryEdit.getChildCount()];
        StringBuilder strViewOrder = new StringBuilder();

        //selectedCategoryIndex 초기화
        selectedCategoryIndex = 0;

        rlCategory.removeAllViews();
        float lastViewRight = 0;
        float defaultMargin = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 26.9f, getResources().getDisplayMetrics());
        float categoryViewWidth = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 107.5f, getResources().getDisplayMetrics());

        for (int viewIndex = 0; viewIndex < rlCategoryEdit.getChildCount(); viewIndex++)
        {
            NewHomeCategoryView categoryEditView = (NewHomeCategoryView) rlCategoryEdit.getChildAt(viewIndex);
            CategoryType categoryType = categoryEditView.getCategoryType();

            NewHomeCategoryView categoryView = categoryViews[categoryType.ordinal()];
            categoryView.setViewIndex(viewIndex);

            if ( viewIndex > 0)
            {
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
                params.leftMargin = (int) ( lastViewRight - defaultMargin );
                lastViewRight = lastViewRight + ( categoryViewWidth - defaultMargin );
                rlCategory.addView( categoryView , params);
            }else {
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
                params.leftMargin = 0;
                lastViewRight = categoryViewWidth;
                rlCategory.addView(categoryView , params);
            }

            //이전에 선택되었던 카테고리가 있다면 그 ViewIndex를 selectedCategoryIndex로 설정한다.
            if (preSelectCategoryType.equals(categoryType)) {
                selectedCategoryIndex = viewIndex;
            }

            //ViewOrder 생성
            mCategoryViewOrder[viewIndex] = categoryType;

            //DayAdater용 View 배열 만듬.
            View dayView = dayViews[categoryType.ordinal()];
            dayView.setTag(viewIndex);
            dayGraphs[viewIndex] = dayView;

            //액티비티 초기에 읽어들일 ViewOrder 문자열을 만든다.
            strViewOrder.append(categoryType.ordinal()).append(",");
        }

        rlCategoryEdit.removeAllViews();

        //카테고리를 선택한다.
        ((NewHomeCategoryView) rlCategory.getChildAt(selectedCategoryIndex)).select();

        //일 Adapter를 다시 설정한다.
        mDayPagerAdapter = new DayPagerAdapter(this, dayGraphs , mCategoryViewOrder );
        vpDay.setAdapter(mDayPagerAdapter);
        vpDay.setCurrentItem(selectedCategoryIndex);

        //카테고리 수정버튼을 추가한다.
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
        params.leftMargin = (int) ( lastViewRight - defaultMargin );
        rlCategory.addView(mCategoryEditCategoryView , params);
        changeCategoryViewGravity(rlCategory, hsvCategory);

        //현재 선택된 카테고리 타입을 가져온다.
        CategoryType categoryType = mCategoryViewOrder[selectedCategoryIndex];

        if (!categoryType.equals(preSelectCategoryType)) {
            if (categoryType.equals(CategoryType.WEEKLY_ACHIEVE)) {

                vpWeekBar.setVisibility(View.INVISIBLE);
                vpWeekTable.setVisibility(View.VISIBLE);
                mWeekTablePagerAdapter.setDayTimeMilliseconds(mDayCalendar.getTimeInMillis());
                mWeekTablePagerAdapter.setSelectedDayIndex(mWeekBarPagerAdapter.getSelectedDayIndex());

                if (vpWeekTable.getCurrentItem() != vpWeekBar.getCurrentItem()) {

                    mWeekTablePagerAdapter.setMoveWeek(false);
                    vpWeekTable.setCurrentItem(vpWeekBar.getCurrentItem());

                }

            } else if (preSelectCategoryType.equals(CategoryType.WEEKLY_ACHIEVE)) {

                vpWeekBar.setVisibility(View.VISIBLE);
                vpWeekTable.setVisibility(View.INVISIBLE);
                mWeekBarPagerAdapter.setDayTimeMilliseconds(mDayCalendar.getTimeInMillis());
                mWeekBarPagerAdapter.setSelectedDayIndex(mWeekTablePagerAdapter.getSelectedDayIndex());
                mWeekBarPagerAdapter.changeCategoryType(categoryType);
                if (vpWeekTable.getCurrentItem() != vpWeekBar.getCurrentItem()) {

                    mWeekBarPagerAdapter.setMoveWeek(false);
                    vpWeekBar.setCurrentItem(vpWeekTable.getCurrentItem());

                }

            } else {

                mWeekBarPagerAdapter.changeCategoryType(categoryType);

            }
        }

        //슬라이드 업 애니메이션
        Animation slide_down = AnimationUtils.loadAnimation(NewHomeActivity.this, R.anim.slide_down);
        llCategoryEditPopup.startAnimation(slide_down);

        //HomeActivity 초기에 카테고리 순서를 읽어올 수 있도록 카테고리 순서를 문자열로 저장한다.
        SharedPreferences pref = this.getSharedPreferences("fitmate", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(NewHomeActivity.VIEWORDER_KEY, strViewOrder.toString());
        editor.apply();
    }

    @SuppressWarnings("ResourceType") //이거 안하면 sign application 만들때 에러 발생함.
    private class CategoryEditClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {

            rlActionBar.setVisibility(View.GONE);
            hsvCategory.setVisibility(View.GONE);
            rlTabBar.setOnClickListener(null);

            rlCategoryEditActionBar.setVisibility(View.VISIBLE);
            llCategoryEditPopup.setVisibility(View.VISIBLE);
            hsvCategoryEdit.setVisibility(View.VISIBLE);

            rlCategoryEdit.removeAllViews();
            rlCategory.removeView(mCategoryEditCategoryView);

            tvCategoryEditSave.setAlpha(0.2f);
            tvCategoryEditSave.setEnabled(false);

            CategoryEditInfo[] categoryEditInfos = new CategoryEditInfo[categoryViews.length];
            categoryEditViews = new NewHomeCategoryView[CategoryType.values().length];
            for (int i = 0; i < categoryViews.length; i++) {
                NewHomeCategoryView categoryView = categoryViews[i];
                CategoryEditInfo categoryEditInfo = categoryView.getCategoryEditInfo();
                categoryEditInfo.setCategoryType(CategoryType.values()[i]);
                categoryEditInfos[i] = categoryEditInfo;
            }

            float lastViewRight = 0;
            float defaultMargin = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 26.9f, getResources().getDisplayMetrics());
            float categoryViewWidth = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 107.5f, getResources().getDisplayMetrics());

            for (int viewIndex = 0; viewIndex < rlCategory.getChildCount(); viewIndex++)
            {

                NewHomeCategoryView categoryView = (NewHomeCategoryView) rlCategory.getChildAt(viewIndex);
                NewHomeCategoryView categoryEditView = NewHomeCategoryView_.build(NewHomeActivity.this);
                categoryView.copyContents(categoryEditView);
                categoryEditView.setCategoryIntroListener( new CategoryIntroListener() );


                int[] categoryAttrs = new int[]{ R.attr.newhomeCategoryEditCancel , R.attr.newhomeCategoryEditSave , R.attr.newhomeCategoryEditTitle ,  R.attr.newhomeCategoryNormal, R.attr.newhomeCategorySelect , R.attr.newhomeCategoryTextNormal ,
                        R.attr.newhomeCategoryTextSelect , R.attr.newhomeCategoryValueTextNormal , R.attr.newhomeCategoryValueTextSelect};

                TypedArray categoryTypedArray = obtainStyledAttributes(MyApplication.tabStyleList[MyApplication.mSelectedTabStyleIndex], categoryAttrs);
                int newhomeCategoryEditCancel = categoryTypedArray.getColor( 0, 0 );
                int newhomeCategoryEditSave = categoryTypedArray.getColor( 1, 0 );
                int newhomeCategoryEditTitle = categoryTypedArray.getColor( 2, 0 );
                Drawable newhomeCategoryNormal = categoryTypedArray.getDrawable(3);
                Drawable newhomeCategorySelect = categoryTypedArray.getDrawable(4);
                int newhomeCategoryTextNormal = categoryTypedArray.getColor(5, 0);
                int newhomeCategoryTextSelect = categoryTypedArray.getColor(6, 0);
                int newhomeCategoryValueTextSelect = categoryTypedArray.getColor(7, 0);
                int newhomeCategoryValueTextNormal = categoryTypedArray.getColor(8, 0);
                categoryTypedArray.recycle();

                categoryEditView.changeNewHomeCategoryStyle( newhomeCategorySelect , newhomeCategoryNormal , newhomeCategoryTextSelect , newhomeCategoryTextNormal , newhomeCategoryValueTextSelect , newhomeCategoryValueTextNormal );

                categoryEditView.unSelect();

                tvCategoryEditCancel.setTextColor( newhomeCategoryEditCancel );
                tvCategoryEditSave.setTextColor( newhomeCategoryEditSave );
                tvCategoryEditTitle.setTextColor( newhomeCategoryEditTitle );

                if ( viewIndex > 0)
                {
                    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
                    params.leftMargin = (int) ( lastViewRight - defaultMargin );
                    lastViewRight = lastViewRight + ( categoryViewWidth - defaultMargin );
                    rlCategoryEdit.addView(categoryEditView , params);
                }else {
                    lastViewRight = categoryViewWidth;
                    rlCategoryEdit.addView(categoryEditView);
                }

                categoryEditViews[categoryView.getCategoryType().ordinal()] = categoryEditView;

                CategoryEditInfo categoryEditInfo = categoryEditInfos[categoryView.getCategoryType().ordinal()];
                categoryEditInfo.setVisible(true);
            }

            rlCategoryEdit.post(new Runnable() {
                @Override
                public void run() {
                    changeCategoryViewGravity(rlCategoryEdit, hsvCategoryEdit);
                }
            });

            gvCategoryEditPopup.setAdapter(new CategoryEditAdapter(NewHomeActivity.this, categoryEditInfos , new CategoryEditSelectListener()));
//            gvCategoryEditPopup.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                @Override
//                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//
//                    NewHomeCategoryEditItemView categoryEditItemView = (NewHomeCategoryEditItemView) view;
//                    boolean isVisible = categoryEditItemView.isVisible();
//                    if (isVisible) {
//
//                        //카테고리 수정 삭제뷰에서 없어짐.
//                        NewHomeCategoryView newHomeCategoryView = categoryViews[categoryEditItemView.getCategoryType().ordinal()];
//                        newHomeUtils.unSelectCategoryView(newHomeCategoryView);
//
//                        rlCategoryEdit.removeView(categoryEditViews[categoryEditItemView.getCategoryType().ordinal()]);
//                        changeCategoryViewGravity(rlCategoryEdit, hsvCategoryEdit);
//
//                        categoryEditItemView.setImage(R.drawable.newhome_categoryedit_item_normal);
//                        categoryEditItemView.setVisible(false);
//
//                        tvCategoryEditSave.setAlpha(1);
//                        tvCategoryEditSave.setEnabled(true);
//
//                    } else {
//                        //카테고리 수정 삭제 뷰에서 다시 보임.
//                        NewHomeCategoryView categoryView = categoryViews[categoryEditItemView.getCategoryType().ordinal()];
//
//                        NewHomeCategoryView categoryEditView = NewHomeCategoryView_.build(NewHomeActivity.this);
//                        categoryEditView.setCategoryIntroListener( new CategoryIntroListener() );
//                        categoryView.copyContents(categoryEditView);
//                        newHomeUtils.unSelectCategoryView(categoryEditView);
//                        rlCategoryEdit.addView(categoryEditView);
//                        categoryEditViews[categoryEditView.getCategoryType().ordinal()] = categoryEditView;
//
//                        changeCategoryViewGravity(rlCategoryEdit, hsvCategoryEdit);
//
//                        categoryEditItemView.setImage(R.drawable.newhome_categoryedit_item_select_day);
//                        categoryEditItemView.setVisible(true);
//                        categoryEditItemView.setCategoryViewIndex(rlCategoryEdit.getChildCount() - 1);
//
//                        tvCategoryEditSave.setAlpha(1);
//                        tvCategoryEditSave.setEnabled(true);
//                    }
//                }
//            });

            Animation slide_up = AnimationUtils.loadAnimation(NewHomeActivity.this, R.anim.slide_up);
            llCategoryEditPopup.startAnimation(slide_up);
        }
    }

    private class CategoryEditAdapter extends BaseAdapter {

        private Context mContext = null;
        private CategoryEditInfo[] arrCategoryEditInfo = null;
        private CategoryEditSelectListener mSelectListener = null;

        public CategoryEditAdapter(Context context, CategoryEditInfo[] categoryEditInfos , CategoryEditSelectListener selectListener) {

            this.mContext = context;
            this.arrCategoryEditInfo = categoryEditInfos;
            this.mSelectListener = selectListener;

        }

        @Override
        public int getCount() {
            return arrCategoryEditInfo.length;
        }

        @Override
        public Object getItem(int position) {
            return arrCategoryEditInfo[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            NewHomeCategoryEditItemView newHomeCategoryEditItemView = null;
            if (convertView == null) {
                newHomeCategoryEditItemView = NewHomeCategoryEditItemView_.build(mContext);
            } else {
                newHomeCategoryEditItemView = (NewHomeCategoryEditItemView) convertView;
            }

            int[] attrs = new int[]{R.attr.newhomeCategoryEditItemSelect};
            TypedArray a = mContext.obtainStyledAttributes( MyApplication.tabStyleList[MyApplication.mSelectedTabStyleIndex ] , attrs );
            Drawable selectDrawable = a.getDrawable(0);
            a.recycle();
            newHomeCategoryEditItemView.setSelectDrawable( selectDrawable );

            newHomeCategoryEditItemView.bind(arrCategoryEditInfo[position]);
            newHomeCategoryEditItemView.setCategoryEditSelectListener(mSelectListener);
            newHomeCategoryEditItemView.setCategoryIntroListener(new CategoryIntroListener());

            return newHomeCategoryEditItemView;
        }
    }

    private void changeCategoryViewGravity( RelativeLayout llView, HorizontalScrollView hsvView) {

        int viewWidthSum = 0;

        float viewPartWidth = TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP , 80.7f , getResources().getDisplayMetrics() );
        float viewWidth = TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP , 107.7f , getResources().getDisplayMetrics() );

        for (int i = 0; i < llView.getChildCount(); i++)
        {
            if( i == ( llView.getChildCount() - 1 ) ){
                viewWidthSum += viewWidth;
            }else {
                viewWidthSum += viewPartWidth;
            }
        }

        if (viewWidthSum < hsvView.getMeasuredWidth()) {
            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) llView.getLayoutParams();
            params.width = LinearLayout.LayoutParams.MATCH_PARENT;
            params.gravity = Gravity.CENTER_HORIZONTAL;

            llView.setLayoutParams(params);

        } else {

            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) llView.getLayoutParams();
            params.width = LinearLayout.LayoutParams.WRAP_CONTENT;
            params.gravity = Gravity.NO_GRAVITY;

            llView.setLayoutParams(params);
        }
    }

    @Click(R.id.img_activity_newhome_program)
    void programClick() {

        ExerciseProgram program = getExerciseProgram();

        UserInfo userInfo = mDBManager.getUserInfo();
        UserInfoForAnalyzer userInfoForAnalyzer = com.fitdotlife.fitmate_lib.service.util.Utils.getUserInfoForAnalyzer(userInfo);
        com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseProgram exerciseProgram = mDBManager.getAppliedExerciseProgram();
        WearingLocation location = WearingLocation.getWearingLocation(userInfo.getWearAt());
        ExerciseAnalyzer analyzer = new ExerciseAnalyzer( exerciseProgram , userInfoForAnalyzer , location , 10 );
        newHomeUtils.showProgramDialog( program , true , analyzer);
    }

    @Click(R.id.img_activity_newhome_met)
    void metClick() {

        //오늘 날짜의 데이터중 메트 데이터를 가져온다

        float[] values = null;
        DayActivity dayActivity = mDBManager.getDayActivity(DateUtils.getDateString(mDayCalendar));
        if(dayActivity != null){
            values = dayActivity.getMetArray();
        }

        //선택된 탭이 일일 달성도, 일간 누적점수가 아닐 때 텍스트 뷰의 아래의 선을 안보이도록 한다.
        //팝업이 돌출될 때 DayTextView의 하단 라인을 안보이게 한다.

        CategoryType selectCategoryType = mCategoryViewOrder[selectedCategoryIndex];
        if (!selectCategoryType.equals(CategoryType.DAILY_ACHIEVE) && !selectCategoryType.equals(CategoryType.WEEKLY_ACHIEVE)) {
            NewHomeDayTextView newHomeDayTextView = (NewHomeDayTextView) dayViews[selectCategoryType.ordinal()];
            newHomeDayTextView.setLineVisibility(View.INVISIBLE);
        }

        //메트 그래프의 값을 설정한다.
        metChartView.setValues(values);
        ExerciseProgram exProgram = mDBManager.getAppliedProgram();
        StrengthInputType inputType = StrengthInputType.values()[exProgram.getCategory()];

        if( inputType.equals( StrengthInputType.CALORIE ) || inputType.equals( StrengthInputType.CALORIE_SUM ) || inputType.equals( StrengthInputType.VS_BMR ) ){

            metChartView.setIntensityCharacterList(new String[]{newhomeHighIntensity, newhomeMediumIntensity, newhomeLowIntensity});
            metChartView.setChartType(NewHomeMETChartView.ChartType.CALORIE);

        }else{

            UserInfo userInfo = mDBManager.getUserInfo();
            UserInfoForAnalyzer userInfoForAnalyzer = com.fitdotlife.fitmate_lib.service.util.Utils.getUserInfoForAnalyzer(userInfo);

            com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseProgram program = Utils.ConvertExerciseProgram(exProgram);
            ExerciseAnalyzer analyzer = new ExerciseAnalyzer(program, userInfoForAnalyzer, WearingLocation.WRIST, 10);


            float fromStrength = Float.parseFloat(String.format("%.1f", analyzer.getMetFrom()));
            float toStrength = Float.parseFloat(String.format("%.1f", analyzer.getMetTo()));
            metChartView.setMetRange( fromStrength , toStrength );
            metChartView.setChartType(NewHomeMETChartView.ChartType.MET );
        }


        metChartView.invalidate();

        metChartView.setMetChartTouchListener(new NewHomeMETChartView.MetChartTouchListener(  ) {
            @Override
            public void onTouch(  float x , float y , float met , int metIndex , float chartStartX , float chartWidth ) {

                if( metChartInfoView.getVisibility() != View.VISIBLE ){ metChartInfoView.setVisibility( View.VISIBLE ); }
                metChartInfoView.setMETInfo( x , y , met , metIndex , chartStartX , chartWidth );
                metChartInfoView.invalidate();
            }
        });

        //메트 그래프를 보이게 한다.
        llMetContainer.setVisibility(View.VISIBLE);

        Animation slide_up = AnimationUtils.loadAnimation(NewHomeActivity.this, R.anim.slide_up);
        llMetContainer.startAnimation(slide_up);
        slide_up.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                metChartView.startAnimation();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void dismissMetGraph(){
        //선택된 탭이 일일 달성도, 일간 누적점수가 아닐 때 텍스트 뷰의 아래의 선을 안보이도록 한다.
        //팝업이 돌출될 때 DayTextView의 하단 라인을 안보이게 한다.

        CategoryType selectCategoryType = mCategoryViewOrder[selectedCategoryIndex];
        if (!selectCategoryType.equals(CategoryType.DAILY_ACHIEVE) && !selectCategoryType.equals(CategoryType.WEEKLY_ACHIEVE)) {
            NewHomeDayTextView newHomeDayTextView = (NewHomeDayTextView) dayViews[selectCategoryType.ordinal()];
            newHomeDayTextView.setLineVisibility(View.VISIBLE);
        }

        metChartInfoView.setVisibility(View.GONE);

        Animation slide_down = AnimationUtils.loadAnimation( NewHomeActivity.this, R.anim.slide_down );
        llMetContainer.startAnimation(slide_down);
        //메트 그래프를 사라지게 한다.
        llMetContainer.setVisibility(View.GONE);
    }

    @Touch(R.id.view_activity_newhome_met_background)
    boolean metGraphBackgroundTouch( MotionEvent event )
    {
        if( event.getAction() == MotionEvent.ACTION_DOWN ) {
            dismissMetGraph();
        }

        return true;
}

    @Click(R.id.img_activity_newhome_met_help)
    void metHelpClick(){

        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View metHelpView = inflater.inflate(R.layout.dialog_met_intro, null);

        TextView tvMETTitle = (TextView) metHelpView.findViewById(R.id.tv_dialog_met_intro_title);
        TextView tvMETIntro = (TextView) metHelpView.findViewById(R.id.tv_dialog_met_intro_content);
        ImageView imgMETIntro = (ImageView) metHelpView.findViewById(R.id.img_dialog_met_intro_activityclass);
        imgMETIntro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent( NewHomeActivity.this , ActivityClassActivity_.class );
                intent.putExtra( ActivityClassActivity.TYPE_KEY , ActivityClassActivity.COMMON );
                startActivity(intent);

                metDialog.dismiss();
            }
        });

        tvMETTitle.setText("MET");
        tvMETIntro.setText(this.getResources().getString(R.string.newhome_met_help));

        AlertDialog.Builder builder = new AlertDialog.Builder( this );
        builder.setView(metHelpView);
        metDialog = builder.create();    // 알림창 객체 생성
        metDialog.setCanceledOnTouchOutside(true);
        metDialog.show();    // 알림창 띄우기

        metHelpView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                metDialog.dismiss();
                metDialog = null;
                return true;
            }
        });
    }

    public class CategoryIntroListener implements View.OnClickListener{

        @Override
        public void onClick(View view) {
            newHomeUtils.showCategoryIntroDialog( (Integer) view.getTag() );
        }
    }

    @SuppressWarnings("ResourceType") //이거 안하면 sign application 만들때 에러 발생함.
    public class CategoryEditSelectListener implements View.OnClickListener
    {
        @Override
        public void onClick(View view ) {

            NewHomeCategoryEditItemView categoryEditItemView = (NewHomeCategoryEditItemView) view.getTag();
            boolean isVisible = categoryEditItemView.isVisible();

            if (isVisible) {

                if (rlCategoryEdit.getChildCount() < 2)
                {
                    Toast.makeText( NewHomeActivity.this , R.string.tab_setting_select_one , Toast.LENGTH_SHORT ).show();
                    return;
                }

                //카테고리 수정 삭제뷰에서 없어짐.
                NewHomeCategoryView newHomeCategoryView = categoryViews[categoryEditItemView.getCategoryType().ordinal()];
                newHomeCategoryView.unSelect();
                rlCategoryEdit.removeView(categoryEditViews[ categoryEditItemView.getCategoryType().ordinal()] );

                float lastViewRight = 0;
                float defaultMargin = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 26.9f, getResources().getDisplayMetrics());
                float categoryViewWidth = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 107.5f, getResources().getDisplayMetrics());
                for (int viewIndex = 0; viewIndex < rlCategoryEdit.getChildCount() ; viewIndex++)
                {
                    View childView = rlCategoryEdit.getChildAt( viewIndex );
                    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
                    if (viewIndex > 0)
                    {
                        params.leftMargin = (int) (lastViewRight - defaultMargin);
                        lastViewRight = lastViewRight + (categoryViewWidth - defaultMargin);

                    } else {
                        params.leftMargin = 0;
                        lastViewRight = categoryViewWidth;
                    }

                    childView.setLayoutParams( params );

                }

                changeCategoryViewGravity(rlCategoryEdit, hsvCategoryEdit);

                categoryEditItemView.setImage(R.drawable.newhome_categoryedit_item_normal);
                categoryEditItemView.setVisible(false);

                tvCategoryEditSave.setAlpha(1);
                tvCategoryEditSave.setEnabled(true);

            } else {
                //카테고리 수정 삭제 뷰에서 다시 보임.
                NewHomeCategoryView categoryView = categoryViews[categoryEditItemView.getCategoryType().ordinal()];

                int[] categoryAttrs = new int[]{ R.attr.newhomeCategoryNormal, R.attr.newhomeCategorySelect ,
                        R.attr.newhomeCategoryTextNormal , R.attr.newhomeCategoryTextSelect , R.attr.newhomeCategoryValueTextNormal , R.attr.newhomeCategoryValueTextSelect };

                TypedArray categoryTypedArray = obtainStyledAttributes(MyApplication.tabStyleList[MyApplication.mSelectedTabStyleIndex], categoryAttrs);
                Drawable newhomeCategoryNormal = categoryTypedArray.getDrawable(0);
                Drawable newhomeCategorySelect = categoryTypedArray.getDrawable(1);

                int newhomeCategoryTextNormal = categoryTypedArray.getColor(2, 0);
                int newhomeCategoryTextSelect = categoryTypedArray.getColor(3, 0);
                int newhomeCategoryValueTextNormal = categoryTypedArray.getColor(5, 0);
                int newhomeCategoryValueTextSelect = categoryTypedArray.getColor( 6 ,0 );
                categoryTypedArray.recycle();

                NewHomeCategoryView categoryEditView = NewHomeCategoryView_.build(NewHomeActivity.this);
                categoryEditView.changeNewHomeCategoryStyle( newhomeCategorySelect , newhomeCategoryNormal , newhomeCategoryTextSelect , newhomeCategoryTextNormal , newhomeCategoryValueTextSelect , newhomeCategoryValueTextNormal  );
                categoryEditView.setCategoryIntroListener(new CategoryIntroListener());
                categoryView.copyContents(categoryEditView);
                categoryEditView.unSelect();

                categoryEditViews[categoryEditView.getCategoryType().ordinal()] = categoryEditView;

                float defaultMargin = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 26.9f, getResources().getDisplayMetrics());
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);

                View lastChildView = rlCategoryEdit.getChildAt( rlCategoryEdit.getChildCount() -1  );

                params.leftMargin = (int) ( lastChildView.getRight()  - defaultMargin);
                rlCategoryEdit.addView( categoryEditView , params);

                changeCategoryViewGravity(rlCategoryEdit, hsvCategoryEdit);

                int[] attrs = new int[]{R.attr.newhomeCategoryEditItemSelect};
                TypedArray a = obtainStyledAttributes(MyApplication.tabStyleList[MyApplication.mSelectedTabStyleIndex], attrs);
                Drawable selectDrawable = a.getDrawable(0);
                a.recycle();

                categoryEditItemView.setSelectDrawable(selectDrawable);
                categoryEditItemView.setImageDrawable( selectDrawable );
                categoryEditItemView.setVisible(true);
                categoryEditItemView.setCategoryViewIndex(rlCategoryEdit.getChildCount() - 1);

                tvCategoryEditSave.setAlpha(1);
                tvCategoryEditSave.setEnabled(true);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if( requestCode == PERMISSION_REQUEST_LOCATION ){

            if( grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED ){
                initBluetooth();

            }else{

                Toast.makeText( this , this.getResources().getString( R.string.home_location_permission_reject ) , Toast.LENGTH_LONG ).show();
                setCheckBluetoothOn(true);
                MyApplication.LOCATION_PERMISSION_GRANTED = false;
                isDeviceFound = false;
                showBatteryView();
                setBattery();
            }
        }
    }

   synchronized public void processWithReceivedFiles( List<ReceivedDataInfo> receivedDataInfoList )
   {

        ContinuousCheckPolicy policy = ContinuousCheckPolicy.Loosely_SumOverXExercise;
        //현재 적용된 운동 프로그램을 가져옵니다.
        com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseProgram appliedExerciseProgram = this.mDBManager.getAppliedExerciseProgram();
        //사용자 정보를 가져옵니다.
        UserInfo userInfo = this.mDBManager.getUserInfo();
        //사용자 정보로부터 Analyzer 정보를 가져온다.
        UserInfoForAnalyzer userInfoForAnalyzer = com.fitdotlife.fitmate_lib.service.util.Utils.getUserInfoForAnalyzer(userInfo);

        Log.d(TAG, "RawDataList 크기 : " + receivedDataInfoList.size());

        if( receivedDataInfoList.isEmpty() ){
            Log.d(TAG , "데이터가 없습니다...");
            return;
        }

        ActivityParser activityParser = new ActivityParser();

        BluetoothRawData lastSavedData= mServiceDBManager.getLastFileData();
        ReceivedDataInfo lastData=null;
        for( int i = 0 ; i < receivedDataInfoList.size() ;i++ )
        {
            ReceivedDataInfo rawData = receivedDataInfoList.get(i);
            if(rawData.getBytes()==null)
            {
                continue;
            }
            if(lastSavedData!=null && lastSavedData.getFileData()!=null && lastSavedData.getFileIndex()== rawData.getIndex())
            {
                Log.e(TAG, "lastSavedData index:" + lastSavedData.getFileIndex() + " size:" + lastSavedData.getFileData().length);
                Log.e(TAG, "rawdata index:" + rawData.getIndex() + " size:" + rawData.getBytes().length);
                byte[] c = new byte[lastSavedData.getFileData().length + rawData.getBytes().length];
                System.arraycopy(lastSavedData.getFileData(), 0, c, 0, lastSavedData.getFileData().length);
                System.arraycopy(rawData.getBytes(), 0, c, lastSavedData.getFileData().length, rawData.getBytes().length);
                rawData.setOffset(c.length);
                Log.e(TAG, "offset result" + (rawData.getBytes().length+ lastSavedData.getFileData().length));
                rawData.setBytes(c);
                Log.e(TAG, "length result" + c.length);
            }

            try {
                Log.e(TAG , "last index - " + rawData.getIndex() );
                activityParser.parse(rawData.getBytes(), rawData.getIndex());
                lastData= rawData;
            } catch (DataParseException dpe) {
                Log.e( TAG ,"데이터 파일 분석중에 오류가 발생했습니다. Index = " + rawData.getIndex() );
                Log.e( TAG , "오류 내용 : " + dpe.getMessage() );
                continue;
            } catch (HeaderParseException e) {
                Log.e( TAG ,"데이터 파일 분석중에 헤더에서 오류가 발생했습니다. Index = " + rawData.getIndex() );
                Log.e(TAG, "오류 내용 : " + e.getMessage());
                this.mServiceDBManager.deleteFile(rawData.getIndex());
                continue;
            }
        }

        List<ActivityData> activityDataList = activityParser.getActivityData();
        if( activityDataList.size() == 0 ) {
            return;
        }

        List<DayActivity> dayActivityList = new ArrayList<DayActivity>();

        //rawdataList의 데이터들을 날짜별 데이터로 만든다.
        for( int index = 0 ; index < activityDataList.size() ;index++ )
        {
            try
            {
                ActivityData activityData = activityDataList.get(index);

                int wearingPosition = activityData.getSystemInfoResponse().getSystemInfo().getWearingPosition();
                //Log.i( TAG , "착용 위치 : " + wearingPosition );

                WearingLocation location = WearingLocation.WAIST;
                if( wearingPosition > 1 ) {
                    location = WearingLocation.values()[activityData.getSystemInfoResponse().getSystemInfo().getWearingPosition() - 1];
                }

                ExerciseAnalyzer analyzer = new ExerciseAnalyzer( appliedExerciseProgram, userInfoForAnalyzer, location , (int) activityData.getSaveInterval());
                List<Integer> svmList = activityData.getSVMList();

                int calorieByActivity = 0;
                float[] metArray = new float[(24 * 60 * 60) / (int) activityData.getSaveInterval()];

                DayActivity dayActivity = new DayActivity();
                float[] rawMetArray = new float[svmList.size()];
                dayActivity.addFileIndex(activityData.getRawFileIndex());
                dayActivity.setWearingLocation( location );

                //칼로리와 MET를 계산한다.
                int metArrayIndex = ((activityData.getStartTime().getHour() * 60 * 60) + (activityData.getStartTime().getMinute() * 60) + activityData.getStartTime().getSecond()) / (int) activityData.getSaveInterval();
                for (int i = 0; i < svmList.size(); i++)
                {
                    int calorie = analyzer.Calcuate_Calorie(svmList.get(i));
                    calorieByActivity += calorie;
                    float met = analyzer.Calculate_METbyCaloire(calorie);
                    rawMetArray[i] = met;
                    if(  ( metArrayIndex + i ) < metArray.length  ) {
                        metArray[metArrayIndex + i] = met;
                    }
                }


                //저중고 강도 운동시간을 계산한다.
                int[] arrHMLTime = analyzer.MinutesforEachStrength(rawMetArray);

                dayActivity.setRawMetArray(rawMetArray);
                dayActivity.setStartTime(activityData.getStartTime());
                dayActivity.setCalorieByActivity((int) Math.round(calorieByActivity / 1000.0));
                dayActivity.setHmlStrengthTime(arrHMLTime);
                dayActivity.setMetArray(metArray);
                dayActivity.setDataSaveInterval((int) activityData.getSaveInterval());

                //평균 MET를 계산한다.
                dayActivity.setAverageMET(analyzer.CalculateAverageMET(rawMetArray));

                //AhcieveOfTarget를 계산한다.
                StrengthInputType strengthInputType= appliedExerciseProgram.getStrengthInputType();
                int todayDayOfAchieve = 0;

                if(isCalorieType(strengthInputType)){
                    todayDayOfAchieve= dayActivity.getCalorieByActivity();
                }else{
                    //AchieveOfTarget 를 계산한다.
                    List<ContinuousExerciseInfo> todayExerciseInfoList = analyzer.getContinuousExercise(policy, rawMetArray, dayActivity.getStartTime().getMilliSecond() , 10, 60, 60, 600);

                    if (todayExerciseInfoList.size() > 0) {
                        ContinuousExerciseInfo todayExerciseInfo = todayExerciseInfoList.get(0);
                        todayDayOfAchieve = Math.round((todayExerciseInfo.getTimeSpan()) / (60 * 1000));
                    }
                }

                //지방, 탄수화물, 이동거리를 계산한다.
                List<Float> metList = new ArrayList<>();
                for( float f : metArray ){
                    metList.add( f );
                }

                FatAndCarbonhydrateConsumtion fatBurn = analyzer.getFatandCarbonhydrate( metList , (int) activityData.getSaveInterval() );
                float fat = fatBurn.getFatBurned();
                float carbon = fatBurn.getCarbonhydrateBurned();
                double distance = ExerciseAnalyzer.getDistanceFromMETArray_over2MET( metList , (int) activityData.getSaveInterval() );

                dayActivity.setFat( fat );
                dayActivity.setCarbohydrate( carbon );
                dayActivity.setDistance(distance);

                dayActivity.setAchieveOfTarget(todayDayOfAchieve);

                //이전데이터와 겹치는지 확인한다.
                if (dayActivityList.size() > 0) {

                    //최근에 날짜목록에 저장된 데이터를 가져온다. 날짜목록에 있는 데이터의 날짜와 현재의 날짜가 겹치는 확인한다.
                    //저장간격이 같은지 확인한다.
                    //날짜도 같고 저장간격도 같다면 데이터를 합친다.
                    DayActivity preDayActivity = dayActivityList.get(dayActivityList.size() - 1);

                    if (dayActivity.getActivityDate().equals(preDayActivity.getActivityDate())) { //날짜가 겹치는지 확인한다.
                        preDayActivity.add(dayActivity);
                        continue;
                    }
                }

                dayActivityList.add(dayActivity);

            }catch( Exception e ){
                Log.e(TAG, e.getMessage());
                return;
            }

        }//END FOR

        if(lastData!=null){
            mServiceDBManager.updateFileData( lastData.getIndex() , lastData.getBytes() );
            Log.e(TAG, "Last updated Data ==> index:" + lastData.getIndex() + ", offset:" + lastData.getBytes().length);
        }


        //마지막 날짜의 시작 파일 인덱스를 저장한다.
        Log.i(TAG, "Day Activity 목록의 크기 : " + dayActivityList.size() );

        //####
        // 일 데이터를 DB에 삽입한다.
        // 삽입한 일 데이터에 해당하는 주는 다시 계산한다.
        //####
        ArrayList<String> weeksNeedUpdate= new ArrayList<>();
        String weekStartDate = null;
        final JSONArray jsonWeekArray = new JSONArray();

        for(int i=0;i<dayActivityList.size();i++)
        {
            DayActivity day=  mDBManager.getDayActivity( dayActivityList.get(i).getActivityDate() );
            if(day==null){
                mDBManager.setDayActivity(dayActivityList.get(i));
            }else{
                WearingLocation location =  dayActivityList.get(i).getWearingLocation();
                ExerciseAnalyzer analyzer = new ExerciseAnalyzer(appliedExerciseProgram, userInfoForAnalyzer, location, (int) day.getDataSaveInterval());
                dayActivityList.set(i, com.fitdotlife.fitmate_lib.service.util.Utils.mergyDayActivity(day, dayActivityList.get(i), analyzer) );
                mDBManager.updateDayActivity( dayActivityList.get(i) );
            }

            Calendar weekCalendar = Calendar.getInstance();
            weekCalendar.setTimeInMillis( dayActivityList.get(i).getStartTime().getMilliSecond() );

            int dayOfWeek = weekCalendar.get(Calendar.DAY_OF_WEEK);
            weekCalendar.add(Calendar.DATE, -(dayOfWeek - 1));
            weekStartDate = this.getDateString( weekCalendar );

            if( !weeksNeedUpdate.contains( weekStartDate ) )
            {
                weeksNeedUpdate.add(( weekStartDate ));
            }

            JSONObject dayObject = new JSONObject();
            try {
                dayObject.put("daydate" , dayActivityList.get(i).getActivityDate() );
                dayObject.put("dayindex" , dayOfWeek);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            boolean isContain = false;
            for( int j = 0 ; j < jsonWeekArray.length() ; j++ )
            {

                try {

                    JSONObject jsonWeekObject = jsonWeekArray.getJSONObject(j);
                    if(weekStartDate.equals( jsonWeekObject.getString("weekdate") )){

                        isContain = true;
                        JSONArray jsonDayArray = jsonWeekObject.getJSONArray("daylist");
                        jsonDayArray.put( dayObject );
                        break;

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            if( !isContain ){

                try {
                    JSONObject jsonWeekObject = new JSONObject();
                    JSONArray jsonDayArray = new JSONArray();
                    jsonDayArray.put(dayObject);
                    jsonWeekObject.put("weekdate", weekStartDate );
                    jsonWeekObject.put("daylist", jsonDayArray);
                    jsonWeekArray.put(jsonWeekObject);
                }catch(JSONException e){ }
            }

        }//End for

        for(int i=0;i<weeksNeedUpdate.size();i++){
            recalculateweek(DateUtils.getDateFromString_yyyy_MM_dd(weeksNeedUpdate.get(i)));
        }

        //오늘 목표 로컬 푸쉬를 확인한다.
        checkLocalPush();
        runOnUiThread(new Runnable()
        {
            @Override
            public void run() {
                calculateCompleted(jsonWeekArray.toString() );
            }
        });



    }

    private boolean isCalorieType(StrengthInputType type){
        if(type == StrengthInputType.CALORIE || type== StrengthInputType.CALORIE_SUM || type==StrengthInputType.VS_BMR){
            return true;
        }else{
            return false;
        }
    }

    private String getDateString( Calendar calendar )
    {
        return calendar.get(Calendar.YEAR) + "-" +String.format("%02d", calendar.get(Calendar.MONTH) + 1) + "-" + String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH));
    }

    private boolean recalculateweek( Date weekStartDate )
    {
        FitmateDBManager dbManager = new FitmateDBManager(this);
        UserInfo userInfo =dbManager.getUserInfo();
        WeekActivity weekActivity= dbManager.getWeekActivity(DateUtils.getDateString_yyyy_MM_dd(weekStartDate));

        Date weekEndDate = new Date( weekStartDate.getTime() + 86400*6*1000 );
        List<DayActivity> dayActivitySet = dbManager.getDayActivityList(DateUtils.getDateString_yyyy_MM_dd(weekStartDate), DateUtils.getDateString_yyyy_MM_dd(weekEndDate));

        DayActivity lastDayActivity= dayActivitySet.get( dayActivitySet.size() - 1 );

        int exerciseProgramId= dbManager.getAppliedProgram().getId();

        // 주간 데이터가 없을 때 확인한다.
        boolean needInsert= true;

        if(weekActivity==null)
        {
            needInsert= true;
            weekActivity = new WeekActivity();
            weekActivity.setActivityDate(DateUtils.getDateString_yyyy_MM_dd(weekStartDate));
            weekActivity.setExerciseProgramID(exerciseProgramId);

        }else{
            exerciseProgramId = weekActivity.getExerciseProgramID();
            needInsert= false;
        }
        //   weekTable = lastDayActivityTable.datasavinginterval;
        com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseProgram exTable =dbManager.getExerciseProgramForAnalyzer(exerciseProgramId);

        UserInfoForAnalyzer userInfoForAnalyzer = com.fitdotlife.fitmate_lib.service.util.Utils.getUserInfoForAnalyzer(userInfo);

        com.fitdotlife.fitdotlifelib.ExerciseAnalysis.ExerciseProgram program =dbManager.getExerciseProgramForAnalyzer(exerciseProgramId);

        WearingLocation location = WearingLocation.getWearingLocation(mDBManager.getUserInfo().getWearAt());
        ExerciseAnalyzer analyzer = new ExerciseAnalyzer(exTable, userInfoForAnalyzer, location , (int) lastDayActivity.getDataSaveInterval());

        List<Integer> arrayforScore = new ArrayList<Integer>();

        boolean calorieType=false;

        if(program.getStrengthInputType()== StrengthInputType.CALORIE|| program.getStrengthInputType()== StrengthInputType.CALORIE_SUM ||program.getStrengthInputType()== StrengthInputType.VS_BMR){
            calorieType=true;
        }
        Date tempDate=weekStartDate;

        int str_h=0;
        int str_m=0;
        int str_l=0;
        int str_ul=0;
        int calorie=0;
        float metSm=0;
        int nullIndex=0;

        List<Integer> nullIndexArray= new ArrayList<Integer>();
        for(DayActivity table: dayActivitySet)
        {

            Date date = DateUtils.getDateFromString_yyyy_MM_dd(table.getActivityDate());

            while( tempDate.compareTo(date) <0){
                arrayforScore.add(0);
                tempDate=new Date(tempDate.getTime()+86400000);
                nullIndexArray.add(nullIndex);
                nullIndex++;
            }
            if(calorieType){
                table.setAchieveOfTarget(table.getCalorieByActivity());
            }else{
                List<ContinuousExerciseInfo> continuousEx = analyzer.getContinuousExercise(ContinuousCheckPolicy.Loosely_SumOverXExercise, table.getMetArray(),date.getTime(), 1, 60,60, 600 );
                if(continuousEx==null || continuousEx.size()==0){
                    table.setAchieveOfTarget(0);
                }
                else{
                    if(analyzer.getUserInfo().getContinuousCheckPolicy() == ContinuousCheckPolicy.Loosely_SumOverXExercise){
                        int seconds = 0;
                        for (ContinuousExerciseInfo info : continuousEx) {
                            seconds+= info.getTimeSpan()/1000;
                        }
                        table.setAchieveOfTarget((int)Math.round(seconds / 60.0));
                    }
                    else{
                        int minutes =(int) Math.round( continuousEx.get(0).getTimeSpan()/(60.0*1000));
                        table.setAchieveOfTarget(minutes);
                    }
                }
            }

            if(table.getProgramID()!= exerciseProgramId)
            {
                table.setProgramID(exerciseProgramId);
            }
            arrayforScore.add(table.getAchieveOfTarget());
            calorie += table.getCalorieByActivity();
            metSm += table.getAverageMET();
            str_h+= table.getStrengthHigh();
            str_m+= table.getStrengthMedium();
            str_l+= table.getStrengthLow();
            str_ul+= table.getStrengthUnderLow();
            tempDate=new Date(tempDate.getTime() +86400*1000);

            nullIndex++;
        }



        if(dayActivitySet.size()>0){

            str_h = str_h/dayActivitySet.size();

            str_m = str_m/dayActivitySet.size();

            str_l = str_l/dayActivitySet.size();

            str_ul = str_ul/dayActivitySet.size();

            calorie =calorie/dayActivitySet.size();

            metSm = metSm/dayActivitySet.size();

        }

        weekActivity.setAverageCalorie(calorie);

        weekActivity.setAverageStrengthHigh(str_h);
        weekActivity.setAverageStrengthMedium(str_m);
        weekActivity.setAverageStrengthLow(str_l);
        weekActivity.setAverageStrengthUnderLow(str_ul);

        weekActivity.setAverageMET(metSm);

        weekActivity.setActivityDate(DateUtils.getDateString_yyyy_MM_dd(weekStartDate));

        List<ScoreClass> scoreArray = new ArrayList<ScoreClass>();
        if(calorieType){
            scoreArray=CommonFunction.CalculateDayScores_Calories(arrayforScore , analyzer);

        }
        else{
            scoreArray=CommonFunction.CalculateDayScores_Minutes(arrayforScore , analyzer);
        }
        int nth=0;

        for(int i=0;i<scoreArray.size() ;i++)
        {
            if(nth<nullIndexArray.size()){

                if( nullIndexArray.get(nth)== i){

                    nth++;
                    continue;
                }

            }



            ScoreClass score= scoreArray.get(i);

            DayActivity act= dayActivitySet.get(i-nth);
            act.setPredayScore(score.preDayAchieve);
            act.setTodayScore(score.todayGetPoint);
            act.setExtraScore(score.todayPossibleMorePoint);
            act.setAchieveMax(score.isAchieveMax);
            act.setPossibleMaxScore(score.possibleMaxScore);
            act.setPossibleTimes(score.possibleTimes);
            act.setPossibleValue(score.possibleValue);
            ///notuploaded==0
            act.setUploadState(0);

            dbManager.updateDayActivity( act );
        }

        ScoreClass score= scoreArray.get(scoreArray.size()-1);

        weekActivity.setScore(score.weekAchieve);

        if(needInsert){
            dbManager.setWeekActivity(weekActivity , FitmateDBManager.NOT_UPLOADED );
        }else{
            dbManager.updateWeekActivity(weekActivity);
        }

        return true;
    }

    private void checkLocalPush()
    {
        Date todayDate = new Date();
        String strTodayDate = DateUtils.getDateString_yyyy_MM_dd(todayDate);
        DayActivity todayActivity = mDBManager.getDayActivity( strTodayDate );

        //오늘 날짜 데이터가 없으면
        if( todayActivity == null ) return;

        //주간 목표 확인
        Calendar weekCalendar = Calendar.getInstance();
        int dayOfWeek = weekCalendar.get(Calendar.DAY_OF_WEEK);
        weekCalendar.add(Calendar.DATE, -(dayOfWeek - 1));
        String weekStartDate = DateUtils.getDateString(weekCalendar);
        WeekActivity weekActivity = mDBManager.getWeekActivity(weekStartDate);

        if( weekActivity.getScore() >= 100 )
        {
            //주간 목표 달성 푸쉬를 보냈는지 확인한다.
            if( !isSendWeekAchievePush( weekStartDate ) ){
                showNotification( "Fitmate" , getString( R.string.noti_week_achieve_success) , getString( R.string.noti_week_achieve_success) , 522 );
                setSendWeekAchievePush( weekStartDate , true );
            }
            //주간 목표를 달성했으면 오늘 운동 목표에 대한 푸쉬를 보내지 않는다.
            return;
        }

        boolean isCaloridType = false;
        float goalValue = 0;
        //운동 프로그램을 가져온다.
        com.fitdotlife.fitmate_lib.object.ExerciseProgram program = mDBManager.getAppliedProgram();
        //운동프로그램 종류를 확인한다.
        StrengthInputType strengthInputType = StrengthInputType.values()[ program.getCategory() ];

        if( strengthInputType== StrengthInputType.CALORIE|| strengthInputType == StrengthInputType.CALORIE_SUM || strengthInputType == StrengthInputType.VS_BMR){
            isCaloridType = true;

            if( strengthInputType.equals( StrengthInputType.CALORIE) ){
                goalValue = program.getStrengthFrom();
            }else if(strengthInputType.equals( StrengthInputType.CALORIE_SUM )){

                goalValue = program.getStrengthFrom() / 7;

            }else{

                UserInfoForAnalyzer userInfoForAnalyzer = com.fitdotlife.fitmate_lib.service.util.Utils.getUserInfoForAnalyzer(mDBManager.getUserInfo());
                //strengthFrom=(int)Math.round(strengthFrom /100.0 * bmr);
                //strengthTo=(int)Math.round(strengthTo /100.0 * bmr);
                //goalValue =  ( userInfoForAnalyzer.getBMR() * program.getStrengthFrom() ) * 100;
                goalValue = (int)Math.round( program.getStrengthFrom() / 100.0 * userInfoForAnalyzer.getBMR() );
            }

        }else{

            goalValue = program.getMinuteFrom();
        }

        int achieveOfTarget = todayActivity.getAchieveOfTarget();
        int achievePercent = (int) (( achieveOfTarget / goalValue ) * 100);

        DayAchieveType dayAchieveType = DayAchieveType.values()[getDayAchieve(strTodayDate) ];
        if(achievePercent < 50) //오늘 목표대비 50% 미만일 때 암것도 안한다.
        {
        }
        else if( achievePercent < 80 ) //오늘 목표대비 50% 이상 ~ 80% 미만일 때
        {
            if( !dayAchieveType.equals( DayAchieveType.DAY_ACHIEVE_OVER_50) )
            {
                String achieveMessage = String.format( getString(R.string.noti_day_achieve_over_50) , achievePercent );
                int possibleValue = todayActivity.getPossibleValue();
                if( isCaloridType ){
                    achieveMessage +=  " " + String.format( getString(R.string.noti_day_achieve_need_calorie) , ( possibleValue - achieveOfTarget ) );
                }else{
                    achieveMessage +=  " " + String.format( getString(R.string.noti_day_achieve_need_intensity) , ( possibleValue - achieveOfTarget ) );
                }

                showNotification("Fitmate", achieveMessage, achieveMessage , 523);
                setDayAchieve(strTodayDate, DayAchieveType.DAY_ACHIEVE_OVER_50.ordinal());
            }

        }else if( achievePercent < 100 ){ //오늘 목표대비 80% 이상 ~ 100% 미만일 때

            if( !dayAchieveType.equals( DayAchieveType.DAY_ACHIEVE_OVER_80) ) {

                String achieveMessage = String.format( getString(R.string.noti_day_achieve_over_50) , achievePercent);
                int possibleValue = todayActivity.getPossibleValue();

                if( isCaloridType ){
                    achieveMessage +=  " " + String.format( getString(R.string.noti_day_achieve_need_calorie) , ( possibleValue - achieveOfTarget ) );
                }else{
                    achieveMessage +=  " " + String.format( getString(R.string.noti_day_achieve_need_intensity) , ( possibleValue - achieveOfTarget ) );
                }

                showNotification("Fitmate", achieveMessage, achieveMessage , 524);
                setDayAchieve(strTodayDate, DayAchieveType.DAY_ACHIEVE_OVER_80.ordinal());
            }

        }else{ //오늘 목표대비 100% 달성했을 때

            if( !dayAchieveType.equals( DayAchieveType.DAY_ACHIEVE_100 ) )
            {
                showNotification("Fitmate", getString(R.string.noti_day_achieve_success), getString(R.string.noti_day_achieve_success) , 525);
                setDayAchieve( strTodayDate , DayAchieveType.DAY_ACHIEVE_100.ordinal() );
            }
        }


    }

    public static enum DayAchieveType{ DAY_ACHIEVE_BELOW_50, DAY_ACHIEVE_OVER_50, DAY_ACHIEVE_OVER_80, DAY_ACHIEVE_100  };
    public static final String DAY_ACHIEVE_KEY = "DayAchieve";
    public static final String WEEK_ACHIEVE_KEY = "WeekAchieve";


    private boolean isSendWeekAchievePush( String weekStartDate  ){
        SharedPreferences pref = getSharedPreferences("fitmate", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        return pref.getBoolean(WEEK_ACHIEVE_KEY + "_" + weekStartDate, false);
    }

    private void setSendWeekAchievePush( String weekStartDate , boolean isSetWeekAchieve   ){
        SharedPreferences pref = getSharedPreferences("fitmate", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(WEEK_ACHIEVE_KEY + "_" + weekStartDate, isSetWeekAchieve);
        editor.commit();
    }

    private int getDayAchieve( String dayDate  ){
        SharedPreferences pref = getSharedPreferences("fitmate", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        return pref.getInt(DAY_ACHIEVE_KEY + "_" + dayDate, 0);
    }

    private void showNotification( String title , String content , String notification , int id )
    {
        NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, SplashActivity_.class), PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mCompatBuilder = new NotificationCompat.Builder(this);
        mCompatBuilder.setSmallIcon(R.drawable.fitlife_ico);
        mCompatBuilder.setTicker(notification);
        mCompatBuilder.setWhen(System.currentTimeMillis());
        mCompatBuilder.setContentTitle(title);
        mCompatBuilder.setContentText(content);
        mCompatBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(content));
        mCompatBuilder.setVisibility(Notification.VISIBILITY_PUBLIC);
        mCompatBuilder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);
        mCompatBuilder.setContentIntent(pendingIntent);
        mCompatBuilder.setAutoCancel(true);

        nm.notify(222, mCompatBuilder.build());
    }

    private void setDayAchieve( String dayDate , int dayAchieve   ){
        SharedPreferences pref = getSharedPreferences("fitmate", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(DAY_ACHIEVE_KEY + "_" + dayDate, dayAchieve);
        editor.commit();
    }

    private long getLastBatteryCheckTime() {
        SharedPreferences pref = this.getSharedPreferences("fitmate", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        return pref.getLong(this.BATTERY_CHECK_TIME_KEY, 0);
    }

    private void setLastBatteryCheckTime(long batteryCheckTime ) {
        SharedPreferences pref = this.getSharedPreferences("fitmate", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putLong(this.BATTERY_CHECK_TIME_KEY, batteryCheckTime);
        editor.commit();
    }

    private FirmwareVersionInfo getDeviceFirmwareVersionInfo(){
        SharedPreferences pref = getSharedPreferences("fitmate", Activity.MODE_PRIVATE);
        FirmwareVersionInfo version = new FirmwareVersionInfo();
        version.setMajorVersion(pref.getInt(FirmwareVersionInfo.DEVICE_MAJORVERSION_KEY, -1));
        version.setMinorVersion(pref.getInt(FirmwareVersionInfo.DEVICE_MINORVERSION_KEY, -1));
        return version;
    }

    private void setDeviceFirmwareVersionInfo(int majorVersion, int minorVersion )
    {
        SharedPreferences pref = getSharedPreferences("fitmate", Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt( FirmwareVersionInfo.DEVICE_MAJORVERSION_KEY, majorVersion);
        editor.putInt(FirmwareVersionInfo.DEVICE_MINORVERSION_KEY, minorVersion);
        editor.commit();
    }

    private boolean getBatteryNoti()
    {
        SharedPreferences pref = getSharedPreferences(getString(R.string.app_name), Activity.MODE_PRIVATE);
        boolean batteryNoti = pref.getBoolean("BatteryNoti", false);
        return batteryNoti;
    }

    private void setBatteryNoti(boolean batteryNoti )
    {
        SharedPreferences pref = getSharedPreferences(getString(R.string.app_name), Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean("BatteryNoti", batteryNoti);
        editor.commit();
    }

    private void setFitmeterNotUseAlarm()
    {
        //long connectedTime = mDBManager.getLastConnectedTime(mUserInfo.getEmail());
        //Calendar currentTimeCalendar = Calendar.getInstance();
        long alarmTime = SystemClock.elapsedRealtime() + (8 * 60 * 60 * 1000);
        //long alarmTime = SystemClock.elapsedRealtime() + (2 * 60 * 1000);

        //if(alarmTime > currentTimeCalendar.getTimeInMillis())
        //{
            AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            Intent intent = new Intent(this, FitmeterNotUseAlarmReceiver.class);
            PendingIntent alarmIntent = PendingIntent.getBroadcast(this, 0, intent, 0);
            alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, alarmTime, alarmIntent);

            Log.e(TAG, "8시간 알람 등록함.");
        //}

    }

    private void cancelFitmeterNotUseAlarm()
    {
        AlarmManager alarmManager = (AlarmManager) getSystemService( Context.ALARM_SERVICE );
        Intent intent = new Intent( this , FitmeterNotUseAlarmReceiver.class);
        PendingIntent alarmIntent = PendingIntent.getBroadcast(this, 0, intent, 0);
        alarmManager.cancel(alarmIntent);
    }

    @SuppressWarnings("ResourceType") //이거 안하면 sign application 만들때 에러 발생함.
    private void changeTabStyle( int themeResource )
    {
//        int[] attrs = new int[]{ R.attr.newhomeBatteryCover , R.attr.newhomeCategoryBackground , R.attr.newhomeCategoryNormal, R.attr.newhomeCategorySelect ,
//        R.attr.newhomeCategoryTextNormal , R.attr.newhomeCategoryTextSelect, R.attr.newhomeCategoryValueTextSelect , R.attr.newhomeCategoryValueTextNormal ,
//        R.attr.newhomeBatteryEmpty , R.attr.newhomeBatteryFill , R.attr.newhomeBatteryText , R.attr.newhomeSync , R.attr.newhomeDrawer, R.attr.newhomeDateText , R.attr.newhomeDateArrow };

        //카테고리
        int[] categoryAttrs = new int[]{ R.attr.newhomeCategoryBackground ,  R.attr.newhomeCategoryNormal, R.attr.newhomeCategorySelect , R.attr.newhomeCategorySwipe ,
                R.attr.newhomeCategoryTextNormal , R.attr.newhomeCategoryTextSelect , R.attr.newhomeCategoryValueTextNormal , R.attr.newhomeCategoryValueTextSelect , R.attr.newhomeDateArrow , R.attr.newhomeDateText  };

        TypedArray categoryTypedArray = this.obtainStyledAttributes(themeResource, categoryAttrs);
        Drawable newhomeTabBackground = categoryTypedArray.getDrawable(0);
        Drawable newhomeCategoryNormal = categoryTypedArray.getDrawable(1);
        Drawable newhomeCategorySelect = categoryTypedArray.getDrawable(2);
        Drawable newhomeCategorySwipe = categoryTypedArray.getDrawable(3);

        int newhomeCategoryTextNormal = categoryTypedArray.getColor(4, 0);
        int newhomeCategoryTextSelect = categoryTypedArray.getColor(5, 0);
        int newhomeCategoryValueTextNormal = categoryTypedArray.getColor(6, 0);
        int newhomeCategoryValueTextSelect = categoryTypedArray.getColor(7, 0);
        Drawable newhomeDateArrow = categoryTypedArray.getDrawable(8);
        int newhomeDateText = categoryTypedArray.getColor(9, 0);

        categoryTypedArray.recycle();

        int[] batteryAttrs = new int[]{ R.attr.newhomeBatteryCover , R.attr.newhomeBatteryEmpty , R.attr.newhomeBatteryFill , R.attr.newhomeBatteryNone , R.attr.newhomeBatteryText , R.attr.newhomeDrawer , R.attr.newhomeSync  };
        TypedArray batteryTypedArray = this.obtainStyledAttributes(themeResource, batteryAttrs);

        Drawable newhomeBatteryCover = batteryTypedArray.getDrawable(0);
        int newhomeBatteryEmpty = batteryTypedArray.getColor(1, 0);
        int newhomeBatteryFill = batteryTypedArray.getColor(2, 0);
        int newhomeBatteryNone = batteryTypedArray.getColor(3, 0);
        int newhomeBatteryText = batteryTypedArray.getColor(4, 0);
        Drawable newhomeDrawer = batteryTypedArray.getDrawable(5);
        Drawable newhomeSync = batteryTypedArray.getDrawable(6);

        batteryTypedArray.recycle();

        rlTabBar.setBackground(newhomeTabBackground);

        for (NewHomeCategoryView categoryView : categoryViews )
        {
            categoryView.changeNewHomeCategoryStyle( newhomeCategorySelect , newhomeCategoryNormal , newhomeCategoryTextSelect , newhomeCategoryTextNormal , newhomeCategoryValueTextSelect , newhomeCategoryValueTextNormal );
        }

        int[] categoryEditAtts = new int[]{ R.attr.newhomeCategoryEdit , R.attr.newhomeCategoryNormal , R.attr.newhomeCategorySelect, R.attr.newhomeCategoryTextNormal };
        TypedArray categoryEditTypedArray = this.obtainStyledAttributes(themeResource, categoryEditAtts);

        Drawable newhomeCategoryEdit = categoryTypedArray.getDrawable(0);
        Drawable newhomeCategoryEditNormal = categoryEditTypedArray.getDrawable(1);
        Drawable newhomeCategoryEditSelect = categoryEditTypedArray.getDrawable(2);
        int newhomeCategoryEditText = categoryEditTypedArray.getColor(3, 0);
        categoryEditTypedArray.recycle();

        StateListDrawable categoryEditSelector = new StateListDrawable();
        categoryEditSelector.addState(new int[]{android.R.attr.state_pressed}, newhomeCategoryEditSelect );
        categoryEditSelector.addState(new int[]{}, newhomeCategoryEditNormal);
        mCategoryEditCategoryView.setImageBackground(categoryEditSelector);
        mCategoryEditCategoryView.setCategoryEditImage(newhomeCategoryEdit);
        mCategoryEditCategoryView.setCategoryTextColor(newhomeCategoryEditText);

        ivCategorySwipe.setBackground(newhomeCategorySwipe);

        ivBatteryBackground.setImageDrawable(newhomeBatteryCover);
        batteryCircleView.setBatteryEmptyColor(newhomeBatteryEmpty);
        batteryCircleView.setBatteryFillColor(newhomeBatteryFill);
        batteryCircleView.invalidate();

        imgBatteryValue.setColorFilter( newhomeBatteryNone );

        tvBatteryValue.setTextColor( newhomeBatteryText );

        imgSync.setImageDrawable( newhomeSync );
        tvSyncPercent.setTextColor( newhomeBatteryText );

        imgDrawer.setImageDrawable( newhomeDrawer );

        tvDate.setTextColor( newhomeDateText );
        tvWeekDay.setTextColor( newhomeDateText );
        imgCalendarArrow.setImageDrawable( newhomeDateArrow );

    }

    private class ActionBarClickListener implements View.OnClickListener{

        @Override
        public void onClick(View view) {
            if( MyApplication.mSelectedTabStyleIndex == ( MyApplication.tabStyleList.length - 1 ) ){
                MyApplication.mSelectedTabStyleIndex = 0;
            }else{
                MyApplication.mSelectedTabStyleIndex++;
            }

            changeTabStyle(MyApplication.tabStyleList[MyApplication.mSelectedTabStyleIndex]);
        }
    }

    private void setInitialAlarm( boolean initialAlarm )
    {
        SharedPreferences pref = this.getSharedPreferences( "fitmateservice" , Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(INIT_AlARM_KEY, initialAlarm);
        editor.commit();
    }

    private boolean getInitialAlarm(  )
    {
        SharedPreferences pref = this.getSharedPreferences("fitmateservice", Context.MODE_PRIVATE);
        return pref.getBoolean( INIT_AlARM_KEY , false );
    }

    private void setActivityNotiAlarm()
    {
        Calendar alarmCalendar = Calendar.getInstance();
        alarmCalendar.set(Calendar.HOUR_OF_DAY , 18 );
        alarmCalendar.set(Calendar.MINUTE, 0);

        Calendar currentTimeCalendar = Calendar.getInstance();
        AlarmManager alarmManager = (AlarmManager) getSystemService( Context.ALARM_SERVICE );
        Intent intent = new Intent( this , ActivityNotiAlarmReceiver.class);
        PendingIntent alarmIntent = PendingIntent.getBroadcast(this , 0 , intent, 0 );

        long alarmTimeMilliSecond = alarmCalendar.getTimeInMillis();
        long currentTimeMilliSecond = currentTimeCalendar.getTimeInMillis();
        long timeGap = alarmTimeMilliSecond - currentTimeMilliSecond;

        if (timeGap > 0) {
            alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + timeGap , alarmIntent);
        } else {
            alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + timeGap + AlarmManager.INTERVAL_DAY, alarmIntent);
        }

        Log.e(TAG, "저녁 6시 알람 등록함.");
    }

    @Background
    void setWearNotiAlarm( )
    {
        UserNotiSetting userNotiSetting = null;
        //서버에 알람정보가 있는지 확인한다.
        try {
            userNotiSetting = userInfoServiceClient.getUserNotiSetting(mUserInfo.getEmail());
        }catch ( RestClientException e ){
        }

        if( userNotiSetting != null )
        {
            this.mDBManager.setUserNotiSetting( userNotiSetting );
        }else{
            userNotiSetting = this.mDBManager.getUserNotiSetting();
        }

        if( !userNotiSetting.isFitmeterwearnoti() )
        {
            return;
        }

        String alarmTime = userNotiSetting.getAlarmtime();

        Calendar alarmCalendar = Calendar.getInstance();
        Calendar tempCalendar = Calendar.getInstance();

        SimpleDateFormat sdf = new SimpleDateFormat( "HH:mm" , Locale.getDefault() );
        try {
            tempCalendar.setTime(sdf.parse(alarmTime));
        } catch (ParseException e) {
            e.printStackTrace();
            alarmCalendar = null;
        }

        alarmCalendar.set(Calendar.HOUR_OF_DAY, tempCalendar.get(Calendar.HOUR_OF_DAY));
        alarmCalendar.set(Calendar.MINUTE, tempCalendar.get(Calendar.MINUTE));

        Calendar currentTimeCalendar = Calendar.getInstance();
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent( this , WearNotiAlarmReceiver.class);
        PendingIntent alarmIntent = PendingIntent.getBroadcast( this , 0 , intent , 0 );
        long alarmTimeMilliSecond = alarmCalendar.getTimeInMillis();
        long currentTimeMilliSecond = currentTimeCalendar.getTimeInMillis();
        long timeGap = alarmTimeMilliSecond - currentTimeMilliSecond;

        if( timeGap > 0 ){
            alarmManager.set( AlarmManager.ELAPSED_REALTIME_WAKEUP , SystemClock.elapsedRealtime() + timeGap , alarmIntent );
            Log.e(TAG, "특정시간 알람 등록함.");
        }else{
            alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + timeGap + AlarmManager.INTERVAL_DAY, alarmIntent);
            Log.e(TAG, "특정시간 알람 등록함.");
        }
    }

    private void cancelWearNotiAlarm()
    {
        AlarmManager alarmManager = (AlarmManager) getSystemService( Context.ALARM_SERVICE );
        Intent intent = new Intent( NewHomeActivity.this , WearNotiAlarmReceiver.class);
        PendingIntent alarmIntent = PendingIntent.getBroadcast( this , 0 , intent , 0 );
        alarmManager.cancel( alarmIntent );
    }


    private void cancelActivityNotiAlarm()
    {
        AlarmManager alarmManager = (AlarmManager) getSystemService( Context.ALARM_SERVICE );
        Intent intent = new Intent( NewHomeActivity.this , ActivityNotiAlarmReceiver.class);
        PendingIntent alarmIntent = PendingIntent.getBroadcast( this , 0 , intent , 0 );
        alarmManager.cancel( alarmIntent );
    }

}
