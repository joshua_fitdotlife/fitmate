package com.fitdotlife.fitmate;

import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.SmallTest;

import com.fitdotlife.fitmate_lib.key.FatFoodType;

import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.assertTrue;

/**
 * Created by Joshua on 2016-03-24.
 */
@RunWith(AndroidJUnit4.class)
@SmallTest
public class FatFoodTypeTest {

    @Test
    public void selectFatFoodType(){
        FatFoodType selectFatFoodType = FatFoodType.selectFatFoodType(39);
        assertTrue( selectFatFoodType.ordinal() <= 3 );
    }


}
